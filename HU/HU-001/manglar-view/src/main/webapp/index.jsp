<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
</head>
<body>
<%-- Redirected because we can't set the welcome page to a virtual URL. --%>
<c:redirect url="/login.xhtml"/>
</body>
</html>


