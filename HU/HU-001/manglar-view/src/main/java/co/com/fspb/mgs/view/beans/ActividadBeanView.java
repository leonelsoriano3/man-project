package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ActividadLazyList;
import co.com.fspb.mgs.view.lazylist.ComponenteLazyList;
import co.com.fspb.mgs.view.lazylist.EstadoActividadLazyList;
import co.com.fspb.mgs.view.lazylist.UsuarioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.facade.api.ActividadFacadeAPI;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link Actividad}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ActividadBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ActividadBeanView extends AbstractManagedBeanBase<ActividadDTO> {

    private static final String FORM_PG_FORM = "formActividad:pgForm";
    private static final String I18N_MSG = "msgActividad";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-actividad";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ActividadBeanView.class);

    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ActividadLazyList lazyDataModel;

    private ComponenteLazyList componenteLazyDataModel; 
    private EstadoActividadLazyList estadoActividadLazyDataModel; 
    private UsuarioLazyList usuarioLazyDataModel; 

    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaSeguimientoInterval; 
    private PrimeDateInterval fechaEjecucionInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{actividadFacade}")
    private ActividadFacadeAPI actividadFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new ActividadDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaSeguimientoInterval = new PrimeDateInterval("fechaSeguimiento");
        fechaEjecucionInterval = new PrimeDateInterval("fechaEjecucion");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ActividadLazyList();
        componenteLazyDataModel = new ComponenteLazyList();
        estadoActividadLazyDataModel = new EstadoActividadLazyList();
        usuarioLazyDataModel = new UsuarioLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaSeguimientoInterval);
        dateIntervalList.add(fechaEjecucionInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            actividadFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            actividadFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ActividadDTO actividadDTO) {
        setBaseDTO(actividadDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ActividadDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ActividadFacadeAPI getActividadFacade() {
        return actividadFacade;
    }

    public void setActividadFacade(ActividadFacadeAPI actividadFacade) {
        this.actividadFacade = actividadFacade;
    }

    public ActividadDTO getSelectedActividad() {
        return getBaseDTO();
    }
    
    public ActividadLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ActividadLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public ComponenteLazyList getComponenteLazyDataModel() {
        return componenteLazyDataModel;
    }

    public void setComponenteLazyDataModel(ComponenteLazyList componenteLazyDataModel) {
        this.componenteLazyDataModel = componenteLazyDataModel;
    }

	public EstadoActividadLazyList getEstadoActividadLazyDataModel() {
        return estadoActividadLazyDataModel;
    }

    public void setEstadoActividadLazyDataModel(EstadoActividadLazyList estadoActividadLazyDataModel) {
        this.estadoActividadLazyDataModel = estadoActividadLazyDataModel;
    }

	public UsuarioLazyList getUsuarioLazyDataModel() {
        return usuarioLazyDataModel;
    }

    public void setUsuarioLazyDataModel(UsuarioLazyList usuarioLazyDataModel) {
        this.usuarioLazyDataModel = usuarioLazyDataModel;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaSeguimientoInterval() {
        return fechaSeguimientoInterval;
    }

    public void setFechaSeguimientoInterval(PrimeDateInterval fechaSeguimientoInterval) {
        this.fechaSeguimientoInterval = fechaSeguimientoInterval;
    }

	public PrimeDateInterval getFechaEjecucionInterval() {
        return fechaEjecucionInterval;
    }

    public void setFechaEjecucionInterval(PrimeDateInterval fechaEjecucionInterval) {
        this.fechaEjecucionInterval = fechaEjecucionInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
