package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.IndicadorObjetivoLazyList;
import co.com.fspb.mgs.view.lazylist.ObjetivoLazyList;
import co.com.fspb.mgs.view.lazylist.IndicadorLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link IndicadorObjetivo}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class IndicadorObjetivoBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class IndicadorObjetivoBeanView extends AbstractManagedBeanBase<IndicadorObjetivoDTO> {

    private static final String FORM_PG_FORM = "formIndicadorObjetivo:pgForm";
    private static final String I18N_MSG = "msgIndicadorObjetivo";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-indicadorObjetivo";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(IndicadorObjetivoBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private IndicadorObjetivoLazyList lazyDataModel;

    private ObjetivoLazyList objetivoLazyDataModel; 
    private IndicadorLazyList indicadorLazyDataModel; 


    @ManagedProperty("#{indicadorObjetivoFacade}")
    private IndicadorObjetivoFacadeAPI indicadorObjetivoFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new IndicadorObjetivoDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new IndicadorObjetivoLazyList();
        objetivoLazyDataModel = new ObjetivoLazyList();
        indicadorLazyDataModel = new IndicadorLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            indicadorObjetivoFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            indicadorObjetivoFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(IndicadorObjetivoDTO indicadorObjetivoDTO) {
        setBaseDTO(indicadorObjetivoDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new IndicadorObjetivoDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public IndicadorObjetivoFacadeAPI getIndicadorObjetivoFacade() {
        return indicadorObjetivoFacade;
    }

    public void setIndicadorObjetivoFacade(IndicadorObjetivoFacadeAPI indicadorObjetivoFacade) {
        this.indicadorObjetivoFacade = indicadorObjetivoFacade;
    }

    public IndicadorObjetivoDTO getSelectedIndicadorObjetivo() {
        return getBaseDTO();
    }
    
    public IndicadorObjetivoLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(IndicadorObjetivoLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ObjetivoLazyList getObjetivoLazyDataModel() {
        return objetivoLazyDataModel;
    }

    public void setObjetivoLazyDataModel(ObjetivoLazyList objetivoLazyDataModel) {
        this.objetivoLazyDataModel = objetivoLazyDataModel;
    }

	public IndicadorLazyList getIndicadorLazyDataModel() {
        return indicadorLazyDataModel;
    }

    public void setIndicadorLazyDataModel(IndicadorLazyList indicadorLazyDataModel) {
        this.indicadorLazyDataModel = indicadorLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
