package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.TrazaActividadLazyList;
import co.com.fspb.mgs.view.lazylist.ActividadLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link TrazaActividad}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class TrazaActividadBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class TrazaActividadBeanView extends AbstractManagedBeanBase<TrazaActividadDTO> {

    private static final String FORM_PG_FORM = "formTrazaActividad:pgForm";
    private static final String I18N_MSG = "msgTrazaActividad";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-trazaActividad";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TrazaActividadBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private TrazaActividadLazyList lazyDataModel;

    private ActividadLazyList actividadLazyDataModel; 

    private PrimeDateInterval fechaInterval; 

    @ManagedProperty("#{trazaActividadFacade}")
    private TrazaActividadFacadeAPI trazaActividadFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new TrazaActividadDTO());
        fechaInterval = new PrimeDateInterval("fecha");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new TrazaActividadLazyList();
        actividadLazyDataModel = new ActividadLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            trazaActividadFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            trazaActividadFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(TrazaActividadDTO trazaActividadDTO) {
        setBaseDTO(trazaActividadDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new TrazaActividadDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public TrazaActividadFacadeAPI getTrazaActividadFacade() {
        return trazaActividadFacade;
    }

    public void setTrazaActividadFacade(TrazaActividadFacadeAPI trazaActividadFacade) {
        this.trazaActividadFacade = trazaActividadFacade;
    }

    public TrazaActividadDTO getSelectedTrazaActividad() {
        return getBaseDTO();
    }
    
    public TrazaActividadLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(TrazaActividadLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ActividadLazyList getActividadLazyDataModel() {
        return actividadLazyDataModel;
    }

    public void setActividadLazyDataModel(ActividadLazyList actividadLazyDataModel) {
        this.actividadLazyDataModel = actividadLazyDataModel;
    }

	public PrimeDateInterval getFechaInterval() {
        return fechaInterval;
    }

    public void setFechaInterval(PrimeDateInterval fechaInterval) {
        this.fechaInterval = fechaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
