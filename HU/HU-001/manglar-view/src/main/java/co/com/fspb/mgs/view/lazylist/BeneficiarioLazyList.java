package co.com.fspb.mgs.view.lazylist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.model.SortOrder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.premize.pmz.prime5.AbstractLazyDataModel;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI;
import com.premize.pmz.api.dto.PmzDateInterval;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.dto.PmzSearch;
import com.premize.pmz.api.dto.PmzSortDirection;
import com.premize.pmz.api.dto.PmzSortField;

/**
 * Lazy List para la entidad {@link Beneficiario}.
 * Extiende la interfaz de PMZ {@link AbstractLazyDataModel}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class BeneficiarioLazyList
 * @date nov 11, 2016
 */
public class BeneficiarioLazyList extends AbstractLazyDataModel<BeneficiarioDTO> {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = 1L;

    private transient BeneficiarioFacadeAPI beneficiarioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public BeneficiarioLazyList() {
        WebApplicationContext webApplicationContext = WebApplicationContextUtils
                .getRequiredWebApplicationContext((ServletContext) FacesContext
                        .getCurrentInstance().getExternalContext().getContext());
        beneficiarioFacade = webApplicationContext
                .getBean(BeneficiarioFacadeAPI.class);
    }

    /**
     * @see com.premize.pmz.prime5.AbstractLazyDataModel#getRowKey()
     */
    @Override
    public Object getRowKey(BeneficiarioDTO object) {
        return object != null ? object.getId() : null;
    }

    /**
     * @see com.premize.pmz.prime5.AbstractLazyDataModel#getRowData()
     */
    @Override
    public BeneficiarioDTO getRowData(String rowKey) {
        @SuppressWarnings("unchecked")
        List<BeneficiarioDTO> list = (List<BeneficiarioDTO>) getWrappedData();
        for (BeneficiarioDTO beneficiario : list) {
            if (beneficiario.getId().equals(rowKey)) {
                return beneficiario;
            }
        }
        return null;
    }

    /**
     * @see com.premize.pmz.prime5.AbstractLazyDataModel#findDataModelEntries()
     */
    @Override
    public Map<String, Object> findDataModelEntries(int first, int pageSize,
            String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        PmzPagingCriteria criteria = ManagedBeanUtils.getPaginCriteria(first,
                pageSize, sortField, sortOrder, filters, getGlobalSearch(),
                getDateIntervalList());
        PmzResultSet<BeneficiarioDTO> resultSet = beneficiarioFacade
                .getRecords(criteria);
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(KEY_ROWS, resultSet.getRows());
        data.put(KEY_TOTAL_RECORDS, resultSet.getTotalRecords());
        data.put(KEY_TOTAL_DISPLAY_RECORDS, resultSet.getTotalDisplayRecords());
        return data;
    }

    /**
     * @see com.premize.pmz.prime5.AbstractLazyDataModel#load()
     */
    @Override
    public List<BeneficiarioDTO> load(int first, int pageSize, String sortField,
            SortOrder sortOrder, Map<String, Object> filters) {
        List<PmzSearch> searchFields = ManagedBeanUtils.getSearchList(filters);
        List<PmzSortField> sortFields = new ArrayList<PmzSortField>();
        if (sortField != null) {
            String sortOrderString = PmzSortDirection.ASC.toString();
            if (SortOrder.DESCENDING.equals(sortOrder)) {
                sortOrderString = PmzSortDirection.DESC.toString();
            }
            sortFields.add(new PmzSortField(sortField, sortOrderString));
        }

        List<PrimeDateInterval> list = getDateIntervalList();
        List<PmzDateInterval> dates = new ArrayList<PmzDateInterval>();
        if (list != null && !list.isEmpty()) {
            for (PrimeDateInterval primeDateInterval : list) {
                dates.add(new PmzDateInterval(
                        primeDateInterval.getProperty(),
                        primeDateInterval.getMinDate() != null ? primeDateInterval
                                .getMinDate().getTime() : null,
                        primeDateInterval.getMaxDate() != null ? primeDateInterval
                                .getMaxDate().getTime() : null));
            }
        }

        PmzPagingCriteria criteria = new PmzPagingCriteria(null, searchFields, first,
                pageSize, null, sortFields, dates);
        PmzResultSet<BeneficiarioDTO> resultSet = beneficiarioFacade
                .getRecords(criteria);
        List<BeneficiarioDTO> records = resultSet.getRows();
        this.setRowCount(resultSet.getTotalRecords().intValue());
        return records;
    }

}
