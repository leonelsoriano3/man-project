package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.RolUsuarioLazyList;
import co.com.fspb.mgs.view.lazylist.UsuarioLazyList;
import co.com.fspb.mgs.view.lazylist.RolLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link RolUsuario}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class RolUsuarioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class RolUsuarioBeanView extends AbstractManagedBeanBase<RolUsuarioDTO> {

    private static final String FORM_PG_FORM = "formRolUsuario:pgForm";
    private static final String I18N_MSG = "msgRolUsuario";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-rolUsuario";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(RolUsuarioBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private RolUsuarioLazyList lazyDataModel;

    private UsuarioLazyList usuarioLazyDataModel; 
    private RolLazyList rolLazyDataModel; 


    @ManagedProperty("#{rolUsuarioFacade}")
    private RolUsuarioFacadeAPI rolUsuarioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new RolUsuarioDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new RolUsuarioLazyList();
        usuarioLazyDataModel = new UsuarioLazyList();
        rolLazyDataModel = new RolLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            rolUsuarioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            rolUsuarioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(RolUsuarioDTO rolUsuarioDTO) {
        setBaseDTO(rolUsuarioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new RolUsuarioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public RolUsuarioFacadeAPI getRolUsuarioFacade() {
        return rolUsuarioFacade;
    }

    public void setRolUsuarioFacade(RolUsuarioFacadeAPI rolUsuarioFacade) {
        this.rolUsuarioFacade = rolUsuarioFacade;
    }

    public RolUsuarioDTO getSelectedRolUsuario() {
        return getBaseDTO();
    }
    
    public RolUsuarioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(RolUsuarioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public UsuarioLazyList getUsuarioLazyDataModel() {
        return usuarioLazyDataModel;
    }

    public void setUsuarioLazyDataModel(UsuarioLazyList usuarioLazyDataModel) {
        this.usuarioLazyDataModel = usuarioLazyDataModel;
    }

	public RolLazyList getRolLazyDataModel() {
        return rolLazyDataModel;
    }

    public void setRolLazyDataModel(RolLazyList rolLazyDataModel) {
        this.rolLazyDataModel = rolLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
