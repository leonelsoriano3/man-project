package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ComponenteProyectoLazyList;
import co.com.fspb.mgs.view.lazylist.ComponenteLazyList;
import co.com.fspb.mgs.view.lazylist.ProyectoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link ComponenteProyecto}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ComponenteProyectoBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ComponenteProyectoBeanView extends AbstractManagedBeanBase<ComponenteProyectoDTO> {

    private static final String FORM_PG_FORM = "formComponenteProyecto:pgForm";
    private static final String I18N_MSG = "msgComponenteProyecto";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-componenteProyecto";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ComponenteProyectoBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ComponenteProyectoLazyList lazyDataModel;

    private ComponenteLazyList componenteLazyDataModel; 
    private ProyectoLazyList proyectoLazyDataModel; 


    @ManagedProperty("#{componenteProyectoFacade}")
    private ComponenteProyectoFacadeAPI componenteProyectoFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new ComponenteProyectoDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ComponenteProyectoLazyList();
        componenteLazyDataModel = new ComponenteLazyList();
        proyectoLazyDataModel = new ProyectoLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            componenteProyectoFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            componenteProyectoFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ComponenteProyectoDTO componenteProyectoDTO) {
        setBaseDTO(componenteProyectoDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ComponenteProyectoDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ComponenteProyectoFacadeAPI getComponenteProyectoFacade() {
        return componenteProyectoFacade;
    }

    public void setComponenteProyectoFacade(ComponenteProyectoFacadeAPI componenteProyectoFacade) {
        this.componenteProyectoFacade = componenteProyectoFacade;
    }

    public ComponenteProyectoDTO getSelectedComponenteProyecto() {
        return getBaseDTO();
    }
    
    public ComponenteProyectoLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ComponenteProyectoLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ComponenteLazyList getComponenteLazyDataModel() {
        return componenteLazyDataModel;
    }

    public void setComponenteLazyDataModel(ComponenteLazyList componenteLazyDataModel) {
        this.componenteLazyDataModel = componenteLazyDataModel;
    }

	public ProyectoLazyList getProyectoLazyDataModel() {
        return proyectoLazyDataModel;
    }

    public void setProyectoLazyDataModel(ProyectoLazyList proyectoLazyDataModel) {
        this.proyectoLazyDataModel = proyectoLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
