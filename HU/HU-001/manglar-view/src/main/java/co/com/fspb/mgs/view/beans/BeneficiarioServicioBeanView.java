package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.BeneficiarioServicioLazyList;
import co.com.fspb.mgs.view.lazylist.ServicioLazyList;
import co.com.fspb.mgs.view.lazylist.BeneficiarioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link BeneficiarioServicio}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class BeneficiarioServicioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class BeneficiarioServicioBeanView extends AbstractManagedBeanBase<BeneficiarioServicioDTO> {

    private static final String FORM_PG_FORM = "formBeneficiarioServicio:pgForm";
    private static final String I18N_MSG = "msgBeneficiarioServicio";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-beneficiarioServicio";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(BeneficiarioServicioBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private BeneficiarioServicioLazyList lazyDataModel;

    private ServicioLazyList servicioLazyDataModel; 
    private BeneficiarioLazyList beneficiarioLazyDataModel; 


    @ManagedProperty("#{beneficiarioServicioFacade}")
    private BeneficiarioServicioFacadeAPI beneficiarioServicioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new BeneficiarioServicioDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new BeneficiarioServicioLazyList();
        servicioLazyDataModel = new ServicioLazyList();
        beneficiarioLazyDataModel = new BeneficiarioLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            beneficiarioServicioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            beneficiarioServicioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(BeneficiarioServicioDTO beneficiarioServicioDTO) {
        setBaseDTO(beneficiarioServicioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new BeneficiarioServicioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public BeneficiarioServicioFacadeAPI getBeneficiarioServicioFacade() {
        return beneficiarioServicioFacade;
    }

    public void setBeneficiarioServicioFacade(BeneficiarioServicioFacadeAPI beneficiarioServicioFacade) {
        this.beneficiarioServicioFacade = beneficiarioServicioFacade;
    }

    public BeneficiarioServicioDTO getSelectedBeneficiarioServicio() {
        return getBaseDTO();
    }
    
    public BeneficiarioServicioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(BeneficiarioServicioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ServicioLazyList getServicioLazyDataModel() {
        return servicioLazyDataModel;
    }

    public void setServicioLazyDataModel(ServicioLazyList servicioLazyDataModel) {
        this.servicioLazyDataModel = servicioLazyDataModel;
    }

	public BeneficiarioLazyList getBeneficiarioLazyDataModel() {
        return beneficiarioLazyDataModel;
    }

    public void setBeneficiarioLazyDataModel(BeneficiarioLazyList beneficiarioLazyDataModel) {
        this.beneficiarioLazyDataModel = beneficiarioLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
