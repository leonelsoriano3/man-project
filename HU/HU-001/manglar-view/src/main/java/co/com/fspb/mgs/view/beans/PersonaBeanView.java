package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.PersonaLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.facade.api.PersonaFacadeAPI;

import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link Persona}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class PersonaBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class PersonaBeanView extends AbstractManagedBeanBase<PersonaDTO> {

    private static final String FORM_PG_FORM = "formPersona:pgForm";
    private static final String I18N_MSG = "msgPersona";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-persona";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PersonaBeanView.class);

    private List<SelectItem> tipoIdList; 
    private List<SelectItem> tipoIdFilter; 
    
    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private PersonaLazyList lazyDataModel;


    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{personaFacade}")
    private PersonaFacadeAPI personaFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        tipoIdList = ManagedBeanUtils
                .convertirSelectItem(TipoIdEnum.class);
        tipoIdFilter = ManagedBeanUtils.convertirSelectItem(
                TipoIdEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblTipoId"));
    
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new PersonaDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new PersonaLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            personaFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            personaFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(PersonaDTO personaDTO) {
        setBaseDTO(personaDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new PersonaDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public PersonaFacadeAPI getPersonaFacade() {
        return personaFacade;
    }

    public void setPersonaFacade(PersonaFacadeAPI personaFacade) {
        this.personaFacade = personaFacade;
    }

    public PersonaDTO getSelectedPersona() {
        return getBaseDTO();
    }
    
    public PersonaLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(PersonaLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getTipoIdList() {
        return tipoIdList;
    }

    public void setTipoIdList(List<SelectItem> tipoIdList) {
        this.tipoIdList = tipoIdList;
    }

	public List<SelectItem> getTipoIdFilter() {
        return tipoIdFilter;
    }

    public void setTipoIdFilter(List<SelectItem> tipoIdFilter) {
        this.tipoIdFilter = tipoIdFilter;
    }

	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
