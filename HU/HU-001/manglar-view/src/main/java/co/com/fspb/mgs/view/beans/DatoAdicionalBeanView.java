package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.DatoAdicionalLazyList;
import co.com.fspb.mgs.view.lazylist.ProyectoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link DatoAdicional}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class DatoAdicionalBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class DatoAdicionalBeanView extends AbstractManagedBeanBase<DatoAdicionalDTO> {

    private static final String FORM_PG_FORM = "formDatoAdicional:pgForm";
    private static final String I18N_MSG = "msgDatoAdicional";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-datoAdicional";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DatoAdicionalBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private DatoAdicionalLazyList lazyDataModel;

    private ProyectoLazyList proyectoLazyDataModel; 


    @ManagedProperty("#{datoAdicionalFacade}")
    private DatoAdicionalFacadeAPI datoAdicionalFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new DatoAdicionalDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new DatoAdicionalLazyList();
        proyectoLazyDataModel = new ProyectoLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            datoAdicionalFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            datoAdicionalFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(DatoAdicionalDTO datoAdicionalDTO) {
        setBaseDTO(datoAdicionalDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new DatoAdicionalDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public DatoAdicionalFacadeAPI getDatoAdicionalFacade() {
        return datoAdicionalFacade;
    }

    public void setDatoAdicionalFacade(DatoAdicionalFacadeAPI datoAdicionalFacade) {
        this.datoAdicionalFacade = datoAdicionalFacade;
    }

    public DatoAdicionalDTO getSelectedDatoAdicional() {
        return getBaseDTO();
    }
    
    public DatoAdicionalLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(DatoAdicionalLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ProyectoLazyList getProyectoLazyDataModel() {
        return proyectoLazyDataModel;
    }

    public void setProyectoLazyDataModel(ProyectoLazyList proyectoLazyDataModel) {
        this.proyectoLazyDataModel = proyectoLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
