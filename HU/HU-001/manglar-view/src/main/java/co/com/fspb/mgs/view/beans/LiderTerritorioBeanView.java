package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.LiderTerritorioLazyList;
import co.com.fspb.mgs.view.lazylist.LiderLazyList;
import co.com.fspb.mgs.view.lazylist.TerritorioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link LiderTerritorio}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class LiderTerritorioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class LiderTerritorioBeanView extends AbstractManagedBeanBase<LiderTerritorioDTO> {

    private static final String FORM_PG_FORM = "formLiderTerritorio:pgForm";
    private static final String I18N_MSG = "msgLiderTerritorio";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-liderTerritorio";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LiderTerritorioBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private LiderTerritorioLazyList lazyDataModel;

    private LiderLazyList liderLazyDataModel; 
    private TerritorioLazyList territorioLazyDataModel; 


    @ManagedProperty("#{liderTerritorioFacade}")
    private LiderTerritorioFacadeAPI liderTerritorioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new LiderTerritorioDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new LiderTerritorioLazyList();
        liderLazyDataModel = new LiderLazyList();
        territorioLazyDataModel = new TerritorioLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            liderTerritorioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            liderTerritorioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(LiderTerritorioDTO liderTerritorioDTO) {
        setBaseDTO(liderTerritorioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new LiderTerritorioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public LiderTerritorioFacadeAPI getLiderTerritorioFacade() {
        return liderTerritorioFacade;
    }

    public void setLiderTerritorioFacade(LiderTerritorioFacadeAPI liderTerritorioFacade) {
        this.liderTerritorioFacade = liderTerritorioFacade;
    }

    public LiderTerritorioDTO getSelectedLiderTerritorio() {
        return getBaseDTO();
    }
    
    public LiderTerritorioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(LiderTerritorioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public LiderLazyList getLiderLazyDataModel() {
        return liderLazyDataModel;
    }

    public void setLiderLazyDataModel(LiderLazyList liderLazyDataModel) {
        this.liderLazyDataModel = liderLazyDataModel;
    }

	public TerritorioLazyList getTerritorioLazyDataModel() {
        return territorioLazyDataModel;
    }

    public void setTerritorioLazyDataModel(TerritorioLazyList territorioLazyDataModel) {
        this.territorioLazyDataModel = territorioLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
