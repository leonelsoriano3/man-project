package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ProyectoBeneficiarioLazyList;
import co.com.fspb.mgs.view.lazylist.BeneficiarioLazyList;
import co.com.fspb.mgs.view.lazylist.ProyectoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link ProyectoBeneficiario}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ProyectoBeneficiarioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ProyectoBeneficiarioBeanView extends AbstractManagedBeanBase<ProyectoBeneficiarioDTO> {

    private static final String FORM_PG_FORM = "formProyectoBeneficiario:pgForm";
    private static final String I18N_MSG = "msgProyectoBeneficiario";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-proyectoBeneficiario";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ProyectoBeneficiarioBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ProyectoBeneficiarioLazyList lazyDataModel;

    private BeneficiarioLazyList beneficiarioLazyDataModel; 
    private ProyectoLazyList proyectoLazyDataModel; 


    @ManagedProperty("#{proyectoBeneficiarioFacade}")
    private ProyectoBeneficiarioFacadeAPI proyectoBeneficiarioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new ProyectoBeneficiarioDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ProyectoBeneficiarioLazyList();
        beneficiarioLazyDataModel = new BeneficiarioLazyList();
        proyectoLazyDataModel = new ProyectoLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            proyectoBeneficiarioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            proyectoBeneficiarioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ProyectoBeneficiarioDTO proyectoBeneficiarioDTO) {
        setBaseDTO(proyectoBeneficiarioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ProyectoBeneficiarioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ProyectoBeneficiarioFacadeAPI getProyectoBeneficiarioFacade() {
        return proyectoBeneficiarioFacade;
    }

    public void setProyectoBeneficiarioFacade(ProyectoBeneficiarioFacadeAPI proyectoBeneficiarioFacade) {
        this.proyectoBeneficiarioFacade = proyectoBeneficiarioFacade;
    }

    public ProyectoBeneficiarioDTO getSelectedProyectoBeneficiario() {
        return getBaseDTO();
    }
    
    public ProyectoBeneficiarioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ProyectoBeneficiarioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public BeneficiarioLazyList getBeneficiarioLazyDataModel() {
        return beneficiarioLazyDataModel;
    }

    public void setBeneficiarioLazyDataModel(BeneficiarioLazyList beneficiarioLazyDataModel) {
        this.beneficiarioLazyDataModel = beneficiarioLazyDataModel;
    }

	public ProyectoLazyList getProyectoLazyDataModel() {
        return proyectoLazyDataModel;
    }

    public void setProyectoLazyDataModel(ProyectoLazyList proyectoLazyDataModel) {
        this.proyectoLazyDataModel = proyectoLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
