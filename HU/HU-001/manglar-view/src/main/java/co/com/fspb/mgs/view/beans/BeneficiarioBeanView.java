package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.BeneficiarioLazyList;
import co.com.fspb.mgs.view.lazylist.AreaEducacionLazyList;
import co.com.fspb.mgs.view.lazylist.EstructuraFamiliarLazyList;
import co.com.fspb.mgs.view.lazylist.EstadoCivilLazyList;
import co.com.fspb.mgs.view.lazylist.DescIndependienteLazyList;
import co.com.fspb.mgs.view.lazylist.NivelEducativoLazyList;
import co.com.fspb.mgs.view.lazylist.TipoViviendaLazyList;
import co.com.fspb.mgs.view.lazylist.TipoTenenciaLazyList;
import co.com.fspb.mgs.view.lazylist.ZonaLazyList;
import co.com.fspb.mgs.view.lazylist.MaterialViviendaLazyList;
import co.com.fspb.mgs.view.lazylist.BdNacionalLazyList;
import co.com.fspb.mgs.view.lazylist.AfiliadoSaludLazyList;
import co.com.fspb.mgs.view.lazylist.PersonaLazyList;
import co.com.fspb.mgs.view.lazylist.GrupoEtnicoLazyList;
import co.com.fspb.mgs.view.lazylist.OcupacionLazyList;
import co.com.fspb.mgs.view.lazylist.MunicipioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI;

import co.com.fspb.mgs.enums.EstratoSocioeconomicoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link Beneficiario}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class BeneficiarioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class BeneficiarioBeanView extends AbstractManagedBeanBase<BeneficiarioDTO> {

    private static final String FORM_PG_FORM = "formBeneficiario:pgForm";
    private static final String I18N_MSG = "msgBeneficiario";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-beneficiario";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(BeneficiarioBeanView.class);

    private List<SelectItem> estratoSocioeconomicoList; 
    private List<SelectItem> estratoSocioeconomicoFilter; 
    
    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private BeneficiarioLazyList lazyDataModel;

    private AreaEducacionLazyList areaEducacionLazyDataModel; 
    private EstructuraFamiliarLazyList estructuraFamiliarLazyDataModel; 
    private EstadoCivilLazyList estadoCivilLazyDataModel; 
    private DescIndependienteLazyList descIndependienteLazyDataModel; 
    private NivelEducativoLazyList nivelEducativoLazyDataModel; 
    private TipoViviendaLazyList tipoViviendaLazyDataModel; 
    private TipoTenenciaLazyList tipoTenenciaLazyDataModel; 
    private ZonaLazyList zonaLazyDataModel; 
    private MaterialViviendaLazyList materialViviendaLazyDataModel; 
    private BdNacionalLazyList bdNacionalLazyDataModel; 
    private AfiliadoSaludLazyList afiliadoSaludLazyDataModel; 
    private PersonaLazyList personaLazyDataModel; 
    private GrupoEtnicoLazyList grupoEtnicoLazyDataModel; 
    private OcupacionLazyList ocupacionLazyDataModel; 
    private MunicipioLazyList municipioLazyDataModel; 

    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{beneficiarioFacade}")
    private BeneficiarioFacadeAPI beneficiarioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        estratoSocioeconomicoList = ManagedBeanUtils
                .convertirSelectItem(EstratoSocioeconomicoEnum.class);
        estratoSocioeconomicoFilter = ManagedBeanUtils.convertirSelectItem(
                EstratoSocioeconomicoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstratoSocioeconomico"));
    
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new BeneficiarioDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new BeneficiarioLazyList();
        areaEducacionLazyDataModel = new AreaEducacionLazyList();
        estructuraFamiliarLazyDataModel = new EstructuraFamiliarLazyList();
        estadoCivilLazyDataModel = new EstadoCivilLazyList();
        descIndependienteLazyDataModel = new DescIndependienteLazyList();
        nivelEducativoLazyDataModel = new NivelEducativoLazyList();
        tipoViviendaLazyDataModel = new TipoViviendaLazyList();
        tipoTenenciaLazyDataModel = new TipoTenenciaLazyList();
        zonaLazyDataModel = new ZonaLazyList();
        materialViviendaLazyDataModel = new MaterialViviendaLazyList();
        bdNacionalLazyDataModel = new BdNacionalLazyList();
        afiliadoSaludLazyDataModel = new AfiliadoSaludLazyList();
        personaLazyDataModel = new PersonaLazyList();
        grupoEtnicoLazyDataModel = new GrupoEtnicoLazyList();
        ocupacionLazyDataModel = new OcupacionLazyList();
        municipioLazyDataModel = new MunicipioLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            beneficiarioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            beneficiarioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(BeneficiarioDTO beneficiarioDTO) {
        setBaseDTO(beneficiarioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new BeneficiarioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public BeneficiarioFacadeAPI getBeneficiarioFacade() {
        return beneficiarioFacade;
    }

    public void setBeneficiarioFacade(BeneficiarioFacadeAPI beneficiarioFacade) {
        this.beneficiarioFacade = beneficiarioFacade;
    }

    public BeneficiarioDTO getSelectedBeneficiario() {
        return getBaseDTO();
    }
    
    public BeneficiarioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(BeneficiarioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getEstratoSocioeconomicoList() {
        return estratoSocioeconomicoList;
    }

    public void setEstratoSocioeconomicoList(List<SelectItem> estratoSocioeconomicoList) {
        this.estratoSocioeconomicoList = estratoSocioeconomicoList;
    }

	public List<SelectItem> getEstratoSocioeconomicoFilter() {
        return estratoSocioeconomicoFilter;
    }

    public void setEstratoSocioeconomicoFilter(List<SelectItem> estratoSocioeconomicoFilter) {
        this.estratoSocioeconomicoFilter = estratoSocioeconomicoFilter;
    }

	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public AreaEducacionLazyList getAreaEducacionLazyDataModel() {
        return areaEducacionLazyDataModel;
    }

    public void setAreaEducacionLazyDataModel(AreaEducacionLazyList areaEducacionLazyDataModel) {
        this.areaEducacionLazyDataModel = areaEducacionLazyDataModel;
    }

	public EstructuraFamiliarLazyList getEstructuraFamiliarLazyDataModel() {
        return estructuraFamiliarLazyDataModel;
    }

    public void setEstructuraFamiliarLazyDataModel(EstructuraFamiliarLazyList estructuraFamiliarLazyDataModel) {
        this.estructuraFamiliarLazyDataModel = estructuraFamiliarLazyDataModel;
    }

	public EstadoCivilLazyList getEstadoCivilLazyDataModel() {
        return estadoCivilLazyDataModel;
    }

    public void setEstadoCivilLazyDataModel(EstadoCivilLazyList estadoCivilLazyDataModel) {
        this.estadoCivilLazyDataModel = estadoCivilLazyDataModel;
    }

	public DescIndependienteLazyList getDescIndependienteLazyDataModel() {
        return descIndependienteLazyDataModel;
    }

    public void setDescIndependienteLazyDataModel(DescIndependienteLazyList descIndependienteLazyDataModel) {
        this.descIndependienteLazyDataModel = descIndependienteLazyDataModel;
    }

	public NivelEducativoLazyList getNivelEducativoLazyDataModel() {
        return nivelEducativoLazyDataModel;
    }

    public void setNivelEducativoLazyDataModel(NivelEducativoLazyList nivelEducativoLazyDataModel) {
        this.nivelEducativoLazyDataModel = nivelEducativoLazyDataModel;
    }

	public TipoViviendaLazyList getTipoViviendaLazyDataModel() {
        return tipoViviendaLazyDataModel;
    }

    public void setTipoViviendaLazyDataModel(TipoViviendaLazyList tipoViviendaLazyDataModel) {
        this.tipoViviendaLazyDataModel = tipoViviendaLazyDataModel;
    }

	public TipoTenenciaLazyList getTipoTenenciaLazyDataModel() {
        return tipoTenenciaLazyDataModel;
    }

    public void setTipoTenenciaLazyDataModel(TipoTenenciaLazyList tipoTenenciaLazyDataModel) {
        this.tipoTenenciaLazyDataModel = tipoTenenciaLazyDataModel;
    }

	public ZonaLazyList getZonaLazyDataModel() {
        return zonaLazyDataModel;
    }

    public void setZonaLazyDataModel(ZonaLazyList zonaLazyDataModel) {
        this.zonaLazyDataModel = zonaLazyDataModel;
    }

	public MaterialViviendaLazyList getMaterialViviendaLazyDataModel() {
        return materialViviendaLazyDataModel;
    }

    public void setMaterialViviendaLazyDataModel(MaterialViviendaLazyList materialViviendaLazyDataModel) {
        this.materialViviendaLazyDataModel = materialViviendaLazyDataModel;
    }

	public BdNacionalLazyList getBdNacionalLazyDataModel() {
        return bdNacionalLazyDataModel;
    }

    public void setBdNacionalLazyDataModel(BdNacionalLazyList bdNacionalLazyDataModel) {
        this.bdNacionalLazyDataModel = bdNacionalLazyDataModel;
    }

	public AfiliadoSaludLazyList getAfiliadoSaludLazyDataModel() {
        return afiliadoSaludLazyDataModel;
    }

    public void setAfiliadoSaludLazyDataModel(AfiliadoSaludLazyList afiliadoSaludLazyDataModel) {
        this.afiliadoSaludLazyDataModel = afiliadoSaludLazyDataModel;
    }

	public PersonaLazyList getPersonaLazyDataModel() {
        return personaLazyDataModel;
    }

    public void setPersonaLazyDataModel(PersonaLazyList personaLazyDataModel) {
        this.personaLazyDataModel = personaLazyDataModel;
    }

	public GrupoEtnicoLazyList getGrupoEtnicoLazyDataModel() {
        return grupoEtnicoLazyDataModel;
    }

    public void setGrupoEtnicoLazyDataModel(GrupoEtnicoLazyList grupoEtnicoLazyDataModel) {
        this.grupoEtnicoLazyDataModel = grupoEtnicoLazyDataModel;
    }

	public OcupacionLazyList getOcupacionLazyDataModel() {
        return ocupacionLazyDataModel;
    }

    public void setOcupacionLazyDataModel(OcupacionLazyList ocupacionLazyDataModel) {
        this.ocupacionLazyDataModel = ocupacionLazyDataModel;
    }

	public MunicipioLazyList getMunicipioLazyDataModel() {
        return municipioLazyDataModel;
    }

    public void setMunicipioLazyDataModel(MunicipioLazyList municipioLazyDataModel) {
        this.municipioLazyDataModel = municipioLazyDataModel;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
