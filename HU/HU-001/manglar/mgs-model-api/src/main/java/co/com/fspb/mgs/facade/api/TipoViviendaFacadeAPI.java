package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link TipoVivienda}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TipoViviendaFacadeAPI
 * @date nov 11, 2016
 */
public interface TipoViviendaFacadeAPI {

    /**
	 * Registra una entidad {@link TipoVivienda} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoVivienda - {@link TipoViviendaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(TipoViviendaDTO tipoVivienda) throws PmzException;

	/**
	 * Actualiza una entidad {@link TipoVivienda} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoVivienda - {@link TipoViviendaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(TipoViviendaDTO tipoVivienda) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TipoViviendaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link TipoViviendaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TipoViviendaDTO}
     */
    TipoViviendaDTO getTipoViviendaDTO(Long id);
}

