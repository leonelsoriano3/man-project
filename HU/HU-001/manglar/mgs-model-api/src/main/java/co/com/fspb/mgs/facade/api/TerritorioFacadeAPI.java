package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TerritorioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Territorio}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TerritorioFacadeAPI
 * @date nov 11, 2016
 */
public interface TerritorioFacadeAPI {

    /**
	 * Registra una entidad {@link Territorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link TerritorioDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(TerritorioDTO territorio) throws PmzException;

	/**
	 * Actualiza una entidad {@link Territorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link TerritorioDTO}
	 * @throws {@link PmzException}
	 */
    void editar(TerritorioDTO territorio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TerritorioDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link TerritorioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TerritorioDTO}
     */
    TerritorioDTO getTerritorioDTO(Long id);
}

