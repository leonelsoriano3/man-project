package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link IndicadorObjetivo}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class IndicadorObjetivoFacadeAPI
 * @date nov 11, 2016
 */
public interface IndicadorObjetivoFacadeAPI {

    /**
	 * Registra una entidad {@link IndicadorObjetivo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorObjetivo - {@link IndicadorObjetivoDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(IndicadorObjetivoDTO indicadorObjetivo) throws PmzException;

	/**
	 * Actualiza una entidad {@link IndicadorObjetivo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorObjetivo - {@link IndicadorObjetivoDTO}
	 * @throws {@link PmzException}
	 */
    void editar(IndicadorObjetivoDTO indicadorObjetivo) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<IndicadorObjetivoDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link IndicadorObjetivoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link IndicadorDTO}
	 * @param objetivo - {@link ObjetivoDTO}
     * @return {@link IndicadorObjetivoDTO}
     */
    IndicadorObjetivoDTO getIndicadorObjetivoDTO(IndicadorDTO indicador, ObjetivoDTO objetivo);
}

