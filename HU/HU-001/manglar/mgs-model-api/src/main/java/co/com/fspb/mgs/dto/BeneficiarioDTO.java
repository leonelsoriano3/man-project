package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Beneficiario}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class BeneficiarioDTO
 * @date nov 11, 2016
 */
public class BeneficiarioDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private Date fechaModifica; 

    private AreaEducacionDTO areaEducacionDTO; 

    private EstructuraFamiliarDTO estructuraFamiliarDTO; 

    @NotNull
    private String estratoSocioeconomico; 
    
    @NotNull
    private String estratoSocioeconomicoDesc; 

    private EstadoCivilDTO estadoCivilDTO; 

    private DescIndependienteDTO descIndependienteDTO; 

    private NivelEducativoDTO nivelEducativoDTO; 

    private String otraAreaEducacion; 

    private TipoViviendaDTO tipoViviendaDTO; 

    @NotNull
    private Integer cantPersonasHogar; 

    @NotNull
    private String organizacionComunitaria; 

    private Integer nivelIngresos; 

    @NotNull
    private Integer cantPersonasCargo; 

    private String usuarioCrea; 

    private TipoTenenciaDTO tipoTenenciaDTO; 

    private ZonaDTO zonaDTO; 

    private MaterialViviendaDTO materialViviendaDTO; 

    private BdNacionalDTO bdNacionalDTO; 

    private AfiliadoSaludDTO afiliadoSaludDTO; 

    @NotNull
    private PersonaDTO personaDTO; 

    private GrupoEtnicoDTO grupoEtnicoDTO; 

    private OcupacionDTO ocupacionDTO; 

    private String genero; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String usuarioModifica; 

    private Date fechaCrea; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private MunicipioDTO municipioDTO; 

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public AreaEducacionDTO getAreaEducacionDTO() {
        return areaEducacionDTO;
    }

    public void setAreaEducacionDTO(AreaEducacionDTO areaEducacionDTO) {
        this.areaEducacionDTO = areaEducacionDTO;
    }

    public EstructuraFamiliarDTO getEstructuraFamiliarDTO() {
        return estructuraFamiliarDTO;
    }

    public void setEstructuraFamiliarDTO(EstructuraFamiliarDTO estructuraFamiliarDTO) {
        this.estructuraFamiliarDTO = estructuraFamiliarDTO;
    }

    public String getEstratoSocioeconomico() {
        return estratoSocioeconomico;
    }

    public void setEstratoSocioeconomico(String estratoSocioeconomico) {
        this.estratoSocioeconomico = estratoSocioeconomico;
    }

    public String getEstratoSocioeconomicoDesc() {
        return estratoSocioeconomicoDesc;
    }

    public void setEstratoSocioeconomicoDesc(String estratoSocioeconomicoDesc) {
        this.estratoSocioeconomicoDesc = estratoSocioeconomicoDesc;
    }

    public EstadoCivilDTO getEstadoCivilDTO() {
        return estadoCivilDTO;
    }

    public void setEstadoCivilDTO(EstadoCivilDTO estadoCivilDTO) {
        this.estadoCivilDTO = estadoCivilDTO;
    }

    public DescIndependienteDTO getDescIndependienteDTO() {
        return descIndependienteDTO;
    }

    public void setDescIndependienteDTO(DescIndependienteDTO descIndependienteDTO) {
        this.descIndependienteDTO = descIndependienteDTO;
    }

    public NivelEducativoDTO getNivelEducativoDTO() {
        return nivelEducativoDTO;
    }

    public void setNivelEducativoDTO(NivelEducativoDTO nivelEducativoDTO) {
        this.nivelEducativoDTO = nivelEducativoDTO;
    }

    public String getOtraAreaEducacion() {
        return otraAreaEducacion;
    }

    public void setOtraAreaEducacion(String otraAreaEducacion) {
        this.otraAreaEducacion = otraAreaEducacion;
    }

    public TipoViviendaDTO getTipoViviendaDTO() {
        return tipoViviendaDTO;
    }

    public void setTipoViviendaDTO(TipoViviendaDTO tipoViviendaDTO) {
        this.tipoViviendaDTO = tipoViviendaDTO;
    }

    public Integer getCantPersonasHogar() {
        return cantPersonasHogar;
    }

    public void setCantPersonasHogar(Integer cantPersonasHogar) {
        this.cantPersonasHogar = cantPersonasHogar;
    }

    public String getOrganizacionComunitaria() {
        return organizacionComunitaria;
    }

    public void setOrganizacionComunitaria(String organizacionComunitaria) {
        this.organizacionComunitaria = organizacionComunitaria;
    }

    public Integer getNivelIngresos() {
        return nivelIngresos;
    }

    public void setNivelIngresos(Integer nivelIngresos) {
        this.nivelIngresos = nivelIngresos;
    }

    public Integer getCantPersonasCargo() {
        return cantPersonasCargo;
    }

    public void setCantPersonasCargo(Integer cantPersonasCargo) {
        this.cantPersonasCargo = cantPersonasCargo;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public TipoTenenciaDTO getTipoTenenciaDTO() {
        return tipoTenenciaDTO;
    }

    public void setTipoTenenciaDTO(TipoTenenciaDTO tipoTenenciaDTO) {
        this.tipoTenenciaDTO = tipoTenenciaDTO;
    }

    public ZonaDTO getZonaDTO() {
        return zonaDTO;
    }

    public void setZonaDTO(ZonaDTO zonaDTO) {
        this.zonaDTO = zonaDTO;
    }

    public MaterialViviendaDTO getMaterialViviendaDTO() {
        return materialViviendaDTO;
    }

    public void setMaterialViviendaDTO(MaterialViviendaDTO materialViviendaDTO) {
        this.materialViviendaDTO = materialViviendaDTO;
    }

    public BdNacionalDTO getBdNacionalDTO() {
        return bdNacionalDTO;
    }

    public void setBdNacionalDTO(BdNacionalDTO bdNacionalDTO) {
        this.bdNacionalDTO = bdNacionalDTO;
    }

    public AfiliadoSaludDTO getAfiliadoSaludDTO() {
        return afiliadoSaludDTO;
    }

    public void setAfiliadoSaludDTO(AfiliadoSaludDTO afiliadoSaludDTO) {
        this.afiliadoSaludDTO = afiliadoSaludDTO;
    }

    public PersonaDTO getPersonaDTO() {
        return personaDTO;
    }

    public void setPersonaDTO(PersonaDTO personaDTO) {
        this.personaDTO = personaDTO;
    }

    public GrupoEtnicoDTO getGrupoEtnicoDTO() {
        return grupoEtnicoDTO;
    }

    public void setGrupoEtnicoDTO(GrupoEtnicoDTO grupoEtnicoDTO) {
        this.grupoEtnicoDTO = grupoEtnicoDTO;
    }

    public OcupacionDTO getOcupacionDTO() {
        return ocupacionDTO;
    }

    public void setOcupacionDTO(OcupacionDTO ocupacionDTO) {
        this.ocupacionDTO = ocupacionDTO;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MunicipioDTO getMunicipioDTO() {
        return municipioDTO;
    }

    public void setMunicipioDTO(MunicipioDTO municipioDTO) {
        this.municipioDTO = municipioDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "BeneficiarioDTO ["
                + " fechaModifica=" + fechaModifica
                + ", areaEducacion=" + areaEducacionDTO
                + ", estructuraFamiliar=" + estructuraFamiliarDTO
                + ", estratoSocioeconomico=" + estratoSocioeconomico
                + ", estadoCivil=" + estadoCivilDTO
                + ", descIndependiente=" + descIndependienteDTO
                + ", nivelEducativo=" + nivelEducativoDTO
                + ", otraAreaEducacion=" + otraAreaEducacion
                + ", tipoVivienda=" + tipoViviendaDTO
                + ", cantPersonasHogar=" + cantPersonasHogar
                + ", organizacionComunitaria=" + organizacionComunitaria
                + ", nivelIngresos=" + nivelIngresos
                + ", cantPersonasCargo=" + cantPersonasCargo
                + ", usuarioCrea=" + usuarioCrea
                + ", tipoTenencia=" + tipoTenenciaDTO
                + ", zona=" + zonaDTO
                + ", materialVivienda=" + materialViviendaDTO
                + ", bdNacional=" + bdNacionalDTO
                + ", afiliadoSalud=" + afiliadoSaludDTO
                + ", persona=" + personaDTO
                + ", grupoEtnico=" + grupoEtnicoDTO
                + ", ocupacion=" + ocupacionDTO
                + ", genero=" + genero
                + ", estado=" + estado
                + ", usuarioModifica=" + usuarioModifica
                + ", fechaCrea=" + fechaCrea
                + ", id=" + id
                + ", municipio=" + municipioDTO
                + " ]";
    }
    
}
