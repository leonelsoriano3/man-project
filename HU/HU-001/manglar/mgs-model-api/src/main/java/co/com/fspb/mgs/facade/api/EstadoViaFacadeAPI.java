package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoViaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link EstadoVia}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstadoViaFacadeAPI
 * @date nov 11, 2016
 */
public interface EstadoViaFacadeAPI {

    /**
	 * Registra una entidad {@link EstadoVia} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoVia - {@link EstadoViaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(EstadoViaDTO estadoVia) throws PmzException;

	/**
	 * Actualiza una entidad {@link EstadoVia} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoVia - {@link EstadoViaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(EstadoViaDTO estadoVia) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EstadoViaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link EstadoViaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link EstadoViaDTO}
     */
    EstadoViaDTO getEstadoViaDTO(Long id);
}

