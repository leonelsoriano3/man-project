package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link IndicadorProyecto}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class IndicadorProyectoDTO
 * @date nov 11, 2016
 */
public class IndicadorProyectoDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private IndicadorDTO indicadorDTO; 

    @JsonSerialize
    @NotNull
    private ProyectoDTO proyectoDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(indicadorDTO);
        idBuilder.append("-");
        idBuilder.append(proyectoDTO);
        return idBuilder.toString();
    }

    public IndicadorDTO getIndicadorDTO() {
        return indicadorDTO;
    }

    public void setIndicadorDTO(IndicadorDTO indicadorDTO) {
        this.indicadorDTO = indicadorDTO;
    }

    public ProyectoDTO getProyectoDTO() {
        return proyectoDTO;
    }

    public void setProyectoDTO(ProyectoDTO proyectoDTO) {
        this.proyectoDTO = proyectoDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "IndicadorProyectoDTO ["
                + " indicador=" + indicadorDTO
                + ", proyecto=" + proyectoDTO
                + " ]";
    }
    
}
