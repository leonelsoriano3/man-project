package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link EstadoCivil}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstadoCivilFacadeAPI
 * @date nov 11, 2016
 */
public interface EstadoCivilFacadeAPI {

    /**
	 * Registra una entidad {@link EstadoCivil} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoCivil - {@link EstadoCivilDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(EstadoCivilDTO estadoCivil) throws PmzException;

	/**
	 * Actualiza una entidad {@link EstadoCivil} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoCivil - {@link EstadoCivilDTO}
	 * @throws {@link PmzException}
	 */
    void editar(EstadoCivilDTO estadoCivil) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EstadoCivilDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link EstadoCivilDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link EstadoCivilDTO}
     */
    EstadoCivilDTO getEstadoCivilDTO(Long id);
}

