package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Actividad}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ActividadDTO
 * @date nov 11, 2016
 */
public class ActividadDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private ComponenteDTO componenteDTO; 

    private Date fechaModifica; 

    private String resultadoObtenido; 

    private EstadoActividadDTO estadoActividadDTO; 

    private String observacionSeg; 

    @NotNull
    private String resultadoEsperado; 

    @NotNull
    private Date fechaSeguimiento; 

    @NotNull
    private String nombre; 

    @NotNull
    private String recursos; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String usuarioModifica; 

    private Integer cantAsistentes; 

    private String usuarioCrea; 

    @NotNull
    private UsuarioDTO usuarioDTO; 

    @NotNull
    private Date fechaEjecucion; 

    private Date fechaCrea; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private String leccionesAprendidas; 

    public ComponenteDTO getComponenteDTO() {
        return componenteDTO;
    }

    public void setComponenteDTO(ComponenteDTO componenteDTO) {
        this.componenteDTO = componenteDTO;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getResultadoObtenido() {
        return resultadoObtenido;
    }

    public void setResultadoObtenido(String resultadoObtenido) {
        this.resultadoObtenido = resultadoObtenido;
    }

    public EstadoActividadDTO getEstadoActividadDTO() {
        return estadoActividadDTO;
    }

    public void setEstadoActividadDTO(EstadoActividadDTO estadoActividadDTO) {
        this.estadoActividadDTO = estadoActividadDTO;
    }

    public String getObservacionSeg() {
        return observacionSeg;
    }

    public void setObservacionSeg(String observacionSeg) {
        this.observacionSeg = observacionSeg;
    }

    public String getResultadoEsperado() {
        return resultadoEsperado;
    }

    public void setResultadoEsperado(String resultadoEsperado) {
        this.resultadoEsperado = resultadoEsperado;
    }

    public Date getFechaSeguimiento() {
        return fechaSeguimiento != null ? new Date(fechaSeguimiento.getTime()) : null;
    }

    public void setFechaSeguimiento(Date fechaSeguimiento) {
        this.fechaSeguimiento = fechaSeguimiento != null ? new Date(fechaSeguimiento.getTime()) : null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Integer getCantAsistentes() {
        return cantAsistentes;
    }

    public void setCantAsistentes(Integer cantAsistentes) {
        this.cantAsistentes = cantAsistentes;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    public Date getFechaEjecucion() {
        return fechaEjecucion != null ? new Date(fechaEjecucion.getTime()) : null;
    }

    public void setFechaEjecucion(Date fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion != null ? new Date(fechaEjecucion.getTime()) : null;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLeccionesAprendidas() {
        return leccionesAprendidas;
    }

    public void setLeccionesAprendidas(String leccionesAprendidas) {
        this.leccionesAprendidas = leccionesAprendidas;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombre;
    }

	@Override
    public String toString() {
        return "ActividadDTO ["
                + " componente=" + componenteDTO
                + ", fechaModifica=" + fechaModifica
                + ", resultadoObtenido=" + resultadoObtenido
                + ", estadoActividad=" + estadoActividadDTO
                + ", observacionSeg=" + observacionSeg
                + ", resultadoEsperado=" + resultadoEsperado
                + ", fechaSeguimiento=" + fechaSeguimiento
                + ", nombre=" + nombre
                + ", recursos=" + recursos
                + ", estado=" + estado
                + ", usuarioModifica=" + usuarioModifica
                + ", cantAsistentes=" + cantAsistentes
                + ", usuarioCrea=" + usuarioCrea
                + ", usuario=" + usuarioDTO
                + ", fechaEjecucion=" + fechaEjecucion
                + ", fechaCrea=" + fechaCrea
                + ", id=" + id
                + ", leccionesAprendidas=" + leccionesAprendidas
                + " ]";
    }
    
}
