package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link DatoContacto}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class DatoContactoFacadeAPI
 * @date nov 11, 2016
 */
public interface DatoContactoFacadeAPI {

    /**
	 * Registra una entidad {@link DatoContacto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoContacto - {@link DatoContactoDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(DatoContactoDTO datoContacto) throws PmzException;

	/**
	 * Actualiza una entidad {@link DatoContacto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoContacto - {@link DatoContactoDTO}
	 * @throws {@link PmzException}
	 */
    void editar(DatoContactoDTO datoContacto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<DatoContactoDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link DatoContactoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link DatoContactoDTO}
     */
    DatoContactoDTO getDatoContactoDTO(Long id);
}

