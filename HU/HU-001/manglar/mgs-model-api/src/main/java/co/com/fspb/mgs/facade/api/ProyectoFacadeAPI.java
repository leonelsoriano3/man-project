package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Proyecto}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ProyectoFacadeAPI
 * @date nov 11, 2016
 */
public interface ProyectoFacadeAPI {

    /**
	 * Registra una entidad {@link Proyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyecto - {@link ProyectoDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ProyectoDTO proyecto) throws PmzException;

	/**
	 * Actualiza una entidad {@link Proyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyecto - {@link ProyectoDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ProyectoDTO proyecto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ProyectoDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ProyectoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ProyectoDTO}
     */
    ProyectoDTO getProyectoDTO(Long id);
}

