package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.ServicioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link BeneficiarioServicio}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class BeneficiarioServicioFacadeAPI
 * @date nov 11, 2016
 */
public interface BeneficiarioServicioFacadeAPI {

    /**
	 * Registra una entidad {@link BeneficiarioServicio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiarioServicio - {@link BeneficiarioServicioDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(BeneficiarioServicioDTO beneficiarioServicio) throws PmzException;

	/**
	 * Actualiza una entidad {@link BeneficiarioServicio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiarioServicio - {@link BeneficiarioServicioDTO}
	 * @throws {@link PmzException}
	 */
    void editar(BeneficiarioServicioDTO beneficiarioServicio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<BeneficiarioServicioDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link BeneficiarioServicioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link BeneficiarioDTO}
	 * @param servicio - {@link ServicioDTO}
     * @return {@link BeneficiarioServicioDTO}
     */
    BeneficiarioServicioDTO getBeneficiarioServicioDTO(BeneficiarioDTO beneficiario, ServicioDTO servicio);
}

