package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ZonaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Zona}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ZonaFacadeAPI
 * @date nov 11, 2016
 */
public interface ZonaFacadeAPI {

    /**
	 * Registra una entidad {@link Zona} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param zona - {@link ZonaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ZonaDTO zona) throws PmzException;

	/**
	 * Actualiza una entidad {@link Zona} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param zona - {@link ZonaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ZonaDTO zona) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ZonaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ZonaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ZonaDTO}
     */
    ZonaDTO getZonaDTO(Long id);
}

