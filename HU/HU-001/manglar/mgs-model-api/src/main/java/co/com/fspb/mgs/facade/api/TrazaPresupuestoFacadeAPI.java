package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link TrazaPresupuesto}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TrazaPresupuestoFacadeAPI
 * @date nov 11, 2016
 */
public interface TrazaPresupuestoFacadeAPI {

    /**
	 * Registra una entidad {@link TrazaPresupuesto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaPresupuesto - {@link TrazaPresupuestoDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(TrazaPresupuestoDTO trazaPresupuesto) throws PmzException;

	/**
	 * Actualiza una entidad {@link TrazaPresupuesto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaPresupuesto - {@link TrazaPresupuestoDTO}
	 * @throws {@link PmzException}
	 */
    void editar(TrazaPresupuestoDTO trazaPresupuesto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TrazaPresupuestoDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link TrazaPresupuestoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TrazaPresupuestoDTO}
     */
    TrazaPresupuestoDTO getTrazaPresupuestoDTO(Long id);
}

