package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link TipoTenencia}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TipoTenenciaFacadeAPI
 * @date nov 11, 2016
 */
public interface TipoTenenciaFacadeAPI {

    /**
	 * Registra una entidad {@link TipoTenencia} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTenencia - {@link TipoTenenciaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(TipoTenenciaDTO tipoTenencia) throws PmzException;

	/**
	 * Actualiza una entidad {@link TipoTenencia} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTenencia - {@link TipoTenenciaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(TipoTenenciaDTO tipoTenencia) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TipoTenenciaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link TipoTenenciaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TipoTenenciaDTO}
     */
    TipoTenenciaDTO getTipoTenenciaDTO(Long id);
}

