package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link TrazaActividad}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TrazaActividadDTO
 * @date nov 11, 2016
 */
public class TrazaActividadDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private Date fecha; 

    @NotNull
    private String usuario; 

    @NotNull
    private String estadoActividad; 

    @NotNull
    private ActividadDTO actividadDTO; 

    @JsonSerialize
    @NotNull
    private Long id; 

    public Date getFecha() {
        return fecha != null ? new Date(fecha.getTime()) : null;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha != null ? new Date(fecha.getTime()) : null;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstadoActividad() {
        return estadoActividad;
    }

    public void setEstadoActividad(String estadoActividad) {
        this.estadoActividad = estadoActividad;
    }

    public ActividadDTO getActividadDTO() {
        return actividadDTO;
    }

    public void setActividadDTO(ActividadDTO actividadDTO) {
        this.actividadDTO = actividadDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "TrazaActividadDTO ["
                + " fecha=" + fecha
                + ", usuario=" + usuario
                + ", estadoActividad=" + estadoActividad
                + ", actividad=" + actividadDTO
                + ", id=" + id
                + " ]";
    }
    
}
