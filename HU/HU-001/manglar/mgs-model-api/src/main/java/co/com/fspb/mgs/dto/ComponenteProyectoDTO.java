package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link ComponenteProyecto}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ComponenteProyectoDTO
 * @date nov 11, 2016
 */
public class ComponenteProyectoDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private ComponenteDTO componenteDTO; 

    @JsonSerialize
    @NotNull
    private ProyectoDTO proyectoDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(proyectoDTO);
        idBuilder.append("-");
        idBuilder.append(componenteDTO);
        return idBuilder.toString();
    }

    public ComponenteDTO getComponenteDTO() {
        return componenteDTO;
    }

    public void setComponenteDTO(ComponenteDTO componenteDTO) {
        this.componenteDTO = componenteDTO;
    }

    public ProyectoDTO getProyectoDTO() {
        return proyectoDTO;
    }

    public void setProyectoDTO(ProyectoDTO proyectoDTO) {
        this.proyectoDTO = proyectoDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "ComponenteProyectoDTO ["
                + " componente=" + componenteDTO
                + ", proyecto=" + proyectoDTO
                + " ]";
    }
    
}
