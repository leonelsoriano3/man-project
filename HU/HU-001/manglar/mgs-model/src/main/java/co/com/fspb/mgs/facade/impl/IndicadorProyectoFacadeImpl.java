package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI;
import co.com.fspb.mgs.service.api.IndicadorProyectoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link IndicadorProyectoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoFacadeImpl
 * @date nov 11, 2016
 */
@Service("indicadorProyectoFacade")
public class IndicadorProyectoFacadeImpl implements IndicadorProyectoFacadeAPI {

    @Autowired
    private IndicadorProyectoServiceAPI indicadorProyectoService;

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        return indicadorProyectoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI#guardar()
     */
    @Override
    public void guardar(IndicadorProyectoDTO indicadorProyecto) throws PmzException {
        indicadorProyectoService.guardar(indicadorProyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI#editar()
     */
    @Override
    public void editar(IndicadorProyectoDTO indicadorProyecto) throws PmzException {
        indicadorProyectoService.editar(indicadorProyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI#getIndicadorProyectoDTO()
     */
    @Override
    public IndicadorProyectoDTO getIndicadorProyectoDTO(IndicadorDTO indicador, ProyectoDTO proyecto) {
        return indicadorProyectoService.getIndicadorProyectoDTO(indicador, proyecto);
    }

}
