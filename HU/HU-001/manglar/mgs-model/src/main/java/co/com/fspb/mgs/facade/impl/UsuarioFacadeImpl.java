package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.facade.api.UsuarioFacadeAPI;
import co.com.fspb.mgs.service.api.UsuarioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link UsuarioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class UsuarioFacadeImpl
 * @date nov 11, 2016
 */
@Service("usuarioFacade")
public class UsuarioFacadeImpl implements UsuarioFacadeAPI {

    @Autowired
    private UsuarioServiceAPI usuarioService;

    /**
     * @see co.com.fspb.mgs.facade.api.UsuarioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<UsuarioDTO> getRecords(PmzPagingCriteria criteria) {
        return usuarioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.UsuarioFacadeAPI#guardar()
     */
    @Override
    public void guardar(UsuarioDTO usuario) throws PmzException {
        usuarioService.guardar(usuario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.UsuarioFacadeAPI#editar()
     */
    @Override
    public void editar(UsuarioDTO usuario) throws PmzException {
        usuarioService.editar(usuario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.UsuarioFacadeAPI#getUsuarioDTO()
     */
    @Override
    public UsuarioDTO getUsuarioDTO(Long id) {
        return usuarioService.getUsuarioDTO(id);
    }

}
