package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TipoTerritorioDaoAPI;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.dao.model.TipoTerritorio;
import co.com.fspb.mgs.service.api.TipoTerritorioServiceAPI;
import co.com.fspb.mgs.dto.TipoTerritorioDTO;

/**
 * Implementación de la Interfaz {@link TipoTerritorioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTerritorioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TipoTerritorioServiceImpl implements TipoTerritorioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TipoTerritorioDaoAPI tipoTerritorioDao;
    @Autowired
    private ZonaDaoAPI zonaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TipoTerritorioDTO tipoTerritorioDTO) throws PmzException {

        TipoTerritorio tipoTerritorioValidate = tipoTerritorioDao.get(tipoTerritorioDTO.getId());

        if (tipoTerritorioValidate != null) {
            throw new PmzException("El tipoTerritorio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TipoTerritorio tipoTerritorio = DTOTransformer.getTipoTerritorioFromTipoTerritorioDTO(new TipoTerritorio(), tipoTerritorioDTO);

		if (tipoTerritorioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(tipoTerritorioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		tipoTerritorio.setZona(zona);
		}
		
        tipoTerritorioDao.save(tipoTerritorio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        List<TipoTerritorio> listaReturn = tipoTerritorioDao.getRecords(criteria);
        Long countTotalRegistros = tipoTerritorioDao.countRecords(criteria);

        List<TipoTerritorioDTO> resultList = new ArrayList<TipoTerritorioDTO>();
        for (TipoTerritorio tipoTerritorio : listaReturn) {
            TipoTerritorioDTO tipoTerritorioDTO = DTOTransformer
                    .getTipoTerritorioDTOFromTipoTerritorio(tipoTerritorio);
            resultList.add(tipoTerritorioDTO);
        }
        
        return new PmzResultSet<TipoTerritorioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TipoTerritorioDTO tipoTerritorioDTO) throws PmzException{

        TipoTerritorio tipoTerritorio = tipoTerritorioDao.get(tipoTerritorioDTO.getId());

        if (tipoTerritorio == null) {
            throw new PmzException("El tipoTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTipoTerritorioFromTipoTerritorioDTO(tipoTerritorio, tipoTerritorioDTO);

		if (tipoTerritorioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(tipoTerritorioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		tipoTerritorio.setZona(zona);
		}
		
        tipoTerritorioDao.update(tipoTerritorio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TipoTerritorioDTO getTipoTerritorioDTO(Long id) {
        TipoTerritorio tipoTerritorio = tipoTerritorioDao.get(id);
        return DTOTransformer.getTipoTerritorioDTOFromTipoTerritorio(tipoTerritorio);
    }
}
