package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.AreaEducacionDaoAPI;
import co.com.fspb.mgs.dao.model.AreaEducacion;
import co.com.fspb.mgs.service.api.AreaEducacionServiceAPI;
import co.com.fspb.mgs.dto.AreaEducacionDTO;

/**
 * Implementación de la Interfaz {@link AreaEducacionServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AreaEducacionServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class AreaEducacionServiceImpl implements AreaEducacionServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private AreaEducacionDaoAPI areaEducacionDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(AreaEducacionDTO areaEducacionDTO) throws PmzException {

        AreaEducacion areaEducacionValidate = areaEducacionDao.get(areaEducacionDTO.getId());

        if (areaEducacionValidate != null) {
            throw new PmzException("El areaEducacion ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        AreaEducacion areaEducacion = DTOTransformer.getAreaEducacionFromAreaEducacionDTO(new AreaEducacion(), areaEducacionDTO);

        areaEducacionDao.save(areaEducacion);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<AreaEducacionDTO> getRecords(PmzPagingCriteria criteria) {
        List<AreaEducacion> listaReturn = areaEducacionDao.getRecords(criteria);
        Long countTotalRegistros = areaEducacionDao.countRecords(criteria);

        List<AreaEducacionDTO> resultList = new ArrayList<AreaEducacionDTO>();
        for (AreaEducacion areaEducacion : listaReturn) {
            AreaEducacionDTO areaEducacionDTO = DTOTransformer
                    .getAreaEducacionDTOFromAreaEducacion(areaEducacion);
            resultList.add(areaEducacionDTO);
        }
        
        return new PmzResultSet<AreaEducacionDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(AreaEducacionDTO areaEducacionDTO) throws PmzException{

        AreaEducacion areaEducacion = areaEducacionDao.get(areaEducacionDTO.getId());

        if (areaEducacion == null) {
            throw new PmzException("El areaEducacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getAreaEducacionFromAreaEducacionDTO(areaEducacion, areaEducacionDTO);

        areaEducacionDao.update(areaEducacion);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public AreaEducacionDTO getAreaEducacionDTO(Long id) {
        AreaEducacion areaEducacion = areaEducacionDao.get(id);
        return DTOTransformer.getAreaEducacionDTOFromAreaEducacion(areaEducacion);
    }
}
