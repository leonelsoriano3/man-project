package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI;
import co.com.fspb.mgs.service.api.ProblemaComunidadServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ProblemaComunidadFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadFacadeImpl
 * @date nov 11, 2016
 */
@Service("problemaComunidadFacade")
public class ProblemaComunidadFacadeImpl implements ProblemaComunidadFacadeAPI {

    @Autowired
    private ProblemaComunidadServiceAPI problemaComunidadService;

    /**
     * @see co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProblemaComunidadDTO> getRecords(PmzPagingCriteria criteria) {
        return problemaComunidadService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI#guardar()
     */
    @Override
    public void guardar(ProblemaComunidadDTO problemaComunidad) throws PmzException {
        problemaComunidadService.guardar(problemaComunidad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI#editar()
     */
    @Override
    public void editar(ProblemaComunidadDTO problemaComunidad) throws PmzException {
        problemaComunidadService.editar(problemaComunidad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI#getProblemaComunidadDTO()
     */
    @Override
    public ProblemaComunidadDTO getProblemaComunidadDTO(Long id) {
        return problemaComunidadService.getProblemaComunidadDTO(id);
    }

}
