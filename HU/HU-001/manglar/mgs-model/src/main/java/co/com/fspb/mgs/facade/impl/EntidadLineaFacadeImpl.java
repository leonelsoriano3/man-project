package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI;
import co.com.fspb.mgs.service.api.EntidadLineaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EntidadLineaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaFacadeImpl
 * @date nov 11, 2016
 */
@Service("entidadLineaFacade")
public class EntidadLineaFacadeImpl implements EntidadLineaFacadeAPI {

    @Autowired
    private EntidadLineaServiceAPI entidadLineaService;

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadLineaDTO> getRecords(PmzPagingCriteria criteria) {
        return entidadLineaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI#guardar()
     */
    @Override
    public void guardar(EntidadLineaDTO entidadLinea) throws PmzException {
        entidadLineaService.guardar(entidadLinea);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI#editar()
     */
    @Override
    public void editar(EntidadLineaDTO entidadLinea) throws PmzException {
        entidadLineaService.editar(entidadLinea);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI#getEntidadLineaDTO()
     */
    @Override
    public EntidadLineaDTO getEntidadLineaDTO(EntidadDTO entidad, LineaIntervencionDTO lineaIntervencion) {
        return entidadLineaService.getEntidadLineaDTO(entidad, lineaIntervencion);
    }

}
