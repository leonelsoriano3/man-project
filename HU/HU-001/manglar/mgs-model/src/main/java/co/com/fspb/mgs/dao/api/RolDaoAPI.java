package co.com.fspb.mgs.dao.api;

import java.util.List;

import com.premize.pmz.dao.api.Dao;

import co.com.fspb.mgs.dao.model.Rol;
import com.premize.pmz.api.dto.PmzPagingCriteria;

/**
 * Interfaz de Data Access Object (DAO) para la entidad {@link Rol}
 * Extiende la interfaz de PMZ {@link Dao}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolDaoAPI
 * @date nov 11, 2016
 */
public interface RolDaoAPI extends Dao<Rol, Long> {

    /**
	 * Retorna la lista paginada y filtrada según el {@link PmzPagingCriteria}
	 * de la entidad {@link Rol}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public List<Rol> getRecords(PmzPagingCriteria criteria);
    
    /**
	 * Retorna el total de regirtos según el {@link PmzPagingCriteria}
	 * de la entidad {@link Rol}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public Long countRecords(PmzPagingCriteria criteria);

}
