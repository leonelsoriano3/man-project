package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link ProyectoTerritorio}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioServiceAPI
 * @date nov 11, 2016
 */
public interface ProyectoTerritorioServiceAPI {

    /**
	 * Registra una entidad {@link ProyectoTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoTerritorio - {@link ProyectoTerritorioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ProyectoTerritorioDTO proyectoTerritorio) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link ProyectoTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoTerritorio - {@link ProyectoTerritorioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ProyectoTerritorioDTO proyectoTerritorio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ProyectoTerritorioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ProyectoTerritorioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link TerritorioDTO}
	 * @param proyecto - {@link ProyectoDTO}
     * @return {@link ProyectoTerritorioDTO}
     */
    ProyectoTerritorioDTO getProyectoTerritorioDTO(TerritorioDTO territorio, ProyectoDTO proyecto);

}
