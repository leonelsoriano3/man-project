package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParentescoDTO;
import co.com.fspb.mgs.facade.api.ParentescoFacadeAPI;
import co.com.fspb.mgs.service.api.ParentescoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ParentescoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoFacadeImpl
 * @date nov 11, 2016
 */
@Service("parentescoFacade")
public class ParentescoFacadeImpl implements ParentescoFacadeAPI {

    @Autowired
    private ParentescoServiceAPI parentescoService;

    /**
     * @see co.com.fspb.mgs.facade.api.ParentescoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParentescoDTO> getRecords(PmzPagingCriteria criteria) {
        return parentescoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ParentescoFacadeAPI#guardar()
     */
    @Override
    public void guardar(ParentescoDTO parentesco) throws PmzException {
        parentescoService.guardar(parentesco);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParentescoFacadeAPI#editar()
     */
    @Override
    public void editar(ParentescoDTO parentesco) throws PmzException {
        parentescoService.editar(parentesco);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParentescoFacadeAPI#getParentescoDTO()
     */
    @Override
    public ParentescoDTO getParentescoDTO(Long id) {
        return parentescoService.getParentescoDTO(id);
    }

}
