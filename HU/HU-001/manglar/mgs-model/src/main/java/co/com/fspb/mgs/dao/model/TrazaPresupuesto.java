package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_traza_presupuesto en la BD a la entidad TrazaPresupuesto.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuesto
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_traza_presupuesto" )
public class TrazaPresupuesto implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_traza_ppto", nullable = false)
	private Long id; 

    @Column(name = "anio", nullable = true, precision = 10, scale = 0)
    private Integer anio; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 


	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_linea", nullable = false)
    private Linea linea; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

    @Column(name = "presupuesto_ejecutado", nullable = false, precision = 131089, scale = 0)
    private Long presupuestoEjecutado; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 

    @Column(name = "presupuesto_planeado", nullable = true, precision = 131089, scale = 0)
    private Long presupuestoPlaneado; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Linea getLinea() {
        return linea;
    }

    public void setLinea(Linea linea) {
        this.linea = linea;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Long getPresupuestoEjecutado() {
        return presupuestoEjecutado;
    }

    public void setPresupuestoEjecutado(Long presupuestoEjecutado) {
        this.presupuestoEjecutado = presupuestoEjecutado;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getPresupuestoPlaneado() {
        return presupuestoPlaneado;
    }

    public void setPresupuestoPlaneado(Long presupuestoPlaneado) {
        this.presupuestoPlaneado = presupuestoPlaneado;
    }

}
