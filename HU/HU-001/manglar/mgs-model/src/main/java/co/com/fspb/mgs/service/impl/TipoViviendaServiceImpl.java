package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TipoViviendaDaoAPI;
import co.com.fspb.mgs.dao.model.TipoVivienda;
import co.com.fspb.mgs.service.api.TipoViviendaServiceAPI;
import co.com.fspb.mgs.dto.TipoViviendaDTO;

/**
 * Implementación de la Interfaz {@link TipoViviendaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoViviendaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TipoViviendaServiceImpl implements TipoViviendaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TipoViviendaDaoAPI tipoViviendaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TipoViviendaDTO tipoViviendaDTO) throws PmzException {

        TipoVivienda tipoViviendaValidate = tipoViviendaDao.get(tipoViviendaDTO.getId());

        if (tipoViviendaValidate != null) {
            throw new PmzException("El tipoVivienda ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TipoVivienda tipoVivienda = DTOTransformer.getTipoViviendaFromTipoViviendaDTO(new TipoVivienda(), tipoViviendaDTO);

        tipoViviendaDao.save(tipoVivienda);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoViviendaDTO> getRecords(PmzPagingCriteria criteria) {
        List<TipoVivienda> listaReturn = tipoViviendaDao.getRecords(criteria);
        Long countTotalRegistros = tipoViviendaDao.countRecords(criteria);

        List<TipoViviendaDTO> resultList = new ArrayList<TipoViviendaDTO>();
        for (TipoVivienda tipoVivienda : listaReturn) {
            TipoViviendaDTO tipoViviendaDTO = DTOTransformer
                    .getTipoViviendaDTOFromTipoVivienda(tipoVivienda);
            resultList.add(tipoViviendaDTO);
        }
        
        return new PmzResultSet<TipoViviendaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TipoViviendaDTO tipoViviendaDTO) throws PmzException{

        TipoVivienda tipoVivienda = tipoViviendaDao.get(tipoViviendaDTO.getId());

        if (tipoVivienda == null) {
            throw new PmzException("El tipoVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTipoViviendaFromTipoViviendaDTO(tipoVivienda, tipoViviendaDTO);

        tipoViviendaDao.update(tipoVivienda);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TipoViviendaDTO getTipoViviendaDTO(Long id) {
        TipoVivienda tipoVivienda = tipoViviendaDao.get(id);
        return DTOTransformer.getTipoViviendaDTOFromTipoVivienda(tipoVivienda);
    }
}
