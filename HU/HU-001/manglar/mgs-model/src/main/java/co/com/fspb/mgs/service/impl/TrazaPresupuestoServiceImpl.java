package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TrazaPresupuestoDaoAPI;
import co.com.fspb.mgs.dao.api.LineaDaoAPI;
import co.com.fspb.mgs.dao.model.Linea;
import co.com.fspb.mgs.dao.model.TrazaPresupuesto;
import co.com.fspb.mgs.service.api.TrazaPresupuestoServiceAPI;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;

/**
 * Implementación de la Interfaz {@link TrazaPresupuestoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuestoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TrazaPresupuestoServiceImpl implements TrazaPresupuestoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TrazaPresupuestoDaoAPI trazaPresupuestoDao;
    @Autowired
    private LineaDaoAPI lineaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TrazaPresupuestoDTO trazaPresupuestoDTO) throws PmzException {

        TrazaPresupuesto trazaPresupuestoValidate = trazaPresupuestoDao.get(trazaPresupuestoDTO.getId());

        if (trazaPresupuestoValidate != null) {
            throw new PmzException("El trazaPresupuesto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TrazaPresupuesto trazaPresupuesto = DTOTransformer.getTrazaPresupuestoFromTrazaPresupuestoDTO(new TrazaPresupuesto(), trazaPresupuestoDTO);

		if (trazaPresupuestoDTO.getLineaDTO() != null) {
       		Linea linea = lineaDao.get(trazaPresupuestoDTO.getLineaDTO().getId());
       		if (linea == null) {
                throw new PmzException("El Linea no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		trazaPresupuesto.setLinea(linea);
		}
		
        trazaPresupuestoDao.save(trazaPresupuesto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TrazaPresupuestoDTO> getRecords(PmzPagingCriteria criteria) {
        List<TrazaPresupuesto> listaReturn = trazaPresupuestoDao.getRecords(criteria);
        Long countTotalRegistros = trazaPresupuestoDao.countRecords(criteria);

        List<TrazaPresupuestoDTO> resultList = new ArrayList<TrazaPresupuestoDTO>();
        for (TrazaPresupuesto trazaPresupuesto : listaReturn) {
            TrazaPresupuestoDTO trazaPresupuestoDTO = DTOTransformer
                    .getTrazaPresupuestoDTOFromTrazaPresupuesto(trazaPresupuesto);
            resultList.add(trazaPresupuestoDTO);
        }
        
        return new PmzResultSet<TrazaPresupuestoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TrazaPresupuestoDTO trazaPresupuestoDTO) throws PmzException{

        TrazaPresupuesto trazaPresupuesto = trazaPresupuestoDao.get(trazaPresupuestoDTO.getId());

        if (trazaPresupuesto == null) {
            throw new PmzException("El trazaPresupuesto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTrazaPresupuestoFromTrazaPresupuestoDTO(trazaPresupuesto, trazaPresupuestoDTO);

		if (trazaPresupuestoDTO.getLineaDTO() != null) {
       		Linea linea = lineaDao.get(trazaPresupuestoDTO.getLineaDTO().getId());
       		if (linea == null) {
                throw new PmzException("El Linea no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		trazaPresupuesto.setLinea(linea);
		}
		
        trazaPresupuestoDao.update(trazaPresupuesto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TrazaPresupuestoDTO getTrazaPresupuestoDTO(Long id) {
        TrazaPresupuesto trazaPresupuesto = trazaPresupuestoDao.get(id);
        return DTOTransformer.getTrazaPresupuestoDTOFromTrazaPresupuesto(trazaPresupuesto);
    }
}
