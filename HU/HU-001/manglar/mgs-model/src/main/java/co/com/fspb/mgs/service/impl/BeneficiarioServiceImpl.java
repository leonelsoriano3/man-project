package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.api.AreaEducacionDaoAPI;
import co.com.fspb.mgs.dao.model.AreaEducacion;
import co.com.fspb.mgs.dao.api.EstructuraFamiliarDaoAPI;
import co.com.fspb.mgs.dao.model.EstructuraFamiliar;
import co.com.fspb.mgs.dao.api.EstadoCivilDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoCivil;
import co.com.fspb.mgs.dao.api.DescIndependienteDaoAPI;
import co.com.fspb.mgs.dao.model.DescIndependiente;
import co.com.fspb.mgs.dao.api.NivelEducativoDaoAPI;
import co.com.fspb.mgs.dao.model.NivelEducativo;
import co.com.fspb.mgs.dao.api.TipoViviendaDaoAPI;
import co.com.fspb.mgs.dao.model.TipoVivienda;
import co.com.fspb.mgs.dao.api.TipoTenenciaDaoAPI;
import co.com.fspb.mgs.dao.model.TipoTenencia;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.dao.api.MaterialViviendaDaoAPI;
import co.com.fspb.mgs.dao.model.MaterialVivienda;
import co.com.fspb.mgs.dao.api.BdNacionalDaoAPI;
import co.com.fspb.mgs.dao.model.BdNacional;
import co.com.fspb.mgs.dao.api.AfiliadoSaludDaoAPI;
import co.com.fspb.mgs.dao.model.AfiliadoSalud;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.api.GrupoEtnicoDaoAPI;
import co.com.fspb.mgs.dao.model.GrupoEtnico;
import co.com.fspb.mgs.dao.api.OcupacionDaoAPI;
import co.com.fspb.mgs.dao.model.Ocupacion;
import co.com.fspb.mgs.dao.api.MunicipioDaoAPI;
import co.com.fspb.mgs.dao.model.Municipio;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.service.api.BeneficiarioServiceAPI;
import co.com.fspb.mgs.dto.BeneficiarioDTO;

/**
 * Implementación de la Interfaz {@link BeneficiarioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class BeneficiarioServiceImpl implements BeneficiarioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private BeneficiarioDaoAPI beneficiarioDao;
    @Autowired
    private AreaEducacionDaoAPI areaEducacionDao;
    @Autowired
    private EstructuraFamiliarDaoAPI estructuraFamiliarDao;
    @Autowired
    private EstadoCivilDaoAPI estadoCivilDao;
    @Autowired
    private DescIndependienteDaoAPI descIndependienteDao;
    @Autowired
    private NivelEducativoDaoAPI nivelEducativoDao;
    @Autowired
    private TipoViviendaDaoAPI tipoViviendaDao;
    @Autowired
    private TipoTenenciaDaoAPI tipoTenenciaDao;
    @Autowired
    private ZonaDaoAPI zonaDao;
    @Autowired
    private MaterialViviendaDaoAPI materialViviendaDao;
    @Autowired
    private BdNacionalDaoAPI bdNacionalDao;
    @Autowired
    private AfiliadoSaludDaoAPI afiliadoSaludDao;
    @Autowired
    private PersonaDaoAPI personaDao;
    @Autowired
    private GrupoEtnicoDaoAPI grupoEtnicoDao;
    @Autowired
    private OcupacionDaoAPI ocupacionDao;
    @Autowired
    private MunicipioDaoAPI municipioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(BeneficiarioDTO beneficiarioDTO) throws PmzException {

        Beneficiario beneficiarioValidate = beneficiarioDao.get(beneficiarioDTO.getId());

        if (beneficiarioValidate != null) {
            throw new PmzException("El beneficiario ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Beneficiario beneficiario = DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), beneficiarioDTO);

		if (beneficiarioDTO.getAreaEducacionDTO() != null) {
       		AreaEducacion areaEducacion = areaEducacionDao.get(beneficiarioDTO.getAreaEducacionDTO().getId());
       		if (areaEducacion == null) {
                throw new PmzException("El AreaEducacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setAreaEducacion(areaEducacion);
		}
		
		if (beneficiarioDTO.getEstructuraFamiliarDTO() != null) {
       		EstructuraFamiliar estructuraFamiliar = estructuraFamiliarDao.get(beneficiarioDTO.getEstructuraFamiliarDTO().getId());
       		if (estructuraFamiliar == null) {
                throw new PmzException("El EstructuraFamiliar no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setEstructuraFamiliar(estructuraFamiliar);
		}
		
		if (beneficiarioDTO.getEstadoCivilDTO() != null) {
       		EstadoCivil estadoCivil = estadoCivilDao.get(beneficiarioDTO.getEstadoCivilDTO().getId());
       		if (estadoCivil == null) {
                throw new PmzException("El EstadoCivil no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setEstadoCivil(estadoCivil);
		}
		
		if (beneficiarioDTO.getDescIndependienteDTO() != null) {
       		DescIndependiente descIndependiente = descIndependienteDao.get(beneficiarioDTO.getDescIndependienteDTO().getId());
       		if (descIndependiente == null) {
                throw new PmzException("El DescIndependiente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setDescIndependiente(descIndependiente);
		}
		
		if (beneficiarioDTO.getNivelEducativoDTO() != null) {
       		NivelEducativo nivelEducativo = nivelEducativoDao.get(beneficiarioDTO.getNivelEducativoDTO().getId());
       		if (nivelEducativo == null) {
                throw new PmzException("El NivelEducativo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setNivelEducativo(nivelEducativo);
		}
		
		if (beneficiarioDTO.getTipoViviendaDTO() != null) {
       		TipoVivienda tipoVivienda = tipoViviendaDao.get(beneficiarioDTO.getTipoViviendaDTO().getId());
       		if (tipoVivienda == null) {
                throw new PmzException("El TipoVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setTipoVivienda(tipoVivienda);
		}
		
		if (beneficiarioDTO.getTipoTenenciaDTO() != null) {
       		TipoTenencia tipoTenencia = tipoTenenciaDao.get(beneficiarioDTO.getTipoTenenciaDTO().getId());
       		if (tipoTenencia == null) {
                throw new PmzException("El TipoTenencia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setTipoTenencia(tipoTenencia);
		}
		
		if (beneficiarioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(beneficiarioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setZona(zona);
		}
		
		if (beneficiarioDTO.getMaterialViviendaDTO() != null) {
       		MaterialVivienda materialVivienda = materialViviendaDao.get(beneficiarioDTO.getMaterialViviendaDTO().getId());
       		if (materialVivienda == null) {
                throw new PmzException("El MaterialVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setMaterialVivienda(materialVivienda);
		}
		
		if (beneficiarioDTO.getBdNacionalDTO() != null) {
       		BdNacional bdNacional = bdNacionalDao.get(beneficiarioDTO.getBdNacionalDTO().getId());
       		if (bdNacional == null) {
                throw new PmzException("El BdNacional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setBdNacional(bdNacional);
		}
		
		if (beneficiarioDTO.getAfiliadoSaludDTO() != null) {
       		AfiliadoSalud afiliadoSalud = afiliadoSaludDao.get(beneficiarioDTO.getAfiliadoSaludDTO().getId());
       		if (afiliadoSalud == null) {
                throw new PmzException("El AfiliadoSalud no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setAfiliadoSalud(afiliadoSalud);
		}
		
		if (beneficiarioDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(beneficiarioDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setPersona(persona);
		}
		
		if (beneficiarioDTO.getGrupoEtnicoDTO() != null) {
       		GrupoEtnico grupoEtnico = grupoEtnicoDao.get(beneficiarioDTO.getGrupoEtnicoDTO().getId());
       		if (grupoEtnico == null) {
                throw new PmzException("El GrupoEtnico no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setGrupoEtnico(grupoEtnico);
		}
		
		if (beneficiarioDTO.getOcupacionDTO() != null) {
       		Ocupacion ocupacion = ocupacionDao.get(beneficiarioDTO.getOcupacionDTO().getId());
       		if (ocupacion == null) {
                throw new PmzException("El Ocupacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setOcupacion(ocupacion);
		}
		
		if (beneficiarioDTO.getMunicipioDTO() != null) {
       		Municipio municipio = municipioDao.get(beneficiarioDTO.getMunicipioDTO().getId());
       		if (municipio == null) {
                throw new PmzException("El Municipio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setMunicipio(municipio);
		}
		
        beneficiarioDao.save(beneficiario);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<BeneficiarioDTO> getRecords(PmzPagingCriteria criteria) {
        List<Beneficiario> listaReturn = beneficiarioDao.getRecords(criteria);
        Long countTotalRegistros = beneficiarioDao.countRecords(criteria);

        List<BeneficiarioDTO> resultList = new ArrayList<BeneficiarioDTO>();
        for (Beneficiario beneficiario : listaReturn) {
            BeneficiarioDTO beneficiarioDTO = DTOTransformer
                    .getBeneficiarioDTOFromBeneficiario(beneficiario);
            resultList.add(beneficiarioDTO);
        }
        
        return new PmzResultSet<BeneficiarioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(BeneficiarioDTO beneficiarioDTO) throws PmzException{

        Beneficiario beneficiario = beneficiarioDao.get(beneficiarioDTO.getId());

        if (beneficiario == null) {
            throw new PmzException("El beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getBeneficiarioFromBeneficiarioDTO(beneficiario, beneficiarioDTO);

		if (beneficiarioDTO.getAreaEducacionDTO() != null) {
       		AreaEducacion areaEducacion = areaEducacionDao.get(beneficiarioDTO.getAreaEducacionDTO().getId());
       		if (areaEducacion == null) {
                throw new PmzException("El AreaEducacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setAreaEducacion(areaEducacion);
		}
		
		if (beneficiarioDTO.getEstructuraFamiliarDTO() != null) {
       		EstructuraFamiliar estructuraFamiliar = estructuraFamiliarDao.get(beneficiarioDTO.getEstructuraFamiliarDTO().getId());
       		if (estructuraFamiliar == null) {
                throw new PmzException("El EstructuraFamiliar no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setEstructuraFamiliar(estructuraFamiliar);
		}
		
		if (beneficiarioDTO.getEstadoCivilDTO() != null) {
       		EstadoCivil estadoCivil = estadoCivilDao.get(beneficiarioDTO.getEstadoCivilDTO().getId());
       		if (estadoCivil == null) {
                throw new PmzException("El EstadoCivil no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setEstadoCivil(estadoCivil);
		}
		
		if (beneficiarioDTO.getDescIndependienteDTO() != null) {
       		DescIndependiente descIndependiente = descIndependienteDao.get(beneficiarioDTO.getDescIndependienteDTO().getId());
       		if (descIndependiente == null) {
                throw new PmzException("El DescIndependiente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setDescIndependiente(descIndependiente);
		}
		
		if (beneficiarioDTO.getNivelEducativoDTO() != null) {
       		NivelEducativo nivelEducativo = nivelEducativoDao.get(beneficiarioDTO.getNivelEducativoDTO().getId());
       		if (nivelEducativo == null) {
                throw new PmzException("El NivelEducativo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setNivelEducativo(nivelEducativo);
		}
		
		if (beneficiarioDTO.getTipoViviendaDTO() != null) {
       		TipoVivienda tipoVivienda = tipoViviendaDao.get(beneficiarioDTO.getTipoViviendaDTO().getId());
       		if (tipoVivienda == null) {
                throw new PmzException("El TipoVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setTipoVivienda(tipoVivienda);
		}
		
		if (beneficiarioDTO.getTipoTenenciaDTO() != null) {
       		TipoTenencia tipoTenencia = tipoTenenciaDao.get(beneficiarioDTO.getTipoTenenciaDTO().getId());
       		if (tipoTenencia == null) {
                throw new PmzException("El TipoTenencia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setTipoTenencia(tipoTenencia);
		}
		
		if (beneficiarioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(beneficiarioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setZona(zona);
		}
		
		if (beneficiarioDTO.getMaterialViviendaDTO() != null) {
       		MaterialVivienda materialVivienda = materialViviendaDao.get(beneficiarioDTO.getMaterialViviendaDTO().getId());
       		if (materialVivienda == null) {
                throw new PmzException("El MaterialVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setMaterialVivienda(materialVivienda);
		}
		
		if (beneficiarioDTO.getBdNacionalDTO() != null) {
       		BdNacional bdNacional = bdNacionalDao.get(beneficiarioDTO.getBdNacionalDTO().getId());
       		if (bdNacional == null) {
                throw new PmzException("El BdNacional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setBdNacional(bdNacional);
		}
		
		if (beneficiarioDTO.getAfiliadoSaludDTO() != null) {
       		AfiliadoSalud afiliadoSalud = afiliadoSaludDao.get(beneficiarioDTO.getAfiliadoSaludDTO().getId());
       		if (afiliadoSalud == null) {
                throw new PmzException("El AfiliadoSalud no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setAfiliadoSalud(afiliadoSalud);
		}
		
		if (beneficiarioDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(beneficiarioDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setPersona(persona);
		}
		
		if (beneficiarioDTO.getGrupoEtnicoDTO() != null) {
       		GrupoEtnico grupoEtnico = grupoEtnicoDao.get(beneficiarioDTO.getGrupoEtnicoDTO().getId());
       		if (grupoEtnico == null) {
                throw new PmzException("El GrupoEtnico no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setGrupoEtnico(grupoEtnico);
		}
		
		if (beneficiarioDTO.getOcupacionDTO() != null) {
       		Ocupacion ocupacion = ocupacionDao.get(beneficiarioDTO.getOcupacionDTO().getId());
       		if (ocupacion == null) {
                throw new PmzException("El Ocupacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setOcupacion(ocupacion);
		}
		
		if (beneficiarioDTO.getMunicipioDTO() != null) {
       		Municipio municipio = municipioDao.get(beneficiarioDTO.getMunicipioDTO().getId());
       		if (municipio == null) {
                throw new PmzException("El Municipio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		beneficiario.setMunicipio(municipio);
		}
		
        beneficiarioDao.update(beneficiario);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public BeneficiarioDTO getBeneficiarioDTO(Long id) {
        Beneficiario beneficiario = beneficiarioDao.get(id);
        return DTOTransformer.getBeneficiarioDTOFromBeneficiario(beneficiario);
    }
}
