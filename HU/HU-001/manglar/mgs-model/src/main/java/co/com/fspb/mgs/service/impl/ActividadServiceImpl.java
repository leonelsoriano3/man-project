package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.dao.model.Componente;
import co.com.fspb.mgs.dao.api.EstadoActividadDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoActividad;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dao.model.Actividad;
import co.com.fspb.mgs.service.api.ActividadServiceAPI;
import co.com.fspb.mgs.dto.ActividadDTO;

/**
 * Implementación de la Interfaz {@link ActividadServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ActividadServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ActividadServiceImpl implements ActividadServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ActividadDaoAPI actividadDao;
    @Autowired
    private ComponenteDaoAPI componenteDao;
    @Autowired
    private EstadoActividadDaoAPI estadoActividadDao;
    @Autowired
    private UsuarioDaoAPI usuarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ActividadDTO actividadDTO) throws PmzException {

        Actividad actividadValidate = actividadDao.get(actividadDTO.getId());

        if (actividadValidate != null) {
            throw new PmzException("El actividad ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Actividad actividad = DTOTransformer.getActividadFromActividadDTO(new Actividad(), actividadDTO);

		if (actividadDTO.getComponenteDTO() != null) {
       		Componente componente = componenteDao.get(actividadDTO.getComponenteDTO().getId());
       		if (componente == null) {
                throw new PmzException("El Componente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setComponente(componente);
		}
		
		if (actividadDTO.getEstadoActividadDTO() != null) {
       		EstadoActividad estadoActividad = estadoActividadDao.get(actividadDTO.getEstadoActividadDTO().getId());
       		if (estadoActividad == null) {
                throw new PmzException("El EstadoActividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setEstadoActividad(estadoActividad);
		}
		
		if (actividadDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(actividadDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setUsuario(usuario);
		}
		
        actividadDao.save(actividad);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ActividadDTO> getRecords(PmzPagingCriteria criteria) {
        List<Actividad> listaReturn = actividadDao.getRecords(criteria);
        Long countTotalRegistros = actividadDao.countRecords(criteria);

        List<ActividadDTO> resultList = new ArrayList<ActividadDTO>();
        for (Actividad actividad : listaReturn) {
            ActividadDTO actividadDTO = DTOTransformer
                    .getActividadDTOFromActividad(actividad);
            resultList.add(actividadDTO);
        }
        
        return new PmzResultSet<ActividadDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ActividadDTO actividadDTO) throws PmzException{

        Actividad actividad = actividadDao.get(actividadDTO.getId());

        if (actividad == null) {
            throw new PmzException("El actividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getActividadFromActividadDTO(actividad, actividadDTO);

		if (actividadDTO.getComponenteDTO() != null) {
       		Componente componente = componenteDao.get(actividadDTO.getComponenteDTO().getId());
       		if (componente == null) {
                throw new PmzException("El Componente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setComponente(componente);
		}
		
		if (actividadDTO.getEstadoActividadDTO() != null) {
       		EstadoActividad estadoActividad = estadoActividadDao.get(actividadDTO.getEstadoActividadDTO().getId());
       		if (estadoActividad == null) {
                throw new PmzException("El EstadoActividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setEstadoActividad(estadoActividad);
		}
		
		if (actividadDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(actividadDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		actividad.setUsuario(usuario);
		}
		
        actividadDao.update(actividad);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ActividadDTO getActividadDTO(Long id) {
        Actividad actividad = actividadDao.get(id);
        return DTOTransformer.getActividadDTOFromActividad(actividad);
    }
}
