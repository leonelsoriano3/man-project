package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI;
import co.com.fspb.mgs.service.api.TipoAliadoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TipoAliadoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoFacadeImpl
 * @date nov 11, 2016
 */
@Service("tipoAliadoFacade")
public class TipoAliadoFacadeImpl implements TipoAliadoFacadeAPI {

    @Autowired
    private TipoAliadoServiceAPI tipoAliadoService;

    /**
     * @see co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoAliadoDTO> getRecords(PmzPagingCriteria criteria) {
        return tipoAliadoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI#guardar()
     */
    @Override
    public void guardar(TipoAliadoDTO tipoAliado) throws PmzException {
        tipoAliadoService.guardar(tipoAliado);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI#editar()
     */
    @Override
    public void editar(TipoAliadoDTO tipoAliado) throws PmzException {
        tipoAliadoService.editar(tipoAliado);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI#getTipoAliadoDTO()
     */
    @Override
    public TipoAliadoDTO getTipoAliadoDTO(Long id) {
        return tipoAliadoService.getTipoAliadoDTO(id);
    }

}
