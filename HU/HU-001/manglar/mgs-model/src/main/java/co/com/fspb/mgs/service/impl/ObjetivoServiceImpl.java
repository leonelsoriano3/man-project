package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.service.api.ObjetivoServiceAPI;
import co.com.fspb.mgs.dto.ObjetivoDTO;

/**
 * Implementación de la Interfaz {@link ObjetivoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ObjetivoServiceImpl implements ObjetivoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ObjetivoDaoAPI objetivoDao;
    @Autowired
    private UsuarioDaoAPI usuarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ObjetivoDTO objetivoDTO) throws PmzException {

        Objetivo objetivoValidate = objetivoDao.get(objetivoDTO.getId());

        if (objetivoValidate != null) {
            throw new PmzException("El objetivo ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Objetivo objetivo = DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), objetivoDTO);

		if (objetivoDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(objetivoDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		objetivo.setUsuario(usuario);
		}
		
        objetivoDao.save(objetivo);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ObjetivoDTO> getRecords(PmzPagingCriteria criteria) {
        List<Objetivo> listaReturn = objetivoDao.getRecords(criteria);
        Long countTotalRegistros = objetivoDao.countRecords(criteria);

        List<ObjetivoDTO> resultList = new ArrayList<ObjetivoDTO>();
        for (Objetivo objetivo : listaReturn) {
            ObjetivoDTO objetivoDTO = DTOTransformer
                    .getObjetivoDTOFromObjetivo(objetivo);
            resultList.add(objetivoDTO);
        }
        
        return new PmzResultSet<ObjetivoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ObjetivoDTO objetivoDTO) throws PmzException{

        Objetivo objetivo = objetivoDao.get(objetivoDTO.getId());

        if (objetivo == null) {
            throw new PmzException("El objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getObjetivoFromObjetivoDTO(objetivo, objetivoDTO);

		if (objetivoDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(objetivoDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		objetivo.setUsuario(usuario);
		}
		
        objetivoDao.update(objetivo);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ObjetivoDTO getObjetivoDTO(Long id) {
        Objetivo objetivo = objetivoDao.get(id);
        return DTOTransformer.getObjetivoDTOFromObjetivo(objetivo);
    }
}
