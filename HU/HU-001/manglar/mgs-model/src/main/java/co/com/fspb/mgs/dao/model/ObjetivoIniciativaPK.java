package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaPK
 * @date nov 11, 2016
 */
@Embeddable
public class ObjetivoIniciativaPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_objetivo", nullable = true)
    private Objetivo objetivo; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_iniciativa", nullable = true)
    private Iniciativa iniciativa; 

    @Column(name = "id_usuario_resp", nullable = true, precision = 10, scale = 0)
    private Integer idUsuarioResp; 
 
 
    public Objetivo getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Objetivo objetivo) {
        this.objetivo = objetivo;
    }

    public Iniciativa getIniciativa() {
        return iniciativa;
    }

    public void setIniciativa(Iniciativa iniciativa) {
        this.iniciativa = iniciativa;
    }

    public Integer getIdUsuarioResp() {
        return idUsuarioResp;
    }

    public void setIdUsuarioResp(Integer idUsuarioResp) {
        this.idUsuarioResp = idUsuarioResp;
    }

}
