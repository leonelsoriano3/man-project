package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link AfiliadoSalud}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludServiceAPI
 * @date nov 11, 2016
 */
public interface AfiliadoSaludServiceAPI {

    /**
	 * Registra una entidad {@link AfiliadoSalud} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param afiliadoSalud - {@link AfiliadoSaludDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(AfiliadoSaludDTO afiliadoSalud) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link AfiliadoSalud} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param afiliadoSalud - {@link AfiliadoSaludDTO}
	 * @throws {@link PmzException}
	 */
	void editar(AfiliadoSaludDTO afiliadoSalud) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<AfiliadoSaludDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link AfiliadoSaludDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link AfiliadoSaludDTO}
     */
    AfiliadoSaludDTO getAfiliadoSaludDTO(Long id);

}
