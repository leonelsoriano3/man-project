package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.service.api.IniciativaServiceAPI;
import co.com.fspb.mgs.dto.IniciativaDTO;

/**
 * Implementación de la Interfaz {@link IniciativaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class IniciativaServiceImpl implements IniciativaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private IniciativaDaoAPI iniciativaDao;
    @Autowired
    private UsuarioDaoAPI usuarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(IniciativaDTO iniciativaDTO) throws PmzException {

        Iniciativa iniciativaValidate = iniciativaDao.get(iniciativaDTO.getId());

        if (iniciativaValidate != null) {
            throw new PmzException("El iniciativa ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Iniciativa iniciativa = DTOTransformer.getIniciativaFromIniciativaDTO(new Iniciativa(), iniciativaDTO);

		if (iniciativaDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(iniciativaDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		iniciativa.setUsuario(usuario);
		}
		
        iniciativaDao.save(iniciativa);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<IniciativaDTO> getRecords(PmzPagingCriteria criteria) {
        List<Iniciativa> listaReturn = iniciativaDao.getRecords(criteria);
        Long countTotalRegistros = iniciativaDao.countRecords(criteria);

        List<IniciativaDTO> resultList = new ArrayList<IniciativaDTO>();
        for (Iniciativa iniciativa : listaReturn) {
            IniciativaDTO iniciativaDTO = DTOTransformer
                    .getIniciativaDTOFromIniciativa(iniciativa);
            resultList.add(iniciativaDTO);
        }
        
        return new PmzResultSet<IniciativaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(IniciativaDTO iniciativaDTO) throws PmzException{

        Iniciativa iniciativa = iniciativaDao.get(iniciativaDTO.getId());

        if (iniciativa == null) {
            throw new PmzException("El iniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getIniciativaFromIniciativaDTO(iniciativa, iniciativaDTO);

		if (iniciativaDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(iniciativaDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		iniciativa.setUsuario(usuario);
		}
		
        iniciativaDao.update(iniciativa);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public IniciativaDTO getIniciativaDTO(Long id) {
        Iniciativa iniciativa = iniciativaDao.get(id);
        return DTOTransformer.getIniciativaDTOFromIniciativa(iniciativa);
    }
}
