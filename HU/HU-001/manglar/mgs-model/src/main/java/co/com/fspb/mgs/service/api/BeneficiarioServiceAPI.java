package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Beneficiario}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServiceAPI
 * @date nov 11, 2016
 */
public interface BeneficiarioServiceAPI {

    /**
	 * Registra una entidad {@link Beneficiario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link BeneficiarioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(BeneficiarioDTO beneficiario) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Beneficiario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link BeneficiarioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(BeneficiarioDTO beneficiario) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<BeneficiarioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link BeneficiarioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link BeneficiarioDTO}
     */
    BeneficiarioDTO getBeneficiarioDTO(Long id);

}
