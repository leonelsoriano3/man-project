package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_traza_actividad en la BD a la entidad TrazaActividad.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividad
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_traza_actividad" )
public class TrazaActividad implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_traza", nullable = false)
	private Long id; 

	@Column(name = "fecha", nullable = true, length = 13)
    private Date fecha; 

	@Column(name = "usuario", nullable = true, length = 20)
    private String usuario; 

	@Column(name = "estado_actividad", nullable = true, length = 30)
    private String estadoActividad; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_actividad", nullable = true)
    private Actividad actividad; 

 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha != null ? new Date(fecha.getTime()) : null;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha != null ? new Date(fecha.getTime()) : null;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstadoActividad() {
        return estadoActividad;
    }

    public void setEstadoActividad(String estadoActividad) {
        this.estadoActividad = estadoActividad;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

}
