package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EstadoCivilDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoCivil;
import co.com.fspb.mgs.service.api.EstadoCivilServiceAPI;
import co.com.fspb.mgs.dto.EstadoCivilDTO;

/**
 * Implementación de la Interfaz {@link EstadoCivilServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoCivilServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EstadoCivilServiceImpl implements EstadoCivilServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EstadoCivilDaoAPI estadoCivilDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EstadoCivilDTO estadoCivilDTO) throws PmzException {

        EstadoCivil estadoCivilValidate = estadoCivilDao.get(estadoCivilDTO.getId());

        if (estadoCivilValidate != null) {
            throw new PmzException("El estadoCivil ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EstadoCivil estadoCivil = DTOTransformer.getEstadoCivilFromEstadoCivilDTO(new EstadoCivil(), estadoCivilDTO);

        estadoCivilDao.save(estadoCivil);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoCivilDTO> getRecords(PmzPagingCriteria criteria) {
        List<EstadoCivil> listaReturn = estadoCivilDao.getRecords(criteria);
        Long countTotalRegistros = estadoCivilDao.countRecords(criteria);

        List<EstadoCivilDTO> resultList = new ArrayList<EstadoCivilDTO>();
        for (EstadoCivil estadoCivil : listaReturn) {
            EstadoCivilDTO estadoCivilDTO = DTOTransformer
                    .getEstadoCivilDTOFromEstadoCivil(estadoCivil);
            resultList.add(estadoCivilDTO);
        }
        
        return new PmzResultSet<EstadoCivilDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EstadoCivilDTO estadoCivilDTO) throws PmzException{

        EstadoCivil estadoCivil = estadoCivilDao.get(estadoCivilDTO.getId());

        if (estadoCivil == null) {
            throw new PmzException("El estadoCivil no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEstadoCivilFromEstadoCivilDTO(estadoCivil, estadoCivilDTO);

        estadoCivilDao.update(estadoCivil);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EstadoCivilDTO getEstadoCivilDTO(Long id) {
        EstadoCivil estadoCivil = estadoCivilDao.get(id);
        return DTOTransformer.getEstadoCivilDTOFromEstadoCivil(estadoCivil);
    }
}
