package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.facade.api.CargoFacadeAPI;
import co.com.fspb.mgs.service.api.CargoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link CargoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class CargoFacadeImpl
 * @date nov 11, 2016
 */
@Service("cargoFacade")
public class CargoFacadeImpl implements CargoFacadeAPI {

    @Autowired
    private CargoServiceAPI cargoService;

    /**
     * @see co.com.fspb.mgs.facade.api.CargoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<CargoDTO> getRecords(PmzPagingCriteria criteria) {
        return cargoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.CargoFacadeAPI#guardar()
     */
    @Override
    public void guardar(CargoDTO cargo) throws PmzException {
        cargoService.guardar(cargo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.CargoFacadeAPI#editar()
     */
    @Override
    public void editar(CargoDTO cargo) throws PmzException {
        cargoService.editar(cargo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.CargoFacadeAPI#getCargoDTO()
     */
    @Override
    public CargoDTO getCargoDTO(Long id) {
        return cargoService.getCargoDTO(id);
    }

}
