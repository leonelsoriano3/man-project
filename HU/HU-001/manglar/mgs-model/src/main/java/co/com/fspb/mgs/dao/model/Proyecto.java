package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_proyecto en la BD a la entidad Proyecto.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Proyecto
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_proyecto" )
public class Proyecto implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_proyecto", nullable = false)
	private Long id; 

	@Column(name = "nombre", nullable = true, length = 50)
    private String nombre; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_rol_fundacion", nullable = false)
    private RolFundacion rolFundacion; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_resp", nullable = true)
    private Usuario usuario; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_iniciativa", nullable = true)
    private Iniciativa iniciativa; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 

    @Column(name = "ppto", nullable = true, precision = 131089, scale = 0)
    private Long ppto; 

	@Column(name = "poblacion_beneficiaria", nullable = true, length = 150)
    private String poblacionBeneficiaria; 

    @Column(name = "duracion", nullable = true, precision = 10, scale = 0)
    private Integer duracion; 

 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public RolFundacion getRolFundacion() {
        return rolFundacion;
    }

    public void setRolFundacion(RolFundacion rolFundacion) {
        this.rolFundacion = rolFundacion;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Iniciativa getIniciativa() {
        return iniciativa;
    }

    public void setIniciativa(Iniciativa iniciativa) {
        this.iniciativa = iniciativa;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getPpto() {
        return ppto;
    }

    public void setPpto(Long ppto) {
        this.ppto = ppto;
    }

    public String getPoblacionBeneficiaria() {
        return poblacionBeneficiaria;
    }

    public void setPoblacionBeneficiaria(String poblacionBeneficiaria) {
        this.poblacionBeneficiaria = poblacionBeneficiaria;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

}
