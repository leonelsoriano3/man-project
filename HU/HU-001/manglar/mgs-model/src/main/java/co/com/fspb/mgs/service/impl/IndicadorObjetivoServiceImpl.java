package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.IndicadorObjetivoDaoAPI;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.dao.model.IndicadorObjetivo;
import co.com.fspb.mgs.dao.model.IndicadorObjetivoPK;
import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.service.api.IndicadorObjetivoServiceAPI;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;

/**
 * Implementación de la Interfaz {@link IndicadorObjetivoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class IndicadorObjetivoServiceImpl implements IndicadorObjetivoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private IndicadorObjetivoDaoAPI indicadorObjetivoDao;
    @Autowired
    private ObjetivoDaoAPI objetivoDao;
    @Autowired
    private IndicadorDaoAPI indicadorDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(IndicadorObjetivoDTO indicadorObjetivoDTO) throws PmzException {


		IndicadorObjetivoPK pk = new IndicadorObjetivoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicadorObjetivoDTO.getIndicadorDTO()));
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), indicadorObjetivoDTO.getObjetivoDTO()));
        IndicadorObjetivo indicadorObjetivoValidate = indicadorObjetivoDao.get(pk);

        if (indicadorObjetivoValidate != null) {
            throw new PmzException("El indicadorObjetivo ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        IndicadorObjetivo indicadorObjetivo = DTOTransformer.getIndicadorObjetivoFromIndicadorObjetivoDTO(new IndicadorObjetivo(), indicadorObjetivoDTO);

		pk = indicadorObjetivo.getId();
		if (indicadorObjetivoDTO.getIndicadorDTO() != null) {
       		Indicador indicador = indicadorDao.get(indicadorObjetivoDTO.getIndicadorDTO().getId());
       		if (indicador == null) {
                throw new PmzException("El Indicador no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIndicador(indicador);
		}
		
		if (indicadorObjetivoDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(indicadorObjetivoDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setObjetivo(objetivo);
		}
		
        indicadorObjetivo.setId(pk);
        
        indicadorObjetivoDao.save(indicadorObjetivo);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorObjetivoDTO> getRecords(PmzPagingCriteria criteria) {
        List<IndicadorObjetivo> listaReturn = indicadorObjetivoDao.getRecords(criteria);
        Long countTotalRegistros = indicadorObjetivoDao.countRecords(criteria);

        List<IndicadorObjetivoDTO> resultList = new ArrayList<IndicadorObjetivoDTO>();
        for (IndicadorObjetivo indicadorObjetivo : listaReturn) {
            IndicadorObjetivoDTO indicadorObjetivoDTO = DTOTransformer
                    .getIndicadorObjetivoDTOFromIndicadorObjetivo(indicadorObjetivo);
            resultList.add(indicadorObjetivoDTO);
        }
        
        return new PmzResultSet<IndicadorObjetivoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(IndicadorObjetivoDTO indicadorObjetivoDTO) throws PmzException{


		IndicadorObjetivoPK pk = new IndicadorObjetivoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicadorObjetivoDTO.getIndicadorDTO()));
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), indicadorObjetivoDTO.getObjetivoDTO()));
        IndicadorObjetivo indicadorObjetivo = indicadorObjetivoDao.get(pk);

        if (indicadorObjetivo == null) {
            throw new PmzException("El indicadorObjetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getIndicadorObjetivoFromIndicadorObjetivoDTO(indicadorObjetivo, indicadorObjetivoDTO);

		pk = indicadorObjetivo.getId();
		if (indicadorObjetivoDTO.getIndicadorDTO() != null) {
       		Indicador indicador = indicadorDao.get(indicadorObjetivoDTO.getIndicadorDTO().getId());
       		if (indicador == null) {
                throw new PmzException("El Indicador no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIndicador(indicador);
		}
		
		if (indicadorObjetivoDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(indicadorObjetivoDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setObjetivo(objetivo);
		}
		
        indicadorObjetivo.setId(pk);
        
        indicadorObjetivoDao.update(indicadorObjetivo);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public IndicadorObjetivoDTO getIndicadorObjetivoDTO(IndicadorDTO indicador, ObjetivoDTO objetivo) {

		IndicadorObjetivoPK pk = new IndicadorObjetivoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicador));
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), objetivo));
        IndicadorObjetivo indicadorObjetivo = indicadorObjetivoDao.get(pk);
        return DTOTransformer.getIndicadorObjetivoDTOFromIndicadorObjetivo(indicadorObjetivo);
    }
}
