package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Objetivo}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoServiceAPI
 * @date nov 11, 2016
 */
public interface ObjetivoServiceAPI {

    /**
	 * Registra una entidad {@link Objetivo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivo - {@link ObjetivoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ObjetivoDTO objetivo) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Objetivo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivo - {@link ObjetivoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ObjetivoDTO objetivo) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ObjetivoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ObjetivoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ObjetivoDTO}
     */
    ObjetivoDTO getObjetivoDTO(Long id);

}
