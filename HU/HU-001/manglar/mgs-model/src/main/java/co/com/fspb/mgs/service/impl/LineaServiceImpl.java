package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.LineaDaoAPI;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dao.model.Linea;
import co.com.fspb.mgs.service.api.LineaServiceAPI;
import co.com.fspb.mgs.dto.LineaDTO;

/**
 * Implementación de la Interfaz {@link LineaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class LineaServiceImpl implements LineaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private LineaDaoAPI lineaDao;
    @Autowired
    private ObjetivoDaoAPI objetivoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(LineaDTO lineaDTO) throws PmzException {

        Linea lineaValidate = lineaDao.get(lineaDTO.getId());

        if (lineaValidate != null) {
            throw new PmzException("El linea ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Linea linea = DTOTransformer.getLineaFromLineaDTO(new Linea(), lineaDTO);

		if (lineaDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(lineaDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		linea.setObjetivo(objetivo);
		}
		
        lineaDao.save(linea);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<LineaDTO> getRecords(PmzPagingCriteria criteria) {
        List<Linea> listaReturn = lineaDao.getRecords(criteria);
        Long countTotalRegistros = lineaDao.countRecords(criteria);

        List<LineaDTO> resultList = new ArrayList<LineaDTO>();
        for (Linea linea : listaReturn) {
            LineaDTO lineaDTO = DTOTransformer
                    .getLineaDTOFromLinea(linea);
            resultList.add(lineaDTO);
        }
        
        return new PmzResultSet<LineaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(LineaDTO lineaDTO) throws PmzException{

        Linea linea = lineaDao.get(lineaDTO.getId());

        if (linea == null) {
            throw new PmzException("El linea no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getLineaFromLineaDTO(linea, lineaDTO);

		if (lineaDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(lineaDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		linea.setObjetivo(objetivo);
		}
		
        lineaDao.update(linea);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public LineaDTO getLineaDTO(Long id) {
        Linea linea = lineaDao.get(id);
        return DTOTransformer.getLineaDTOFromLinea(linea);
    }
}
