package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LineaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Linea}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaServiceAPI
 * @date nov 11, 2016
 */
public interface LineaServiceAPI {

    /**
	 * Registra una entidad {@link Linea} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param linea - {@link LineaDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(LineaDTO linea) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Linea} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param linea - {@link LineaDTO}
	 * @throws {@link PmzException}
	 */
	void editar(LineaDTO linea) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<LineaDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link LineaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link LineaDTO}
     */
    LineaDTO getLineaDTO(Long id);

}
