package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.facade.api.TerritorioFacadeAPI;
import co.com.fspb.mgs.service.api.TerritorioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TerritorioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TerritorioFacadeImpl
 * @date nov 11, 2016
 */
@Service("territorioFacade")
public class TerritorioFacadeImpl implements TerritorioFacadeAPI {

    @Autowired
    private TerritorioServiceAPI territorioService;

    /**
     * @see co.com.fspb.mgs.facade.api.TerritorioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        return territorioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TerritorioFacadeAPI#guardar()
     */
    @Override
    public void guardar(TerritorioDTO territorio) throws PmzException {
        territorioService.guardar(territorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TerritorioFacadeAPI#editar()
     */
    @Override
    public void editar(TerritorioDTO territorio) throws PmzException {
        territorioService.editar(territorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TerritorioFacadeAPI#getTerritorioDTO()
     */
    @Override
    public TerritorioDTO getTerritorioDTO(Long id) {
        return territorioService.getTerritorioDTO(id);
    }

}
