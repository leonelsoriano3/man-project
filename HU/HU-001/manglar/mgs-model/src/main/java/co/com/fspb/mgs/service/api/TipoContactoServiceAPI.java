package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link TipoContacto}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoServiceAPI
 * @date nov 11, 2016
 */
public interface TipoContactoServiceAPI {

    /**
	 * Registra una entidad {@link TipoContacto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoContacto - {@link TipoContactoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(TipoContactoDTO tipoContacto) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link TipoContacto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoContacto - {@link TipoContactoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(TipoContactoDTO tipoContacto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TipoContactoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link TipoContactoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TipoContactoDTO}
     */
    TipoContactoDTO getTipoContactoDTO(Long id);

}
