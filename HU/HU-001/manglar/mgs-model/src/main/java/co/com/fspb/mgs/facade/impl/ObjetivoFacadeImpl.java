package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI;
import co.com.fspb.mgs.service.api.ObjetivoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ObjetivoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoFacadeImpl
 * @date nov 11, 2016
 */
@Service("objetivoFacade")
public class ObjetivoFacadeImpl implements ObjetivoFacadeAPI {

    @Autowired
    private ObjetivoServiceAPI objetivoService;

    /**
     * @see co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ObjetivoDTO> getRecords(PmzPagingCriteria criteria) {
        return objetivoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI#guardar()
     */
    @Override
    public void guardar(ObjetivoDTO objetivo) throws PmzException {
        objetivoService.guardar(objetivo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI#editar()
     */
    @Override
    public void editar(ObjetivoDTO objetivo) throws PmzException {
        objetivoService.editar(objetivo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI#getObjetivoDTO()
     */
    @Override
    public ObjetivoDTO getObjetivoDTO(Long id) {
        return objetivoService.getObjetivoDTO(id);
    }

}
