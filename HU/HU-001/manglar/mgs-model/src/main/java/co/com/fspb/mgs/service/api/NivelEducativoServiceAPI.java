package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link NivelEducativo}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoServiceAPI
 * @date nov 11, 2016
 */
public interface NivelEducativoServiceAPI {

    /**
	 * Registra una entidad {@link NivelEducativo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param nivelEducativo - {@link NivelEducativoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(NivelEducativoDTO nivelEducativo) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link NivelEducativo} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param nivelEducativo - {@link NivelEducativoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(NivelEducativoDTO nivelEducativo) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<NivelEducativoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link NivelEducativoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link NivelEducativoDTO}
     */
    NivelEducativoDTO getNivelEducativoDTO(Long id);

}
