package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.facade.api.PersonaFacadeAPI;
import co.com.fspb.mgs.service.api.PersonaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link PersonaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaFacadeImpl
 * @date nov 11, 2016
 */
@Service("personaFacade")
public class PersonaFacadeImpl implements PersonaFacadeAPI {

    @Autowired
    private PersonaServiceAPI personaService;

    /**
     * @see co.com.fspb.mgs.facade.api.PersonaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<PersonaDTO> getRecords(PmzPagingCriteria criteria) {
        return personaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.PersonaFacadeAPI#guardar()
     */
    @Override
    public void guardar(PersonaDTO persona) throws PmzException {
        personaService.guardar(persona);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.PersonaFacadeAPI#editar()
     */
    @Override
    public void editar(PersonaDTO persona) throws PmzException {
        personaService.editar(persona);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.PersonaFacadeAPI#getPersonaDTO()
     */
    @Override
    public PersonaDTO getPersonaDTO(Long id) {
        return personaService.getPersonaDTO(id);
    }

}
