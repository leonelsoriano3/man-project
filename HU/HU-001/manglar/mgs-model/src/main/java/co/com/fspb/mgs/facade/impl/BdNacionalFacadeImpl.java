package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI;
import co.com.fspb.mgs.service.api.BdNacionalServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link BdNacionalFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BdNacionalFacadeImpl
 * @date nov 11, 2016
 */
@Service("bdNacionalFacade")
public class BdNacionalFacadeImpl implements BdNacionalFacadeAPI {

    @Autowired
    private BdNacionalServiceAPI bdNacionalService;

    /**
     * @see co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<BdNacionalDTO> getRecords(PmzPagingCriteria criteria) {
        return bdNacionalService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI#guardar()
     */
    @Override
    public void guardar(BdNacionalDTO bdNacional) throws PmzException {
        bdNacionalService.guardar(bdNacional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI#editar()
     */
    @Override
    public void editar(BdNacionalDTO bdNacional) throws PmzException {
        bdNacionalService.editar(bdNacional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI#getBdNacionalDTO()
     */
    @Override
    public BdNacionalDTO getBdNacionalDTO(Long id) {
        return bdNacionalService.getBdNacionalDTO(id);
    }

}
