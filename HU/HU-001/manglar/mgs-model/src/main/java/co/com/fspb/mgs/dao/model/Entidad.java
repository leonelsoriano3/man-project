package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_entidad en la BD a la entidad Entidad.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Entidad
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_entidad" )
public class Entidad implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_entidad", nullable = false)
	private Long id; 

	@Column(name = "tipo_id", nullable = true, length = 30)
    @Enumerated(EnumType.STRING)
    private TipoIdEnum tipoId; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona_contacto", nullable = true)
    private Persona contacto; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_representante_legal", nullable = false)
    private Persona persona;

	@Column(name = "fecha_constitucion", nullable = false, length = 13)
    private Date fechaConstitucion; 

	@Column(name = "info_convocatorias", nullable = false, length = 2000)
    private String infoConvocatorias; 

	@Column(name = "nombre", nullable = true, length = 250)
    private String nombre; 


	@Column(name = "num_id", nullable = true, length = 30)
    private String numId; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "certificaciones", nullable = false, length = 250)
    private String certificaciones; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 

	@Column(name = "tipo_entidad", nullable = true, length = 30)
    private String tipoEntidad; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoIdEnum getTipoId() {
        return tipoId;
    }

    public void setTipoId(TipoIdEnum tipoId) {
        this.tipoId = tipoId;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public Persona getContacto() {
        return contacto;
    }

    public void setContacto(Persona persona) {
        this.contacto = persona;
    }

    public Date getFechaConstitucion() {
        return fechaConstitucion != null ? new Date(fechaConstitucion.getTime()) : null;
    }

    public void setFechaConstitucion(Date fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion != null ? new Date(fechaConstitucion.getTime()) : null;
    }

    public String getInfoConvocatorias() {
        return infoConvocatorias;
    }

    public void setInfoConvocatorias(String infoConvocatorias) {
        this.infoConvocatorias = infoConvocatorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getCertificaciones() {
        return certificaciones;
    }

    public void setCertificaciones(String certificaciones) {
        this.certificaciones = certificaciones;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

}
