package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoTerritorioDTO;
import co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI;
import co.com.fspb.mgs.service.api.TipoTerritorioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TipoTerritorioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTerritorioFacadeImpl
 * @date nov 11, 2016
 */
@Service("tipoTerritorioFacade")
public class TipoTerritorioFacadeImpl implements TipoTerritorioFacadeAPI {

    @Autowired
    private TipoTerritorioServiceAPI tipoTerritorioService;

    /**
     * @see co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        return tipoTerritorioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI#guardar()
     */
    @Override
    public void guardar(TipoTerritorioDTO tipoTerritorio) throws PmzException {
        tipoTerritorioService.guardar(tipoTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI#editar()
     */
    @Override
    public void editar(TipoTerritorioDTO tipoTerritorio) throws PmzException {
        tipoTerritorioService.editar(tipoTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI#getTipoTerritorioDTO()
     */
    @Override
    public TipoTerritorioDTO getTipoTerritorioDTO(Long id) {
        return tipoTerritorioService.getTipoTerritorioDTO(id);
    }

}
