package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.OcupacionDaoAPI;
import co.com.fspb.mgs.dao.model.Ocupacion;
import co.com.fspb.mgs.service.api.OcupacionServiceAPI;
import co.com.fspb.mgs.dto.OcupacionDTO;

/**
 * Implementación de la Interfaz {@link OcupacionServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class OcupacionServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class OcupacionServiceImpl implements OcupacionServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private OcupacionDaoAPI ocupacionDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(OcupacionDTO ocupacionDTO) throws PmzException {

        Ocupacion ocupacionValidate = ocupacionDao.get(ocupacionDTO.getId());

        if (ocupacionValidate != null) {
            throw new PmzException("El ocupacion ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Ocupacion ocupacion = DTOTransformer.getOcupacionFromOcupacionDTO(new Ocupacion(), ocupacionDTO);

        ocupacionDao.save(ocupacion);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<OcupacionDTO> getRecords(PmzPagingCriteria criteria) {
        List<Ocupacion> listaReturn = ocupacionDao.getRecords(criteria);
        Long countTotalRegistros = ocupacionDao.countRecords(criteria);

        List<OcupacionDTO> resultList = new ArrayList<OcupacionDTO>();
        for (Ocupacion ocupacion : listaReturn) {
            OcupacionDTO ocupacionDTO = DTOTransformer
                    .getOcupacionDTOFromOcupacion(ocupacion);
            resultList.add(ocupacionDTO);
        }
        
        return new PmzResultSet<OcupacionDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(OcupacionDTO ocupacionDTO) throws PmzException{

        Ocupacion ocupacion = ocupacionDao.get(ocupacionDTO.getId());

        if (ocupacion == null) {
            throw new PmzException("El ocupacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getOcupacionFromOcupacionDTO(ocupacion, ocupacionDTO);

        ocupacionDao.update(ocupacion);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public OcupacionDTO getOcupacionDTO(Long id) {
        Ocupacion ocupacion = ocupacionDao.get(id);
        return DTOTransformer.getOcupacionDTOFromOcupacion(ocupacion);
    }
}
