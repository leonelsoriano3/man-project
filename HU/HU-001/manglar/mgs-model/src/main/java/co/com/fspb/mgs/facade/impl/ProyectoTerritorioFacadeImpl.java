package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI;
import co.com.fspb.mgs.service.api.ProyectoTerritorioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ProyectoTerritorioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioFacadeImpl
 * @date nov 11, 2016
 */
@Service("proyectoTerritorioFacade")
public class ProyectoTerritorioFacadeImpl implements ProyectoTerritorioFacadeAPI {

    @Autowired
    private ProyectoTerritorioServiceAPI proyectoTerritorioService;

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        return proyectoTerritorioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI#guardar()
     */
    @Override
    public void guardar(ProyectoTerritorioDTO proyectoTerritorio) throws PmzException {
        proyectoTerritorioService.guardar(proyectoTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI#editar()
     */
    @Override
    public void editar(ProyectoTerritorioDTO proyectoTerritorio) throws PmzException {
        proyectoTerritorioService.editar(proyectoTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI#getProyectoTerritorioDTO()
     */
    @Override
    public ProyectoTerritorioDTO getProyectoTerritorioDTO(TerritorioDTO territorio, ProyectoDTO proyecto) {
        return proyectoTerritorioService.getProyectoTerritorioDTO(territorio, proyecto);
    }

}
