package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.facade.api.IniciativaFacadeAPI;
import co.com.fspb.mgs.service.api.IniciativaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link IniciativaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaFacadeImpl
 * @date nov 11, 2016
 */
@Service("iniciativaFacade")
public class IniciativaFacadeImpl implements IniciativaFacadeAPI {

    @Autowired
    private IniciativaServiceAPI iniciativaService;

    /**
     * @see co.com.fspb.mgs.facade.api.IniciativaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<IniciativaDTO> getRecords(PmzPagingCriteria criteria) {
        return iniciativaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.IniciativaFacadeAPI#guardar()
     */
    @Override
    public void guardar(IniciativaDTO iniciativa) throws PmzException {
        iniciativaService.guardar(iniciativa);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IniciativaFacadeAPI#editar()
     */
    @Override
    public void editar(IniciativaDTO iniciativa) throws PmzException {
        iniciativaService.editar(iniciativa);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IniciativaFacadeAPI#getIniciativaDTO()
     */
    @Override
    public IniciativaDTO getIniciativaDTO(Long id) {
        return iniciativaService.getIniciativaDTO(id);
    }

}
