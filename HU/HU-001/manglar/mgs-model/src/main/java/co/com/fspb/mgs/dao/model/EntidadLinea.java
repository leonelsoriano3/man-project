package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_entidad_linea en la BD a la entidad EntidadLinea.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLinea
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_entidad_linea" )
public class EntidadLinea implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private EntidadLineaPK id; 


 
    public EntidadLineaPK getId() {
        return id;
    }

    public void setId(EntidadLineaPK id) {
        this.id = id;
    }

}
