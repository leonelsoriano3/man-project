package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.facade.api.LiderFacadeAPI;
import co.com.fspb.mgs.service.api.LiderServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link LiderFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderFacadeImpl
 * @date nov 11, 2016
 */
@Service("liderFacade")
public class LiderFacadeImpl implements LiderFacadeAPI {

    @Autowired
    private LiderServiceAPI liderService;

    /**
     * @see co.com.fspb.mgs.facade.api.LiderFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<LiderDTO> getRecords(PmzPagingCriteria criteria) {
        return liderService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.LiderFacadeAPI#guardar()
     */
    @Override
    public void guardar(LiderDTO lider) throws PmzException {
        liderService.guardar(lider);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LiderFacadeAPI#editar()
     */
    @Override
    public void editar(LiderDTO lider) throws PmzException {
        liderService.editar(lider);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LiderFacadeAPI#getLiderDTO()
     */
    @Override
    public LiderDTO getLiderDTO(Long id) {
        return liderService.getLiderDTO(id);
    }

}
