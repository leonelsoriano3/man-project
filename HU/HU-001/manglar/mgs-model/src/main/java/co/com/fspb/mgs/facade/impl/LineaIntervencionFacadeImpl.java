package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI;
import co.com.fspb.mgs.service.api.LineaIntervencionServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link LineaIntervencionFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaIntervencionFacadeImpl
 * @date nov 11, 2016
 */
@Service("lineaIntervencionFacade")
public class LineaIntervencionFacadeImpl implements LineaIntervencionFacadeAPI {

    @Autowired
    private LineaIntervencionServiceAPI lineaIntervencionService;

    /**
     * @see co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<LineaIntervencionDTO> getRecords(PmzPagingCriteria criteria) {
        return lineaIntervencionService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI#guardar()
     */
    @Override
    public void guardar(LineaIntervencionDTO lineaIntervencion) throws PmzException {
        lineaIntervencionService.guardar(lineaIntervencion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI#editar()
     */
    @Override
    public void editar(LineaIntervencionDTO lineaIntervencion) throws PmzException {
        lineaIntervencionService.editar(lineaIntervencion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI#getLineaIntervencionDTO()
     */
    @Override
    public LineaIntervencionDTO getLineaIntervencionDTO(Long id) {
        return lineaIntervencionService.getLineaIntervencionDTO(id);
    }

}
