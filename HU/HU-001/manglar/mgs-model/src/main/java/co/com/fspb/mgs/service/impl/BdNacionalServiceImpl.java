package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.BdNacionalDaoAPI;
import co.com.fspb.mgs.dao.model.BdNacional;
import co.com.fspb.mgs.service.api.BdNacionalServiceAPI;
import co.com.fspb.mgs.dto.BdNacionalDTO;

/**
 * Implementación de la Interfaz {@link BdNacionalServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BdNacionalServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class BdNacionalServiceImpl implements BdNacionalServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private BdNacionalDaoAPI bdNacionalDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(BdNacionalDTO bdNacionalDTO) throws PmzException {

        BdNacional bdNacionalValidate = bdNacionalDao.get(bdNacionalDTO.getId());

        if (bdNacionalValidate != null) {
            throw new PmzException("El bdNacional ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        BdNacional bdNacional = DTOTransformer.getBdNacionalFromBdNacionalDTO(new BdNacional(), bdNacionalDTO);

        bdNacionalDao.save(bdNacional);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<BdNacionalDTO> getRecords(PmzPagingCriteria criteria) {
        List<BdNacional> listaReturn = bdNacionalDao.getRecords(criteria);
        Long countTotalRegistros = bdNacionalDao.countRecords(criteria);

        List<BdNacionalDTO> resultList = new ArrayList<BdNacionalDTO>();
        for (BdNacional bdNacional : listaReturn) {
            BdNacionalDTO bdNacionalDTO = DTOTransformer
                    .getBdNacionalDTOFromBdNacional(bdNacional);
            resultList.add(bdNacionalDTO);
        }
        
        return new PmzResultSet<BdNacionalDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(BdNacionalDTO bdNacionalDTO) throws PmzException{

        BdNacional bdNacional = bdNacionalDao.get(bdNacionalDTO.getId());

        if (bdNacional == null) {
            throw new PmzException("El bdNacional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getBdNacionalFromBdNacionalDTO(bdNacional, bdNacionalDTO);

        bdNacionalDao.update(bdNacional);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public BdNacionalDTO getBdNacionalDTO(Long id) {
        BdNacional bdNacional = bdNacionalDao.get(id);
        return DTOTransformer.getBdNacionalDTOFromBdNacional(bdNacional);
    }
}
