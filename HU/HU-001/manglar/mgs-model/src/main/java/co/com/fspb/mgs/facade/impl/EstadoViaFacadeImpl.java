package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoViaDTO;
import co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI;
import co.com.fspb.mgs.service.api.EstadoViaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EstadoViaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoViaFacadeImpl
 * @date nov 11, 2016
 */
@Service("estadoViaFacade")
public class EstadoViaFacadeImpl implements EstadoViaFacadeAPI {

    @Autowired
    private EstadoViaServiceAPI estadoViaService;

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoViaDTO> getRecords(PmzPagingCriteria criteria) {
        return estadoViaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI#guardar()
     */
    @Override
    public void guardar(EstadoViaDTO estadoVia) throws PmzException {
        estadoViaService.guardar(estadoVia);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI#editar()
     */
    @Override
    public void editar(EstadoViaDTO estadoVia) throws PmzException {
        estadoViaService.editar(estadoVia);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI#getEstadoViaDTO()
     */
    @Override
    public EstadoViaDTO getEstadoViaDTO(Long id) {
        return estadoViaService.getEstadoViaDTO(id);
    }

}
