package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EntidadTerritorioDaoAPI;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dao.model.EntidadTerritorio;
import co.com.fspb.mgs.dao.model.EntidadTerritorioPK;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.service.api.EntidadTerritorioServiceAPI;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.EntidadDTO;

/**
 * Implementación de la Interfaz {@link EntidadTerritorioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EntidadTerritorioServiceImpl implements EntidadTerritorioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EntidadTerritorioDaoAPI entidadTerritorioDao;
    @Autowired
    private EntidadDaoAPI entidadDao;
    @Autowired
    private TerritorioDaoAPI territorioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EntidadTerritorioDTO entidadTerritorioDTO) throws PmzException {


		EntidadTerritorioPK pk = new EntidadTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), entidadTerritorioDTO.getTerritorioDTO()));
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidadTerritorioDTO.getEntidadDTO()));
        EntidadTerritorio entidadTerritorioValidate = entidadTerritorioDao.get(pk);

        if (entidadTerritorioValidate != null) {
            throw new PmzException("El entidadTerritorio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EntidadTerritorio entidadTerritorio = DTOTransformer.getEntidadTerritorioFromEntidadTerritorioDTO(new EntidadTerritorio(), entidadTerritorioDTO);

		pk = entidadTerritorio.getId();
		if (entidadTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(entidadTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (entidadTerritorioDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(entidadTerritorioDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setEntidad(entidad);
		}
		
        entidadTerritorio.setId(pk);
        
        entidadTerritorioDao.save(entidadTerritorio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        List<EntidadTerritorio> listaReturn = entidadTerritorioDao.getRecords(criteria);
        Long countTotalRegistros = entidadTerritorioDao.countRecords(criteria);

        List<EntidadTerritorioDTO> resultList = new ArrayList<EntidadTerritorioDTO>();
        for (EntidadTerritorio entidadTerritorio : listaReturn) {
            EntidadTerritorioDTO entidadTerritorioDTO = DTOTransformer
                    .getEntidadTerritorioDTOFromEntidadTerritorio(entidadTerritorio);
            resultList.add(entidadTerritorioDTO);
        }
        
        return new PmzResultSet<EntidadTerritorioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EntidadTerritorioDTO entidadTerritorioDTO) throws PmzException{


		EntidadTerritorioPK pk = new EntidadTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), entidadTerritorioDTO.getTerritorioDTO()));
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidadTerritorioDTO.getEntidadDTO()));
        EntidadTerritorio entidadTerritorio = entidadTerritorioDao.get(pk);

        if (entidadTerritorio == null) {
            throw new PmzException("El entidadTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEntidadTerritorioFromEntidadTerritorioDTO(entidadTerritorio, entidadTerritorioDTO);

		pk = entidadTerritorio.getId();
		if (entidadTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(entidadTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (entidadTerritorioDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(entidadTerritorioDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setEntidad(entidad);
		}
		
        entidadTerritorio.setId(pk);
        
        entidadTerritorioDao.update(entidadTerritorio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EntidadTerritorioDTO getEntidadTerritorioDTO(TerritorioDTO territorio, EntidadDTO entidad) {

		EntidadTerritorioPK pk = new EntidadTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), territorio));
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidad));
        EntidadTerritorio entidadTerritorio = entidadTerritorioDao.get(pk);
        return DTOTransformer.getEntidadTerritorioDTOFromEntidadTerritorio(entidadTerritorio);
    }
}
