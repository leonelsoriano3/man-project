package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.LiderDaoAPI;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.model.Lider;
import co.com.fspb.mgs.service.api.LiderServiceAPI;
import co.com.fspb.mgs.dto.LiderDTO;

/**
 * Implementación de la Interfaz {@link LiderServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class LiderServiceImpl implements LiderServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private LiderDaoAPI liderDao;
    @Autowired
    private PersonaDaoAPI personaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(LiderDTO liderDTO) throws PmzException {

        Lider liderValidate = liderDao.get(liderDTO.getId());

        if (liderValidate != null) {
            throw new PmzException("El lider ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Lider lider = DTOTransformer.getLiderFromLiderDTO(new Lider(), liderDTO);

		if (liderDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(liderDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		lider.setPersona(persona);
		}
		
        liderDao.save(lider);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<LiderDTO> getRecords(PmzPagingCriteria criteria) {
        List<Lider> listaReturn = liderDao.getRecords(criteria);
        Long countTotalRegistros = liderDao.countRecords(criteria);

        List<LiderDTO> resultList = new ArrayList<LiderDTO>();
        for (Lider lider : listaReturn) {
            LiderDTO liderDTO = DTOTransformer
                    .getLiderDTOFromLider(lider);
            resultList.add(liderDTO);
        }
        
        return new PmzResultSet<LiderDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(LiderDTO liderDTO) throws PmzException{

        Lider lider = liderDao.get(liderDTO.getId());

        if (lider == null) {
            throw new PmzException("El lider no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getLiderFromLiderDTO(lider, liderDTO);

		if (liderDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(liderDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		lider.setPersona(persona);
		}
		
        liderDao.update(lider);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public LiderDTO getLiderDTO(Long id) {
        Lider lider = liderDao.get(id);
        return DTOTransformer.getLiderDTOFromLider(lider);
    }
}
