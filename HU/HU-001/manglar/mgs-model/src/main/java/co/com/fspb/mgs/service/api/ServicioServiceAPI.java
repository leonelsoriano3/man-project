package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ServicioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Servicio}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioServiceAPI
 * @date nov 11, 2016
 */
public interface ServicioServiceAPI {

    /**
	 * Registra una entidad {@link Servicio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param servicio - {@link ServicioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ServicioDTO servicio) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Servicio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param servicio - {@link ServicioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ServicioDTO servicio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ServicioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ServicioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ServicioDTO}
     */
    ServicioDTO getServicioDTO(Long id);

}
