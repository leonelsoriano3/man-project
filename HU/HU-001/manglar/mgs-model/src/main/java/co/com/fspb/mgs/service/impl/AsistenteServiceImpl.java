package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.AsistenteDaoAPI;
import co.com.fspb.mgs.dao.api.ProyectoBeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.ProyectoBeneficiario;
import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.dao.model.Actividad;
import co.com.fspb.mgs.dao.model.Asistente;
import co.com.fspb.mgs.service.api.AsistenteServiceAPI;
import co.com.fspb.mgs.dto.AsistenteDTO;

/**
 * Implementación de la Interfaz {@link AsistenteServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AsistenteServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class AsistenteServiceImpl implements AsistenteServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private AsistenteDaoAPI asistenteDao;
    @Autowired
    private ProyectoBeneficiarioDaoAPI proyectoBeneficiarioDao;
    @Autowired
    private ActividadDaoAPI actividadDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(AsistenteDTO asistenteDTO) throws PmzException {

        Asistente asistenteValidate = asistenteDao.get(asistenteDTO.getId());

        if (asistenteValidate != null) {
            throw new PmzException("El asistente ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Asistente asistente = DTOTransformer.getAsistenteFromAsistenteDTO(new Asistente(), asistenteDTO);

		if (asistenteDTO.getProyectoBeneficiarioDTO() != null) {
       		ProyectoBeneficiario proyectoBeneficiario = proyectoBeneficiarioDao.get(asistenteDTO.getProyectoBeneficiarioDTO().getId());
       		if (proyectoBeneficiario == null) {
                throw new PmzException("El ProyectoBeneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		asistente.setProyectoBeneficiario(proyectoBeneficiario);
		}
		
		if (asistenteDTO.getActividadDTO() != null) {
       		Actividad actividad = actividadDao.get(asistenteDTO.getActividadDTO().getId());
       		if (actividad == null) {
                throw new PmzException("El Actividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		asistente.setActividad(actividad);
		}
		
        asistenteDao.save(asistente);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<AsistenteDTO> getRecords(PmzPagingCriteria criteria) {
        List<Asistente> listaReturn = asistenteDao.getRecords(criteria);
        Long countTotalRegistros = asistenteDao.countRecords(criteria);

        List<AsistenteDTO> resultList = new ArrayList<AsistenteDTO>();
        for (Asistente asistente : listaReturn) {
            AsistenteDTO asistenteDTO = DTOTransformer
                    .getAsistenteDTOFromAsistente(asistente);
            resultList.add(asistenteDTO);
        }
        
        return new PmzResultSet<AsistenteDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(AsistenteDTO asistenteDTO) throws PmzException{

        Asistente asistente = asistenteDao.get(asistenteDTO.getId());

        if (asistente == null) {
            throw new PmzException("El asistente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getAsistenteFromAsistenteDTO(asistente, asistenteDTO);

		if (asistenteDTO.getProyectoBeneficiarioDTO() != null) {
       		ProyectoBeneficiario proyectoBeneficiario = proyectoBeneficiarioDao.get(asistenteDTO.getProyectoBeneficiarioDTO().getId());
       		if (proyectoBeneficiario == null) {
                throw new PmzException("El ProyectoBeneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		asistente.setProyectoBeneficiario(proyectoBeneficiario);
		}
		
		if (asistenteDTO.getActividadDTO() != null) {
       		Actividad actividad = actividadDao.get(asistenteDTO.getActividadDTO().getId());
       		if (actividad == null) {
                throw new PmzException("El Actividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		asistente.setActividad(actividad);
		}
		
        asistenteDao.update(asistente);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public AsistenteDTO getAsistenteDTO(Long id) {
        Asistente asistente = asistenteDao.get(id);
        return DTOTransformer.getAsistenteDTOFromAsistente(asistente);
    }
}
