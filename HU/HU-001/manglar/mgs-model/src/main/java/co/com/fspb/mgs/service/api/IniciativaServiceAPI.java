package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IniciativaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Iniciativa}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaServiceAPI
 * @date nov 11, 2016
 */
public interface IniciativaServiceAPI {

    /**
	 * Registra una entidad {@link Iniciativa} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param iniciativa - {@link IniciativaDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(IniciativaDTO iniciativa) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Iniciativa} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param iniciativa - {@link IniciativaDTO}
	 * @throws {@link PmzException}
	 */
	void editar(IniciativaDTO iniciativa) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<IniciativaDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link IniciativaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link IniciativaDTO}
     */
    IniciativaDTO getIniciativaDTO(Long id);

}
