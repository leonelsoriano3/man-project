package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.facade.api.ServicioFacadeAPI;
import co.com.fspb.mgs.service.api.ServicioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ServicioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioFacadeImpl
 * @date nov 11, 2016
 */
@Service("servicioFacade")
public class ServicioFacadeImpl implements ServicioFacadeAPI {

    @Autowired
    private ServicioServiceAPI servicioService;

    /**
     * @see co.com.fspb.mgs.facade.api.ServicioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ServicioDTO> getRecords(PmzPagingCriteria criteria) {
        return servicioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ServicioFacadeAPI#guardar()
     */
    @Override
    public void guardar(ServicioDTO servicio) throws PmzException {
        servicioService.guardar(servicio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ServicioFacadeAPI#editar()
     */
    @Override
    public void editar(ServicioDTO servicio) throws PmzException {
        servicioService.editar(servicio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ServicioFacadeAPI#getServicioDTO()
     */
    @Override
    public ServicioDTO getServicioDTO(Long id) {
        return servicioService.getServicioDTO(id);
    }

}
