package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EstadoActividadDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoActividad;
import co.com.fspb.mgs.service.api.EstadoActividadServiceAPI;
import co.com.fspb.mgs.dto.EstadoActividadDTO;

/**
 * Implementación de la Interfaz {@link EstadoActividadServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoActividadServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EstadoActividadServiceImpl implements EstadoActividadServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EstadoActividadDaoAPI estadoActividadDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EstadoActividadDTO estadoActividadDTO) throws PmzException {

        EstadoActividad estadoActividadValidate = estadoActividadDao.get(estadoActividadDTO.getId());

        if (estadoActividadValidate != null) {
            throw new PmzException("El estadoActividad ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EstadoActividad estadoActividad = DTOTransformer.getEstadoActividadFromEstadoActividadDTO(new EstadoActividad(), estadoActividadDTO);

        estadoActividadDao.save(estadoActividad);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoActividadDTO> getRecords(PmzPagingCriteria criteria) {
        List<EstadoActividad> listaReturn = estadoActividadDao.getRecords(criteria);
        Long countTotalRegistros = estadoActividadDao.countRecords(criteria);

        List<EstadoActividadDTO> resultList = new ArrayList<EstadoActividadDTO>();
        for (EstadoActividad estadoActividad : listaReturn) {
            EstadoActividadDTO estadoActividadDTO = DTOTransformer
                    .getEstadoActividadDTOFromEstadoActividad(estadoActividad);
            resultList.add(estadoActividadDTO);
        }
        
        return new PmzResultSet<EstadoActividadDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EstadoActividadDTO estadoActividadDTO) throws PmzException{

        EstadoActividad estadoActividad = estadoActividadDao.get(estadoActividadDTO.getId());

        if (estadoActividad == null) {
            throw new PmzException("El estadoActividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEstadoActividadFromEstadoActividadDTO(estadoActividad, estadoActividadDTO);

        estadoActividadDao.update(estadoActividad);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EstadoActividadDTO getEstadoActividadDTO(Long id) {
        EstadoActividad estadoActividad = estadoActividadDao.get(id);
        return DTOTransformer.getEstadoActividadDTOFromEstadoActividad(estadoActividad);
    }
}
