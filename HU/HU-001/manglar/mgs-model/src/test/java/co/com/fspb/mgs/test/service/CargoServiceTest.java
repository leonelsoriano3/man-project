package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.dao.api.CargoDaoAPI;
import co.com.fspb.mgs.service.api.CargoServiceAPI;
import co.com.fspb.mgs.service.impl.CargoServiceImpl;
import co.com.fspb.mgs.test.service.data.CargoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link CargoServiceAPI} de la entidad {@link Cargo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class CargoServiceAPI
 * @date nov 11, 2016
 */
public class CargoServiceTest {
	
    private static final Long ID_PRIMER_CARGO = 1;

    @InjectMocks
    private CargoServiceAPI cargoServiceAPI = new CargoServiceImpl();

    @Mock
    private CargoDaoAPI cargoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(CargoServiceTestData.getCargos()).when(cargoDaoAPI).getRecords(null);
        Mockito.doReturn(CargoServiceTestData.getTotalCargos()).when(cargoDaoAPI).countRecords(null);
        PmzResultSet<CargoDTO> cargos = cargoServiceAPI.getRecords(null);
        Assert.assertEquals(CargoServiceTestData.getTotalCargos(), cargos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link CargoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_cargo_existe(){
        Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(ID_PRIMER_CARGO);
        CargoDTO cargo = cargoServiceAPI.getCargoDTO(ID_PRIMER_CARGO);
        Assert.assertEquals(CargoServiceTestData.getCargo().getId(), cargo.getId());
    }

    /**
     * Prueba unitaria de get con {@link CargoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_cargo_no_existe(){
        Mockito.doReturn(null).when(cargoDaoAPI).get(ID_PRIMER_CARGO);
        CargoDTO cargo = cargoServiceAPI.getCargoDTO(ID_PRIMER_CARGO);
        Assert.assertEquals(null, cargo);
    }

    /**
     * Prueba unitaria de registro de una {@link CargoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_cargo_no_existe() throws PmzException{
    	cargoServiceAPI.guardar(CargoServiceTestData.getCargoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link CargoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_cargo_existe() throws PmzException{
        Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(ID_PRIMER_CARGO);
        cargoServiceAPI.guardar(CargoServiceTestData.getCargoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link CargoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_cargo_existe() throws PmzException{
        Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(ID_PRIMER_CARGO);
        cargoServiceAPI.editar(CargoServiceTestData.getCargoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link CargoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_cargo_no_existe() throws PmzException{
        cargoServiceAPI.editar(CargoServiceTestData.getCargoDTO());
    }
    
    
}
