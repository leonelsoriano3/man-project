package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Usuario}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class UsuarioServiceTestData
 * @date nov 11, 2016
 */
public abstract class UsuarioServiceTestData {
    
    private static List<Usuario> entities = new ArrayList<Usuario>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Usuario entity = new Usuario();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setNombreUsuario("NombreUsuario" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setPersona(PersonaServiceTestData.getPersona());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setCargo(CargoServiceTestData.getCargo());
            entity.setContrasena("Contrasena" + i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Usuario} Mock 
     * tranformado a DTO {@link UsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link UsuarioDTO}
     */
    public static UsuarioDTO getUsuarioDTO(){
        return DTOTransformer.getUsuarioDTOFromUsuario(getUsuario());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Usuario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Usuario} Mock
     */
    public static Usuario getUsuario(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Usuario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Usuario}
     */
    public static List<Usuario> getUsuarios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<UsuarioDTO> getResultSetUsuariosDTO(){
        List<UsuarioDTO> dtos = new ArrayList<UsuarioDTO>();
        for (Usuario entity : entities) {
            dtos.add(DTOTransformer.getUsuarioDTOFromUsuario(entity));
        }
        return new PmzResultSet<UsuarioDTO>(dtos, getTotalUsuarios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalUsuarios(){
        return Long.valueOf(entities.size());
    }

}
