package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI;
import co.com.fspb.mgs.facade.impl.ComponenteProyectoFacadeImpl;
import co.com.fspb.mgs.service.api.ComponenteProyectoServiceAPI;
import co.com.fspb.mgs.test.service.data.ComponenteProyectoServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.test.service.data.ComponenteServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ComponenteProyectoFacadeAPI} de la entidad {@link ComponenteProyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoFacadeTest
 * @date nov 11, 2016
 */
public class ComponenteProyectoFacadeTest {

    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();
    private static final ComponenteDTO COMPONENTEDTO = ComponenteServiceTestData.getComponenteDTO();

    @InjectMocks
    private ComponenteProyectoFacadeAPI componenteProyectoFacadeAPI = new ComponenteProyectoFacadeImpl();

    @Mock
    private ComponenteProyectoServiceAPI componenteProyectoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ComponenteProyectoServiceTestData.getResultSetComponenteProyectosDTO()).when(componenteProyectoServiceAPI).getRecords(null);
        PmzResultSet<ComponenteProyectoDTO> componenteProyectos = componenteProyectoFacadeAPI.getRecords(null);
        Assert.assertEquals(ComponenteProyectoServiceTestData.getTotalComponenteProyectos(), componenteProyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componenteProyecto_existe(){
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyectoDTO()).when(componenteProyectoServiceAPI).getComponenteProyectoDTO(PROYECTODTO, COMPONENTEDTO);
        ComponenteProyectoDTO componenteProyecto = componenteProyectoFacadeAPI.getComponenteProyectoDTO(PROYECTODTO, COMPONENTEDTO);
        Assert.assertEquals(ComponenteProyectoServiceTestData.getComponenteProyectoDTO().getId(), componenteProyecto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_componenteProyecto_no_existe() throws PmzException{
        componenteProyectoFacadeAPI.guardar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ComponenteProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_componenteProyecto_existe() throws PmzException{
        componenteProyectoFacadeAPI.editar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
}
