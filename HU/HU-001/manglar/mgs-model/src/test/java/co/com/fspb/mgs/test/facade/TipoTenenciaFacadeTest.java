package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI;
import co.com.fspb.mgs.facade.impl.TipoTenenciaFacadeImpl;
import co.com.fspb.mgs.service.api.TipoTenenciaServiceAPI;
import co.com.fspb.mgs.test.service.data.TipoTenenciaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TipoTenenciaFacadeAPI} de la entidad {@link TipoTenencia}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTenenciaFacadeTest
 * @date nov 11, 2016
 */
public class TipoTenenciaFacadeTest {

    private static final Long ID_PRIMER_TIPOTENENCIA = 1;

    @InjectMocks
    private TipoTenenciaFacadeAPI tipoTenenciaFacadeAPI = new TipoTenenciaFacadeImpl();

    @Mock
    private TipoTenenciaServiceAPI tipoTenenciaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoTenenciaServiceTestData.getResultSetTipoTenenciasDTO()).when(tipoTenenciaServiceAPI).getRecords(null);
        PmzResultSet<TipoTenenciaDTO> tipoTenencias = tipoTenenciaFacadeAPI.getRecords(null);
        Assert.assertEquals(TipoTenenciaServiceTestData.getTotalTipoTenencias(), tipoTenencias.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoTenenciaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTenencia_existe(){
        Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenenciaDTO()).when(tipoTenenciaServiceAPI).getTipoTenenciaDTO(ID_PRIMER_TIPOTENENCIA);
        TipoTenenciaDTO tipoTenencia = tipoTenenciaFacadeAPI.getTipoTenenciaDTO(ID_PRIMER_TIPOTENENCIA);
        Assert.assertEquals(TipoTenenciaServiceTestData.getTipoTenenciaDTO().getId(), tipoTenencia.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTenenciaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_tipoTenencia_no_existe() throws PmzException{
        tipoTenenciaFacadeAPI.guardar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TipoTenenciaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_tipoTenencia_existe() throws PmzException{
        tipoTenenciaFacadeAPI.editar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }
    
}
