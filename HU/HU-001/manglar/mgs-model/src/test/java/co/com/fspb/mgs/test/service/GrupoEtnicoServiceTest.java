package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.dao.api.GrupoEtnicoDaoAPI;
import co.com.fspb.mgs.service.api.GrupoEtnicoServiceAPI;
import co.com.fspb.mgs.service.impl.GrupoEtnicoServiceImpl;
import co.com.fspb.mgs.test.service.data.GrupoEtnicoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link GrupoEtnicoServiceAPI} de la entidad {@link GrupoEtnico}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class GrupoEtnicoServiceAPI
 * @date nov 11, 2016
 */
public class GrupoEtnicoServiceTest {
	
    private static final Long ID_PRIMER_GRUPOETNICO = 1;

    @InjectMocks
    private GrupoEtnicoServiceAPI grupoEtnicoServiceAPI = new GrupoEtnicoServiceImpl();

    @Mock
    private GrupoEtnicoDaoAPI grupoEtnicoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnicos()).when(grupoEtnicoDaoAPI).getRecords(null);
        Mockito.doReturn(GrupoEtnicoServiceTestData.getTotalGrupoEtnicos()).when(grupoEtnicoDaoAPI).countRecords(null);
        PmzResultSet<GrupoEtnicoDTO> grupoEtnicos = grupoEtnicoServiceAPI.getRecords(null);
        Assert.assertEquals(GrupoEtnicoServiceTestData.getTotalGrupoEtnicos(), grupoEtnicos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link GrupoEtnicoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_grupoEtnico_existe(){
        Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(ID_PRIMER_GRUPOETNICO);
        GrupoEtnicoDTO grupoEtnico = grupoEtnicoServiceAPI.getGrupoEtnicoDTO(ID_PRIMER_GRUPOETNICO);
        Assert.assertEquals(GrupoEtnicoServiceTestData.getGrupoEtnico().getId(), grupoEtnico.getId());
    }

    /**
     * Prueba unitaria de get con {@link GrupoEtnicoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_grupoEtnico_no_existe(){
        Mockito.doReturn(null).when(grupoEtnicoDaoAPI).get(ID_PRIMER_GRUPOETNICO);
        GrupoEtnicoDTO grupoEtnico = grupoEtnicoServiceAPI.getGrupoEtnicoDTO(ID_PRIMER_GRUPOETNICO);
        Assert.assertEquals(null, grupoEtnico);
    }

    /**
     * Prueba unitaria de registro de una {@link GrupoEtnicoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_grupoEtnico_no_existe() throws PmzException{
    	grupoEtnicoServiceAPI.guardar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link GrupoEtnicoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_grupoEtnico_existe() throws PmzException{
        Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(ID_PRIMER_GRUPOETNICO);
        grupoEtnicoServiceAPI.guardar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link GrupoEtnicoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_grupoEtnico_existe() throws PmzException{
        Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(ID_PRIMER_GRUPOETNICO);
        grupoEtnicoServiceAPI.editar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link GrupoEtnicoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_grupoEtnico_no_existe() throws PmzException{
        grupoEtnicoServiceAPI.editar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }
    
    
}
