package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.dao.api.EstructuraFamiliarDaoAPI;
import co.com.fspb.mgs.service.api.EstructuraFamiliarServiceAPI;
import co.com.fspb.mgs.service.impl.EstructuraFamiliarServiceImpl;
import co.com.fspb.mgs.test.service.data.EstructuraFamiliarServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EstructuraFamiliarServiceAPI} de la entidad {@link EstructuraFamiliar}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstructuraFamiliarServiceAPI
 * @date nov 11, 2016
 */
public class EstructuraFamiliarServiceTest {
	
    private static final Long ID_PRIMER_ESTRUCTURAFAMILIAR = 1;

    @InjectMocks
    private EstructuraFamiliarServiceAPI estructuraFamiliarServiceAPI = new EstructuraFamiliarServiceImpl();

    @Mock
    private EstructuraFamiliarDaoAPI estructuraFamiliarDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliars()).when(estructuraFamiliarDaoAPI).getRecords(null);
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getTotalEstructuraFamiliars()).when(estructuraFamiliarDaoAPI).countRecords(null);
        PmzResultSet<EstructuraFamiliarDTO> estructuraFamiliars = estructuraFamiliarServiceAPI.getRecords(null);
        Assert.assertEquals(EstructuraFamiliarServiceTestData.getTotalEstructuraFamiliars(), estructuraFamiliars.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstructuraFamiliarDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estructuraFamiliar_existe(){
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(ID_PRIMER_ESTRUCTURAFAMILIAR);
        EstructuraFamiliarDTO estructuraFamiliar = estructuraFamiliarServiceAPI.getEstructuraFamiliarDTO(ID_PRIMER_ESTRUCTURAFAMILIAR);
        Assert.assertEquals(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId(), estructuraFamiliar.getId());
    }

    /**
     * Prueba unitaria de get con {@link EstructuraFamiliarDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estructuraFamiliar_no_existe(){
        Mockito.doReturn(null).when(estructuraFamiliarDaoAPI).get(ID_PRIMER_ESTRUCTURAFAMILIAR);
        EstructuraFamiliarDTO estructuraFamiliar = estructuraFamiliarServiceAPI.getEstructuraFamiliarDTO(ID_PRIMER_ESTRUCTURAFAMILIAR);
        Assert.assertEquals(null, estructuraFamiliar);
    }

    /**
     * Prueba unitaria de registro de una {@link EstructuraFamiliarDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_estructuraFamiliar_no_existe() throws PmzException{
    	estructuraFamiliarServiceAPI.guardar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EstructuraFamiliarDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_estructuraFamiliar_existe() throws PmzException{
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(ID_PRIMER_ESTRUCTURAFAMILIAR);
        estructuraFamiliarServiceAPI.guardar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EstructuraFamiliarDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_estructuraFamiliar_existe() throws PmzException{
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(ID_PRIMER_ESTRUCTURAFAMILIAR);
        estructuraFamiliarServiceAPI.editar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EstructuraFamiliarDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_estructuraFamiliar_no_existe() throws PmzException{
        estructuraFamiliarServiceAPI.editar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }
    
    
}
