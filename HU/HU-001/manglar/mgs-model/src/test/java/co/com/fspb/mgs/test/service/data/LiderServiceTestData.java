package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Lider;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Lider}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderServiceTestData
 * @date nov 11, 2016
 */
public abstract class LiderServiceTestData {
    
    private static List<Lider> entities = new ArrayList<Lider>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Lider entity = new Lider();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setPersona(PersonaServiceTestData.getPersona());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setOrganizacionComun("OrganizacionComun" + i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Lider} Mock 
     * tranformado a DTO {@link LiderDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link LiderDTO}
     */
    public static LiderDTO getLiderDTO(){
        return DTOTransformer.getLiderDTOFromLider(getLider());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Lider} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Lider} Mock
     */
    public static Lider getLider(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Lider} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Lider}
     */
    public static List<Lider> getLiders(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<LiderDTO> getResultSetLidersDTO(){
        List<LiderDTO> dtos = new ArrayList<LiderDTO>();
        for (Lider entity : entities) {
            dtos.add(DTOTransformer.getLiderDTOFromLider(entity));
        }
        return new PmzResultSet<LiderDTO>(dtos, getTotalLiders(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalLiders(){
        return Long.valueOf(entities.size());
    }

}
