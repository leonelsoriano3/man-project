package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TipoVivienda;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TipoVivienda}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoViviendaServiceTestData
 * @date nov 11, 2016
 */
public abstract class TipoViviendaServiceTestData {
    
    private static List<TipoVivienda> entities = new ArrayList<TipoVivienda>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TipoVivienda entity = new TipoVivienda();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoVivienda} Mock 
     * tranformado a DTO {@link TipoViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TipoViviendaDTO}
     */
    public static TipoViviendaDTO getTipoViviendaDTO(){
        return DTOTransformer.getTipoViviendaDTOFromTipoVivienda(getTipoVivienda());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoVivienda} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TipoVivienda} Mock
     */
    public static TipoVivienda getTipoVivienda(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TipoVivienda} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TipoVivienda}
     */
    public static List<TipoVivienda> getTipoViviendas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TipoViviendaDTO> getResultSetTipoViviendasDTO(){
        List<TipoViviendaDTO> dtos = new ArrayList<TipoViviendaDTO>();
        for (TipoVivienda entity : entities) {
            dtos.add(DTOTransformer.getTipoViviendaDTOFromTipoVivienda(entity));
        }
        return new PmzResultSet<TipoViviendaDTO>(dtos, getTotalTipoViviendas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTipoViviendas(){
        return Long.valueOf(entities.size());
    }

}
