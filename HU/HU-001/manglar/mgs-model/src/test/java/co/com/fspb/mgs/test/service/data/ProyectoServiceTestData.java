package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Proyecto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoServiceTestData
 * @date nov 11, 2016
 */
public abstract class ProyectoServiceTestData {
    
    private static List<Proyecto> entities = new ArrayList<Proyecto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Proyecto entity = new Proyecto();

            entity.setId(i);
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setRolFundacion(RolFundacionServiceTestData.getRolFundacion());
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setUsuario(UsuarioServiceTestData.getUsuario());
            entity.setIniciativa(IniciativaServiceTestData.getIniciativa());
            entity.setFechaCrea(new Date());
            entity.setPpto(i);
            entity.setPoblacionBeneficiaria("PoblacionBeneficiaria" + i);
            entity.setDuracion(i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Proyecto} Mock 
     * tranformado a DTO {@link ProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ProyectoDTO}
     */
    public static ProyectoDTO getProyectoDTO(){
        return DTOTransformer.getProyectoDTOFromProyecto(getProyecto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Proyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Proyecto} Mock
     */
    public static Proyecto getProyecto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Proyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Proyecto}
     */
    public static List<Proyecto> getProyectos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ProyectoDTO> getResultSetProyectosDTO(){
        List<ProyectoDTO> dtos = new ArrayList<ProyectoDTO>();
        for (Proyecto entity : entities) {
            dtos.add(DTOTransformer.getProyectoDTOFromProyecto(entity));
        }
        return new PmzResultSet<ProyectoDTO>(dtos, getTotalProyectos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalProyectos(){
        return Long.valueOf(entities.size());
    }

}
