package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.dao.api.TipoAliadoDaoAPI;
import co.com.fspb.mgs.service.api.TipoAliadoServiceAPI;
import co.com.fspb.mgs.service.impl.TipoAliadoServiceImpl;
import co.com.fspb.mgs.test.service.data.TipoAliadoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TipoAliadoServiceAPI} de la entidad {@link TipoAliado}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoServiceAPI
 * @date nov 11, 2016
 */
public class TipoAliadoServiceTest {
	
    private static final Long ID_PRIMER_TIPOALIADO = 1;

    @InjectMocks
    private TipoAliadoServiceAPI tipoAliadoServiceAPI = new TipoAliadoServiceImpl();

    @Mock
    private TipoAliadoDaoAPI tipoAliadoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliados()).when(tipoAliadoDaoAPI).getRecords(null);
        Mockito.doReturn(TipoAliadoServiceTestData.getTotalTipoAliados()).when(tipoAliadoDaoAPI).countRecords(null);
        PmzResultSet<TipoAliadoDTO> tipoAliados = tipoAliadoServiceAPI.getRecords(null);
        Assert.assertEquals(TipoAliadoServiceTestData.getTotalTipoAliados(), tipoAliados.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoAliadoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoAliado_existe(){
        Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(ID_PRIMER_TIPOALIADO);
        TipoAliadoDTO tipoAliado = tipoAliadoServiceAPI.getTipoAliadoDTO(ID_PRIMER_TIPOALIADO);
        Assert.assertEquals(TipoAliadoServiceTestData.getTipoAliado().getId(), tipoAliado.getId());
    }

    /**
     * Prueba unitaria de get con {@link TipoAliadoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoAliado_no_existe(){
        Mockito.doReturn(null).when(tipoAliadoDaoAPI).get(ID_PRIMER_TIPOALIADO);
        TipoAliadoDTO tipoAliado = tipoAliadoServiceAPI.getTipoAliadoDTO(ID_PRIMER_TIPOALIADO);
        Assert.assertEquals(null, tipoAliado);
    }

    /**
     * Prueba unitaria de registro de una {@link TipoAliadoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_tipoAliado_no_existe() throws PmzException{
    	tipoAliadoServiceAPI.guardar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoAliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoAliado_existe() throws PmzException{
        Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(ID_PRIMER_TIPOALIADO);
        tipoAliadoServiceAPI.guardar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TipoAliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_tipoAliado_existe() throws PmzException{
        Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(ID_PRIMER_TIPOALIADO);
        tipoAliadoServiceAPI.editar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoAliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_tipoAliado_no_existe() throws PmzException{
        tipoAliadoServiceAPI.editar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }
    
    
}
