package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI;
import co.com.fspb.mgs.facade.impl.ObjetivoIniciativaFacadeImpl;
import co.com.fspb.mgs.service.api.ObjetivoIniciativaServiceAPI;
import co.com.fspb.mgs.test.service.data.ObjetivoIniciativaServiceTestData;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.test.service.data.IniciativaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ObjetivoIniciativaFacadeAPI} de la entidad {@link ObjetivoIniciativa}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaFacadeTest
 * @date nov 11, 2016
 */
public class ObjetivoIniciativaFacadeTest {

    private static final ObjetivoDTO OBJETIVODTO = ObjetivoServiceTestData.getObjetivoDTO();
    private static final IniciativaDTO INICIATIVADTO = IniciativaServiceTestData.getIniciativaDTO();
    private static final Integer IDUSUARIORESP = 1;

    @InjectMocks
    private ObjetivoIniciativaFacadeAPI objetivoIniciativaFacadeAPI = new ObjetivoIniciativaFacadeImpl();

    @Mock
    private ObjetivoIniciativaServiceAPI objetivoIniciativaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getResultSetObjetivoIniciativasDTO()).when(objetivoIniciativaServiceAPI).getRecords(null);
        PmzResultSet<ObjetivoIniciativaDTO> objetivoIniciativas = objetivoIniciativaFacadeAPI.getRecords(null);
        Assert.assertEquals(ObjetivoIniciativaServiceTestData.getTotalObjetivoIniciativas(), objetivoIniciativas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoIniciativaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivoIniciativa_existe(){
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO()).when(objetivoIniciativaServiceAPI).getObjetivoIniciativaDTO(OBJETIVODTO, INICIATIVADTO, IDUSUARIORESP);
        ObjetivoIniciativaDTO objetivoIniciativa = objetivoIniciativaFacadeAPI.getObjetivoIniciativaDTO(OBJETIVODTO, INICIATIVADTO, IDUSUARIORESP);
        Assert.assertEquals(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO().getId(), objetivoIniciativa.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoIniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_objetivoIniciativa_no_existe() throws PmzException{
        objetivoIniciativaFacadeAPI.guardar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ObjetivoIniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_objetivoIniciativa_existe() throws PmzException{
        objetivoIniciativaFacadeAPI.editar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
}
