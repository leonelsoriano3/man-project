package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Componente;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Componente}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteServiceTestData
 * @date nov 11, 2016
 */
public abstract class ComponenteServiceTestData {
    
    private static List<Componente> entities = new ArrayList<Componente>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Componente entity = new Componente();

            entity.setId(i);
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setInfoActividades("InfoActividades" + i);
            entity.setResultadosEsperados("ResultadosEsperados" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Componente} Mock 
     * tranformado a DTO {@link ComponenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ComponenteDTO}
     */
    public static ComponenteDTO getComponenteDTO(){
        return DTOTransformer.getComponenteDTOFromComponente(getComponente());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Componente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Componente} Mock
     */
    public static Componente getComponente(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Componente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Componente}
     */
    public static List<Componente> getComponentes(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ComponenteDTO> getResultSetComponentesDTO(){
        List<ComponenteDTO> dtos = new ArrayList<ComponenteDTO>();
        for (Componente entity : entities) {
            dtos.add(DTOTransformer.getComponenteDTOFromComponente(entity));
        }
        return new PmzResultSet<ComponenteDTO>(dtos, getTotalComponentes(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalComponentes(){
        return Long.valueOf(entities.size());
    }

}
