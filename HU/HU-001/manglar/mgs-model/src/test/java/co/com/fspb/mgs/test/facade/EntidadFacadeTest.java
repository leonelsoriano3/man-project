package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.facade.api.EntidadFacadeAPI;
import co.com.fspb.mgs.facade.impl.EntidadFacadeImpl;
import co.com.fspb.mgs.service.api.EntidadServiceAPI;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EntidadFacadeAPI} de la entidad {@link Entidad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadFacadeTest
 * @date nov 11, 2016
 */
public class EntidadFacadeTest {

    private static final Long ID_PRIMER_ENTIDAD = 1;

    @InjectMocks
    private EntidadFacadeAPI entidadFacadeAPI = new EntidadFacadeImpl();

    @Mock
    private EntidadServiceAPI entidadServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadServiceTestData.getResultSetEntidadsDTO()).when(entidadServiceAPI).getRecords(null);
        PmzResultSet<EntidadDTO> entidads = entidadFacadeAPI.getRecords(null);
        Assert.assertEquals(EntidadServiceTestData.getTotalEntidads(), entidads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidad_existe(){
        Mockito.doReturn(EntidadServiceTestData.getEntidadDTO()).when(entidadServiceAPI).getEntidadDTO(ID_PRIMER_ENTIDAD);
        EntidadDTO entidad = entidadFacadeAPI.getEntidadDTO(ID_PRIMER_ENTIDAD);
        Assert.assertEquals(EntidadServiceTestData.getEntidadDTO().getId(), entidad.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_entidad_no_existe() throws PmzException{
        entidadFacadeAPI.guardar(EntidadServiceTestData.getEntidadDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EntidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_entidad_existe() throws PmzException{
        entidadFacadeAPI.editar(EntidadServiceTestData.getEntidadDTO());
    }
    
}
