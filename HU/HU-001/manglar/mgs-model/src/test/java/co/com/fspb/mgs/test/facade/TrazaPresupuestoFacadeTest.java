package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI;
import co.com.fspb.mgs.facade.impl.TrazaPresupuestoFacadeImpl;
import co.com.fspb.mgs.service.api.TrazaPresupuestoServiceAPI;
import co.com.fspb.mgs.test.service.data.TrazaPresupuestoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TrazaPresupuestoFacadeAPI} de la entidad {@link TrazaPresupuesto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuestoFacadeTest
 * @date nov 11, 2016
 */
public class TrazaPresupuestoFacadeTest {

    private static final Long ID_PRIMER_TRAZAPRESUPUESTO = 1;

    @InjectMocks
    private TrazaPresupuestoFacadeAPI trazaPresupuestoFacadeAPI = new TrazaPresupuestoFacadeImpl();

    @Mock
    private TrazaPresupuestoServiceAPI trazaPresupuestoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getResultSetTrazaPresupuestosDTO()).when(trazaPresupuestoServiceAPI).getRecords(null);
        PmzResultSet<TrazaPresupuestoDTO> trazaPresupuestos = trazaPresupuestoFacadeAPI.getRecords(null);
        Assert.assertEquals(TrazaPresupuestoServiceTestData.getTotalTrazaPresupuestos(), trazaPresupuestos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TrazaPresupuestoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaPresupuesto_existe(){
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO()).when(trazaPresupuestoServiceAPI).getTrazaPresupuestoDTO(ID_PRIMER_TRAZAPRESUPUESTO);
        TrazaPresupuestoDTO trazaPresupuesto = trazaPresupuestoFacadeAPI.getTrazaPresupuestoDTO(ID_PRIMER_TRAZAPRESUPUESTO);
        Assert.assertEquals(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO().getId(), trazaPresupuesto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaPresupuestoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_trazaPresupuesto_no_existe() throws PmzException{
        trazaPresupuestoFacadeAPI.guardar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TrazaPresupuestoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_trazaPresupuesto_existe() throws PmzException{
        trazaPresupuestoFacadeAPI.editar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    
}
