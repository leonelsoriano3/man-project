package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.dao.api.AliadoDaoAPI;
import co.com.fspb.mgs.service.api.AliadoServiceAPI;
import co.com.fspb.mgs.service.impl.AliadoServiceImpl;
import co.com.fspb.mgs.test.service.data.AliadoServiceTestData;
      import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.dao.api.TipoAliadoDaoAPI;
import co.com.fspb.mgs.test.service.data.TipoAliadoServiceTestData;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link AliadoServiceAPI} de la entidad {@link Aliado}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoServiceAPI
 * @date nov 11, 2016
 */
public class AliadoServiceTest {
	
    private static final Long ID_PRIMER_ALIADO = 1;

    @InjectMocks
    private AliadoServiceAPI aliadoServiceAPI = new AliadoServiceImpl();

    @Mock
    private AliadoDaoAPI aliadoDaoAPI;
    @Mock
    private TipoAliadoDaoAPI tipoAliadoDaoAPI;
    @Mock
    private EntidadDaoAPI entidadDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AliadoServiceTestData.getAliados()).when(aliadoDaoAPI).getRecords(null);
        Mockito.doReturn(AliadoServiceTestData.getTotalAliados()).when(aliadoDaoAPI).countRecords(null);
        PmzResultSet<AliadoDTO> aliados = aliadoServiceAPI.getRecords(null);
        Assert.assertEquals(AliadoServiceTestData.getTotalAliados(), aliados.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AliadoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_aliado_existe(){
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
        AliadoDTO aliado = aliadoServiceAPI.getAliadoDTO(ID_PRIMER_ALIADO);
        Assert.assertEquals(AliadoServiceTestData.getAliado().getId(), aliado.getId());
    }

    /**
     * Prueba unitaria de get con {@link AliadoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_aliado_no_existe(){
        Mockito.doReturn(null).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
        AliadoDTO aliado = aliadoServiceAPI.getAliadoDTO(ID_PRIMER_ALIADO);
        Assert.assertEquals(null, aliado);
    }

    /**
     * Prueba unitaria de registro de una {@link AliadoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_aliado_no_existe() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	aliadoServiceAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link AliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_aliado_existe() throws PmzException{
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
        aliadoServiceAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link AliadoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con tipoAliado
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_aliado_no_existe_no_tipoAliado() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	aliadoServiceAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link AliadoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_aliado_no_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	aliadoServiceAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link AliadoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_aliado_no_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	aliadoServiceAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_aliado_existe() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
        aliadoServiceAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_aliado_no_existe() throws PmzException{
        aliadoServiceAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con tipoAliado
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_aliado_existe_no_tipoAliado() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
    	aliadoServiceAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_aliado_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
    	aliadoServiceAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_aliado_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliado()).when(tipoAliadoDaoAPI).get(TipoAliadoServiceTestData.getTipoAliado().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
        Mockito.doReturn(AliadoServiceTestData.getAliado()).when(aliadoDaoAPI).get(ID_PRIMER_ALIADO);
    	aliadoServiceAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
    
}
