package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI;
import co.com.fspb.mgs.facade.impl.EntidadTerritorioFacadeImpl;
import co.com.fspb.mgs.service.api.EntidadTerritorioServiceAPI;
import co.com.fspb.mgs.test.service.data.EntidadTerritorioServiceTestData;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EntidadTerritorioFacadeAPI} de la entidad {@link EntidadTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioFacadeTest
 * @date nov 11, 2016
 */
public class EntidadTerritorioFacadeTest {

    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final EntidadDTO ENTIDADDTO = EntidadServiceTestData.getEntidadDTO();

    @InjectMocks
    private EntidadTerritorioFacadeAPI entidadTerritorioFacadeAPI = new EntidadTerritorioFacadeImpl();

    @Mock
    private EntidadTerritorioServiceAPI entidadTerritorioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadTerritorioServiceTestData.getResultSetEntidadTerritoriosDTO()).when(entidadTerritorioServiceAPI).getRecords(null);
        PmzResultSet<EntidadTerritorioDTO> entidadTerritorios = entidadTerritorioFacadeAPI.getRecords(null);
        Assert.assertEquals(EntidadTerritorioServiceTestData.getTotalEntidadTerritorios(), entidadTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadTerritorio_existe(){
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO()).when(entidadTerritorioServiceAPI).getEntidadTerritorioDTO(TERRITORIODTO, ENTIDADDTO);
        EntidadTerritorioDTO entidadTerritorio = entidadTerritorioFacadeAPI.getEntidadTerritorioDTO(TERRITORIODTO, ENTIDADDTO);
        Assert.assertEquals(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO().getId(), entidadTerritorio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_entidadTerritorio_no_existe() throws PmzException{
        entidadTerritorioFacadeAPI.guardar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EntidadTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_entidadTerritorio_existe() throws PmzException{
        entidadTerritorioFacadeAPI.editar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
}
