package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.dao.api.IndicadorObjetivoDaoAPI;
import co.com.fspb.mgs.service.api.IndicadorObjetivoServiceAPI;
import co.com.fspb.mgs.service.impl.IndicadorObjetivoServiceImpl;
import co.com.fspb.mgs.test.service.data.IndicadorObjetivoServiceTestData;
import co.com.fspb.mgs.dao.model.IndicadorObjetivoPK;
  import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link IndicadorObjetivoServiceAPI} de la entidad {@link IndicadorObjetivo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoServiceAPI
 * @date nov 11, 2016
 */
public class IndicadorObjetivoServiceTest {
	
    private static final IndicadorDTO INDICADORDTO = IndicadorServiceTestData.getIndicadorDTO();
    private static final ObjetivoDTO OBJETIVODTO = ObjetivoServiceTestData.getObjetivoDTO();

    @InjectMocks
    private IndicadorObjetivoServiceAPI indicadorObjetivoServiceAPI = new IndicadorObjetivoServiceImpl();

    @Mock
    private IndicadorObjetivoDaoAPI indicadorObjetivoDaoAPI;
    @Mock
    private IndicadorDaoAPI indicadorDaoAPI;
    @Mock
    private ObjetivoDaoAPI objetivoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivos()).when(indicadorObjetivoDaoAPI).getRecords(null);
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getTotalIndicadorObjetivos()).when(indicadorObjetivoDaoAPI).countRecords(null);
        PmzResultSet<IndicadorObjetivoDTO> indicadorObjetivos = indicadorObjetivoServiceAPI.getRecords(null);
        Assert.assertEquals(IndicadorObjetivoServiceTestData.getTotalIndicadorObjetivos(), indicadorObjetivos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorObjetivoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorObjetivo_existe(){
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivo()).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
        IndicadorObjetivoDTO indicadorObjetivo = indicadorObjetivoServiceAPI.getIndicadorObjetivoDTO(INDICADORDTO, OBJETIVODTO);
        Assert.assertEquals(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO().getId(), indicadorObjetivo.getId());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorObjetivoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorObjetivo_no_existe(){
        Mockito.doReturn(null).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
        IndicadorObjetivoDTO indicadorObjetivo = indicadorObjetivoServiceAPI.getIndicadorObjetivoDTO(INDICADORDTO, OBJETIVODTO);
        Assert.assertEquals(null, indicadorObjetivo);
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorObjetivoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_indicadorObjetivo_no_existe() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	indicadorObjetivoServiceAPI.guardar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorObjetivo_existe() throws PmzException{
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivo()).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
        indicadorObjetivoServiceAPI.guardar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link IndicadorObjetivoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con indicador
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorObjetivo_no_existe_no_indicador() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	indicadorObjetivoServiceAPI.guardar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link IndicadorObjetivoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorObjetivo_no_existe_no_objetivo() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	indicadorObjetivoServiceAPI.guardar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link IndicadorObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_indicadorObjetivo_existe() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivo()).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
        indicadorObjetivoServiceAPI.editar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_indicadorObjetivo_no_existe() throws PmzException{
        indicadorObjetivoServiceAPI.editar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorObjetivoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con indicador
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_indicadorObjetivo_existe_no_indicador() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivo()).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
    	indicadorObjetivoServiceAPI.editar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorObjetivoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_indicadorObjetivo_existe_no_objetivo() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivo()).when(indicadorObjetivoDaoAPI).get(Mockito.any(IndicadorObjetivoPK.class));
    	indicadorObjetivoServiceAPI.editar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
    
}
