package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI;
import co.com.fspb.mgs.facade.impl.DescIndependienteFacadeImpl;
import co.com.fspb.mgs.service.api.DescIndependienteServiceAPI;
import co.com.fspb.mgs.test.service.data.DescIndependienteServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link DescIndependienteFacadeAPI} de la entidad {@link DescIndependiente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DescIndependienteFacadeTest
 * @date nov 11, 2016
 */
public class DescIndependienteFacadeTest {

    private static final Long ID_PRIMER_DESCINDEPENDIENTE = 1;

    @InjectMocks
    private DescIndependienteFacadeAPI descIndependienteFacadeAPI = new DescIndependienteFacadeImpl();

    @Mock
    private DescIndependienteServiceAPI descIndependienteServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DescIndependienteServiceTestData.getResultSetDescIndependientesDTO()).when(descIndependienteServiceAPI).getRecords(null);
        PmzResultSet<DescIndependienteDTO> descIndependientes = descIndependienteFacadeAPI.getRecords(null);
        Assert.assertEquals(DescIndependienteServiceTestData.getTotalDescIndependientes(), descIndependientes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DescIndependienteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_descIndependiente_existe(){
        Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependienteDTO()).when(descIndependienteServiceAPI).getDescIndependienteDTO(ID_PRIMER_DESCINDEPENDIENTE);
        DescIndependienteDTO descIndependiente = descIndependienteFacadeAPI.getDescIndependienteDTO(ID_PRIMER_DESCINDEPENDIENTE);
        Assert.assertEquals(DescIndependienteServiceTestData.getDescIndependienteDTO().getId(), descIndependiente.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link DescIndependienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_descIndependiente_no_existe() throws PmzException{
        descIndependienteFacadeAPI.guardar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link DescIndependienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_descIndependiente_existe() throws PmzException{
        descIndependienteFacadeAPI.editar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }
    
}
