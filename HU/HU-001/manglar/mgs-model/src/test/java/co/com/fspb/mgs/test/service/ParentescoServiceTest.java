package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ParentescoDTO;
import co.com.fspb.mgs.dao.api.ParentescoDaoAPI;
import co.com.fspb.mgs.service.api.ParentescoServiceAPI;
import co.com.fspb.mgs.service.impl.ParentescoServiceImpl;
import co.com.fspb.mgs.test.service.data.ParentescoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ParentescoServiceAPI} de la entidad {@link Parentesco}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoServiceAPI
 * @date nov 11, 2016
 */
public class ParentescoServiceTest {
	
    private static final Long ID_PRIMER_PARENTESCO = 1;

    @InjectMocks
    private ParentescoServiceAPI parentescoServiceAPI = new ParentescoServiceImpl();

    @Mock
    private ParentescoDaoAPI parentescoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParentescoServiceTestData.getParentescos()).when(parentescoDaoAPI).getRecords(null);
        Mockito.doReturn(ParentescoServiceTestData.getTotalParentescos()).when(parentescoDaoAPI).countRecords(null);
        PmzResultSet<ParentescoDTO> parentescos = parentescoServiceAPI.getRecords(null);
        Assert.assertEquals(ParentescoServiceTestData.getTotalParentescos(), parentescos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParentescoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parentesco_existe(){
        Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ID_PRIMER_PARENTESCO);
        ParentescoDTO parentesco = parentescoServiceAPI.getParentescoDTO(ID_PRIMER_PARENTESCO);
        Assert.assertEquals(ParentescoServiceTestData.getParentesco().getId(), parentesco.getId());
    }

    /**
     * Prueba unitaria de get con {@link ParentescoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parentesco_no_existe(){
        Mockito.doReturn(null).when(parentescoDaoAPI).get(ID_PRIMER_PARENTESCO);
        ParentescoDTO parentesco = parentescoServiceAPI.getParentescoDTO(ID_PRIMER_PARENTESCO);
        Assert.assertEquals(null, parentesco);
    }

    /**
     * Prueba unitaria de registro de una {@link ParentescoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_parentesco_no_existe() throws PmzException{
    	parentescoServiceAPI.guardar(ParentescoServiceTestData.getParentescoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ParentescoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_parentesco_existe() throws PmzException{
        Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ID_PRIMER_PARENTESCO);
        parentescoServiceAPI.guardar(ParentescoServiceTestData.getParentescoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ParentescoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_parentesco_existe() throws PmzException{
        Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ID_PRIMER_PARENTESCO);
        parentescoServiceAPI.editar(ParentescoServiceTestData.getParentescoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParentescoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_parentesco_no_existe() throws PmzException{
        parentescoServiceAPI.editar(ParentescoServiceTestData.getParentescoDTO());
    }
    
    
}
