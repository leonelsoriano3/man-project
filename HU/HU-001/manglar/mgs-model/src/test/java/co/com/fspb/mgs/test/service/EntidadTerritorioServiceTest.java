package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.dao.api.EntidadTerritorioDaoAPI;
import co.com.fspb.mgs.service.api.EntidadTerritorioServiceAPI;
import co.com.fspb.mgs.service.impl.EntidadTerritorioServiceImpl;
import co.com.fspb.mgs.test.service.data.EntidadTerritorioServiceTestData;
import co.com.fspb.mgs.dao.model.EntidadTerritorioPK;
  import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EntidadTerritorioServiceAPI} de la entidad {@link EntidadTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioServiceAPI
 * @date nov 11, 2016
 */
public class EntidadTerritorioServiceTest {
	
    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final EntidadDTO ENTIDADDTO = EntidadServiceTestData.getEntidadDTO();

    @InjectMocks
    private EntidadTerritorioServiceAPI entidadTerritorioServiceAPI = new EntidadTerritorioServiceImpl();

    @Mock
    private EntidadTerritorioDaoAPI entidadTerritorioDaoAPI;
    @Mock
    private TerritorioDaoAPI territorioDaoAPI;
    @Mock
    private EntidadDaoAPI entidadDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorios()).when(entidadTerritorioDaoAPI).getRecords(null);
        Mockito.doReturn(EntidadTerritorioServiceTestData.getTotalEntidadTerritorios()).when(entidadTerritorioDaoAPI).countRecords(null);
        PmzResultSet<EntidadTerritorioDTO> entidadTerritorios = entidadTerritorioServiceAPI.getRecords(null);
        Assert.assertEquals(EntidadTerritorioServiceTestData.getTotalEntidadTerritorios(), entidadTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadTerritorio_existe(){
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorio()).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
        EntidadTerritorioDTO entidadTerritorio = entidadTerritorioServiceAPI.getEntidadTerritorioDTO(TERRITORIODTO, ENTIDADDTO);
        Assert.assertEquals(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO().getId(), entidadTerritorio.getId());
    }

    /**
     * Prueba unitaria de get con {@link EntidadTerritorioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadTerritorio_no_existe(){
        Mockito.doReturn(null).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
        EntidadTerritorioDTO entidadTerritorio = entidadTerritorioServiceAPI.getEntidadTerritorioDTO(TERRITORIODTO, ENTIDADDTO);
        Assert.assertEquals(null, entidadTerritorio);
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadTerritorioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_entidadTerritorio_no_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	entidadTerritorioServiceAPI.guardar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadTerritorio_existe() throws PmzException{
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorio()).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
        entidadTerritorioServiceAPI.guardar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link EntidadTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadTerritorio_no_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	entidadTerritorioServiceAPI.guardar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link EntidadTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadTerritorio_no_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	entidadTerritorioServiceAPI.guardar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EntidadTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_entidadTerritorio_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorio()).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
        entidadTerritorioServiceAPI.editar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_entidadTerritorio_no_existe() throws PmzException{
        entidadTerritorioServiceAPI.editar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_entidadTerritorio_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorio()).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
    	entidadTerritorioServiceAPI.editar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_entidadTerritorio_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
        Mockito.doReturn(EntidadTerritorioServiceTestData.getEntidadTerritorio()).when(entidadTerritorioDaoAPI).get(Mockito.any(EntidadTerritorioPK.class));
    	entidadTerritorioServiceAPI.editar(EntidadTerritorioServiceTestData.getEntidadTerritorioDTO());
    }
    
    
}
