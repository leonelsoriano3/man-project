package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EstadoViaDTO;
import co.com.fspb.mgs.dao.api.EstadoViaDaoAPI;
import co.com.fspb.mgs.service.api.EstadoViaServiceAPI;
import co.com.fspb.mgs.service.impl.EstadoViaServiceImpl;
import co.com.fspb.mgs.test.service.data.EstadoViaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EstadoViaServiceAPI} de la entidad {@link EstadoVia}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoViaServiceAPI
 * @date nov 11, 2016
 */
public class EstadoViaServiceTest {
	
    private static final Long ID_PRIMER_ESTADOVIA = 1;

    @InjectMocks
    private EstadoViaServiceAPI estadoViaServiceAPI = new EstadoViaServiceImpl();

    @Mock
    private EstadoViaDaoAPI estadoViaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoViaServiceTestData.getEstadoVias()).when(estadoViaDaoAPI).getRecords(null);
        Mockito.doReturn(EstadoViaServiceTestData.getTotalEstadoVias()).when(estadoViaDaoAPI).countRecords(null);
        PmzResultSet<EstadoViaDTO> estadoVias = estadoViaServiceAPI.getRecords(null);
        Assert.assertEquals(EstadoViaServiceTestData.getTotalEstadoVias(), estadoVias.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoViaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoVia_existe(){
        Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(ID_PRIMER_ESTADOVIA);
        EstadoViaDTO estadoVia = estadoViaServiceAPI.getEstadoViaDTO(ID_PRIMER_ESTADOVIA);
        Assert.assertEquals(EstadoViaServiceTestData.getEstadoVia().getId(), estadoVia.getId());
    }

    /**
     * Prueba unitaria de get con {@link EstadoViaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoVia_no_existe(){
        Mockito.doReturn(null).when(estadoViaDaoAPI).get(ID_PRIMER_ESTADOVIA);
        EstadoViaDTO estadoVia = estadoViaServiceAPI.getEstadoViaDTO(ID_PRIMER_ESTADOVIA);
        Assert.assertEquals(null, estadoVia);
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoViaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_estadoVia_no_existe() throws PmzException{
    	estadoViaServiceAPI.guardar(EstadoViaServiceTestData.getEstadoViaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoViaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_estadoVia_existe() throws PmzException{
        Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(ID_PRIMER_ESTADOVIA);
        estadoViaServiceAPI.guardar(EstadoViaServiceTestData.getEstadoViaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EstadoViaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_estadoVia_existe() throws PmzException{
        Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(ID_PRIMER_ESTADOVIA);
        estadoViaServiceAPI.editar(EstadoViaServiceTestData.getEstadoViaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EstadoViaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_estadoVia_no_existe() throws PmzException{
        estadoViaServiceAPI.editar(EstadoViaServiceTestData.getEstadoViaDTO());
    }
    
    
}
