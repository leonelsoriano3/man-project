package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.service.api.ObjetivoServiceAPI;
import co.com.fspb.mgs.service.impl.ObjetivoServiceImpl;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;
  import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ObjetivoServiceAPI} de la entidad {@link Objetivo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoServiceAPI
 * @date nov 11, 2016
 */
public class ObjetivoServiceTest {
	
    private static final Long ID_PRIMER_OBJETIVO = 1;

    @InjectMocks
    private ObjetivoServiceAPI objetivoServiceAPI = new ObjetivoServiceImpl();

    @Mock
    private ObjetivoDaoAPI objetivoDaoAPI;
    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivos()).when(objetivoDaoAPI).getRecords(null);
        Mockito.doReturn(ObjetivoServiceTestData.getTotalObjetivos()).when(objetivoDaoAPI).countRecords(null);
        PmzResultSet<ObjetivoDTO> objetivos = objetivoServiceAPI.getRecords(null);
        Assert.assertEquals(ObjetivoServiceTestData.getTotalObjetivos(), objetivos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivo_existe(){
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ID_PRIMER_OBJETIVO);
        ObjetivoDTO objetivo = objetivoServiceAPI.getObjetivoDTO(ID_PRIMER_OBJETIVO);
        Assert.assertEquals(ObjetivoServiceTestData.getObjetivo().getId(), objetivo.getId());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivo_no_existe(){
        Mockito.doReturn(null).when(objetivoDaoAPI).get(ID_PRIMER_OBJETIVO);
        ObjetivoDTO objetivo = objetivoServiceAPI.getObjetivoDTO(ID_PRIMER_OBJETIVO);
        Assert.assertEquals(null, objetivo);
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_objetivo_no_existe() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	objetivoServiceAPI.guardar(ObjetivoServiceTestData.getObjetivoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_objetivo_existe() throws PmzException{
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ID_PRIMER_OBJETIVO);
        objetivoServiceAPI.guardar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ObjetivoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_objetivo_no_existe_no_usuario() throws PmzException{
    	objetivoServiceAPI.guardar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_objetivo_existe() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ID_PRIMER_OBJETIVO);
        objetivoServiceAPI.editar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ObjetivoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_objetivo_no_existe() throws PmzException{
        objetivoServiceAPI.editar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ObjetivoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_objetivo_existe_no_usuario() throws PmzException{
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ID_PRIMER_OBJETIVO);
    	objetivoServiceAPI.editar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    
    
}
