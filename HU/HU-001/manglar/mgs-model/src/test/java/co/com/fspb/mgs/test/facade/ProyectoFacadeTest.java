package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.facade.api.ProyectoFacadeAPI;
import co.com.fspb.mgs.facade.impl.ProyectoFacadeImpl;
import co.com.fspb.mgs.service.api.ProyectoServiceAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ProyectoFacadeAPI} de la entidad {@link Proyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoFacadeTest
 * @date nov 11, 2016
 */
public class ProyectoFacadeTest {

    private static final Long ID_PRIMER_PROYECTO = 1;

    @InjectMocks
    private ProyectoFacadeAPI proyectoFacadeAPI = new ProyectoFacadeImpl();

    @Mock
    private ProyectoServiceAPI proyectoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoServiceTestData.getResultSetProyectosDTO()).when(proyectoServiceAPI).getRecords(null);
        PmzResultSet<ProyectoDTO> proyectos = proyectoFacadeAPI.getRecords(null);
        Assert.assertEquals(ProyectoServiceTestData.getTotalProyectos(), proyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyecto_existe(){
        Mockito.doReturn(ProyectoServiceTestData.getProyectoDTO()).when(proyectoServiceAPI).getProyectoDTO(ID_PRIMER_PROYECTO);
        ProyectoDTO proyecto = proyectoFacadeAPI.getProyectoDTO(ID_PRIMER_PROYECTO);
        Assert.assertEquals(ProyectoServiceTestData.getProyectoDTO().getId(), proyecto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_proyecto_no_existe() throws PmzException{
        proyectoFacadeAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_proyecto_existe() throws PmzException{
        proyectoFacadeAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
}
