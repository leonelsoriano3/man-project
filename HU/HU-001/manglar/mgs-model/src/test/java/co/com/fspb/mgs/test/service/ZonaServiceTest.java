package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.service.api.ZonaServiceAPI;
import co.com.fspb.mgs.service.impl.ZonaServiceImpl;
import co.com.fspb.mgs.test.service.data.ZonaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ZonaServiceAPI} de la entidad {@link Zona}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaServiceAPI
 * @date nov 11, 2016
 */
public class ZonaServiceTest {
	
    private static final Long ID_PRIMER_ZONA = 1;

    @InjectMocks
    private ZonaServiceAPI zonaServiceAPI = new ZonaServiceImpl();

    @Mock
    private ZonaDaoAPI zonaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ZonaServiceTestData.getZonas()).when(zonaDaoAPI).getRecords(null);
        Mockito.doReturn(ZonaServiceTestData.getTotalZonas()).when(zonaDaoAPI).countRecords(null);
        PmzResultSet<ZonaDTO> zonas = zonaServiceAPI.getRecords(null);
        Assert.assertEquals(ZonaServiceTestData.getTotalZonas(), zonas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ZonaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_zona_existe(){
        Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ID_PRIMER_ZONA);
        ZonaDTO zona = zonaServiceAPI.getZonaDTO(ID_PRIMER_ZONA);
        Assert.assertEquals(ZonaServiceTestData.getZona().getId(), zona.getId());
    }

    /**
     * Prueba unitaria de get con {@link ZonaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_zona_no_existe(){
        Mockito.doReturn(null).when(zonaDaoAPI).get(ID_PRIMER_ZONA);
        ZonaDTO zona = zonaServiceAPI.getZonaDTO(ID_PRIMER_ZONA);
        Assert.assertEquals(null, zona);
    }

    /**
     * Prueba unitaria de registro de una {@link ZonaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_zona_no_existe() throws PmzException{
    	zonaServiceAPI.guardar(ZonaServiceTestData.getZonaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ZonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_zona_existe() throws PmzException{
        Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ID_PRIMER_ZONA);
        zonaServiceAPI.guardar(ZonaServiceTestData.getZonaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ZonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_zona_existe() throws PmzException{
        Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ID_PRIMER_ZONA);
        zonaServiceAPI.editar(ZonaServiceTestData.getZonaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ZonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_zona_no_existe() throws PmzException{
        zonaServiceAPI.editar(ZonaServiceTestData.getZonaDTO());
    }
    
    
}
