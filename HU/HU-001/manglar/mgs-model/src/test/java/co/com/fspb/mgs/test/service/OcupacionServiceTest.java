package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.dao.api.OcupacionDaoAPI;
import co.com.fspb.mgs.service.api.OcupacionServiceAPI;
import co.com.fspb.mgs.service.impl.OcupacionServiceImpl;
import co.com.fspb.mgs.test.service.data.OcupacionServiceTestData;

/**
 * Prueba unitaria para el servicio {@link OcupacionServiceAPI} de la entidad {@link Ocupacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class OcupacionServiceAPI
 * @date nov 11, 2016
 */
public class OcupacionServiceTest {
	
    private static final Long ID_PRIMER_OCUPACION = 1;

    @InjectMocks
    private OcupacionServiceAPI ocupacionServiceAPI = new OcupacionServiceImpl();

    @Mock
    private OcupacionDaoAPI ocupacionDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(OcupacionServiceTestData.getOcupacions()).when(ocupacionDaoAPI).getRecords(null);
        Mockito.doReturn(OcupacionServiceTestData.getTotalOcupacions()).when(ocupacionDaoAPI).countRecords(null);
        PmzResultSet<OcupacionDTO> ocupacions = ocupacionServiceAPI.getRecords(null);
        Assert.assertEquals(OcupacionServiceTestData.getTotalOcupacions(), ocupacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link OcupacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_ocupacion_existe(){
        Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(ID_PRIMER_OCUPACION);
        OcupacionDTO ocupacion = ocupacionServiceAPI.getOcupacionDTO(ID_PRIMER_OCUPACION);
        Assert.assertEquals(OcupacionServiceTestData.getOcupacion().getId(), ocupacion.getId());
    }

    /**
     * Prueba unitaria de get con {@link OcupacionDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_ocupacion_no_existe(){
        Mockito.doReturn(null).when(ocupacionDaoAPI).get(ID_PRIMER_OCUPACION);
        OcupacionDTO ocupacion = ocupacionServiceAPI.getOcupacionDTO(ID_PRIMER_OCUPACION);
        Assert.assertEquals(null, ocupacion);
    }

    /**
     * Prueba unitaria de registro de una {@link OcupacionDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_ocupacion_no_existe() throws PmzException{
    	ocupacionServiceAPI.guardar(OcupacionServiceTestData.getOcupacionDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link OcupacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_ocupacion_existe() throws PmzException{
        Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(ID_PRIMER_OCUPACION);
        ocupacionServiceAPI.guardar(OcupacionServiceTestData.getOcupacionDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link OcupacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_ocupacion_existe() throws PmzException{
        Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(ID_PRIMER_OCUPACION);
        ocupacionServiceAPI.editar(OcupacionServiceTestData.getOcupacionDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link OcupacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_ocupacion_no_existe() throws PmzException{
        ocupacionServiceAPI.editar(OcupacionServiceTestData.getOcupacionDTO());
    }
    
    
}
