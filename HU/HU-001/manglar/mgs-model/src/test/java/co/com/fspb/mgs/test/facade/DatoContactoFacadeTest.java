package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI;
import co.com.fspb.mgs.facade.impl.DatoContactoFacadeImpl;
import co.com.fspb.mgs.service.api.DatoContactoServiceAPI;
import co.com.fspb.mgs.test.service.data.DatoContactoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link DatoContactoFacadeAPI} de la entidad {@link DatoContacto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoContactoFacadeTest
 * @date nov 11, 2016
 */
public class DatoContactoFacadeTest {

    private static final Long ID_PRIMER_DATOCONTACTO = 1;

    @InjectMocks
    private DatoContactoFacadeAPI datoContactoFacadeAPI = new DatoContactoFacadeImpl();

    @Mock
    private DatoContactoServiceAPI datoContactoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DatoContactoServiceTestData.getResultSetDatoContactosDTO()).when(datoContactoServiceAPI).getRecords(null);
        PmzResultSet<DatoContactoDTO> datoContactos = datoContactoFacadeAPI.getRecords(null);
        Assert.assertEquals(DatoContactoServiceTestData.getTotalDatoContactos(), datoContactos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DatoContactoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoContacto_existe(){
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContactoDTO()).when(datoContactoServiceAPI).getDatoContactoDTO(ID_PRIMER_DATOCONTACTO);
        DatoContactoDTO datoContacto = datoContactoFacadeAPI.getDatoContactoDTO(ID_PRIMER_DATOCONTACTO);
        Assert.assertEquals(DatoContactoServiceTestData.getDatoContactoDTO().getId(), datoContacto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link DatoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_datoContacto_no_existe() throws PmzException{
        datoContactoFacadeAPI.guardar(DatoContactoServiceTestData.getDatoContactoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link DatoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_datoContacto_existe() throws PmzException{
        datoContactoFacadeAPI.editar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
}
