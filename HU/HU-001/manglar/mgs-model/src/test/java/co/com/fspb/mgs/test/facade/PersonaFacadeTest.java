package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.facade.api.PersonaFacadeAPI;
import co.com.fspb.mgs.facade.impl.PersonaFacadeImpl;
import co.com.fspb.mgs.service.api.PersonaServiceAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link PersonaFacadeAPI} de la entidad {@link Persona}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaFacadeTest
 * @date nov 11, 2016
 */
public class PersonaFacadeTest {

    private static final Long ID_PRIMER_PERSONA = 1;

    @InjectMocks
    private PersonaFacadeAPI personaFacadeAPI = new PersonaFacadeImpl();

    @Mock
    private PersonaServiceAPI personaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(PersonaServiceTestData.getResultSetPersonasDTO()).when(personaServiceAPI).getRecords(null);
        PmzResultSet<PersonaDTO> personas = personaFacadeAPI.getRecords(null);
        Assert.assertEquals(PersonaServiceTestData.getTotalPersonas(), personas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link PersonaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_persona_existe(){
        Mockito.doReturn(PersonaServiceTestData.getPersonaDTO()).when(personaServiceAPI).getPersonaDTO(ID_PRIMER_PERSONA);
        PersonaDTO persona = personaFacadeAPI.getPersonaDTO(ID_PRIMER_PERSONA);
        Assert.assertEquals(PersonaServiceTestData.getPersonaDTO().getId(), persona.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link PersonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_persona_no_existe() throws PmzException{
        personaFacadeAPI.guardar(PersonaServiceTestData.getPersonaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link PersonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_persona_existe() throws PmzException{
        personaFacadeAPI.editar(PersonaServiceTestData.getPersonaDTO());
    }
    
}
