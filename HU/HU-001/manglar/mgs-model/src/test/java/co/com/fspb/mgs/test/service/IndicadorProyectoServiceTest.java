package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.dao.api.IndicadorProyectoDaoAPI;
import co.com.fspb.mgs.service.api.IndicadorProyectoServiceAPI;
import co.com.fspb.mgs.service.impl.IndicadorProyectoServiceImpl;
import co.com.fspb.mgs.test.service.data.IndicadorProyectoServiceTestData;
import co.com.fspb.mgs.dao.model.IndicadorProyectoPK;
  import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link IndicadorProyectoServiceAPI} de la entidad {@link IndicadorProyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoServiceAPI
 * @date nov 11, 2016
 */
public class IndicadorProyectoServiceTest {
	
    private static final IndicadorDTO INDICADORDTO = IndicadorServiceTestData.getIndicadorDTO();
    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();

    @InjectMocks
    private IndicadorProyectoServiceAPI indicadorProyectoServiceAPI = new IndicadorProyectoServiceImpl();

    @Mock
    private IndicadorProyectoDaoAPI indicadorProyectoDaoAPI;
    @Mock
    private IndicadorDaoAPI indicadorDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyectos()).when(indicadorProyectoDaoAPI).getRecords(null);
        Mockito.doReturn(IndicadorProyectoServiceTestData.getTotalIndicadorProyectos()).when(indicadorProyectoDaoAPI).countRecords(null);
        PmzResultSet<IndicadorProyectoDTO> indicadorProyectos = indicadorProyectoServiceAPI.getRecords(null);
        Assert.assertEquals(IndicadorProyectoServiceTestData.getTotalIndicadorProyectos(), indicadorProyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorProyecto_existe(){
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyecto()).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
        IndicadorProyectoDTO indicadorProyecto = indicadorProyectoServiceAPI.getIndicadorProyectoDTO(INDICADORDTO, PROYECTODTO);
        Assert.assertEquals(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO().getId(), indicadorProyecto.getId());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorProyectoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorProyecto_no_existe(){
        Mockito.doReturn(null).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
        IndicadorProyectoDTO indicadorProyecto = indicadorProyectoServiceAPI.getIndicadorProyectoDTO(INDICADORDTO, PROYECTODTO);
        Assert.assertEquals(null, indicadorProyecto);
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorProyectoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_indicadorProyecto_no_existe() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	indicadorProyectoServiceAPI.guardar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorProyecto_existe() throws PmzException{
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyecto()).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
        indicadorProyectoServiceAPI.guardar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link IndicadorProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con indicador
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorProyecto_no_existe_no_indicador() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	indicadorProyectoServiceAPI.guardar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link IndicadorProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicadorProyecto_no_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	indicadorProyectoServiceAPI.guardar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link IndicadorProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_indicadorProyecto_existe() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyecto()).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
        indicadorProyectoServiceAPI.editar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_indicadorProyecto_no_existe() throws PmzException{
        indicadorProyectoServiceAPI.editar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con indicador
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_indicadorProyecto_existe_no_indicador() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyecto()).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
    	indicadorProyectoServiceAPI.editar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_indicadorProyecto_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(IndicadorServiceTestData.getIndicador().getId());
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyecto()).when(indicadorProyectoDaoAPI).get(Mockito.any(IndicadorProyectoPK.class));
    	indicadorProyectoServiceAPI.editar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
    
}
