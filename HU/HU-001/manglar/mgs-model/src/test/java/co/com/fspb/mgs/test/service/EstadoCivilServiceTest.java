package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.dao.api.EstadoCivilDaoAPI;
import co.com.fspb.mgs.service.api.EstadoCivilServiceAPI;
import co.com.fspb.mgs.service.impl.EstadoCivilServiceImpl;
import co.com.fspb.mgs.test.service.data.EstadoCivilServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EstadoCivilServiceAPI} de la entidad {@link EstadoCivil}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoCivilServiceAPI
 * @date nov 11, 2016
 */
public class EstadoCivilServiceTest {
	
    private static final Long ID_PRIMER_ESTADOCIVIL = 1;

    @InjectMocks
    private EstadoCivilServiceAPI estadoCivilServiceAPI = new EstadoCivilServiceImpl();

    @Mock
    private EstadoCivilDaoAPI estadoCivilDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivils()).when(estadoCivilDaoAPI).getRecords(null);
        Mockito.doReturn(EstadoCivilServiceTestData.getTotalEstadoCivils()).when(estadoCivilDaoAPI).countRecords(null);
        PmzResultSet<EstadoCivilDTO> estadoCivils = estadoCivilServiceAPI.getRecords(null);
        Assert.assertEquals(EstadoCivilServiceTestData.getTotalEstadoCivils(), estadoCivils.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoCivilDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoCivil_existe(){
        Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(ID_PRIMER_ESTADOCIVIL);
        EstadoCivilDTO estadoCivil = estadoCivilServiceAPI.getEstadoCivilDTO(ID_PRIMER_ESTADOCIVIL);
        Assert.assertEquals(EstadoCivilServiceTestData.getEstadoCivil().getId(), estadoCivil.getId());
    }

    /**
     * Prueba unitaria de get con {@link EstadoCivilDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoCivil_no_existe(){
        Mockito.doReturn(null).when(estadoCivilDaoAPI).get(ID_PRIMER_ESTADOCIVIL);
        EstadoCivilDTO estadoCivil = estadoCivilServiceAPI.getEstadoCivilDTO(ID_PRIMER_ESTADOCIVIL);
        Assert.assertEquals(null, estadoCivil);
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoCivilDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_estadoCivil_no_existe() throws PmzException{
    	estadoCivilServiceAPI.guardar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoCivilDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_estadoCivil_existe() throws PmzException{
        Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(ID_PRIMER_ESTADOCIVIL);
        estadoCivilServiceAPI.guardar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EstadoCivilDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_estadoCivil_existe() throws PmzException{
        Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(ID_PRIMER_ESTADOCIVIL);
        estadoCivilServiceAPI.editar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EstadoCivilDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_estadoCivil_no_existe() throws PmzException{
        estadoCivilServiceAPI.editar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }
    
    
}
