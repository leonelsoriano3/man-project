package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.dao.api.ObjetivoIniciativaDaoAPI;
import co.com.fspb.mgs.service.api.ObjetivoIniciativaServiceAPI;
import co.com.fspb.mgs.service.impl.ObjetivoIniciativaServiceImpl;
import co.com.fspb.mgs.test.service.data.ObjetivoIniciativaServiceTestData;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativaPK;
  import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.test.service.data.IniciativaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ObjetivoIniciativaServiceAPI} de la entidad {@link ObjetivoIniciativa}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaServiceAPI
 * @date nov 11, 2016
 */
public class ObjetivoIniciativaServiceTest {
	
    private static final ObjetivoDTO OBJETIVODTO = ObjetivoServiceTestData.getObjetivoDTO();
    private static final IniciativaDTO INICIATIVADTO = IniciativaServiceTestData.getIniciativaDTO();
    private static final Integer IDUSUARIORESP = 1;

    @InjectMocks
    private ObjetivoIniciativaServiceAPI objetivoIniciativaServiceAPI = new ObjetivoIniciativaServiceImpl();

    @Mock
    private ObjetivoIniciativaDaoAPI objetivoIniciativaDaoAPI;
    @Mock
    private ObjetivoDaoAPI objetivoDaoAPI;
    @Mock
    private IniciativaDaoAPI iniciativaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativas()).when(objetivoIniciativaDaoAPI).getRecords(null);
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getTotalObjetivoIniciativas()).when(objetivoIniciativaDaoAPI).countRecords(null);
        PmzResultSet<ObjetivoIniciativaDTO> objetivoIniciativas = objetivoIniciativaServiceAPI.getRecords(null);
        Assert.assertEquals(ObjetivoIniciativaServiceTestData.getTotalObjetivoIniciativas(), objetivoIniciativas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoIniciativaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivoIniciativa_existe(){
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativa()).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
        ObjetivoIniciativaDTO objetivoIniciativa = objetivoIniciativaServiceAPI.getObjetivoIniciativaDTO(OBJETIVODTO, INICIATIVADTO, IDUSUARIORESP);
        Assert.assertEquals(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO().getId(), objetivoIniciativa.getId());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoIniciativaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivoIniciativa_no_existe(){
        Mockito.doReturn(null).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
        ObjetivoIniciativaDTO objetivoIniciativa = objetivoIniciativaServiceAPI.getObjetivoIniciativaDTO(OBJETIVODTO, INICIATIVADTO, IDUSUARIORESP);
        Assert.assertEquals(null, objetivoIniciativa);
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoIniciativaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_objetivoIniciativa_no_existe() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
    	objetivoIniciativaServiceAPI.guardar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoIniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_objetivoIniciativa_existe() throws PmzException{
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativa()).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
        objetivoIniciativaServiceAPI.guardar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ObjetivoIniciativaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_objetivoIniciativa_no_existe_no_objetivo() throws PmzException{
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
    	objetivoIniciativaServiceAPI.guardar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ObjetivoIniciativaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con iniciativa
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_objetivoIniciativa_no_existe_no_iniciativa() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	objetivoIniciativaServiceAPI.guardar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ObjetivoIniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_objetivoIniciativa_existe() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativa()).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
        objetivoIniciativaServiceAPI.editar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ObjetivoIniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_objetivoIniciativa_no_existe() throws PmzException{
        objetivoIniciativaServiceAPI.editar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ObjetivoIniciativaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_objetivoIniciativa_existe_no_objetivo() throws PmzException{
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativa()).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
    	objetivoIniciativaServiceAPI.editar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ObjetivoIniciativaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con iniciativa
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_objetivoIniciativa_existe_no_iniciativa() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
        Mockito.doReturn(ObjetivoIniciativaServiceTestData.getObjetivoIniciativa()).when(objetivoIniciativaDaoAPI).get(Mockito.any(ObjetivoIniciativaPK.class));
    	objetivoIniciativaServiceAPI.editar(ObjetivoIniciativaServiceTestData.getObjetivoIniciativaDTO());
    }
    
    
}
