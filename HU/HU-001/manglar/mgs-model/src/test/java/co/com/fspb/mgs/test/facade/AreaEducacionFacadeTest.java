package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI;
import co.com.fspb.mgs.facade.impl.AreaEducacionFacadeImpl;
import co.com.fspb.mgs.service.api.AreaEducacionServiceAPI;
import co.com.fspb.mgs.test.service.data.AreaEducacionServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link AreaEducacionFacadeAPI} de la entidad {@link AreaEducacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AreaEducacionFacadeTest
 * @date nov 11, 2016
 */
public class AreaEducacionFacadeTest {

    private static final Long ID_PRIMER_AREAEDUCACION = 1;

    @InjectMocks
    private AreaEducacionFacadeAPI areaEducacionFacadeAPI = new AreaEducacionFacadeImpl();

    @Mock
    private AreaEducacionServiceAPI areaEducacionServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AreaEducacionServiceTestData.getResultSetAreaEducacionsDTO()).when(areaEducacionServiceAPI).getRecords(null);
        PmzResultSet<AreaEducacionDTO> areaEducacions = areaEducacionFacadeAPI.getRecords(null);
        Assert.assertEquals(AreaEducacionServiceTestData.getTotalAreaEducacions(), areaEducacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AreaEducacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_areaEducacion_existe(){
        Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacionDTO()).when(areaEducacionServiceAPI).getAreaEducacionDTO(ID_PRIMER_AREAEDUCACION);
        AreaEducacionDTO areaEducacion = areaEducacionFacadeAPI.getAreaEducacionDTO(ID_PRIMER_AREAEDUCACION);
        Assert.assertEquals(AreaEducacionServiceTestData.getAreaEducacionDTO().getId(), areaEducacion.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link AreaEducacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_areaEducacion_no_existe() throws PmzException{
        areaEducacionFacadeAPI.guardar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link AreaEducacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_areaEducacion_existe() throws PmzException{
        areaEducacionFacadeAPI.editar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }
    
}
