package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.dao.api.EstadoActividadDaoAPI;
import co.com.fspb.mgs.service.api.EstadoActividadServiceAPI;
import co.com.fspb.mgs.service.impl.EstadoActividadServiceImpl;
import co.com.fspb.mgs.test.service.data.EstadoActividadServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EstadoActividadServiceAPI} de la entidad {@link EstadoActividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoActividadServiceAPI
 * @date nov 11, 2016
 */
public class EstadoActividadServiceTest {
	
    private static final Long ID_PRIMER_ESTADOACTIVIDAD = 1;

    @InjectMocks
    private EstadoActividadServiceAPI estadoActividadServiceAPI = new EstadoActividadServiceImpl();

    @Mock
    private EstadoActividadDaoAPI estadoActividadDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividads()).when(estadoActividadDaoAPI).getRecords(null);
        Mockito.doReturn(EstadoActividadServiceTestData.getTotalEstadoActividads()).when(estadoActividadDaoAPI).countRecords(null);
        PmzResultSet<EstadoActividadDTO> estadoActividads = estadoActividadServiceAPI.getRecords(null);
        Assert.assertEquals(EstadoActividadServiceTestData.getTotalEstadoActividads(), estadoActividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoActividad_existe(){
        Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(ID_PRIMER_ESTADOACTIVIDAD);
        EstadoActividadDTO estadoActividad = estadoActividadServiceAPI.getEstadoActividadDTO(ID_PRIMER_ESTADOACTIVIDAD);
        Assert.assertEquals(EstadoActividadServiceTestData.getEstadoActividad().getId(), estadoActividad.getId());
    }

    /**
     * Prueba unitaria de get con {@link EstadoActividadDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoActividad_no_existe(){
        Mockito.doReturn(null).when(estadoActividadDaoAPI).get(ID_PRIMER_ESTADOACTIVIDAD);
        EstadoActividadDTO estadoActividad = estadoActividadServiceAPI.getEstadoActividadDTO(ID_PRIMER_ESTADOACTIVIDAD);
        Assert.assertEquals(null, estadoActividad);
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoActividadDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_estadoActividad_no_existe() throws PmzException{
    	estadoActividadServiceAPI.guardar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_estadoActividad_existe() throws PmzException{
        Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(ID_PRIMER_ESTADOACTIVIDAD);
        estadoActividadServiceAPI.guardar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EstadoActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_estadoActividad_existe() throws PmzException{
        Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(ID_PRIMER_ESTADOACTIVIDAD);
        estadoActividadServiceAPI.editar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EstadoActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_estadoActividad_no_existe() throws PmzException{
        estadoActividadServiceAPI.editar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }
    
    
}
