package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.dao.api.AreaEducacionDaoAPI;
import co.com.fspb.mgs.service.api.AreaEducacionServiceAPI;
import co.com.fspb.mgs.service.impl.AreaEducacionServiceImpl;
import co.com.fspb.mgs.test.service.data.AreaEducacionServiceTestData;

/**
 * Prueba unitaria para el servicio {@link AreaEducacionServiceAPI} de la entidad {@link AreaEducacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AreaEducacionServiceAPI
 * @date nov 11, 2016
 */
public class AreaEducacionServiceTest {
	
    private static final Long ID_PRIMER_AREAEDUCACION = 1;

    @InjectMocks
    private AreaEducacionServiceAPI areaEducacionServiceAPI = new AreaEducacionServiceImpl();

    @Mock
    private AreaEducacionDaoAPI areaEducacionDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacions()).when(areaEducacionDaoAPI).getRecords(null);
        Mockito.doReturn(AreaEducacionServiceTestData.getTotalAreaEducacions()).when(areaEducacionDaoAPI).countRecords(null);
        PmzResultSet<AreaEducacionDTO> areaEducacions = areaEducacionServiceAPI.getRecords(null);
        Assert.assertEquals(AreaEducacionServiceTestData.getTotalAreaEducacions(), areaEducacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AreaEducacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_areaEducacion_existe(){
        Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(ID_PRIMER_AREAEDUCACION);
        AreaEducacionDTO areaEducacion = areaEducacionServiceAPI.getAreaEducacionDTO(ID_PRIMER_AREAEDUCACION);
        Assert.assertEquals(AreaEducacionServiceTestData.getAreaEducacion().getId(), areaEducacion.getId());
    }

    /**
     * Prueba unitaria de get con {@link AreaEducacionDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_areaEducacion_no_existe(){
        Mockito.doReturn(null).when(areaEducacionDaoAPI).get(ID_PRIMER_AREAEDUCACION);
        AreaEducacionDTO areaEducacion = areaEducacionServiceAPI.getAreaEducacionDTO(ID_PRIMER_AREAEDUCACION);
        Assert.assertEquals(null, areaEducacion);
    }

    /**
     * Prueba unitaria de registro de una {@link AreaEducacionDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_areaEducacion_no_existe() throws PmzException{
    	areaEducacionServiceAPI.guardar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link AreaEducacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_areaEducacion_existe() throws PmzException{
        Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(ID_PRIMER_AREAEDUCACION);
        areaEducacionServiceAPI.guardar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link AreaEducacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_areaEducacion_existe() throws PmzException{
        Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(ID_PRIMER_AREAEDUCACION);
        areaEducacionServiceAPI.editar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AreaEducacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_areaEducacion_no_existe() throws PmzException{
        areaEducacionServiceAPI.editar(AreaEducacionServiceTestData.getAreaEducacionDTO());
    }
    
    
}
