package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.service.api.IndicadorServiceAPI;
import co.com.fspb.mgs.service.impl.IndicadorServiceImpl;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;

/**
 * Prueba unitaria para el servicio {@link IndicadorServiceAPI} de la entidad {@link Indicador}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorServiceAPI
 * @date nov 11, 2016
 */
public class IndicadorServiceTest {
	
    private static final Long ID_PRIMER_INDICADOR = 1;

    @InjectMocks
    private IndicadorServiceAPI indicadorServiceAPI = new IndicadorServiceImpl();

    @Mock
    private IndicadorDaoAPI indicadorDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorServiceTestData.getIndicadors()).when(indicadorDaoAPI).getRecords(null);
        Mockito.doReturn(IndicadorServiceTestData.getTotalIndicadors()).when(indicadorDaoAPI).countRecords(null);
        PmzResultSet<IndicadorDTO> indicadors = indicadorServiceAPI.getRecords(null);
        Assert.assertEquals(IndicadorServiceTestData.getTotalIndicadors(), indicadors.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicador_existe(){
        Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(ID_PRIMER_INDICADOR);
        IndicadorDTO indicador = indicadorServiceAPI.getIndicadorDTO(ID_PRIMER_INDICADOR);
        Assert.assertEquals(IndicadorServiceTestData.getIndicador().getId(), indicador.getId());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicador_no_existe(){
        Mockito.doReturn(null).when(indicadorDaoAPI).get(ID_PRIMER_INDICADOR);
        IndicadorDTO indicador = indicadorServiceAPI.getIndicadorDTO(ID_PRIMER_INDICADOR);
        Assert.assertEquals(null, indicador);
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_indicador_no_existe() throws PmzException{
    	indicadorServiceAPI.guardar(IndicadorServiceTestData.getIndicadorDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_indicador_existe() throws PmzException{
        Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(ID_PRIMER_INDICADOR);
        indicadorServiceAPI.guardar(IndicadorServiceTestData.getIndicadorDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link IndicadorDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_indicador_existe() throws PmzException{
        Mockito.doReturn(IndicadorServiceTestData.getIndicador()).when(indicadorDaoAPI).get(ID_PRIMER_INDICADOR);
        indicadorServiceAPI.editar(IndicadorServiceTestData.getIndicadorDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IndicadorDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_indicador_no_existe() throws PmzException{
        indicadorServiceAPI.editar(IndicadorServiceTestData.getIndicadorDTO());
    }
    
    
}
