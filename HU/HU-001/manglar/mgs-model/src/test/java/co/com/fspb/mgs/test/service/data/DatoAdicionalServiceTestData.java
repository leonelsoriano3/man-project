package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.DatoAdicional;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link DatoAdicional}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicionalServiceTestData
 * @date nov 11, 2016
 */
public abstract class DatoAdicionalServiceTestData {
    
    private static List<DatoAdicional> entities = new ArrayList<DatoAdicional>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            DatoAdicional entity = new DatoAdicional();

            entity.setId(i);
            
            entity.setNombreDato("NombreDato" + i);
            entity.setProyecto(ProyectoServiceTestData.getProyecto());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DatoAdicional} Mock 
     * tranformado a DTO {@link DatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link DatoAdicionalDTO}
     */
    public static DatoAdicionalDTO getDatoAdicionalDTO(){
        return DTOTransformer.getDatoAdicionalDTOFromDatoAdicional(getDatoAdicional());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DatoAdicional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link DatoAdicional} Mock
     */
    public static DatoAdicional getDatoAdicional(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link DatoAdicional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link DatoAdicional}
     */
    public static List<DatoAdicional> getDatoAdicionals(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<DatoAdicionalDTO> getResultSetDatoAdicionalsDTO(){
        List<DatoAdicionalDTO> dtos = new ArrayList<DatoAdicionalDTO>();
        for (DatoAdicional entity : entities) {
            dtos.add(DTOTransformer.getDatoAdicionalDTOFromDatoAdicional(entity));
        }
        return new PmzResultSet<DatoAdicionalDTO>(dtos, getTotalDatoAdicionals(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalDatoAdicionals(){
        return Long.valueOf(entities.size());
    }

}
