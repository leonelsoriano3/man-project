package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI;
import co.com.fspb.mgs.facade.impl.DatoAdicionalFacadeImpl;
import co.com.fspb.mgs.service.api.DatoAdicionalServiceAPI;
import co.com.fspb.mgs.test.service.data.DatoAdicionalServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link DatoAdicionalFacadeAPI} de la entidad {@link DatoAdicional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicionalFacadeTest
 * @date nov 11, 2016
 */
public class DatoAdicionalFacadeTest {

    private static final Long ID_PRIMER_DATOADICIONAL = 1;

    @InjectMocks
    private DatoAdicionalFacadeAPI datoAdicionalFacadeAPI = new DatoAdicionalFacadeImpl();

    @Mock
    private DatoAdicionalServiceAPI datoAdicionalServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DatoAdicionalServiceTestData.getResultSetDatoAdicionalsDTO()).when(datoAdicionalServiceAPI).getRecords(null);
        PmzResultSet<DatoAdicionalDTO> datoAdicionals = datoAdicionalFacadeAPI.getRecords(null);
        Assert.assertEquals(DatoAdicionalServiceTestData.getTotalDatoAdicionals(), datoAdicionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DatoAdicionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoAdicional_existe(){
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicionalDTO()).when(datoAdicionalServiceAPI).getDatoAdicionalDTO(ID_PRIMER_DATOADICIONAL);
        DatoAdicionalDTO datoAdicional = datoAdicionalFacadeAPI.getDatoAdicionalDTO(ID_PRIMER_DATOADICIONAL);
        Assert.assertEquals(DatoAdicionalServiceTestData.getDatoAdicionalDTO().getId(), datoAdicional.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link DatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_datoAdicional_no_existe() throws PmzException{
        datoAdicionalFacadeAPI.guardar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link DatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_datoAdicional_existe() throws PmzException{
        datoAdicionalFacadeAPI.editar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    
}
