package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI;
import co.com.fspb.mgs.facade.impl.EstadoActividadFacadeImpl;
import co.com.fspb.mgs.service.api.EstadoActividadServiceAPI;
import co.com.fspb.mgs.test.service.data.EstadoActividadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EstadoActividadFacadeAPI} de la entidad {@link EstadoActividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoActividadFacadeTest
 * @date nov 11, 2016
 */
public class EstadoActividadFacadeTest {

    private static final Long ID_PRIMER_ESTADOACTIVIDAD = 1;

    @InjectMocks
    private EstadoActividadFacadeAPI estadoActividadFacadeAPI = new EstadoActividadFacadeImpl();

    @Mock
    private EstadoActividadServiceAPI estadoActividadServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoActividadServiceTestData.getResultSetEstadoActividadsDTO()).when(estadoActividadServiceAPI).getRecords(null);
        PmzResultSet<EstadoActividadDTO> estadoActividads = estadoActividadFacadeAPI.getRecords(null);
        Assert.assertEquals(EstadoActividadServiceTestData.getTotalEstadoActividads(), estadoActividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoActividad_existe(){
        Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividadDTO()).when(estadoActividadServiceAPI).getEstadoActividadDTO(ID_PRIMER_ESTADOACTIVIDAD);
        EstadoActividadDTO estadoActividad = estadoActividadFacadeAPI.getEstadoActividadDTO(ID_PRIMER_ESTADOACTIVIDAD);
        Assert.assertEquals(EstadoActividadServiceTestData.getEstadoActividadDTO().getId(), estadoActividad.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_estadoActividad_no_existe() throws PmzException{
        estadoActividadFacadeAPI.guardar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EstadoActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_estadoActividad_existe() throws PmzException{
        estadoActividadFacadeAPI.editar(EstadoActividadServiceTestData.getEstadoActividadDTO());
    }
    
}
