package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.EstadoCivil;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link EstadoCivil}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoCivilServiceTestData
 * @date nov 11, 2016
 */
public abstract class EstadoCivilServiceTestData {
    
    private static List<EstadoCivil> entities = new ArrayList<EstadoCivil>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            EstadoCivil entity = new EstadoCivil();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstadoCivil} Mock 
     * tranformado a DTO {@link EstadoCivilDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EstadoCivilDTO}
     */
    public static EstadoCivilDTO getEstadoCivilDTO(){
        return DTOTransformer.getEstadoCivilDTOFromEstadoCivil(getEstadoCivil());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstadoCivil} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link EstadoCivil} Mock
     */
    public static EstadoCivil getEstadoCivil(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link EstadoCivil} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link EstadoCivil}
     */
    public static List<EstadoCivil> getEstadoCivils(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EstadoCivilDTO> getResultSetEstadoCivilsDTO(){
        List<EstadoCivilDTO> dtos = new ArrayList<EstadoCivilDTO>();
        for (EstadoCivil entity : entities) {
            dtos.add(DTOTransformer.getEstadoCivilDTOFromEstadoCivil(entity));
        }
        return new PmzResultSet<EstadoCivilDTO>(dtos, getTotalEstadoCivils(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEstadoCivils(){
        return Long.valueOf(entities.size());
    }

}
