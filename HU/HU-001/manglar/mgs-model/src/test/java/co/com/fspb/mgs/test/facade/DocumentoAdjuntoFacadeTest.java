package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;
import co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI;
import co.com.fspb.mgs.facade.impl.DocumentoAdjuntoFacadeImpl;
import co.com.fspb.mgs.service.api.DocumentoAdjuntoServiceAPI;
import co.com.fspb.mgs.test.service.data.DocumentoAdjuntoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link DocumentoAdjuntoFacadeAPI} de la entidad {@link DocumentoAdjunto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DocumentoAdjuntoFacadeTest
 * @date nov 11, 2016
 */
public class DocumentoAdjuntoFacadeTest {

    private static final Long ID_PRIMER_DOCUMENTOADJUNTO = 1;

    @InjectMocks
    private DocumentoAdjuntoFacadeAPI documentoAdjuntoFacadeAPI = new DocumentoAdjuntoFacadeImpl();

    @Mock
    private DocumentoAdjuntoServiceAPI documentoAdjuntoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getResultSetDocumentoAdjuntosDTO()).when(documentoAdjuntoServiceAPI).getRecords(null);
        PmzResultSet<DocumentoAdjuntoDTO> documentoAdjuntos = documentoAdjuntoFacadeAPI.getRecords(null);
        Assert.assertEquals(DocumentoAdjuntoServiceTestData.getTotalDocumentoAdjuntos(), documentoAdjuntos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DocumentoAdjuntoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_documentoAdjunto_existe(){
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO()).when(documentoAdjuntoServiceAPI).getDocumentoAdjuntoDTO(ID_PRIMER_DOCUMENTOADJUNTO);
        DocumentoAdjuntoDTO documentoAdjunto = documentoAdjuntoFacadeAPI.getDocumentoAdjuntoDTO(ID_PRIMER_DOCUMENTOADJUNTO);
        Assert.assertEquals(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO().getId(), documentoAdjunto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link DocumentoAdjuntoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_documentoAdjunto_no_existe() throws PmzException{
        documentoAdjuntoFacadeAPI.guardar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link DocumentoAdjuntoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_documentoAdjunto_existe() throws PmzException{
        documentoAdjuntoFacadeAPI.editar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    
}
