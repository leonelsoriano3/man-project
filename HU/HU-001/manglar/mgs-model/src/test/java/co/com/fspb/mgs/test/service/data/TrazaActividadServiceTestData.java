package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TrazaActividad;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TrazaActividad}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadServiceTestData
 * @date nov 11, 2016
 */
public abstract class TrazaActividadServiceTestData {
    
    private static List<TrazaActividad> entities = new ArrayList<TrazaActividad>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TrazaActividad entity = new TrazaActividad();

            entity.setId(i);
            
            entity.setFecha(new Date());
            entity.setUsuario("Usuario" + i);
            entity.setEstadoActividad("EstadoActividad" + i);
            entity.setActividad(ActividadServiceTestData.getActividad());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TrazaActividad} Mock 
     * tranformado a DTO {@link TrazaActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TrazaActividadDTO}
     */
    public static TrazaActividadDTO getTrazaActividadDTO(){
        return DTOTransformer.getTrazaActividadDTOFromTrazaActividad(getTrazaActividad());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TrazaActividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TrazaActividad} Mock
     */
    public static TrazaActividad getTrazaActividad(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TrazaActividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TrazaActividad}
     */
    public static List<TrazaActividad> getTrazaActividads(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TrazaActividadDTO> getResultSetTrazaActividadsDTO(){
        List<TrazaActividadDTO> dtos = new ArrayList<TrazaActividadDTO>();
        for (TrazaActividad entity : entities) {
            dtos.add(DTOTransformer.getTrazaActividadDTOFromTrazaActividad(entity));
        }
        return new PmzResultSet<TrazaActividadDTO>(dtos, getTotalTrazaActividads(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTrazaActividads(){
        return Long.valueOf(entities.size());
    }

}
