package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.ValorDatoAdicional;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ValorDatoAdicional}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ValorDatoAdicionalServiceTestData
 * @date nov 11, 2016
 */
public abstract class ValorDatoAdicionalServiceTestData {
    
    private static List<ValorDatoAdicional> entities = new ArrayList<ValorDatoAdicional>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ValorDatoAdicional entity = new ValorDatoAdicional();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setDatoAdicional(DatoAdicionalServiceTestData.getDatoAdicional());
            entity.setFechaCrea(new Date());
            entity.setBeneficiario(BeneficiarioServiceTestData.getBeneficiario());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ValorDatoAdicional} Mock 
     * tranformado a DTO {@link ValorDatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ValorDatoAdicionalDTO}
     */
    public static ValorDatoAdicionalDTO getValorDatoAdicionalDTO(){
        return DTOTransformer.getValorDatoAdicionalDTOFromValorDatoAdicional(getValorDatoAdicional());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ValorDatoAdicional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ValorDatoAdicional} Mock
     */
    public static ValorDatoAdicional getValorDatoAdicional(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ValorDatoAdicional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ValorDatoAdicional}
     */
    public static List<ValorDatoAdicional> getValorDatoAdicionals(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ValorDatoAdicionalDTO> getResultSetValorDatoAdicionalsDTO(){
        List<ValorDatoAdicionalDTO> dtos = new ArrayList<ValorDatoAdicionalDTO>();
        for (ValorDatoAdicional entity : entities) {
            dtos.add(DTOTransformer.getValorDatoAdicionalDTOFromValorDatoAdicional(entity));
        }
        return new PmzResultSet<ValorDatoAdicionalDTO>(dtos, getTotalValorDatoAdicionals(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalValorDatoAdicionals(){
        return Long.valueOf(entities.size());
    }

}
