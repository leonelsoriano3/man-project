package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.dao.api.DatoAdicionalDaoAPI;
import co.com.fspb.mgs.service.api.DatoAdicionalServiceAPI;
import co.com.fspb.mgs.service.impl.DatoAdicionalServiceImpl;
import co.com.fspb.mgs.test.service.data.DatoAdicionalServiceTestData;
  import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link DatoAdicionalServiceAPI} de la entidad {@link DatoAdicional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicionalServiceAPI
 * @date nov 11, 2016
 */
public class DatoAdicionalServiceTest {
	
    private static final Long ID_PRIMER_DATOADICIONAL = 1;

    @InjectMocks
    private DatoAdicionalServiceAPI datoAdicionalServiceAPI = new DatoAdicionalServiceImpl();

    @Mock
    private DatoAdicionalDaoAPI datoAdicionalDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicionals()).when(datoAdicionalDaoAPI).getRecords(null);
        Mockito.doReturn(DatoAdicionalServiceTestData.getTotalDatoAdicionals()).when(datoAdicionalDaoAPI).countRecords(null);
        PmzResultSet<DatoAdicionalDTO> datoAdicionals = datoAdicionalServiceAPI.getRecords(null);
        Assert.assertEquals(DatoAdicionalServiceTestData.getTotalDatoAdicionals(), datoAdicionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DatoAdicionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoAdicional_existe(){
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(ID_PRIMER_DATOADICIONAL);
        DatoAdicionalDTO datoAdicional = datoAdicionalServiceAPI.getDatoAdicionalDTO(ID_PRIMER_DATOADICIONAL);
        Assert.assertEquals(DatoAdicionalServiceTestData.getDatoAdicional().getId(), datoAdicional.getId());
    }

    /**
     * Prueba unitaria de get con {@link DatoAdicionalDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoAdicional_no_existe(){
        Mockito.doReturn(null).when(datoAdicionalDaoAPI).get(ID_PRIMER_DATOADICIONAL);
        DatoAdicionalDTO datoAdicional = datoAdicionalServiceAPI.getDatoAdicionalDTO(ID_PRIMER_DATOADICIONAL);
        Assert.assertEquals(null, datoAdicional);
    }

    /**
     * Prueba unitaria de registro de una {@link DatoAdicionalDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_datoAdicional_no_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	datoAdicionalServiceAPI.guardar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link DatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_datoAdicional_existe() throws PmzException{
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(ID_PRIMER_DATOADICIONAL);
        datoAdicionalServiceAPI.guardar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link DatoAdicionalDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_datoAdicional_no_existe_no_proyecto() throws PmzException{
    	datoAdicionalServiceAPI.guardar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link DatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_datoAdicional_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(ID_PRIMER_DATOADICIONAL);
        datoAdicionalServiceAPI.editar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_datoAdicional_no_existe() throws PmzException{
        datoAdicionalServiceAPI.editar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DatoAdicionalDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_datoAdicional_existe_no_proyecto() throws PmzException{
        Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(ID_PRIMER_DATOADICIONAL);
    	datoAdicionalServiceAPI.editar(DatoAdicionalServiceTestData.getDatoAdicionalDTO());
    }
    
    
}
