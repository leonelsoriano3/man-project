package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.IndicadorObjetivoPK;
import co.com.fspb.mgs.dao.model.IndicadorObjetivo;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link IndicadorObjetivo}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoServiceTestData
 * @date nov 11, 2016
 */
public abstract class IndicadorObjetivoServiceTestData {
    
    private static List<IndicadorObjetivo> entities = new ArrayList<IndicadorObjetivo>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            IndicadorObjetivo entity = new IndicadorObjetivo();

		    IndicadorObjetivoPK pk = new IndicadorObjetivoPK();
            pk.setIndicador(IndicadorServiceTestData.getIndicador());
            pk.setObjetivo(ObjetivoServiceTestData.getObjetivo());
            entity.setId(pk);
            
            entity.setResultado("Resultado" + i);
            entity.setMeta("Meta" + i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link IndicadorObjetivo} Mock 
     * tranformado a DTO {@link IndicadorObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link IndicadorObjetivoDTO}
     */
    public static IndicadorObjetivoDTO getIndicadorObjetivoDTO(){
        return DTOTransformer.getIndicadorObjetivoDTOFromIndicadorObjetivo(getIndicadorObjetivo());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link IndicadorObjetivo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link IndicadorObjetivo} Mock
     */
    public static IndicadorObjetivo getIndicadorObjetivo(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link IndicadorObjetivo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link IndicadorObjetivo}
     */
    public static List<IndicadorObjetivo> getIndicadorObjetivos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<IndicadorObjetivoDTO> getResultSetIndicadorObjetivosDTO(){
        List<IndicadorObjetivoDTO> dtos = new ArrayList<IndicadorObjetivoDTO>();
        for (IndicadorObjetivo entity : entities) {
            dtos.add(DTOTransformer.getIndicadorObjetivoDTOFromIndicadorObjetivo(entity));
        }
        return new PmzResultSet<IndicadorObjetivoDTO>(dtos, getTotalIndicadorObjetivos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalIndicadorObjetivos(){
        return Long.valueOf(entities.size());
    }

}
