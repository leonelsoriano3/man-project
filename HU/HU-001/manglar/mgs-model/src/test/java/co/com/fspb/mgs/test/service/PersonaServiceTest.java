package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.service.api.PersonaServiceAPI;
import co.com.fspb.mgs.service.impl.PersonaServiceImpl;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link PersonaServiceAPI} de la entidad {@link Persona}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaServiceAPI
 * @date nov 11, 2016
 */
public class PersonaServiceTest {
	
    private static final Long ID_PRIMER_PERSONA = 1;

    @InjectMocks
    private PersonaServiceAPI personaServiceAPI = new PersonaServiceImpl();

    @Mock
    private PersonaDaoAPI personaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(PersonaServiceTestData.getPersonas()).when(personaDaoAPI).getRecords(null);
        Mockito.doReturn(PersonaServiceTestData.getTotalPersonas()).when(personaDaoAPI).countRecords(null);
        PmzResultSet<PersonaDTO> personas = personaServiceAPI.getRecords(null);
        Assert.assertEquals(PersonaServiceTestData.getTotalPersonas(), personas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link PersonaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_persona_existe(){
        Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(ID_PRIMER_PERSONA);
        PersonaDTO persona = personaServiceAPI.getPersonaDTO(ID_PRIMER_PERSONA);
        Assert.assertEquals(PersonaServiceTestData.getPersona().getId(), persona.getId());
    }

    /**
     * Prueba unitaria de get con {@link PersonaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_persona_no_existe(){
        Mockito.doReturn(null).when(personaDaoAPI).get(ID_PRIMER_PERSONA);
        PersonaDTO persona = personaServiceAPI.getPersonaDTO(ID_PRIMER_PERSONA);
        Assert.assertEquals(null, persona);
    }

    /**
     * Prueba unitaria de registro de una {@link PersonaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_persona_no_existe() throws PmzException{
    	personaServiceAPI.guardar(PersonaServiceTestData.getPersonaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link PersonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_persona_existe() throws PmzException{
        Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(ID_PRIMER_PERSONA);
        personaServiceAPI.guardar(PersonaServiceTestData.getPersonaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link PersonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_persona_existe() throws PmzException{
        Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(ID_PRIMER_PERSONA);
        personaServiceAPI.editar(PersonaServiceTestData.getPersonaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link PersonaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_persona_no_existe() throws PmzException{
        personaServiceAPI.editar(PersonaServiceTestData.getPersonaDTO());
    }
    
    
}
