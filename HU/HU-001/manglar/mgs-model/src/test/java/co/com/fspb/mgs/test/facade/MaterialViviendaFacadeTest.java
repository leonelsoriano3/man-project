package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI;
import co.com.fspb.mgs.facade.impl.MaterialViviendaFacadeImpl;
import co.com.fspb.mgs.service.api.MaterialViviendaServiceAPI;
import co.com.fspb.mgs.test.service.data.MaterialViviendaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link MaterialViviendaFacadeAPI} de la entidad {@link MaterialVivienda}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MaterialViviendaFacadeTest
 * @date nov 11, 2016
 */
public class MaterialViviendaFacadeTest {

    private static final Long ID_PRIMER_MATERIALVIVIENDA = 1;

    @InjectMocks
    private MaterialViviendaFacadeAPI materialViviendaFacadeAPI = new MaterialViviendaFacadeImpl();

    @Mock
    private MaterialViviendaServiceAPI materialViviendaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(MaterialViviendaServiceTestData.getResultSetMaterialViviendasDTO()).when(materialViviendaServiceAPI).getRecords(null);
        PmzResultSet<MaterialViviendaDTO> materialViviendas = materialViviendaFacadeAPI.getRecords(null);
        Assert.assertEquals(MaterialViviendaServiceTestData.getTotalMaterialViviendas(), materialViviendas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link MaterialViviendaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_materialVivienda_existe(){
        Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialViviendaDTO()).when(materialViviendaServiceAPI).getMaterialViviendaDTO(ID_PRIMER_MATERIALVIVIENDA);
        MaterialViviendaDTO materialVivienda = materialViviendaFacadeAPI.getMaterialViviendaDTO(ID_PRIMER_MATERIALVIVIENDA);
        Assert.assertEquals(MaterialViviendaServiceTestData.getMaterialViviendaDTO().getId(), materialVivienda.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link MaterialViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_materialVivienda_no_existe() throws PmzException{
        materialViviendaFacadeAPI.guardar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link MaterialViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_materialVivienda_existe() throws PmzException{
        materialViviendaFacadeAPI.editar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }
    
}
