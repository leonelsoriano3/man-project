package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.facade.api.ActividadFacadeAPI;
import co.com.fspb.mgs.facade.impl.ActividadFacadeImpl;
import co.com.fspb.mgs.service.api.ActividadServiceAPI;
import co.com.fspb.mgs.test.service.data.ActividadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ActividadFacadeAPI} de la entidad {@link Actividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ActividadFacadeTest
 * @date nov 11, 2016
 */
public class ActividadFacadeTest {

    private static final Long ID_PRIMER_ACTIVIDAD = 1;

    @InjectMocks
    private ActividadFacadeAPI actividadFacadeAPI = new ActividadFacadeImpl();

    @Mock
    private ActividadServiceAPI actividadServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ActividadServiceTestData.getResultSetActividadsDTO()).when(actividadServiceAPI).getRecords(null);
        PmzResultSet<ActividadDTO> actividads = actividadFacadeAPI.getRecords(null);
        Assert.assertEquals(ActividadServiceTestData.getTotalActividads(), actividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_actividad_existe(){
        Mockito.doReturn(ActividadServiceTestData.getActividadDTO()).when(actividadServiceAPI).getActividadDTO(ID_PRIMER_ACTIVIDAD);
        ActividadDTO actividad = actividadFacadeAPI.getActividadDTO(ID_PRIMER_ACTIVIDAD);
        Assert.assertEquals(ActividadServiceTestData.getActividadDTO().getId(), actividad.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_actividad_no_existe() throws PmzException{
        actividadFacadeAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_actividad_existe() throws PmzException{
        actividadFacadeAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
}
