package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Ocupacion;
import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Ocupacion}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class OcupacionServiceTestData
 * @date nov 11, 2016
 */
public abstract class OcupacionServiceTestData {
    
    private static List<Ocupacion> entities = new ArrayList<Ocupacion>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Ocupacion entity = new Ocupacion();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Ocupacion} Mock 
     * tranformado a DTO {@link OcupacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link OcupacionDTO}
     */
    public static OcupacionDTO getOcupacionDTO(){
        return DTOTransformer.getOcupacionDTOFromOcupacion(getOcupacion());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Ocupacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Ocupacion} Mock
     */
    public static Ocupacion getOcupacion(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Ocupacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Ocupacion}
     */
    public static List<Ocupacion> getOcupacions(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<OcupacionDTO> getResultSetOcupacionsDTO(){
        List<OcupacionDTO> dtos = new ArrayList<OcupacionDTO>();
        for (Ocupacion entity : entities) {
            dtos.add(DTOTransformer.getOcupacionDTOFromOcupacion(entity));
        }
        return new PmzResultSet<OcupacionDTO>(dtos, getTotalOcupacions(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalOcupacions(){
        return Long.valueOf(entities.size());
    }

}
