package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI;
import co.com.fspb.mgs.facade.impl.AfiliadoSaludFacadeImpl;
import co.com.fspb.mgs.service.api.AfiliadoSaludServiceAPI;
import co.com.fspb.mgs.test.service.data.AfiliadoSaludServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link AfiliadoSaludFacadeAPI} de la entidad {@link AfiliadoSalud}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludFacadeTest
 * @date nov 11, 2016
 */
public class AfiliadoSaludFacadeTest {

    private static final Long ID_PRIMER_AFILIADOSALUD = 1;

    @InjectMocks
    private AfiliadoSaludFacadeAPI afiliadoSaludFacadeAPI = new AfiliadoSaludFacadeImpl();

    @Mock
    private AfiliadoSaludServiceAPI afiliadoSaludServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AfiliadoSaludServiceTestData.getResultSetAfiliadoSaludsDTO()).when(afiliadoSaludServiceAPI).getRecords(null);
        PmzResultSet<AfiliadoSaludDTO> afiliadoSaluds = afiliadoSaludFacadeAPI.getRecords(null);
        Assert.assertEquals(AfiliadoSaludServiceTestData.getTotalAfiliadoSaluds(), afiliadoSaluds.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AfiliadoSaludDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_afiliadoSalud_existe(){
        Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO()).when(afiliadoSaludServiceAPI).getAfiliadoSaludDTO(ID_PRIMER_AFILIADOSALUD);
        AfiliadoSaludDTO afiliadoSalud = afiliadoSaludFacadeAPI.getAfiliadoSaludDTO(ID_PRIMER_AFILIADOSALUD);
        Assert.assertEquals(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO().getId(), afiliadoSalud.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link AfiliadoSaludDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_afiliadoSalud_no_existe() throws PmzException{
        afiliadoSaludFacadeAPI.guardar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link AfiliadoSaludDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_afiliadoSalud_existe() throws PmzException{
        afiliadoSaludFacadeAPI.editar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }
    
}
