package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.facade.api.UsuarioFacadeAPI;
import co.com.fspb.mgs.facade.impl.UsuarioFacadeImpl;
import co.com.fspb.mgs.service.api.UsuarioServiceAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link UsuarioFacadeAPI} de la entidad {@link Usuario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class UsuarioFacadeTest
 * @date nov 11, 2016
 */
public class UsuarioFacadeTest {

    private static final Long ID_PRIMER_USUARIO = 1;

    @InjectMocks
    private UsuarioFacadeAPI usuarioFacadeAPI = new UsuarioFacadeImpl();

    @Mock
    private UsuarioServiceAPI usuarioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(UsuarioServiceTestData.getResultSetUsuariosDTO()).when(usuarioServiceAPI).getRecords(null);
        PmzResultSet<UsuarioDTO> usuarios = usuarioFacadeAPI.getRecords(null);
        Assert.assertEquals(UsuarioServiceTestData.getTotalUsuarios(), usuarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link UsuarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_usuario_existe(){
        Mockito.doReturn(UsuarioServiceTestData.getUsuarioDTO()).when(usuarioServiceAPI).getUsuarioDTO(ID_PRIMER_USUARIO);
        UsuarioDTO usuario = usuarioFacadeAPI.getUsuarioDTO(ID_PRIMER_USUARIO);
        Assert.assertEquals(UsuarioServiceTestData.getUsuarioDTO().getId(), usuario.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link UsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_usuario_no_existe() throws PmzException{
        usuarioFacadeAPI.guardar(UsuarioServiceTestData.getUsuarioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link UsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_usuario_existe() throws PmzException{
        usuarioFacadeAPI.editar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
}
