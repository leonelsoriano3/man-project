package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Actividad;
import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Actividad}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ActividadServiceTestData
 * @date nov 11, 2016
 */
public abstract class ActividadServiceTestData {
    
    private static List<Actividad> entities = new ArrayList<Actividad>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Actividad entity = new Actividad();

            entity.setId(i);
            
            entity.setComponente(ComponenteServiceTestData.getComponente());
            entity.setFechaModifica(new Date());
            entity.setResultadoObtenido("ResultadoObtenido" + i);
            entity.setEstadoActividad(EstadoActividadServiceTestData.getEstadoActividad());
            entity.setObservacionSeg("ObservacionSeg" + i);
            entity.setResultadoEsperado("ResultadoEsperado" + i);
            entity.setFechaSeguimiento(new Date());
            entity.setNombre("Nombre" + i);
            entity.setRecursos("Recursos" + i);
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setCantAsistentes(i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setUsuario(UsuarioServiceTestData.getUsuario());
            entity.setFechaEjecucion(new Date());
            entity.setFechaCrea(new Date());
            entity.setLeccionesAprendidas("LeccionesAprendidas" + i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Actividad} Mock 
     * tranformado a DTO {@link ActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ActividadDTO}
     */
    public static ActividadDTO getActividadDTO(){
        return DTOTransformer.getActividadDTOFromActividad(getActividad());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Actividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Actividad} Mock
     */
    public static Actividad getActividad(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Actividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Actividad}
     */
    public static List<Actividad> getActividads(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ActividadDTO> getResultSetActividadsDTO(){
        List<ActividadDTO> dtos = new ArrayList<ActividadDTO>();
        for (Actividad entity : entities) {
            dtos.add(DTOTransformer.getActividadDTOFromActividad(entity));
        }
        return new PmzResultSet<ActividadDTO>(dtos, getTotalActividads(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalActividads(){
        return Long.valueOf(entities.size());
    }

}
