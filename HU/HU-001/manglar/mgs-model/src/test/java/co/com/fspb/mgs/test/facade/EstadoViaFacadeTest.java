package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoViaDTO;
import co.com.fspb.mgs.facade.api.EstadoViaFacadeAPI;
import co.com.fspb.mgs.facade.impl.EstadoViaFacadeImpl;
import co.com.fspb.mgs.service.api.EstadoViaServiceAPI;
import co.com.fspb.mgs.test.service.data.EstadoViaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EstadoViaFacadeAPI} de la entidad {@link EstadoVia}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoViaFacadeTest
 * @date nov 11, 2016
 */
public class EstadoViaFacadeTest {

    private static final Long ID_PRIMER_ESTADOVIA = 1;

    @InjectMocks
    private EstadoViaFacadeAPI estadoViaFacadeAPI = new EstadoViaFacadeImpl();

    @Mock
    private EstadoViaServiceAPI estadoViaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoViaServiceTestData.getResultSetEstadoViasDTO()).when(estadoViaServiceAPI).getRecords(null);
        PmzResultSet<EstadoViaDTO> estadoVias = estadoViaFacadeAPI.getRecords(null);
        Assert.assertEquals(EstadoViaServiceTestData.getTotalEstadoVias(), estadoVias.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoViaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoVia_existe(){
        Mockito.doReturn(EstadoViaServiceTestData.getEstadoViaDTO()).when(estadoViaServiceAPI).getEstadoViaDTO(ID_PRIMER_ESTADOVIA);
        EstadoViaDTO estadoVia = estadoViaFacadeAPI.getEstadoViaDTO(ID_PRIMER_ESTADOVIA);
        Assert.assertEquals(EstadoViaServiceTestData.getEstadoViaDTO().getId(), estadoVia.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoViaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_estadoVia_no_existe() throws PmzException{
        estadoViaFacadeAPI.guardar(EstadoViaServiceTestData.getEstadoViaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EstadoViaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_estadoVia_existe() throws PmzException{
        estadoViaFacadeAPI.editar(EstadoViaServiceTestData.getEstadoViaDTO());
    }
    
}
