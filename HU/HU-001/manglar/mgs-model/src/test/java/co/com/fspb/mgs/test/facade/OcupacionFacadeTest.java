package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.facade.api.OcupacionFacadeAPI;
import co.com.fspb.mgs.facade.impl.OcupacionFacadeImpl;
import co.com.fspb.mgs.service.api.OcupacionServiceAPI;
import co.com.fspb.mgs.test.service.data.OcupacionServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link OcupacionFacadeAPI} de la entidad {@link Ocupacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class OcupacionFacadeTest
 * @date nov 11, 2016
 */
public class OcupacionFacadeTest {

    private static final Long ID_PRIMER_OCUPACION = 1;

    @InjectMocks
    private OcupacionFacadeAPI ocupacionFacadeAPI = new OcupacionFacadeImpl();

    @Mock
    private OcupacionServiceAPI ocupacionServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(OcupacionServiceTestData.getResultSetOcupacionsDTO()).when(ocupacionServiceAPI).getRecords(null);
        PmzResultSet<OcupacionDTO> ocupacions = ocupacionFacadeAPI.getRecords(null);
        Assert.assertEquals(OcupacionServiceTestData.getTotalOcupacions(), ocupacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link OcupacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_ocupacion_existe(){
        Mockito.doReturn(OcupacionServiceTestData.getOcupacionDTO()).when(ocupacionServiceAPI).getOcupacionDTO(ID_PRIMER_OCUPACION);
        OcupacionDTO ocupacion = ocupacionFacadeAPI.getOcupacionDTO(ID_PRIMER_OCUPACION);
        Assert.assertEquals(OcupacionServiceTestData.getOcupacionDTO().getId(), ocupacion.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link OcupacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_ocupacion_no_existe() throws PmzException{
        ocupacionFacadeAPI.guardar(OcupacionServiceTestData.getOcupacionDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link OcupacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_ocupacion_existe() throws PmzException{
        ocupacionFacadeAPI.editar(OcupacionServiceTestData.getOcupacionDTO());
    }
    
}
