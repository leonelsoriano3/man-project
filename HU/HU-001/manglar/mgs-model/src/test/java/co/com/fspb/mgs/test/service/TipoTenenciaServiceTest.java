package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.dao.api.TipoTenenciaDaoAPI;
import co.com.fspb.mgs.service.api.TipoTenenciaServiceAPI;
import co.com.fspb.mgs.service.impl.TipoTenenciaServiceImpl;
import co.com.fspb.mgs.test.service.data.TipoTenenciaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TipoTenenciaServiceAPI} de la entidad {@link TipoTenencia}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTenenciaServiceAPI
 * @date nov 11, 2016
 */
public class TipoTenenciaServiceTest {
	
    private static final Long ID_PRIMER_TIPOTENENCIA = 1;

    @InjectMocks
    private TipoTenenciaServiceAPI tipoTenenciaServiceAPI = new TipoTenenciaServiceImpl();

    @Mock
    private TipoTenenciaDaoAPI tipoTenenciaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencias()).when(tipoTenenciaDaoAPI).getRecords(null);
        Mockito.doReturn(TipoTenenciaServiceTestData.getTotalTipoTenencias()).when(tipoTenenciaDaoAPI).countRecords(null);
        PmzResultSet<TipoTenenciaDTO> tipoTenencias = tipoTenenciaServiceAPI.getRecords(null);
        Assert.assertEquals(TipoTenenciaServiceTestData.getTotalTipoTenencias(), tipoTenencias.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoTenenciaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTenencia_existe(){
        Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(ID_PRIMER_TIPOTENENCIA);
        TipoTenenciaDTO tipoTenencia = tipoTenenciaServiceAPI.getTipoTenenciaDTO(ID_PRIMER_TIPOTENENCIA);
        Assert.assertEquals(TipoTenenciaServiceTestData.getTipoTenencia().getId(), tipoTenencia.getId());
    }

    /**
     * Prueba unitaria de get con {@link TipoTenenciaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTenencia_no_existe(){
        Mockito.doReturn(null).when(tipoTenenciaDaoAPI).get(ID_PRIMER_TIPOTENENCIA);
        TipoTenenciaDTO tipoTenencia = tipoTenenciaServiceAPI.getTipoTenenciaDTO(ID_PRIMER_TIPOTENENCIA);
        Assert.assertEquals(null, tipoTenencia);
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTenenciaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_tipoTenencia_no_existe() throws PmzException{
    	tipoTenenciaServiceAPI.guardar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTenenciaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoTenencia_existe() throws PmzException{
        Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(ID_PRIMER_TIPOTENENCIA);
        tipoTenenciaServiceAPI.guardar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TipoTenenciaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_tipoTenencia_existe() throws PmzException{
        Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(ID_PRIMER_TIPOTENENCIA);
        tipoTenenciaServiceAPI.editar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoTenenciaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_tipoTenencia_no_existe() throws PmzException{
        tipoTenenciaServiceAPI.editar(TipoTenenciaServiceTestData.getTipoTenenciaDTO());
    }
    
    
}
