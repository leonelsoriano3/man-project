package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.EstadoActividad;
import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link EstadoActividad}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoActividadServiceTestData
 * @date nov 11, 2016
 */
public abstract class EstadoActividadServiceTestData {
    
    private static List<EstadoActividad> entities = new ArrayList<EstadoActividad>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            EstadoActividad entity = new EstadoActividad();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstadoActividad} Mock 
     * tranformado a DTO {@link EstadoActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EstadoActividadDTO}
     */
    public static EstadoActividadDTO getEstadoActividadDTO(){
        return DTOTransformer.getEstadoActividadDTOFromEstadoActividad(getEstadoActividad());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstadoActividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link EstadoActividad} Mock
     */
    public static EstadoActividad getEstadoActividad(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link EstadoActividad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link EstadoActividad}
     */
    public static List<EstadoActividad> getEstadoActividads(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EstadoActividadDTO> getResultSetEstadoActividadsDTO(){
        List<EstadoActividadDTO> dtos = new ArrayList<EstadoActividadDTO>();
        for (EstadoActividad entity : entities) {
            dtos.add(DTOTransformer.getEstadoActividadDTOFromEstadoActividad(entity));
        }
        return new PmzResultSet<EstadoActividadDTO>(dtos, getTotalEstadoActividads(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEstadoActividads(){
        return Long.valueOf(entities.size());
    }

}
