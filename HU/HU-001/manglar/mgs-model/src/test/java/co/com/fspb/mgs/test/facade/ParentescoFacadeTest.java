package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParentescoDTO;
import co.com.fspb.mgs.facade.api.ParentescoFacadeAPI;
import co.com.fspb.mgs.facade.impl.ParentescoFacadeImpl;
import co.com.fspb.mgs.service.api.ParentescoServiceAPI;
import co.com.fspb.mgs.test.service.data.ParentescoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ParentescoFacadeAPI} de la entidad {@link Parentesco}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoFacadeTest
 * @date nov 11, 2016
 */
public class ParentescoFacadeTest {

    private static final Long ID_PRIMER_PARENTESCO = 1;

    @InjectMocks
    private ParentescoFacadeAPI parentescoFacadeAPI = new ParentescoFacadeImpl();

    @Mock
    private ParentescoServiceAPI parentescoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParentescoServiceTestData.getResultSetParentescosDTO()).when(parentescoServiceAPI).getRecords(null);
        PmzResultSet<ParentescoDTO> parentescos = parentescoFacadeAPI.getRecords(null);
        Assert.assertEquals(ParentescoServiceTestData.getTotalParentescos(), parentescos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParentescoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parentesco_existe(){
        Mockito.doReturn(ParentescoServiceTestData.getParentescoDTO()).when(parentescoServiceAPI).getParentescoDTO(ID_PRIMER_PARENTESCO);
        ParentescoDTO parentesco = parentescoFacadeAPI.getParentescoDTO(ID_PRIMER_PARENTESCO);
        Assert.assertEquals(ParentescoServiceTestData.getParentescoDTO().getId(), parentesco.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ParentescoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_parentesco_no_existe() throws PmzException{
        parentescoFacadeAPI.guardar(ParentescoServiceTestData.getParentescoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ParentescoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_parentesco_existe() throws PmzException{
        parentescoFacadeAPI.editar(ParentescoServiceTestData.getParentescoDTO());
    }
    
}
