package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TipoTerritorioDTO;
import co.com.fspb.mgs.dao.api.TipoTerritorioDaoAPI;
import co.com.fspb.mgs.service.api.TipoTerritorioServiceAPI;
import co.com.fspb.mgs.service.impl.TipoTerritorioServiceImpl;
import co.com.fspb.mgs.test.service.data.TipoTerritorioServiceTestData;
  import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.test.service.data.ZonaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TipoTerritorioServiceAPI} de la entidad {@link TipoTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTerritorioServiceAPI
 * @date nov 11, 2016
 */
public class TipoTerritorioServiceTest {
	
    private static final Long ID_PRIMER_TIPOTERRITORIO = 1;

    @InjectMocks
    private TipoTerritorioServiceAPI tipoTerritorioServiceAPI = new TipoTerritorioServiceImpl();

    @Mock
    private TipoTerritorioDaoAPI tipoTerritorioDaoAPI;
    @Mock
    private ZonaDaoAPI zonaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorios()).when(tipoTerritorioDaoAPI).getRecords(null);
        Mockito.doReturn(TipoTerritorioServiceTestData.getTotalTipoTerritorios()).when(tipoTerritorioDaoAPI).countRecords(null);
        PmzResultSet<TipoTerritorioDTO> tipoTerritorios = tipoTerritorioServiceAPI.getRecords(null);
        Assert.assertEquals(TipoTerritorioServiceTestData.getTotalTipoTerritorios(), tipoTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTerritorio_existe(){
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(ID_PRIMER_TIPOTERRITORIO);
        TipoTerritorioDTO tipoTerritorio = tipoTerritorioServiceAPI.getTipoTerritorioDTO(ID_PRIMER_TIPOTERRITORIO);
        Assert.assertEquals(TipoTerritorioServiceTestData.getTipoTerritorio().getId(), tipoTerritorio.getId());
    }

    /**
     * Prueba unitaria de get con {@link TipoTerritorioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTerritorio_no_existe(){
        Mockito.doReturn(null).when(tipoTerritorioDaoAPI).get(ID_PRIMER_TIPOTERRITORIO);
        TipoTerritorioDTO tipoTerritorio = tipoTerritorioServiceAPI.getTipoTerritorioDTO(ID_PRIMER_TIPOTERRITORIO);
        Assert.assertEquals(null, tipoTerritorio);
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTerritorioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_tipoTerritorio_no_existe() throws PmzException{
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	tipoTerritorioServiceAPI.guardar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoTerritorio_existe() throws PmzException{
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(ID_PRIMER_TIPOTERRITORIO);
        tipoTerritorioServiceAPI.guardar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TipoTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoTerritorio_no_existe_no_zona() throws PmzException{
    	tipoTerritorioServiceAPI.guardar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TipoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_tipoTerritorio_existe() throws PmzException{
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(ID_PRIMER_TIPOTERRITORIO);
        tipoTerritorioServiceAPI.editar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_tipoTerritorio_no_existe() throws PmzException{
        tipoTerritorioServiceAPI.editar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_tipoTerritorio_existe_no_zona() throws PmzException{
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(ID_PRIMER_TIPOTERRITORIO);
    	tipoTerritorioServiceAPI.editar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    
    
}
