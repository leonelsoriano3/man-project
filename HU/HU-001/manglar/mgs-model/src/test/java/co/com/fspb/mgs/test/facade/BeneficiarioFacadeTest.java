package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI;
import co.com.fspb.mgs.facade.impl.BeneficiarioFacadeImpl;
import co.com.fspb.mgs.service.api.BeneficiarioServiceAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link BeneficiarioFacadeAPI} de la entidad {@link Beneficiario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioFacadeTest
 * @date nov 11, 2016
 */
public class BeneficiarioFacadeTest {

    private static final Long ID_PRIMER_BENEFICIARIO = 1;

    @InjectMocks
    private BeneficiarioFacadeAPI beneficiarioFacadeAPI = new BeneficiarioFacadeImpl();

    @Mock
    private BeneficiarioServiceAPI beneficiarioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BeneficiarioServiceTestData.getResultSetBeneficiariosDTO()).when(beneficiarioServiceAPI).getRecords(null);
        PmzResultSet<BeneficiarioDTO> beneficiarios = beneficiarioFacadeAPI.getRecords(null);
        Assert.assertEquals(BeneficiarioServiceTestData.getTotalBeneficiarios(), beneficiarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiario_existe(){
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiarioDTO()).when(beneficiarioServiceAPI).getBeneficiarioDTO(ID_PRIMER_BENEFICIARIO);
        BeneficiarioDTO beneficiario = beneficiarioFacadeAPI.getBeneficiarioDTO(ID_PRIMER_BENEFICIARIO);
        Assert.assertEquals(BeneficiarioServiceTestData.getBeneficiarioDTO().getId(), beneficiario.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_beneficiario_no_existe() throws PmzException{
        beneficiarioFacadeAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_beneficiario_existe() throws PmzException{
        beneficiarioFacadeAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
}
