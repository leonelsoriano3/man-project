package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.facade.api.LineaIntervencionFacadeAPI;
import co.com.fspb.mgs.facade.impl.LineaIntervencionFacadeImpl;
import co.com.fspb.mgs.service.api.LineaIntervencionServiceAPI;
import co.com.fspb.mgs.test.service.data.LineaIntervencionServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link LineaIntervencionFacadeAPI} de la entidad {@link LineaIntervencion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaIntervencionFacadeTest
 * @date nov 11, 2016
 */
public class LineaIntervencionFacadeTest {

    private static final Long ID_PRIMER_LINEAINTERVENCION = 1;

    @InjectMocks
    private LineaIntervencionFacadeAPI lineaIntervencionFacadeAPI = new LineaIntervencionFacadeImpl();

    @Mock
    private LineaIntervencionServiceAPI lineaIntervencionServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LineaIntervencionServiceTestData.getResultSetLineaIntervencionsDTO()).when(lineaIntervencionServiceAPI).getRecords(null);
        PmzResultSet<LineaIntervencionDTO> lineaIntervencions = lineaIntervencionFacadeAPI.getRecords(null);
        Assert.assertEquals(LineaIntervencionServiceTestData.getTotalLineaIntervencions(), lineaIntervencions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LineaIntervencionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lineaIntervencion_existe(){
        Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencionDTO()).when(lineaIntervencionServiceAPI).getLineaIntervencionDTO(ID_PRIMER_LINEAINTERVENCION);
        LineaIntervencionDTO lineaIntervencion = lineaIntervencionFacadeAPI.getLineaIntervencionDTO(ID_PRIMER_LINEAINTERVENCION);
        Assert.assertEquals(LineaIntervencionServiceTestData.getLineaIntervencionDTO().getId(), lineaIntervencion.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link LineaIntervencionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_lineaIntervencion_no_existe() throws PmzException{
        lineaIntervencionFacadeAPI.guardar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link LineaIntervencionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_lineaIntervencion_existe() throws PmzException{
        lineaIntervencionFacadeAPI.editar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }
    
}
