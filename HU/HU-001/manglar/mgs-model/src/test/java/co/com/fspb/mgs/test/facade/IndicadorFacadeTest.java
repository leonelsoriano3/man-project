package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.facade.api.IndicadorFacadeAPI;
import co.com.fspb.mgs.facade.impl.IndicadorFacadeImpl;
import co.com.fspb.mgs.service.api.IndicadorServiceAPI;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link IndicadorFacadeAPI} de la entidad {@link Indicador}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorFacadeTest
 * @date nov 11, 2016
 */
public class IndicadorFacadeTest {

    private static final Long ID_PRIMER_INDICADOR = 1;

    @InjectMocks
    private IndicadorFacadeAPI indicadorFacadeAPI = new IndicadorFacadeImpl();

    @Mock
    private IndicadorServiceAPI indicadorServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorServiceTestData.getResultSetIndicadorsDTO()).when(indicadorServiceAPI).getRecords(null);
        PmzResultSet<IndicadorDTO> indicadors = indicadorFacadeAPI.getRecords(null);
        Assert.assertEquals(IndicadorServiceTestData.getTotalIndicadors(), indicadors.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicador_existe(){
        Mockito.doReturn(IndicadorServiceTestData.getIndicadorDTO()).when(indicadorServiceAPI).getIndicadorDTO(ID_PRIMER_INDICADOR);
        IndicadorDTO indicador = indicadorFacadeAPI.getIndicadorDTO(ID_PRIMER_INDICADOR);
        Assert.assertEquals(IndicadorServiceTestData.getIndicadorDTO().getId(), indicador.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_indicador_no_existe() throws PmzException{
        indicadorFacadeAPI.guardar(IndicadorServiceTestData.getIndicadorDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link IndicadorDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_indicador_existe() throws PmzException{
        indicadorFacadeAPI.editar(IndicadorServiceTestData.getIndicadorDTO());
    }
    
}
