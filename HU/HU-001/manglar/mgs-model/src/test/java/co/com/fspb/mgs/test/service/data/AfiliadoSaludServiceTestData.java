package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.AfiliadoSalud;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link AfiliadoSalud}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludServiceTestData
 * @date nov 11, 2016
 */
public abstract class AfiliadoSaludServiceTestData {
    
    private static List<AfiliadoSalud> entities = new ArrayList<AfiliadoSalud>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            AfiliadoSalud entity = new AfiliadoSalud();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link AfiliadoSalud} Mock 
     * tranformado a DTO {@link AfiliadoSaludDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link AfiliadoSaludDTO}
     */
    public static AfiliadoSaludDTO getAfiliadoSaludDTO(){
        return DTOTransformer.getAfiliadoSaludDTOFromAfiliadoSalud(getAfiliadoSalud());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link AfiliadoSalud} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link AfiliadoSalud} Mock
     */
    public static AfiliadoSalud getAfiliadoSalud(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link AfiliadoSalud} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link AfiliadoSalud}
     */
    public static List<AfiliadoSalud> getAfiliadoSaluds(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<AfiliadoSaludDTO> getResultSetAfiliadoSaludsDTO(){
        List<AfiliadoSaludDTO> dtos = new ArrayList<AfiliadoSaludDTO>();
        for (AfiliadoSalud entity : entities) {
            dtos.add(DTOTransformer.getAfiliadoSaludDTOFromAfiliadoSalud(entity));
        }
        return new PmzResultSet<AfiliadoSaludDTO>(dtos, getTotalAfiliadoSaluds(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalAfiliadoSaluds(){
        return Long.valueOf(entities.size());
    }

}
