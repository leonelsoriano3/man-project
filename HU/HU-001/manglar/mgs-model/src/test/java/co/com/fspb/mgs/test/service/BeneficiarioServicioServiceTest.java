package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.dao.api.BeneficiarioServicioDaoAPI;
import co.com.fspb.mgs.service.api.BeneficiarioServicioServiceAPI;
import co.com.fspb.mgs.service.impl.BeneficiarioServicioServiceImpl;
import co.com.fspb.mgs.test.service.data.BeneficiarioServicioServiceTestData;
import co.com.fspb.mgs.dao.model.BeneficiarioServicioPK;
  import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.dao.api.ServicioDaoAPI;
import co.com.fspb.mgs.test.service.data.ServicioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link BeneficiarioServicioServiceAPI} de la entidad {@link BeneficiarioServicio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioServiceAPI
 * @date nov 11, 2016
 */
public class BeneficiarioServicioServiceTest {
	
    private static final BeneficiarioDTO BENEFICIARIODTO = BeneficiarioServiceTestData.getBeneficiarioDTO();
    private static final ServicioDTO SERVICIODTO = ServicioServiceTestData.getServicioDTO();

    @InjectMocks
    private BeneficiarioServicioServiceAPI beneficiarioServicioServiceAPI = new BeneficiarioServicioServiceImpl();

    @Mock
    private BeneficiarioServicioDaoAPI beneficiarioServicioDaoAPI;
    @Mock
    private BeneficiarioDaoAPI beneficiarioDaoAPI;
    @Mock
    private ServicioDaoAPI servicioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicios()).when(beneficiarioServicioDaoAPI).getRecords(null);
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getTotalBeneficiarioServicios()).when(beneficiarioServicioDaoAPI).countRecords(null);
        PmzResultSet<BeneficiarioServicioDTO> beneficiarioServicios = beneficiarioServicioServiceAPI.getRecords(null);
        Assert.assertEquals(BeneficiarioServicioServiceTestData.getTotalBeneficiarioServicios(), beneficiarioServicios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioServicioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiarioServicio_existe(){
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicio()).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
        BeneficiarioServicioDTO beneficiarioServicio = beneficiarioServicioServiceAPI.getBeneficiarioServicioDTO(BENEFICIARIODTO, SERVICIODTO);
        Assert.assertEquals(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO().getId(), beneficiarioServicio.getId());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioServicioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiarioServicio_no_existe(){
        Mockito.doReturn(null).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
        BeneficiarioServicioDTO beneficiarioServicio = beneficiarioServicioServiceAPI.getBeneficiarioServicioDTO(BENEFICIARIODTO, SERVICIODTO);
        Assert.assertEquals(null, beneficiarioServicio);
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioServicioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_beneficiarioServicio_no_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ServicioServiceTestData.getServicio().getId());
    	beneficiarioServicioServiceAPI.guardar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiarioServicio_existe() throws PmzException{
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicio()).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
        beneficiarioServicioServiceAPI.guardar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioServicioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiarioServicio_no_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ServicioServiceTestData.getServicio().getId());
    	beneficiarioServicioServiceAPI.guardar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioServicioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con servicio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiarioServicio_no_existe_no_servicio() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	beneficiarioServicioServiceAPI.guardar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_beneficiarioServicio_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ServicioServiceTestData.getServicio().getId());
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicio()).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
        beneficiarioServicioServiceAPI.editar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_beneficiarioServicio_no_existe() throws PmzException{
        beneficiarioServicioServiceAPI.editar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioServicioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiarioServicio_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ServicioServiceTestData.getServicio().getId());
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicio()).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
    	beneficiarioServicioServiceAPI.editar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioServicioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con servicio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiarioServicio_existe_no_servicio() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicio()).when(beneficiarioServicioDaoAPI).get(Mockito.any(BeneficiarioServicioPK.class));
    	beneficiarioServicioServiceAPI.editar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
    
}
