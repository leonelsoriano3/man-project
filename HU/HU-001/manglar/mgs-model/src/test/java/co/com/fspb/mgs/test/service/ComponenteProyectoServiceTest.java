package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.dao.api.ComponenteProyectoDaoAPI;
import co.com.fspb.mgs.service.api.ComponenteProyectoServiceAPI;
import co.com.fspb.mgs.service.impl.ComponenteProyectoServiceImpl;
import co.com.fspb.mgs.test.service.data.ComponenteProyectoServiceTestData;
import co.com.fspb.mgs.dao.model.ComponenteProyectoPK;
  import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.test.service.data.ComponenteServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ComponenteProyectoServiceAPI} de la entidad {@link ComponenteProyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoServiceAPI
 * @date nov 11, 2016
 */
public class ComponenteProyectoServiceTest {
	
    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();
    private static final ComponenteDTO COMPONENTEDTO = ComponenteServiceTestData.getComponenteDTO();

    @InjectMocks
    private ComponenteProyectoServiceAPI componenteProyectoServiceAPI = new ComponenteProyectoServiceImpl();

    @Mock
    private ComponenteProyectoDaoAPI componenteProyectoDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;
    @Mock
    private ComponenteDaoAPI componenteDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyectos()).when(componenteProyectoDaoAPI).getRecords(null);
        Mockito.doReturn(ComponenteProyectoServiceTestData.getTotalComponenteProyectos()).when(componenteProyectoDaoAPI).countRecords(null);
        PmzResultSet<ComponenteProyectoDTO> componenteProyectos = componenteProyectoServiceAPI.getRecords(null);
        Assert.assertEquals(ComponenteProyectoServiceTestData.getTotalComponenteProyectos(), componenteProyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componenteProyecto_existe(){
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyecto()).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
        ComponenteProyectoDTO componenteProyecto = componenteProyectoServiceAPI.getComponenteProyectoDTO(PROYECTODTO, COMPONENTEDTO);
        Assert.assertEquals(ComponenteProyectoServiceTestData.getComponenteProyectoDTO().getId(), componenteProyecto.getId());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteProyectoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componenteProyecto_no_existe(){
        Mockito.doReturn(null).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
        ComponenteProyectoDTO componenteProyecto = componenteProyectoServiceAPI.getComponenteProyectoDTO(PROYECTODTO, COMPONENTEDTO);
        Assert.assertEquals(null, componenteProyecto);
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteProyectoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_componenteProyecto_no_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	componenteProyectoServiceAPI.guardar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_componenteProyecto_existe() throws PmzException{
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyecto()).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
        componenteProyectoServiceAPI.guardar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ComponenteProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_componenteProyecto_no_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	componenteProyectoServiceAPI.guardar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ComponenteProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con componente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_componenteProyecto_no_existe_no_componente() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	componenteProyectoServiceAPI.guardar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ComponenteProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_componenteProyecto_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyecto()).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
        componenteProyectoServiceAPI.editar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ComponenteProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_componenteProyecto_no_existe() throws PmzException{
        componenteProyectoServiceAPI.editar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ComponenteProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_componenteProyecto_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyecto()).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
    	componenteProyectoServiceAPI.editar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ComponenteProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con componente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_componenteProyecto_existe_no_componente() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(ComponenteProyectoServiceTestData.getComponenteProyecto()).when(componenteProyectoDaoAPI).get(Mockito.any(ComponenteProyectoPK.class));
    	componenteProyectoServiceAPI.editar(ComponenteProyectoServiceTestData.getComponenteProyectoDTO());
    }
    
    
}
