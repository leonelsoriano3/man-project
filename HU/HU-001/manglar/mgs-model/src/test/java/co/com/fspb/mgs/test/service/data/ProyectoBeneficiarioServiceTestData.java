package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.ProyectoBeneficiario;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ProyectoBeneficiario}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioServiceTestData
 * @date nov 11, 2016
 */
public abstract class ProyectoBeneficiarioServiceTestData {
    
    private static List<ProyectoBeneficiario> entities = new ArrayList<ProyectoBeneficiario>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ProyectoBeneficiario entity = new ProyectoBeneficiario();

            entity.setId(i);
            
            entity.setBeneficiario(BeneficiarioServiceTestData.getBeneficiario());
            entity.setProyecto(ProyectoServiceTestData.getProyecto());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProyectoBeneficiario} Mock 
     * tranformado a DTO {@link ProyectoBeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ProyectoBeneficiarioDTO}
     */
    public static ProyectoBeneficiarioDTO getProyectoBeneficiarioDTO(){
        return DTOTransformer.getProyectoBeneficiarioDTOFromProyectoBeneficiario(getProyectoBeneficiario());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProyectoBeneficiario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ProyectoBeneficiario} Mock
     */
    public static ProyectoBeneficiario getProyectoBeneficiario(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ProyectoBeneficiario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ProyectoBeneficiario}
     */
    public static List<ProyectoBeneficiario> getProyectoBeneficiarios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ProyectoBeneficiarioDTO> getResultSetProyectoBeneficiariosDTO(){
        List<ProyectoBeneficiarioDTO> dtos = new ArrayList<ProyectoBeneficiarioDTO>();
        for (ProyectoBeneficiario entity : entities) {
            dtos.add(DTOTransformer.getProyectoBeneficiarioDTOFromProyectoBeneficiario(entity));
        }
        return new PmzResultSet<ProyectoBeneficiarioDTO>(dtos, getTotalProyectoBeneficiarios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalProyectoBeneficiarios(){
        return Long.valueOf(entities.size());
    }

}
