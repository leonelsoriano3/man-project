package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import co.com.fspb.mgs.dao.api.TrazaPresupuestoDaoAPI;
import co.com.fspb.mgs.service.api.TrazaPresupuestoServiceAPI;
import co.com.fspb.mgs.service.impl.TrazaPresupuestoServiceImpl;
import co.com.fspb.mgs.test.service.data.TrazaPresupuestoServiceTestData;
  import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.dao.api.LineaDaoAPI;
import co.com.fspb.mgs.test.service.data.LineaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TrazaPresupuestoServiceAPI} de la entidad {@link TrazaPresupuesto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuestoServiceAPI
 * @date nov 11, 2016
 */
public class TrazaPresupuestoServiceTest {
	
    private static final Long ID_PRIMER_TRAZAPRESUPUESTO = 1;

    @InjectMocks
    private TrazaPresupuestoServiceAPI trazaPresupuestoServiceAPI = new TrazaPresupuestoServiceImpl();

    @Mock
    private TrazaPresupuestoDaoAPI trazaPresupuestoDaoAPI;
    @Mock
    private LineaDaoAPI lineaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuestos()).when(trazaPresupuestoDaoAPI).getRecords(null);
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTotalTrazaPresupuestos()).when(trazaPresupuestoDaoAPI).countRecords(null);
        PmzResultSet<TrazaPresupuestoDTO> trazaPresupuestos = trazaPresupuestoServiceAPI.getRecords(null);
        Assert.assertEquals(TrazaPresupuestoServiceTestData.getTotalTrazaPresupuestos(), trazaPresupuestos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TrazaPresupuestoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaPresupuesto_existe(){
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuesto()).when(trazaPresupuestoDaoAPI).get(ID_PRIMER_TRAZAPRESUPUESTO);
        TrazaPresupuestoDTO trazaPresupuesto = trazaPresupuestoServiceAPI.getTrazaPresupuestoDTO(ID_PRIMER_TRAZAPRESUPUESTO);
        Assert.assertEquals(TrazaPresupuestoServiceTestData.getTrazaPresupuesto().getId(), trazaPresupuesto.getId());
    }

    /**
     * Prueba unitaria de get con {@link TrazaPresupuestoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaPresupuesto_no_existe(){
        Mockito.doReturn(null).when(trazaPresupuestoDaoAPI).get(ID_PRIMER_TRAZAPRESUPUESTO);
        TrazaPresupuestoDTO trazaPresupuesto = trazaPresupuestoServiceAPI.getTrazaPresupuestoDTO(ID_PRIMER_TRAZAPRESUPUESTO);
        Assert.assertEquals(null, trazaPresupuesto);
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaPresupuestoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_trazaPresupuesto_no_existe() throws PmzException{
    	Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(LineaServiceTestData.getLinea().getId());
    	trazaPresupuestoServiceAPI.guardar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaPresupuestoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_trazaPresupuesto_existe() throws PmzException{
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuesto()).when(trazaPresupuestoDaoAPI).get(ID_PRIMER_TRAZAPRESUPUESTO);
        trazaPresupuestoServiceAPI.guardar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TrazaPresupuestoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con linea
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_trazaPresupuesto_no_existe_no_linea() throws PmzException{
    	trazaPresupuestoServiceAPI.guardar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TrazaPresupuestoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_trazaPresupuesto_existe() throws PmzException{
    	Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(LineaServiceTestData.getLinea().getId());
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuesto()).when(trazaPresupuestoDaoAPI).get(ID_PRIMER_TRAZAPRESUPUESTO);
        trazaPresupuestoServiceAPI.editar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TrazaPresupuestoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_trazaPresupuesto_no_existe() throws PmzException{
        trazaPresupuestoServiceAPI.editar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TrazaPresupuestoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con linea
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_trazaPresupuesto_existe_no_linea() throws PmzException{
        Mockito.doReturn(TrazaPresupuestoServiceTestData.getTrazaPresupuesto()).when(trazaPresupuestoDaoAPI).get(ID_PRIMER_TRAZAPRESUPUESTO);
    	trazaPresupuestoServiceAPI.editar(TrazaPresupuestoServiceTestData.getTrazaPresupuestoDTO());
    }
    
    
}
