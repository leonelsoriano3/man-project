package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI;
import co.com.fspb.mgs.facade.impl.GrupoEtnicoFacadeImpl;
import co.com.fspb.mgs.service.api.GrupoEtnicoServiceAPI;
import co.com.fspb.mgs.test.service.data.GrupoEtnicoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link GrupoEtnicoFacadeAPI} de la entidad {@link GrupoEtnico}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class GrupoEtnicoFacadeTest
 * @date nov 11, 2016
 */
public class GrupoEtnicoFacadeTest {

    private static final Long ID_PRIMER_GRUPOETNICO = 1;

    @InjectMocks
    private GrupoEtnicoFacadeAPI grupoEtnicoFacadeAPI = new GrupoEtnicoFacadeImpl();

    @Mock
    private GrupoEtnicoServiceAPI grupoEtnicoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(GrupoEtnicoServiceTestData.getResultSetGrupoEtnicosDTO()).when(grupoEtnicoServiceAPI).getRecords(null);
        PmzResultSet<GrupoEtnicoDTO> grupoEtnicos = grupoEtnicoFacadeAPI.getRecords(null);
        Assert.assertEquals(GrupoEtnicoServiceTestData.getTotalGrupoEtnicos(), grupoEtnicos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link GrupoEtnicoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_grupoEtnico_existe(){
        Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO()).when(grupoEtnicoServiceAPI).getGrupoEtnicoDTO(ID_PRIMER_GRUPOETNICO);
        GrupoEtnicoDTO grupoEtnico = grupoEtnicoFacadeAPI.getGrupoEtnicoDTO(ID_PRIMER_GRUPOETNICO);
        Assert.assertEquals(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO().getId(), grupoEtnico.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link GrupoEtnicoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_grupoEtnico_no_existe() throws PmzException{
        grupoEtnicoFacadeAPI.guardar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link GrupoEtnicoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_grupoEtnico_existe() throws PmzException{
        grupoEtnicoFacadeAPI.editar(GrupoEtnicoServiceTestData.getGrupoEtnicoDTO());
    }
    
}
