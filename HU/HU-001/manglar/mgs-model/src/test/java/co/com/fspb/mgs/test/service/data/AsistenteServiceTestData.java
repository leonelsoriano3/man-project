package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Asistente;
import co.com.fspb.mgs.dto.AsistenteDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Asistente}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AsistenteServiceTestData
 * @date nov 11, 2016
 */
public abstract class AsistenteServiceTestData {
    
    private static List<Asistente> entities = new ArrayList<Asistente>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Asistente entity = new Asistente();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setProyectoBeneficiario(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setActividad(ActividadServiceTestData.getActividad());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Asistente} Mock 
     * tranformado a DTO {@link AsistenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link AsistenteDTO}
     */
    public static AsistenteDTO getAsistenteDTO(){
        return DTOTransformer.getAsistenteDTOFromAsistente(getAsistente());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Asistente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Asistente} Mock
     */
    public static Asistente getAsistente(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Asistente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Asistente}
     */
    public static List<Asistente> getAsistentes(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<AsistenteDTO> getResultSetAsistentesDTO(){
        List<AsistenteDTO> dtos = new ArrayList<AsistenteDTO>();
        for (Asistente entity : entities) {
            dtos.add(DTOTransformer.getAsistenteDTOFromAsistente(entity));
        }
        return new PmzResultSet<AsistenteDTO>(dtos, getTotalAsistentes(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalAsistentes(){
        return Long.valueOf(entities.size());
    }

}
