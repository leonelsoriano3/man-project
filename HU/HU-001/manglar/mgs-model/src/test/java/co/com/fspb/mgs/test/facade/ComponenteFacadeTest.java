package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.facade.api.ComponenteFacadeAPI;
import co.com.fspb.mgs.facade.impl.ComponenteFacadeImpl;
import co.com.fspb.mgs.service.api.ComponenteServiceAPI;
import co.com.fspb.mgs.test.service.data.ComponenteServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ComponenteFacadeAPI} de la entidad {@link Componente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteFacadeTest
 * @date nov 11, 2016
 */
public class ComponenteFacadeTest {

    private static final Long ID_PRIMER_COMPONENTE = 1;

    @InjectMocks
    private ComponenteFacadeAPI componenteFacadeAPI = new ComponenteFacadeImpl();

    @Mock
    private ComponenteServiceAPI componenteServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ComponenteServiceTestData.getResultSetComponentesDTO()).when(componenteServiceAPI).getRecords(null);
        PmzResultSet<ComponenteDTO> componentes = componenteFacadeAPI.getRecords(null);
        Assert.assertEquals(ComponenteServiceTestData.getTotalComponentes(), componentes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componente_existe(){
        Mockito.doReturn(ComponenteServiceTestData.getComponenteDTO()).when(componenteServiceAPI).getComponenteDTO(ID_PRIMER_COMPONENTE);
        ComponenteDTO componente = componenteFacadeAPI.getComponenteDTO(ID_PRIMER_COMPONENTE);
        Assert.assertEquals(ComponenteServiceTestData.getComponenteDTO().getId(), componente.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_componente_no_existe() throws PmzException{
        componenteFacadeAPI.guardar(ComponenteServiceTestData.getComponenteDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ComponenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_componente_existe() throws PmzException{
        componenteFacadeAPI.editar(ComponenteServiceTestData.getComponenteDTO());
    }
    
}
