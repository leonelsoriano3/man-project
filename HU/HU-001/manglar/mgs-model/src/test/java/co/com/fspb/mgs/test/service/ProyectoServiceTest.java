package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.service.api.ProyectoServiceAPI;
import co.com.fspb.mgs.service.impl.ProyectoServiceImpl;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
      import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.dao.api.RolFundacionDaoAPI;
import co.com.fspb.mgs.test.service.data.RolFundacionServiceTestData;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.test.service.data.IniciativaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ProyectoServiceAPI} de la entidad {@link Proyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoServiceAPI
 * @date nov 11, 2016
 */
public class ProyectoServiceTest {
	
    private static final Long ID_PRIMER_PROYECTO = 1;

    @InjectMocks
    private ProyectoServiceAPI proyectoServiceAPI = new ProyectoServiceImpl();

    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;
    @Mock
    private RolFundacionDaoAPI rolFundacionDaoAPI;
    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;
    @Mock
    private IniciativaDaoAPI iniciativaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoServiceTestData.getProyectos()).when(proyectoDaoAPI).getRecords(null);
        Mockito.doReturn(ProyectoServiceTestData.getTotalProyectos()).when(proyectoDaoAPI).countRecords(null);
        PmzResultSet<ProyectoDTO> proyectos = proyectoServiceAPI.getRecords(null);
        Assert.assertEquals(ProyectoServiceTestData.getTotalProyectos(), proyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyecto_existe(){
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
        ProyectoDTO proyecto = proyectoServiceAPI.getProyectoDTO(ID_PRIMER_PROYECTO);
        Assert.assertEquals(ProyectoServiceTestData.getProyecto().getId(), proyecto.getId());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyecto_no_existe(){
        Mockito.doReturn(null).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
        ProyectoDTO proyecto = proyectoServiceAPI.getProyectoDTO(ID_PRIMER_PROYECTO);
        Assert.assertEquals(null, proyecto);
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_proyecto_no_existe() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
    	proyectoServiceAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyecto_existe() throws PmzException{
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
        proyectoServiceAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con rolFundacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyecto_no_existe_no_rolFundacion() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
    	proyectoServiceAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyecto_no_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
    	proyectoServiceAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con iniciativa
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyecto_no_existe_no_iniciativa() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	proyectoServiceAPI.guardar(ProyectoServiceTestData.getProyectoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_proyecto_existe() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
        proyectoServiceAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_proyecto_no_existe() throws PmzException{
        proyectoServiceAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con rolFundacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyecto_existe_no_rolFundacion() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
    	proyectoServiceAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyecto_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(IniciativaServiceTestData.getIniciativa().getId());
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
    	proyectoServiceAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con iniciativa
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyecto_existe_no_iniciativa() throws PmzException{
    	Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(RolFundacionServiceTestData.getRolFundacion().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ID_PRIMER_PROYECTO);
    	proyectoServiceAPI.editar(ProyectoServiceTestData.getProyectoDTO());
    }
    
    
}
