package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.LiderDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link LiderTerritorio}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class LiderTerritorioFacadeAPI
 * @date nov 11, 2016
 */
public interface LiderTerritorioFacadeAPI {

    /**
	 * Registra una entidad {@link LiderTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param liderTerritorio - {@link LiderTerritorioDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(LiderTerritorioDTO liderTerritorio) throws PmzException;

	/**
	 * Actualiza una entidad {@link LiderTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param liderTerritorio - {@link LiderTerritorioDTO}
	 * @throws {@link PmzException}
	 */
    void editar(LiderTerritorioDTO liderTerritorio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<LiderTerritorioDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link LiderTerritorioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link TerritorioDTO}
	 * @param lider - {@link LiderDTO}
     * @return {@link LiderTerritorioDTO}
     */
    LiderTerritorioDTO getLiderTerritorioDTO(TerritorioDTO territorio, LiderDTO lider);
}

