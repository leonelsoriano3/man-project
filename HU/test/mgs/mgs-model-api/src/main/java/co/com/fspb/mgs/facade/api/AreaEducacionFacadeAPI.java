package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AreaEducacionDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link AreaEducacion}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class AreaEducacionFacadeAPI
 * @date nov 11, 2016
 */
public interface AreaEducacionFacadeAPI {

    /**
	 * Registra una entidad {@link AreaEducacion} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param areaEducacion - {@link AreaEducacionDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(AreaEducacionDTO areaEducacion) throws PmzException;

	/**
	 * Actualiza una entidad {@link AreaEducacion} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param areaEducacion - {@link AreaEducacionDTO}
	 * @throws {@link PmzException}
	 */
    void editar(AreaEducacionDTO areaEducacion) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<AreaEducacionDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link AreaEducacionDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link AreaEducacionDTO}
     */
    AreaEducacionDTO getAreaEducacionDTO(Long id);
}

