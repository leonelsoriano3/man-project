package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstratoSocioeconomicoEnum
 * @date nov 11, 2016
 */
public enum EstratoSocioeconomicoEnum {
	    1,    2,    3,    4,    5,    6,    7;
}