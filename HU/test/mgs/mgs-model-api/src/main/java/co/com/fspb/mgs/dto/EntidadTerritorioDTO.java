package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link EntidadTerritorio}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EntidadTerritorioDTO
 * @date nov 11, 2016
 */
public class EntidadTerritorioDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private EntidadDTO entidadDTO; 

    @JsonSerialize
    @NotNull
    private TerritorioDTO territorioDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(territorioDTO);
        idBuilder.append("-");
        idBuilder.append(entidadDTO);
        return idBuilder.toString();
    }

    public EntidadDTO getEntidadDTO() {
        return entidadDTO;
    }

    public void setEntidadDTO(EntidadDTO entidadDTO) {
        this.entidadDTO = entidadDTO;
    }

    public TerritorioDTO getTerritorioDTO() {
        return territorioDTO;
    }

    public void setTerritorioDTO(TerritorioDTO territorioDTO) {
        this.territorioDTO = territorioDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "EntidadTerritorioDTO ["
                + " entidad=" + entidadDTO
                + ", territorio=" + territorioDTO
                + " ]";
    }
    
}
