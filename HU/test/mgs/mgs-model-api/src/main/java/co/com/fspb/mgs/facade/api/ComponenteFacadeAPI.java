package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Componente}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ComponenteFacadeAPI
 * @date nov 11, 2016
 */
public interface ComponenteFacadeAPI {

    /**
	 * Registra una entidad {@link Componente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componente - {@link ComponenteDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ComponenteDTO componente) throws PmzException;

	/**
	 * Actualiza una entidad {@link Componente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componente - {@link ComponenteDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ComponenteDTO componente) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ComponenteDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ComponenteDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ComponenteDTO}
     */
    ComponenteDTO getComponenteDTO(Long id);
}

