package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TipoIdEnum
 * @date nov 11, 2016
 */
public enum TipoIdEnum {
	    CC,    TI,    RC,    CE,    PP;
}