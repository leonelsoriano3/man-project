package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link BeneficiarioServicio}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class BeneficiarioServicioDTO
 * @date nov 11, 2016
 */
public class BeneficiarioServicioDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private ServicioDTO servicioDTO; 

    @JsonSerialize
    @NotNull
    private BeneficiarioDTO beneficiarioDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(beneficiarioDTO);
        idBuilder.append("-");
        idBuilder.append(servicioDTO);
        return idBuilder.toString();
    }

    public ServicioDTO getServicioDTO() {
        return servicioDTO;
    }

    public void setServicioDTO(ServicioDTO servicioDTO) {
        this.servicioDTO = servicioDTO;
    }

    public BeneficiarioDTO getBeneficiarioDTO() {
        return beneficiarioDTO;
    }

    public void setBeneficiarioDTO(BeneficiarioDTO beneficiarioDTO) {
        this.beneficiarioDTO = beneficiarioDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "BeneficiarioServicioDTO ["
                + " servicio=" + servicioDTO
                + ", beneficiario=" + beneficiarioDTO
                + " ]";
    }
    
}
