package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link DatoAdicional}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class DatoAdicionalDTO
 * @date nov 11, 2016
 */
public class DatoAdicionalDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private String nombreDato; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private ProyectoDTO proyectoDTO; 

    public String getNombreDato() {
        return nombreDato;
    }

    public void setNombreDato(String nombreDato) {
        this.nombreDato = nombreDato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProyectoDTO getProyectoDTO() {
        return proyectoDTO;
    }

    public void setProyectoDTO(ProyectoDTO proyectoDTO) {
        this.proyectoDTO = proyectoDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombreDato;
    }

	@Override
    public String toString() {
        return "DatoAdicionalDTO ["
                + " nombreDato=" + nombreDato
                + ", id=" + id
                + ", proyecto=" + proyectoDTO
                + " ]";
    }
    
}
