package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link ProyectoBeneficiario}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ProyectoBeneficiarioDTO
 * @date nov 11, 2016
 */
public class ProyectoBeneficiarioDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private Integer id; 

    @NotNull
    private BeneficiarioDTO beneficiarioDTO; 

    @NotNull
    private ProyectoDTO proyectoDTO; 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BeneficiarioDTO getBeneficiarioDTO() {
        return beneficiarioDTO;
    }

    public void setBeneficiarioDTO(BeneficiarioDTO beneficiarioDTO) {
        this.beneficiarioDTO = beneficiarioDTO;
    }

    public ProyectoDTO getProyectoDTO() {
        return proyectoDTO;
    }

    public void setProyectoDTO(ProyectoDTO proyectoDTO) {
        this.proyectoDTO = proyectoDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "ProyectoBeneficiarioDTO ["
                + " id=" + id
                + ", beneficiario=" + beneficiarioDTO
                + ", proyecto=" + proyectoDTO
                + " ]";
    }
    
}
