package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.PersonaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Pariente}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ParienteFacadeAPI
 * @date nov 11, 2016
 */
public interface ParienteFacadeAPI {

    /**
	 * Registra una entidad {@link Pariente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param pariente - {@link ParienteDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ParienteDTO pariente) throws PmzException;

	/**
	 * Actualiza una entidad {@link Pariente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param pariente - {@link ParienteDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ParienteDTO pariente) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ParienteDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ParienteDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link BeneficiarioDTO}
	 * @param persona - {@link PersonaDTO}
     * @return {@link ParienteDTO}
     */
    ParienteDTO getParienteDTO(BeneficiarioDTO beneficiario, PersonaDTO persona);
}

