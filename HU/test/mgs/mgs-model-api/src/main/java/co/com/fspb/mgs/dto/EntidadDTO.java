package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Entidad}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EntidadDTO
 * @date nov 11, 2016
 */
public class EntidadDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private String tipoId; 
    
    @NotNull
    private String tipoIdDesc; 

    private Date fechaModifica;  

    private PersonaDTO contactoDTO;

    @NotNull
    private PersonaDTO personaDTO;

    private Date fechaConstitucion; 

    private String infoConvocatorias; 

    @NotNull
    private String nombre; 

    @JsonSerialize
    @NotNull
    private Long id; 

    @NotNull
    private String numId; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String certificaciones; 

    private String usuarioModifica; 

    private String usuarioCrea; 

    private Date fechaCrea; 

    @NotNull
    private String tipoEntidad; 

    public String getTipoId() {
        return tipoId;
    }

    public void setTipoId(String tipoId) {
        this.tipoId = tipoId;
    }

    public String getTipoIdDesc() {
        return tipoIdDesc;
    }

    public void setTipoIdDesc(String tipoIdDesc) {
        this.tipoIdDesc = tipoIdDesc;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public PersonaDTO getContactoDTO() {
        return contactoDTO;
    }

    public void setContactoDTO(PersonaDTO personaDTO) {
        this.contactoDTO = personaDTO;
    }

    public Date getFechaConstitucion() {
        return fechaConstitucion != null ? new Date(fechaConstitucion.getTime()) : null;
    }

    public void setFechaConstitucion(Date fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion != null ? new Date(fechaConstitucion.getTime()) : null;
    }

    public String getInfoConvocatorias() {
        return infoConvocatorias;
    }

    public void setInfoConvocatorias(String infoConvocatorias) {
        this.infoConvocatorias = infoConvocatorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getCertificaciones() {
        return certificaciones;
    }

    public void setCertificaciones(String certificaciones) {
        this.certificaciones = certificaciones;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombre;
    }

	@Override
    public String toString() {
        return "EntidadDTO ["
                + " tipoId=" + tipoId
                + ", fechaModifica=" + fechaModifica
                + ", persona=" + contactoDTO
                + ", persona=" + contactoDTO
                + ", fechaConstitucion=" + fechaConstitucion
                + ", infoConvocatorias=" + infoConvocatorias
                + ", nombre=" + nombre
                + ", id=" + id
                + ", numId=" + numId
                + ", estado=" + estado
                + ", certificaciones=" + certificaciones
                + ", usuarioModifica=" + usuarioModifica
                + ", usuarioCrea=" + usuarioCrea
                + ", fechaCrea=" + fechaCrea
                + ", tipoEntidad=" + tipoEntidad
                + " ]";
    }
    
}
