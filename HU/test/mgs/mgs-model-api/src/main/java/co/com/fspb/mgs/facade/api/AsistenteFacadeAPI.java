package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AsistenteDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Asistente}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class AsistenteFacadeAPI
 * @date nov 11, 2016
 */
public interface AsistenteFacadeAPI {

    /**
	 * Registra una entidad {@link Asistente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param asistente - {@link AsistenteDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(AsistenteDTO asistente) throws PmzException;

	/**
	 * Actualiza una entidad {@link Asistente} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param asistente - {@link AsistenteDTO}
	 * @throws {@link PmzException}
	 */
    void editar(AsistenteDTO asistente) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<AsistenteDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link AsistenteDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link AsistenteDTO}
     */
    AsistenteDTO getAsistenteDTO(Long id);
}

