package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link ValorDatoAdicional}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ValorDatoAdicionalDTO
 * @date nov 11, 2016
 */
public class ValorDatoAdicionalDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String valor; 

    private String usuarioModifica; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private String usuarioCrea; 

    @NotNull
    private DatoAdicionalDTO datoAdicionalDTO; 

    private Date fechaCrea; 

    @NotNull
    private BeneficiarioDTO beneficiarioDTO; 

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public DatoAdicionalDTO getDatoAdicionalDTO() {
        return datoAdicionalDTO;
    }

    public void setDatoAdicionalDTO(DatoAdicionalDTO datoAdicionalDTO) {
        this.datoAdicionalDTO = datoAdicionalDTO;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public BeneficiarioDTO getBeneficiarioDTO() {
        return beneficiarioDTO;
    }

    public void setBeneficiarioDTO(BeneficiarioDTO beneficiarioDTO) {
        this.beneficiarioDTO = beneficiarioDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.valor;
    }

	@Override
    public String toString() {
        return "ValorDatoAdicionalDTO ["
                + " fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", valor=" + valor
                + ", usuarioModifica=" + usuarioModifica
                + ", id=" + id
                + ", usuarioCrea=" + usuarioCrea
                + ", datoAdicional=" + datoAdicionalDTO
                + ", fechaCrea=" + fechaCrea
                + ", beneficiario=" + beneficiarioDTO
                + " ]";
    }
    
}
