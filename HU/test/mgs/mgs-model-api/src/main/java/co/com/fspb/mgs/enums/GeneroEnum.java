package co.com.fspb.mgs.enums;


/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class GeneroEnum
 * @date nov 15, 2016
 */
public enum GeneroEnum {
	MASCULINO,    FEMENINO;
}
