package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link EstructuraFamiliar}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstructuraFamiliarFacadeAPI
 * @date nov 11, 2016
 */
public interface EstructuraFamiliarFacadeAPI {

    /**
	 * Registra una entidad {@link EstructuraFamiliar} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estructuraFamiliar - {@link EstructuraFamiliarDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(EstructuraFamiliarDTO estructuraFamiliar) throws PmzException;

	/**
	 * Actualiza una entidad {@link EstructuraFamiliar} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estructuraFamiliar - {@link EstructuraFamiliarDTO}
	 * @throws {@link PmzException}
	 */
    void editar(EstructuraFamiliarDTO estructuraFamiliar) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EstructuraFamiliarDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link EstructuraFamiliarDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link EstructuraFamiliarDTO}
     */
    EstructuraFamiliarDTO getEstructuraFamiliarDTO(Long id);
}

