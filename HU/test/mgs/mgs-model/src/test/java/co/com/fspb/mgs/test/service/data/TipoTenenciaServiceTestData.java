package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TipoTenencia;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TipoTenencia}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTenenciaServiceTestData
 * @date nov 11, 2016
 */
public abstract class TipoTenenciaServiceTestData {
    
    private static List<TipoTenencia> entities = new ArrayList<TipoTenencia>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TipoTenencia entity = new TipoTenencia();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoTenencia} Mock 
     * tranformado a DTO {@link TipoTenenciaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TipoTenenciaDTO}
     */
    public static TipoTenenciaDTO getTipoTenenciaDTO(){
        return DTOTransformer.getTipoTenenciaDTOFromTipoTenencia(getTipoTenencia());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoTenencia} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TipoTenencia} Mock
     */
    public static TipoTenencia getTipoTenencia(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TipoTenencia} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TipoTenencia}
     */
    public static List<TipoTenencia> getTipoTenencias(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TipoTenenciaDTO> getResultSetTipoTenenciasDTO(){
        List<TipoTenenciaDTO> dtos = new ArrayList<TipoTenenciaDTO>();
        for (TipoTenencia entity : entities) {
            dtos.add(DTOTransformer.getTipoTenenciaDTOFromTipoTenencia(entity));
        }
        return new PmzResultSet<TipoTenenciaDTO>(dtos, getTotalTipoTenencias(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTipoTenencias(){
        return Long.valueOf(entities.size());
    }

}
