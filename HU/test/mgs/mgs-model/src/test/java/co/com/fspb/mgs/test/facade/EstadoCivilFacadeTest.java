package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI;
import co.com.fspb.mgs.facade.impl.EstadoCivilFacadeImpl;
import co.com.fspb.mgs.service.api.EstadoCivilServiceAPI;
import co.com.fspb.mgs.test.service.data.EstadoCivilServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EstadoCivilFacadeAPI} de la entidad {@link EstadoCivil}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoCivilFacadeTest
 * @date nov 11, 2016
 */
public class EstadoCivilFacadeTest {

    private static final Long ID_PRIMER_ESTADOCIVIL = 1L;

    @InjectMocks
    private EstadoCivilFacadeAPI estadoCivilFacadeAPI = new EstadoCivilFacadeImpl();

    @Mock
    private EstadoCivilServiceAPI estadoCivilServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstadoCivilServiceTestData.getResultSetEstadoCivilsDTO()).when(estadoCivilServiceAPI).getRecords(null);
        PmzResultSet<EstadoCivilDTO> estadoCivils = estadoCivilFacadeAPI.getRecords(null);
        Assert.assertEquals(EstadoCivilServiceTestData.getTotalEstadoCivils(), estadoCivils.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstadoCivilDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estadoCivil_existe(){
        Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivilDTO()).when(estadoCivilServiceAPI).getEstadoCivilDTO(ID_PRIMER_ESTADOCIVIL);
        EstadoCivilDTO estadoCivil = estadoCivilFacadeAPI.getEstadoCivilDTO(ID_PRIMER_ESTADOCIVIL);
        Assert.assertEquals(EstadoCivilServiceTestData.getEstadoCivilDTO().getId(), estadoCivil.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EstadoCivilDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_estadoCivil_no_existe() throws PmzException{
        estadoCivilFacadeAPI.guardar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EstadoCivilDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_estadoCivil_existe() throws PmzException{
        estadoCivilFacadeAPI.editar(EstadoCivilServiceTestData.getEstadoCivilDTO());
    }
    
}
