package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.IndicadorProyectoPK;
import co.com.fspb.mgs.dao.model.IndicadorProyecto;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link IndicadorProyecto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoServiceTestData
 * @date nov 11, 2016
 */
public abstract class IndicadorProyectoServiceTestData {
    
    private static List<IndicadorProyecto> entities = new ArrayList<IndicadorProyecto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            IndicadorProyecto entity = new IndicadorProyecto();

		    IndicadorProyectoPK pk = new IndicadorProyectoPK();
            pk.setIndicador(IndicadorServiceTestData.getIndicador());
            pk.setProyecto(ProyectoServiceTestData.getProyecto());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link IndicadorProyecto} Mock 
     * tranformado a DTO {@link IndicadorProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link IndicadorProyectoDTO}
     */
    public static IndicadorProyectoDTO getIndicadorProyectoDTO(){
        return DTOTransformer.getIndicadorProyectoDTOFromIndicadorProyecto(getIndicadorProyecto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link IndicadorProyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link IndicadorProyecto} Mock
     */
    public static IndicadorProyecto getIndicadorProyecto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link IndicadorProyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link IndicadorProyecto}
     */
    public static List<IndicadorProyecto> getIndicadorProyectos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<IndicadorProyectoDTO> getResultSetIndicadorProyectosDTO(){
        List<IndicadorProyectoDTO> dtos = new ArrayList<IndicadorProyectoDTO>();
        for (IndicadorProyecto entity : entities) {
            dtos.add(DTOTransformer.getIndicadorProyectoDTOFromIndicadorProyecto(entity));
        }
        return new PmzResultSet<IndicadorProyectoDTO>(dtos, getTotalIndicadorProyectos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalIndicadorProyectos(){
        return Long.valueOf(entities.size());
    }

}
