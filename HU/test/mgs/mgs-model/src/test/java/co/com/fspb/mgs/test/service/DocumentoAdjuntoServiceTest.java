package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;
import co.com.fspb.mgs.dao.api.DocumentoAdjuntoDaoAPI;
import co.com.fspb.mgs.service.api.DocumentoAdjuntoServiceAPI;
import co.com.fspb.mgs.service.impl.DocumentoAdjuntoServiceImpl;
import co.com.fspb.mgs.test.service.data.DocumentoAdjuntoServiceTestData;
  import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link DocumentoAdjuntoServiceAPI} de la entidad {@link DocumentoAdjunto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DocumentoAdjuntoServiceAPI
 * @date nov 11, 2016
 */
public class DocumentoAdjuntoServiceTest {
	
    private static final Long ID_PRIMER_DOCUMENTOADJUNTO = 1L;

    @InjectMocks
    private DocumentoAdjuntoServiceAPI documentoAdjuntoServiceAPI = new DocumentoAdjuntoServiceImpl();

    @Mock
    private DocumentoAdjuntoDaoAPI documentoAdjuntoDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntos()).when(documentoAdjuntoDaoAPI).getRecords(null);
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getTotalDocumentoAdjuntos()).when(documentoAdjuntoDaoAPI).countRecords(null);
        PmzResultSet<DocumentoAdjuntoDTO> documentoAdjuntos = documentoAdjuntoServiceAPI.getRecords(null);
        Assert.assertEquals(DocumentoAdjuntoServiceTestData.getTotalDocumentoAdjuntos(), documentoAdjuntos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DocumentoAdjuntoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_documentoAdjunto_existe(){
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjunto()).when(documentoAdjuntoDaoAPI).get(ID_PRIMER_DOCUMENTOADJUNTO);
        DocumentoAdjuntoDTO documentoAdjunto = documentoAdjuntoServiceAPI.getDocumentoAdjuntoDTO(ID_PRIMER_DOCUMENTOADJUNTO);
        Assert.assertEquals(DocumentoAdjuntoServiceTestData.getDocumentoAdjunto().getId(), documentoAdjunto.getId());
    }

    /**
     * Prueba unitaria de get con {@link DocumentoAdjuntoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_documentoAdjunto_no_existe(){
        Mockito.doReturn(null).when(documentoAdjuntoDaoAPI).get(ID_PRIMER_DOCUMENTOADJUNTO);
        DocumentoAdjuntoDTO documentoAdjunto = documentoAdjuntoServiceAPI.getDocumentoAdjuntoDTO(ID_PRIMER_DOCUMENTOADJUNTO);
        Assert.assertEquals(null, documentoAdjunto);
    }

    /**
     * Prueba unitaria de registro de una {@link DocumentoAdjuntoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_documentoAdjunto_no_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	documentoAdjuntoServiceAPI.guardar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link DocumentoAdjuntoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_documentoAdjunto_existe() throws PmzException{
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjunto()).when(documentoAdjuntoDaoAPI).get(ID_PRIMER_DOCUMENTOADJUNTO);
        documentoAdjuntoServiceAPI.guardar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link DocumentoAdjuntoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_documentoAdjunto_no_existe_no_proyecto() throws PmzException{
    	documentoAdjuntoServiceAPI.guardar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link DocumentoAdjuntoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_documentoAdjunto_existe() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjunto()).when(documentoAdjuntoDaoAPI).get(ID_PRIMER_DOCUMENTOADJUNTO);
        documentoAdjuntoServiceAPI.editar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DocumentoAdjuntoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_documentoAdjunto_no_existe() throws PmzException{
        documentoAdjuntoServiceAPI.editar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DocumentoAdjuntoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_documentoAdjunto_existe_no_proyecto() throws PmzException{
        Mockito.doReturn(DocumentoAdjuntoServiceTestData.getDocumentoAdjunto()).when(documentoAdjuntoDaoAPI).get(ID_PRIMER_DOCUMENTOADJUNTO);
    	documentoAdjuntoServiceAPI.editar(DocumentoAdjuntoServiceTestData.getDocumentoAdjuntoDTO());
    }
    
    
}
