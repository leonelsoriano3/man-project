package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI;
import co.com.fspb.mgs.facade.impl.EntidadLineaFacadeImpl;
import co.com.fspb.mgs.service.api.EntidadLineaServiceAPI;
import co.com.fspb.mgs.test.service.data.EntidadLineaServiceTestData;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.test.service.data.LineaIntervencionServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EntidadLineaFacadeAPI} de la entidad {@link EntidadLinea}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaFacadeTest
 * @date nov 11, 2016
 */
public class EntidadLineaFacadeTest {

    private static final EntidadDTO ENTIDADDTO = EntidadServiceTestData.getEntidadDTO();
    private static final LineaIntervencionDTO LINEAINTERVENCIONDTO = LineaIntervencionServiceTestData.getLineaIntervencionDTO();

    @InjectMocks
    private EntidadLineaFacadeAPI entidadLineaFacadeAPI = new EntidadLineaFacadeImpl();

    @Mock
    private EntidadLineaServiceAPI entidadLineaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadLineaServiceTestData.getResultSetEntidadLineasDTO()).when(entidadLineaServiceAPI).getRecords(null);
        PmzResultSet<EntidadLineaDTO> entidadLineas = entidadLineaFacadeAPI.getRecords(null);
        Assert.assertEquals(EntidadLineaServiceTestData.getTotalEntidadLineas(), entidadLineas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadLineaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadLinea_existe(){
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLineaDTO()).when(entidadLineaServiceAPI).getEntidadLineaDTO(ENTIDADDTO, LINEAINTERVENCIONDTO);
        EntidadLineaDTO entidadLinea = entidadLineaFacadeAPI.getEntidadLineaDTO(ENTIDADDTO, LINEAINTERVENCIONDTO);
        Assert.assertEquals(EntidadLineaServiceTestData.getEntidadLineaDTO().getId(), entidadLinea.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadLineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_entidadLinea_no_existe() throws PmzException{
        entidadLineaFacadeAPI.guardar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EntidadLineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_entidadLinea_existe() throws PmzException{
        entidadLineaFacadeAPI.editar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
}
