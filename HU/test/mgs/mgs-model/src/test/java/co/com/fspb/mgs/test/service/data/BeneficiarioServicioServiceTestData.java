package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.BeneficiarioServicioPK;
import co.com.fspb.mgs.dao.model.BeneficiarioServicio;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link BeneficiarioServicio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioServiceTestData
 * @date nov 11, 2016
 */
public abstract class BeneficiarioServicioServiceTestData {
    
    private static List<BeneficiarioServicio> entities = new ArrayList<BeneficiarioServicio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            BeneficiarioServicio entity = new BeneficiarioServicio();

		    BeneficiarioServicioPK pk = new BeneficiarioServicioPK();
            pk.setBeneficiario(BeneficiarioServiceTestData.getBeneficiario());
            pk.setServicio(ServicioServiceTestData.getServicio());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link BeneficiarioServicio} Mock 
     * tranformado a DTO {@link BeneficiarioServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link BeneficiarioServicioDTO}
     */
    public static BeneficiarioServicioDTO getBeneficiarioServicioDTO(){
        return DTOTransformer.getBeneficiarioServicioDTOFromBeneficiarioServicio(getBeneficiarioServicio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link BeneficiarioServicio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link BeneficiarioServicio} Mock
     */
    public static BeneficiarioServicio getBeneficiarioServicio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link BeneficiarioServicio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link BeneficiarioServicio}
     */
    public static List<BeneficiarioServicio> getBeneficiarioServicios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<BeneficiarioServicioDTO> getResultSetBeneficiarioServiciosDTO(){
        List<BeneficiarioServicioDTO> dtos = new ArrayList<BeneficiarioServicioDTO>();
        for (BeneficiarioServicio entity : entities) {
            dtos.add(DTOTransformer.getBeneficiarioServicioDTOFromBeneficiarioServicio(entity));
        }
        return new PmzResultSet<BeneficiarioServicioDTO>(dtos, getTotalBeneficiarioServicios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalBeneficiarioServicios(){
        return Long.valueOf(entities.size());
    }

}
