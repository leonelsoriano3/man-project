package co.com.fspb.mgs.test.dao.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzDateInterval;
import com.premize.pmz.api.dto.PmzSearch;
import com.premize.pmz.api.dto.PmzSortField;

/**
 * Clase abstracta que creación de criterias genericos para las pruebas unitarias del los DAO.
 * Adicional ofrece metodos para que sean implementados por las pruebas unitarias concretas por entidad.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DaoTestData
 * @date nov 11, 2016
 */
public abstract class DaoTestData extends
		AbstractTransactionalJUnit4SpringContextTests {

	/**
	 * Creación del {@link PmzPagingCriteria} a partir de todos sus atributos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param search - valor para busqueda general en todos los campos
	 * @param searchFields - Lista de {@link PmzSearch}. Cada elemento de la lista es un campo de la tabla
	 * @param displayStart - Número de registro inicio de la consulta 
	 * @param displaySize - Número de registros máximo que debe retornar la consulta
	 * @param pageNumber - Número de la página en la que se encuentra el páginador
	 * @param sortFields - Lista de {@link PmzSortField} por los cuales se va a ordenar la consulta
	 * @param dateInterval - Lista de {@link PmzDateInterval} que representan los rangos de todos los campos fechas
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria createPagingCriteria(String search,
			List<PmzSearch> searchFields, Integer displayStart,
			Integer displaySize, Integer pageNumber,
			List<PmzSortField> sortFields, List<PmzDateInterval> dateInterval) {
		return new PmzPagingCriteria(search, searchFields, displayStart,
				displaySize, pageNumber, sortFields, dateInterval);
	}

	/**
	 * Creación del {@link PmzPagingCriteria} con sus atributos en nulo
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria getPagingCriteriaEmpty() {
		return new PmzPagingCriteria(null, null, null, null, null, null, null);
	}

	/**
	 * Creación del {@link PmzPagingCriteria} con sus atributos vacios
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria getPagingCriteriaAttr() {
		Integer iEcho = 0;
		Integer iDisplayStart = 0;
		Integer iDisplayLength = 0;
		Integer iColumns = 10;

		List<PmzSortField> sortFields = new ArrayList<PmzSortField>();

		List<PmzSearch> listSearchs = new ArrayList<PmzSearch>();
		for (int i = 0; i < iColumns; i++) {
			String sColName = "";
			String sSearchi = "";
			listSearchs.add(new PmzSearch(sColName, sSearchi));
		}

		List<PmzDateInterval> dateIntervals = new ArrayList<PmzDateInterval>();

		return new PmzPagingCriteria("", listSearchs, iDisplayStart,
				iDisplayLength, iEcho, sortFields, dateIntervals);
	}

	/**
	 * Creación del {@link PmzPagingCriteria} con sus atributos llenos con valores por defecto.
	 * Los valores por defecto se llenan en los métodos implementados por las pruebas unitarias concretas de cada entidad
	 * 
	 * @author PMZ - Premize S.A.S
	 * @param search - valor para busqueda general en todos los campos
	 * @param asc - true = Ascendente, false = Descendente
	 * @param searchs - Indica si deben incluirse los filtros por defecto
	 * @date nov 11, 2016
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria getPagingCriteriaFull(String search, boolean asc, boolean searchs) {
		Integer iEcho = 1;
		Integer iDisplayStart = 0;
		Integer iDisplayLength = 100;
		return new PmzPagingCriteria(search, getListSearchs(searchs), iDisplayStart,
				iDisplayLength, iEcho, getSortFields(asc), getIntervals(true,
						true));
	}

	/**
	 * Creación del {@link PmzPagingCriteria} con sus atributos llenos con valores por defecto.
	 * Los valores por defecto se llenan en los métodos implementados por las pruebas unitarias concretas de cada entidad
	 * Este metodo se usa para probar los intervalos de fechas solo con fecha inicial
	 * 
	 * @author PMZ - Premize S.A.S
	 * @param search - valor para busqueda general en todos los campos
	 * @param asc - true = Ascendente, false = Descendente
	 * @param searchs - Indica si deben incluirse los filtros por defecto
	 * @date nov 11, 2016
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria getPagingCriteriaFullInital(String search, boolean asc, boolean searchs) {
		Integer iEcho = 1;
		Integer iDisplayStart = 0;
		Integer iDisplayLength = 100;
		return new PmzPagingCriteria(search, getListSearchs(searchs), iDisplayStart,
				iDisplayLength, iEcho, getSortFields(asc), getIntervals(true,
						false));
	}

	/**
	 * Creación del {@link PmzPagingCriteria} con sus atributos llenos con valores por defecto.
	 * Los valores por defecto se llenan en los métodos implementados por las pruebas unitarias concretas de cada entidad
	 * Este metodo se usa para probar los intervalos de fechas solo con fecha final
	 * 
	 * @author PMZ - Premize S.A.S
	 * @param search - valor para busqueda general en todos los campos
	 * @param asc - true = Ascendente, false = Descendente
	 * @param searchs - Indica si deben incluirse los filtros por defecto
	 * @date nov 11, 2016
	 * @return {@link PmzPagingCriteria}
	 */
	protected PmzPagingCriteria getPagingCriteriaFullFinal(String search, boolean asc, boolean searchs) {
		Integer iEcho = 1;
		Integer iDisplayStart = 0;
		Integer iDisplayLength = 100;
		return new PmzPagingCriteria(search, getListSearchs(searchs), iDisplayStart,
				iDisplayLength, iEcho, getSortFields(asc), getIntervals(false,
						true));
	}

	/**
	 * Método abstracto que debe ser implementado por las pruebas untarias concretas.
	 * Este metodo crea la lista de filtros de los campos (no fechas) de la entidad. 
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param searchs - Indica si se deben incluir los filtros o estar vacíos
	 * @return {@link List} - Lista de filtros
	 */
	public abstract List<PmzSearch> getListSearchs(boolean searchs);

	/**
	 * Método abstracto que debe ser implementado por las pruebas untarias concretas.
	 * Este metodo crea la lista de campos por los que se ordena la entidas. 
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param asc - true = Ascendente, false = Descendente
	 * @return {@link List} - Lista de campos
	 */
	public abstract List<PmzSortField> getSortFields(boolean asc);
	
	/**
	 * Método abstracto que debe ser implementado por las pruebas untarias concretas.
	 * Este metodo crea la lista de filtros de los campos (solo fechas) de la entidad. 
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param fechaInicial - Indica si el rango tiene fecha inicial
	 * @param fechaFinal - Indica si el rango tiene fecha final
	 * @return {@link List} - Lista de intervalos de fechas
	 */
	public abstract List<PmzDateInterval> getIntervals(boolean fechaInicial,
			boolean fechaFinal);

}
