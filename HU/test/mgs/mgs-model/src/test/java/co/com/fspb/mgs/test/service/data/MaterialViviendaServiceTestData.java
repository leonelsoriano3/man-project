package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.MaterialVivienda;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link MaterialVivienda}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MaterialViviendaServiceTestData
 * @date nov 11, 2016
 */
public abstract class MaterialViviendaServiceTestData {
    
    private static List<MaterialVivienda> entities = new ArrayList<MaterialVivienda>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            MaterialVivienda entity = new MaterialVivienda();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link MaterialVivienda} Mock 
     * tranformado a DTO {@link MaterialViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link MaterialViviendaDTO}
     */
    public static MaterialViviendaDTO getMaterialViviendaDTO(){
        return DTOTransformer.getMaterialViviendaDTOFromMaterialVivienda(getMaterialVivienda());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link MaterialVivienda} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link MaterialVivienda} Mock
     */
    public static MaterialVivienda getMaterialVivienda(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link MaterialVivienda} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link MaterialVivienda}
     */
    public static List<MaterialVivienda> getMaterialViviendas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<MaterialViviendaDTO> getResultSetMaterialViviendasDTO(){
        List<MaterialViviendaDTO> dtos = new ArrayList<MaterialViviendaDTO>();
        for (MaterialVivienda entity : entities) {
            dtos.add(DTOTransformer.getMaterialViviendaDTOFromMaterialVivienda(entity));
        }
        return new PmzResultSet<MaterialViviendaDTO>(dtos, getTotalMaterialViviendas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalMaterialViviendas(){
        return Long.valueOf(entities.size());
    }

}
