package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.ParametroGeneral;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.TipoParametroEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ParametroGeneral}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneralServiceTestData
 * @date nov 11, 2016
 */
public abstract class ParametroGeneralServiceTestData {
    
    private static List<ParametroGeneral> entities = new ArrayList<ParametroGeneral>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ParametroGeneral entity = new ParametroGeneral();

            entity.setId(new Long(i));
            
            entity.setTipoParametro(TipoParametroEnum.TIPOPARAMETROENUM_1);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setDescripcion("Descripcion" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setClave("Clave" + i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ParametroGeneral} Mock 
     * tranformado a DTO {@link ParametroGeneralDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ParametroGeneralDTO}
     */
    public static ParametroGeneralDTO getParametroGeneralDTO(){
        return DTOTransformer.getParametroGeneralDTOFromParametroGeneral(getParametroGeneral());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ParametroGeneral} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ParametroGeneral} Mock
     */
    public static ParametroGeneral getParametroGeneral(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ParametroGeneral} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ParametroGeneral}
     */
    public static List<ParametroGeneral> getParametroGenerals(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ParametroGeneralDTO> getResultSetParametroGeneralsDTO(){
        List<ParametroGeneralDTO> dtos = new ArrayList<ParametroGeneralDTO>();
        for (ParametroGeneral entity : entities) {
            dtos.add(DTOTransformer.getParametroGeneralDTOFromParametroGeneral(entity));
        }
        return new PmzResultSet<ParametroGeneralDTO>(dtos, getTotalParametroGenerals(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalParametroGenerals(){
        return Long.valueOf(entities.size());
    }

}
