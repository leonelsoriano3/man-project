package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.DatoContacto;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link DatoContacto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoContactoServiceTestData
 * @date nov 11, 2016
 */
public abstract class DatoContactoServiceTestData {
    
    private static List<DatoContacto> entities = new ArrayList<DatoContacto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            DatoContacto entity = new DatoContacto();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setPersona(PersonaServiceTestData.getPersona());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setTipoContacto(TipoContactoServiceTestData.getTipoContacto());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DatoContacto} Mock 
     * tranformado a DTO {@link DatoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link DatoContactoDTO}
     */
    public static DatoContactoDTO getDatoContactoDTO(){
        return DTOTransformer.getDatoContactoDTOFromDatoContacto(getDatoContacto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DatoContacto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link DatoContacto} Mock
     */
    public static DatoContacto getDatoContacto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link DatoContacto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link DatoContacto}
     */
    public static List<DatoContacto> getDatoContactos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<DatoContactoDTO> getResultSetDatoContactosDTO(){
        List<DatoContactoDTO> dtos = new ArrayList<DatoContactoDTO>();
        for (DatoContacto entity : entities) {
            dtos.add(DTOTransformer.getDatoContactoDTOFromDatoContacto(entity));
        }
        return new PmzResultSet<DatoContactoDTO>(dtos, getTotalDatoContactos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalDatoContactos(){
        return Long.valueOf(entities.size());
    }

}
