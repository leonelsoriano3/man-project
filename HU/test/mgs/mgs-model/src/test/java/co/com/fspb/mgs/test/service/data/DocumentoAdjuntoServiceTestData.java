package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.DocumentoAdjunto;
import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link DocumentoAdjunto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DocumentoAdjuntoServiceTestData
 * @date nov 11, 2016
 */
public abstract class DocumentoAdjuntoServiceTestData {
    
    private static List<DocumentoAdjunto> entities = new ArrayList<DocumentoAdjunto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            DocumentoAdjunto entity = new DocumentoAdjunto();

            entity.setId(new Long(i));
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setDescripcion("Descripcion" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setProyecto(ProyectoServiceTestData.getProyecto());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DocumentoAdjunto} Mock 
     * tranformado a DTO {@link DocumentoAdjuntoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link DocumentoAdjuntoDTO}
     */
    public static DocumentoAdjuntoDTO getDocumentoAdjuntoDTO(){
        return DTOTransformer.getDocumentoAdjuntoDTOFromDocumentoAdjunto(getDocumentoAdjunto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DocumentoAdjunto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link DocumentoAdjunto} Mock
     */
    public static DocumentoAdjunto getDocumentoAdjunto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link DocumentoAdjunto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link DocumentoAdjunto}
     */
    public static List<DocumentoAdjunto> getDocumentoAdjuntos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<DocumentoAdjuntoDTO> getResultSetDocumentoAdjuntosDTO(){
        List<DocumentoAdjuntoDTO> dtos = new ArrayList<DocumentoAdjuntoDTO>();
        for (DocumentoAdjunto entity : entities) {
            dtos.add(DTOTransformer.getDocumentoAdjuntoDTOFromDocumentoAdjunto(entity));
        }
        return new PmzResultSet<DocumentoAdjuntoDTO>(dtos, getTotalDocumentoAdjuntos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalDocumentoAdjuntos(){
        return Long.valueOf(entities.size());
    }

}
