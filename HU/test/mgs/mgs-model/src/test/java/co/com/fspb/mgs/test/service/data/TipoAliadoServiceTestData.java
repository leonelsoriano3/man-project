package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TipoAliado;
import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TipoAliado}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoServiceTestData
 * @date nov 11, 2016
 */
public abstract class TipoAliadoServiceTestData {
    
    private static List<TipoAliado> entities = new ArrayList<TipoAliado>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TipoAliado entity = new TipoAliado();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoAliado} Mock 
     * tranformado a DTO {@link TipoAliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TipoAliadoDTO}
     */
    public static TipoAliadoDTO getTipoAliadoDTO(){
        return DTOTransformer.getTipoAliadoDTOFromTipoAliado(getTipoAliado());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoAliado} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TipoAliado} Mock
     */
    public static TipoAliado getTipoAliado(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TipoAliado} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TipoAliado}
     */
    public static List<TipoAliado> getTipoAliados(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TipoAliadoDTO> getResultSetTipoAliadosDTO(){
        List<TipoAliadoDTO> dtos = new ArrayList<TipoAliadoDTO>();
        for (TipoAliado entity : entities) {
            dtos.add(DTOTransformer.getTipoAliadoDTOFromTipoAliado(entity));
        }
        return new PmzResultSet<TipoAliadoDTO>(dtos, getTotalTipoAliados(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTipoAliados(){
        return Long.valueOf(entities.size());
    }

}
