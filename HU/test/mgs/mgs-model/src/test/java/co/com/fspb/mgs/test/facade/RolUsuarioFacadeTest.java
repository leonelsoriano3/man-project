package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI;
import co.com.fspb.mgs.facade.impl.RolUsuarioFacadeImpl;
import co.com.fspb.mgs.service.api.RolUsuarioServiceAPI;
import co.com.fspb.mgs.test.service.data.RolUsuarioServiceTestData;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.test.service.data.RolServiceTestData;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link RolUsuarioFacadeAPI} de la entidad {@link RolUsuario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioFacadeTest
 * @date nov 11, 2016
 */
public class RolUsuarioFacadeTest {

    private static final RolDTO ROLDTO = RolServiceTestData.getRolDTO();
    private static final UsuarioDTO USUARIODTO = UsuarioServiceTestData.getUsuarioDTO();

    @InjectMocks
    private RolUsuarioFacadeAPI rolUsuarioFacadeAPI = new RolUsuarioFacadeImpl();

    @Mock
    private RolUsuarioServiceAPI rolUsuarioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolUsuarioServiceTestData.getResultSetRolUsuariosDTO()).when(rolUsuarioServiceAPI).getRecords(null);
        PmzResultSet<RolUsuarioDTO> rolUsuarios = rolUsuarioFacadeAPI.getRecords(null);
        Assert.assertEquals(RolUsuarioServiceTestData.getTotalRolUsuarios(), rolUsuarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolUsuarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolUsuario_existe(){
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuarioDTO()).when(rolUsuarioServiceAPI).getRolUsuarioDTO(ROLDTO, USUARIODTO);
        RolUsuarioDTO rolUsuario = rolUsuarioFacadeAPI.getRolUsuarioDTO(ROLDTO, USUARIODTO);
        Assert.assertEquals(RolUsuarioServiceTestData.getRolUsuarioDTO().getId(), rolUsuario.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link RolUsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_rolUsuario_no_existe() throws PmzException{
        rolUsuarioFacadeAPI.guardar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link RolUsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_rolUsuario_existe() throws PmzException{
        rolUsuarioFacadeAPI.editar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
}
