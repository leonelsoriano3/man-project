package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.ProblemaComunidad;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ProblemaComunidad}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadServiceTestData
 * @date nov 11, 2016
 */
public abstract class ProblemaComunidadServiceTestData {
    
    private static List<ProblemaComunidad> entities = new ArrayList<ProblemaComunidad>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ProblemaComunidad entity = new ProblemaComunidad();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProblemaComunidad} Mock 
     * tranformado a DTO {@link ProblemaComunidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ProblemaComunidadDTO}
     */
    public static ProblemaComunidadDTO getProblemaComunidadDTO(){
        return DTOTransformer.getProblemaComunidadDTOFromProblemaComunidad(getProblemaComunidad());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProblemaComunidad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ProblemaComunidad} Mock
     */
    public static ProblemaComunidad getProblemaComunidad(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ProblemaComunidad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ProblemaComunidad}
     */
    public static List<ProblemaComunidad> getProblemaComunidads(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ProblemaComunidadDTO> getResultSetProblemaComunidadsDTO(){
        List<ProblemaComunidadDTO> dtos = new ArrayList<ProblemaComunidadDTO>();
        for (ProblemaComunidad entity : entities) {
            dtos.add(DTOTransformer.getProblemaComunidadDTOFromProblemaComunidad(entity));
        }
        return new PmzResultSet<ProblemaComunidadDTO>(dtos, getTotalProblemaComunidads(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalProblemaComunidads(){
        return Long.valueOf(entities.size());
    }

}
