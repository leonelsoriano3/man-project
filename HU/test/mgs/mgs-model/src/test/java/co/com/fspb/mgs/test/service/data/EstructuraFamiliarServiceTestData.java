package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.EstructuraFamiliar;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link EstructuraFamiliar}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstructuraFamiliarServiceTestData
 * @date nov 11, 2016
 */
public abstract class EstructuraFamiliarServiceTestData {
    
    private static List<EstructuraFamiliar> entities = new ArrayList<EstructuraFamiliar>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            EstructuraFamiliar entity = new EstructuraFamiliar();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstructuraFamiliar} Mock 
     * tranformado a DTO {@link EstructuraFamiliarDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EstructuraFamiliarDTO}
     */
    public static EstructuraFamiliarDTO getEstructuraFamiliarDTO(){
        return DTOTransformer.getEstructuraFamiliarDTOFromEstructuraFamiliar(getEstructuraFamiliar());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EstructuraFamiliar} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link EstructuraFamiliar} Mock
     */
    public static EstructuraFamiliar getEstructuraFamiliar(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link EstructuraFamiliar} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link EstructuraFamiliar}
     */
    public static List<EstructuraFamiliar> getEstructuraFamiliars(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EstructuraFamiliarDTO> getResultSetEstructuraFamiliarsDTO(){
        List<EstructuraFamiliarDTO> dtos = new ArrayList<EstructuraFamiliarDTO>();
        for (EstructuraFamiliar entity : entities) {
            dtos.add(DTOTransformer.getEstructuraFamiliarDTOFromEstructuraFamiliar(entity));
        }
        return new PmzResultSet<EstructuraFamiliarDTO>(dtos, getTotalEstructuraFamiliars(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEstructuraFamiliars(){
        return Long.valueOf(entities.size());
    }

}
