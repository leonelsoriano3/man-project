package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI;
import co.com.fspb.mgs.facade.impl.TipoViviendaFacadeImpl;
import co.com.fspb.mgs.service.api.TipoViviendaServiceAPI;
import co.com.fspb.mgs.test.service.data.TipoViviendaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TipoViviendaFacadeAPI} de la entidad {@link TipoVivienda}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoViviendaFacadeTest
 * @date nov 11, 2016
 */
public class TipoViviendaFacadeTest {

    private static final Long ID_PRIMER_TIPOVIVIENDA = 1L;

    @InjectMocks
    private TipoViviendaFacadeAPI tipoViviendaFacadeAPI = new TipoViviendaFacadeImpl();

    @Mock
    private TipoViviendaServiceAPI tipoViviendaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoViviendaServiceTestData.getResultSetTipoViviendasDTO()).when(tipoViviendaServiceAPI).getRecords(null);
        PmzResultSet<TipoViviendaDTO> tipoViviendas = tipoViviendaFacadeAPI.getRecords(null);
        Assert.assertEquals(TipoViviendaServiceTestData.getTotalTipoViviendas(), tipoViviendas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoViviendaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoVivienda_existe(){
        Mockito.doReturn(TipoViviendaServiceTestData.getTipoViviendaDTO()).when(tipoViviendaServiceAPI).getTipoViviendaDTO(ID_PRIMER_TIPOVIVIENDA);
        TipoViviendaDTO tipoVivienda = tipoViviendaFacadeAPI.getTipoViviendaDTO(ID_PRIMER_TIPOVIVIENDA);
        Assert.assertEquals(TipoViviendaServiceTestData.getTipoViviendaDTO().getId(), tipoVivienda.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_tipoVivienda_no_existe() throws PmzException{
        tipoViviendaFacadeAPI.guardar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TipoViviendaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_tipoVivienda_existe() throws PmzException{
        tipoViviendaFacadeAPI.editar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }
    
}
