package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.dao.api.ProyectoBeneficiarioDaoAPI;
import co.com.fspb.mgs.service.api.ProyectoBeneficiarioServiceAPI;
import co.com.fspb.mgs.service.impl.ProyectoBeneficiarioServiceImpl;
import co.com.fspb.mgs.test.service.data.ProyectoBeneficiarioServiceTestData;
    import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ProyectoBeneficiarioServiceAPI} de la entidad {@link ProyectoBeneficiario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioServiceAPI
 * @date nov 11, 2016
 */
public class ProyectoBeneficiarioServiceTest {
	
    private static final Integer ID_PRIMER_PROYECTOBENEFICIARIO = 1;

    @InjectMocks
    private ProyectoBeneficiarioServiceAPI proyectoBeneficiarioServiceAPI = new ProyectoBeneficiarioServiceImpl();

    @Mock
    private ProyectoBeneficiarioDaoAPI proyectoBeneficiarioDaoAPI;
    @Mock
    private BeneficiarioDaoAPI beneficiarioDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarios()).when(proyectoBeneficiarioDaoAPI).getRecords(null);
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getTotalProyectoBeneficiarios()).when(proyectoBeneficiarioDaoAPI).countRecords(null);
        PmzResultSet<ProyectoBeneficiarioDTO> proyectoBeneficiarios = proyectoBeneficiarioServiceAPI.getRecords(null);
        Assert.assertEquals(ProyectoBeneficiarioServiceTestData.getTotalProyectoBeneficiarios(), proyectoBeneficiarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoBeneficiarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoBeneficiario_existe(){
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
        ProyectoBeneficiarioDTO proyectoBeneficiario = proyectoBeneficiarioServiceAPI.getProyectoBeneficiarioDTO(ID_PRIMER_PROYECTOBENEFICIARIO);
        Assert.assertEquals(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario().getId(), proyectoBeneficiario.getId());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoBeneficiarioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoBeneficiario_no_existe(){
        Mockito.doReturn(null).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
        ProyectoBeneficiarioDTO proyectoBeneficiario = proyectoBeneficiarioServiceAPI.getProyectoBeneficiarioDTO(ID_PRIMER_PROYECTOBENEFICIARIO);
        Assert.assertEquals(null, proyectoBeneficiario);
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoBeneficiarioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_proyectoBeneficiario_no_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	proyectoBeneficiarioServiceAPI.guardar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoBeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoBeneficiario_existe() throws PmzException{
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
        proyectoBeneficiarioServiceAPI.guardar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoBeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoBeneficiario_no_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	proyectoBeneficiarioServiceAPI.guardar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoBeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoBeneficiario_no_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	proyectoBeneficiarioServiceAPI.guardar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ProyectoBeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_proyectoBeneficiario_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
        proyectoBeneficiarioServiceAPI.editar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoBeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_proyectoBeneficiario_no_existe() throws PmzException{
        proyectoBeneficiarioServiceAPI.editar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoBeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyectoBeneficiario_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
    	proyectoBeneficiarioServiceAPI.editar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoBeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyectoBeneficiario_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ID_PRIMER_PROYECTOBENEFICIARIO);
    	proyectoBeneficiarioServiceAPI.editar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
    
}
