package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.dao.api.MaterialViviendaDaoAPI;
import co.com.fspb.mgs.service.api.MaterialViviendaServiceAPI;
import co.com.fspb.mgs.service.impl.MaterialViviendaServiceImpl;
import co.com.fspb.mgs.test.service.data.MaterialViviendaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link MaterialViviendaServiceAPI} de la entidad {@link MaterialVivienda}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MaterialViviendaServiceAPI
 * @date nov 11, 2016
 */
public class MaterialViviendaServiceTest {
	
    private static final Long ID_PRIMER_MATERIALVIVIENDA = 1L;

    @InjectMocks
    private MaterialViviendaServiceAPI materialViviendaServiceAPI = new MaterialViviendaServiceImpl();

    @Mock
    private MaterialViviendaDaoAPI materialViviendaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialViviendas()).when(materialViviendaDaoAPI).getRecords(null);
        Mockito.doReturn(MaterialViviendaServiceTestData.getTotalMaterialViviendas()).when(materialViviendaDaoAPI).countRecords(null);
        PmzResultSet<MaterialViviendaDTO> materialViviendas = materialViviendaServiceAPI.getRecords(null);
        Assert.assertEquals(MaterialViviendaServiceTestData.getTotalMaterialViviendas(), materialViviendas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link MaterialViviendaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_materialVivienda_existe(){
        Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(ID_PRIMER_MATERIALVIVIENDA);
        MaterialViviendaDTO materialVivienda = materialViviendaServiceAPI.getMaterialViviendaDTO(ID_PRIMER_MATERIALVIVIENDA);
        Assert.assertEquals(MaterialViviendaServiceTestData.getMaterialVivienda().getId(), materialVivienda.getId());
    }

    /**
     * Prueba unitaria de get con {@link MaterialViviendaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_materialVivienda_no_existe(){
        Mockito.doReturn(null).when(materialViviendaDaoAPI).get(ID_PRIMER_MATERIALVIVIENDA);
        MaterialViviendaDTO materialVivienda = materialViviendaServiceAPI.getMaterialViviendaDTO(ID_PRIMER_MATERIALVIVIENDA);
        Assert.assertEquals(null, materialVivienda);
    }

    /**
     * Prueba unitaria de registro de una {@link MaterialViviendaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_materialVivienda_no_existe() throws PmzException{
    	materialViviendaServiceAPI.guardar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link MaterialViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_materialVivienda_existe() throws PmzException{
        Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(ID_PRIMER_MATERIALVIVIENDA);
        materialViviendaServiceAPI.guardar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link MaterialViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_materialVivienda_existe() throws PmzException{
        Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(ID_PRIMER_MATERIALVIVIENDA);
        materialViviendaServiceAPI.editar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link MaterialViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_materialVivienda_no_existe() throws PmzException{
        materialViviendaServiceAPI.editar(MaterialViviendaServiceTestData.getMaterialViviendaDTO());
    }
    
    
}
