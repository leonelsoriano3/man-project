package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.facade.api.ServicioFacadeAPI;
import co.com.fspb.mgs.facade.impl.ServicioFacadeImpl;
import co.com.fspb.mgs.service.api.ServicioServiceAPI;
import co.com.fspb.mgs.test.service.data.ServicioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ServicioFacadeAPI} de la entidad {@link Servicio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioFacadeTest
 * @date nov 11, 2016
 */
public class ServicioFacadeTest {

    private static final Long ID_PRIMER_SERVICIO = 1L;

    @InjectMocks
    private ServicioFacadeAPI servicioFacadeAPI = new ServicioFacadeImpl();

    @Mock
    private ServicioServiceAPI servicioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ServicioServiceTestData.getResultSetServiciosDTO()).when(servicioServiceAPI).getRecords(null);
        PmzResultSet<ServicioDTO> servicios = servicioFacadeAPI.getRecords(null);
        Assert.assertEquals(ServicioServiceTestData.getTotalServicios(), servicios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ServicioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_servicio_existe(){
        Mockito.doReturn(ServicioServiceTestData.getServicioDTO()).when(servicioServiceAPI).getServicioDTO(ID_PRIMER_SERVICIO);
        ServicioDTO servicio = servicioFacadeAPI.getServicioDTO(ID_PRIMER_SERVICIO);
        Assert.assertEquals(ServicioServiceTestData.getServicioDTO().getId(), servicio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_servicio_no_existe() throws PmzException{
        servicioFacadeAPI.guardar(ServicioServiceTestData.getServicioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_servicio_existe() throws PmzException{
        servicioFacadeAPI.editar(ServicioServiceTestData.getServicioDTO());
    }
    
}
