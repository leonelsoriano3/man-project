package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI;
import co.com.fspb.mgs.facade.impl.ProyectoBeneficiarioFacadeImpl;
import co.com.fspb.mgs.service.api.ProyectoBeneficiarioServiceAPI;
import co.com.fspb.mgs.test.service.data.ProyectoBeneficiarioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ProyectoBeneficiarioFacadeAPI} de la entidad {@link ProyectoBeneficiario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioFacadeTest
 * @date nov 11, 2016
 */
public class ProyectoBeneficiarioFacadeTest {

    private static final Integer ID_PRIMER_PROYECTOBENEFICIARIO = 1;

    @InjectMocks
    private ProyectoBeneficiarioFacadeAPI proyectoBeneficiarioFacadeAPI = new ProyectoBeneficiarioFacadeImpl();

    @Mock
    private ProyectoBeneficiarioServiceAPI proyectoBeneficiarioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getResultSetProyectoBeneficiariosDTO()).when(proyectoBeneficiarioServiceAPI).getRecords(null);
        PmzResultSet<ProyectoBeneficiarioDTO> proyectoBeneficiarios = proyectoBeneficiarioFacadeAPI.getRecords(null);
        Assert.assertEquals(ProyectoBeneficiarioServiceTestData.getTotalProyectoBeneficiarios(), proyectoBeneficiarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoBeneficiarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoBeneficiario_existe(){
        Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO()).when(proyectoBeneficiarioServiceAPI).getProyectoBeneficiarioDTO(ID_PRIMER_PROYECTOBENEFICIARIO);
        ProyectoBeneficiarioDTO proyectoBeneficiario = proyectoBeneficiarioFacadeAPI.getProyectoBeneficiarioDTO(ID_PRIMER_PROYECTOBENEFICIARIO);
        Assert.assertEquals(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO().getId(), proyectoBeneficiario.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoBeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_proyectoBeneficiario_no_existe() throws PmzException{
        proyectoBeneficiarioFacadeAPI.guardar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ProyectoBeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_proyectoBeneficiario_existe() throws PmzException{
        proyectoBeneficiarioFacadeAPI.editar(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiarioDTO());
    }
    
}
