package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.DescIndependiente;
import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link DescIndependiente}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DescIndependienteServiceTestData
 * @date nov 11, 2016
 */
public abstract class DescIndependienteServiceTestData {
    
    private static List<DescIndependiente> entities = new ArrayList<DescIndependiente>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            DescIndependiente entity = new DescIndependiente();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DescIndependiente} Mock 
     * tranformado a DTO {@link DescIndependienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link DescIndependienteDTO}
     */
    public static DescIndependienteDTO getDescIndependienteDTO(){
        return DTOTransformer.getDescIndependienteDTOFromDescIndependiente(getDescIndependiente());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link DescIndependiente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link DescIndependiente} Mock
     */
    public static DescIndependiente getDescIndependiente(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link DescIndependiente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link DescIndependiente}
     */
    public static List<DescIndependiente> getDescIndependientes(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<DescIndependienteDTO> getResultSetDescIndependientesDTO(){
        List<DescIndependienteDTO> dtos = new ArrayList<DescIndependienteDTO>();
        for (DescIndependiente entity : entities) {
            dtos.add(DTOTransformer.getDescIndependienteDTOFromDescIndependiente(entity));
        }
        return new PmzResultSet<DescIndependienteDTO>(dtos, getTotalDescIndependientes(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalDescIndependientes(){
        return Long.valueOf(entities.size());
    }

}
