package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.dao.api.ParienteDaoAPI;
import co.com.fspb.mgs.service.api.ParienteServiceAPI;
import co.com.fspb.mgs.service.impl.ParienteServiceImpl;
import co.com.fspb.mgs.test.service.data.ParienteServiceTestData;
import co.com.fspb.mgs.dao.model.ParientePK;
    import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import co.com.fspb.mgs.dao.api.ParentescoDaoAPI;
import co.com.fspb.mgs.test.service.data.ParentescoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ParienteServiceAPI} de la entidad {@link Pariente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParienteServiceAPI
 * @date nov 11, 2016
 */
public class ParienteServiceTest {
	
    private static final BeneficiarioDTO BENEFICIARIODTO = BeneficiarioServiceTestData.getBeneficiarioDTO();
    private static final PersonaDTO PERSONADTO = PersonaServiceTestData.getPersonaDTO();

    @InjectMocks
    private ParienteServiceAPI parienteServiceAPI = new ParienteServiceImpl();

    @Mock
    private ParienteDaoAPI parienteDaoAPI;
    @Mock
    private BeneficiarioDaoAPI beneficiarioDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;
    @Mock
    private ParentescoDaoAPI parentescoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParienteServiceTestData.getParientes()).when(parienteDaoAPI).getRecords(null);
        Mockito.doReturn(ParienteServiceTestData.getTotalParientes()).when(parienteDaoAPI).countRecords(null);
        PmzResultSet<ParienteDTO> parientes = parienteServiceAPI.getRecords(null);
        Assert.assertEquals(ParienteServiceTestData.getTotalParientes(), parientes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParienteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_pariente_existe(){
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
        ParienteDTO pariente = parienteServiceAPI.getParienteDTO(BENEFICIARIODTO, PERSONADTO);
        Assert.assertEquals(ParienteServiceTestData.getParienteDTO().getId(), pariente.getId());
    }

    /**
     * Prueba unitaria de get con {@link ParienteDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_pariente_no_existe(){
        Mockito.doReturn(null).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
        ParienteDTO pariente = parienteServiceAPI.getParienteDTO(BENEFICIARIODTO, PERSONADTO);
        Assert.assertEquals(null, pariente);
    }

    /**
     * Prueba unitaria de registro de una {@link ParienteDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_pariente_no_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
    	parienteServiceAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ParienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_pariente_existe() throws PmzException{
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
        parienteServiceAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ParienteDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_pariente_no_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
    	parienteServiceAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ParienteDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_pariente_no_existe_no_persona() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
    	parienteServiceAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ParienteDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con parentesco
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_pariente_no_existe_no_parentesco() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	parienteServiceAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_pariente_existe() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
        parienteServiceAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_pariente_no_existe() throws PmzException{
        parienteServiceAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_pariente_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
    	parienteServiceAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_pariente_existe_no_persona() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(ParentescoServiceTestData.getParentesco()).when(parentescoDaoAPI).get(ParentescoServiceTestData.getParentesco().getId());
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
    	parienteServiceAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con parentesco
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_pariente_existe_no_parentesco() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
        Mockito.doReturn(ParienteServiceTestData.getPariente()).when(parienteDaoAPI).get(Mockito.any(ParientePK.class));
    	parienteServiceAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
    
}
