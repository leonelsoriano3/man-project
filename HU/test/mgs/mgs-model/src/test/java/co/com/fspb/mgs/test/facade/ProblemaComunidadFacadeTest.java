package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import co.com.fspb.mgs.facade.api.ProblemaComunidadFacadeAPI;
import co.com.fspb.mgs.facade.impl.ProblemaComunidadFacadeImpl;
import co.com.fspb.mgs.service.api.ProblemaComunidadServiceAPI;
import co.com.fspb.mgs.test.service.data.ProblemaComunidadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ProblemaComunidadFacadeAPI} de la entidad {@link ProblemaComunidad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadFacadeTest
 * @date nov 11, 2016
 */
public class ProblemaComunidadFacadeTest {

    private static final Long ID_PRIMER_PROBLEMACOMUNIDAD = 1L;

    @InjectMocks
    private ProblemaComunidadFacadeAPI problemaComunidadFacadeAPI = new ProblemaComunidadFacadeImpl();

    @Mock
    private ProblemaComunidadServiceAPI problemaComunidadServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProblemaComunidadServiceTestData.getResultSetProblemaComunidadsDTO()).when(problemaComunidadServiceAPI).getRecords(null);
        PmzResultSet<ProblemaComunidadDTO> problemaComunidads = problemaComunidadFacadeAPI.getRecords(null);
        Assert.assertEquals(ProblemaComunidadServiceTestData.getTotalProblemaComunidads(), problemaComunidads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProblemaComunidadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_problemaComunidad_existe(){
        Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidadDTO()).when(problemaComunidadServiceAPI).getProblemaComunidadDTO(ID_PRIMER_PROBLEMACOMUNIDAD);
        ProblemaComunidadDTO problemaComunidad = problemaComunidadFacadeAPI.getProblemaComunidadDTO(ID_PRIMER_PROBLEMACOMUNIDAD);
        Assert.assertEquals(ProblemaComunidadServiceTestData.getProblemaComunidadDTO().getId(), problemaComunidad.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ProblemaComunidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_problemaComunidad_no_existe() throws PmzException{
        problemaComunidadFacadeAPI.guardar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ProblemaComunidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_problemaComunidad_existe() throws PmzException{
        problemaComunidadFacadeAPI.editar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }
    
}
