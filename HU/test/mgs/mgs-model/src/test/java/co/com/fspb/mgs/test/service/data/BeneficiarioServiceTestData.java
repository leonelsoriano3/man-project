package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstratoSocioeconomicoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.GeneroEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Beneficiario}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServiceTestData
 * @date nov 11, 2016
 */
public abstract class BeneficiarioServiceTestData {
    
    private static List<Beneficiario> entities = new ArrayList<Beneficiario>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Beneficiario entity = new Beneficiario();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setAreaEducacion(AreaEducacionServiceTestData.getAreaEducacion());
            entity.setEstructuraFamiliar(EstructuraFamiliarServiceTestData.getEstructuraFamiliar());
            entity.setEstratoSocioeconomico(EstratoSocioeconomicoEnum.ESTRATOSOCIOECONOMICOENUM_1);
            entity.setEstadoCivil(EstadoCivilServiceTestData.getEstadoCivil());
            entity.setDescIndependiente(DescIndependienteServiceTestData.getDescIndependiente());
            entity.setNivelEducativo(NivelEducativoServiceTestData.getNivelEducativo());
            entity.setOtraAreaEducacion("OtraAreaEducacion" + i);
            entity.setTipoVivienda(TipoViviendaServiceTestData.getTipoVivienda());
            entity.setCantPersonasHogar(i);
            entity.setOrganizacionComunitaria("OrganizacionComunitaria" + i);
            entity.setNivelIngresos(i);
            entity.setCantPersonasCargo(i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setTipoTenencia(TipoTenenciaServiceTestData.getTipoTenencia());
            entity.setZona(ZonaServiceTestData.getZona());
            entity.setMaterialVivienda(MaterialViviendaServiceTestData.getMaterialVivienda());
            entity.setBdNacional(BdNacionalServiceTestData.getBdNacional());
            entity.setAfiliadoSalud(AfiliadoSaludServiceTestData.getAfiliadoSalud());
            entity.setPersona(PersonaServiceTestData.getPersona());
            entity.setGrupoEtnico(GrupoEtnicoServiceTestData.getGrupoEtnico());
            entity.setOcupacion(OcupacionServiceTestData.getOcupacion());
            entity.setGenero(GeneroEnum.FEMENINO);
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setFechaCrea(new Date());
            entity.setMunicipio(MunicipioServiceTestData.getMunicipio());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Beneficiario} Mock 
     * tranformado a DTO {@link BeneficiarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link BeneficiarioDTO}
     */
    public static BeneficiarioDTO getBeneficiarioDTO(){
        return DTOTransformer.getBeneficiarioDTOFromBeneficiario(getBeneficiario());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Beneficiario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Beneficiario} Mock
     */
    public static Beneficiario getBeneficiario(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Beneficiario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Beneficiario}
     */
    public static List<Beneficiario> getBeneficiarios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<BeneficiarioDTO> getResultSetBeneficiariosDTO(){
        List<BeneficiarioDTO> dtos = new ArrayList<BeneficiarioDTO>();
        for (Beneficiario entity : entities) {
            dtos.add(DTOTransformer.getBeneficiarioDTOFromBeneficiario(entity));
        }
        return new PmzResultSet<BeneficiarioDTO>(dtos, getTotalBeneficiarios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalBeneficiarios(){
        return Long.valueOf(entities.size());
    }

}
