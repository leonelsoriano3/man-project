package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.facade.api.LiderFacadeAPI;
import co.com.fspb.mgs.facade.impl.LiderFacadeImpl;
import co.com.fspb.mgs.service.api.LiderServiceAPI;
import co.com.fspb.mgs.test.service.data.LiderServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link LiderFacadeAPI} de la entidad {@link Lider}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderFacadeTest
 * @date nov 11, 2016
 */
public class LiderFacadeTest {

    private static final Long ID_PRIMER_LIDER = 1L;

    @InjectMocks
    private LiderFacadeAPI liderFacadeAPI = new LiderFacadeImpl();

    @Mock
    private LiderServiceAPI liderServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LiderServiceTestData.getResultSetLidersDTO()).when(liderServiceAPI).getRecords(null);
        PmzResultSet<LiderDTO> liders = liderFacadeAPI.getRecords(null);
        Assert.assertEquals(LiderServiceTestData.getTotalLiders(), liders.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LiderDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lider_existe(){
        Mockito.doReturn(LiderServiceTestData.getLiderDTO()).when(liderServiceAPI).getLiderDTO(ID_PRIMER_LIDER);
        LiderDTO lider = liderFacadeAPI.getLiderDTO(ID_PRIMER_LIDER);
        Assert.assertEquals(LiderServiceTestData.getLiderDTO().getId(), lider.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link LiderDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_lider_no_existe() throws PmzException{
        liderFacadeAPI.guardar(LiderServiceTestData.getLiderDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link LiderDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_lider_existe() throws PmzException{
        liderFacadeAPI.editar(LiderServiceTestData.getLiderDTO());
    }
    
}
