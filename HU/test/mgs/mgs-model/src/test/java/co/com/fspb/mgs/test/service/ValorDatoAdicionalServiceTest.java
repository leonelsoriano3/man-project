package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import co.com.fspb.mgs.dao.api.ValorDatoAdicionalDaoAPI;
import co.com.fspb.mgs.service.api.ValorDatoAdicionalServiceAPI;
import co.com.fspb.mgs.service.impl.ValorDatoAdicionalServiceImpl;
import co.com.fspb.mgs.test.service.data.ValorDatoAdicionalServiceTestData;
    import co.com.fspb.mgs.dao.api.DatoAdicionalDaoAPI;
import co.com.fspb.mgs.test.service.data.DatoAdicionalServiceTestData;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ValorDatoAdicionalServiceAPI} de la entidad {@link ValorDatoAdicional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ValorDatoAdicionalServiceAPI
 * @date nov 11, 2016
 */
public class ValorDatoAdicionalServiceTest {
	
    private static final Long ID_PRIMER_VALORDATOADICIONAL = 1L;

    @InjectMocks
    private ValorDatoAdicionalServiceAPI valorDatoAdicionalServiceAPI = new ValorDatoAdicionalServiceImpl();

    @Mock
    private ValorDatoAdicionalDaoAPI valorDatoAdicionalDaoAPI;
    @Mock
    private DatoAdicionalDaoAPI datoAdicionalDaoAPI;
    @Mock
    private BeneficiarioDaoAPI beneficiarioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicionals()).when(valorDatoAdicionalDaoAPI).getRecords(null);
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getTotalValorDatoAdicionals()).when(valorDatoAdicionalDaoAPI).countRecords(null);
        PmzResultSet<ValorDatoAdicionalDTO> valorDatoAdicionals = valorDatoAdicionalServiceAPI.getRecords(null);
        Assert.assertEquals(ValorDatoAdicionalServiceTestData.getTotalValorDatoAdicionals(), valorDatoAdicionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ValorDatoAdicionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_valorDatoAdicional_existe(){
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicional()).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
        ValorDatoAdicionalDTO valorDatoAdicional = valorDatoAdicionalServiceAPI.getValorDatoAdicionalDTO(ID_PRIMER_VALORDATOADICIONAL);
        Assert.assertEquals(ValorDatoAdicionalServiceTestData.getValorDatoAdicional().getId(), valorDatoAdicional.getId());
    }

    /**
     * Prueba unitaria de get con {@link ValorDatoAdicionalDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_valorDatoAdicional_no_existe(){
        Mockito.doReturn(null).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
        ValorDatoAdicionalDTO valorDatoAdicional = valorDatoAdicionalServiceAPI.getValorDatoAdicionalDTO(ID_PRIMER_VALORDATOADICIONAL);
        Assert.assertEquals(null, valorDatoAdicional);
    }

    /**
     * Prueba unitaria de registro de una {@link ValorDatoAdicionalDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_valorDatoAdicional_no_existe() throws PmzException{
    	Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(DatoAdicionalServiceTestData.getDatoAdicional().getId());
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	valorDatoAdicionalServiceAPI.guardar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ValorDatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_valorDatoAdicional_existe() throws PmzException{
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicional()).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
        valorDatoAdicionalServiceAPI.guardar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ValorDatoAdicionalDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con datoAdicional
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_valorDatoAdicional_no_existe_no_datoAdicional() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
    	valorDatoAdicionalServiceAPI.guardar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ValorDatoAdicionalDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_valorDatoAdicional_no_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(DatoAdicionalServiceTestData.getDatoAdicional().getId());
    	valorDatoAdicionalServiceAPI.guardar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ValorDatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_valorDatoAdicional_existe() throws PmzException{
    	Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(DatoAdicionalServiceTestData.getDatoAdicional().getId());
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicional()).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
        valorDatoAdicionalServiceAPI.editar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ValorDatoAdicionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_valorDatoAdicional_no_existe() throws PmzException{
        valorDatoAdicionalServiceAPI.editar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ValorDatoAdicionalDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con datoAdicional
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_valorDatoAdicional_existe_no_datoAdicional() throws PmzException{
    	Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(BeneficiarioServiceTestData.getBeneficiario().getId());
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicional()).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
    	valorDatoAdicionalServiceAPI.editar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ValorDatoAdicionalDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con beneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_valorDatoAdicional_existe_no_beneficiario() throws PmzException{
    	Mockito.doReturn(DatoAdicionalServiceTestData.getDatoAdicional()).when(datoAdicionalDaoAPI).get(DatoAdicionalServiceTestData.getDatoAdicional().getId());
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicional()).when(valorDatoAdicionalDaoAPI).get(ID_PRIMER_VALORDATOADICIONAL);
    	valorDatoAdicionalServiceAPI.editar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
    
}
