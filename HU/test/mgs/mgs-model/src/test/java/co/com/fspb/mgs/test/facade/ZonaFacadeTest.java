package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.facade.api.ZonaFacadeAPI;
import co.com.fspb.mgs.facade.impl.ZonaFacadeImpl;
import co.com.fspb.mgs.service.api.ZonaServiceAPI;
import co.com.fspb.mgs.test.service.data.ZonaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ZonaFacadeAPI} de la entidad {@link Zona}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaFacadeTest
 * @date nov 11, 2016
 */
public class ZonaFacadeTest {

    private static final Long ID_PRIMER_ZONA = 1L;

    @InjectMocks
    private ZonaFacadeAPI zonaFacadeAPI = new ZonaFacadeImpl();

    @Mock
    private ZonaServiceAPI zonaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ZonaServiceTestData.getResultSetZonasDTO()).when(zonaServiceAPI).getRecords(null);
        PmzResultSet<ZonaDTO> zonas = zonaFacadeAPI.getRecords(null);
        Assert.assertEquals(ZonaServiceTestData.getTotalZonas(), zonas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ZonaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_zona_existe(){
        Mockito.doReturn(ZonaServiceTestData.getZonaDTO()).when(zonaServiceAPI).getZonaDTO(ID_PRIMER_ZONA);
        ZonaDTO zona = zonaFacadeAPI.getZonaDTO(ID_PRIMER_ZONA);
        Assert.assertEquals(ZonaServiceTestData.getZonaDTO().getId(), zona.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ZonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_zona_no_existe() throws PmzException{
        zonaFacadeAPI.guardar(ZonaServiceTestData.getZonaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ZonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_zona_existe() throws PmzException{
        zonaFacadeAPI.editar(ZonaServiceTestData.getZonaDTO());
    }
    
}
