package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Parentesco;
import co.com.fspb.mgs.dto.ParentescoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Parentesco}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoServiceTestData
 * @date nov 11, 2016
 */
public abstract class ParentescoServiceTestData {
    
    private static List<Parentesco> entities = new ArrayList<Parentesco>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Parentesco entity = new Parentesco();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Parentesco} Mock 
     * tranformado a DTO {@link ParentescoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ParentescoDTO}
     */
    public static ParentescoDTO getParentescoDTO(){
        return DTOTransformer.getParentescoDTOFromParentesco(getParentesco());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Parentesco} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Parentesco} Mock
     */
    public static Parentesco getParentesco(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Parentesco} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Parentesco}
     */
    public static List<Parentesco> getParentescos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ParentescoDTO> getResultSetParentescosDTO(){
        List<ParentescoDTO> dtos = new ArrayList<ParentescoDTO>();
        for (Parentesco entity : entities) {
            dtos.add(DTOTransformer.getParentescoDTOFromParentesco(entity));
        }
        return new PmzResultSet<ParentescoDTO>(dtos, getTotalParentescos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalParentescos(){
        return Long.valueOf(entities.size());
    }

}
