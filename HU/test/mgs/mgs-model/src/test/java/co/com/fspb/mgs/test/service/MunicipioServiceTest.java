package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.dao.api.MunicipioDaoAPI;
import co.com.fspb.mgs.service.api.MunicipioServiceAPI;
import co.com.fspb.mgs.service.impl.MunicipioServiceImpl;
import co.com.fspb.mgs.test.service.data.MunicipioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link MunicipioServiceAPI} de la entidad {@link Municipio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioServiceAPI
 * @date nov 11, 2016
 */
public class MunicipioServiceTest {
	
    private static final Long ID_PRIMER_MUNICIPIO = 1L;

    @InjectMocks
    private MunicipioServiceAPI municipioServiceAPI = new MunicipioServiceImpl();

    @Mock
    private MunicipioDaoAPI municipioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(MunicipioServiceTestData.getMunicipios()).when(municipioDaoAPI).getRecords(null);
        Mockito.doReturn(MunicipioServiceTestData.getTotalMunicipios()).when(municipioDaoAPI).countRecords(null);
        PmzResultSet<MunicipioDTO> municipios = municipioServiceAPI.getRecords(null);
        Assert.assertEquals(MunicipioServiceTestData.getTotalMunicipios(), municipios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link MunicipioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_municipio_existe(){
        Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(ID_PRIMER_MUNICIPIO);
        MunicipioDTO municipio = municipioServiceAPI.getMunicipioDTO(ID_PRIMER_MUNICIPIO);
        Assert.assertEquals(MunicipioServiceTestData.getMunicipio().getId(), municipio.getId());
    }

    /**
     * Prueba unitaria de get con {@link MunicipioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_municipio_no_existe(){
        Mockito.doReturn(null).when(municipioDaoAPI).get(ID_PRIMER_MUNICIPIO);
        MunicipioDTO municipio = municipioServiceAPI.getMunicipioDTO(ID_PRIMER_MUNICIPIO);
        Assert.assertEquals(null, municipio);
    }

    /**
     * Prueba unitaria de registro de una {@link MunicipioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_municipio_no_existe() throws PmzException{
    	municipioServiceAPI.guardar(MunicipioServiceTestData.getMunicipioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link MunicipioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_municipio_existe() throws PmzException{
        Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(ID_PRIMER_MUNICIPIO);
        municipioServiceAPI.guardar(MunicipioServiceTestData.getMunicipioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link MunicipioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_municipio_existe() throws PmzException{
        Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(ID_PRIMER_MUNICIPIO);
        municipioServiceAPI.editar(MunicipioServiceTestData.getMunicipioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link MunicipioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_municipio_no_existe() throws PmzException{
        municipioServiceAPI.editar(MunicipioServiceTestData.getMunicipioDTO());
    }
    
    
}
