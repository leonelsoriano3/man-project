package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.dao.api.TipoContactoDaoAPI;
import co.com.fspb.mgs.service.api.TipoContactoServiceAPI;
import co.com.fspb.mgs.service.impl.TipoContactoServiceImpl;
import co.com.fspb.mgs.test.service.data.TipoContactoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TipoContactoServiceAPI} de la entidad {@link TipoContacto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoServiceAPI
 * @date nov 11, 2016
 */
public class TipoContactoServiceTest {
	
    private static final Long ID_PRIMER_TIPOCONTACTO = 1L;

    @InjectMocks
    private TipoContactoServiceAPI tipoContactoServiceAPI = new TipoContactoServiceImpl();

    @Mock
    private TipoContactoDaoAPI tipoContactoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoContactoServiceTestData.getTipoContactos()).when(tipoContactoDaoAPI).getRecords(null);
        Mockito.doReturn(TipoContactoServiceTestData.getTotalTipoContactos()).when(tipoContactoDaoAPI).countRecords(null);
        PmzResultSet<TipoContactoDTO> tipoContactos = tipoContactoServiceAPI.getRecords(null);
        Assert.assertEquals(TipoContactoServiceTestData.getTotalTipoContactos(), tipoContactos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoContactoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoContacto_existe(){
        Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(ID_PRIMER_TIPOCONTACTO);
        TipoContactoDTO tipoContacto = tipoContactoServiceAPI.getTipoContactoDTO(ID_PRIMER_TIPOCONTACTO);
        Assert.assertEquals(TipoContactoServiceTestData.getTipoContacto().getId(), tipoContacto.getId());
    }

    /**
     * Prueba unitaria de get con {@link TipoContactoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoContacto_no_existe(){
        Mockito.doReturn(null).when(tipoContactoDaoAPI).get(ID_PRIMER_TIPOCONTACTO);
        TipoContactoDTO tipoContacto = tipoContactoServiceAPI.getTipoContactoDTO(ID_PRIMER_TIPOCONTACTO);
        Assert.assertEquals(null, tipoContacto);
    }

    /**
     * Prueba unitaria de registro de una {@link TipoContactoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_tipoContacto_no_existe() throws PmzException{
    	tipoContactoServiceAPI.guardar(TipoContactoServiceTestData.getTipoContactoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoContacto_existe() throws PmzException{
        Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(ID_PRIMER_TIPOCONTACTO);
        tipoContactoServiceAPI.guardar(TipoContactoServiceTestData.getTipoContactoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TipoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_tipoContacto_existe() throws PmzException{
        Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(ID_PRIMER_TIPOCONTACTO);
        tipoContactoServiceAPI.editar(TipoContactoServiceTestData.getTipoContactoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_tipoContacto_no_existe() throws PmzException{
        tipoContactoServiceAPI.editar(TipoContactoServiceTestData.getTipoContactoDTO());
    }
    
    
}
