package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Indicador}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorServiceTestData
 * @date nov 11, 2016
 */
public abstract class IndicadorServiceTestData {
    
    private static List<Indicador> entities = new ArrayList<Indicador>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Indicador entity = new Indicador();

            entity.setId(new Long(i));
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setMeta(new Long(i));
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Indicador} Mock 
     * tranformado a DTO {@link IndicadorDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link IndicadorDTO}
     */
    public static IndicadorDTO getIndicadorDTO(){
        return DTOTransformer.getIndicadorDTOFromIndicador(getIndicador());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Indicador} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Indicador} Mock
     */
    public static Indicador getIndicador(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Indicador} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Indicador}
     */
    public static List<Indicador> getIndicadors(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<IndicadorDTO> getResultSetIndicadorsDTO(){
        List<IndicadorDTO> dtos = new ArrayList<IndicadorDTO>();
        for (Indicador entity : entities) {
            dtos.add(DTOTransformer.getIndicadorDTOFromIndicador(entity));
        }
        return new PmzResultSet<IndicadorDTO>(dtos, getTotalIndicadors(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalIndicadors(){
        return Long.valueOf(entities.size());
    }

}
