package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.EntidadTerritorioPK;
import co.com.fspb.mgs.dao.model.EntidadTerritorio;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link EntidadTerritorio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioServiceTestData
 * @date nov 11, 2016
 */
public abstract class EntidadTerritorioServiceTestData {
    
    private static List<EntidadTerritorio> entities = new ArrayList<EntidadTerritorio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            EntidadTerritorio entity = new EntidadTerritorio();

		    EntidadTerritorioPK pk = new EntidadTerritorioPK();
            pk.setTerritorio(TerritorioServiceTestData.getTerritorio());
            pk.setEntidad(EntidadServiceTestData.getEntidad());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EntidadTerritorio} Mock 
     * tranformado a DTO {@link EntidadTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EntidadTerritorioDTO}
     */
    public static EntidadTerritorioDTO getEntidadTerritorioDTO(){
        return DTOTransformer.getEntidadTerritorioDTOFromEntidadTerritorio(getEntidadTerritorio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EntidadTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link EntidadTerritorio} Mock
     */
    public static EntidadTerritorio getEntidadTerritorio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link EntidadTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link EntidadTerritorio}
     */
    public static List<EntidadTerritorio> getEntidadTerritorios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EntidadTerritorioDTO> getResultSetEntidadTerritoriosDTO(){
        List<EntidadTerritorioDTO> dtos = new ArrayList<EntidadTerritorioDTO>();
        for (EntidadTerritorio entity : entities) {
            dtos.add(DTOTransformer.getEntidadTerritorioDTOFromEntidadTerritorio(entity));
        }
        return new PmzResultSet<EntidadTerritorioDTO>(dtos, getTotalEntidadTerritorios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEntidadTerritorios(){
        return Long.valueOf(entities.size());
    }

}
