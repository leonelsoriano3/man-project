package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.dao.api.ProyectoTerritorioDaoAPI;
import co.com.fspb.mgs.service.api.ProyectoTerritorioServiceAPI;
import co.com.fspb.mgs.service.impl.ProyectoTerritorioServiceImpl;
import co.com.fspb.mgs.test.service.data.ProyectoTerritorioServiceTestData;
import co.com.fspb.mgs.dao.model.ProyectoTerritorioPK;
  import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ProyectoTerritorioServiceAPI} de la entidad {@link ProyectoTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioServiceAPI
 * @date nov 11, 2016
 */
public class ProyectoTerritorioServiceTest {
	
    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();

    @InjectMocks
    private ProyectoTerritorioServiceAPI proyectoTerritorioServiceAPI = new ProyectoTerritorioServiceImpl();

    @Mock
    private ProyectoTerritorioDaoAPI proyectoTerritorioDaoAPI;
    @Mock
    private TerritorioDaoAPI territorioDaoAPI;
    @Mock
    private ProyectoDaoAPI proyectoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorios()).when(proyectoTerritorioDaoAPI).getRecords(null);
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getTotalProyectoTerritorios()).when(proyectoTerritorioDaoAPI).countRecords(null);
        PmzResultSet<ProyectoTerritorioDTO> proyectoTerritorios = proyectoTerritorioServiceAPI.getRecords(null);
        Assert.assertEquals(ProyectoTerritorioServiceTestData.getTotalProyectoTerritorios(), proyectoTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoTerritorio_existe(){
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorio()).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
        ProyectoTerritorioDTO proyectoTerritorio = proyectoTerritorioServiceAPI.getProyectoTerritorioDTO(TERRITORIODTO, PROYECTODTO);
        Assert.assertEquals(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO().getId(), proyectoTerritorio.getId());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoTerritorioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoTerritorio_no_existe(){
        Mockito.doReturn(null).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
        ProyectoTerritorioDTO proyectoTerritorio = proyectoTerritorioServiceAPI.getProyectoTerritorioDTO(TERRITORIODTO, PROYECTODTO);
        Assert.assertEquals(null, proyectoTerritorio);
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoTerritorioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_proyectoTerritorio_no_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	proyectoTerritorioServiceAPI.guardar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoTerritorio_existe() throws PmzException{
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorio()).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
        proyectoTerritorioServiceAPI.guardar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoTerritorio_no_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
    	proyectoTerritorioServiceAPI.guardar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ProyectoTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_proyectoTerritorio_no_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	proyectoTerritorioServiceAPI.guardar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ProyectoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_proyectoTerritorio_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorio()).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
        proyectoTerritorioServiceAPI.editar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_proyectoTerritorio_no_existe() throws PmzException{
        proyectoTerritorioServiceAPI.editar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyectoTerritorio_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(ProyectoServiceTestData.getProyecto()).when(proyectoDaoAPI).get(ProyectoServiceTestData.getProyecto().getId());
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorio()).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
    	proyectoTerritorioServiceAPI.editar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProyectoTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyecto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_proyectoTerritorio_existe_no_proyecto() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorio()).when(proyectoTerritorioDaoAPI).get(Mockito.any(ProyectoTerritorioPK.class));
    	proyectoTerritorioServiceAPI.editar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
    
}
