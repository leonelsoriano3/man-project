package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI;
import co.com.fspb.mgs.facade.impl.TipoContactoFacadeImpl;
import co.com.fspb.mgs.service.api.TipoContactoServiceAPI;
import co.com.fspb.mgs.test.service.data.TipoContactoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TipoContactoFacadeAPI} de la entidad {@link TipoContacto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoFacadeTest
 * @date nov 11, 2016
 */
public class TipoContactoFacadeTest {

    private static final Long ID_PRIMER_TIPOCONTACTO = 1L;

    @InjectMocks
    private TipoContactoFacadeAPI tipoContactoFacadeAPI = new TipoContactoFacadeImpl();

    @Mock
    private TipoContactoServiceAPI tipoContactoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoContactoServiceTestData.getResultSetTipoContactosDTO()).when(tipoContactoServiceAPI).getRecords(null);
        PmzResultSet<TipoContactoDTO> tipoContactos = tipoContactoFacadeAPI.getRecords(null);
        Assert.assertEquals(TipoContactoServiceTestData.getTotalTipoContactos(), tipoContactos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoContactoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoContacto_existe(){
        Mockito.doReturn(TipoContactoServiceTestData.getTipoContactoDTO()).when(tipoContactoServiceAPI).getTipoContactoDTO(ID_PRIMER_TIPOCONTACTO);
        TipoContactoDTO tipoContacto = tipoContactoFacadeAPI.getTipoContactoDTO(ID_PRIMER_TIPOCONTACTO);
        Assert.assertEquals(TipoContactoServiceTestData.getTipoContactoDTO().getId(), tipoContacto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_tipoContacto_no_existe() throws PmzException{
        tipoContactoFacadeAPI.guardar(TipoContactoServiceTestData.getTipoContactoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TipoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_tipoContacto_existe() throws PmzException{
        tipoContactoFacadeAPI.editar(TipoContactoServiceTestData.getTipoContactoDTO());
    }
    
}
