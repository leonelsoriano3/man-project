package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.EntidadLineaPK;
import co.com.fspb.mgs.dao.model.EntidadLinea;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link EntidadLinea}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaServiceTestData
 * @date nov 11, 2016
 */
public abstract class EntidadLineaServiceTestData {
    
    private static List<EntidadLinea> entities = new ArrayList<EntidadLinea>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            EntidadLinea entity = new EntidadLinea();

		    EntidadLineaPK pk = new EntidadLineaPK();
            pk.setEntidad(EntidadServiceTestData.getEntidad());
            pk.setLineaIntervencion(LineaIntervencionServiceTestData.getLineaIntervencion());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EntidadLinea} Mock 
     * tranformado a DTO {@link EntidadLineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EntidadLineaDTO}
     */
    public static EntidadLineaDTO getEntidadLineaDTO(){
        return DTOTransformer.getEntidadLineaDTOFromEntidadLinea(getEntidadLinea());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link EntidadLinea} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link EntidadLinea} Mock
     */
    public static EntidadLinea getEntidadLinea(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link EntidadLinea} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link EntidadLinea}
     */
    public static List<EntidadLinea> getEntidadLineas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EntidadLineaDTO> getResultSetEntidadLineasDTO(){
        List<EntidadLineaDTO> dtos = new ArrayList<EntidadLineaDTO>();
        for (EntidadLinea entity : entities) {
            dtos.add(DTOTransformer.getEntidadLineaDTOFromEntidadLinea(entity));
        }
        return new PmzResultSet<EntidadLineaDTO>(dtos, getTotalEntidadLineas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEntidadLineas(){
        return Long.valueOf(entities.size());
    }

}
