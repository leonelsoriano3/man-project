package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.ComponenteProyectoPK;
import co.com.fspb.mgs.dao.model.ComponenteProyecto;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ComponenteProyecto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoServiceTestData
 * @date nov 11, 2016
 */
public abstract class ComponenteProyectoServiceTestData {
    
    private static List<ComponenteProyecto> entities = new ArrayList<ComponenteProyecto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ComponenteProyecto entity = new ComponenteProyecto();

		    ComponenteProyectoPK pk = new ComponenteProyectoPK();
            pk.setProyecto(ProyectoServiceTestData.getProyecto());
            pk.setComponente(ComponenteServiceTestData.getComponente());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ComponenteProyecto} Mock 
     * tranformado a DTO {@link ComponenteProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ComponenteProyectoDTO}
     */
    public static ComponenteProyectoDTO getComponenteProyectoDTO(){
        return DTOTransformer.getComponenteProyectoDTOFromComponenteProyecto(getComponenteProyecto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ComponenteProyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ComponenteProyecto} Mock
     */
    public static ComponenteProyecto getComponenteProyecto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ComponenteProyecto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ComponenteProyecto}
     */
    public static List<ComponenteProyecto> getComponenteProyectos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ComponenteProyectoDTO> getResultSetComponenteProyectosDTO(){
        List<ComponenteProyectoDTO> dtos = new ArrayList<ComponenteProyectoDTO>();
        for (ComponenteProyecto entity : entities) {
            dtos.add(DTOTransformer.getComponenteProyectoDTOFromComponenteProyecto(entity));
        }
        return new PmzResultSet<ComponenteProyectoDTO>(dtos, getTotalComponenteProyectos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalComponenteProyectos(){
        return Long.valueOf(entities.size());
    }

}
