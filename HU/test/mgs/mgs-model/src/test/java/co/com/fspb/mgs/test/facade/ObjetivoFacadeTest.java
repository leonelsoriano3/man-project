package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.facade.api.ObjetivoFacadeAPI;
import co.com.fspb.mgs.facade.impl.ObjetivoFacadeImpl;
import co.com.fspb.mgs.service.api.ObjetivoServiceAPI;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ObjetivoFacadeAPI} de la entidad {@link Objetivo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoFacadeTest
 * @date nov 11, 2016
 */
public class ObjetivoFacadeTest {

    private static final Long ID_PRIMER_OBJETIVO = 1L;

    @InjectMocks
    private ObjetivoFacadeAPI objetivoFacadeAPI = new ObjetivoFacadeImpl();

    @Mock
    private ObjetivoServiceAPI objetivoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ObjetivoServiceTestData.getResultSetObjetivosDTO()).when(objetivoServiceAPI).getRecords(null);
        PmzResultSet<ObjetivoDTO> objetivos = objetivoFacadeAPI.getRecords(null);
        Assert.assertEquals(ObjetivoServiceTestData.getTotalObjetivos(), objetivos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ObjetivoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_objetivo_existe(){
        Mockito.doReturn(ObjetivoServiceTestData.getObjetivoDTO()).when(objetivoServiceAPI).getObjetivoDTO(ID_PRIMER_OBJETIVO);
        ObjetivoDTO objetivo = objetivoFacadeAPI.getObjetivoDTO(ID_PRIMER_OBJETIVO);
        Assert.assertEquals(ObjetivoServiceTestData.getObjetivoDTO().getId(), objetivo.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_objetivo_no_existe() throws PmzException{
        objetivoFacadeAPI.guardar(ObjetivoServiceTestData.getObjetivoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_objetivo_existe() throws PmzException{
        objetivoFacadeAPI.editar(ObjetivoServiceTestData.getObjetivoDTO());
    }
    
}
