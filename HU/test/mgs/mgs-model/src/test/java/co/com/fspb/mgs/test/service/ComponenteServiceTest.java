package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.service.api.ComponenteServiceAPI;
import co.com.fspb.mgs.service.impl.ComponenteServiceImpl;
import co.com.fspb.mgs.test.service.data.ComponenteServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ComponenteServiceAPI} de la entidad {@link Componente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteServiceAPI
 * @date nov 11, 2016
 */
public class ComponenteServiceTest {
	
    private static final Long ID_PRIMER_COMPONENTE = 1L;

    @InjectMocks
    private ComponenteServiceAPI componenteServiceAPI = new ComponenteServiceImpl();

    @Mock
    private ComponenteDaoAPI componenteDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ComponenteServiceTestData.getComponentes()).when(componenteDaoAPI).getRecords(null);
        Mockito.doReturn(ComponenteServiceTestData.getTotalComponentes()).when(componenteDaoAPI).countRecords(null);
        PmzResultSet<ComponenteDTO> componentes = componenteServiceAPI.getRecords(null);
        Assert.assertEquals(ComponenteServiceTestData.getTotalComponentes(), componentes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componente_existe(){
        Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ID_PRIMER_COMPONENTE);
        ComponenteDTO componente = componenteServiceAPI.getComponenteDTO(ID_PRIMER_COMPONENTE);
        Assert.assertEquals(ComponenteServiceTestData.getComponente().getId(), componente.getId());
    }

    /**
     * Prueba unitaria de get con {@link ComponenteDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_componente_no_existe(){
        Mockito.doReturn(null).when(componenteDaoAPI).get(ID_PRIMER_COMPONENTE);
        ComponenteDTO componente = componenteServiceAPI.getComponenteDTO(ID_PRIMER_COMPONENTE);
        Assert.assertEquals(null, componente);
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_componente_no_existe() throws PmzException{
    	componenteServiceAPI.guardar(ComponenteServiceTestData.getComponenteDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ComponenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_componente_existe() throws PmzException{
        Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ID_PRIMER_COMPONENTE);
        componenteServiceAPI.guardar(ComponenteServiceTestData.getComponenteDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ComponenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_componente_existe() throws PmzException{
        Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ID_PRIMER_COMPONENTE);
        componenteServiceAPI.editar(ComponenteServiceTestData.getComponenteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ComponenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_componente_no_existe() throws PmzException{
        componenteServiceAPI.editar(ComponenteServiceTestData.getComponenteDTO());
    }
    
    
}
