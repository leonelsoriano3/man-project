package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.BdNacional;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link BdNacional}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BdNacionalServiceTestData
 * @date nov 11, 2016
 */
public abstract class BdNacionalServiceTestData {
    
    private static List<BdNacional> entities = new ArrayList<BdNacional>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            BdNacional entity = new BdNacional();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link BdNacional} Mock 
     * tranformado a DTO {@link BdNacionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link BdNacionalDTO}
     */
    public static BdNacionalDTO getBdNacionalDTO(){
        return DTOTransformer.getBdNacionalDTOFromBdNacional(getBdNacional());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link BdNacional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link BdNacional} Mock
     */
    public static BdNacional getBdNacional(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link BdNacional} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link BdNacional}
     */
    public static List<BdNacional> getBdNacionals(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<BdNacionalDTO> getResultSetBdNacionalsDTO(){
        List<BdNacionalDTO> dtos = new ArrayList<BdNacionalDTO>();
        for (BdNacional entity : entities) {
            dtos.add(DTOTransformer.getBdNacionalDTOFromBdNacional(entity));
        }
        return new PmzResultSet<BdNacionalDTO>(dtos, getTotalBdNacionals(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalBdNacionals(){
        return Long.valueOf(entities.size());
    }

}
