package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.dao.api.LiderDaoAPI;
import co.com.fspb.mgs.service.api.LiderServiceAPI;
import co.com.fspb.mgs.service.impl.LiderServiceImpl;
import co.com.fspb.mgs.test.service.data.LiderServiceTestData;
  import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link LiderServiceAPI} de la entidad {@link Lider}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderServiceAPI
 * @date nov 11, 2016
 */
public class LiderServiceTest {
	
    private static final Long ID_PRIMER_LIDER = 1L;

    @InjectMocks
    private LiderServiceAPI liderServiceAPI = new LiderServiceImpl();

    @Mock
    private LiderDaoAPI liderDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LiderServiceTestData.getLiders()).when(liderDaoAPI).getRecords(null);
        Mockito.doReturn(LiderServiceTestData.getTotalLiders()).when(liderDaoAPI).countRecords(null);
        PmzResultSet<LiderDTO> liders = liderServiceAPI.getRecords(null);
        Assert.assertEquals(LiderServiceTestData.getTotalLiders(), liders.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LiderDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lider_existe(){
        Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(ID_PRIMER_LIDER);
        LiderDTO lider = liderServiceAPI.getLiderDTO(ID_PRIMER_LIDER);
        Assert.assertEquals(LiderServiceTestData.getLider().getId(), lider.getId());
    }

    /**
     * Prueba unitaria de get con {@link LiderDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lider_no_existe(){
        Mockito.doReturn(null).when(liderDaoAPI).get(ID_PRIMER_LIDER);
        LiderDTO lider = liderServiceAPI.getLiderDTO(ID_PRIMER_LIDER);
        Assert.assertEquals(null, lider);
    }

    /**
     * Prueba unitaria de registro de una {@link LiderDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_lider_no_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	liderServiceAPI.guardar(LiderServiceTestData.getLiderDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link LiderDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_lider_existe() throws PmzException{
        Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(ID_PRIMER_LIDER);
        liderServiceAPI.guardar(LiderServiceTestData.getLiderDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link LiderDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_lider_no_existe_no_persona() throws PmzException{
    	liderServiceAPI.guardar(LiderServiceTestData.getLiderDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link LiderDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_lider_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
        Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(ID_PRIMER_LIDER);
        liderServiceAPI.editar(LiderServiceTestData.getLiderDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LiderDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_lider_no_existe() throws PmzException{
        liderServiceAPI.editar(LiderServiceTestData.getLiderDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LiderDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_lider_existe_no_persona() throws PmzException{
        Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(ID_PRIMER_LIDER);
    	liderServiceAPI.editar(LiderServiceTestData.getLiderDTO());
    }
    
    
}
