package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI;
import co.com.fspb.mgs.facade.impl.ValorDatoAdicionalFacadeImpl;
import co.com.fspb.mgs.service.api.ValorDatoAdicionalServiceAPI;
import co.com.fspb.mgs.test.service.data.ValorDatoAdicionalServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ValorDatoAdicionalFacadeAPI} de la entidad {@link ValorDatoAdicional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ValorDatoAdicionalFacadeTest
 * @date nov 11, 2016
 */
public class ValorDatoAdicionalFacadeTest {

    private static final Long ID_PRIMER_VALORDATOADICIONAL = 1L;

    @InjectMocks
    private ValorDatoAdicionalFacadeAPI valorDatoAdicionalFacadeAPI = new ValorDatoAdicionalFacadeImpl();

    @Mock
    private ValorDatoAdicionalServiceAPI valorDatoAdicionalServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getResultSetValorDatoAdicionalsDTO()).when(valorDatoAdicionalServiceAPI).getRecords(null);
        PmzResultSet<ValorDatoAdicionalDTO> valorDatoAdicionals = valorDatoAdicionalFacadeAPI.getRecords(null);
        Assert.assertEquals(ValorDatoAdicionalServiceTestData.getTotalValorDatoAdicionals(), valorDatoAdicionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ValorDatoAdicionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_valorDatoAdicional_existe(){
        Mockito.doReturn(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO()).when(valorDatoAdicionalServiceAPI).getValorDatoAdicionalDTO(ID_PRIMER_VALORDATOADICIONAL);
        ValorDatoAdicionalDTO valorDatoAdicional = valorDatoAdicionalFacadeAPI.getValorDatoAdicionalDTO(ID_PRIMER_VALORDATOADICIONAL);
        Assert.assertEquals(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO().getId(), valorDatoAdicional.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ValorDatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_valorDatoAdicional_no_existe() throws PmzException{
        valorDatoAdicionalFacadeAPI.guardar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ValorDatoAdicionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_valorDatoAdicional_existe() throws PmzException{
        valorDatoAdicionalFacadeAPI.editar(ValorDatoAdicionalServiceTestData.getValorDatoAdicionalDTO());
    }
    
}
