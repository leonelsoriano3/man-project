package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI;
import co.com.fspb.mgs.facade.impl.BeneficiarioServicioFacadeImpl;
import co.com.fspb.mgs.service.api.BeneficiarioServicioServiceAPI;
import co.com.fspb.mgs.test.service.data.BeneficiarioServicioServiceTestData;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.test.service.data.ServicioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link BeneficiarioServicioFacadeAPI} de la entidad {@link BeneficiarioServicio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioFacadeTest
 * @date nov 11, 2016
 */
public class BeneficiarioServicioFacadeTest {

    private static final BeneficiarioDTO BENEFICIARIODTO = BeneficiarioServiceTestData.getBeneficiarioDTO();
    private static final ServicioDTO SERVICIODTO = ServicioServiceTestData.getServicioDTO();

    @InjectMocks
    private BeneficiarioServicioFacadeAPI beneficiarioServicioFacadeAPI = new BeneficiarioServicioFacadeImpl();

    @Mock
    private BeneficiarioServicioServiceAPI beneficiarioServicioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getResultSetBeneficiarioServiciosDTO()).when(beneficiarioServicioServiceAPI).getRecords(null);
        PmzResultSet<BeneficiarioServicioDTO> beneficiarioServicios = beneficiarioServicioFacadeAPI.getRecords(null);
        Assert.assertEquals(BeneficiarioServicioServiceTestData.getTotalBeneficiarioServicios(), beneficiarioServicios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioServicioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiarioServicio_existe(){
        Mockito.doReturn(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO()).when(beneficiarioServicioServiceAPI).getBeneficiarioServicioDTO(BENEFICIARIODTO, SERVICIODTO);
        BeneficiarioServicioDTO beneficiarioServicio = beneficiarioServicioFacadeAPI.getBeneficiarioServicioDTO(BENEFICIARIODTO, SERVICIODTO);
        Assert.assertEquals(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO().getId(), beneficiarioServicio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_beneficiarioServicio_no_existe() throws PmzException{
        beneficiarioServicioFacadeAPI.guardar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_beneficiarioServicio_existe() throws PmzException{
        beneficiarioServicioFacadeAPI.editar(BeneficiarioServicioServiceTestData.getBeneficiarioServicioDTO());
    }
    
}
