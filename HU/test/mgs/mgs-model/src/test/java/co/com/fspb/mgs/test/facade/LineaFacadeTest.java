package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.facade.api.LineaFacadeAPI;
import co.com.fspb.mgs.facade.impl.LineaFacadeImpl;
import co.com.fspb.mgs.service.api.LineaServiceAPI;
import co.com.fspb.mgs.test.service.data.LineaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link LineaFacadeAPI} de la entidad {@link Linea}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaFacadeTest
 * @date nov 11, 2016
 */
public class LineaFacadeTest {

    private static final Long ID_PRIMER_LINEA = 1L;

    @InjectMocks
    private LineaFacadeAPI lineaFacadeAPI = new LineaFacadeImpl();

    @Mock
    private LineaServiceAPI lineaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LineaServiceTestData.getResultSetLineasDTO()).when(lineaServiceAPI).getRecords(null);
        PmzResultSet<LineaDTO> lineas = lineaFacadeAPI.getRecords(null);
        Assert.assertEquals(LineaServiceTestData.getTotalLineas(), lineas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LineaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_linea_existe(){
        Mockito.doReturn(LineaServiceTestData.getLineaDTO()).when(lineaServiceAPI).getLineaDTO(ID_PRIMER_LINEA);
        LineaDTO linea = lineaFacadeAPI.getLineaDTO(ID_PRIMER_LINEA);
        Assert.assertEquals(LineaServiceTestData.getLineaDTO().getId(), linea.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link LineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_linea_no_existe() throws PmzException{
        lineaFacadeAPI.guardar(LineaServiceTestData.getLineaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link LineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_linea_existe() throws PmzException{
        lineaFacadeAPI.editar(LineaServiceTestData.getLineaDTO());
    }
    
}
