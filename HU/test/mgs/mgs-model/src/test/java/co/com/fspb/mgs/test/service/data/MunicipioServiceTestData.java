package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Municipio;
import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Municipio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioServiceTestData
 * @date nov 11, 2016
 */
public abstract class MunicipioServiceTestData {
    
    private static List<Municipio> entities = new ArrayList<Municipio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Municipio entity = new Municipio();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Municipio} Mock 
     * tranformado a DTO {@link MunicipioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link MunicipioDTO}
     */
    public static MunicipioDTO getMunicipioDTO(){
        return DTOTransformer.getMunicipioDTOFromMunicipio(getMunicipio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Municipio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Municipio} Mock
     */
    public static Municipio getMunicipio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Municipio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Municipio}
     */
    public static List<Municipio> getMunicipios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<MunicipioDTO> getResultSetMunicipiosDTO(){
        List<MunicipioDTO> dtos = new ArrayList<MunicipioDTO>();
        for (Municipio entity : entities) {
            dtos.add(DTOTransformer.getMunicipioDTOFromMunicipio(entity));
        }
        return new PmzResultSet<MunicipioDTO>(dtos, getTotalMunicipios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalMunicipios(){
        return Long.valueOf(entities.size());
    }

}
