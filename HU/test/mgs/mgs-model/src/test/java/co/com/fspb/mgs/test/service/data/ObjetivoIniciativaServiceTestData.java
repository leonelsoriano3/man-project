package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.ObjetivoIniciativaPK;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativa;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ObjetivoIniciativa}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaServiceTestData
 * @date nov 11, 2016
 */
public abstract class ObjetivoIniciativaServiceTestData {
    
    private static List<ObjetivoIniciativa> entities = new ArrayList<ObjetivoIniciativa>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ObjetivoIniciativa entity = new ObjetivoIniciativa();

		    ObjetivoIniciativaPK pk = new ObjetivoIniciativaPK();
            pk.setObjetivo(ObjetivoServiceTestData.getObjetivo());
            pk.setIniciativa(IniciativaServiceTestData.getIniciativa());
            pk.setIdUsuarioResp(i);
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ObjetivoIniciativa} Mock 
     * tranformado a DTO {@link ObjetivoIniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ObjetivoIniciativaDTO}
     */
    public static ObjetivoIniciativaDTO getObjetivoIniciativaDTO(){
        return DTOTransformer.getObjetivoIniciativaDTOFromObjetivoIniciativa(getObjetivoIniciativa());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ObjetivoIniciativa} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ObjetivoIniciativa} Mock
     */
    public static ObjetivoIniciativa getObjetivoIniciativa(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ObjetivoIniciativa} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ObjetivoIniciativa}
     */
    public static List<ObjetivoIniciativa> getObjetivoIniciativas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ObjetivoIniciativaDTO> getResultSetObjetivoIniciativasDTO(){
        List<ObjetivoIniciativaDTO> dtos = new ArrayList<ObjetivoIniciativaDTO>();
        for (ObjetivoIniciativa entity : entities) {
            dtos.add(DTOTransformer.getObjetivoIniciativaDTOFromObjetivoIniciativa(entity));
        }
        return new PmzResultSet<ObjetivoIniciativaDTO>(dtos, getTotalObjetivoIniciativas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalObjetivoIniciativas(){
        return Long.valueOf(entities.size());
    }

}
