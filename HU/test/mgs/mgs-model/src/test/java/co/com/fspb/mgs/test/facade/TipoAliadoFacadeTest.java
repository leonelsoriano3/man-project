package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.facade.api.TipoAliadoFacadeAPI;
import co.com.fspb.mgs.facade.impl.TipoAliadoFacadeImpl;
import co.com.fspb.mgs.service.api.TipoAliadoServiceAPI;
import co.com.fspb.mgs.test.service.data.TipoAliadoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TipoAliadoFacadeAPI} de la entidad {@link TipoAliado}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoFacadeTest
 * @date nov 11, 2016
 */
public class TipoAliadoFacadeTest {

    private static final Long ID_PRIMER_TIPOALIADO = 1L;

    @InjectMocks
    private TipoAliadoFacadeAPI tipoAliadoFacadeAPI = new TipoAliadoFacadeImpl();

    @Mock
    private TipoAliadoServiceAPI tipoAliadoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoAliadoServiceTestData.getResultSetTipoAliadosDTO()).when(tipoAliadoServiceAPI).getRecords(null);
        PmzResultSet<TipoAliadoDTO> tipoAliados = tipoAliadoFacadeAPI.getRecords(null);
        Assert.assertEquals(TipoAliadoServiceTestData.getTotalTipoAliados(), tipoAliados.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoAliadoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoAliado_existe(){
        Mockito.doReturn(TipoAliadoServiceTestData.getTipoAliadoDTO()).when(tipoAliadoServiceAPI).getTipoAliadoDTO(ID_PRIMER_TIPOALIADO);
        TipoAliadoDTO tipoAliado = tipoAliadoFacadeAPI.getTipoAliadoDTO(ID_PRIMER_TIPOALIADO);
        Assert.assertEquals(TipoAliadoServiceTestData.getTipoAliadoDTO().getId(), tipoAliado.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoAliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_tipoAliado_no_existe() throws PmzException{
        tipoAliadoFacadeAPI.guardar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TipoAliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_tipoAliado_existe() throws PmzException{
        tipoAliadoFacadeAPI.editar(TipoAliadoServiceTestData.getTipoAliadoDTO());
    }
    
}
