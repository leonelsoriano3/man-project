package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Zona}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaServiceTestData
 * @date nov 11, 2016
 */
public abstract class ZonaServiceTestData {
    
    private static List<Zona> entities = new ArrayList<Zona>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Zona entity = new Zona();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Zona} Mock 
     * tranformado a DTO {@link ZonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ZonaDTO}
     */
    public static ZonaDTO getZonaDTO(){
        return DTOTransformer.getZonaDTOFromZona(getZona());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Zona} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Zona} Mock
     */
    public static Zona getZona(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Zona} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Zona}
     */
    public static List<Zona> getZonas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ZonaDTO> getResultSetZonasDTO(){
        List<ZonaDTO> dtos = new ArrayList<ZonaDTO>();
        for (Zona entity : entities) {
            dtos.add(DTOTransformer.getZonaDTOFromZona(entity));
        }
        return new PmzResultSet<ZonaDTO>(dtos, getTotalZonas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalZonas(){
        return Long.valueOf(entities.size());
    }

}
