package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.dao.api.TipoViviendaDaoAPI;
import co.com.fspb.mgs.service.api.TipoViviendaServiceAPI;
import co.com.fspb.mgs.service.impl.TipoViviendaServiceImpl;
import co.com.fspb.mgs.test.service.data.TipoViviendaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TipoViviendaServiceAPI} de la entidad {@link TipoVivienda}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoViviendaServiceAPI
 * @date nov 11, 2016
 */
public class TipoViviendaServiceTest {
	
    private static final Long ID_PRIMER_TIPOVIVIENDA = 1L;

    @InjectMocks
    private TipoViviendaServiceAPI tipoViviendaServiceAPI = new TipoViviendaServiceImpl();

    @Mock
    private TipoViviendaDaoAPI tipoViviendaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoViviendaServiceTestData.getTipoViviendas()).when(tipoViviendaDaoAPI).getRecords(null);
        Mockito.doReturn(TipoViviendaServiceTestData.getTotalTipoViviendas()).when(tipoViviendaDaoAPI).countRecords(null);
        PmzResultSet<TipoViviendaDTO> tipoViviendas = tipoViviendaServiceAPI.getRecords(null);
        Assert.assertEquals(TipoViviendaServiceTestData.getTotalTipoViviendas(), tipoViviendas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoViviendaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoVivienda_existe(){
        Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(ID_PRIMER_TIPOVIVIENDA);
        TipoViviendaDTO tipoVivienda = tipoViviendaServiceAPI.getTipoViviendaDTO(ID_PRIMER_TIPOVIVIENDA);
        Assert.assertEquals(TipoViviendaServiceTestData.getTipoVivienda().getId(), tipoVivienda.getId());
    }

    /**
     * Prueba unitaria de get con {@link TipoViviendaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoVivienda_no_existe(){
        Mockito.doReturn(null).when(tipoViviendaDaoAPI).get(ID_PRIMER_TIPOVIVIENDA);
        TipoViviendaDTO tipoVivienda = tipoViviendaServiceAPI.getTipoViviendaDTO(ID_PRIMER_TIPOVIVIENDA);
        Assert.assertEquals(null, tipoVivienda);
    }

    /**
     * Prueba unitaria de registro de una {@link TipoViviendaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_tipoVivienda_no_existe() throws PmzException{
    	tipoViviendaServiceAPI.guardar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_tipoVivienda_existe() throws PmzException{
        Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(ID_PRIMER_TIPOVIVIENDA);
        tipoViviendaServiceAPI.guardar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TipoViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_tipoVivienda_existe() throws PmzException{
        Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(ID_PRIMER_TIPOVIVIENDA);
        tipoViviendaServiceAPI.editar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TipoViviendaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_tipoVivienda_no_existe() throws PmzException{
        tipoViviendaServiceAPI.editar(TipoViviendaServiceTestData.getTipoViviendaDTO());
    }
    
    
}
