package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.dao.api.DescIndependienteDaoAPI;
import co.com.fspb.mgs.service.api.DescIndependienteServiceAPI;
import co.com.fspb.mgs.service.impl.DescIndependienteServiceImpl;
import co.com.fspb.mgs.test.service.data.DescIndependienteServiceTestData;

/**
 * Prueba unitaria para el servicio {@link DescIndependienteServiceAPI} de la entidad {@link DescIndependiente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DescIndependienteServiceAPI
 * @date nov 11, 2016
 */
public class DescIndependienteServiceTest {
	
    private static final Long ID_PRIMER_DESCINDEPENDIENTE = 1L;

    @InjectMocks
    private DescIndependienteServiceAPI descIndependienteServiceAPI = new DescIndependienteServiceImpl();

    @Mock
    private DescIndependienteDaoAPI descIndependienteDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependientes()).when(descIndependienteDaoAPI).getRecords(null);
        Mockito.doReturn(DescIndependienteServiceTestData.getTotalDescIndependientes()).when(descIndependienteDaoAPI).countRecords(null);
        PmzResultSet<DescIndependienteDTO> descIndependientes = descIndependienteServiceAPI.getRecords(null);
        Assert.assertEquals(DescIndependienteServiceTestData.getTotalDescIndependientes(), descIndependientes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DescIndependienteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_descIndependiente_existe(){
        Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(ID_PRIMER_DESCINDEPENDIENTE);
        DescIndependienteDTO descIndependiente = descIndependienteServiceAPI.getDescIndependienteDTO(ID_PRIMER_DESCINDEPENDIENTE);
        Assert.assertEquals(DescIndependienteServiceTestData.getDescIndependiente().getId(), descIndependiente.getId());
    }

    /**
     * Prueba unitaria de get con {@link DescIndependienteDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_descIndependiente_no_existe(){
        Mockito.doReturn(null).when(descIndependienteDaoAPI).get(ID_PRIMER_DESCINDEPENDIENTE);
        DescIndependienteDTO descIndependiente = descIndependienteServiceAPI.getDescIndependienteDTO(ID_PRIMER_DESCINDEPENDIENTE);
        Assert.assertEquals(null, descIndependiente);
    }

    /**
     * Prueba unitaria de registro de una {@link DescIndependienteDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_descIndependiente_no_existe() throws PmzException{
    	descIndependienteServiceAPI.guardar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link DescIndependienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_descIndependiente_existe() throws PmzException{
        Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(ID_PRIMER_DESCINDEPENDIENTE);
        descIndependienteServiceAPI.guardar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link DescIndependienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_descIndependiente_existe() throws PmzException{
        Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(ID_PRIMER_DESCINDEPENDIENTE);
        descIndependienteServiceAPI.editar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DescIndependienteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_descIndependiente_no_existe() throws PmzException{
        descIndependienteServiceAPI.editar(DescIndependienteServiceTestData.getDescIndependienteDTO());
    }
    
    
}
