package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI;
import co.com.fspb.mgs.facade.impl.ParametroGeneralFacadeImpl;
import co.com.fspb.mgs.service.api.ParametroGeneralServiceAPI;
import co.com.fspb.mgs.test.service.data.ParametroGeneralServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ParametroGeneralFacadeAPI} de la entidad {@link ParametroGeneral}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneralFacadeTest
 * @date nov 11, 2016
 */
public class ParametroGeneralFacadeTest {

    private static final Long ID_PRIMER_PARAMETROGENERAL = 1L;

    @InjectMocks
    private ParametroGeneralFacadeAPI parametroGeneralFacadeAPI = new ParametroGeneralFacadeImpl();

    @Mock
    private ParametroGeneralServiceAPI parametroGeneralServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParametroGeneralServiceTestData.getResultSetParametroGeneralsDTO()).when(parametroGeneralServiceAPI).getRecords(null);
        PmzResultSet<ParametroGeneralDTO> parametroGenerals = parametroGeneralFacadeAPI.getRecords(null);
        Assert.assertEquals(ParametroGeneralServiceTestData.getTotalParametroGenerals(), parametroGenerals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParametroGeneralDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parametroGeneral_existe(){
        Mockito.doReturn(ParametroGeneralServiceTestData.getParametroGeneralDTO()).when(parametroGeneralServiceAPI).getParametroGeneralDTO(ID_PRIMER_PARAMETROGENERAL);
        ParametroGeneralDTO parametroGeneral = parametroGeneralFacadeAPI.getParametroGeneralDTO(ID_PRIMER_PARAMETROGENERAL);
        Assert.assertEquals(ParametroGeneralServiceTestData.getParametroGeneralDTO().getId(), parametroGeneral.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ParametroGeneralDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_parametroGeneral_no_existe() throws PmzException{
        parametroGeneralFacadeAPI.guardar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ParametroGeneralDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_parametroGeneral_existe() throws PmzException{
        parametroGeneralFacadeAPI.editar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }
    
}
