package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.facade.api.BdNacionalFacadeAPI;
import co.com.fspb.mgs.facade.impl.BdNacionalFacadeImpl;
import co.com.fspb.mgs.service.api.BdNacionalServiceAPI;
import co.com.fspb.mgs.test.service.data.BdNacionalServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link BdNacionalFacadeAPI} de la entidad {@link BdNacional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BdNacionalFacadeTest
 * @date nov 11, 2016
 */
public class BdNacionalFacadeTest {

    private static final Long ID_PRIMER_BDNACIONAL = 1L;

    @InjectMocks
    private BdNacionalFacadeAPI bdNacionalFacadeAPI = new BdNacionalFacadeImpl();

    @Mock
    private BdNacionalServiceAPI bdNacionalServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BdNacionalServiceTestData.getResultSetBdNacionalsDTO()).when(bdNacionalServiceAPI).getRecords(null);
        PmzResultSet<BdNacionalDTO> bdNacionals = bdNacionalFacadeAPI.getRecords(null);
        Assert.assertEquals(BdNacionalServiceTestData.getTotalBdNacionals(), bdNacionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BdNacionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_bdNacional_existe(){
        Mockito.doReturn(BdNacionalServiceTestData.getBdNacionalDTO()).when(bdNacionalServiceAPI).getBdNacionalDTO(ID_PRIMER_BDNACIONAL);
        BdNacionalDTO bdNacional = bdNacionalFacadeAPI.getBdNacionalDTO(ID_PRIMER_BDNACIONAL);
        Assert.assertEquals(BdNacionalServiceTestData.getBdNacionalDTO().getId(), bdNacional.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link BdNacionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_bdNacional_no_existe() throws PmzException{
        bdNacionalFacadeAPI.guardar(BdNacionalServiceTestData.getBdNacionalDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link BdNacionalDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_bdNacional_existe() throws PmzException{
        bdNacionalFacadeAPI.editar(BdNacionalServiceTestData.getBdNacionalDTO());
    }
    
}
