package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Aliado;
import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Aliado}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoServiceTestData
 * @date nov 11, 2016
 */
public abstract class AliadoServiceTestData {
    
    private static List<Aliado> entities = new ArrayList<Aliado>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Aliado entity = new Aliado();

            entity.setId(new Long(i));
            
            entity.setTipoAliado(TipoAliadoServiceTestData.getTipoAliado());
            entity.setFechaModifica(new Date());
            entity.setEntidad(EntidadServiceTestData.getEntidad());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setProyecto(ProyectoServiceTestData.getProyecto());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Aliado} Mock 
     * tranformado a DTO {@link AliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link AliadoDTO}
     */
    public static AliadoDTO getAliadoDTO(){
        return DTOTransformer.getAliadoDTOFromAliado(getAliado());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Aliado} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Aliado} Mock
     */
    public static Aliado getAliado(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Aliado} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Aliado}
     */
    public static List<Aliado> getAliados(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<AliadoDTO> getResultSetAliadosDTO(){
        List<AliadoDTO> dtos = new ArrayList<AliadoDTO>();
        for (Aliado entity : entities) {
            dtos.add(DTOTransformer.getAliadoDTOFromAliado(entity));
        }
        return new PmzResultSet<AliadoDTO>(dtos, getTotalAliados(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalAliados(){
        return Long.valueOf(entities.size());
    }

}
