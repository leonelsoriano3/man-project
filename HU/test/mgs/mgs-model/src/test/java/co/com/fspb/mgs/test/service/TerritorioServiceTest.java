package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.service.api.TerritorioServiceAPI;
import co.com.fspb.mgs.service.impl.TerritorioServiceImpl;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
        import co.com.fspb.mgs.dao.api.EstadoViaDaoAPI;
import co.com.fspb.mgs.test.service.data.EstadoViaServiceTestData;
import co.com.fspb.mgs.dao.api.TipoTerritorioDaoAPI;
import co.com.fspb.mgs.test.service.data.TipoTerritorioServiceTestData;
import co.com.fspb.mgs.dao.api.ProblemaComunidadDaoAPI;
import co.com.fspb.mgs.test.service.data.ProblemaComunidadServiceTestData;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.test.service.data.ZonaServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TerritorioServiceAPI} de la entidad {@link Territorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TerritorioServiceAPI
 * @date nov 11, 2016
 */
public class TerritorioServiceTest {
	
    private static final Long ID_PRIMER_TERRITORIO = 1L;

    @InjectMocks
    private TerritorioServiceAPI territorioServiceAPI = new TerritorioServiceImpl();

    @Mock
    private TerritorioDaoAPI territorioDaoAPI;
    @Mock
    private EstadoViaDaoAPI estadoViaDaoAPI;
    @Mock
    private TipoTerritorioDaoAPI tipoTerritorioDaoAPI;
    @Mock
    private ProblemaComunidadDaoAPI problemaComunidadDaoAPI;
    @Mock
    private ZonaDaoAPI zonaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TerritorioServiceTestData.getTerritorios()).when(territorioDaoAPI).getRecords(null);
        Mockito.doReturn(TerritorioServiceTestData.getTotalTerritorios()).when(territorioDaoAPI).countRecords(null);
        PmzResultSet<TerritorioDTO> territorios = territorioServiceAPI.getRecords(null);
        Assert.assertEquals(TerritorioServiceTestData.getTotalTerritorios(), territorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_territorio_existe(){
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
        TerritorioDTO territorio = territorioServiceAPI.getTerritorioDTO(ID_PRIMER_TERRITORIO);
        Assert.assertEquals(TerritorioServiceTestData.getTerritorio().getId(), territorio.getId());
    }

    /**
     * Prueba unitaria de get con {@link TerritorioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_territorio_no_existe(){
        Mockito.doReturn(null).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
        TerritorioDTO territorio = territorioServiceAPI.getTerritorioDTO(ID_PRIMER_TERRITORIO);
        Assert.assertEquals(null, territorio);
    }

    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_territorio_no_existe() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_territorio_existe() throws PmzException{
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
        territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con estadoVia
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_territorio_no_existe_no_estadoVia() throws PmzException{
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con tipoTerritorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_territorio_no_existe_no_tipoTerritorio() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con problemaComunidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_territorio_no_existe_no_problemaComunidad() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_territorio_no_existe_no_zona() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	territorioServiceAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_territorio_existe() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
        territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_territorio_no_existe() throws PmzException{
        territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con estadoVia
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_territorio_existe_no_estadoVia() throws PmzException{
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
    	territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con tipoTerritorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_territorio_existe_no_tipoTerritorio() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
    	territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con problemaComunidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_territorio_existe_no_problemaComunidad() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
    	territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_territorio_existe_no_zona() throws PmzException{
    	Mockito.doReturn(EstadoViaServiceTestData.getEstadoVia()).when(estadoViaDaoAPI).get(EstadoViaServiceTestData.getEstadoVia().getId());
    	Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorio()).when(tipoTerritorioDaoAPI).get(TipoTerritorioServiceTestData.getTipoTerritorio().getId());
    	Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ProblemaComunidadServiceTestData.getProblemaComunidad().getId());
        Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(ID_PRIMER_TERRITORIO);
    	territorioServiceAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
    
}
