package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Servicio;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Servicio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioServiceTestData
 * @date nov 11, 2016
 */
public abstract class ServicioServiceTestData {
    
    private static List<Servicio> entities = new ArrayList<Servicio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Servicio entity = new Servicio();

            entity.setId(new Long(i));
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setTipoServicio("TipoServicio" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Servicio} Mock 
     * tranformado a DTO {@link ServicioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ServicioDTO}
     */
    public static ServicioDTO getServicioDTO(){
        return DTOTransformer.getServicioDTOFromServicio(getServicio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Servicio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Servicio} Mock
     */
    public static Servicio getServicio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Servicio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Servicio}
     */
    public static List<Servicio> getServicios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ServicioDTO> getResultSetServiciosDTO(){
        List<ServicioDTO> dtos = new ArrayList<ServicioDTO>();
        for (Servicio entity : entities) {
            dtos.add(DTOTransformer.getServicioDTOFromServicio(entity));
        }
        return new PmzResultSet<ServicioDTO>(dtos, getTotalServicios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalServicios(){
        return Long.valueOf(entities.size());
    }

}
