package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.dao.api.NivelEducativoDaoAPI;
import co.com.fspb.mgs.service.api.NivelEducativoServiceAPI;
import co.com.fspb.mgs.service.impl.NivelEducativoServiceImpl;
import co.com.fspb.mgs.test.service.data.NivelEducativoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link NivelEducativoServiceAPI} de la entidad {@link NivelEducativo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoServiceAPI
 * @date nov 11, 2016
 */
public class NivelEducativoServiceTest {
	
    private static final Long ID_PRIMER_NIVELEDUCATIVO = 1L;

    @InjectMocks
    private NivelEducativoServiceAPI nivelEducativoServiceAPI = new NivelEducativoServiceImpl();

    @Mock
    private NivelEducativoDaoAPI nivelEducativoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativos()).when(nivelEducativoDaoAPI).getRecords(null);
        Mockito.doReturn(NivelEducativoServiceTestData.getTotalNivelEducativos()).when(nivelEducativoDaoAPI).countRecords(null);
        PmzResultSet<NivelEducativoDTO> nivelEducativos = nivelEducativoServiceAPI.getRecords(null);
        Assert.assertEquals(NivelEducativoServiceTestData.getTotalNivelEducativos(), nivelEducativos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link NivelEducativoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_nivelEducativo_existe(){
        Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(ID_PRIMER_NIVELEDUCATIVO);
        NivelEducativoDTO nivelEducativo = nivelEducativoServiceAPI.getNivelEducativoDTO(ID_PRIMER_NIVELEDUCATIVO);
        Assert.assertEquals(NivelEducativoServiceTestData.getNivelEducativo().getId(), nivelEducativo.getId());
    }

    /**
     * Prueba unitaria de get con {@link NivelEducativoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_nivelEducativo_no_existe(){
        Mockito.doReturn(null).when(nivelEducativoDaoAPI).get(ID_PRIMER_NIVELEDUCATIVO);
        NivelEducativoDTO nivelEducativo = nivelEducativoServiceAPI.getNivelEducativoDTO(ID_PRIMER_NIVELEDUCATIVO);
        Assert.assertEquals(null, nivelEducativo);
    }

    /**
     * Prueba unitaria de registro de una {@link NivelEducativoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_nivelEducativo_no_existe() throws PmzException{
    	nivelEducativoServiceAPI.guardar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link NivelEducativoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_nivelEducativo_existe() throws PmzException{
        Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(ID_PRIMER_NIVELEDUCATIVO);
        nivelEducativoServiceAPI.guardar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link NivelEducativoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_nivelEducativo_existe() throws PmzException{
        Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(ID_PRIMER_NIVELEDUCATIVO);
        nivelEducativoServiceAPI.editar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link NivelEducativoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_nivelEducativo_no_existe() throws PmzException{
        nivelEducativoServiceAPI.editar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }
    
    
}
