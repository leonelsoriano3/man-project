package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.TipoEntidadEnum;
import co.com.fspb.mgs.enums.TipoIdEntidadEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Entidad}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadServiceTestData
 * @date nov 11, 2016
 */
public abstract class EntidadServiceTestData {
    
    private static List<Entidad> entities = new ArrayList<Entidad>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Entidad entity = new Entidad();

            entity.setId(new Long(i));
            
            entity.setTipoId(TipoIdEntidadEnum.NIT);
            entity.setFechaModifica(new Date());
            entity.setContacto(PersonaServiceTestData.getPersona());
            entity.setContacto(PersonaServiceTestData.getPersona());
            entity.setFechaConstitucion(new Date());
            entity.setInfoConvocatorias("InfoConvocatorias" + i);
            entity.setNombre("Nombre" + i);
            entity.setNumId("NumId" + i);
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setCertificaciones("Certificaciones" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entity.setTipoEntidad(TipoEntidadEnum.ONG);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Entidad} Mock 
     * tranformado a DTO {@link EntidadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link EntidadDTO}
     */
    public static EntidadDTO getEntidadDTO(){
        return DTOTransformer.getEntidadDTOFromEntidad(getEntidad());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Entidad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Entidad} Mock
     */
    public static Entidad getEntidad(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Entidad} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Entidad}
     */
    public static List<Entidad> getEntidads(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<EntidadDTO> getResultSetEntidadsDTO(){
        List<EntidadDTO> dtos = new ArrayList<EntidadDTO>();
        for (Entidad entity : entities) {
            dtos.add(DTOTransformer.getEntidadDTOFromEntidad(entity));
        }
        return new PmzResultSet<EntidadDTO>(dtos, getTotalEntidads(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalEntidads(){
        return Long.valueOf(entities.size());
    }

}
