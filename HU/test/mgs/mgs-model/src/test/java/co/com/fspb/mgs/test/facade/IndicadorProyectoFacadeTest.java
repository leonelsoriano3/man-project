package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI;
import co.com.fspb.mgs.facade.impl.IndicadorProyectoFacadeImpl;
import co.com.fspb.mgs.service.api.IndicadorProyectoServiceAPI;
import co.com.fspb.mgs.test.service.data.IndicadorProyectoServiceTestData;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link IndicadorProyectoFacadeAPI} de la entidad {@link IndicadorProyecto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoFacadeTest
 * @date nov 11, 2016
 */
public class IndicadorProyectoFacadeTest {

    private static final IndicadorDTO INDICADORDTO = IndicadorServiceTestData.getIndicadorDTO();
    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();

    @InjectMocks
    private IndicadorProyectoFacadeAPI indicadorProyectoFacadeAPI = new IndicadorProyectoFacadeImpl();

    @Mock
    private IndicadorProyectoServiceAPI indicadorProyectoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorProyectoServiceTestData.getResultSetIndicadorProyectosDTO()).when(indicadorProyectoServiceAPI).getRecords(null);
        PmzResultSet<IndicadorProyectoDTO> indicadorProyectos = indicadorProyectoFacadeAPI.getRecords(null);
        Assert.assertEquals(IndicadorProyectoServiceTestData.getTotalIndicadorProyectos(), indicadorProyectos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorProyectoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorProyecto_existe(){
        Mockito.doReturn(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO()).when(indicadorProyectoServiceAPI).getIndicadorProyectoDTO(INDICADORDTO, PROYECTODTO);
        IndicadorProyectoDTO indicadorProyecto = indicadorProyectoFacadeAPI.getIndicadorProyectoDTO(INDICADORDTO, PROYECTODTO);
        Assert.assertEquals(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO().getId(), indicadorProyecto.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_indicadorProyecto_no_existe() throws PmzException{
        indicadorProyectoFacadeAPI.guardar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link IndicadorProyectoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_indicadorProyecto_existe() throws PmzException{
        indicadorProyectoFacadeAPI.editar(IndicadorProyectoServiceTestData.getIndicadorProyectoDTO());
    }
    
}
