package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.dao.api.AfiliadoSaludDaoAPI;
import co.com.fspb.mgs.service.api.AfiliadoSaludServiceAPI;
import co.com.fspb.mgs.service.impl.AfiliadoSaludServiceImpl;
import co.com.fspb.mgs.test.service.data.AfiliadoSaludServiceTestData;

/**
 * Prueba unitaria para el servicio {@link AfiliadoSaludServiceAPI} de la entidad {@link AfiliadoSalud}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludServiceAPI
 * @date nov 11, 2016
 */
public class AfiliadoSaludServiceTest {
	
    private static final Long ID_PRIMER_AFILIADOSALUD = 1L;

    @InjectMocks
    private AfiliadoSaludServiceAPI afiliadoSaludServiceAPI = new AfiliadoSaludServiceImpl();

    @Mock
    private AfiliadoSaludDaoAPI afiliadoSaludDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSaluds()).when(afiliadoSaludDaoAPI).getRecords(null);
        Mockito.doReturn(AfiliadoSaludServiceTestData.getTotalAfiliadoSaluds()).when(afiliadoSaludDaoAPI).countRecords(null);
        PmzResultSet<AfiliadoSaludDTO> afiliadoSaluds = afiliadoSaludServiceAPI.getRecords(null);
        Assert.assertEquals(AfiliadoSaludServiceTestData.getTotalAfiliadoSaluds(), afiliadoSaluds.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AfiliadoSaludDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_afiliadoSalud_existe(){
        Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(ID_PRIMER_AFILIADOSALUD);
        AfiliadoSaludDTO afiliadoSalud = afiliadoSaludServiceAPI.getAfiliadoSaludDTO(ID_PRIMER_AFILIADOSALUD);
        Assert.assertEquals(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId(), afiliadoSalud.getId());
    }

    /**
     * Prueba unitaria de get con {@link AfiliadoSaludDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_afiliadoSalud_no_existe(){
        Mockito.doReturn(null).when(afiliadoSaludDaoAPI).get(ID_PRIMER_AFILIADOSALUD);
        AfiliadoSaludDTO afiliadoSalud = afiliadoSaludServiceAPI.getAfiliadoSaludDTO(ID_PRIMER_AFILIADOSALUD);
        Assert.assertEquals(null, afiliadoSalud);
    }

    /**
     * Prueba unitaria de registro de una {@link AfiliadoSaludDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_afiliadoSalud_no_existe() throws PmzException{
    	afiliadoSaludServiceAPI.guardar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link AfiliadoSaludDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_afiliadoSalud_existe() throws PmzException{
        Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(ID_PRIMER_AFILIADOSALUD);
        afiliadoSaludServiceAPI.guardar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link AfiliadoSaludDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_afiliadoSalud_existe() throws PmzException{
        Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(ID_PRIMER_AFILIADOSALUD);
        afiliadoSaludServiceAPI.editar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AfiliadoSaludDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_afiliadoSalud_no_existe() throws PmzException{
        afiliadoSaludServiceAPI.editar(AfiliadoSaludServiceTestData.getAfiliadoSaludDTO());
    }
    
    
}
