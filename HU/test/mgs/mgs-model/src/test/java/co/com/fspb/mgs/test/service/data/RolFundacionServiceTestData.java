package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.RolFundacion;
import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link RolFundacion}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFundacionServiceTestData
 * @date nov 11, 2016
 */
public abstract class RolFundacionServiceTestData {
    
    private static List<RolFundacion> entities = new ArrayList<RolFundacion>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            RolFundacion entity = new RolFundacion();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link RolFundacion} Mock 
     * tranformado a DTO {@link RolFundacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link RolFundacionDTO}
     */
    public static RolFundacionDTO getRolFundacionDTO(){
        return DTOTransformer.getRolFundacionDTOFromRolFundacion(getRolFundacion());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link RolFundacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link RolFundacion} Mock
     */
    public static RolFundacion getRolFundacion(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link RolFundacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link RolFundacion}
     */
    public static List<RolFundacion> getRolFundacions(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<RolFundacionDTO> getResultSetRolFundacionsDTO(){
        List<RolFundacionDTO> dtos = new ArrayList<RolFundacionDTO>();
        for (RolFundacion entity : entities) {
            dtos.add(DTOTransformer.getRolFundacionDTOFromRolFundacion(entity));
        }
        return new PmzResultSet<RolFundacionDTO>(dtos, getTotalRolFundacions(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalRolFundacions(){
        return Long.valueOf(entities.size());
    }

}
