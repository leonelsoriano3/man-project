package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Iniciativa}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaServiceTestData
 * @date nov 11, 2016
 */
public abstract class IniciativaServiceTestData {
    
    private static List<Iniciativa> entities = new ArrayList<Iniciativa>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Iniciativa entity = new Iniciativa();

            entity.setId(new Long(i));
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setDescripcion("Descripcion" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setUsuario(UsuarioServiceTestData.getUsuario());
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Iniciativa} Mock 
     * tranformado a DTO {@link IniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link IniciativaDTO}
     */
    public static IniciativaDTO getIniciativaDTO(){
        return DTOTransformer.getIniciativaDTOFromIniciativa(getIniciativa());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Iniciativa} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Iniciativa} Mock
     */
    public static Iniciativa getIniciativa(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Iniciativa} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Iniciativa}
     */
    public static List<Iniciativa> getIniciativas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<IniciativaDTO> getResultSetIniciativasDTO(){
        List<IniciativaDTO> dtos = new ArrayList<IniciativaDTO>();
        for (Iniciativa entity : entities) {
            dtos.add(DTOTransformer.getIniciativaDTOFromIniciativa(entity));
        }
        return new PmzResultSet<IniciativaDTO>(dtos, getTotalIniciativas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalIniciativas(){
        return Long.valueOf(entities.size());
    }

}
