package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.facade.api.MunicipioFacadeAPI;
import co.com.fspb.mgs.facade.impl.MunicipioFacadeImpl;
import co.com.fspb.mgs.service.api.MunicipioServiceAPI;
import co.com.fspb.mgs.test.service.data.MunicipioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link MunicipioFacadeAPI} de la entidad {@link Municipio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioFacadeTest
 * @date nov 11, 2016
 */
public class MunicipioFacadeTest {

    private static final Long ID_PRIMER_MUNICIPIO = 1L;

    @InjectMocks
    private MunicipioFacadeAPI municipioFacadeAPI = new MunicipioFacadeImpl();

    @Mock
    private MunicipioServiceAPI municipioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(MunicipioServiceTestData.getResultSetMunicipiosDTO()).when(municipioServiceAPI).getRecords(null);
        PmzResultSet<MunicipioDTO> municipios = municipioFacadeAPI.getRecords(null);
        Assert.assertEquals(MunicipioServiceTestData.getTotalMunicipios(), municipios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link MunicipioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_municipio_existe(){
        Mockito.doReturn(MunicipioServiceTestData.getMunicipioDTO()).when(municipioServiceAPI).getMunicipioDTO(ID_PRIMER_MUNICIPIO);
        MunicipioDTO municipio = municipioFacadeAPI.getMunicipioDTO(ID_PRIMER_MUNICIPIO);
        Assert.assertEquals(MunicipioServiceTestData.getMunicipioDTO().getId(), municipio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link MunicipioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_municipio_no_existe() throws PmzException{
        municipioFacadeAPI.guardar(MunicipioServiceTestData.getMunicipioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link MunicipioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_municipio_existe() throws PmzException{
        municipioFacadeAPI.editar(MunicipioServiceTestData.getMunicipioDTO());
    }
    
}
