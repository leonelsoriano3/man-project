package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.LiderTerritorioPK;
import co.com.fspb.mgs.dao.model.LiderTerritorio;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link LiderTerritorio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioServiceTestData
 * @date nov 11, 2016
 */
public abstract class LiderTerritorioServiceTestData {
    
    private static List<LiderTerritorio> entities = new ArrayList<LiderTerritorio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            LiderTerritorio entity = new LiderTerritorio();

		    LiderTerritorioPK pk = new LiderTerritorioPK();
            pk.setTerritorio(TerritorioServiceTestData.getTerritorio());
            pk.setLider(LiderServiceTestData.getLider());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link LiderTerritorio} Mock 
     * tranformado a DTO {@link LiderTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link LiderTerritorioDTO}
     */
    public static LiderTerritorioDTO getLiderTerritorioDTO(){
        return DTOTransformer.getLiderTerritorioDTOFromLiderTerritorio(getLiderTerritorio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link LiderTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link LiderTerritorio} Mock
     */
    public static LiderTerritorio getLiderTerritorio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link LiderTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link LiderTerritorio}
     */
    public static List<LiderTerritorio> getLiderTerritorios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<LiderTerritorioDTO> getResultSetLiderTerritoriosDTO(){
        List<LiderTerritorioDTO> dtos = new ArrayList<LiderTerritorioDTO>();
        for (LiderTerritorio entity : entities) {
            dtos.add(DTOTransformer.getLiderTerritorioDTOFromLiderTerritorio(entity));
        }
        return new PmzResultSet<LiderTerritorioDTO>(dtos, getTotalLiderTerritorios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalLiderTerritorios(){
        return Long.valueOf(entities.size());
    }

}
