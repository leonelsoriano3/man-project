package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.RolUsuarioPK;
import co.com.fspb.mgs.dao.model.RolUsuario;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link RolUsuario}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioServiceTestData
 * @date nov 11, 2016
 */
public abstract class RolUsuarioServiceTestData {
    
    private static List<RolUsuario> entities = new ArrayList<RolUsuario>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            RolUsuario entity = new RolUsuario();

		    RolUsuarioPK pk = new RolUsuarioPK();
            pk.setRol(RolServiceTestData.getRol());
            pk.setUsuario(UsuarioServiceTestData.getUsuario());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link RolUsuario} Mock 
     * tranformado a DTO {@link RolUsuarioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link RolUsuarioDTO}
     */
    public static RolUsuarioDTO getRolUsuarioDTO(){
        return DTOTransformer.getRolUsuarioDTOFromRolUsuario(getRolUsuario());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link RolUsuario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link RolUsuario} Mock
     */
    public static RolUsuario getRolUsuario(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link RolUsuario} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link RolUsuario}
     */
    public static List<RolUsuario> getRolUsuarios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<RolUsuarioDTO> getResultSetRolUsuariosDTO(){
        List<RolUsuarioDTO> dtos = new ArrayList<RolUsuarioDTO>();
        for (RolUsuario entity : entities) {
            dtos.add(DTOTransformer.getRolUsuarioDTOFromRolUsuario(entity));
        }
        return new PmzResultSet<RolUsuarioDTO>(dtos, getTotalRolUsuarios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalRolUsuarios(){
        return Long.valueOf(entities.size());
    }

}
