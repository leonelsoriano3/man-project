package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.facade.api.ParienteFacadeAPI;
import co.com.fspb.mgs.facade.impl.ParienteFacadeImpl;
import co.com.fspb.mgs.service.api.ParienteServiceAPI;
import co.com.fspb.mgs.test.service.data.ParienteServiceTestData;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ParienteFacadeAPI} de la entidad {@link Pariente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParienteFacadeTest
 * @date nov 11, 2016
 */
public class ParienteFacadeTest {

    private static final BeneficiarioDTO BENEFICIARIODTO = BeneficiarioServiceTestData.getBeneficiarioDTO();
    private static final PersonaDTO PERSONADTO = PersonaServiceTestData.getPersonaDTO();

    @InjectMocks
    private ParienteFacadeAPI parienteFacadeAPI = new ParienteFacadeImpl();

    @Mock
    private ParienteServiceAPI parienteServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParienteServiceTestData.getResultSetParientesDTO()).when(parienteServiceAPI).getRecords(null);
        PmzResultSet<ParienteDTO> parientes = parienteFacadeAPI.getRecords(null);
        Assert.assertEquals(ParienteServiceTestData.getTotalParientes(), parientes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParienteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_pariente_existe(){
        Mockito.doReturn(ParienteServiceTestData.getParienteDTO()).when(parienteServiceAPI).getParienteDTO(BENEFICIARIODTO, PERSONADTO);
        ParienteDTO pariente = parienteFacadeAPI.getParienteDTO(BENEFICIARIODTO, PERSONADTO);
        Assert.assertEquals(ParienteServiceTestData.getParienteDTO().getId(), pariente.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ParienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_pariente_no_existe() throws PmzException{
        parienteFacadeAPI.guardar(ParienteServiceTestData.getParienteDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ParienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_pariente_existe() throws PmzException{
        parienteFacadeAPI.editar(ParienteServiceTestData.getParienteDTO());
    }
    
}
