package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Territorio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TerritorioServiceTestData
 * @date nov 11, 2016
 */
public abstract class TerritorioServiceTestData {
    
    private static List<Territorio> entities = new ArrayList<Territorio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Territorio entity = new Territorio();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setJovenesHombres(i);
            entity.setJovenesMujeres(i);
            entity.setMenoresMujeres(i);
            entity.setEstadoVia(EstadoViaServiceTestData.getEstadoVia());
            entity.setNombre("Nombre" + i);
            entity.setAdultosMayoresHombres(i);
            entity.setTipoTerritorio(TipoTerritorioServiceTestData.getTipoTerritorio());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setProblemaComunidad(ProblemaComunidadServiceTestData.getProblemaComunidad());
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setAdultosMayoresMujeres(i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setMenoresHombres(i);
            entity.setZona(ZonaServiceTestData.getZona());
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Territorio} Mock 
     * tranformado a DTO {@link TerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TerritorioDTO}
     */
    public static TerritorioDTO getTerritorioDTO(){
        return DTOTransformer.getTerritorioDTOFromTerritorio(getTerritorio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Territorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Territorio} Mock
     */
    public static Territorio getTerritorio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Territorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Territorio}
     */
    public static List<Territorio> getTerritorios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TerritorioDTO> getResultSetTerritoriosDTO(){
        List<TerritorioDTO> dtos = new ArrayList<TerritorioDTO>();
        for (Territorio entity : entities) {
            dtos.add(DTOTransformer.getTerritorioDTOFromTerritorio(entity));
        }
        return new PmzResultSet<TerritorioDTO>(dtos, getTotalTerritorios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTerritorios(){
        return Long.valueOf(entities.size());
    }

}
