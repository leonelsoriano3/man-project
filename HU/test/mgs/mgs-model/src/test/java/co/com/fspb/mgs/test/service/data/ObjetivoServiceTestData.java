package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Objetivo}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoServiceTestData
 * @date nov 11, 2016
 */
public abstract class ObjetivoServiceTestData {
    
    private static List<Objetivo> entities = new ArrayList<Objetivo>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Objetivo entity = new Objetivo();

            entity.setId(new Long(i));
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setDescripcion("Descripcion" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setUsuario(UsuarioServiceTestData.getUsuario());
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Objetivo} Mock 
     * tranformado a DTO {@link ObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ObjetivoDTO}
     */
    public static ObjetivoDTO getObjetivoDTO(){
        return DTOTransformer.getObjetivoDTOFromObjetivo(getObjetivo());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Objetivo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Objetivo} Mock
     */
    public static Objetivo getObjetivo(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Objetivo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Objetivo}
     */
    public static List<Objetivo> getObjetivos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ObjetivoDTO> getResultSetObjetivosDTO(){
        List<ObjetivoDTO> dtos = new ArrayList<ObjetivoDTO>();
        for (Objetivo entity : entities) {
            dtos.add(DTOTransformer.getObjetivoDTOFromObjetivo(entity));
        }
        return new PmzResultSet<ObjetivoDTO>(dtos, getTotalObjetivos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalObjetivos(){
        return Long.valueOf(entities.size());
    }

}
