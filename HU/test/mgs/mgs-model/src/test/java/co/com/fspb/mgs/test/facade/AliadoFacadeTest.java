package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.facade.api.AliadoFacadeAPI;
import co.com.fspb.mgs.facade.impl.AliadoFacadeImpl;
import co.com.fspb.mgs.service.api.AliadoServiceAPI;
import co.com.fspb.mgs.test.service.data.AliadoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link AliadoFacadeAPI} de la entidad {@link Aliado}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoFacadeTest
 * @date nov 11, 2016
 */
public class AliadoFacadeTest {

    private static final Long ID_PRIMER_ALIADO = 1L;

    @InjectMocks
    private AliadoFacadeAPI aliadoFacadeAPI = new AliadoFacadeImpl();

    @Mock
    private AliadoServiceAPI aliadoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AliadoServiceTestData.getResultSetAliadosDTO()).when(aliadoServiceAPI).getRecords(null);
        PmzResultSet<AliadoDTO> aliados = aliadoFacadeAPI.getRecords(null);
        Assert.assertEquals(AliadoServiceTestData.getTotalAliados(), aliados.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AliadoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_aliado_existe(){
        Mockito.doReturn(AliadoServiceTestData.getAliadoDTO()).when(aliadoServiceAPI).getAliadoDTO(ID_PRIMER_ALIADO);
        AliadoDTO aliado = aliadoFacadeAPI.getAliadoDTO(ID_PRIMER_ALIADO);
        Assert.assertEquals(AliadoServiceTestData.getAliadoDTO().getId(), aliado.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link AliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_aliado_no_existe() throws PmzException{
        aliadoFacadeAPI.guardar(AliadoServiceTestData.getAliadoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link AliadoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_aliado_existe() throws PmzException{
        aliadoFacadeAPI.editar(AliadoServiceTestData.getAliadoDTO());
    }
    
}
