package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.dao.api.LiderTerritorioDaoAPI;
import co.com.fspb.mgs.service.api.LiderTerritorioServiceAPI;
import co.com.fspb.mgs.service.impl.LiderTerritorioServiceImpl;
import co.com.fspb.mgs.test.service.data.LiderTerritorioServiceTestData;
import co.com.fspb.mgs.dao.model.LiderTerritorioPK;
  import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.dao.api.LiderDaoAPI;
import co.com.fspb.mgs.test.service.data.LiderServiceTestData;

/**
 * Prueba unitaria para el servicio {@link LiderTerritorioServiceAPI} de la entidad {@link LiderTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioServiceAPI
 * @date nov 11, 2016
 */
public class LiderTerritorioServiceTest {
	
    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final LiderDTO LIDERDTO = LiderServiceTestData.getLiderDTO();

    @InjectMocks
    private LiderTerritorioServiceAPI liderTerritorioServiceAPI = new LiderTerritorioServiceImpl();

    @Mock
    private LiderTerritorioDaoAPI liderTerritorioDaoAPI;
    @Mock
    private TerritorioDaoAPI territorioDaoAPI;
    @Mock
    private LiderDaoAPI liderDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorios()).when(liderTerritorioDaoAPI).getRecords(null);
        Mockito.doReturn(LiderTerritorioServiceTestData.getTotalLiderTerritorios()).when(liderTerritorioDaoAPI).countRecords(null);
        PmzResultSet<LiderTerritorioDTO> liderTerritorios = liderTerritorioServiceAPI.getRecords(null);
        Assert.assertEquals(LiderTerritorioServiceTestData.getTotalLiderTerritorios(), liderTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LiderTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_liderTerritorio_existe(){
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorio()).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
        LiderTerritorioDTO liderTerritorio = liderTerritorioServiceAPI.getLiderTerritorioDTO(TERRITORIODTO, LIDERDTO);
        Assert.assertEquals(LiderTerritorioServiceTestData.getLiderTerritorioDTO().getId(), liderTerritorio.getId());
    }

    /**
     * Prueba unitaria de get con {@link LiderTerritorioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_liderTerritorio_no_existe(){
        Mockito.doReturn(null).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
        LiderTerritorioDTO liderTerritorio = liderTerritorioServiceAPI.getLiderTerritorioDTO(TERRITORIODTO, LIDERDTO);
        Assert.assertEquals(null, liderTerritorio);
    }

    /**
     * Prueba unitaria de registro de una {@link LiderTerritorioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_liderTerritorio_no_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(LiderServiceTestData.getLider().getId());
    	liderTerritorioServiceAPI.guardar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link LiderTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_liderTerritorio_existe() throws PmzException{
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorio()).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
        liderTerritorioServiceAPI.guardar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link LiderTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_liderTerritorio_no_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(LiderServiceTestData.getLider().getId());
    	liderTerritorioServiceAPI.guardar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link LiderTerritorioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con lider
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_liderTerritorio_no_existe_no_lider() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	liderTerritorioServiceAPI.guardar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link LiderTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_liderTerritorio_existe() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
    	Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(LiderServiceTestData.getLider().getId());
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorio()).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
        liderTerritorioServiceAPI.editar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LiderTerritorioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_liderTerritorio_no_existe() throws PmzException{
        liderTerritorioServiceAPI.editar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LiderTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con territorio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_liderTerritorio_existe_no_territorio() throws PmzException{
    	Mockito.doReturn(LiderServiceTestData.getLider()).when(liderDaoAPI).get(LiderServiceTestData.getLider().getId());
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorio()).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
    	liderTerritorioServiceAPI.editar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LiderTerritorioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con lider
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_liderTerritorio_existe_no_lider() throws PmzException{
    	Mockito.doReturn(TerritorioServiceTestData.getTerritorio()).when(territorioDaoAPI).get(TerritorioServiceTestData.getTerritorio().getId());
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorio()).when(liderTerritorioDaoAPI).get(Mockito.any(LiderTerritorioPK.class));
    	liderTerritorioServiceAPI.editar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
    
}
