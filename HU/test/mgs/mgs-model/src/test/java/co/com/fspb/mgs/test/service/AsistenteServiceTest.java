package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.AsistenteDTO;
import co.com.fspb.mgs.dao.api.AsistenteDaoAPI;
import co.com.fspb.mgs.service.api.AsistenteServiceAPI;
import co.com.fspb.mgs.service.impl.AsistenteServiceImpl;
import co.com.fspb.mgs.test.service.data.AsistenteServiceTestData;
    import co.com.fspb.mgs.dao.api.ProyectoBeneficiarioDaoAPI;
import co.com.fspb.mgs.test.service.data.ProyectoBeneficiarioServiceTestData;
import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.test.service.data.ActividadServiceTestData;

/**
 * Prueba unitaria para el servicio {@link AsistenteServiceAPI} de la entidad {@link Asistente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AsistenteServiceAPI
 * @date nov 11, 2016
 */
public class AsistenteServiceTest {
	
    private static final Long ID_PRIMER_ASISTENTE = 1L;

    @InjectMocks
    private AsistenteServiceAPI asistenteServiceAPI = new AsistenteServiceImpl();

    @Mock
    private AsistenteDaoAPI asistenteDaoAPI;
    @Mock
    private ProyectoBeneficiarioDaoAPI proyectoBeneficiarioDaoAPI;
    @Mock
    private ActividadDaoAPI actividadDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AsistenteServiceTestData.getAsistentes()).when(asistenteDaoAPI).getRecords(null);
        Mockito.doReturn(AsistenteServiceTestData.getTotalAsistentes()).when(asistenteDaoAPI).countRecords(null);
        PmzResultSet<AsistenteDTO> asistentes = asistenteServiceAPI.getRecords(null);
        Assert.assertEquals(AsistenteServiceTestData.getTotalAsistentes(), asistentes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AsistenteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_asistente_existe(){
        Mockito.doReturn(AsistenteServiceTestData.getAsistente()).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
        AsistenteDTO asistente = asistenteServiceAPI.getAsistenteDTO(ID_PRIMER_ASISTENTE);
        Assert.assertEquals(AsistenteServiceTestData.getAsistente().getId(), asistente.getId());
    }

    /**
     * Prueba unitaria de get con {@link AsistenteDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_asistente_no_existe(){
        Mockito.doReturn(null).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
        AsistenteDTO asistente = asistenteServiceAPI.getAsistenteDTO(ID_PRIMER_ASISTENTE);
        Assert.assertEquals(null, asistente);
    }

    /**
     * Prueba unitaria de registro de una {@link AsistenteDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_asistente_no_existe() throws PmzException{
    	Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario().getId());
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
    	asistenteServiceAPI.guardar(AsistenteServiceTestData.getAsistenteDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link AsistenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_asistente_existe() throws PmzException{
        Mockito.doReturn(AsistenteServiceTestData.getAsistente()).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
        asistenteServiceAPI.guardar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link AsistenteDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con proyectoBeneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_asistente_no_existe_no_proyectoBeneficiario() throws PmzException{
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
    	asistenteServiceAPI.guardar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link AsistenteDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con actividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_asistente_no_existe_no_actividad() throws PmzException{
    	Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario().getId());
    	asistenteServiceAPI.guardar(AsistenteServiceTestData.getAsistenteDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link AsistenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_asistente_existe() throws PmzException{
    	Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario().getId());
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
        Mockito.doReturn(AsistenteServiceTestData.getAsistente()).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
        asistenteServiceAPI.editar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AsistenteDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_asistente_no_existe() throws PmzException{
        asistenteServiceAPI.editar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AsistenteDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con proyectoBeneficiario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_asistente_existe_no_proyectoBeneficiario() throws PmzException{
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
        Mockito.doReturn(AsistenteServiceTestData.getAsistente()).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
    	asistenteServiceAPI.editar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link AsistenteDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con actividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_asistente_existe_no_actividad() throws PmzException{
    	Mockito.doReturn(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario()).when(proyectoBeneficiarioDaoAPI).get(ProyectoBeneficiarioServiceTestData.getProyectoBeneficiario().getId());
        Mockito.doReturn(AsistenteServiceTestData.getAsistente()).when(asistenteDaoAPI).get(ID_PRIMER_ASISTENTE);
    	asistenteServiceAPI.editar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
    
}
