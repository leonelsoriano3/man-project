package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;

import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Persona}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaServiceTestData
 * @date nov 11, 2016
 */
public abstract class PersonaServiceTestData {
    
    private static List<Persona> entities = new ArrayList<Persona>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Persona entity = new Persona();

            entity.setId(new Long(i));
            
            entity.setTipoId(TipoIdEnum.CC);
            entity.setFechaModifica(new Date());
            entity.setNumId("NumId" + i);
            entity.setEstado(EstadoEnum.ACTIVO);
            entity.setNombres("Nombres" + i);
            entity.setApellidos("Apellidos" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Persona} Mock 
     * tranformado a DTO {@link PersonaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PersonaDTO}
     */
    public static PersonaDTO getPersonaDTO(){
        return DTOTransformer.getPersonaDTOFromPersona(getPersona());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Persona} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Persona} Mock
     */
    public static Persona getPersona(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Persona} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Persona}
     */
    public static List<Persona> getPersonas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<PersonaDTO> getResultSetPersonasDTO(){
        List<PersonaDTO> dtos = new ArrayList<PersonaDTO>();
        for (Persona entity : entities) {
            dtos.add(DTOTransformer.getPersonaDTOFromPersona(entity));
        }
        return new PmzResultSet<PersonaDTO>(dtos, getTotalPersonas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalPersonas(){
        return Long.valueOf(entities.size());
    }

}
