package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.dao.api.TrazaActividadDaoAPI;
import co.com.fspb.mgs.service.api.TrazaActividadServiceAPI;
import co.com.fspb.mgs.service.impl.TrazaActividadServiceImpl;
import co.com.fspb.mgs.test.service.data.TrazaActividadServiceTestData;
  import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.test.service.data.ActividadServiceTestData;

/**
 * Prueba unitaria para el servicio {@link TrazaActividadServiceAPI} de la entidad {@link TrazaActividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadServiceAPI
 * @date nov 11, 2016
 */
public class TrazaActividadServiceTest {
	
    private static final Long ID_PRIMER_TRAZAACTIVIDAD = 1L;

    @InjectMocks
    private TrazaActividadServiceAPI trazaActividadServiceAPI = new TrazaActividadServiceImpl();

    @Mock
    private TrazaActividadDaoAPI trazaActividadDaoAPI;
    @Mock
    private ActividadDaoAPI actividadDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividads()).when(trazaActividadDaoAPI).getRecords(null);
        Mockito.doReturn(TrazaActividadServiceTestData.getTotalTrazaActividads()).when(trazaActividadDaoAPI).countRecords(null);
        PmzResultSet<TrazaActividadDTO> trazaActividads = trazaActividadServiceAPI.getRecords(null);
        Assert.assertEquals(TrazaActividadServiceTestData.getTotalTrazaActividads(), trazaActividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TrazaActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaActividad_existe(){
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividad()).when(trazaActividadDaoAPI).get(ID_PRIMER_TRAZAACTIVIDAD);
        TrazaActividadDTO trazaActividad = trazaActividadServiceAPI.getTrazaActividadDTO(ID_PRIMER_TRAZAACTIVIDAD);
        Assert.assertEquals(TrazaActividadServiceTestData.getTrazaActividad().getId(), trazaActividad.getId());
    }

    /**
     * Prueba unitaria de get con {@link TrazaActividadDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaActividad_no_existe(){
        Mockito.doReturn(null).when(trazaActividadDaoAPI).get(ID_PRIMER_TRAZAACTIVIDAD);
        TrazaActividadDTO trazaActividad = trazaActividadServiceAPI.getTrazaActividadDTO(ID_PRIMER_TRAZAACTIVIDAD);
        Assert.assertEquals(null, trazaActividad);
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaActividadDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_trazaActividad_no_existe() throws PmzException{
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
    	trazaActividadServiceAPI.guardar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_trazaActividad_existe() throws PmzException{
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividad()).when(trazaActividadDaoAPI).get(ID_PRIMER_TRAZAACTIVIDAD);
        trazaActividadServiceAPI.guardar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link TrazaActividadDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con actividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_trazaActividad_no_existe_no_actividad() throws PmzException{
    	trazaActividadServiceAPI.guardar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link TrazaActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_trazaActividad_existe() throws PmzException{
    	Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ActividadServiceTestData.getActividad().getId());
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividad()).when(trazaActividadDaoAPI).get(ID_PRIMER_TRAZAACTIVIDAD);
        trazaActividadServiceAPI.editar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TrazaActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_trazaActividad_no_existe() throws PmzException{
        trazaActividadServiceAPI.editar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link TrazaActividadDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con actividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_trazaActividad_existe_no_actividad() throws PmzException{
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividad()).when(trazaActividadDaoAPI).get(ID_PRIMER_TRAZAACTIVIDAD);
    	trazaActividadServiceAPI.editar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    
    
}
