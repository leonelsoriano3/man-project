package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.List;

import co.com.fspb.mgs.dao.model.ProyectoTerritorioPK;
import co.com.fspb.mgs.dao.model.ProyectoTerritorio;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;


/**
 * Clase Mock que simula la lista de respuesta de entidades {@link ProyectoTerritorio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioServiceTestData
 * @date nov 11, 2016
 */
public abstract class ProyectoTerritorioServiceTestData {
    
    private static List<ProyectoTerritorio> entities = new ArrayList<ProyectoTerritorio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            ProyectoTerritorio entity = new ProyectoTerritorio();

		    ProyectoTerritorioPK pk = new ProyectoTerritorioPK();
            pk.setTerritorio(TerritorioServiceTestData.getTerritorio());
            pk.setProyecto(ProyectoServiceTestData.getProyecto());
            entity.setId(pk);
            
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProyectoTerritorio} Mock 
     * tranformado a DTO {@link ProyectoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ProyectoTerritorioDTO}
     */
    public static ProyectoTerritorioDTO getProyectoTerritorioDTO(){
        return DTOTransformer.getProyectoTerritorioDTOFromProyectoTerritorio(getProyectoTerritorio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link ProyectoTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link ProyectoTerritorio} Mock
     */
    public static ProyectoTerritorio getProyectoTerritorio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link ProyectoTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link ProyectoTerritorio}
     */
    public static List<ProyectoTerritorio> getProyectoTerritorios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ProyectoTerritorioDTO> getResultSetProyectoTerritoriosDTO(){
        List<ProyectoTerritorioDTO> dtos = new ArrayList<ProyectoTerritorioDTO>();
        for (ProyectoTerritorio entity : entities) {
            dtos.add(DTOTransformer.getProyectoTerritorioDTOFromProyectoTerritorio(entity));
        }
        return new PmzResultSet<ProyectoTerritorioDTO>(dtos, getTotalProyectoTerritorios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalProyectoTerritorios(){
        return Long.valueOf(entities.size());
    }

}
