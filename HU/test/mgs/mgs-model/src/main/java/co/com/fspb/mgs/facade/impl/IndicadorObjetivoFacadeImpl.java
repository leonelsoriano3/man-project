package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI;
import co.com.fspb.mgs.service.api.IndicadorObjetivoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link IndicadorObjetivoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoFacadeImpl
 * @date nov 11, 2016
 */
@Service("indicadorObjetivoFacade")
public class IndicadorObjetivoFacadeImpl implements IndicadorObjetivoFacadeAPI {

    @Autowired
    private IndicadorObjetivoServiceAPI indicadorObjetivoService;

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorObjetivoDTO> getRecords(PmzPagingCriteria criteria) {
        return indicadorObjetivoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI#guardar()
     */
    @Override
    public void guardar(IndicadorObjetivoDTO indicadorObjetivo) throws PmzException {
        indicadorObjetivoService.guardar(indicadorObjetivo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI#editar()
     */
    @Override
    public void editar(IndicadorObjetivoDTO indicadorObjetivo) throws PmzException {
        indicadorObjetivoService.editar(indicadorObjetivo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI#getIndicadorObjetivoDTO()
     */
    @Override
    public IndicadorObjetivoDTO getIndicadorObjetivoDTO(IndicadorDTO indicador, ObjetivoDTO objetivo) {
        return indicadorObjetivoService.getIndicadorObjetivoDTO(indicador, objetivo);
    }

}
