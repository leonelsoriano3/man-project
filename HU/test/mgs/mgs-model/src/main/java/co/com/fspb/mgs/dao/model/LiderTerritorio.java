package co.com.fspb.mgs.dao.model;


import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_lider_territorio en la BD a la entidad LiderTerritorio.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorio
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_lider_territorio" )
public class LiderTerritorio implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private LiderTerritorioPK id; 


 
    public LiderTerritorioPK getId() {
        return id;
    }

    public void setId(LiderTerritorioPK id) {
        this.id = id;
    }

}
