package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI;
import co.com.fspb.mgs.service.api.BeneficiarioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link BeneficiarioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioFacadeImpl
 * @date nov 11, 2016
 */
@Service("beneficiarioFacade")
public class BeneficiarioFacadeImpl implements BeneficiarioFacadeAPI {

    @Autowired
    private BeneficiarioServiceAPI beneficiarioService;

    /**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<BeneficiarioDTO> getRecords(PmzPagingCriteria criteria) {
        return beneficiarioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI#guardar()
     */
    @Override
    public void guardar(BeneficiarioDTO beneficiario) throws PmzException {
        beneficiarioService.guardar(beneficiario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI#editar()
     */
    @Override
    public void editar(BeneficiarioDTO beneficiario) throws PmzException {
        beneficiarioService.editar(beneficiario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioFacadeAPI#getBeneficiarioDTO()
     */
    @Override
    public BeneficiarioDTO getBeneficiarioDTO(Long id) {
        return beneficiarioService.getBeneficiarioDTO(id);
    }

}
