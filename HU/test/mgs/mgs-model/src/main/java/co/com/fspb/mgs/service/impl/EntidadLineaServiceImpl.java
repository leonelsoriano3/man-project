package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EntidadLineaDaoAPI;
import co.com.fspb.mgs.dao.api.LineaIntervencionDaoAPI;
import co.com.fspb.mgs.dao.model.LineaIntervencion;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dao.model.EntidadLinea;
import co.com.fspb.mgs.dao.model.EntidadLineaPK;
import co.com.fspb.mgs.service.api.EntidadLineaServiceAPI;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;

/**
 * Implementación de la Interfaz {@link EntidadLineaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EntidadLineaServiceImpl implements EntidadLineaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EntidadLineaDaoAPI entidadLineaDao;
    @Autowired
    private LineaIntervencionDaoAPI lineaIntervencionDao;
    @Autowired
    private EntidadDaoAPI entidadDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EntidadLineaDTO entidadLineaDTO) throws PmzException {


		EntidadLineaPK pk = new EntidadLineaPK();
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidadLineaDTO.getEntidadDTO()));
        pk.setLineaIntervencion(DTOTransformer.getLineaIntervencionFromLineaIntervencionDTO(new LineaIntervencion(), entidadLineaDTO.getLineaIntervencionDTO()));
        EntidadLinea entidadLineaValidate = entidadLineaDao.get(pk);

        if (entidadLineaValidate != null) {
            throw new PmzException("El entidadLinea ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EntidadLinea entidadLinea = DTOTransformer.getEntidadLineaFromEntidadLineaDTO(new EntidadLinea(), entidadLineaDTO);

		pk = entidadLinea.getId();
		if (entidadLineaDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(entidadLineaDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setEntidad(entidad);
		}
		
		if (entidadLineaDTO.getLineaIntervencionDTO() != null) {
       		LineaIntervencion lineaIntervencion = lineaIntervencionDao.get(entidadLineaDTO.getLineaIntervencionDTO().getId());
       		if (lineaIntervencion == null) {
                throw new PmzException("El LineaIntervencion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setLineaIntervencion(lineaIntervencion);
		}
		
        entidadLinea.setId(pk);
        
        entidadLineaDao.save(entidadLinea);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadLineaDTO> getRecords(PmzPagingCriteria criteria) {
        List<EntidadLinea> listaReturn = entidadLineaDao.getRecords(criteria);
        Long countTotalRegistros = entidadLineaDao.countRecords(criteria);

        List<EntidadLineaDTO> resultList = new ArrayList<EntidadLineaDTO>();
        for (EntidadLinea entidadLinea : listaReturn) {
            EntidadLineaDTO entidadLineaDTO = DTOTransformer
                    .getEntidadLineaDTOFromEntidadLinea(entidadLinea);
            resultList.add(entidadLineaDTO);
        }
        
        return new PmzResultSet<EntidadLineaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EntidadLineaDTO entidadLineaDTO) throws PmzException{


		EntidadLineaPK pk = new EntidadLineaPK();
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidadLineaDTO.getEntidadDTO()));
        pk.setLineaIntervencion(DTOTransformer.getLineaIntervencionFromLineaIntervencionDTO(new LineaIntervencion(), entidadLineaDTO.getLineaIntervencionDTO()));
        EntidadLinea entidadLinea = entidadLineaDao.get(pk);

        if (entidadLinea == null) {
            throw new PmzException("El entidadLinea no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEntidadLineaFromEntidadLineaDTO(entidadLinea, entidadLineaDTO);

		pk = entidadLinea.getId();
		if (entidadLineaDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(entidadLineaDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setEntidad(entidad);
		}
		
		if (entidadLineaDTO.getLineaIntervencionDTO() != null) {
       		LineaIntervencion lineaIntervencion = lineaIntervencionDao.get(entidadLineaDTO.getLineaIntervencionDTO().getId());
       		if (lineaIntervencion == null) {
                throw new PmzException("El LineaIntervencion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setLineaIntervencion(lineaIntervencion);
		}
		
        entidadLinea.setId(pk);
        
        entidadLineaDao.update(entidadLinea);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EntidadLineaDTO getEntidadLineaDTO(EntidadDTO entidad, LineaIntervencionDTO lineaIntervencion) {

		EntidadLineaPK pk = new EntidadLineaPK();
        pk.setEntidad(DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidad));
        pk.setLineaIntervencion(DTOTransformer.getLineaIntervencionFromLineaIntervencionDTO(new LineaIntervencion(), lineaIntervencion));
        EntidadLinea entidadLinea = entidadLineaDao.get(pk);
        return DTOTransformer.getEntidadLineaDTOFromEntidadLinea(entidadLinea);
    }
}
