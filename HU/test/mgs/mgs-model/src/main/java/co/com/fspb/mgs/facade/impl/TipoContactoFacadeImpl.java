package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI;
import co.com.fspb.mgs.service.api.TipoContactoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TipoContactoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoFacadeImpl
 * @date nov 11, 2016
 */
@Service("tipoContactoFacade")
public class TipoContactoFacadeImpl implements TipoContactoFacadeAPI {

    @Autowired
    private TipoContactoServiceAPI tipoContactoService;

    /**
     * @see co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoContactoDTO> getRecords(PmzPagingCriteria criteria) {
        return tipoContactoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI#guardar()
     */
    @Override
    public void guardar(TipoContactoDTO tipoContacto) throws PmzException {
        tipoContactoService.guardar(tipoContacto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI#editar()
     */
    @Override
    public void editar(TipoContactoDTO tipoContacto) throws PmzException {
        tipoContactoService.editar(tipoContacto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoContactoFacadeAPI#getTipoContactoDTO()
     */
    @Override
    public TipoContactoDTO getTipoContactoDTO(Long id) {
        return tipoContactoService.getTipoContactoDTO(id);
    }

}
