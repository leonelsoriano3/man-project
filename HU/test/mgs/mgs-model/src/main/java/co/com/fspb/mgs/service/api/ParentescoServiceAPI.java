package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParentescoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Parentesco}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoServiceAPI
 * @date nov 11, 2016
 */
public interface ParentescoServiceAPI {

    /**
	 * Registra una entidad {@link Parentesco} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parentesco - {@link ParentescoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ParentescoDTO parentesco) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Parentesco} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parentesco - {@link ParentescoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ParentescoDTO parentesco) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ParentescoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ParentescoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ParentescoDTO}
     */
    ParentescoDTO getParentescoDTO(Long id);

}
