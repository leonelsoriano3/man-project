package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.dao.api.EstadoViaDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoVia;
import co.com.fspb.mgs.dao.api.TipoTerritorioDaoAPI;
import co.com.fspb.mgs.dao.model.TipoTerritorio;
import co.com.fspb.mgs.dao.api.ProblemaComunidadDaoAPI;
import co.com.fspb.mgs.dao.model.ProblemaComunidad;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.service.api.TerritorioServiceAPI;
import co.com.fspb.mgs.dto.TerritorioDTO;

/**
 * Implementación de la Interfaz {@link TerritorioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TerritorioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TerritorioServiceImpl implements TerritorioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TerritorioDaoAPI territorioDao;
    @Autowired
    private EstadoViaDaoAPI estadoViaDao;
    @Autowired
    private TipoTerritorioDaoAPI tipoTerritorioDao;
    @Autowired
    private ProblemaComunidadDaoAPI problemaComunidadDao;
    @Autowired
    private ZonaDaoAPI zonaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TerritorioDTO territorioDTO) throws PmzException {

        Territorio territorioValidate = territorioDao.get(territorioDTO.getId());

        if (territorioValidate != null) {
            throw new PmzException("El territorio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Territorio territorio = DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), territorioDTO);

		if (territorioDTO.getEstadoViaDTO() != null) {
       		EstadoVia estadoVia = estadoViaDao.get(territorioDTO.getEstadoViaDTO().getId());
       		if (estadoVia == null) {
                throw new PmzException("El EstadoVia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setEstadoVia(estadoVia);
		}
		
		if (territorioDTO.getTipoTerritorioDTO() != null) {
       		TipoTerritorio tipoTerritorio = tipoTerritorioDao.get(territorioDTO.getTipoTerritorioDTO().getId());
       		if (tipoTerritorio == null) {
                throw new PmzException("El TipoTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setTipoTerritorio(tipoTerritorio);
		}
		
		if (territorioDTO.getProblemaComunidadDTO() != null) {
       		ProblemaComunidad problemaComunidad = problemaComunidadDao.get(territorioDTO.getProblemaComunidadDTO().getId());
       		if (problemaComunidad == null) {
                throw new PmzException("El ProblemaComunidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setProblemaComunidad(problemaComunidad);
		}
		
		if (territorioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(territorioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setZona(zona);
		}
		
        territorioDao.save(territorio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        List<Territorio> listaReturn = territorioDao.getRecords(criteria);
        Long countTotalRegistros = territorioDao.countRecords(criteria);

        List<TerritorioDTO> resultList = new ArrayList<TerritorioDTO>();
        for (Territorio territorio : listaReturn) {
            TerritorioDTO territorioDTO = DTOTransformer
                    .getTerritorioDTOFromTerritorio(territorio);
            resultList.add(territorioDTO);
        }
        
        return new PmzResultSet<TerritorioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TerritorioDTO territorioDTO) throws PmzException{

        Territorio territorio = territorioDao.get(territorioDTO.getId());

        if (territorio == null) {
            throw new PmzException("El territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTerritorioFromTerritorioDTO(territorio, territorioDTO);

		if (territorioDTO.getEstadoViaDTO() != null) {
       		EstadoVia estadoVia = estadoViaDao.get(territorioDTO.getEstadoViaDTO().getId());
       		if (estadoVia == null) {
                throw new PmzException("El EstadoVia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setEstadoVia(estadoVia);
		}
		
		if (territorioDTO.getTipoTerritorioDTO() != null) {
       		TipoTerritorio tipoTerritorio = tipoTerritorioDao.get(territorioDTO.getTipoTerritorioDTO().getId());
       		if (tipoTerritorio == null) {
                throw new PmzException("El TipoTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setTipoTerritorio(tipoTerritorio);
		}
		
		if (territorioDTO.getProblemaComunidadDTO() != null) {
       		ProblemaComunidad problemaComunidad = problemaComunidadDao.get(territorioDTO.getProblemaComunidadDTO().getId());
       		if (problemaComunidad == null) {
                throw new PmzException("El ProblemaComunidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setProblemaComunidad(problemaComunidad);
		}
		
		if (territorioDTO.getZonaDTO() != null) {
       		Zona zona = zonaDao.get(territorioDTO.getZonaDTO().getId());
       		if (zona == null) {
                throw new PmzException("El Zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		territorio.setZona(zona);
		}
		
        territorioDao.update(territorio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TerritorioDTO getTerritorioDTO(Long id) {
        Territorio territorio = territorioDao.get(id);
        return DTOTransformer.getTerritorioDTOFromTerritorio(territorio);
    }
}
