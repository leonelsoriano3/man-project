package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI;
import co.com.fspb.mgs.service.api.TipoTenenciaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TipoTenenciaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTenenciaFacadeImpl
 * @date nov 11, 2016
 */
@Service("tipoTenenciaFacade")
public class TipoTenenciaFacadeImpl implements TipoTenenciaFacadeAPI {

    @Autowired
    private TipoTenenciaServiceAPI tipoTenenciaService;

    /**
     * @see co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoTenenciaDTO> getRecords(PmzPagingCriteria criteria) {
        return tipoTenenciaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI#guardar()
     */
    @Override
    public void guardar(TipoTenenciaDTO tipoTenencia) throws PmzException {
        tipoTenenciaService.guardar(tipoTenencia);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI#editar()
     */
    @Override
    public void editar(TipoTenenciaDTO tipoTenencia) throws PmzException {
        tipoTenenciaService.editar(tipoTenencia);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoTenenciaFacadeAPI#getTipoTenenciaDTO()
     */
    @Override
    public TipoTenenciaDTO getTipoTenenciaDTO(Long id) {
        return tipoTenenciaService.getTipoTenenciaDTO(id);
    }

}
