package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.dao.model.Componente;
import co.com.fspb.mgs.service.api.ComponenteServiceAPI;
import co.com.fspb.mgs.dto.ComponenteDTO;

/**
 * Implementación de la Interfaz {@link ComponenteServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ComponenteServiceImpl implements ComponenteServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ComponenteDaoAPI componenteDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ComponenteDTO componenteDTO) throws PmzException {

        Componente componenteValidate = componenteDao.get(componenteDTO.getId());

        if (componenteValidate != null) {
            throw new PmzException("El componente ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Componente componente = DTOTransformer.getComponenteFromComponenteDTO(new Componente(), componenteDTO);

        componenteDao.save(componente);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ComponenteDTO> getRecords(PmzPagingCriteria criteria) {
        List<Componente> listaReturn = componenteDao.getRecords(criteria);
        Long countTotalRegistros = componenteDao.countRecords(criteria);

        List<ComponenteDTO> resultList = new ArrayList<ComponenteDTO>();
        for (Componente componente : listaReturn) {
            ComponenteDTO componenteDTO = DTOTransformer
                    .getComponenteDTOFromComponente(componente);
            resultList.add(componenteDTO);
        }
        
        return new PmzResultSet<ComponenteDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ComponenteDTO componenteDTO) throws PmzException{

        Componente componente = componenteDao.get(componenteDTO.getId());

        if (componente == null) {
            throw new PmzException("El componente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getComponenteFromComponenteDTO(componente, componenteDTO);

        componenteDao.update(componente);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ComponenteDTO getComponenteDTO(Long id) {
        Componente componente = componenteDao.get(id);
        return DTOTransformer.getComponenteDTOFromComponente(componente);
    }
}
