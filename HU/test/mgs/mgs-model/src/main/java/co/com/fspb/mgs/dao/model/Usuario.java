package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_usuario en la BD a la entidad Usuario.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Usuario
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_usuario" )
public class Usuario implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_usuario", nullable = false)
	private Long id; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "nombre_usuario", nullable = false, length = 20)
    private String nombreUsuario; 


	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona", nullable = true)
    private Persona persona; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_cargo", nullable = false)
    private Cargo cargo; 

	@Column(name = "contrasena", nullable = true, length = 100)
    private String contrasena; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

}
