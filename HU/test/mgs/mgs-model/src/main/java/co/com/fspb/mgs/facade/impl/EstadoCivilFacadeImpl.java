package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI;
import co.com.fspb.mgs.service.api.EstadoCivilServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EstadoCivilFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoCivilFacadeImpl
 * @date nov 11, 2016
 */
@Service("estadoCivilFacade")
public class EstadoCivilFacadeImpl implements EstadoCivilFacadeAPI {

    @Autowired
    private EstadoCivilServiceAPI estadoCivilService;

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoCivilDTO> getRecords(PmzPagingCriteria criteria) {
        return estadoCivilService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI#guardar()
     */
    @Override
    public void guardar(EstadoCivilDTO estadoCivil) throws PmzException {
        estadoCivilService.guardar(estadoCivil);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI#editar()
     */
    @Override
    public void editar(EstadoCivilDTO estadoCivil) throws PmzException {
        estadoCivilService.editar(estadoCivil);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoCivilFacadeAPI#getEstadoCivilDTO()
     */
    @Override
    public EstadoCivilDTO getEstadoCivilDTO(Long id) {
        return estadoCivilService.getEstadoCivilDTO(id);
    }

}
