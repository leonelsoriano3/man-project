package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EstructuraFamiliarDaoAPI;
import co.com.fspb.mgs.dao.model.EstructuraFamiliar;
import co.com.fspb.mgs.service.api.EstructuraFamiliarServiceAPI;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;

/**
 * Implementación de la Interfaz {@link EstructuraFamiliarServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstructuraFamiliarServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EstructuraFamiliarServiceImpl implements EstructuraFamiliarServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EstructuraFamiliarDaoAPI estructuraFamiliarDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EstructuraFamiliarDTO estructuraFamiliarDTO) throws PmzException {

        EstructuraFamiliar estructuraFamiliarValidate = estructuraFamiliarDao.get(estructuraFamiliarDTO.getId());

        if (estructuraFamiliarValidate != null) {
            throw new PmzException("El estructuraFamiliar ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EstructuraFamiliar estructuraFamiliar = DTOTransformer.getEstructuraFamiliarFromEstructuraFamiliarDTO(new EstructuraFamiliar(), estructuraFamiliarDTO);

        estructuraFamiliarDao.save(estructuraFamiliar);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstructuraFamiliarDTO> getRecords(PmzPagingCriteria criteria) {
        List<EstructuraFamiliar> listaReturn = estructuraFamiliarDao.getRecords(criteria);
        Long countTotalRegistros = estructuraFamiliarDao.countRecords(criteria);

        List<EstructuraFamiliarDTO> resultList = new ArrayList<EstructuraFamiliarDTO>();
        for (EstructuraFamiliar estructuraFamiliar : listaReturn) {
            EstructuraFamiliarDTO estructuraFamiliarDTO = DTOTransformer
                    .getEstructuraFamiliarDTOFromEstructuraFamiliar(estructuraFamiliar);
            resultList.add(estructuraFamiliarDTO);
        }
        
        return new PmzResultSet<EstructuraFamiliarDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EstructuraFamiliarDTO estructuraFamiliarDTO) throws PmzException{

        EstructuraFamiliar estructuraFamiliar = estructuraFamiliarDao.get(estructuraFamiliarDTO.getId());

        if (estructuraFamiliar == null) {
            throw new PmzException("El estructuraFamiliar no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEstructuraFamiliarFromEstructuraFamiliarDTO(estructuraFamiliar, estructuraFamiliarDTO);

        estructuraFamiliarDao.update(estructuraFamiliar);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EstructuraFamiliarDTO getEstructuraFamiliarDTO(Long id) {
        EstructuraFamiliar estructuraFamiliar = estructuraFamiliarDao.get(id);
        return DTOTransformer.getEstructuraFamiliarDTOFromEstructuraFamiliar(estructuraFamiliar);
    }
}
