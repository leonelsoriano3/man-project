package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.IndicadorProyectoDaoAPI;
import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.IndicadorProyecto;
import co.com.fspb.mgs.dao.model.IndicadorProyectoPK;
import co.com.fspb.mgs.service.api.IndicadorProyectoServiceAPI;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;

/**
 * Implementación de la Interfaz {@link IndicadorProyectoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class IndicadorProyectoServiceImpl implements IndicadorProyectoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private IndicadorProyectoDaoAPI indicadorProyectoDao;
    @Autowired
    private IndicadorDaoAPI indicadorDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(IndicadorProyectoDTO indicadorProyectoDTO) throws PmzException {


		IndicadorProyectoPK pk = new IndicadorProyectoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicadorProyectoDTO.getIndicadorDTO()));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), indicadorProyectoDTO.getProyectoDTO()));
        IndicadorProyecto indicadorProyectoValidate = indicadorProyectoDao.get(pk);

        if (indicadorProyectoValidate != null) {
            throw new PmzException("El indicadorProyecto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        IndicadorProyecto indicadorProyecto = DTOTransformer.getIndicadorProyectoFromIndicadorProyectoDTO(new IndicadorProyecto(), indicadorProyectoDTO);

		pk = indicadorProyecto.getId();
		if (indicadorProyectoDTO.getIndicadorDTO() != null) {
       		Indicador indicador = indicadorDao.get(indicadorProyectoDTO.getIndicadorDTO().getId());
       		if (indicador == null) {
                throw new PmzException("El Indicador no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIndicador(indicador);
		}
		
		if (indicadorProyectoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(indicadorProyectoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
        indicadorProyecto.setId(pk);
        
        indicadorProyectoDao.save(indicadorProyecto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        List<IndicadorProyecto> listaReturn = indicadorProyectoDao.getRecords(criteria);
        Long countTotalRegistros = indicadorProyectoDao.countRecords(criteria);

        List<IndicadorProyectoDTO> resultList = new ArrayList<IndicadorProyectoDTO>();
        for (IndicadorProyecto indicadorProyecto : listaReturn) {
            IndicadorProyectoDTO indicadorProyectoDTO = DTOTransformer
                    .getIndicadorProyectoDTOFromIndicadorProyecto(indicadorProyecto);
            resultList.add(indicadorProyectoDTO);
        }
        
        return new PmzResultSet<IndicadorProyectoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(IndicadorProyectoDTO indicadorProyectoDTO) throws PmzException{


		IndicadorProyectoPK pk = new IndicadorProyectoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicadorProyectoDTO.getIndicadorDTO()));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), indicadorProyectoDTO.getProyectoDTO()));
        IndicadorProyecto indicadorProyecto = indicadorProyectoDao.get(pk);

        if (indicadorProyecto == null) {
            throw new PmzException("El indicadorProyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getIndicadorProyectoFromIndicadorProyectoDTO(indicadorProyecto, indicadorProyectoDTO);

		pk = indicadorProyecto.getId();
		if (indicadorProyectoDTO.getIndicadorDTO() != null) {
       		Indicador indicador = indicadorDao.get(indicadorProyectoDTO.getIndicadorDTO().getId());
       		if (indicador == null) {
                throw new PmzException("El Indicador no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIndicador(indicador);
		}
		
		if (indicadorProyectoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(indicadorProyectoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
        indicadorProyecto.setId(pk);
        
        indicadorProyectoDao.update(indicadorProyecto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public IndicadorProyectoDTO getIndicadorProyectoDTO(IndicadorDTO indicador, ProyectoDTO proyecto) {

		IndicadorProyectoPK pk = new IndicadorProyectoPK();
        pk.setIndicador(DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicador));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyecto));
        IndicadorProyecto indicadorProyecto = indicadorProyectoDao.get(pk);
        return DTOTransformer.getIndicadorProyectoDTOFromIndicadorProyecto(indicadorProyecto);
    }
}
