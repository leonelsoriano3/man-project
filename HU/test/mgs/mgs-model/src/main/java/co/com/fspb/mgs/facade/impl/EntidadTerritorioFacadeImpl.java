package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI;
import co.com.fspb.mgs.service.api.EntidadTerritorioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EntidadTerritorioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioFacadeImpl
 * @date nov 11, 2016
 */
@Service("entidadTerritorioFacade")
public class EntidadTerritorioFacadeImpl implements EntidadTerritorioFacadeAPI {

    @Autowired
    private EntidadTerritorioServiceAPI entidadTerritorioService;

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        return entidadTerritorioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI#guardar()
     */
    @Override
    public void guardar(EntidadTerritorioDTO entidadTerritorio) throws PmzException {
        entidadTerritorioService.guardar(entidadTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI#editar()
     */
    @Override
    public void editar(EntidadTerritorioDTO entidadTerritorio) throws PmzException {
        entidadTerritorioService.editar(entidadTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI#getEntidadTerritorioDTO()
     */
    @Override
    public EntidadTerritorioDTO getEntidadTerritorioDTO(TerritorioDTO territorio, EntidadDTO entidad) {
        return entidadTerritorioService.getEntidadTerritorioDTO(territorio, entidad);
    }

}
