package co.com.fspb.mgs.dao.api;

import java.util.List;

import com.premize.pmz.dao.api.Dao;

import co.com.fspb.mgs.dao.model.ProblemaComunidad;
import com.premize.pmz.api.dto.PmzPagingCriteria;

/**
 * Interfaz de Data Access Object (DAO) para la entidad {@link ProblemaComunidad}
 * Extiende la interfaz de PMZ {@link Dao}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadDaoAPI
 * @date nov 11, 2016
 */
public interface ProblemaComunidadDaoAPI extends Dao<ProblemaComunidad, Long> {

    /**
	 * Retorna la lista paginada y filtrada según el {@link PmzPagingCriteria}
	 * de la entidad {@link ProblemaComunidad}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public List<ProblemaComunidad> getRecords(PmzPagingCriteria criteria);
    
    /**
	 * Retorna el total de regirtos según el {@link PmzPagingCriteria}
	 * de la entidad {@link ProblemaComunidad}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public Long countRecords(PmzPagingCriteria criteria);

}
