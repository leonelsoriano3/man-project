package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ParentescoDaoAPI;
import co.com.fspb.mgs.dao.model.Parentesco;
import co.com.fspb.mgs.service.api.ParentescoServiceAPI;
import co.com.fspb.mgs.dto.ParentescoDTO;

/**
 * Implementación de la Interfaz {@link ParentescoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParentescoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ParentescoServiceImpl implements ParentescoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ParentescoDaoAPI parentescoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ParentescoDTO parentescoDTO) throws PmzException {

        Parentesco parentescoValidate = parentescoDao.get(parentescoDTO.getId());

        if (parentescoValidate != null) {
            throw new PmzException("El parentesco ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Parentesco parentesco = DTOTransformer.getParentescoFromParentescoDTO(new Parentesco(), parentescoDTO);

        parentescoDao.save(parentesco);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParentescoDTO> getRecords(PmzPagingCriteria criteria) {
        List<Parentesco> listaReturn = parentescoDao.getRecords(criteria);
        Long countTotalRegistros = parentescoDao.countRecords(criteria);

        List<ParentescoDTO> resultList = new ArrayList<ParentescoDTO>();
        for (Parentesco parentesco : listaReturn) {
            ParentescoDTO parentescoDTO = DTOTransformer
                    .getParentescoDTOFromParentesco(parentesco);
            resultList.add(parentescoDTO);
        }
        
        return new PmzResultSet<ParentescoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ParentescoDTO parentescoDTO) throws PmzException{

        Parentesco parentesco = parentescoDao.get(parentescoDTO.getId());

        if (parentesco == null) {
            throw new PmzException("El parentesco no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getParentescoFromParentescoDTO(parentesco, parentescoDTO);

        parentescoDao.update(parentesco);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ParentescoDTO getParentescoDTO(Long id) {
        Parentesco parentesco = parentescoDao.get(id);
        return DTOTransformer.getParentescoDTOFromParentesco(parentesco);
    }
}
