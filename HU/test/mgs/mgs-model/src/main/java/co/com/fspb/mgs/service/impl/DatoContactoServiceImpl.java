package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.DatoContactoDaoAPI;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.api.TipoContactoDaoAPI;
import co.com.fspb.mgs.dao.model.TipoContacto;
import co.com.fspb.mgs.dao.model.DatoContacto;
import co.com.fspb.mgs.service.api.DatoContactoServiceAPI;
import co.com.fspb.mgs.dto.DatoContactoDTO;

/**
 * Implementación de la Interfaz {@link DatoContactoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoContactoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class DatoContactoServiceImpl implements DatoContactoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private DatoContactoDaoAPI datoContactoDao;
    @Autowired
    private PersonaDaoAPI personaDao;
    @Autowired
    private TipoContactoDaoAPI tipoContactoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(DatoContactoDTO datoContactoDTO) throws PmzException {

        DatoContacto datoContactoValidate = datoContactoDao.get(datoContactoDTO.getId());

        if (datoContactoValidate != null) {
            throw new PmzException("El datoContacto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        DatoContacto datoContacto = DTOTransformer.getDatoContactoFromDatoContactoDTO(new DatoContacto(), datoContactoDTO);

		if (datoContactoDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(datoContactoDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoContacto.setPersona(persona);
		}
		
		if (datoContactoDTO.getTipoContactoDTO() != null) {
       		TipoContacto tipoContacto = tipoContactoDao.get(datoContactoDTO.getTipoContactoDTO().getId());
       		if (tipoContacto == null) {
                throw new PmzException("El TipoContacto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoContacto.setTipoContacto(tipoContacto);
		}
		
        datoContactoDao.save(datoContacto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<DatoContactoDTO> getRecords(PmzPagingCriteria criteria) {
        List<DatoContacto> listaReturn = datoContactoDao.getRecords(criteria);
        Long countTotalRegistros = datoContactoDao.countRecords(criteria);

        List<DatoContactoDTO> resultList = new ArrayList<DatoContactoDTO>();
        for (DatoContacto datoContacto : listaReturn) {
            DatoContactoDTO datoContactoDTO = DTOTransformer
                    .getDatoContactoDTOFromDatoContacto(datoContacto);
            resultList.add(datoContactoDTO);
        }
        
        return new PmzResultSet<DatoContactoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(DatoContactoDTO datoContactoDTO) throws PmzException{

        DatoContacto datoContacto = datoContactoDao.get(datoContactoDTO.getId());

        if (datoContacto == null) {
            throw new PmzException("El datoContacto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getDatoContactoFromDatoContactoDTO(datoContacto, datoContactoDTO);

		if (datoContactoDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(datoContactoDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoContacto.setPersona(persona);
		}
		
		if (datoContactoDTO.getTipoContactoDTO() != null) {
       		TipoContacto tipoContacto = tipoContactoDao.get(datoContactoDTO.getTipoContactoDTO().getId());
       		if (tipoContacto == null) {
                throw new PmzException("El TipoContacto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoContacto.setTipoContacto(tipoContacto);
		}
		
        datoContactoDao.update(datoContacto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public DatoContactoDTO getDatoContactoDTO(Long id) {
        DatoContacto datoContacto = datoContactoDao.get(id);
        return DTOTransformer.getDatoContactoDTOFromDatoContacto(datoContacto);
    }
}
