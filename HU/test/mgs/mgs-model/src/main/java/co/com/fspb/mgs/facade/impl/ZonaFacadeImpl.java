package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.facade.api.ZonaFacadeAPI;
import co.com.fspb.mgs.service.api.ZonaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ZonaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaFacadeImpl
 * @date nov 11, 2016
 */
@Service("zonaFacade")
public class ZonaFacadeImpl implements ZonaFacadeAPI {

    @Autowired
    private ZonaServiceAPI zonaService;

    /**
     * @see co.com.fspb.mgs.facade.api.ZonaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ZonaDTO> getRecords(PmzPagingCriteria criteria) {
        return zonaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ZonaFacadeAPI#guardar()
     */
    @Override
    public void guardar(ZonaDTO zona) throws PmzException {
        zonaService.guardar(zona);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ZonaFacadeAPI#editar()
     */
    @Override
    public void editar(ZonaDTO zona) throws PmzException {
        zonaService.editar(zona);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ZonaFacadeAPI#getZonaDTO()
     */
    @Override
    public ZonaDTO getZonaDTO(Long id) {
        return zonaService.getZonaDTO(id);
    }

}
