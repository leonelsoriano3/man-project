package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstratoSocioeconomicoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.GeneroEnum;

/**
 * Mapeo de la tabla mgs_beneficiario en la BD a la entidad Beneficiario.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Beneficiario
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_beneficiario" )
public class Beneficiario implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_beneficiario", nullable = false)
	private Long id; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "area_id", nullable = false)
    private AreaEducacion areaEducacion; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_estructura_familiar", nullable = false)
    private EstructuraFamiliar estructuraFamiliar; 

	@Column(name = "estrato_socioeconomico", nullable = true, length = 30)
    @Enumerated(EnumType.STRING)
    private EstratoSocioeconomicoEnum estratoSocioeconomico; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estado_civil", nullable = false)
    private EstadoCivil estadoCivil; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_desc_indep", nullable = false)
    private DescIndependiente descIndependiente; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_nivel_educativo", nullable = false)
    private NivelEducativo nivelEducativo; 

	@Column(name = "otra_area_educacion", nullable = false, length = 100)
    private String otraAreaEducacion; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_tipo_vivienda", nullable = false)
    private TipoVivienda tipoVivienda; 

    @Column(name = "cant_personas_hogar", nullable = true, precision = 10, scale = 0)
    private Integer cantPersonasHogar; 

	@Column(name = "organizacion_comunitaria", nullable = true, length = 50)
    private String organizacionComunitaria; 

    @Column(name = "nivel_ingresos", nullable = false, precision = 10, scale = 0)
    private Integer nivelIngresos; 

    @Column(name = "cant_personas_cargo", nullable = true, precision = 10, scale = 0)
    private Integer cantPersonasCargo; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_tipo_tenencia", nullable = false)
    private TipoTenencia tipoTenencia; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_zona", nullable = false)
    private Zona zona; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_material_vivienda", nullable = false)
    private MaterialVivienda materialVivienda; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_bd_nacional", nullable = false)
    private BdNacional bdNacional; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_afiliado_salud", nullable = false)
    private AfiliadoSalud afiliadoSalud; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona", nullable = true)
    private Persona persona; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grupo_etnico", nullable = false)
    private GrupoEtnico grupoEtnico; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_ocupacion", nullable = false)
    private Ocupacion ocupacion; 

	@Column(name = "genero", nullable = false, length = 30)
	@Enumerated(EnumType.STRING)
	private GeneroEnum genero; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_lugar_expedicion", nullable = false)
    private Municipio municipio; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public AreaEducacion getAreaEducacion() {
        return areaEducacion;
    }

    public void setAreaEducacion(AreaEducacion areaEducacion) {
        this.areaEducacion = areaEducacion;
    }

    public EstructuraFamiliar getEstructuraFamiliar() {
        return estructuraFamiliar;
    }

    public void setEstructuraFamiliar(EstructuraFamiliar estructuraFamiliar) {
        this.estructuraFamiliar = estructuraFamiliar;
    }

    public EstratoSocioeconomicoEnum getEstratoSocioeconomico() {
        return estratoSocioeconomico;
    }

    public void setEstratoSocioeconomico(EstratoSocioeconomicoEnum estratoSocioeconomico) {
        this.estratoSocioeconomico = estratoSocioeconomico;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public DescIndependiente getDescIndependiente() {
        return descIndependiente;
    }

    public void setDescIndependiente(DescIndependiente descIndependiente) {
        this.descIndependiente = descIndependiente;
    }

    public NivelEducativo getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(NivelEducativo nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public String getOtraAreaEducacion() {
        return otraAreaEducacion;
    }

    public void setOtraAreaEducacion(String otraAreaEducacion) {
        this.otraAreaEducacion = otraAreaEducacion;
    }

    public TipoVivienda getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(TipoVivienda tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public Integer getCantPersonasHogar() {
        return cantPersonasHogar;
    }

    public void setCantPersonasHogar(Integer cantPersonasHogar) {
        this.cantPersonasHogar = cantPersonasHogar;
    }

    public String getOrganizacionComunitaria() {
        return organizacionComunitaria;
    }

    public void setOrganizacionComunitaria(String organizacionComunitaria) {
        this.organizacionComunitaria = organizacionComunitaria;
    }

    public Integer getNivelIngresos() {
        return nivelIngresos;
    }

    public void setNivelIngresos(Integer nivelIngresos) {
        this.nivelIngresos = nivelIngresos;
    }

    public Integer getCantPersonasCargo() {
        return cantPersonasCargo;
    }

    public void setCantPersonasCargo(Integer cantPersonasCargo) {
        this.cantPersonasCargo = cantPersonasCargo;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public TipoTenencia getTipoTenencia() {
        return tipoTenencia;
    }

    public void setTipoTenencia(TipoTenencia tipoTenencia) {
        this.tipoTenencia = tipoTenencia;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public MaterialVivienda getMaterialVivienda() {
        return materialVivienda;
    }

    public void setMaterialVivienda(MaterialVivienda materialVivienda) {
        this.materialVivienda = materialVivienda;
    }

    public BdNacional getBdNacional() {
        return bdNacional;
    }

    public void setBdNacional(BdNacional bdNacional) {
        this.bdNacional = bdNacional;
    }

    public AfiliadoSalud getAfiliadoSalud() {
        return afiliadoSalud;
    }

    public void setAfiliadoSalud(AfiliadoSalud afiliadoSalud) {
        this.afiliadoSalud = afiliadoSalud;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public GrupoEtnico getGrupoEtnico() {
        return grupoEtnico;
    }

    public void setGrupoEtnico(GrupoEtnico grupoEtnico) {
        this.grupoEtnico = grupoEtnico;
    }

    public Ocupacion getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(Ocupacion ocupacion) {
        this.ocupacion = ocupacion;
    }

    public GeneroEnum getGenero() {
        return genero;
    }

    public void setGenero(GeneroEnum genero) {
        this.genero = genero;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

}
