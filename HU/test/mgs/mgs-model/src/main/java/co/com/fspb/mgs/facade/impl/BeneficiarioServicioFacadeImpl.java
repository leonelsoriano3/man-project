package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI;
import co.com.fspb.mgs.service.api.BeneficiarioServicioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link BeneficiarioServicioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioFacadeImpl
 * @date nov 11, 2016
 */
@Service("beneficiarioServicioFacade")
public class BeneficiarioServicioFacadeImpl implements BeneficiarioServicioFacadeAPI {

    @Autowired
    private BeneficiarioServicioServiceAPI beneficiarioServicioService;

    /**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<BeneficiarioServicioDTO> getRecords(PmzPagingCriteria criteria) {
        return beneficiarioServicioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI#guardar()
     */
    @Override
    public void guardar(BeneficiarioServicioDTO beneficiarioServicio) throws PmzException {
        beneficiarioServicioService.guardar(beneficiarioServicio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI#editar()
     */
    @Override
    public void editar(BeneficiarioServicioDTO beneficiarioServicio) throws PmzException {
        beneficiarioServicioService.editar(beneficiarioServicio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.BeneficiarioServicioFacadeAPI#getBeneficiarioServicioDTO()
     */
    @Override
    public BeneficiarioServicioDTO getBeneficiarioServicioDTO(BeneficiarioDTO beneficiario, ServicioDTO servicio) {
        return beneficiarioServicioService.getBeneficiarioServicioDTO(beneficiario, servicio);
    }

}
