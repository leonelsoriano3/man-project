package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI;
import co.com.fspb.mgs.service.api.TrazaActividadServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TrazaActividadFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadFacadeImpl
 * @date nov 11, 2016
 */
@Service("trazaActividadFacade")
public class TrazaActividadFacadeImpl implements TrazaActividadFacadeAPI {

    @Autowired
    private TrazaActividadServiceAPI trazaActividadService;

    /**
     * @see co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TrazaActividadDTO> getRecords(PmzPagingCriteria criteria) {
        return trazaActividadService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI#guardar()
     */
    @Override
    public void guardar(TrazaActividadDTO trazaActividad) throws PmzException {
        trazaActividadService.guardar(trazaActividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI#editar()
     */
    @Override
    public void editar(TrazaActividadDTO trazaActividad) throws PmzException {
        trazaActividadService.editar(trazaActividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI#getTrazaActividadDTO()
     */
    @Override
    public TrazaActividadDTO getTrazaActividadDTO(Long id) {
        return trazaActividadService.getTrazaActividadDTO(id);
    }

}
