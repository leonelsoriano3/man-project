package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TipoContactoDaoAPI;
import co.com.fspb.mgs.dao.model.TipoContacto;
import co.com.fspb.mgs.service.api.TipoContactoServiceAPI;
import co.com.fspb.mgs.dto.TipoContactoDTO;

/**
 * Implementación de la Interfaz {@link TipoContactoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TipoContactoServiceImpl implements TipoContactoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TipoContactoDaoAPI tipoContactoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TipoContactoDTO tipoContactoDTO) throws PmzException {

        TipoContacto tipoContactoValidate = tipoContactoDao.get(tipoContactoDTO.getId());

        if (tipoContactoValidate != null) {
            throw new PmzException("El tipoContacto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TipoContacto tipoContacto = DTOTransformer.getTipoContactoFromTipoContactoDTO(new TipoContacto(), tipoContactoDTO);

        tipoContactoDao.save(tipoContacto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoContactoDTO> getRecords(PmzPagingCriteria criteria) {
        List<TipoContacto> listaReturn = tipoContactoDao.getRecords(criteria);
        Long countTotalRegistros = tipoContactoDao.countRecords(criteria);

        List<TipoContactoDTO> resultList = new ArrayList<TipoContactoDTO>();
        for (TipoContacto tipoContacto : listaReturn) {
            TipoContactoDTO tipoContactoDTO = DTOTransformer
                    .getTipoContactoDTOFromTipoContacto(tipoContacto);
            resultList.add(tipoContactoDTO);
        }
        
        return new PmzResultSet<TipoContactoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TipoContactoDTO tipoContactoDTO) throws PmzException{

        TipoContacto tipoContacto = tipoContactoDao.get(tipoContactoDTO.getId());

        if (tipoContacto == null) {
            throw new PmzException("El tipoContacto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTipoContactoFromTipoContactoDTO(tipoContacto, tipoContactoDTO);

        tipoContactoDao.update(tipoContacto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TipoContactoDTO getTipoContactoDTO(Long id) {
        TipoContacto tipoContacto = tipoContactoDao.get(id);
        return DTOTransformer.getTipoContactoDTOFromTipoContacto(tipoContacto);
    }
}
