package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.api.RolFundacionDaoAPI;
import co.com.fspb.mgs.dao.model.RolFundacion;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.service.api.ProyectoServiceAPI;
import co.com.fspb.mgs.dto.ProyectoDTO;

/**
 * Implementación de la Interfaz {@link ProyectoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ProyectoServiceImpl implements ProyectoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ProyectoDaoAPI proyectoDao;
    @Autowired
    private RolFundacionDaoAPI rolFundacionDao;
    @Autowired
    private UsuarioDaoAPI usuarioDao;
    @Autowired
    private IniciativaDaoAPI iniciativaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ProyectoDTO proyectoDTO) throws PmzException {

        Proyecto proyectoValidate = proyectoDao.get(proyectoDTO.getId());

        if (proyectoValidate != null) {
            throw new PmzException("El proyecto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Proyecto proyecto = DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyectoDTO);

		if (proyectoDTO.getRolFundacionDTO() != null) {
       		RolFundacion rolFundacion = rolFundacionDao.get(proyectoDTO.getRolFundacionDTO().getId());
       		if (rolFundacion == null) {
                throw new PmzException("El RolFundacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setRolFundacion(rolFundacion);
		}
		
		if (proyectoDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(proyectoDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setUsuario(usuario);
		}
		
		if (proyectoDTO.getIniciativaDTO() != null) {
       		Iniciativa iniciativa = iniciativaDao.get(proyectoDTO.getIniciativaDTO().getId());
       		if (iniciativa == null) {
                throw new PmzException("El Iniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setIniciativa(iniciativa);
		}
		
        proyectoDao.save(proyecto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        List<Proyecto> listaReturn = proyectoDao.getRecords(criteria);
        Long countTotalRegistros = proyectoDao.countRecords(criteria);

        List<ProyectoDTO> resultList = new ArrayList<ProyectoDTO>();
        for (Proyecto proyecto : listaReturn) {
            ProyectoDTO proyectoDTO = DTOTransformer
                    .getProyectoDTOFromProyecto(proyecto);
            resultList.add(proyectoDTO);
        }
        
        return new PmzResultSet<ProyectoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ProyectoDTO proyectoDTO) throws PmzException{

        Proyecto proyecto = proyectoDao.get(proyectoDTO.getId());

        if (proyecto == null) {
            throw new PmzException("El proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getProyectoFromProyectoDTO(proyecto, proyectoDTO);

		if (proyectoDTO.getRolFundacionDTO() != null) {
       		RolFundacion rolFundacion = rolFundacionDao.get(proyectoDTO.getRolFundacionDTO().getId());
       		if (rolFundacion == null) {
                throw new PmzException("El RolFundacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setRolFundacion(rolFundacion);
		}
		
		if (proyectoDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(proyectoDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setUsuario(usuario);
		}
		
		if (proyectoDTO.getIniciativaDTO() != null) {
       		Iniciativa iniciativa = iniciativaDao.get(proyectoDTO.getIniciativaDTO().getId());
       		if (iniciativa == null) {
                throw new PmzException("El Iniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyecto.setIniciativa(iniciativa);
		}
		
        proyectoDao.update(proyecto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ProyectoDTO getProyectoDTO(Long id) {
        Proyecto proyecto = proyectoDao.get(id);
        return DTOTransformer.getProyectoDTOFromProyecto(proyecto);
    }
}
