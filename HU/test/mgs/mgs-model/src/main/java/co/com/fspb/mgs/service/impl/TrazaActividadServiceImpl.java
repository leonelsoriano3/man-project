package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TrazaActividadDaoAPI;
import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.dao.model.Actividad;
import co.com.fspb.mgs.dao.model.TrazaActividad;
import co.com.fspb.mgs.service.api.TrazaActividadServiceAPI;
import co.com.fspb.mgs.dto.TrazaActividadDTO;

/**
 * Implementación de la Interfaz {@link TrazaActividadServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TrazaActividadServiceImpl implements TrazaActividadServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TrazaActividadDaoAPI trazaActividadDao;
    @Autowired
    private ActividadDaoAPI actividadDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TrazaActividadDTO trazaActividadDTO) throws PmzException {

        TrazaActividad trazaActividadValidate = trazaActividadDao.get(trazaActividadDTO.getId());

        if (trazaActividadValidate != null) {
            throw new PmzException("El trazaActividad ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TrazaActividad trazaActividad = DTOTransformer.getTrazaActividadFromTrazaActividadDTO(new TrazaActividad(), trazaActividadDTO);

		if (trazaActividadDTO.getActividadDTO() != null) {
       		Actividad actividad = actividadDao.get(trazaActividadDTO.getActividadDTO().getId());
       		if (actividad == null) {
                throw new PmzException("El Actividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		trazaActividad.setActividad(actividad);
		}
		
        trazaActividadDao.save(trazaActividad);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TrazaActividadDTO> getRecords(PmzPagingCriteria criteria) {
        List<TrazaActividad> listaReturn = trazaActividadDao.getRecords(criteria);
        Long countTotalRegistros = trazaActividadDao.countRecords(criteria);

        List<TrazaActividadDTO> resultList = new ArrayList<TrazaActividadDTO>();
        for (TrazaActividad trazaActividad : listaReturn) {
            TrazaActividadDTO trazaActividadDTO = DTOTransformer
                    .getTrazaActividadDTOFromTrazaActividad(trazaActividad);
            resultList.add(trazaActividadDTO);
        }
        
        return new PmzResultSet<TrazaActividadDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TrazaActividadDTO trazaActividadDTO) throws PmzException{

        TrazaActividad trazaActividad = trazaActividadDao.get(trazaActividadDTO.getId());

        if (trazaActividad == null) {
            throw new PmzException("El trazaActividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTrazaActividadFromTrazaActividadDTO(trazaActividad, trazaActividadDTO);

		if (trazaActividadDTO.getActividadDTO() != null) {
       		Actividad actividad = actividadDao.get(trazaActividadDTO.getActividadDTO().getId());
       		if (actividad == null) {
                throw new PmzException("El Actividad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		trazaActividad.setActividad(actividad);
		}
		
        trazaActividadDao.update(trazaActividad);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TrazaActividadDTO getTrazaActividadDTO(Long id) {
        TrazaActividad trazaActividad = trazaActividadDao.get(id);
        return DTOTransformer.getTrazaActividadDTOFromTrazaActividad(trazaActividad);
    }
}
