package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.DatoAdicionalDaoAPI;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.DatoAdicional;
import co.com.fspb.mgs.service.api.DatoAdicionalServiceAPI;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;

/**
 * Implementación de la Interfaz {@link DatoAdicionalServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicionalServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class DatoAdicionalServiceImpl implements DatoAdicionalServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private DatoAdicionalDaoAPI datoAdicionalDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(DatoAdicionalDTO datoAdicionalDTO) throws PmzException {

        DatoAdicional datoAdicionalValidate = datoAdicionalDao.get(datoAdicionalDTO.getId());

        if (datoAdicionalValidate != null) {
            throw new PmzException("El datoAdicional ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        DatoAdicional datoAdicional = DTOTransformer.getDatoAdicionalFromDatoAdicionalDTO(new DatoAdicional(), datoAdicionalDTO);

		if (datoAdicionalDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(datoAdicionalDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoAdicional.setProyecto(proyecto);
		}
		
        datoAdicionalDao.save(datoAdicional);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<DatoAdicionalDTO> getRecords(PmzPagingCriteria criteria) {
        List<DatoAdicional> listaReturn = datoAdicionalDao.getRecords(criteria);
        Long countTotalRegistros = datoAdicionalDao.countRecords(criteria);

        List<DatoAdicionalDTO> resultList = new ArrayList<DatoAdicionalDTO>();
        for (DatoAdicional datoAdicional : listaReturn) {
            DatoAdicionalDTO datoAdicionalDTO = DTOTransformer
                    .getDatoAdicionalDTOFromDatoAdicional(datoAdicional);
            resultList.add(datoAdicionalDTO);
        }
        
        return new PmzResultSet<DatoAdicionalDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(DatoAdicionalDTO datoAdicionalDTO) throws PmzException{

        DatoAdicional datoAdicional = datoAdicionalDao.get(datoAdicionalDTO.getId());

        if (datoAdicional == null) {
            throw new PmzException("El datoAdicional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getDatoAdicionalFromDatoAdicionalDTO(datoAdicional, datoAdicionalDTO);

		if (datoAdicionalDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(datoAdicionalDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		datoAdicional.setProyecto(proyecto);
		}
		
        datoAdicionalDao.update(datoAdicional);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public DatoAdicionalDTO getDatoAdicionalDTO(Long id) {
        DatoAdicional datoAdicional = datoAdicionalDao.get(id);
        return DTOTransformer.getDatoAdicionalDTOFromDatoAdicional(datoAdicional);
    }
}
