package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI;
import co.com.fspb.mgs.service.api.LiderTerritorioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link LiderTerritorioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioFacadeImpl
 * @date nov 11, 2016
 */
@Service("liderTerritorioFacade")
public class LiderTerritorioFacadeImpl implements LiderTerritorioFacadeAPI {

    @Autowired
    private LiderTerritorioServiceAPI liderTerritorioService;

    /**
     * @see co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<LiderTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        return liderTerritorioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI#guardar()
     */
    @Override
    public void guardar(LiderTerritorioDTO liderTerritorio) throws PmzException {
        liderTerritorioService.guardar(liderTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI#editar()
     */
    @Override
    public void editar(LiderTerritorioDTO liderTerritorio) throws PmzException {
        liderTerritorioService.editar(liderTerritorio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI#getLiderTerritorioDTO()
     */
    @Override
    public LiderTerritorioDTO getLiderTerritorioDTO(TerritorioDTO territorio, LiderDTO lider) {
        return liderTerritorioService.getLiderTerritorioDTO(territorio, lider);
    }

}
