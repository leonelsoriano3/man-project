package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link ProblemaComunidad}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadServiceAPI
 * @date nov 11, 2016
 */
public interface ProblemaComunidadServiceAPI {

    /**
	 * Registra una entidad {@link ProblemaComunidad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param problemaComunidad - {@link ProblemaComunidadDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ProblemaComunidadDTO problemaComunidad) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link ProblemaComunidad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param problemaComunidad - {@link ProblemaComunidadDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ProblemaComunidadDTO problemaComunidad) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ProblemaComunidadDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ProblemaComunidadDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ProblemaComunidadDTO}
     */
    ProblemaComunidadDTO getProblemaComunidadDTO(Long id);

}
