package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ComponenteProyectoDaoAPI;
import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.dao.model.Componente;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.ComponenteProyecto;
import co.com.fspb.mgs.dao.model.ComponenteProyectoPK;
import co.com.fspb.mgs.service.api.ComponenteProyectoServiceAPI;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dto.ComponenteDTO;

/**
 * Implementación de la Interfaz {@link ComponenteProyectoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ComponenteProyectoServiceImpl implements ComponenteProyectoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ComponenteProyectoDaoAPI componenteProyectoDao;
    @Autowired
    private ComponenteDaoAPI componenteDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ComponenteProyectoDTO componenteProyectoDTO) throws PmzException {


		ComponenteProyectoPK pk = new ComponenteProyectoPK();
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), componenteProyectoDTO.getProyectoDTO()));
        pk.setComponente(DTOTransformer.getComponenteFromComponenteDTO(new Componente(), componenteProyectoDTO.getComponenteDTO()));
        ComponenteProyecto componenteProyectoValidate = componenteProyectoDao.get(pk);

        if (componenteProyectoValidate != null) {
            throw new PmzException("El componenteProyecto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ComponenteProyecto componenteProyecto = DTOTransformer.getComponenteProyectoFromComponenteProyectoDTO(new ComponenteProyecto(), componenteProyectoDTO);

		pk = componenteProyecto.getId();
		if (componenteProyectoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(componenteProyectoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
		if (componenteProyectoDTO.getComponenteDTO() != null) {
       		Componente componente = componenteDao.get(componenteProyectoDTO.getComponenteDTO().getId());
       		if (componente == null) {
                throw new PmzException("El Componente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setComponente(componente);
		}
		
        componenteProyecto.setId(pk);
        
        componenteProyectoDao.save(componenteProyecto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ComponenteProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        List<ComponenteProyecto> listaReturn = componenteProyectoDao.getRecords(criteria);
        Long countTotalRegistros = componenteProyectoDao.countRecords(criteria);

        List<ComponenteProyectoDTO> resultList = new ArrayList<ComponenteProyectoDTO>();
        for (ComponenteProyecto componenteProyecto : listaReturn) {
            ComponenteProyectoDTO componenteProyectoDTO = DTOTransformer
                    .getComponenteProyectoDTOFromComponenteProyecto(componenteProyecto);
            resultList.add(componenteProyectoDTO);
        }
        
        return new PmzResultSet<ComponenteProyectoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ComponenteProyectoDTO componenteProyectoDTO) throws PmzException{


		ComponenteProyectoPK pk = new ComponenteProyectoPK();
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), componenteProyectoDTO.getProyectoDTO()));
        pk.setComponente(DTOTransformer.getComponenteFromComponenteDTO(new Componente(), componenteProyectoDTO.getComponenteDTO()));
        ComponenteProyecto componenteProyecto = componenteProyectoDao.get(pk);

        if (componenteProyecto == null) {
            throw new PmzException("El componenteProyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getComponenteProyectoFromComponenteProyectoDTO(componenteProyecto, componenteProyectoDTO);

		pk = componenteProyecto.getId();
		if (componenteProyectoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(componenteProyectoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
		if (componenteProyectoDTO.getComponenteDTO() != null) {
       		Componente componente = componenteDao.get(componenteProyectoDTO.getComponenteDTO().getId());
       		if (componente == null) {
                throw new PmzException("El Componente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setComponente(componente);
		}
		
        componenteProyecto.setId(pk);
        
        componenteProyectoDao.update(componenteProyecto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ComponenteProyectoDTO getComponenteProyectoDTO(ProyectoDTO proyecto, ComponenteDTO componente) {

		ComponenteProyectoPK pk = new ComponenteProyectoPK();
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyecto));
        pk.setComponente(DTOTransformer.getComponenteFromComponenteDTO(new Componente(), componente));
        ComponenteProyecto componenteProyecto = componenteProyectoDao.get(pk);
        return DTOTransformer.getComponenteProyectoDTOFromComponenteProyecto(componenteProyecto);
    }
}
