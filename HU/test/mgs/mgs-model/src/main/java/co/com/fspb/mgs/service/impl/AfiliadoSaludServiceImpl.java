package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.AfiliadoSaludDaoAPI;
import co.com.fspb.mgs.dao.model.AfiliadoSalud;
import co.com.fspb.mgs.service.api.AfiliadoSaludServiceAPI;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;

/**
 * Implementación de la Interfaz {@link AfiliadoSaludServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class AfiliadoSaludServiceImpl implements AfiliadoSaludServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private AfiliadoSaludDaoAPI afiliadoSaludDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(AfiliadoSaludDTO afiliadoSaludDTO) throws PmzException {

        AfiliadoSalud afiliadoSaludValidate = afiliadoSaludDao.get(afiliadoSaludDTO.getId());

        if (afiliadoSaludValidate != null) {
            throw new PmzException("El afiliadoSalud ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        AfiliadoSalud afiliadoSalud = DTOTransformer.getAfiliadoSaludFromAfiliadoSaludDTO(new AfiliadoSalud(), afiliadoSaludDTO);

        afiliadoSaludDao.save(afiliadoSalud);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<AfiliadoSaludDTO> getRecords(PmzPagingCriteria criteria) {
        List<AfiliadoSalud> listaReturn = afiliadoSaludDao.getRecords(criteria);
        Long countTotalRegistros = afiliadoSaludDao.countRecords(criteria);

        List<AfiliadoSaludDTO> resultList = new ArrayList<AfiliadoSaludDTO>();
        for (AfiliadoSalud afiliadoSalud : listaReturn) {
            AfiliadoSaludDTO afiliadoSaludDTO = DTOTransformer
                    .getAfiliadoSaludDTOFromAfiliadoSalud(afiliadoSalud);
            resultList.add(afiliadoSaludDTO);
        }
        
        return new PmzResultSet<AfiliadoSaludDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(AfiliadoSaludDTO afiliadoSaludDTO) throws PmzException{

        AfiliadoSalud afiliadoSalud = afiliadoSaludDao.get(afiliadoSaludDTO.getId());

        if (afiliadoSalud == null) {
            throw new PmzException("El afiliadoSalud no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getAfiliadoSaludFromAfiliadoSaludDTO(afiliadoSalud, afiliadoSaludDTO);

        afiliadoSaludDao.update(afiliadoSalud);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public AfiliadoSaludDTO getAfiliadoSaludDTO(Long id) {
        AfiliadoSalud afiliadoSalud = afiliadoSaludDao.get(id);
        return DTOTransformer.getAfiliadoSaludDTOFromAfiliadoSalud(afiliadoSalud);
    }
}
