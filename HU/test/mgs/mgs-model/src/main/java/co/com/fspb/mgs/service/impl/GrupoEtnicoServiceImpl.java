package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.GrupoEtnicoDaoAPI;
import co.com.fspb.mgs.dao.model.GrupoEtnico;
import co.com.fspb.mgs.service.api.GrupoEtnicoServiceAPI;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;

/**
 * Implementación de la Interfaz {@link GrupoEtnicoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class GrupoEtnicoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class GrupoEtnicoServiceImpl implements GrupoEtnicoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private GrupoEtnicoDaoAPI grupoEtnicoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(GrupoEtnicoDTO grupoEtnicoDTO) throws PmzException {

        GrupoEtnico grupoEtnicoValidate = grupoEtnicoDao.get(grupoEtnicoDTO.getId());

        if (grupoEtnicoValidate != null) {
            throw new PmzException("El grupoEtnico ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        GrupoEtnico grupoEtnico = DTOTransformer.getGrupoEtnicoFromGrupoEtnicoDTO(new GrupoEtnico(), grupoEtnicoDTO);

        grupoEtnicoDao.save(grupoEtnico);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<GrupoEtnicoDTO> getRecords(PmzPagingCriteria criteria) {
        List<GrupoEtnico> listaReturn = grupoEtnicoDao.getRecords(criteria);
        Long countTotalRegistros = grupoEtnicoDao.countRecords(criteria);

        List<GrupoEtnicoDTO> resultList = new ArrayList<GrupoEtnicoDTO>();
        for (GrupoEtnico grupoEtnico : listaReturn) {
            GrupoEtnicoDTO grupoEtnicoDTO = DTOTransformer
                    .getGrupoEtnicoDTOFromGrupoEtnico(grupoEtnico);
            resultList.add(grupoEtnicoDTO);
        }
        
        return new PmzResultSet<GrupoEtnicoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(GrupoEtnicoDTO grupoEtnicoDTO) throws PmzException{

        GrupoEtnico grupoEtnico = grupoEtnicoDao.get(grupoEtnicoDTO.getId());

        if (grupoEtnico == null) {
            throw new PmzException("El grupoEtnico no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getGrupoEtnicoFromGrupoEtnicoDTO(grupoEtnico, grupoEtnicoDTO);

        grupoEtnicoDao.update(grupoEtnico);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public GrupoEtnicoDTO getGrupoEtnicoDTO(Long id) {
        GrupoEtnico grupoEtnico = grupoEtnicoDao.get(id);
        return DTOTransformer.getGrupoEtnicoDTOFromGrupoEtnico(grupoEtnico);
    }
}
