package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ProyectoTerritorioDaoAPI;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.ProyectoTerritorio;
import co.com.fspb.mgs.dao.model.ProyectoTerritorioPK;
import co.com.fspb.mgs.service.api.ProyectoTerritorioServiceAPI;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;

/**
 * Implementación de la Interfaz {@link ProyectoTerritorioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ProyectoTerritorioServiceImpl implements ProyectoTerritorioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ProyectoTerritorioDaoAPI proyectoTerritorioDao;
    @Autowired
    private TerritorioDaoAPI territorioDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ProyectoTerritorioDTO proyectoTerritorioDTO) throws PmzException {


		ProyectoTerritorioPK pk = new ProyectoTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), proyectoTerritorioDTO.getTerritorioDTO()));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyectoTerritorioDTO.getProyectoDTO()));
        ProyectoTerritorio proyectoTerritorioValidate = proyectoTerritorioDao.get(pk);

        if (proyectoTerritorioValidate != null) {
            throw new PmzException("El proyectoTerritorio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ProyectoTerritorio proyectoTerritorio = DTOTransformer.getProyectoTerritorioFromProyectoTerritorioDTO(new ProyectoTerritorio(), proyectoTerritorioDTO);

		pk = proyectoTerritorio.getId();
		if (proyectoTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(proyectoTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (proyectoTerritorioDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(proyectoTerritorioDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
        proyectoTerritorio.setId(pk);
        
        proyectoTerritorioDao.save(proyectoTerritorio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        List<ProyectoTerritorio> listaReturn = proyectoTerritorioDao.getRecords(criteria);
        Long countTotalRegistros = proyectoTerritorioDao.countRecords(criteria);

        List<ProyectoTerritorioDTO> resultList = new ArrayList<ProyectoTerritorioDTO>();
        for (ProyectoTerritorio proyectoTerritorio : listaReturn) {
            ProyectoTerritorioDTO proyectoTerritorioDTO = DTOTransformer
                    .getProyectoTerritorioDTOFromProyectoTerritorio(proyectoTerritorio);
            resultList.add(proyectoTerritorioDTO);
        }
        
        return new PmzResultSet<ProyectoTerritorioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ProyectoTerritorioDTO proyectoTerritorioDTO) throws PmzException{


		ProyectoTerritorioPK pk = new ProyectoTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), proyectoTerritorioDTO.getTerritorioDTO()));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyectoTerritorioDTO.getProyectoDTO()));
        ProyectoTerritorio proyectoTerritorio = proyectoTerritorioDao.get(pk);

        if (proyectoTerritorio == null) {
            throw new PmzException("El proyectoTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getProyectoTerritorioFromProyectoTerritorioDTO(proyectoTerritorio, proyectoTerritorioDTO);

		pk = proyectoTerritorio.getId();
		if (proyectoTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(proyectoTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (proyectoTerritorioDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(proyectoTerritorioDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setProyecto(proyecto);
		}
		
        proyectoTerritorio.setId(pk);
        
        proyectoTerritorioDao.update(proyectoTerritorio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ProyectoTerritorioDTO getProyectoTerritorioDTO(TerritorioDTO territorio, ProyectoDTO proyecto) {

		ProyectoTerritorioPK pk = new ProyectoTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), territorio));
        pk.setProyecto(DTOTransformer.getProyectoFromProyectoDTO(new Proyecto(), proyecto));
        ProyectoTerritorio proyectoTerritorio = proyectoTerritorioDao.get(pk);
        return DTOTransformer.getProyectoTerritorioDTOFromProyectoTerritorio(proyectoTerritorio);
    }
}
