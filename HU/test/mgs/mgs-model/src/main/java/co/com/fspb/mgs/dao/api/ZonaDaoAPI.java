package co.com.fspb.mgs.dao.api;

import java.util.List;

import com.premize.pmz.dao.api.Dao;

import co.com.fspb.mgs.dao.model.Zona;
import com.premize.pmz.api.dto.PmzPagingCriteria;

/**
 * Interfaz de Data Access Object (DAO) para la entidad {@link Zona}
 * Extiende la interfaz de PMZ {@link Dao}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaDaoAPI
 * @date nov 11, 2016
 */
public interface ZonaDaoAPI extends Dao<Zona, Long> {

    /**
	 * Retorna la lista paginada y filtrada según el {@link PmzPagingCriteria}
	 * de la entidad {@link Zona}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public List<Zona> getRecords(PmzPagingCriteria criteria);
    
    /**
	 * Retorna el total de regirtos según el {@link PmzPagingCriteria}
	 * de la entidad {@link Zona}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public Long countRecords(PmzPagingCriteria criteria);

}
