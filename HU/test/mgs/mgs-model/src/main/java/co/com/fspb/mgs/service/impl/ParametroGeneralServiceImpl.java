package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ParametroGeneralDaoAPI;
import co.com.fspb.mgs.dao.model.ParametroGeneral;
import co.com.fspb.mgs.service.api.ParametroGeneralServiceAPI;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;

/**
 * Implementación de la Interfaz {@link ParametroGeneralServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneralServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ParametroGeneralServiceImpl implements ParametroGeneralServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ParametroGeneralDaoAPI parametroGeneralDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ParametroGeneralDTO parametroGeneralDTO) throws PmzException {

        ParametroGeneral parametroGeneralValidate = parametroGeneralDao.get(parametroGeneralDTO.getId());

        if (parametroGeneralValidate != null) {
            throw new PmzException("El parametroGeneral ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ParametroGeneral parametroGeneral = DTOTransformer.getParametroGeneralFromParametroGeneralDTO(new ParametroGeneral(), parametroGeneralDTO);

        parametroGeneralDao.save(parametroGeneral);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParametroGeneralDTO> getRecords(PmzPagingCriteria criteria) {
        List<ParametroGeneral> listaReturn = parametroGeneralDao.getRecords(criteria);
        Long countTotalRegistros = parametroGeneralDao.countRecords(criteria);

        List<ParametroGeneralDTO> resultList = new ArrayList<ParametroGeneralDTO>();
        for (ParametroGeneral parametroGeneral : listaReturn) {
            ParametroGeneralDTO parametroGeneralDTO = DTOTransformer
                    .getParametroGeneralDTOFromParametroGeneral(parametroGeneral);
            resultList.add(parametroGeneralDTO);
        }
        
        return new PmzResultSet<ParametroGeneralDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ParametroGeneralDTO parametroGeneralDTO) throws PmzException{

        ParametroGeneral parametroGeneral = parametroGeneralDao.get(parametroGeneralDTO.getId());

        if (parametroGeneral == null) {
            throw new PmzException("El parametroGeneral no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getParametroGeneralFromParametroGeneralDTO(parametroGeneral, parametroGeneralDTO);

        parametroGeneralDao.update(parametroGeneral);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ParametroGeneralDTO getParametroGeneralDTO(Long id) {
        ParametroGeneral parametroGeneral = parametroGeneralDao.get(id);
        return DTOTransformer.getParametroGeneralDTOFromParametroGeneral(parametroGeneral);
    }
}
