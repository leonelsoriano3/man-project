package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.facade.api.ProyectoFacadeAPI;
import co.com.fspb.mgs.service.api.ProyectoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ProyectoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoFacadeImpl
 * @date nov 11, 2016
 */
@Service("proyectoFacade")
public class ProyectoFacadeImpl implements ProyectoFacadeAPI {

    @Autowired
    private ProyectoServiceAPI proyectoService;

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        return proyectoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoFacadeAPI#guardar()
     */
    @Override
    public void guardar(ProyectoDTO proyecto) throws PmzException {
        proyectoService.guardar(proyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoFacadeAPI#editar()
     */
    @Override
    public void editar(ProyectoDTO proyecto) throws PmzException {
        proyectoService.editar(proyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoFacadeAPI#getProyectoDTO()
     */
    @Override
    public ProyectoDTO getProyectoDTO(Long id) {
        return proyectoService.getProyectoDTO(id);
    }

}
