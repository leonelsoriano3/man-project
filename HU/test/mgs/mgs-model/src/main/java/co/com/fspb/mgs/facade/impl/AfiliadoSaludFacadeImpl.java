package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI;
import co.com.fspb.mgs.service.api.AfiliadoSaludServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link AfiliadoSaludFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AfiliadoSaludFacadeImpl
 * @date nov 11, 2016
 */
@Service("afiliadoSaludFacade")
public class AfiliadoSaludFacadeImpl implements AfiliadoSaludFacadeAPI {

    @Autowired
    private AfiliadoSaludServiceAPI afiliadoSaludService;

    /**
     * @see co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<AfiliadoSaludDTO> getRecords(PmzPagingCriteria criteria) {
        return afiliadoSaludService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI#guardar()
     */
    @Override
    public void guardar(AfiliadoSaludDTO afiliadoSalud) throws PmzException {
        afiliadoSaludService.guardar(afiliadoSalud);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI#editar()
     */
    @Override
    public void editar(AfiliadoSaludDTO afiliadoSalud) throws PmzException {
        afiliadoSaludService.editar(afiliadoSalud);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AfiliadoSaludFacadeAPI#getAfiliadoSaludDTO()
     */
    @Override
    public AfiliadoSaludDTO getAfiliadoSaludDTO(Long id) {
        return afiliadoSaludService.getAfiliadoSaludDTO(id);
    }

}
