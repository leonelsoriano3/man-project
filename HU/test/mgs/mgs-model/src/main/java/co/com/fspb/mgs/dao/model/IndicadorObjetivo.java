package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_indicador_objetivo en la BD a la entidad IndicadorObjetivo.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivo
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_indicador_objetivo" )
public class IndicadorObjetivo implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private IndicadorObjetivoPK id; 



	@Column(name = "resultado", nullable = false, length = 20)
    private String resultado; 

	@Column(name = "meta", nullable = false, length = 20)
    private String meta; 
 
    public IndicadorObjetivoPK getId() {
        return id;
    }

    public void setId(IndicadorObjetivoPK id) {
        this.id = id;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

}
