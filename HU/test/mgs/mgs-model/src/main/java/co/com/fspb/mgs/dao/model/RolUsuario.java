package co.com.fspb.mgs.dao.model;


import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_rol_usuario en la BD a la entidad RolUsuario.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuario
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_rol_usuario" )
public class RolUsuario implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private RolUsuarioPK id; 


 
    public RolUsuarioPK getId() {
        return id;
    }

    public void setId(RolUsuarioPK id) {
        this.id = id;
    }

}
