package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI;
import co.com.fspb.mgs.service.api.DatoContactoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link DatoContactoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoContactoFacadeImpl
 * @date nov 11, 2016
 */
@Service("datoContactoFacade")
public class DatoContactoFacadeImpl implements DatoContactoFacadeAPI {

    @Autowired
    private DatoContactoServiceAPI datoContactoService;

    /**
     * @see co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<DatoContactoDTO> getRecords(PmzPagingCriteria criteria) {
        return datoContactoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI#guardar()
     */
    @Override
    public void guardar(DatoContactoDTO datoContacto) throws PmzException {
        datoContactoService.guardar(datoContacto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI#editar()
     */
    @Override
    public void editar(DatoContactoDTO datoContacto) throws PmzException {
        datoContactoService.editar(datoContacto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI#getDatoContactoDTO()
     */
    @Override
    public DatoContactoDTO getDatoContactoDTO(Long id) {
        return datoContactoService.getDatoContactoDTO(id);
    }

}
