package co.com.fspb.mgs.dao.model;


import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyectoPK
 * @date nov 11, 2016
 */
@Embeddable
public class IndicadorProyectoPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_indicador", nullable = true)
    private Indicador indicador; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_proyecto", nullable = true)
    private Proyecto proyecto; 
 
 
    public Indicador getIndicador() {
        return indicador;
    }

    public void setIndicador(Indicador indicador) {
        this.indicador = indicador;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

}
