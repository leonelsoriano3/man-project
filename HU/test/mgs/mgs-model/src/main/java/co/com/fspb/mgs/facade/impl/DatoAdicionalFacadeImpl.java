package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI;
import co.com.fspb.mgs.service.api.DatoAdicionalServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link DatoAdicionalFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicionalFacadeImpl
 * @date nov 11, 2016
 */
@Service("datoAdicionalFacade")
public class DatoAdicionalFacadeImpl implements DatoAdicionalFacadeAPI {

    @Autowired
    private DatoAdicionalServiceAPI datoAdicionalService;

    /**
     * @see co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<DatoAdicionalDTO> getRecords(PmzPagingCriteria criteria) {
        return datoAdicionalService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI#guardar()
     */
    @Override
    public void guardar(DatoAdicionalDTO datoAdicional) throws PmzException {
        datoAdicionalService.guardar(datoAdicional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI#editar()
     */
    @Override
    public void editar(DatoAdicionalDTO datoAdicional) throws PmzException {
        datoAdicionalService.editar(datoAdicional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DatoAdicionalFacadeAPI#getDatoAdicionalDTO()
     */
    @Override
    public DatoAdicionalDTO getDatoAdicionalDTO(Long id) {
        return datoAdicionalService.getDatoAdicionalDTO(id);
    }

}
