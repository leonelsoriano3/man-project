package co.com.fspb.mgs.dao.model;


import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoPK
 * @date nov 11, 2016
 */
@Embeddable
public class IndicadorObjetivoPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_indicador", nullable = true)
    private Indicador indicador; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_objetivo", nullable = true)
    private Objetivo objetivo; 
 
 
    public Indicador getIndicador() {
        return indicador;
    }

    public void setIndicador(Indicador indicador) {
        this.indicador = indicador;
    }

    public Objetivo getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Objetivo objetivo) {
        this.objetivo = objetivo;
    }

}
