package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.RolDaoAPI;
import co.com.fspb.mgs.dao.model.Rol;
import co.com.fspb.mgs.service.api.RolServiceAPI;
import co.com.fspb.mgs.dto.RolDTO;

/**
 * Implementación de la Interfaz {@link RolServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class RolServiceImpl implements RolServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private RolDaoAPI rolDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(RolDTO rolDTO) throws PmzException {

        Rol rolValidate = rolDao.get(rolDTO.getId());

        if (rolValidate != null) {
            throw new PmzException("El rol ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Rol rol = DTOTransformer.getRolFromRolDTO(new Rol(), rolDTO);

        rolDao.save(rol);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolDTO> getRecords(PmzPagingCriteria criteria) {
        List<Rol> listaReturn = rolDao.getRecords(criteria);
        Long countTotalRegistros = rolDao.countRecords(criteria);

        List<RolDTO> resultList = new ArrayList<RolDTO>();
        for (Rol rol : listaReturn) {
            RolDTO rolDTO = DTOTransformer
                    .getRolDTOFromRol(rol);
            resultList.add(rolDTO);
        }
        
        return new PmzResultSet<RolDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(RolDTO rolDTO) throws PmzException{

        Rol rol = rolDao.get(rolDTO.getId());

        if (rol == null) {
            throw new PmzException("El rol no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getRolFromRolDTO(rol, rolDTO);

        rolDao.update(rol);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public RolDTO getRolDTO(Long id) {
        Rol rol = rolDao.get(id);
        return DTOTransformer.getRolDTOFromRol(rol);
    }
}
