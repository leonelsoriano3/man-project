package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_proyecto_beneficiario en la BD a la entidad ProyectoBeneficiario.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiario
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_proyecto_beneficiario" )
public class ProyectoBeneficiario implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_proy_beneficiario", nullable = false)
	private Integer id; 


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_beneficiario", nullable = true)
    private Beneficiario beneficiario; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proyecto", nullable = true)
    private Proyecto proyecto; 
 
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

}
