package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.facade.api.IndicadorFacadeAPI;
import co.com.fspb.mgs.service.api.IndicadorServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link IndicadorFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorFacadeImpl
 * @date nov 11, 2016
 */
@Service("indicadorFacade")
public class IndicadorFacadeImpl implements IndicadorFacadeAPI {

    @Autowired
    private IndicadorServiceAPI indicadorService;

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorDTO> getRecords(PmzPagingCriteria criteria) {
        return indicadorService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.IndicadorFacadeAPI#guardar()
     */
    @Override
    public void guardar(IndicadorDTO indicador) throws PmzException {
        indicadorService.guardar(indicador);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorFacadeAPI#editar()
     */
    @Override
    public void editar(IndicadorDTO indicador) throws PmzException {
        indicadorService.editar(indicador);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.IndicadorFacadeAPI#getIndicadorDTO()
     */
    @Override
    public IndicadorDTO getIndicadorDTO(Long id) {
        return indicadorService.getIndicadorDTO(id);
    }

}
