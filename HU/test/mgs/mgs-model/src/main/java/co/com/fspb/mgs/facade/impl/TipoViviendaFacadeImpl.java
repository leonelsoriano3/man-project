package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI;
import co.com.fspb.mgs.service.api.TipoViviendaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TipoViviendaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoViviendaFacadeImpl
 * @date nov 11, 2016
 */
@Service("tipoViviendaFacade")
public class TipoViviendaFacadeImpl implements TipoViviendaFacadeAPI {

    @Autowired
    private TipoViviendaServiceAPI tipoViviendaService;

    /**
     * @see co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoViviendaDTO> getRecords(PmzPagingCriteria criteria) {
        return tipoViviendaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI#guardar()
     */
    @Override
    public void guardar(TipoViviendaDTO tipoVivienda) throws PmzException {
        tipoViviendaService.guardar(tipoVivienda);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI#editar()
     */
    @Override
    public void editar(TipoViviendaDTO tipoVivienda) throws PmzException {
        tipoViviendaService.editar(tipoVivienda);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TipoViviendaFacadeAPI#getTipoViviendaDTO()
     */
    @Override
    public TipoViviendaDTO getTipoViviendaDTO(Long id) {
        return tipoViviendaService.getTipoViviendaDTO(id);
    }

}
