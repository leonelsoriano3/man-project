package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.RolUsuarioDaoAPI;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dao.api.RolDaoAPI;
import co.com.fspb.mgs.dao.model.Rol;
import co.com.fspb.mgs.dao.model.RolUsuario;
import co.com.fspb.mgs.dao.model.RolUsuarioPK;
import co.com.fspb.mgs.service.api.RolUsuarioServiceAPI;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dto.UsuarioDTO;

/**
 * Implementación de la Interfaz {@link RolUsuarioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class RolUsuarioServiceImpl implements RolUsuarioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private RolUsuarioDaoAPI rolUsuarioDao;
    @Autowired
    private UsuarioDaoAPI usuarioDao;
    @Autowired
    private RolDaoAPI rolDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(RolUsuarioDTO rolUsuarioDTO) throws PmzException {


		RolUsuarioPK pk = new RolUsuarioPK();
        pk.setRol(DTOTransformer.getRolFromRolDTO(new Rol(), rolUsuarioDTO.getRolDTO()));
        pk.setUsuario(DTOTransformer.getUsuarioFromUsuarioDTO(new Usuario(), rolUsuarioDTO.getUsuarioDTO()));
        RolUsuario rolUsuarioValidate = rolUsuarioDao.get(pk);

        if (rolUsuarioValidate != null) {
            throw new PmzException("El rolUsuario ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        RolUsuario rolUsuario = DTOTransformer.getRolUsuarioFromRolUsuarioDTO(new RolUsuario(), rolUsuarioDTO);

		pk = rolUsuario.getId();
		if (rolUsuarioDTO.getRolDTO() != null) {
       		Rol rol = rolDao.get(rolUsuarioDTO.getRolDTO().getId());
       		if (rol == null) {
                throw new PmzException("El Rol no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setRol(rol);
		}
		
		if (rolUsuarioDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(rolUsuarioDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setUsuario(usuario);
		}
		
        rolUsuario.setId(pk);
        
        rolUsuarioDao.save(rolUsuario);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolUsuarioDTO> getRecords(PmzPagingCriteria criteria) {
        List<RolUsuario> listaReturn = rolUsuarioDao.getRecords(criteria);
        Long countTotalRegistros = rolUsuarioDao.countRecords(criteria);

        List<RolUsuarioDTO> resultList = new ArrayList<RolUsuarioDTO>();
        for (RolUsuario rolUsuario : listaReturn) {
            RolUsuarioDTO rolUsuarioDTO = DTOTransformer
                    .getRolUsuarioDTOFromRolUsuario(rolUsuario);
            resultList.add(rolUsuarioDTO);
        }
        
        return new PmzResultSet<RolUsuarioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(RolUsuarioDTO rolUsuarioDTO) throws PmzException{


		RolUsuarioPK pk = new RolUsuarioPK();
        pk.setRol(DTOTransformer.getRolFromRolDTO(new Rol(), rolUsuarioDTO.getRolDTO()));
        pk.setUsuario(DTOTransformer.getUsuarioFromUsuarioDTO(new Usuario(), rolUsuarioDTO.getUsuarioDTO()));
        RolUsuario rolUsuario = rolUsuarioDao.get(pk);

        if (rolUsuario == null) {
            throw new PmzException("El rolUsuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getRolUsuarioFromRolUsuarioDTO(rolUsuario, rolUsuarioDTO);

		pk = rolUsuario.getId();
		if (rolUsuarioDTO.getRolDTO() != null) {
       		Rol rol = rolDao.get(rolUsuarioDTO.getRolDTO().getId());
       		if (rol == null) {
                throw new PmzException("El Rol no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setRol(rol);
		}
		
		if (rolUsuarioDTO.getUsuarioDTO() != null) {
       		Usuario usuario = usuarioDao.get(rolUsuarioDTO.getUsuarioDTO().getId());
       		if (usuario == null) {
                throw new PmzException("El Usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setUsuario(usuario);
		}
		
        rolUsuario.setId(pk);
        
        rolUsuarioDao.update(rolUsuario);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public RolUsuarioDTO getRolUsuarioDTO(RolDTO rol, UsuarioDTO usuario) {

		RolUsuarioPK pk = new RolUsuarioPK();
        pk.setRol(DTOTransformer.getRolFromRolDTO(new Rol(), rol));
        pk.setUsuario(DTOTransformer.getUsuarioFromUsuarioDTO(new Usuario(), usuario));
        RolUsuario rolUsuario = rolUsuarioDao.get(pk);
        return DTOTransformer.getRolUsuarioDTOFromRolUsuario(rolUsuario);
    }
}
