package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI;
import co.com.fspb.mgs.service.api.NivelEducativoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link NivelEducativoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoFacadeImpl
 * @date nov 11, 2016
 */
@Service("nivelEducativoFacade")
public class NivelEducativoFacadeImpl implements NivelEducativoFacadeAPI {

    @Autowired
    private NivelEducativoServiceAPI nivelEducativoService;

    /**
     * @see co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<NivelEducativoDTO> getRecords(PmzPagingCriteria criteria) {
        return nivelEducativoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI#guardar()
     */
    @Override
    public void guardar(NivelEducativoDTO nivelEducativo) throws PmzException {
        nivelEducativoService.guardar(nivelEducativo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI#editar()
     */
    @Override
    public void editar(NivelEducativoDTO nivelEducativo) throws PmzException {
        nivelEducativoService.editar(nivelEducativo);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI#getNivelEducativoDTO()
     */
    @Override
    public NivelEducativoDTO getNivelEducativoDTO(Long id) {
        return nivelEducativoService.getNivelEducativoDTO(id);
    }

}
