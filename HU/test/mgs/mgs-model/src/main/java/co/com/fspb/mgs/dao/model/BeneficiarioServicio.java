package co.com.fspb.mgs.dao.model;


import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_beneficiario_servicio en la BD a la entidad BeneficiarioServicio.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicio
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_beneficiario_servicio" )
public class BeneficiarioServicio implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private BeneficiarioServicioPK id; 


 
    public BeneficiarioServicioPK getId() {
        return id;
    }

    public void setId(BeneficiarioServicioPK id) {
        this.id = id;
    }

}
