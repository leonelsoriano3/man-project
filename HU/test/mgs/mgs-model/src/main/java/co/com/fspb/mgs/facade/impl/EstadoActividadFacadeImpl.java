package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI;
import co.com.fspb.mgs.service.api.EstadoActividadServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EstadoActividadFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoActividadFacadeImpl
 * @date nov 11, 2016
 */
@Service("estadoActividadFacade")
public class EstadoActividadFacadeImpl implements EstadoActividadFacadeAPI {

    @Autowired
    private EstadoActividadServiceAPI estadoActividadService;

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoActividadDTO> getRecords(PmzPagingCriteria criteria) {
        return estadoActividadService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI#guardar()
     */
    @Override
    public void guardar(EstadoActividadDTO estadoActividad) throws PmzException {
        estadoActividadService.guardar(estadoActividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI#editar()
     */
    @Override
    public void editar(EstadoActividadDTO estadoActividad) throws PmzException {
        estadoActividadService.editar(estadoActividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstadoActividadFacadeAPI#getEstadoActividadDTO()
     */
    @Override
    public EstadoActividadDTO getEstadoActividadDTO(Long id) {
        return estadoActividadService.getEstadoActividadDTO(id);
    }

}
