package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.DocumentoAdjuntoDaoAPI;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.DocumentoAdjunto;
import co.com.fspb.mgs.service.api.DocumentoAdjuntoServiceAPI;
import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;

/**
 * Implementación de la Interfaz {@link DocumentoAdjuntoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DocumentoAdjuntoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class DocumentoAdjuntoServiceImpl implements DocumentoAdjuntoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private DocumentoAdjuntoDaoAPI documentoAdjuntoDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(DocumentoAdjuntoDTO documentoAdjuntoDTO) throws PmzException {

        DocumentoAdjunto documentoAdjuntoValidate = documentoAdjuntoDao.get(documentoAdjuntoDTO.getId());

        if (documentoAdjuntoValidate != null) {
            throw new PmzException("El documentoAdjunto ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        DocumentoAdjunto documentoAdjunto = DTOTransformer.getDocumentoAdjuntoFromDocumentoAdjuntoDTO(new DocumentoAdjunto(), documentoAdjuntoDTO);

		if (documentoAdjuntoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(documentoAdjuntoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		documentoAdjunto.setProyecto(proyecto);
		}
		
        documentoAdjuntoDao.save(documentoAdjunto);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<DocumentoAdjuntoDTO> getRecords(PmzPagingCriteria criteria) {
        List<DocumentoAdjunto> listaReturn = documentoAdjuntoDao.getRecords(criteria);
        Long countTotalRegistros = documentoAdjuntoDao.countRecords(criteria);

        List<DocumentoAdjuntoDTO> resultList = new ArrayList<DocumentoAdjuntoDTO>();
        for (DocumentoAdjunto documentoAdjunto : listaReturn) {
            DocumentoAdjuntoDTO documentoAdjuntoDTO = DTOTransformer
                    .getDocumentoAdjuntoDTOFromDocumentoAdjunto(documentoAdjunto);
            resultList.add(documentoAdjuntoDTO);
        }
        
        return new PmzResultSet<DocumentoAdjuntoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(DocumentoAdjuntoDTO documentoAdjuntoDTO) throws PmzException{

        DocumentoAdjunto documentoAdjunto = documentoAdjuntoDao.get(documentoAdjuntoDTO.getId());

        if (documentoAdjunto == null) {
            throw new PmzException("El documentoAdjunto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getDocumentoAdjuntoFromDocumentoAdjuntoDTO(documentoAdjunto, documentoAdjuntoDTO);

		if (documentoAdjuntoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(documentoAdjuntoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		documentoAdjunto.setProyecto(proyecto);
		}
		
        documentoAdjuntoDao.update(documentoAdjunto);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public DocumentoAdjuntoDTO getDocumentoAdjuntoDTO(Long id) {
        DocumentoAdjunto documentoAdjunto = documentoAdjuntoDao.get(id);
        return DTOTransformer.getDocumentoAdjuntoDTOFromDocumentoAdjunto(documentoAdjunto);
    }
}
