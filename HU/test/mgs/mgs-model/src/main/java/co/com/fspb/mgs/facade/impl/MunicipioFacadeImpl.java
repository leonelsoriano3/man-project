package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.facade.api.MunicipioFacadeAPI;
import co.com.fspb.mgs.service.api.MunicipioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link MunicipioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioFacadeImpl
 * @date nov 11, 2016
 */
@Service("municipioFacade")
public class MunicipioFacadeImpl implements MunicipioFacadeAPI {

    @Autowired
    private MunicipioServiceAPI municipioService;

    /**
     * @see co.com.fspb.mgs.facade.api.MunicipioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<MunicipioDTO> getRecords(PmzPagingCriteria criteria) {
        return municipioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.MunicipioFacadeAPI#guardar()
     */
    @Override
    public void guardar(MunicipioDTO municipio) throws PmzException {
        municipioService.guardar(municipio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.MunicipioFacadeAPI#editar()
     */
    @Override
    public void editar(MunicipioDTO municipio) throws PmzException {
        municipioService.editar(municipio);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.MunicipioFacadeAPI#getMunicipioDTO()
     */
    @Override
    public MunicipioDTO getMunicipioDTO(Long id) {
        return municipioService.getMunicipioDTO(id);
    }

}
