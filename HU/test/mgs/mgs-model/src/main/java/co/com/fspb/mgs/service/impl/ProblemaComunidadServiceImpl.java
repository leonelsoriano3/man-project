package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ProblemaComunidadDaoAPI;
import co.com.fspb.mgs.dao.model.ProblemaComunidad;
import co.com.fspb.mgs.service.api.ProblemaComunidadServiceAPI;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;

/**
 * Implementación de la Interfaz {@link ProblemaComunidadServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ProblemaComunidadServiceImpl implements ProblemaComunidadServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ProblemaComunidadDaoAPI problemaComunidadDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ProblemaComunidadDTO problemaComunidadDTO) throws PmzException {

        ProblemaComunidad problemaComunidadValidate = problemaComunidadDao.get(problemaComunidadDTO.getId());

        if (problemaComunidadValidate != null) {
            throw new PmzException("El problemaComunidad ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ProblemaComunidad problemaComunidad = DTOTransformer.getProblemaComunidadFromProblemaComunidadDTO(new ProblemaComunidad(), problemaComunidadDTO);

        problemaComunidadDao.save(problemaComunidad);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProblemaComunidadDTO> getRecords(PmzPagingCriteria criteria) {
        List<ProblemaComunidad> listaReturn = problemaComunidadDao.getRecords(criteria);
        Long countTotalRegistros = problemaComunidadDao.countRecords(criteria);

        List<ProblemaComunidadDTO> resultList = new ArrayList<ProblemaComunidadDTO>();
        for (ProblemaComunidad problemaComunidad : listaReturn) {
            ProblemaComunidadDTO problemaComunidadDTO = DTOTransformer
                    .getProblemaComunidadDTOFromProblemaComunidad(problemaComunidad);
            resultList.add(problemaComunidadDTO);
        }
        
        return new PmzResultSet<ProblemaComunidadDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ProblemaComunidadDTO problemaComunidadDTO) throws PmzException{

        ProblemaComunidad problemaComunidad = problemaComunidadDao.get(problemaComunidadDTO.getId());

        if (problemaComunidad == null) {
            throw new PmzException("El problemaComunidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getProblemaComunidadFromProblemaComunidadDTO(problemaComunidad, problemaComunidadDTO);

        problemaComunidadDao.update(problemaComunidad);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ProblemaComunidadDTO getProblemaComunidadDTO(Long id) {
        ProblemaComunidad problemaComunidad = problemaComunidadDao.get(id);
        return DTOTransformer.getProblemaComunidadDTOFromProblemaComunidad(problemaComunidad);
    }
}
