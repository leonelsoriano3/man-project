package co.com.fspb.mgs.dao.model;


import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_proyecto_territorio en la BD a la entidad ProyectoTerritorio.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorio
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_proyecto_territorio" )
public class ProyectoTerritorio implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private ProyectoTerritorioPK id; 


 
    public ProyectoTerritorioPK getId() {
        return id;
    }

    public void setId(ProyectoTerritorioPK id) {
        this.id = id;
    }

}
