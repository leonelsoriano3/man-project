package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ValorDatoAdicionalDaoAPI;
import co.com.fspb.mgs.dao.api.DatoAdicionalDaoAPI;
import co.com.fspb.mgs.dao.model.DatoAdicional;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dao.model.ValorDatoAdicional;
import co.com.fspb.mgs.service.api.ValorDatoAdicionalServiceAPI;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;

/**
 * Implementación de la Interfaz {@link ValorDatoAdicionalServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ValorDatoAdicionalServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ValorDatoAdicionalServiceImpl implements ValorDatoAdicionalServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ValorDatoAdicionalDaoAPI valorDatoAdicionalDao;
    @Autowired
    private DatoAdicionalDaoAPI datoAdicionalDao;
    @Autowired
    private BeneficiarioDaoAPI beneficiarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ValorDatoAdicionalDTO valorDatoAdicionalDTO) throws PmzException {

        ValorDatoAdicional valorDatoAdicionalValidate = valorDatoAdicionalDao.get(valorDatoAdicionalDTO.getId());

        if (valorDatoAdicionalValidate != null) {
            throw new PmzException("El valorDatoAdicional ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ValorDatoAdicional valorDatoAdicional = DTOTransformer.getValorDatoAdicionalFromValorDatoAdicionalDTO(new ValorDatoAdicional(), valorDatoAdicionalDTO);

		if (valorDatoAdicionalDTO.getDatoAdicionalDTO() != null) {
       		DatoAdicional datoAdicional = datoAdicionalDao.get(valorDatoAdicionalDTO.getDatoAdicionalDTO().getId());
       		if (datoAdicional == null) {
                throw new PmzException("El DatoAdicional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		valorDatoAdicional.setDatoAdicional(datoAdicional);
		}
		
		if (valorDatoAdicionalDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(valorDatoAdicionalDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		valorDatoAdicional.setBeneficiario(beneficiario);
		}
		
        valorDatoAdicionalDao.save(valorDatoAdicional);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ValorDatoAdicionalDTO> getRecords(PmzPagingCriteria criteria) {
        List<ValorDatoAdicional> listaReturn = valorDatoAdicionalDao.getRecords(criteria);
        Long countTotalRegistros = valorDatoAdicionalDao.countRecords(criteria);

        List<ValorDatoAdicionalDTO> resultList = new ArrayList<ValorDatoAdicionalDTO>();
        for (ValorDatoAdicional valorDatoAdicional : listaReturn) {
            ValorDatoAdicionalDTO valorDatoAdicionalDTO = DTOTransformer
                    .getValorDatoAdicionalDTOFromValorDatoAdicional(valorDatoAdicional);
            resultList.add(valorDatoAdicionalDTO);
        }
        
        return new PmzResultSet<ValorDatoAdicionalDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ValorDatoAdicionalDTO valorDatoAdicionalDTO) throws PmzException{

        ValorDatoAdicional valorDatoAdicional = valorDatoAdicionalDao.get(valorDatoAdicionalDTO.getId());

        if (valorDatoAdicional == null) {
            throw new PmzException("El valorDatoAdicional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getValorDatoAdicionalFromValorDatoAdicionalDTO(valorDatoAdicional, valorDatoAdicionalDTO);

		if (valorDatoAdicionalDTO.getDatoAdicionalDTO() != null) {
       		DatoAdicional datoAdicional = datoAdicionalDao.get(valorDatoAdicionalDTO.getDatoAdicionalDTO().getId());
       		if (datoAdicional == null) {
                throw new PmzException("El DatoAdicional no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		valorDatoAdicional.setDatoAdicional(datoAdicional);
		}
		
		if (valorDatoAdicionalDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(valorDatoAdicionalDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		valorDatoAdicional.setBeneficiario(beneficiario);
		}
		
        valorDatoAdicionalDao.update(valorDatoAdicional);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ValorDatoAdicionalDTO getValorDatoAdicionalDTO(Long id) {
        ValorDatoAdicional valorDatoAdicional = valorDatoAdicionalDao.get(id);
        return DTOTransformer.getValorDatoAdicionalDTOFromValorDatoAdicional(valorDatoAdicional);
    }
}
