package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI;
import co.com.fspb.mgs.service.api.ValorDatoAdicionalServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ValorDatoAdicionalFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ValorDatoAdicionalFacadeImpl
 * @date nov 11, 2016
 */
@Service("valorDatoAdicionalFacade")
public class ValorDatoAdicionalFacadeImpl implements ValorDatoAdicionalFacadeAPI {

    @Autowired
    private ValorDatoAdicionalServiceAPI valorDatoAdicionalService;

    /**
     * @see co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ValorDatoAdicionalDTO> getRecords(PmzPagingCriteria criteria) {
        return valorDatoAdicionalService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI#guardar()
     */
    @Override
    public void guardar(ValorDatoAdicionalDTO valorDatoAdicional) throws PmzException {
        valorDatoAdicionalService.guardar(valorDatoAdicional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI#editar()
     */
    @Override
    public void editar(ValorDatoAdicionalDTO valorDatoAdicional) throws PmzException {
        valorDatoAdicionalService.editar(valorDatoAdicional);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ValorDatoAdicionalFacadeAPI#getValorDatoAdicionalDTO()
     */
    @Override
    public ValorDatoAdicionalDTO getValorDatoAdicionalDTO(Long id) {
        return valorDatoAdicionalService.getValorDatoAdicionalDTO(id);
    }

}
