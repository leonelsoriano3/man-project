package co.com.fspb.mgs.dao.model;


import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioPK
 * @date nov 11, 2016
 */
@Embeddable
public class BeneficiarioServicioPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_beneficiario", nullable = true)
    private Beneficiario beneficiario; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_servicio_publico", nullable = true)
    private Servicio servicio; 
 
 
    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

}
