package co.com.fspb.mgs.dao.model;


import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_objetivo_iniciativa en la BD a la entidad ObjetivoIniciativa.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativa
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_objetivo_iniciativa" )
public class ObjetivoIniciativa implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private ObjetivoIniciativaPK id; 



 
    public ObjetivoIniciativaPK getId() {
        return id;
    }

    public void setId(ObjetivoIniciativaPK id) {
        this.id = id;
    }

}
