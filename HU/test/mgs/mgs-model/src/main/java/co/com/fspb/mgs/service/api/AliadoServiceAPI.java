package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AliadoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Aliado}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoServiceAPI
 * @date nov 11, 2016
 */
public interface AliadoServiceAPI {

    /**
	 * Registra una entidad {@link Aliado} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param aliado - {@link AliadoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(AliadoDTO aliado) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Aliado} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param aliado - {@link AliadoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(AliadoDTO aliado) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<AliadoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link AliadoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link AliadoDTO}
     */
    AliadoDTO getAliadoDTO(Long id);

}
