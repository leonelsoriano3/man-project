package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI;
import co.com.fspb.mgs.service.api.ParametroGeneralServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ParametroGeneralFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneralFacadeImpl
 * @date nov 11, 2016
 */
@Service("parametroGeneralFacade")
public class ParametroGeneralFacadeImpl implements ParametroGeneralFacadeAPI {

    @Autowired
    private ParametroGeneralServiceAPI parametroGeneralService;

    /**
     * @see co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParametroGeneralDTO> getRecords(PmzPagingCriteria criteria) {
        return parametroGeneralService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI#guardar()
     */
    @Override
    public void guardar(ParametroGeneralDTO parametroGeneral) throws PmzException {
        parametroGeneralService.guardar(parametroGeneral);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI#editar()
     */
    @Override
    public void editar(ParametroGeneralDTO parametroGeneral) throws PmzException {
        parametroGeneralService.editar(parametroGeneral);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI#getParametroGeneralDTO()
     */
    @Override
    public ParametroGeneralDTO getParametroGeneralDTO(Long id) {
        return parametroGeneralService.getParametroGeneralDTO(id);
    }

}
