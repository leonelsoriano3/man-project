package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.BeneficiarioServicioDaoAPI;
import co.com.fspb.mgs.dao.api.ServicioDaoAPI;
import co.com.fspb.mgs.dao.model.Servicio;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dao.model.BeneficiarioServicio;
import co.com.fspb.mgs.dao.model.BeneficiarioServicioPK;
import co.com.fspb.mgs.service.api.BeneficiarioServicioServiceAPI;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.ServicioDTO;

/**
 * Implementación de la Interfaz {@link BeneficiarioServicioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServicioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class BeneficiarioServicioServiceImpl implements BeneficiarioServicioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private BeneficiarioServicioDaoAPI beneficiarioServicioDao;
    @Autowired
    private ServicioDaoAPI servicioDao;
    @Autowired
    private BeneficiarioDaoAPI beneficiarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(BeneficiarioServicioDTO beneficiarioServicioDTO) throws PmzException {


		BeneficiarioServicioPK pk = new BeneficiarioServicioPK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), beneficiarioServicioDTO.getBeneficiarioDTO()));
        pk.setServicio(DTOTransformer.getServicioFromServicioDTO(new Servicio(), beneficiarioServicioDTO.getServicioDTO()));
        BeneficiarioServicio beneficiarioServicioValidate = beneficiarioServicioDao.get(pk);

        if (beneficiarioServicioValidate != null) {
            throw new PmzException("El beneficiarioServicio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        BeneficiarioServicio beneficiarioServicio = DTOTransformer.getBeneficiarioServicioFromBeneficiarioServicioDTO(new BeneficiarioServicio(), beneficiarioServicioDTO);

		pk = beneficiarioServicio.getId();
		if (beneficiarioServicioDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(beneficiarioServicioDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setBeneficiario(beneficiario);
		}
		
		if (beneficiarioServicioDTO.getServicioDTO() != null) {
       		Servicio servicio = servicioDao.get(beneficiarioServicioDTO.getServicioDTO().getId());
       		if (servicio == null) {
                throw new PmzException("El Servicio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setServicio(servicio);
		}
		
        beneficiarioServicio.setId(pk);
        
        beneficiarioServicioDao.save(beneficiarioServicio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<BeneficiarioServicioDTO> getRecords(PmzPagingCriteria criteria) {
        List<BeneficiarioServicio> listaReturn = beneficiarioServicioDao.getRecords(criteria);
        Long countTotalRegistros = beneficiarioServicioDao.countRecords(criteria);

        List<BeneficiarioServicioDTO> resultList = new ArrayList<BeneficiarioServicioDTO>();
        for (BeneficiarioServicio beneficiarioServicio : listaReturn) {
            BeneficiarioServicioDTO beneficiarioServicioDTO = DTOTransformer
                    .getBeneficiarioServicioDTOFromBeneficiarioServicio(beneficiarioServicio);
            resultList.add(beneficiarioServicioDTO);
        }
        
        return new PmzResultSet<BeneficiarioServicioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(BeneficiarioServicioDTO beneficiarioServicioDTO) throws PmzException{


		BeneficiarioServicioPK pk = new BeneficiarioServicioPK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), beneficiarioServicioDTO.getBeneficiarioDTO()));
        pk.setServicio(DTOTransformer.getServicioFromServicioDTO(new Servicio(), beneficiarioServicioDTO.getServicioDTO()));
        BeneficiarioServicio beneficiarioServicio = beneficiarioServicioDao.get(pk);

        if (beneficiarioServicio == null) {
            throw new PmzException("El beneficiarioServicio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getBeneficiarioServicioFromBeneficiarioServicioDTO(beneficiarioServicio, beneficiarioServicioDTO);

		pk = beneficiarioServicio.getId();
		if (beneficiarioServicioDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(beneficiarioServicioDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setBeneficiario(beneficiario);
		}
		
		if (beneficiarioServicioDTO.getServicioDTO() != null) {
       		Servicio servicio = servicioDao.get(beneficiarioServicioDTO.getServicioDTO().getId());
       		if (servicio == null) {
                throw new PmzException("El Servicio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setServicio(servicio);
		}
		
        beneficiarioServicio.setId(pk);
        
        beneficiarioServicioDao.update(beneficiarioServicio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public BeneficiarioServicioDTO getBeneficiarioServicioDTO(BeneficiarioDTO beneficiario, ServicioDTO servicio) {

		BeneficiarioServicioPK pk = new BeneficiarioServicioPK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), beneficiario));
        pk.setServicio(DTOTransformer.getServicioFromServicioDTO(new Servicio(), servicio));
        BeneficiarioServicio beneficiarioServicio = beneficiarioServicioDao.get(pk);
        return DTOTransformer.getBeneficiarioServicioDTOFromBeneficiarioServicio(beneficiarioServicio);
    }
}
