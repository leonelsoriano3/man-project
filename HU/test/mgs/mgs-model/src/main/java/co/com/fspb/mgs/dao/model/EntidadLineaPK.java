package co.com.fspb.mgs.dao.model;


import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaPK
 * @date nov 11, 2016
 */
@Embeddable
public class EntidadLineaPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_entidad", nullable = true)
    private Entidad entidad; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_p_linea_intervencion", nullable = true)
    private LineaIntervencion lineaIntervencion; 
 
 
    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public LineaIntervencion getLineaIntervencion() {
        return lineaIntervencion;
    }

    public void setLineaIntervencion(LineaIntervencion lineaIntervencion) {
        this.lineaIntervencion = lineaIntervencion;
    }

}
