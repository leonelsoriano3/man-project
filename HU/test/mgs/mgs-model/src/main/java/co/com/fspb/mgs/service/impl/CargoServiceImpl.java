package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.CargoDaoAPI;
import co.com.fspb.mgs.dao.model.Cargo;
import co.com.fspb.mgs.service.api.CargoServiceAPI;
import co.com.fspb.mgs.dto.CargoDTO;

/**
 * Implementación de la Interfaz {@link CargoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class CargoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class CargoServiceImpl implements CargoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private CargoDaoAPI cargoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(CargoDTO cargoDTO) throws PmzException {

        Cargo cargoValidate = cargoDao.get(cargoDTO.getId());

        if (cargoValidate != null) {
            throw new PmzException("El cargo ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Cargo cargo = DTOTransformer.getCargoFromCargoDTO(new Cargo(), cargoDTO);

        cargoDao.save(cargo);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<CargoDTO> getRecords(PmzPagingCriteria criteria) {
        List<Cargo> listaReturn = cargoDao.getRecords(criteria);
        Long countTotalRegistros = cargoDao.countRecords(criteria);

        List<CargoDTO> resultList = new ArrayList<CargoDTO>();
        for (Cargo cargo : listaReturn) {
            CargoDTO cargoDTO = DTOTransformer
                    .getCargoDTOFromCargo(cargo);
            resultList.add(cargoDTO);
        }
        
        return new PmzResultSet<CargoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(CargoDTO cargoDTO) throws PmzException{

        Cargo cargo = cargoDao.get(cargoDTO.getId());

        if (cargo == null) {
            throw new PmzException("El cargo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getCargoFromCargoDTO(cargo, cargoDTO);

        cargoDao.update(cargo);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public CargoDTO getCargoDTO(Long id) {
        Cargo cargo = cargoDao.get(id);
        return DTOTransformer.getCargoDTOFromCargo(cargo);
    }
}
