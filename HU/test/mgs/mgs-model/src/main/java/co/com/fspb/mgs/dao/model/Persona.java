package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_persona en la BD a la entidad Persona.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Persona
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_persona" )
public class Persona implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_persona", nullable = false)
	private Long id; 

	@Column(name = "tipo_id", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    private TipoIdEnum tipoId; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "num_id", nullable = true, length = 20)
    private String numId; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "nombres", nullable = true, length = 50)
    private String nombres; 

	@Column(name = "apellidos", nullable = true, length = 50)
    private String apellidos; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 


	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoIdEnum getTipoId() {
        return tipoId;
    }

    public void setTipoId(TipoIdEnum tipoId) {
        this.tipoId = tipoId;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

}
