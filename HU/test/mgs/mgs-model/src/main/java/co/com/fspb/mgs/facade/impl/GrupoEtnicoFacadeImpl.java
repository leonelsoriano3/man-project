package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI;
import co.com.fspb.mgs.service.api.GrupoEtnicoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link GrupoEtnicoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class GrupoEtnicoFacadeImpl
 * @date nov 11, 2016
 */
@Service("grupoEtnicoFacade")
public class GrupoEtnicoFacadeImpl implements GrupoEtnicoFacadeAPI {

    @Autowired
    private GrupoEtnicoServiceAPI grupoEtnicoService;

    /**
     * @see co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<GrupoEtnicoDTO> getRecords(PmzPagingCriteria criteria) {
        return grupoEtnicoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI#guardar()
     */
    @Override
    public void guardar(GrupoEtnicoDTO grupoEtnico) throws PmzException {
        grupoEtnicoService.guardar(grupoEtnico);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI#editar()
     */
    @Override
    public void editar(GrupoEtnicoDTO grupoEtnico) throws PmzException {
        grupoEtnicoService.editar(grupoEtnico);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.GrupoEtnicoFacadeAPI#getGrupoEtnicoDTO()
     */
    @Override
    public GrupoEtnicoDTO getGrupoEtnicoDTO(Long id) {
        return grupoEtnicoService.getGrupoEtnicoDTO(id);
    }

}
