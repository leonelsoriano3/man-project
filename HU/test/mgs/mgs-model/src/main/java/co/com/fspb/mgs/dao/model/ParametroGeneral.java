package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.TipoParametroEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_p_parametro_general en la BD a la entidad ParametroGeneral.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneral
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_p_parametro_general" )
public class ParametroGeneral implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_parametro", nullable = false)
	private Long id; 

	@Column(name = "tipo_parametro", nullable = true, length = 30)
    @Enumerated(EnumType.STRING)
    private TipoParametroEnum tipoParametro; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "estado", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "valor", nullable = true, length = 2147483647)
    private String valor; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

	@Column(name = "descripcion", nullable = false, length = 2000)
    private String descripcion; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 

	@Column(name = "clave", nullable = false, length = 70)
    private String clave; 

 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoParametroEnum getTipoParametro() {
        return tipoParametro;
    }

    public void setTipoParametro(TipoParametroEnum tipoParametro) {
        this.tipoParametro = tipoParametro;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

}
