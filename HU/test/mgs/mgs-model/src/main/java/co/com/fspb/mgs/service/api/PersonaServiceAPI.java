package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.PersonaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Persona}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaServiceAPI
 * @date nov 11, 2016
 */
public interface PersonaServiceAPI {

    /**
	 * Registra una entidad {@link Persona} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param persona - {@link PersonaDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(PersonaDTO persona) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Persona} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param persona - {@link PersonaDTO}
	 * @throws {@link PmzException}
	 */
	void editar(PersonaDTO persona) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<PersonaDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link PersonaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link PersonaDTO}
     */
    PersonaDTO getPersonaDTO(Long id);

}
