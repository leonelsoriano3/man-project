package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.EntidadDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link EntidadTerritorio}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorioServiceAPI
 * @date nov 11, 2016
 */
public interface EntidadTerritorioServiceAPI {

    /**
	 * Registra una entidad {@link EntidadTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadTerritorio - {@link EntidadTerritorioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(EntidadTerritorioDTO entidadTerritorio) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link EntidadTerritorio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadTerritorio - {@link EntidadTerritorioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(EntidadTerritorioDTO entidadTerritorio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EntidadTerritorioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link EntidadTerritorioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link TerritorioDTO}
	 * @param entidad - {@link EntidadDTO}
     * @return {@link EntidadTerritorioDTO}
     */
    EntidadTerritorioDTO getEntidadTerritorioDTO(TerritorioDTO territorio, EntidadDTO entidad);

}
