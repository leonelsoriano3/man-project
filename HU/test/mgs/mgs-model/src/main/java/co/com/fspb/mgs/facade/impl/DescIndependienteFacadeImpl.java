package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI;
import co.com.fspb.mgs.service.api.DescIndependienteServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link DescIndependienteFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DescIndependienteFacadeImpl
 * @date nov 11, 2016
 */
@Service("descIndependienteFacade")
public class DescIndependienteFacadeImpl implements DescIndependienteFacadeAPI {

    @Autowired
    private DescIndependienteServiceAPI descIndependienteService;

    /**
     * @see co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<DescIndependienteDTO> getRecords(PmzPagingCriteria criteria) {
        return descIndependienteService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI#guardar()
     */
    @Override
    public void guardar(DescIndependienteDTO descIndependiente) throws PmzException {
        descIndependienteService.guardar(descIndependiente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI#editar()
     */
    @Override
    public void editar(DescIndependienteDTO descIndependiente) throws PmzException {
        descIndependienteService.editar(descIndependiente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DescIndependienteFacadeAPI#getDescIndependienteDTO()
     */
    @Override
    public DescIndependienteDTO getDescIndependienteDTO(Long id) {
        return descIndependienteService.getDescIndependienteDTO(id);
    }

}
