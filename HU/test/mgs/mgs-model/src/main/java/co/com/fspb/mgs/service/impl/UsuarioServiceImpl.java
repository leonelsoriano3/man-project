package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.api.CargoDaoAPI;
import co.com.fspb.mgs.dao.model.Cargo;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.service.api.UsuarioServiceAPI;
import co.com.fspb.mgs.dto.UsuarioDTO;

/**
 * Implementación de la Interfaz {@link UsuarioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class UsuarioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private UsuarioDaoAPI usuarioDao;
    @Autowired
    private PersonaDaoAPI personaDao;
    @Autowired
    private CargoDaoAPI cargoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(UsuarioDTO usuarioDTO) throws PmzException {

        Usuario usuarioValidate = usuarioDao.get(usuarioDTO.getId());

        if (usuarioValidate != null) {
            throw new PmzException("El usuario ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Usuario usuario = DTOTransformer.getUsuarioFromUsuarioDTO(new Usuario(), usuarioDTO);

		if (usuarioDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(usuarioDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		usuario.setPersona(persona);
		}
		
		if (usuarioDTO.getCargoDTO() != null) {
       		Cargo cargo = cargoDao.get(usuarioDTO.getCargoDTO().getId());
       		if (cargo == null) {
                throw new PmzException("El Cargo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		usuario.setCargo(cargo);
		}
		
        usuarioDao.save(usuario);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<UsuarioDTO> getRecords(PmzPagingCriteria criteria) {
        List<Usuario> listaReturn = usuarioDao.getRecords(criteria);
        Long countTotalRegistros = usuarioDao.countRecords(criteria);

        List<UsuarioDTO> resultList = new ArrayList<UsuarioDTO>();
        for (Usuario usuario : listaReturn) {
            UsuarioDTO usuarioDTO = DTOTransformer
                    .getUsuarioDTOFromUsuario(usuario);
            resultList.add(usuarioDTO);
        }
        
        return new PmzResultSet<UsuarioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(UsuarioDTO usuarioDTO) throws PmzException{

        Usuario usuario = usuarioDao.get(usuarioDTO.getId());

        if (usuario == null) {
            throw new PmzException("El usuario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getUsuarioFromUsuarioDTO(usuario, usuarioDTO);

		if (usuarioDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(usuarioDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		usuario.setPersona(persona);
		}
		
		if (usuarioDTO.getCargoDTO() != null) {
       		Cargo cargo = cargoDao.get(usuarioDTO.getCargoDTO().getId());
       		if (cargo == null) {
                throw new PmzException("El Cargo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		usuario.setCargo(cargo);
		}
		
        usuarioDao.update(usuario);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public UsuarioDTO getUsuarioDTO(Long id) {
        Usuario usuario = usuarioDao.get(id);
        return DTOTransformer.getUsuarioDTOFromUsuario(usuario);
    }
}
