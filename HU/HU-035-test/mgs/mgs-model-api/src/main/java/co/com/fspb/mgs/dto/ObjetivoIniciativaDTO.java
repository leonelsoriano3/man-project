package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link ObjetivoIniciativa}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ObjetivoIniciativaDTO
 * @date nov 11, 2016
 */
public class ObjetivoIniciativaDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private ObjetivoDTO objetivoDTO; 

    @JsonSerialize
    @NotNull
    private Integer idUsuarioResp; 

    @JsonSerialize
    @NotNull
    private IniciativaDTO iniciativaDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(objetivoDTO);
        idBuilder.append("-");
        idBuilder.append(iniciativaDTO);
        idBuilder.append("-");
        idBuilder.append(idUsuarioResp);
        return idBuilder.toString();
    }

    public ObjetivoDTO getObjetivoDTO() {
        return objetivoDTO;
    }

    public void setObjetivoDTO(ObjetivoDTO objetivoDTO) {
        this.objetivoDTO = objetivoDTO;
    }

    public Integer getIdUsuarioResp() {
        return idUsuarioResp;
    }

    public void setIdUsuarioResp(Integer idUsuarioResp) {
        this.idUsuarioResp = idUsuarioResp;
    }

    public IniciativaDTO getIniciativaDTO() {
        return iniciativaDTO;
    }

    public void setIniciativaDTO(IniciativaDTO iniciativaDTO) {
        this.iniciativaDTO = iniciativaDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "ObjetivoIniciativaDTO ["
                + " objetivo=" + objetivoDTO
                + ", idUsuarioResp=" + idUsuarioResp
                + ", iniciativa=" + iniciativaDTO
                + " ]";
    }
    
}
