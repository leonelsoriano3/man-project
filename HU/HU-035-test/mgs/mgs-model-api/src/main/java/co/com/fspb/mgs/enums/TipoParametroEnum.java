package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TipoParametroEnum
 * @date nov 11, 2016
 */
public enum TipoParametroEnum {
	
	//TODO PMZ-Generator co.com.fspb.mgs: Llenar enumeracion
    TIPOPARAMETROENUM_0,    TIPOPARAMETROENUM_1,    TIPOPARAMETROENUM_2,    TIPOPARAMETROENUM_3,    TIPOPARAMETROENUM_4;
}