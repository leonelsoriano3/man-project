package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TipoIdEnum
 * @date nov 11, 2016
 */
public enum TipoIdEnum {
	
	//TODO PMZ-Generator co.com.fspb.mgs: Llenar enumeracion
    TIPOIDENUM_0,    TIPOIDENUM_1,    TIPOIDENUM_2,    TIPOIDENUM_3,    TIPOIDENUM_4;
}