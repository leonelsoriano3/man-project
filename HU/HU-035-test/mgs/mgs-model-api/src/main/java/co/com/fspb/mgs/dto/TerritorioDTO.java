package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Territorio}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TerritorioDTO
 * @date nov 11, 2016
 */
public class TerritorioDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private Date fechaModifica; 

    private Integer jovenesHombres; 

    private Integer jovenesMujeres; 

    private Integer menoresMujeres; 

    private EstadoViaDTO estadoViaDTO; 

    @JsonSerialize
    @NotNull
    private Long id; 

    @NotNull
    private String nombre; 

    private Integer adultosMayoresHombres; 

    private TipoTerritorioDTO tipoTerritorioDTO; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private ProblemaComunidadDTO problemaComunidadDTO; 

    private String usuarioModifica; 

    private Integer adultosMayoresMujeres; 

    private String usuarioCrea; 

    private Integer menoresHombres; 

    private ZonaDTO zonaDTO; 

    private Date fechaCrea; 

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public Integer getJovenesHombres() {
        return jovenesHombres;
    }

    public void setJovenesHombres(Integer jovenesHombres) {
        this.jovenesHombres = jovenesHombres;
    }

    public Integer getJovenesMujeres() {
        return jovenesMujeres;
    }

    public void setJovenesMujeres(Integer jovenesMujeres) {
        this.jovenesMujeres = jovenesMujeres;
    }

    public Integer getMenoresMujeres() {
        return menoresMujeres;
    }

    public void setMenoresMujeres(Integer menoresMujeres) {
        this.menoresMujeres = menoresMujeres;
    }

    public EstadoViaDTO getEstadoViaDTO() {
        return estadoViaDTO;
    }

    public void setEstadoViaDTO(EstadoViaDTO estadoViaDTO) {
        this.estadoViaDTO = estadoViaDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getAdultosMayoresHombres() {
        return adultosMayoresHombres;
    }

    public void setAdultosMayoresHombres(Integer adultosMayoresHombres) {
        this.adultosMayoresHombres = adultosMayoresHombres;
    }

    public TipoTerritorioDTO getTipoTerritorioDTO() {
        return tipoTerritorioDTO;
    }

    public void setTipoTerritorioDTO(TipoTerritorioDTO tipoTerritorioDTO) {
        this.tipoTerritorioDTO = tipoTerritorioDTO;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public ProblemaComunidadDTO getProblemaComunidadDTO() {
        return problemaComunidadDTO;
    }

    public void setProblemaComunidadDTO(ProblemaComunidadDTO problemaComunidadDTO) {
        this.problemaComunidadDTO = problemaComunidadDTO;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Integer getAdultosMayoresMujeres() {
        return adultosMayoresMujeres;
    }

    public void setAdultosMayoresMujeres(Integer adultosMayoresMujeres) {
        this.adultosMayoresMujeres = adultosMayoresMujeres;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Integer getMenoresHombres() {
        return menoresHombres;
    }

    public void setMenoresHombres(Integer menoresHombres) {
        this.menoresHombres = menoresHombres;
    }

    public ZonaDTO getZonaDTO() {
        return zonaDTO;
    }

    public void setZonaDTO(ZonaDTO zonaDTO) {
        this.zonaDTO = zonaDTO;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombre;
    }

	@Override
    public String toString() {
        return "TerritorioDTO ["
                + " fechaModifica=" + fechaModifica
                + ", jovenesHombres=" + jovenesHombres
                + ", jovenesMujeres=" + jovenesMujeres
                + ", menoresMujeres=" + menoresMujeres
                + ", estadoVia=" + estadoViaDTO
                + ", id=" + id
                + ", nombre=" + nombre
                + ", adultosMayoresHombres=" + adultosMayoresHombres
                + ", tipoTerritorio=" + tipoTerritorioDTO
                + ", estado=" + estado
                + ", problemaComunidad=" + problemaComunidadDTO
                + ", usuarioModifica=" + usuarioModifica
                + ", adultosMayoresMujeres=" + adultosMayoresMujeres
                + ", usuarioCrea=" + usuarioCrea
                + ", menoresHombres=" + menoresHombres
                + ", zona=" + zonaDTO
                + ", fechaCrea=" + fechaCrea
                + " ]";
    }
    
}
