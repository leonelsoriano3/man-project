package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Proyecto}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ProyectoDTO
 * @date nov 11, 2016
 */
public class ProyectoDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private String nombre; 

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private RolFundacionDTO rolFundacionDTO; 

    private String usuarioModifica; 

    private String usuarioCrea; 

    @NotNull
    private UsuarioDTO usuarioDTO; 

    @NotNull
    private IniciativaDTO iniciativaDTO; 

    private Date fechaCrea; 

    @NotNull
    private Long ppto; 

    @NotNull
    private String poblacionBeneficiaria; 

    @NotNull
    private Integer duracion; 

    @JsonSerialize
    @NotNull
    private Long id; 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public RolFundacionDTO getRolFundacionDTO() {
        return rolFundacionDTO;
    }

    public void setRolFundacionDTO(RolFundacionDTO rolFundacionDTO) {
        this.rolFundacionDTO = rolFundacionDTO;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    public IniciativaDTO getIniciativaDTO() {
        return iniciativaDTO;
    }

    public void setIniciativaDTO(IniciativaDTO iniciativaDTO) {
        this.iniciativaDTO = iniciativaDTO;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getPpto() {
        return ppto;
    }

    public void setPpto(Long ppto) {
        this.ppto = ppto;
    }

    public String getPoblacionBeneficiaria() {
        return poblacionBeneficiaria;
    }

    public void setPoblacionBeneficiaria(String poblacionBeneficiaria) {
        this.poblacionBeneficiaria = poblacionBeneficiaria;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombre;
    }

	@Override
    public String toString() {
        return "ProyectoDTO ["
                + " nombre=" + nombre
                + ", fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", rolFundacion=" + rolFundacionDTO
                + ", usuarioModifica=" + usuarioModifica
                + ", usuarioCrea=" + usuarioCrea
                + ", usuario=" + usuarioDTO
                + ", iniciativa=" + iniciativaDTO
                + ", fechaCrea=" + fechaCrea
                + ", ppto=" + ppto
                + ", poblacionBeneficiaria=" + poblacionBeneficiaria
                + ", duracion=" + duracion
                + ", id=" + id
                + " ]";
    }
    
}
