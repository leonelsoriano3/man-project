package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Indicador}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class IndicadorDTO
 * @date nov 11, 2016
 */
public class IndicadorDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private String nombre; 

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String usuarioModifica; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private String usuarioCrea; 

    private Date fechaCrea; 

    @NotNull
    private Long meta; 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getMeta() {
        return meta;
    }

    public void setMeta(Long meta) {
        this.meta = meta;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.nombre;
    }

	@Override
    public String toString() {
        return "IndicadorDTO ["
                + " nombre=" + nombre
                + ", fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", usuarioModifica=" + usuarioModifica
                + ", id=" + id
                + ", usuarioCrea=" + usuarioCrea
                + ", fechaCrea=" + fechaCrea
                + ", meta=" + meta
                + " ]";
    }
    
}
