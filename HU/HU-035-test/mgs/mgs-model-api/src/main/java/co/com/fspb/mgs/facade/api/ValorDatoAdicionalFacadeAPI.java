package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link ValorDatoAdicional}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ValorDatoAdicionalFacadeAPI
 * @date nov 11, 2016
 */
public interface ValorDatoAdicionalFacadeAPI {

    /**
	 * Registra una entidad {@link ValorDatoAdicional} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param valorDatoAdicional - {@link ValorDatoAdicionalDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ValorDatoAdicionalDTO valorDatoAdicional) throws PmzException;

	/**
	 * Actualiza una entidad {@link ValorDatoAdicional} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param valorDatoAdicional - {@link ValorDatoAdicionalDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ValorDatoAdicionalDTO valorDatoAdicional) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ValorDatoAdicionalDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ValorDatoAdicionalDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link ValorDatoAdicionalDTO}
     */
    ValorDatoAdicionalDTO getValorDatoAdicionalDTO(Long id);
}

