package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link TrazaPresupuesto}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class TrazaPresupuestoDTO
 * @date nov 11, 2016
 */
public class TrazaPresupuestoDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @NotNull
    private Integer anio; 

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    @JsonSerialize
    @NotNull
    private Long id; 

    private String usuarioModifica; 

    private LineaDTO lineaDTO; 

    private String usuarioCrea; 

    private Long presupuestoEjecutado; 

    private Date fechaCrea; 

    @NotNull
    private Long presupuestoPlaneado; 

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public LineaDTO getLineaDTO() {
        return lineaDTO;
    }

    public void setLineaDTO(LineaDTO lineaDTO) {
        this.lineaDTO = lineaDTO;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Long getPresupuestoEjecutado() {
        return presupuestoEjecutado;
    }

    public void setPresupuestoEjecutado(Long presupuestoEjecutado) {
        this.presupuestoEjecutado = presupuestoEjecutado;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getPresupuestoPlaneado() {
        return presupuestoPlaneado;
    }

    public void setPresupuestoPlaneado(Long presupuestoPlaneado) {
        this.presupuestoPlaneado = presupuestoPlaneado;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "TrazaPresupuestoDTO ["
                + " anio=" + anio
                + ", fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", id=" + id
                + ", usuarioModifica=" + usuarioModifica
                + ", linea=" + lineaDTO
                + ", usuarioCrea=" + usuarioCrea
                + ", presupuestoEjecutado=" + presupuestoEjecutado
                + ", fechaCrea=" + fechaCrea
                + ", presupuestoPlaneado=" + presupuestoPlaneado
                + " ]";
    }
    
}
