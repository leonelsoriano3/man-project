package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Pariente}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ParienteDTO
 * @date nov 11, 2016
 */
public class ParienteDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String usuarioModifica; 

    private ParentescoDTO parentescoDTO; 

    @JsonSerialize
    @NotNull
    private PersonaDTO personaDTO; 

    private String usuarioCrea; 

    private Date fechaCrea; 

    @JsonSerialize
    @NotNull
    private BeneficiarioDTO beneficiarioDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(beneficiarioDTO);
        idBuilder.append("-");
        idBuilder.append(personaDTO);
        return idBuilder.toString();
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public ParentescoDTO getParentescoDTO() {
        return parentescoDTO;
    }

    public void setParentescoDTO(ParentescoDTO parentescoDTO) {
        this.parentescoDTO = parentescoDTO;
    }

    public PersonaDTO getPersonaDTO() {
        return personaDTO;
    }

    public void setPersonaDTO(PersonaDTO personaDTO) {
        this.personaDTO = personaDTO;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public BeneficiarioDTO getBeneficiarioDTO() {
        return beneficiarioDTO;
    }

    public void setBeneficiarioDTO(BeneficiarioDTO beneficiarioDTO) {
        this.beneficiarioDTO = beneficiarioDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "ParienteDTO ["
                + " fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", usuarioModifica=" + usuarioModifica
                + ", parentesco=" + parentescoDTO
                + ", persona=" + personaDTO
                + ", usuarioCrea=" + usuarioCrea
                + ", fechaCrea=" + fechaCrea
                + ", beneficiario=" + beneficiarioDTO
                + " ]";
    }
    
}
