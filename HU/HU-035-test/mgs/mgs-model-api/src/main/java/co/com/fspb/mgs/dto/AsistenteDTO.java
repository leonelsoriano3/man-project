package co.com.fspb.mgs.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link Asistente}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class AsistenteDTO
 * @date nov 11, 2016
 */
public class AsistenteDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    private Date fechaModifica; 

    @NotNull
    private String estado; 
    
    @NotNull
    private String estadoDesc; 

    private String usuarioModifica; 

    @NotNull
    private ProyectoBeneficiarioDTO proyectoBeneficiarioDTO; 

    private String usuarioCrea; 

    private Date fechaCrea; 

    @JsonSerialize
    @NotNull
    private Long id; 

    @NotNull
    private ActividadDTO actividadDTO; 

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public ProyectoBeneficiarioDTO getProyectoBeneficiarioDTO() {
        return proyectoBeneficiarioDTO;
    }

    public void setProyectoBeneficiarioDTO(ProyectoBeneficiarioDTO proyectoBeneficiarioDTO) {
        this.proyectoBeneficiarioDTO = proyectoBeneficiarioDTO;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ActividadDTO getActividadDTO() {
        return actividadDTO;
    }

    public void setActividadDTO(ActividadDTO actividadDTO) {
        this.actividadDTO = actividadDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "AsistenteDTO ["
                + " fechaModifica=" + fechaModifica
                + ", estado=" + estado
                + ", usuarioModifica=" + usuarioModifica
                + ", proyectoBeneficiario=" + proyectoBeneficiarioDTO
                + ", usuarioCrea=" + usuarioCrea
                + ", fechaCrea=" + fechaCrea
                + ", id=" + id
                + ", actividad=" + actividadDTO
                + " ]";
    }
    
}
