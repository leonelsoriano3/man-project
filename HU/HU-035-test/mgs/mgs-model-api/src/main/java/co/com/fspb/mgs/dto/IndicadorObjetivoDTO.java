package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link IndicadorObjetivo}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class IndicadorObjetivoDTO
 * @date nov 11, 2016
 */
public class IndicadorObjetivoDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private ObjetivoDTO objetivoDTO; 

    @JsonSerialize
    @NotNull
    private IndicadorDTO indicadorDTO; 

    private String resultado; 

    private String meta; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(indicadorDTO);
        idBuilder.append("-");
        idBuilder.append(objetivoDTO);
        return idBuilder.toString();
    }

    public ObjetivoDTO getObjetivoDTO() {
        return objetivoDTO;
    }

    public void setObjetivoDTO(ObjetivoDTO objetivoDTO) {
        this.objetivoDTO = objetivoDTO;
    }

    public IndicadorDTO getIndicadorDTO() {
        return indicadorDTO;
    }

    public void setIndicadorDTO(IndicadorDTO indicadorDTO) {
        this.indicadorDTO = indicadorDTO;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "IndicadorObjetivoDTO ["
                + " objetivo=" + objetivoDTO
                + ", indicador=" + indicadorDTO
                + ", resultado=" + resultado
                + ", meta=" + meta
                + " ]";
    }
    
}
