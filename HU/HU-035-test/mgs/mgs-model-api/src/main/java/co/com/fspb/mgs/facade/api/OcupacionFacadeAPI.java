package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.OcupacionDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link Ocupacion}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class OcupacionFacadeAPI
 * @date nov 11, 2016
 */
public interface OcupacionFacadeAPI {

    /**
	 * Registra una entidad {@link Ocupacion} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param ocupacion - {@link OcupacionDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(OcupacionDTO ocupacion) throws PmzException;

	/**
	 * Actualiza una entidad {@link Ocupacion} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param ocupacion - {@link OcupacionDTO}
	 * @throws {@link PmzException}
	 */
    void editar(OcupacionDTO ocupacion) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<OcupacionDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link OcupacionDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link OcupacionDTO}
     */
    OcupacionDTO getOcupacionDTO(Long id);
}

