package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dto.IniciativaDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link ObjetivoIniciativa}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class ObjetivoIniciativaFacadeAPI
 * @date nov 11, 2016
 */
public interface ObjetivoIniciativaFacadeAPI {

    /**
	 * Registra una entidad {@link ObjetivoIniciativa} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivoIniciativa - {@link ObjetivoIniciativaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(ObjetivoIniciativaDTO objetivoIniciativa) throws PmzException;

	/**
	 * Actualiza una entidad {@link ObjetivoIniciativa} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivoIniciativa - {@link ObjetivoIniciativaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(ObjetivoIniciativaDTO objetivoIniciativa) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ObjetivoIniciativaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link ObjetivoIniciativaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivo - {@link ObjetivoDTO}
	 * @param iniciativa - {@link IniciativaDTO}
	 * @param idUsuarioResp - Integer
     * @return {@link ObjetivoIniciativaDTO}
     */
    ObjetivoIniciativaDTO getObjetivoIniciativaDTO(ObjetivoDTO objetivo, IniciativaDTO iniciativa, Integer idUsuarioResp);
}

