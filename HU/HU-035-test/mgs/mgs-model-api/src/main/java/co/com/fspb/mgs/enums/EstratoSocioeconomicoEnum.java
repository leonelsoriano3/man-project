package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstratoSocioeconomicoEnum
 * @date nov 11, 2016
 */
public enum EstratoSocioeconomicoEnum {
	
	//TODO PMZ-Generator co.com.fspb.mgs: Llenar enumeracion
    ESTRATOSOCIOECONOMICOENUM_0,    ESTRATOSOCIOECONOMICOENUM_1,    ESTRATOSOCIOECONOMICOENUM_2,    ESTRATOSOCIOECONOMICOENUM_3,    ESTRATOSOCIOECONOMICOENUM_4;
}