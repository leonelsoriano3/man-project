package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link BdNacional}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class BdNacionalFacadeAPI
 * @date nov 11, 2016
 */
public interface BdNacionalFacadeAPI {

    /**
	 * Registra una entidad {@link BdNacional} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param bdNacional - {@link BdNacionalDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(BdNacionalDTO bdNacional) throws PmzException;

	/**
	 * Actualiza una entidad {@link BdNacional} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param bdNacional - {@link BdNacionalDTO}
	 * @throws {@link PmzException}
	 */
    void editar(BdNacionalDTO bdNacional) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<BdNacionalDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link BdNacionalDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link BdNacionalDTO}
     */
    BdNacionalDTO getBdNacionalDTO(Long id);
}

