package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link EntidadLinea}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EntidadLineaFacadeAPI
 * @date nov 11, 2016
 */
public interface EntidadLineaFacadeAPI {

    /**
	 * Registra una entidad {@link EntidadLinea} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadLinea - {@link EntidadLineaDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(EntidadLineaDTO entidadLinea) throws PmzException;

	/**
	 * Actualiza una entidad {@link EntidadLinea} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadLinea - {@link EntidadLineaDTO}
	 * @throws {@link PmzException}
	 */
    void editar(EntidadLineaDTO entidadLinea) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EntidadLineaDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link EntidadLineaDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidad - {@link EntidadDTO}
	 * @param lineaIntervencion - {@link LineaIntervencionDTO}
     * @return {@link EntidadLineaDTO}
     */
    EntidadLineaDTO getEntidadLineaDTO(EntidadDTO entidad, LineaIntervencionDTO lineaIntervencion);
}

