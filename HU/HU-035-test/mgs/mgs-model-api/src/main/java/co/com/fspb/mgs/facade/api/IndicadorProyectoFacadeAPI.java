package co.com.fspb.mgs.facade.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la fachada para la entidad {@link IndicadorProyecto}.
 * La implementación de esta interfaz debe utilizar la capa de servicios para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class IndicadorProyectoFacadeAPI
 * @date nov 11, 2016
 */
public interface IndicadorProyectoFacadeAPI {

    /**
	 * Registra una entidad {@link IndicadorProyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorProyecto - {@link IndicadorProyectoDTO}
	 * @throws {@link PmzException}
	 */
    void guardar(IndicadorProyectoDTO indicadorProyecto) throws PmzException;

	/**
	 * Actualiza una entidad {@link IndicadorProyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorProyecto - {@link IndicadorProyectoDTO}
	 * @throws {@link PmzException}
	 */
    void editar(IndicadorProyectoDTO indicadorProyecto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<IndicadorProyectoDTO> getRecords(PmzPagingCriteria criteria);
	
	/**
     * Metodo que obtiene un {@link IndicadorProyectoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link IndicadorDTO}
	 * @param proyecto - {@link ProyectoDTO}
     * @return {@link IndicadorProyectoDTO}
     */
    IndicadorProyectoDTO getIndicadorProyectoDTO(IndicadorDTO indicador, ProyectoDTO proyecto);
}

