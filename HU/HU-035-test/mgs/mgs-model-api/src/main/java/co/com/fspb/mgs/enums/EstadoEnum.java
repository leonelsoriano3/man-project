package co.com.fspb.mgs.enums;

/**
 * Enumeración de atributo de clases del modelo
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EstadoEnum
 * @date nov 11, 2016
 */
public enum EstadoEnum {
	
	//TODO PMZ-Generator co.com.fspb.mgs: Llenar enumeracion
    ESTADOENUM_0,    ESTADOENUM_1,    ESTADOENUM_2,    ESTADOENUM_3,    ESTADOENUM_4;
}