package co.com.fspb.mgs.dto;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.premize.pmz.api.dto.PmzBaseEntityDTO;

/**
 * Data Transfer Object (DTO) para la entidad {@link EntidadLinea}.
 * Extiende al DTO {@link PmzBaseEntityDTO}. 
 * 
 * @author PMZ - Premize S.A.S
 * @project API del Modelo
 * @class EntidadLineaDTO
 * @date nov 11, 2016
 */
public class EntidadLineaDTO extends PmzBaseEntityDTO{

    private static final long serialVersionUID = -349018818090205525L;

    @JsonSerialize
    @NotNull
    private LineaIntervencionDTO lineaIntervencionDTO; 

    @JsonSerialize
    @NotNull
    private EntidadDTO entidadDTO; 

    public String getId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(entidadDTO);
        idBuilder.append("-");
        idBuilder.append(lineaIntervencionDTO);
        return idBuilder.toString();
    }

    public LineaIntervencionDTO getLineaIntervencionDTO() {
        return lineaIntervencionDTO;
    }

    public void setLineaIntervencionDTO(LineaIntervencionDTO lineaIntervencionDTO) {
        this.lineaIntervencionDTO = lineaIntervencionDTO;
    }

    public EntidadDTO getEntidadDTO() {
        return entidadDTO;
    }

    public void setEntidadDTO(EntidadDTO entidadDTO) {
        this.entidadDTO = entidadDTO;
    }

   /**
    * Metodo usado en la presentación para mostrar la descripción de la entidad.
    * Cuando la descripción no es definida, por defecto es el toString() de este DTO. 
    * 
    * @author PMZ - Premize S.A.S
    * @date nov 11, 2016
    */
	@Override
    public String getBaseDescription() {
        return this.toString();
    }

	@Override
    public String toString() {
        return "EntidadLineaDTO ["
                + " lineaIntervencion=" + lineaIntervencionDTO
                + ", entidad=" + entidadDTO
                + " ]";
    }
    
}
