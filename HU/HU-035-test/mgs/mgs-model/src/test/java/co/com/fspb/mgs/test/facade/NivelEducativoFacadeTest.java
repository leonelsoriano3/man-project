package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.facade.api.NivelEducativoFacadeAPI;
import co.com.fspb.mgs.facade.impl.NivelEducativoFacadeImpl;
import co.com.fspb.mgs.service.api.NivelEducativoServiceAPI;
import co.com.fspb.mgs.test.service.data.NivelEducativoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link NivelEducativoFacadeAPI} de la entidad {@link NivelEducativo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoFacadeTest
 * @date nov 11, 2016
 */
public class NivelEducativoFacadeTest {

    private static final Long ID_PRIMER_NIVELEDUCATIVO = 1L;

    @InjectMocks
    private NivelEducativoFacadeAPI nivelEducativoFacadeAPI = new NivelEducativoFacadeImpl();

    @Mock
    private NivelEducativoServiceAPI nivelEducativoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(NivelEducativoServiceTestData.getResultSetNivelEducativosDTO()).when(nivelEducativoServiceAPI).getRecords(null);
        PmzResultSet<NivelEducativoDTO> nivelEducativos = nivelEducativoFacadeAPI.getRecords(null);
        Assert.assertEquals(NivelEducativoServiceTestData.getTotalNivelEducativos(), nivelEducativos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link NivelEducativoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_nivelEducativo_existe(){
        Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativoDTO()).when(nivelEducativoServiceAPI).getNivelEducativoDTO(ID_PRIMER_NIVELEDUCATIVO);
        NivelEducativoDTO nivelEducativo = nivelEducativoFacadeAPI.getNivelEducativoDTO(ID_PRIMER_NIVELEDUCATIVO);
        Assert.assertEquals(NivelEducativoServiceTestData.getNivelEducativoDTO().getId(), nivelEducativo.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link NivelEducativoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_nivelEducativo_no_existe() throws PmzException{
        nivelEducativoFacadeAPI.guardar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link NivelEducativoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_nivelEducativo_existe() throws PmzException{
        nivelEducativoFacadeAPI.editar(NivelEducativoServiceTestData.getNivelEducativoDTO());
    }
    
}
