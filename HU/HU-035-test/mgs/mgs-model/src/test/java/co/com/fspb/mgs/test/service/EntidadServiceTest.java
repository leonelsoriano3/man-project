package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.service.api.EntidadServiceAPI;
import co.com.fspb.mgs.service.impl.EntidadServiceImpl;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;

import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

/**
 * Prueba unitaria para el servicio {@link EntidadServiceAPI} de la entidad {@link Entidad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadServiceAPI
 * @date nov 11, 2016
 */
public class EntidadServiceTest {
	
    private static final Long ID_PRIMER_ENTIDAD = 1L;

    @InjectMocks
    private EntidadServiceAPI entidadServiceAPI = new EntidadServiceImpl();

    @Mock
    private EntidadDaoAPI entidadDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadServiceTestData.getEntidads()).when(entidadDaoAPI).getRecords(null);
        Mockito.doReturn(EntidadServiceTestData.getTotalEntidads()).when(entidadDaoAPI).countRecords(null);
        PmzResultSet<EntidadDTO> entidads = entidadServiceAPI.getRecords(null);
        Assert.assertEquals(EntidadServiceTestData.getTotalEntidads(), entidads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidad_existe(){
        Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(ID_PRIMER_ENTIDAD);
        EntidadDTO entidad = entidadServiceAPI.getEntidadDTO(ID_PRIMER_ENTIDAD);
        Assert.assertEquals(EntidadServiceTestData.getEntidad().getId(), entidad.getId());
    }

    /**
     * Prueba unitaria de get con {@link EntidadDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidad_no_existe(){
        Mockito.doReturn(null).when(entidadDaoAPI).get(ID_PRIMER_ENTIDAD);
        EntidadDTO entidad = entidadServiceAPI.getEntidadDTO(ID_PRIMER_ENTIDAD);
        Assert.assertEquals(null, entidad);
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_entidad_no_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	entidadServiceAPI.guardar(EntidadServiceTestData.getEntidadDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidad_existe() throws PmzException{
        Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(ID_PRIMER_ENTIDAD);
        entidadServiceAPI.guardar(EntidadServiceTestData.getEntidadDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link EntidadDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidad_no_existe_no_persona() throws PmzException{
    	entidadServiceAPI.guardar(EntidadServiceTestData.getEntidadDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EntidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_entidad_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
        Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(ID_PRIMER_ENTIDAD);
        entidadServiceAPI.editar(EntidadServiceTestData.getEntidadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_entidad_no_existe() throws PmzException{
        entidadServiceAPI.editar(EntidadServiceTestData.getEntidadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_entidad_existe_no_persona() throws PmzException{
        Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(ID_PRIMER_ENTIDAD);
    	entidadServiceAPI.editar(EntidadServiceTestData.getEntidadDTO());
    }
    
    
}
