package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.dao.api.EntidadLineaDaoAPI;
import co.com.fspb.mgs.service.api.EntidadLineaServiceAPI;
import co.com.fspb.mgs.service.impl.EntidadLineaServiceImpl;
import co.com.fspb.mgs.test.service.data.EntidadLineaServiceTestData;
import co.com.fspb.mgs.dao.model.EntidadLineaPK;
  import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.test.service.data.EntidadServiceTestData;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.dao.api.LineaIntervencionDaoAPI;
import co.com.fspb.mgs.test.service.data.LineaIntervencionServiceTestData;

/**
 * Prueba unitaria para el servicio {@link EntidadLineaServiceAPI} de la entidad {@link EntidadLinea}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadLineaServiceAPI
 * @date nov 11, 2016
 */
public class EntidadLineaServiceTest {
	
    private static final EntidadDTO ENTIDADDTO = EntidadServiceTestData.getEntidadDTO();
    private static final LineaIntervencionDTO LINEAINTERVENCIONDTO = LineaIntervencionServiceTestData.getLineaIntervencionDTO();

    @InjectMocks
    private EntidadLineaServiceAPI entidadLineaServiceAPI = new EntidadLineaServiceImpl();

    @Mock
    private EntidadLineaDaoAPI entidadLineaDaoAPI;
    @Mock
    private EntidadDaoAPI entidadDaoAPI;
    @Mock
    private LineaIntervencionDaoAPI lineaIntervencionDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLineas()).when(entidadLineaDaoAPI).getRecords(null);
        Mockito.doReturn(EntidadLineaServiceTestData.getTotalEntidadLineas()).when(entidadLineaDaoAPI).countRecords(null);
        PmzResultSet<EntidadLineaDTO> entidadLineas = entidadLineaServiceAPI.getRecords(null);
        Assert.assertEquals(EntidadLineaServiceTestData.getTotalEntidadLineas(), entidadLineas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EntidadLineaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadLinea_existe(){
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLinea()).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
        EntidadLineaDTO entidadLinea = entidadLineaServiceAPI.getEntidadLineaDTO(ENTIDADDTO, LINEAINTERVENCIONDTO);
        Assert.assertEquals(EntidadLineaServiceTestData.getEntidadLineaDTO().getId(), entidadLinea.getId());
    }

    /**
     * Prueba unitaria de get con {@link EntidadLineaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_entidadLinea_no_existe(){
        Mockito.doReturn(null).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
        EntidadLineaDTO entidadLinea = entidadLineaServiceAPI.getEntidadLineaDTO(ENTIDADDTO, LINEAINTERVENCIONDTO);
        Assert.assertEquals(null, entidadLinea);
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadLineaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_entidadLinea_no_existe() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(LineaIntervencionServiceTestData.getLineaIntervencion().getId());
    	entidadLineaServiceAPI.guardar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link EntidadLineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadLinea_existe() throws PmzException{
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLinea()).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
        entidadLineaServiceAPI.guardar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link EntidadLineaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadLinea_no_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(LineaIntervencionServiceTestData.getLineaIntervencion().getId());
    	entidadLineaServiceAPI.guardar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link EntidadLineaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con lineaIntervencion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_entidadLinea_no_existe_no_lineaIntervencion() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	entidadLineaServiceAPI.guardar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link EntidadLineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_entidadLinea_existe() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
    	Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(LineaIntervencionServiceTestData.getLineaIntervencion().getId());
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLinea()).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
        entidadLineaServiceAPI.editar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadLineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_entidadLinea_no_existe() throws PmzException{
        entidadLineaServiceAPI.editar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadLineaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con entidad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_entidadLinea_existe_no_entidad() throws PmzException{
    	Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(LineaIntervencionServiceTestData.getLineaIntervencion().getId());
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLinea()).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
    	entidadLineaServiceAPI.editar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link EntidadLineaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con lineaIntervencion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_entidadLinea_existe_no_lineaIntervencion() throws PmzException{
    	Mockito.doReturn(EntidadServiceTestData.getEntidad()).when(entidadDaoAPI).get(EntidadServiceTestData.getEntidad().getId());
        Mockito.doReturn(EntidadLineaServiceTestData.getEntidadLinea()).when(entidadLineaDaoAPI).get(Mockito.any(EntidadLineaPK.class));
    	entidadLineaServiceAPI.editar(EntidadLineaServiceTestData.getEntidadLineaDTO());
    }
    
    
}
