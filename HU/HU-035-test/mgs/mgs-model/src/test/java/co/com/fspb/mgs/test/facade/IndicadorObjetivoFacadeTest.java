package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.facade.api.IndicadorObjetivoFacadeAPI;
import co.com.fspb.mgs.facade.impl.IndicadorObjetivoFacadeImpl;
import co.com.fspb.mgs.service.api.IndicadorObjetivoServiceAPI;
import co.com.fspb.mgs.test.service.data.IndicadorObjetivoServiceTestData;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.test.service.data.IndicadorServiceTestData;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link IndicadorObjetivoFacadeAPI} de la entidad {@link IndicadorObjetivo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorObjetivoFacadeTest
 * @date nov 11, 2016
 */
public class IndicadorObjetivoFacadeTest {

    private static final IndicadorDTO INDICADORDTO = IndicadorServiceTestData.getIndicadorDTO();
    private static final ObjetivoDTO OBJETIVODTO = ObjetivoServiceTestData.getObjetivoDTO();

    @InjectMocks
    private IndicadorObjetivoFacadeAPI indicadorObjetivoFacadeAPI = new IndicadorObjetivoFacadeImpl();

    @Mock
    private IndicadorObjetivoServiceAPI indicadorObjetivoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getResultSetIndicadorObjetivosDTO()).when(indicadorObjetivoServiceAPI).getRecords(null);
        PmzResultSet<IndicadorObjetivoDTO> indicadorObjetivos = indicadorObjetivoFacadeAPI.getRecords(null);
        Assert.assertEquals(IndicadorObjetivoServiceTestData.getTotalIndicadorObjetivos(), indicadorObjetivos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IndicadorObjetivoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_indicadorObjetivo_existe(){
        Mockito.doReturn(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO()).when(indicadorObjetivoServiceAPI).getIndicadorObjetivoDTO(INDICADORDTO, OBJETIVODTO);
        IndicadorObjetivoDTO indicadorObjetivo = indicadorObjetivoFacadeAPI.getIndicadorObjetivoDTO(INDICADORDTO, OBJETIVODTO);
        Assert.assertEquals(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO().getId(), indicadorObjetivo.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link IndicadorObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_indicadorObjetivo_no_existe() throws PmzException{
        indicadorObjetivoFacadeAPI.guardar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link IndicadorObjetivoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_indicadorObjetivo_existe() throws PmzException{
        indicadorObjetivoFacadeAPI.editar(IndicadorObjetivoServiceTestData.getIndicadorObjetivoDTO());
    }
    
}
