package co.com.fspb.mgs.test.dao.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.premize.pmz.api.dto.PmzDateInterval;
import com.premize.pmz.api.dto.PmzSearch;
import com.premize.pmz.api.dto.PmzSortField;

/**
 * Clase abstracta que extiende a {@link DaoTestData} para la entidad
 * {@link Servicio}. Esta clase implementa los métodos genéricos de
 * creación de filtros para la entidad y puedan ser usados por todos las pruebas
 * unitarias de {@link Servicio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioDaoTestData
 * @date nov 11, 2016
 */
public abstract class ServicioDaoTestData extends DaoTestData {

    /**
	 * @see co.com.fspb.mgs.test.dao.data.DaoTestData#getListSearchs()
	 */
	@Override
	public List<PmzSearch> getListSearchs(boolean searchs) {
		List<PmzSearch> listSearchs = new ArrayList<PmzSearch>();
		if (searchs) {
			String sColName = "id";
			String sSearchi = "1";
			listSearchs.add(new PmzSearch(sColName, sSearchi));
			sColName = "estadoDesc";
			sSearchi = "ESTADOENUM_1";
			listSearchs.add(new PmzSearch(sColName, sSearchi));
		}
		return listSearchs;
	}

    /**
	 * @see co.com.fspb.mgs.test.dao.data.DaoTestData#getSortFields()
	 */
	@Override
	public List<PmzSortField> getSortFields(boolean asc) {
		Integer iSortingCols = 1;
		List<PmzSortField> sortFields = new ArrayList<PmzSortField>();
		for (int colCount = 0; colCount < iSortingCols; colCount++) {
			String sSortDir = asc ? "asc" : "desc";
			String sColName = "id";
			sortFields.add(new PmzSortField(sColName, sSortDir));
		}
		return sortFields;
	}

    /**
	 * @see co.com.fspb.mgs.test.dao.data.DaoTestData#getIntervals()
	 */
	@Override
	public List<PmzDateInterval> getIntervals(boolean fechaInicial,
			boolean fechaFinal) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2000);

		List<PmzDateInterval> dateIntervals = new ArrayList<PmzDateInterval>();
		dateIntervals.add(new PmzDateInterval("fechaModifica", fechaInicial ? calendar
				.getTimeInMillis() : null, fechaFinal ? new Date().getTime()
				: null));
		dateIntervals.add(new PmzDateInterval("fechaCrea", fechaInicial ? calendar
				.getTimeInMillis() : null, fechaFinal ? new Date().getTime()
				: null));

		return dateIntervals;
	}

}
