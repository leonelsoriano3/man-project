package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TrazaPresupuesto;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TrazaPresupuesto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuestoServiceTestData
 * @date nov 11, 2016
 */
public abstract class TrazaPresupuestoServiceTestData {
    
    private static List<TrazaPresupuesto> entities = new ArrayList<TrazaPresupuesto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TrazaPresupuesto entity = new TrazaPresupuesto();

            entity.setId(i);
            
            entity.setAnio(i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setLinea(LineaServiceTestData.getLinea());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setPresupuestoEjecutado(i);
            entity.setFechaCrea(new Date());
            entity.setPresupuestoPlaneado(i);
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TrazaPresupuesto} Mock 
     * tranformado a DTO {@link TrazaPresupuestoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TrazaPresupuestoDTO}
     */
    public static TrazaPresupuestoDTO getTrazaPresupuestoDTO(){
        return DTOTransformer.getTrazaPresupuestoDTOFromTrazaPresupuesto(getTrazaPresupuesto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TrazaPresupuesto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TrazaPresupuesto} Mock
     */
    public static TrazaPresupuesto getTrazaPresupuesto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TrazaPresupuesto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TrazaPresupuesto}
     */
    public static List<TrazaPresupuesto> getTrazaPresupuestos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TrazaPresupuestoDTO> getResultSetTrazaPresupuestosDTO(){
        List<TrazaPresupuestoDTO> dtos = new ArrayList<TrazaPresupuestoDTO>();
        for (TrazaPresupuesto entity : entities) {
            dtos.add(DTOTransformer.getTrazaPresupuestoDTOFromTrazaPresupuesto(entity));
        }
        return new PmzResultSet<TrazaPresupuestoDTO>(dtos, getTotalTrazaPresupuestos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTrazaPresupuestos(){
        return Long.valueOf(entities.size());
    }

}
