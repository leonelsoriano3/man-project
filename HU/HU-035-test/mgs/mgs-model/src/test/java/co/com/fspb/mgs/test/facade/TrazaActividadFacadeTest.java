package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.facade.api.TrazaActividadFacadeAPI;
import co.com.fspb.mgs.facade.impl.TrazaActividadFacadeImpl;
import co.com.fspb.mgs.service.api.TrazaActividadServiceAPI;
import co.com.fspb.mgs.test.service.data.TrazaActividadServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TrazaActividadFacadeAPI} de la entidad {@link TrazaActividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadFacadeTest
 * @date nov 11, 2016
 */
public class TrazaActividadFacadeTest {

    private static final Long ID_PRIMER_TRAZAACTIVIDAD = 1L;

    @InjectMocks
    private TrazaActividadFacadeAPI trazaActividadFacadeAPI = new TrazaActividadFacadeImpl();

    @Mock
    private TrazaActividadServiceAPI trazaActividadServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TrazaActividadServiceTestData.getResultSetTrazaActividadsDTO()).when(trazaActividadServiceAPI).getRecords(null);
        PmzResultSet<TrazaActividadDTO> trazaActividads = trazaActividadFacadeAPI.getRecords(null);
        Assert.assertEquals(TrazaActividadServiceTestData.getTotalTrazaActividads(), trazaActividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TrazaActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_trazaActividad_existe(){
        Mockito.doReturn(TrazaActividadServiceTestData.getTrazaActividadDTO()).when(trazaActividadServiceAPI).getTrazaActividadDTO(ID_PRIMER_TRAZAACTIVIDAD);
        TrazaActividadDTO trazaActividad = trazaActividadFacadeAPI.getTrazaActividadDTO(ID_PRIMER_TRAZAACTIVIDAD);
        Assert.assertEquals(TrazaActividadServiceTestData.getTrazaActividadDTO().getId(), trazaActividad.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TrazaActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_trazaActividad_no_existe() throws PmzException{
        trazaActividadFacadeAPI.guardar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TrazaActividadDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_trazaActividad_existe() throws PmzException{
        trazaActividadFacadeAPI.editar(TrazaActividadServiceTestData.getTrazaActividadDTO());
    }
    
}
