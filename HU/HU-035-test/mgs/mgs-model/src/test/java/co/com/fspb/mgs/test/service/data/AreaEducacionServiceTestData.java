package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.AreaEducacion;
import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link AreaEducacion}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AreaEducacionServiceTestData
 * @date nov 11, 2016
 */
public abstract class AreaEducacionServiceTestData {
    
    private static List<AreaEducacion> entities = new ArrayList<AreaEducacion>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            AreaEducacion entity = new AreaEducacion();

            entity.setId(i);
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link AreaEducacion} Mock 
     * tranformado a DTO {@link AreaEducacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link AreaEducacionDTO}
     */
    public static AreaEducacionDTO getAreaEducacionDTO(){
        return DTOTransformer.getAreaEducacionDTOFromAreaEducacion(getAreaEducacion());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link AreaEducacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link AreaEducacion} Mock
     */
    public static AreaEducacion getAreaEducacion(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link AreaEducacion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link AreaEducacion}
     */
    public static List<AreaEducacion> getAreaEducacions(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<AreaEducacionDTO> getResultSetAreaEducacionsDTO(){
        List<AreaEducacionDTO> dtos = new ArrayList<AreaEducacionDTO>();
        for (AreaEducacion entity : entities) {
            dtos.add(DTOTransformer.getAreaEducacionDTOFromAreaEducacion(entity));
        }
        return new PmzResultSet<AreaEducacionDTO>(dtos, getTotalAreaEducacions(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalAreaEducacions(){
        return Long.valueOf(entities.size());
    }

}
