package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI;
import co.com.fspb.mgs.facade.impl.EstructuraFamiliarFacadeImpl;
import co.com.fspb.mgs.service.api.EstructuraFamiliarServiceAPI;
import co.com.fspb.mgs.test.service.data.EstructuraFamiliarServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link EstructuraFamiliarFacadeAPI} de la entidad {@link EstructuraFamiliar}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstructuraFamiliarFacadeTest
 * @date nov 11, 2016
 */
public class EstructuraFamiliarFacadeTest {

    private static final Long ID_PRIMER_ESTRUCTURAFAMILIAR = 1L;

    @InjectMocks
    private EstructuraFamiliarFacadeAPI estructuraFamiliarFacadeAPI = new EstructuraFamiliarFacadeImpl();

    @Mock
    private EstructuraFamiliarServiceAPI estructuraFamiliarServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getResultSetEstructuraFamiliarsDTO()).when(estructuraFamiliarServiceAPI).getRecords(null);
        PmzResultSet<EstructuraFamiliarDTO> estructuraFamiliars = estructuraFamiliarFacadeAPI.getRecords(null);
        Assert.assertEquals(EstructuraFamiliarServiceTestData.getTotalEstructuraFamiliars(), estructuraFamiliars.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link EstructuraFamiliarDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_estructuraFamiliar_existe(){
        Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO()).when(estructuraFamiliarServiceAPI).getEstructuraFamiliarDTO(ID_PRIMER_ESTRUCTURAFAMILIAR);
        EstructuraFamiliarDTO estructuraFamiliar = estructuraFamiliarFacadeAPI.getEstructuraFamiliarDTO(ID_PRIMER_ESTRUCTURAFAMILIAR);
        Assert.assertEquals(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO().getId(), estructuraFamiliar.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link EstructuraFamiliarDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_estructuraFamiliar_no_existe() throws PmzException{
        estructuraFamiliarFacadeAPI.guardar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link EstructuraFamiliarDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_estructuraFamiliar_existe() throws PmzException{
        estructuraFamiliarFacadeAPI.editar(EstructuraFamiliarServiceTestData.getEstructuraFamiliarDTO());
    }
    
}
