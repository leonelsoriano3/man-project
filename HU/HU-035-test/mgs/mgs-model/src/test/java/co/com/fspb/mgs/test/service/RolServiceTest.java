package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dao.api.RolDaoAPI;
import co.com.fspb.mgs.service.api.RolServiceAPI;
import co.com.fspb.mgs.service.impl.RolServiceImpl;
import co.com.fspb.mgs.test.service.data.RolServiceTestData;

/**
 * Prueba unitaria para el servicio {@link RolServiceAPI} de la entidad {@link Rol}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolServiceAPI
 * @date nov 11, 2016
 */
public class RolServiceTest {
	
    private static final Long ID_PRIMER_ROL = 1L;

    @InjectMocks
    private RolServiceAPI rolServiceAPI = new RolServiceImpl();

    @Mock
    private RolDaoAPI rolDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolServiceTestData.getRols()).when(rolDaoAPI).getRecords(null);
        Mockito.doReturn(RolServiceTestData.getTotalRols()).when(rolDaoAPI).countRecords(null);
        PmzResultSet<RolDTO> rols = rolServiceAPI.getRecords(null);
        Assert.assertEquals(RolServiceTestData.getTotalRols(), rols.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rol_existe(){
        Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(ID_PRIMER_ROL);
        RolDTO rol = rolServiceAPI.getRolDTO(ID_PRIMER_ROL);
        Assert.assertEquals(RolServiceTestData.getRol().getId(), rol.getId());
    }

    /**
     * Prueba unitaria de get con {@link RolDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rol_no_existe(){
        Mockito.doReturn(null).when(rolDaoAPI).get(ID_PRIMER_ROL);
        RolDTO rol = rolServiceAPI.getRolDTO(ID_PRIMER_ROL);
        Assert.assertEquals(null, rol);
    }

    /**
     * Prueba unitaria de registro de una {@link RolDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_rol_no_existe() throws PmzException{
    	rolServiceAPI.guardar(RolServiceTestData.getRolDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link RolDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_rol_existe() throws PmzException{
        Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(ID_PRIMER_ROL);
        rolServiceAPI.guardar(RolServiceTestData.getRolDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link RolDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_rol_existe() throws PmzException{
        Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(ID_PRIMER_ROL);
        rolServiceAPI.editar(RolServiceTestData.getRolDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link RolDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_rol_no_existe() throws PmzException{
        rolServiceAPI.editar(RolServiceTestData.getRolDTO());
    }
    
    
}
