package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.dao.api.ParametroGeneralDaoAPI;
import co.com.fspb.mgs.service.api.ParametroGeneralServiceAPI;
import co.com.fspb.mgs.service.impl.ParametroGeneralServiceImpl;
import co.com.fspb.mgs.test.service.data.ParametroGeneralServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ParametroGeneralServiceAPI} de la entidad {@link ParametroGeneral}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParametroGeneralServiceAPI
 * @date nov 11, 2016
 */
public class ParametroGeneralServiceTest {
	
    private static final Long ID_PRIMER_PARAMETROGENERAL = 1L;

    @InjectMocks
    private ParametroGeneralServiceAPI parametroGeneralServiceAPI = new ParametroGeneralServiceImpl();

    @Mock
    private ParametroGeneralDaoAPI parametroGeneralDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ParametroGeneralServiceTestData.getParametroGenerals()).when(parametroGeneralDaoAPI).getRecords(null);
        Mockito.doReturn(ParametroGeneralServiceTestData.getTotalParametroGenerals()).when(parametroGeneralDaoAPI).countRecords(null);
        PmzResultSet<ParametroGeneralDTO> parametroGenerals = parametroGeneralServiceAPI.getRecords(null);
        Assert.assertEquals(ParametroGeneralServiceTestData.getTotalParametroGenerals(), parametroGenerals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ParametroGeneralDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parametroGeneral_existe(){
        Mockito.doReturn(ParametroGeneralServiceTestData.getParametroGeneral()).when(parametroGeneralDaoAPI).get(ID_PRIMER_PARAMETROGENERAL);
        ParametroGeneralDTO parametroGeneral = parametroGeneralServiceAPI.getParametroGeneralDTO(ID_PRIMER_PARAMETROGENERAL);
        Assert.assertEquals(ParametroGeneralServiceTestData.getParametroGeneral().getId(), parametroGeneral.getId());
    }

    /**
     * Prueba unitaria de get con {@link ParametroGeneralDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_parametroGeneral_no_existe(){
        Mockito.doReturn(null).when(parametroGeneralDaoAPI).get(ID_PRIMER_PARAMETROGENERAL);
        ParametroGeneralDTO parametroGeneral = parametroGeneralServiceAPI.getParametroGeneralDTO(ID_PRIMER_PARAMETROGENERAL);
        Assert.assertEquals(null, parametroGeneral);
    }

    /**
     * Prueba unitaria de registro de una {@link ParametroGeneralDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_parametroGeneral_no_existe() throws PmzException{
    	parametroGeneralServiceAPI.guardar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ParametroGeneralDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_parametroGeneral_existe() throws PmzException{
        Mockito.doReturn(ParametroGeneralServiceTestData.getParametroGeneral()).when(parametroGeneralDaoAPI).get(ID_PRIMER_PARAMETROGENERAL);
        parametroGeneralServiceAPI.guardar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ParametroGeneralDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_parametroGeneral_existe() throws PmzException{
        Mockito.doReturn(ParametroGeneralServiceTestData.getParametroGeneral()).when(parametroGeneralDaoAPI).get(ID_PRIMER_PARAMETROGENERAL);
        parametroGeneralServiceAPI.editar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ParametroGeneralDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_parametroGeneral_no_existe() throws PmzException{
        parametroGeneralServiceAPI.editar(ParametroGeneralServiceTestData.getParametroGeneralDTO());
    }
    
    
}
