package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.facade.api.IniciativaFacadeAPI;
import co.com.fspb.mgs.facade.impl.IniciativaFacadeImpl;
import co.com.fspb.mgs.service.api.IniciativaServiceAPI;
import co.com.fspb.mgs.test.service.data.IniciativaServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link IniciativaFacadeAPI} de la entidad {@link Iniciativa}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaFacadeTest
 * @date nov 11, 2016
 */
public class IniciativaFacadeTest {

    private static final Long ID_PRIMER_INICIATIVA = 1L;

    @InjectMocks
    private IniciativaFacadeAPI iniciativaFacadeAPI = new IniciativaFacadeImpl();

    @Mock
    private IniciativaServiceAPI iniciativaServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IniciativaServiceTestData.getResultSetIniciativasDTO()).when(iniciativaServiceAPI).getRecords(null);
        PmzResultSet<IniciativaDTO> iniciativas = iniciativaFacadeAPI.getRecords(null);
        Assert.assertEquals(IniciativaServiceTestData.getTotalIniciativas(), iniciativas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IniciativaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_iniciativa_existe(){
        Mockito.doReturn(IniciativaServiceTestData.getIniciativaDTO()).when(iniciativaServiceAPI).getIniciativaDTO(ID_PRIMER_INICIATIVA);
        IniciativaDTO iniciativa = iniciativaFacadeAPI.getIniciativaDTO(ID_PRIMER_INICIATIVA);
        Assert.assertEquals(IniciativaServiceTestData.getIniciativaDTO().getId(), iniciativa.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link IniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_iniciativa_no_existe() throws PmzException{
        iniciativaFacadeAPI.guardar(IniciativaServiceTestData.getIniciativaDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link IniciativaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_iniciativa_existe() throws PmzException{
        iniciativaFacadeAPI.editar(IniciativaServiceTestData.getIniciativaDTO());
    }
    
}
