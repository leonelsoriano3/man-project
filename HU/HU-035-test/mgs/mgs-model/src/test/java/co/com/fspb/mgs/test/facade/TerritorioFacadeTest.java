package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.facade.api.TerritorioFacadeAPI;
import co.com.fspb.mgs.facade.impl.TerritorioFacadeImpl;
import co.com.fspb.mgs.service.api.TerritorioServiceAPI;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TerritorioFacadeAPI} de la entidad {@link Territorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TerritorioFacadeTest
 * @date nov 11, 2016
 */
public class TerritorioFacadeTest {

    private static final Long ID_PRIMER_TERRITORIO = 1L;

    @InjectMocks
    private TerritorioFacadeAPI territorioFacadeAPI = new TerritorioFacadeImpl();

    @Mock
    private TerritorioServiceAPI territorioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TerritorioServiceTestData.getResultSetTerritoriosDTO()).when(territorioServiceAPI).getRecords(null);
        PmzResultSet<TerritorioDTO> territorios = territorioFacadeAPI.getRecords(null);
        Assert.assertEquals(TerritorioServiceTestData.getTotalTerritorios(), territorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_territorio_existe(){
        Mockito.doReturn(TerritorioServiceTestData.getTerritorioDTO()).when(territorioServiceAPI).getTerritorioDTO(ID_PRIMER_TERRITORIO);
        TerritorioDTO territorio = territorioFacadeAPI.getTerritorioDTO(ID_PRIMER_TERRITORIO);
        Assert.assertEquals(TerritorioServiceTestData.getTerritorioDTO().getId(), territorio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_territorio_no_existe() throws PmzException{
        territorioFacadeAPI.guardar(TerritorioServiceTestData.getTerritorioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_territorio_existe() throws PmzException{
        territorioFacadeAPI.editar(TerritorioServiceTestData.getTerritorioDTO());
    }
    
}
