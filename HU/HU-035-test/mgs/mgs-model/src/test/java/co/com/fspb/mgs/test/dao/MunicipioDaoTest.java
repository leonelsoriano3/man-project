package co.com.fspb.mgs.test.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.com.fspb.mgs.dao.api.MunicipioDaoAPI;
import co.com.fspb.mgs.dao.model.Municipio;
import co.com.fspb.mgs.test.dao.data.MunicipioDaoTestData;

/**
 * Prueba unitaria para el DAO {@link MunicipioDaoAPI} de la entidad {@link Municipio}.
 * Extiende la clase abstracta {@link MunicipioDaoTestData} para los metodoós genéricos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioDaoTest
 * @date nov 11, 2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/test-context.xml" })
public class MunicipioDaoTest extends MunicipioDaoTestData {

	private static final int ROWS_TOTAL = 10;
	private static final int INITIAL_ROW = 0;
	private static final int MAX_ROWS = 5;

	@Autowired
	private MunicipioDaoAPI municipioDaoAPI;

	/**
	 * Consulta con {@link PmzPagingCriteria} nulo
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetRecords_pagingCriteria_nulo() {
		municipioDaoAPI.getRecords(null);
	}

	/**
	 * Consulta el total con {@link PmzPagingCriteria} nulo
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCountRecords_pagingCriteria_nulo() {
		municipioDaoAPI.countRecords(null);
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} con atributos nulos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_paging_vacio() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaEmpty());
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

	/**
	 * Consulta el total con {@link PmzPagingCriteria} con atributos nulos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testCountRecords_pagingCriteria_paging_vacio() {
		int total = municipioDaoAPI.countRecords(getPagingCriteriaEmpty())
				.intValue();
		Assert.assertEquals(ROWS_TOTAL, total);
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} atributos vacíos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_atributos_vacios() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaAttr());
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} pagínado
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_paginado() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(createPagingCriteria(null, null, INITIAL_ROW,
						MAX_ROWS, null, null, null));
		Assert.assertEquals(MAX_ROWS, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado ascendente
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_asc_searchs() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaFull("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado y con fechas iniciales
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_initial() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaFullInital("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado y con fechas finales
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_final() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaFullFinal("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} sin filtros
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_des_no_searchs() {
		List<Municipio> lista = municipioDaoAPI
				.getRecords(getPagingCriteriaFull("id", false, false));
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

}
