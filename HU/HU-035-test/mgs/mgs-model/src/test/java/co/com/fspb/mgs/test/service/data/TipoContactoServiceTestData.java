package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TipoContacto;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TipoContacto}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoContactoServiceTestData
 * @date nov 11, 2016
 */
public abstract class TipoContactoServiceTestData {
    
    private static List<TipoContacto> entities = new ArrayList<TipoContacto>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TipoContacto entity = new TipoContacto();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoContacto} Mock 
     * tranformado a DTO {@link TipoContactoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TipoContactoDTO}
     */
    public static TipoContactoDTO getTipoContactoDTO(){
        return DTOTransformer.getTipoContactoDTOFromTipoContacto(getTipoContacto());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoContacto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TipoContacto} Mock
     */
    public static TipoContacto getTipoContacto(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TipoContacto} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TipoContacto}
     */
    public static List<TipoContacto> getTipoContactos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TipoContactoDTO> getResultSetTipoContactosDTO(){
        List<TipoContactoDTO> dtos = new ArrayList<TipoContactoDTO>();
        for (TipoContacto entity : entities) {
            dtos.add(DTOTransformer.getTipoContactoDTOFromTipoContacto(entity));
        }
        return new PmzResultSet<TipoContactoDTO>(dtos, getTotalTipoContactos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTipoContactos(){
        return Long.valueOf(entities.size());
    }

}
