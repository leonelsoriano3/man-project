package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.NivelEducativo;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link NivelEducativo}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoServiceTestData
 * @date nov 11, 2016
 */
public abstract class NivelEducativoServiceTestData {
    
    private static List<NivelEducativo> entities = new ArrayList<NivelEducativo>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            NivelEducativo entity = new NivelEducativo();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link NivelEducativo} Mock 
     * tranformado a DTO {@link NivelEducativoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link NivelEducativoDTO}
     */
    public static NivelEducativoDTO getNivelEducativoDTO(){
        return DTOTransformer.getNivelEducativoDTOFromNivelEducativo(getNivelEducativo());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link NivelEducativo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link NivelEducativo} Mock
     */
    public static NivelEducativo getNivelEducativo(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link NivelEducativo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link NivelEducativo}
     */
    public static List<NivelEducativo> getNivelEducativos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<NivelEducativoDTO> getResultSetNivelEducativosDTO(){
        List<NivelEducativoDTO> dtos = new ArrayList<NivelEducativoDTO>();
        for (NivelEducativo entity : entities) {
            dtos.add(DTOTransformer.getNivelEducativoDTOFromNivelEducativo(entity));
        }
        return new PmzResultSet<NivelEducativoDTO>(dtos, getTotalNivelEducativos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalNivelEducativos(){
        return Long.valueOf(entities.size());
    }

}
