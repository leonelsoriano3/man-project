package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Rol;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Rol}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolServiceTestData
 * @date nov 11, 2016
 */
public abstract class RolServiceTestData {
    
    private static List<Rol> entities = new ArrayList<Rol>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Rol entity = new Rol();

            entity.setId(i);
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setDescripcion("Descripcion" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Rol} Mock 
     * tranformado a DTO {@link RolDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link RolDTO}
     */
    public static RolDTO getRolDTO(){
        return DTOTransformer.getRolDTOFromRol(getRol());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Rol} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Rol} Mock
     */
    public static Rol getRol(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Rol} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Rol}
     */
    public static List<Rol> getRols(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<RolDTO> getResultSetRolsDTO(){
        List<RolDTO> dtos = new ArrayList<RolDTO>();
        for (Rol entity : entities) {
            dtos.add(DTOTransformer.getRolDTOFromRol(entity));
        }
        return new PmzResultSet<RolDTO>(dtos, getTotalRols(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalRols(){
        return Long.valueOf(entities.size());
    }

}
