package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoTerritorioDTO;
import co.com.fspb.mgs.facade.api.TipoTerritorioFacadeAPI;
import co.com.fspb.mgs.facade.impl.TipoTerritorioFacadeImpl;
import co.com.fspb.mgs.service.api.TipoTerritorioServiceAPI;
import co.com.fspb.mgs.test.service.data.TipoTerritorioServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link TipoTerritorioFacadeAPI} de la entidad {@link TipoTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTerritorioFacadeTest
 * @date nov 11, 2016
 */
public class TipoTerritorioFacadeTest {

    private static final Long ID_PRIMER_TIPOTERRITORIO = 1L;

    @InjectMocks
    private TipoTerritorioFacadeAPI tipoTerritorioFacadeAPI = new TipoTerritorioFacadeImpl();

    @Mock
    private TipoTerritorioServiceAPI tipoTerritorioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(TipoTerritorioServiceTestData.getResultSetTipoTerritoriosDTO()).when(tipoTerritorioServiceAPI).getRecords(null);
        PmzResultSet<TipoTerritorioDTO> tipoTerritorios = tipoTerritorioFacadeAPI.getRecords(null);
        Assert.assertEquals(TipoTerritorioServiceTestData.getTotalTipoTerritorios(), tipoTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link TipoTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_tipoTerritorio_existe(){
        Mockito.doReturn(TipoTerritorioServiceTestData.getTipoTerritorioDTO()).when(tipoTerritorioServiceAPI).getTipoTerritorioDTO(ID_PRIMER_TIPOTERRITORIO);
        TipoTerritorioDTO tipoTerritorio = tipoTerritorioFacadeAPI.getTipoTerritorioDTO(ID_PRIMER_TIPOTERRITORIO);
        Assert.assertEquals(TipoTerritorioServiceTestData.getTipoTerritorioDTO().getId(), tipoTerritorio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link TipoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_tipoTerritorio_no_existe() throws PmzException{
        tipoTerritorioFacadeAPI.guardar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link TipoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_tipoTerritorio_existe() throws PmzException{
        tipoTerritorioFacadeAPI.editar(TipoTerritorioServiceTestData.getTipoTerritorioDTO());
    }
    
}
