package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.dao.api.RolUsuarioDaoAPI;
import co.com.fspb.mgs.service.api.RolUsuarioServiceAPI;
import co.com.fspb.mgs.service.impl.RolUsuarioServiceImpl;
import co.com.fspb.mgs.test.service.data.RolUsuarioServiceTestData;
import co.com.fspb.mgs.dao.model.RolUsuarioPK;
  import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dao.api.RolDaoAPI;
import co.com.fspb.mgs.test.service.data.RolServiceTestData;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link RolUsuarioServiceAPI} de la entidad {@link RolUsuario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioServiceAPI
 * @date nov 11, 2016
 */
public class RolUsuarioServiceTest {
	
    private static final RolDTO ROLDTO = RolServiceTestData.getRolDTO();
    private static final UsuarioDTO USUARIODTO = UsuarioServiceTestData.getUsuarioDTO();

    @InjectMocks
    private RolUsuarioServiceAPI rolUsuarioServiceAPI = new RolUsuarioServiceImpl();

    @Mock
    private RolUsuarioDaoAPI rolUsuarioDaoAPI;
    @Mock
    private RolDaoAPI rolDaoAPI;
    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuarios()).when(rolUsuarioDaoAPI).getRecords(null);
        Mockito.doReturn(RolUsuarioServiceTestData.getTotalRolUsuarios()).when(rolUsuarioDaoAPI).countRecords(null);
        PmzResultSet<RolUsuarioDTO> rolUsuarios = rolUsuarioServiceAPI.getRecords(null);
        Assert.assertEquals(RolUsuarioServiceTestData.getTotalRolUsuarios(), rolUsuarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolUsuarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolUsuario_existe(){
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuario()).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
        RolUsuarioDTO rolUsuario = rolUsuarioServiceAPI.getRolUsuarioDTO(ROLDTO, USUARIODTO);
        Assert.assertEquals(RolUsuarioServiceTestData.getRolUsuarioDTO().getId(), rolUsuario.getId());
    }

    /**
     * Prueba unitaria de get con {@link RolUsuarioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolUsuario_no_existe(){
        Mockito.doReturn(null).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
        RolUsuarioDTO rolUsuario = rolUsuarioServiceAPI.getRolUsuarioDTO(ROLDTO, USUARIODTO);
        Assert.assertEquals(null, rolUsuario);
    }

    /**
     * Prueba unitaria de registro de una {@link RolUsuarioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_rolUsuario_no_existe() throws PmzException{
    	Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(RolServiceTestData.getRol().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	rolUsuarioServiceAPI.guardar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link RolUsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_rolUsuario_existe() throws PmzException{
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuario()).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
        rolUsuarioServiceAPI.guardar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link RolUsuarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con rol
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_rolUsuario_no_existe_no_rol() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	rolUsuarioServiceAPI.guardar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link RolUsuarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_rolUsuario_no_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(RolServiceTestData.getRol().getId());
    	rolUsuarioServiceAPI.guardar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link RolUsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_rolUsuario_existe() throws PmzException{
    	Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(RolServiceTestData.getRol().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuario()).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
        rolUsuarioServiceAPI.editar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link RolUsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_rolUsuario_no_existe() throws PmzException{
        rolUsuarioServiceAPI.editar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link RolUsuarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con rol
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_rolUsuario_existe_no_rol() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuario()).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
    	rolUsuarioServiceAPI.editar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link RolUsuarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_rolUsuario_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(RolServiceTestData.getRol()).when(rolDaoAPI).get(RolServiceTestData.getRol().getId());
        Mockito.doReturn(RolUsuarioServiceTestData.getRolUsuario()).when(rolUsuarioDaoAPI).get(Mockito.any(RolUsuarioPK.class));
    	rolUsuarioServiceAPI.editar(RolUsuarioServiceTestData.getRolUsuarioDTO());
    }
    
    
}
