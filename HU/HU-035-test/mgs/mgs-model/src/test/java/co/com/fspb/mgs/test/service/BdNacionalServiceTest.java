package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.dao.api.BdNacionalDaoAPI;
import co.com.fspb.mgs.service.api.BdNacionalServiceAPI;
import co.com.fspb.mgs.service.impl.BdNacionalServiceImpl;
import co.com.fspb.mgs.test.service.data.BdNacionalServiceTestData;

/**
 * Prueba unitaria para el servicio {@link BdNacionalServiceAPI} de la entidad {@link BdNacional}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BdNacionalServiceAPI
 * @date nov 11, 2016
 */
public class BdNacionalServiceTest {
	
    private static final Long ID_PRIMER_BDNACIONAL = 1L;

    @InjectMocks
    private BdNacionalServiceAPI bdNacionalServiceAPI = new BdNacionalServiceImpl();

    @Mock
    private BdNacionalDaoAPI bdNacionalDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BdNacionalServiceTestData.getBdNacionals()).when(bdNacionalDaoAPI).getRecords(null);
        Mockito.doReturn(BdNacionalServiceTestData.getTotalBdNacionals()).when(bdNacionalDaoAPI).countRecords(null);
        PmzResultSet<BdNacionalDTO> bdNacionals = bdNacionalServiceAPI.getRecords(null);
        Assert.assertEquals(BdNacionalServiceTestData.getTotalBdNacionals(), bdNacionals.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BdNacionalDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_bdNacional_existe(){
        Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(ID_PRIMER_BDNACIONAL);
        BdNacionalDTO bdNacional = bdNacionalServiceAPI.getBdNacionalDTO(ID_PRIMER_BDNACIONAL);
        Assert.assertEquals(BdNacionalServiceTestData.getBdNacional().getId(), bdNacional.getId());
    }

    /**
     * Prueba unitaria de get con {@link BdNacionalDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_bdNacional_no_existe(){
        Mockito.doReturn(null).when(bdNacionalDaoAPI).get(ID_PRIMER_BDNACIONAL);
        BdNacionalDTO bdNacional = bdNacionalServiceAPI.getBdNacionalDTO(ID_PRIMER_BDNACIONAL);
        Assert.assertEquals(null, bdNacional);
    }

    /**
     * Prueba unitaria de registro de una {@link BdNacionalDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_bdNacional_no_existe() throws PmzException{
    	bdNacionalServiceAPI.guardar(BdNacionalServiceTestData.getBdNacionalDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link BdNacionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_bdNacional_existe() throws PmzException{
        Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(ID_PRIMER_BDNACIONAL);
        bdNacionalServiceAPI.guardar(BdNacionalServiceTestData.getBdNacionalDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link BdNacionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_bdNacional_existe() throws PmzException{
        Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(ID_PRIMER_BDNACIONAL);
        bdNacionalServiceAPI.editar(BdNacionalServiceTestData.getBdNacionalDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BdNacionalDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_bdNacional_no_existe() throws PmzException{
        bdNacionalServiceAPI.editar(BdNacionalServiceTestData.getBdNacionalDTO());
    }
    
    
}
