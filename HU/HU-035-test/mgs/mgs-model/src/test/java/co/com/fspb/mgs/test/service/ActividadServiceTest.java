package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.dao.api.ActividadDaoAPI;
import co.com.fspb.mgs.service.api.ActividadServiceAPI;
import co.com.fspb.mgs.service.impl.ActividadServiceImpl;
import co.com.fspb.mgs.test.service.data.ActividadServiceTestData;
      import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.dao.api.ComponenteDaoAPI;
import co.com.fspb.mgs.test.service.data.ComponenteServiceTestData;
import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.dao.api.EstadoActividadDaoAPI;
import co.com.fspb.mgs.test.service.data.EstadoActividadServiceTestData;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ActividadServiceAPI} de la entidad {@link Actividad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ActividadServiceAPI
 * @date nov 11, 2016
 */
public class ActividadServiceTest {
	
    private static final Long ID_PRIMER_ACTIVIDAD = 1L;

    @InjectMocks
    private ActividadServiceAPI actividadServiceAPI = new ActividadServiceImpl();

    @Mock
    private ActividadDaoAPI actividadDaoAPI;
    @Mock
    private ComponenteDaoAPI componenteDaoAPI;
    @Mock
    private EstadoActividadDaoAPI estadoActividadDaoAPI;
    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ActividadServiceTestData.getActividads()).when(actividadDaoAPI).getRecords(null);
        Mockito.doReturn(ActividadServiceTestData.getTotalActividads()).when(actividadDaoAPI).countRecords(null);
        PmzResultSet<ActividadDTO> actividads = actividadServiceAPI.getRecords(null);
        Assert.assertEquals(ActividadServiceTestData.getTotalActividads(), actividads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ActividadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_actividad_existe(){
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
        ActividadDTO actividad = actividadServiceAPI.getActividadDTO(ID_PRIMER_ACTIVIDAD);
        Assert.assertEquals(ActividadServiceTestData.getActividad().getId(), actividad.getId());
    }

    /**
     * Prueba unitaria de get con {@link ActividadDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_actividad_no_existe(){
        Mockito.doReturn(null).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
        ActividadDTO actividad = actividadServiceAPI.getActividadDTO(ID_PRIMER_ACTIVIDAD);
        Assert.assertEquals(null, actividad);
    }

    /**
     * Prueba unitaria de registro de una {@link ActividadDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_actividad_no_existe() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	actividadServiceAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_actividad_existe() throws PmzException{
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
        actividadServiceAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ActividadDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con componente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_actividad_no_existe_no_componente() throws PmzException{
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	actividadServiceAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ActividadDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con estadoActividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_actividad_no_existe_no_estadoActividad() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	actividadServiceAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link ActividadDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_actividad_no_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
    	actividadServiceAPI.guardar(ActividadServiceTestData.getActividadDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_actividad_existe() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
        actividadServiceAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_actividad_no_existe() throws PmzException{
        actividadServiceAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con componente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_actividad_existe_no_componente() throws PmzException{
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
    	actividadServiceAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con estadoActividad
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_actividad_existe_no_estadoActividad() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
    	actividadServiceAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ActividadDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_actividad_existe_no_usuario() throws PmzException{
    	Mockito.doReturn(ComponenteServiceTestData.getComponente()).when(componenteDaoAPI).get(ComponenteServiceTestData.getComponente().getId());
    	Mockito.doReturn(EstadoActividadServiceTestData.getEstadoActividad()).when(estadoActividadDaoAPI).get(EstadoActividadServiceTestData.getEstadoActividad().getId());
        Mockito.doReturn(ActividadServiceTestData.getActividad()).when(actividadDaoAPI).get(ID_PRIMER_ACTIVIDAD);
    	actividadServiceAPI.editar(ActividadServiceTestData.getActividadDTO());
    }
    
    
}
