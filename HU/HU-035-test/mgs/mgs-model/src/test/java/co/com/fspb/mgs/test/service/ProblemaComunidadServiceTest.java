package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import co.com.fspb.mgs.dao.api.ProblemaComunidadDaoAPI;
import co.com.fspb.mgs.service.api.ProblemaComunidadServiceAPI;
import co.com.fspb.mgs.service.impl.ProblemaComunidadServiceImpl;
import co.com.fspb.mgs.test.service.data.ProblemaComunidadServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ProblemaComunidadServiceAPI} de la entidad {@link ProblemaComunidad}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProblemaComunidadServiceAPI
 * @date nov 11, 2016
 */
public class ProblemaComunidadServiceTest {
	
    private static final Long ID_PRIMER_PROBLEMACOMUNIDAD = 1L;

    @InjectMocks
    private ProblemaComunidadServiceAPI problemaComunidadServiceAPI = new ProblemaComunidadServiceImpl();

    @Mock
    private ProblemaComunidadDaoAPI problemaComunidadDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidads()).when(problemaComunidadDaoAPI).getRecords(null);
        Mockito.doReturn(ProblemaComunidadServiceTestData.getTotalProblemaComunidads()).when(problemaComunidadDaoAPI).countRecords(null);
        PmzResultSet<ProblemaComunidadDTO> problemaComunidads = problemaComunidadServiceAPI.getRecords(null);
        Assert.assertEquals(ProblemaComunidadServiceTestData.getTotalProblemaComunidads(), problemaComunidads.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProblemaComunidadDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_problemaComunidad_existe(){
        Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ID_PRIMER_PROBLEMACOMUNIDAD);
        ProblemaComunidadDTO problemaComunidad = problemaComunidadServiceAPI.getProblemaComunidadDTO(ID_PRIMER_PROBLEMACOMUNIDAD);
        Assert.assertEquals(ProblemaComunidadServiceTestData.getProblemaComunidad().getId(), problemaComunidad.getId());
    }

    /**
     * Prueba unitaria de get con {@link ProblemaComunidadDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_problemaComunidad_no_existe(){
        Mockito.doReturn(null).when(problemaComunidadDaoAPI).get(ID_PRIMER_PROBLEMACOMUNIDAD);
        ProblemaComunidadDTO problemaComunidad = problemaComunidadServiceAPI.getProblemaComunidadDTO(ID_PRIMER_PROBLEMACOMUNIDAD);
        Assert.assertEquals(null, problemaComunidad);
    }

    /**
     * Prueba unitaria de registro de una {@link ProblemaComunidadDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_problemaComunidad_no_existe() throws PmzException{
    	problemaComunidadServiceAPI.guardar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ProblemaComunidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_problemaComunidad_existe() throws PmzException{
        Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ID_PRIMER_PROBLEMACOMUNIDAD);
        problemaComunidadServiceAPI.guardar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ProblemaComunidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_problemaComunidad_existe() throws PmzException{
        Mockito.doReturn(ProblemaComunidadServiceTestData.getProblemaComunidad()).when(problemaComunidadDaoAPI).get(ID_PRIMER_PROBLEMACOMUNIDAD);
        problemaComunidadServiceAPI.editar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ProblemaComunidadDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_problemaComunidad_no_existe() throws PmzException{
        problemaComunidadServiceAPI.editar(ProblemaComunidadServiceTestData.getProblemaComunidadDTO());
    }
    
    
}
