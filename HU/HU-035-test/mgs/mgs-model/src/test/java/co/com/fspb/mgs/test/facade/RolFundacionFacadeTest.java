package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI;
import co.com.fspb.mgs.facade.impl.RolFundacionFacadeImpl;
import co.com.fspb.mgs.service.api.RolFundacionServiceAPI;
import co.com.fspb.mgs.test.service.data.RolFundacionServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link RolFundacionFacadeAPI} de la entidad {@link RolFundacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFundacionFacadeTest
 * @date nov 11, 2016
 */
public class RolFundacionFacadeTest {

    private static final Long ID_PRIMER_ROLFUNDACION = 1L;

    @InjectMocks
    private RolFundacionFacadeAPI rolFundacionFacadeAPI = new RolFundacionFacadeImpl();

    @Mock
    private RolFundacionServiceAPI rolFundacionServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolFundacionServiceTestData.getResultSetRolFundacionsDTO()).when(rolFundacionServiceAPI).getRecords(null);
        PmzResultSet<RolFundacionDTO> rolFundacions = rolFundacionFacadeAPI.getRecords(null);
        Assert.assertEquals(RolFundacionServiceTestData.getTotalRolFundacions(), rolFundacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolFundacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolFundacion_existe(){
        Mockito.doReturn(RolFundacionServiceTestData.getRolFundacionDTO()).when(rolFundacionServiceAPI).getRolFundacionDTO(ID_PRIMER_ROLFUNDACION);
        RolFundacionDTO rolFundacion = rolFundacionFacadeAPI.getRolFundacionDTO(ID_PRIMER_ROLFUNDACION);
        Assert.assertEquals(RolFundacionServiceTestData.getRolFundacionDTO().getId(), rolFundacion.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link RolFundacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_rolFundacion_no_existe() throws PmzException{
        rolFundacionFacadeAPI.guardar(RolFundacionServiceTestData.getRolFundacionDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link RolFundacionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_rolFundacion_existe() throws PmzException{
        rolFundacionFacadeAPI.editar(RolFundacionServiceTestData.getRolFundacionDTO());
    }
    
}
