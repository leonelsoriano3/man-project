package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.dao.api.ServicioDaoAPI;
import co.com.fspb.mgs.service.api.ServicioServiceAPI;
import co.com.fspb.mgs.service.impl.ServicioServiceImpl;
import co.com.fspb.mgs.test.service.data.ServicioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link ServicioServiceAPI} de la entidad {@link Servicio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioServiceAPI
 * @date nov 11, 2016
 */
public class ServicioServiceTest {
	
    private static final Long ID_PRIMER_SERVICIO = 1L;

    @InjectMocks
    private ServicioServiceAPI servicioServiceAPI = new ServicioServiceImpl();

    @Mock
    private ServicioDaoAPI servicioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ServicioServiceTestData.getServicios()).when(servicioDaoAPI).getRecords(null);
        Mockito.doReturn(ServicioServiceTestData.getTotalServicios()).when(servicioDaoAPI).countRecords(null);
        PmzResultSet<ServicioDTO> servicios = servicioServiceAPI.getRecords(null);
        Assert.assertEquals(ServicioServiceTestData.getTotalServicios(), servicios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ServicioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_servicio_existe(){
        Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ID_PRIMER_SERVICIO);
        ServicioDTO servicio = servicioServiceAPI.getServicioDTO(ID_PRIMER_SERVICIO);
        Assert.assertEquals(ServicioServiceTestData.getServicio().getId(), servicio.getId());
    }

    /**
     * Prueba unitaria de get con {@link ServicioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_servicio_no_existe(){
        Mockito.doReturn(null).when(servicioDaoAPI).get(ID_PRIMER_SERVICIO);
        ServicioDTO servicio = servicioServiceAPI.getServicioDTO(ID_PRIMER_SERVICIO);
        Assert.assertEquals(null, servicio);
    }

    /**
     * Prueba unitaria de registro de una {@link ServicioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_servicio_no_existe() throws PmzException{
    	servicioServiceAPI.guardar(ServicioServiceTestData.getServicioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link ServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_servicio_existe() throws PmzException{
        Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ID_PRIMER_SERVICIO);
        servicioServiceAPI.guardar(ServicioServiceTestData.getServicioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link ServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_servicio_existe() throws PmzException{
        Mockito.doReturn(ServicioServiceTestData.getServicio()).when(servicioDaoAPI).get(ID_PRIMER_SERVICIO);
        servicioServiceAPI.editar(ServicioServiceTestData.getServicioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link ServicioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_servicio_no_existe() throws PmzException{
        servicioServiceAPI.editar(ServicioServiceTestData.getServicioDTO());
    }
    
    
}
