package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.facade.api.CargoFacadeAPI;
import co.com.fspb.mgs.facade.impl.CargoFacadeImpl;
import co.com.fspb.mgs.service.api.CargoServiceAPI;
import co.com.fspb.mgs.test.service.data.CargoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link CargoFacadeAPI} de la entidad {@link Cargo}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class CargoFacadeTest
 * @date nov 11, 2016
 */
public class CargoFacadeTest {

    private static final Long ID_PRIMER_CARGO = 1L;

    @InjectMocks
    private CargoFacadeAPI cargoFacadeAPI = new CargoFacadeImpl();

    @Mock
    private CargoServiceAPI cargoServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(CargoServiceTestData.getResultSetCargosDTO()).when(cargoServiceAPI).getRecords(null);
        PmzResultSet<CargoDTO> cargos = cargoFacadeAPI.getRecords(null);
        Assert.assertEquals(CargoServiceTestData.getTotalCargos(), cargos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link CargoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_cargo_existe(){
        Mockito.doReturn(CargoServiceTestData.getCargoDTO()).when(cargoServiceAPI).getCargoDTO(ID_PRIMER_CARGO);
        CargoDTO cargo = cargoFacadeAPI.getCargoDTO(ID_PRIMER_CARGO);
        Assert.assertEquals(CargoServiceTestData.getCargoDTO().getId(), cargo.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link CargoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_cargo_no_existe() throws PmzException{
        cargoFacadeAPI.guardar(CargoServiceTestData.getCargoDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link CargoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_cargo_existe() throws PmzException{
        cargoFacadeAPI.editar(CargoServiceTestData.getCargoDTO());
    }
    
}
