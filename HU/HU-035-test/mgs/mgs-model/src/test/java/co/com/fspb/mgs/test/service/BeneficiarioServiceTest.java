package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.service.api.BeneficiarioServiceAPI;
import co.com.fspb.mgs.service.impl.BeneficiarioServiceImpl;
import co.com.fspb.mgs.test.service.data.BeneficiarioServiceTestData;
                              import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.dao.api.AreaEducacionDaoAPI;
import co.com.fspb.mgs.test.service.data.AreaEducacionServiceTestData;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.dao.api.EstructuraFamiliarDaoAPI;
import co.com.fspb.mgs.test.service.data.EstructuraFamiliarServiceTestData;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.dao.api.EstadoCivilDaoAPI;
import co.com.fspb.mgs.test.service.data.EstadoCivilServiceTestData;
import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.dao.api.DescIndependienteDaoAPI;
import co.com.fspb.mgs.test.service.data.DescIndependienteServiceTestData;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.dao.api.NivelEducativoDaoAPI;
import co.com.fspb.mgs.test.service.data.NivelEducativoServiceTestData;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.dao.api.TipoViviendaDaoAPI;
import co.com.fspb.mgs.test.service.data.TipoViviendaServiceTestData;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.dao.api.TipoTenenciaDaoAPI;
import co.com.fspb.mgs.test.service.data.TipoTenenciaServiceTestData;
import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.test.service.data.ZonaServiceTestData;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.dao.api.MaterialViviendaDaoAPI;
import co.com.fspb.mgs.test.service.data.MaterialViviendaServiceTestData;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.dao.api.BdNacionalDaoAPI;
import co.com.fspb.mgs.test.service.data.BdNacionalServiceTestData;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.dao.api.AfiliadoSaludDaoAPI;
import co.com.fspb.mgs.test.service.data.AfiliadoSaludServiceTestData;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.dao.api.GrupoEtnicoDaoAPI;
import co.com.fspb.mgs.test.service.data.GrupoEtnicoServiceTestData;
import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.dao.api.OcupacionDaoAPI;
import co.com.fspb.mgs.test.service.data.OcupacionServiceTestData;
import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.dao.api.MunicipioDaoAPI;
import co.com.fspb.mgs.test.service.data.MunicipioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link BeneficiarioServiceAPI} de la entidad {@link Beneficiario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioServiceAPI
 * @date nov 11, 2016
 */
public class BeneficiarioServiceTest {
	
    private static final Long ID_PRIMER_BENEFICIARIO = 1L;

    @InjectMocks
    private BeneficiarioServiceAPI beneficiarioServiceAPI = new BeneficiarioServiceImpl();

    @Mock
    private BeneficiarioDaoAPI beneficiarioDaoAPI;
    @Mock
    private AreaEducacionDaoAPI areaEducacionDaoAPI;
    @Mock
    private EstructuraFamiliarDaoAPI estructuraFamiliarDaoAPI;
    @Mock
    private EstadoCivilDaoAPI estadoCivilDaoAPI;
    @Mock
    private DescIndependienteDaoAPI descIndependienteDaoAPI;
    @Mock
    private NivelEducativoDaoAPI nivelEducativoDaoAPI;
    @Mock
    private TipoViviendaDaoAPI tipoViviendaDaoAPI;
    @Mock
    private TipoTenenciaDaoAPI tipoTenenciaDaoAPI;
    @Mock
    private ZonaDaoAPI zonaDaoAPI;
    @Mock
    private MaterialViviendaDaoAPI materialViviendaDaoAPI;
    @Mock
    private BdNacionalDaoAPI bdNacionalDaoAPI;
    @Mock
    private AfiliadoSaludDaoAPI afiliadoSaludDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;
    @Mock
    private GrupoEtnicoDaoAPI grupoEtnicoDaoAPI;
    @Mock
    private OcupacionDaoAPI ocupacionDaoAPI;
    @Mock
    private MunicipioDaoAPI municipioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiarios()).when(beneficiarioDaoAPI).getRecords(null);
        Mockito.doReturn(BeneficiarioServiceTestData.getTotalBeneficiarios()).when(beneficiarioDaoAPI).countRecords(null);
        PmzResultSet<BeneficiarioDTO> beneficiarios = beneficiarioServiceAPI.getRecords(null);
        Assert.assertEquals(BeneficiarioServiceTestData.getTotalBeneficiarios(), beneficiarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiario_existe(){
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
        BeneficiarioDTO beneficiario = beneficiarioServiceAPI.getBeneficiarioDTO(ID_PRIMER_BENEFICIARIO);
        Assert.assertEquals(BeneficiarioServiceTestData.getBeneficiario().getId(), beneficiario.getId());
    }

    /**
     * Prueba unitaria de get con {@link BeneficiarioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_beneficiario_no_existe(){
        Mockito.doReturn(null).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
        BeneficiarioDTO beneficiario = beneficiarioServiceAPI.getBeneficiarioDTO(ID_PRIMER_BENEFICIARIO);
        Assert.assertEquals(null, beneficiario);
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_beneficiario_no_existe() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_existe() throws PmzException{
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
        beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con areaEducacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_areaEducacion() throws PmzException{
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con estructuraFamiliar
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_estructuraFamiliar() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con estadoCivil
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_estadoCivil() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con descIndependiente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_descIndependiente() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con nivelEducativo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_nivelEducativo() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con tipoVivienda
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_tipoVivienda() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con tipoTenencia
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_tipoTenencia() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_zona() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con materialVivienda
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_materialVivienda() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con bdNacional
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_bdNacional() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con afiliadoSalud
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_afiliadoSalud() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_persona() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con grupoEtnico
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_grupoEtnico() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con ocupacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_ocupacion() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link BeneficiarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con municipio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_beneficiario_no_existe_no_municipio() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	beneficiarioServiceAPI.guardar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_beneficiario_existe() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
        beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_no_existe() throws PmzException{
        beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con areaEducacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_areaEducacion() throws PmzException{
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con estructuraFamiliar
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_estructuraFamiliar() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con estadoCivil
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_estadoCivil() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con descIndependiente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_descIndependiente() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con nivelEducativo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_nivelEducativo() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con tipoVivienda
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_tipoVivienda() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con tipoTenencia
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_tipoTenencia() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con zona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_zona() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con materialVivienda
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_materialVivienda() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con bdNacional
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_bdNacional() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con afiliadoSalud
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_afiliadoSalud() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_persona() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con grupoEtnico
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_grupoEtnico() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con ocupacion
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_ocupacion() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(MunicipioServiceTestData.getMunicipio()).when(municipioDaoAPI).get(MunicipioServiceTestData.getMunicipio().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link BeneficiarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con municipio
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_beneficiario_existe_no_municipio() throws PmzException{
    	Mockito.doReturn(AreaEducacionServiceTestData.getAreaEducacion()).when(areaEducacionDaoAPI).get(AreaEducacionServiceTestData.getAreaEducacion().getId());
    	Mockito.doReturn(EstructuraFamiliarServiceTestData.getEstructuraFamiliar()).when(estructuraFamiliarDaoAPI).get(EstructuraFamiliarServiceTestData.getEstructuraFamiliar().getId());
    	Mockito.doReturn(EstadoCivilServiceTestData.getEstadoCivil()).when(estadoCivilDaoAPI).get(EstadoCivilServiceTestData.getEstadoCivil().getId());
    	Mockito.doReturn(DescIndependienteServiceTestData.getDescIndependiente()).when(descIndependienteDaoAPI).get(DescIndependienteServiceTestData.getDescIndependiente().getId());
    	Mockito.doReturn(NivelEducativoServiceTestData.getNivelEducativo()).when(nivelEducativoDaoAPI).get(NivelEducativoServiceTestData.getNivelEducativo().getId());
    	Mockito.doReturn(TipoViviendaServiceTestData.getTipoVivienda()).when(tipoViviendaDaoAPI).get(TipoViviendaServiceTestData.getTipoVivienda().getId());
    	Mockito.doReturn(TipoTenenciaServiceTestData.getTipoTenencia()).when(tipoTenenciaDaoAPI).get(TipoTenenciaServiceTestData.getTipoTenencia().getId());
    	Mockito.doReturn(ZonaServiceTestData.getZona()).when(zonaDaoAPI).get(ZonaServiceTestData.getZona().getId());
    	Mockito.doReturn(MaterialViviendaServiceTestData.getMaterialVivienda()).when(materialViviendaDaoAPI).get(MaterialViviendaServiceTestData.getMaterialVivienda().getId());
    	Mockito.doReturn(BdNacionalServiceTestData.getBdNacional()).when(bdNacionalDaoAPI).get(BdNacionalServiceTestData.getBdNacional().getId());
    	Mockito.doReturn(AfiliadoSaludServiceTestData.getAfiliadoSalud()).when(afiliadoSaludDaoAPI).get(AfiliadoSaludServiceTestData.getAfiliadoSalud().getId());
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(GrupoEtnicoServiceTestData.getGrupoEtnico()).when(grupoEtnicoDaoAPI).get(GrupoEtnicoServiceTestData.getGrupoEtnico().getId());
    	Mockito.doReturn(OcupacionServiceTestData.getOcupacion()).when(ocupacionDaoAPI).get(OcupacionServiceTestData.getOcupacion().getId());
        Mockito.doReturn(BeneficiarioServiceTestData.getBeneficiario()).when(beneficiarioDaoAPI).get(ID_PRIMER_BENEFICIARIO);
    	beneficiarioServiceAPI.editar(BeneficiarioServiceTestData.getBeneficiarioDTO());
    }
    
    
}
