package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.TipoTerritorio;
import co.com.fspb.mgs.dto.TipoTerritorioDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link TipoTerritorio}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTerritorioServiceTestData
 * @date nov 11, 2016
 */
public abstract class TipoTerritorioServiceTestData {
    
    private static List<TipoTerritorio> entities = new ArrayList<TipoTerritorio>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            TipoTerritorio entity = new TipoTerritorio();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setZona(ZonaServiceTestData.getZona());
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoTerritorio} Mock 
     * tranformado a DTO {@link TipoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link TipoTerritorioDTO}
     */
    public static TipoTerritorioDTO getTipoTerritorioDTO(){
        return DTOTransformer.getTipoTerritorioDTOFromTipoTerritorio(getTipoTerritorio());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link TipoTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link TipoTerritorio} Mock
     */
    public static TipoTerritorio getTipoTerritorio(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link TipoTerritorio} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link TipoTerritorio}
     */
    public static List<TipoTerritorio> getTipoTerritorios(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<TipoTerritorioDTO> getResultSetTipoTerritoriosDTO(){
        List<TipoTerritorioDTO> dtos = new ArrayList<TipoTerritorioDTO>();
        for (TipoTerritorio entity : entities) {
            dtos.add(DTOTransformer.getTipoTerritorioDTOFromTipoTerritorio(entity));
        }
        return new PmzResultSet<TipoTerritorioDTO>(dtos, getTotalTipoTerritorios(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalTipoTerritorios(){
        return Long.valueOf(entities.size());
    }

}
