package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.dao.api.RolFundacionDaoAPI;
import co.com.fspb.mgs.service.api.RolFundacionServiceAPI;
import co.com.fspb.mgs.service.impl.RolFundacionServiceImpl;
import co.com.fspb.mgs.test.service.data.RolFundacionServiceTestData;

/**
 * Prueba unitaria para el servicio {@link RolFundacionServiceAPI} de la entidad {@link RolFundacion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFundacionServiceAPI
 * @date nov 11, 2016
 */
public class RolFundacionServiceTest {
	
    private static final Long ID_PRIMER_ROLFUNDACION = 1L;

    @InjectMocks
    private RolFundacionServiceAPI rolFundacionServiceAPI = new RolFundacionServiceImpl();

    @Mock
    private RolFundacionDaoAPI rolFundacionDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolFundacionServiceTestData.getRolFundacions()).when(rolFundacionDaoAPI).getRecords(null);
        Mockito.doReturn(RolFundacionServiceTestData.getTotalRolFundacions()).when(rolFundacionDaoAPI).countRecords(null);
        PmzResultSet<RolFundacionDTO> rolFundacions = rolFundacionServiceAPI.getRecords(null);
        Assert.assertEquals(RolFundacionServiceTestData.getTotalRolFundacions(), rolFundacions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolFundacionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolFundacion_existe(){
        Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(ID_PRIMER_ROLFUNDACION);
        RolFundacionDTO rolFundacion = rolFundacionServiceAPI.getRolFundacionDTO(ID_PRIMER_ROLFUNDACION);
        Assert.assertEquals(RolFundacionServiceTestData.getRolFundacion().getId(), rolFundacion.getId());
    }

    /**
     * Prueba unitaria de get con {@link RolFundacionDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rolFundacion_no_existe(){
        Mockito.doReturn(null).when(rolFundacionDaoAPI).get(ID_PRIMER_ROLFUNDACION);
        RolFundacionDTO rolFundacion = rolFundacionServiceAPI.getRolFundacionDTO(ID_PRIMER_ROLFUNDACION);
        Assert.assertEquals(null, rolFundacion);
    }

    /**
     * Prueba unitaria de registro de una {@link RolFundacionDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_rolFundacion_no_existe() throws PmzException{
    	rolFundacionServiceAPI.guardar(RolFundacionServiceTestData.getRolFundacionDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link RolFundacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_rolFundacion_existe() throws PmzException{
        Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(ID_PRIMER_ROLFUNDACION);
        rolFundacionServiceAPI.guardar(RolFundacionServiceTestData.getRolFundacionDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link RolFundacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_rolFundacion_existe() throws PmzException{
        Mockito.doReturn(RolFundacionServiceTestData.getRolFundacion()).when(rolFundacionDaoAPI).get(ID_PRIMER_ROLFUNDACION);
        rolFundacionServiceAPI.editar(RolFundacionServiceTestData.getRolFundacionDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link RolFundacionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_rolFundacion_no_existe() throws PmzException{
        rolFundacionServiceAPI.editar(RolFundacionServiceTestData.getRolFundacionDTO());
    }
    
    
}
