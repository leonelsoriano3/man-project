package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Linea;
import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Linea}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaServiceTestData
 * @date nov 11, 2016
 */
public abstract class LineaServiceTestData {
    
    private static List<Linea> entities = new ArrayList<Linea>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Linea entity = new Linea();

            entity.setId(i);
            
            entity.setNombre("Nombre" + i);
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setObjetivo(ObjetivoServiceTestData.getObjetivo());
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Linea} Mock 
     * tranformado a DTO {@link LineaDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link LineaDTO}
     */
    public static LineaDTO getLineaDTO(){
        return DTOTransformer.getLineaDTOFromLinea(getLinea());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Linea} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Linea} Mock
     */
    public static Linea getLinea(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Linea} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Linea}
     */
    public static List<Linea> getLineas(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<LineaDTO> getResultSetLineasDTO(){
        List<LineaDTO> dtos = new ArrayList<LineaDTO>();
        for (Linea entity : entities) {
            dtos.add(DTOTransformer.getLineaDTOFromLinea(entity));
        }
        return new PmzResultSet<LineaDTO>(dtos, getTotalLineas(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalLineas(){
        return Long.valueOf(entities.size());
    }

}
