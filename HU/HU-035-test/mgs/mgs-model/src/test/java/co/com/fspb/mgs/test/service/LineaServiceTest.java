package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.dao.api.LineaDaoAPI;
import co.com.fspb.mgs.service.api.LineaServiceAPI;
import co.com.fspb.mgs.service.impl.LineaServiceImpl;
import co.com.fspb.mgs.test.service.data.LineaServiceTestData;
  import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.test.service.data.ObjetivoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link LineaServiceAPI} de la entidad {@link Linea}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaServiceAPI
 * @date nov 11, 2016
 */
public class LineaServiceTest {
	
    private static final Long ID_PRIMER_LINEA = 1L;

    @InjectMocks
    private LineaServiceAPI lineaServiceAPI = new LineaServiceImpl();

    @Mock
    private LineaDaoAPI lineaDaoAPI;
    @Mock
    private ObjetivoDaoAPI objetivoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LineaServiceTestData.getLineas()).when(lineaDaoAPI).getRecords(null);
        Mockito.doReturn(LineaServiceTestData.getTotalLineas()).when(lineaDaoAPI).countRecords(null);
        PmzResultSet<LineaDTO> lineas = lineaServiceAPI.getRecords(null);
        Assert.assertEquals(LineaServiceTestData.getTotalLineas(), lineas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LineaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_linea_existe(){
        Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(ID_PRIMER_LINEA);
        LineaDTO linea = lineaServiceAPI.getLineaDTO(ID_PRIMER_LINEA);
        Assert.assertEquals(LineaServiceTestData.getLinea().getId(), linea.getId());
    }

    /**
     * Prueba unitaria de get con {@link LineaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_linea_no_existe(){
        Mockito.doReturn(null).when(lineaDaoAPI).get(ID_PRIMER_LINEA);
        LineaDTO linea = lineaServiceAPI.getLineaDTO(ID_PRIMER_LINEA);
        Assert.assertEquals(null, linea);
    }

    /**
     * Prueba unitaria de registro de una {@link LineaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_linea_no_existe() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
    	lineaServiceAPI.guardar(LineaServiceTestData.getLineaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link LineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_linea_existe() throws PmzException{
        Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(ID_PRIMER_LINEA);
        lineaServiceAPI.guardar(LineaServiceTestData.getLineaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link LineaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_linea_no_existe_no_objetivo() throws PmzException{
    	lineaServiceAPI.guardar(LineaServiceTestData.getLineaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link LineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_linea_existe() throws PmzException{
    	Mockito.doReturn(ObjetivoServiceTestData.getObjetivo()).when(objetivoDaoAPI).get(ObjetivoServiceTestData.getObjetivo().getId());
        Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(ID_PRIMER_LINEA);
        lineaServiceAPI.editar(LineaServiceTestData.getLineaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LineaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_linea_no_existe() throws PmzException{
        lineaServiceAPI.editar(LineaServiceTestData.getLineaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LineaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con objetivo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_linea_existe_no_objetivo() throws PmzException{
        Mockito.doReturn(LineaServiceTestData.getLinea()).when(lineaDaoAPI).get(ID_PRIMER_LINEA);
    	lineaServiceAPI.editar(LineaServiceTestData.getLineaDTO());
    }
    
    
}
