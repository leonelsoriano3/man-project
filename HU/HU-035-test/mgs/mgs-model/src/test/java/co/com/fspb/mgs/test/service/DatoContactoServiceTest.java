package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.dao.api.DatoContactoDaoAPI;
import co.com.fspb.mgs.service.api.DatoContactoServiceAPI;
import co.com.fspb.mgs.service.impl.DatoContactoServiceImpl;
import co.com.fspb.mgs.test.service.data.DatoContactoServiceTestData;
    import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.dao.api.TipoContactoDaoAPI;
import co.com.fspb.mgs.test.service.data.TipoContactoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link DatoContactoServiceAPI} de la entidad {@link DatoContacto}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoContactoServiceAPI
 * @date nov 11, 2016
 */
public class DatoContactoServiceTest {
	
    private static final Long ID_PRIMER_DATOCONTACTO = 1L;

    @InjectMocks
    private DatoContactoServiceAPI datoContactoServiceAPI = new DatoContactoServiceImpl();

    @Mock
    private DatoContactoDaoAPI datoContactoDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;
    @Mock
    private TipoContactoDaoAPI tipoContactoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContactos()).when(datoContactoDaoAPI).getRecords(null);
        Mockito.doReturn(DatoContactoServiceTestData.getTotalDatoContactos()).when(datoContactoDaoAPI).countRecords(null);
        PmzResultSet<DatoContactoDTO> datoContactos = datoContactoServiceAPI.getRecords(null);
        Assert.assertEquals(DatoContactoServiceTestData.getTotalDatoContactos(), datoContactos.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link DatoContactoDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoContacto_existe(){
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContacto()).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
        DatoContactoDTO datoContacto = datoContactoServiceAPI.getDatoContactoDTO(ID_PRIMER_DATOCONTACTO);
        Assert.assertEquals(DatoContactoServiceTestData.getDatoContacto().getId(), datoContacto.getId());
    }

    /**
     * Prueba unitaria de get con {@link DatoContactoDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_datoContacto_no_existe(){
        Mockito.doReturn(null).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
        DatoContactoDTO datoContacto = datoContactoServiceAPI.getDatoContactoDTO(ID_PRIMER_DATOCONTACTO);
        Assert.assertEquals(null, datoContacto);
    }

    /**
     * Prueba unitaria de registro de una {@link DatoContactoDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_datoContacto_no_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(TipoContactoServiceTestData.getTipoContacto().getId());
    	datoContactoServiceAPI.guardar(DatoContactoServiceTestData.getDatoContactoDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link DatoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_datoContacto_existe() throws PmzException{
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContacto()).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
        datoContactoServiceAPI.guardar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link DatoContactoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_datoContacto_no_existe_no_persona() throws PmzException{
    	Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(TipoContactoServiceTestData.getTipoContacto().getId());
    	datoContactoServiceAPI.guardar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link DatoContactoDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con tipoContacto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_datoContacto_no_existe_no_tipoContacto() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	datoContactoServiceAPI.guardar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link DatoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_datoContacto_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(TipoContactoServiceTestData.getTipoContacto().getId());
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContacto()).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
        datoContactoServiceAPI.editar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DatoContactoDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_datoContacto_no_existe() throws PmzException{
        datoContactoServiceAPI.editar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DatoContactoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_datoContacto_existe_no_persona() throws PmzException{
    	Mockito.doReturn(TipoContactoServiceTestData.getTipoContacto()).when(tipoContactoDaoAPI).get(TipoContactoServiceTestData.getTipoContacto().getId());
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContacto()).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
    	datoContactoServiceAPI.editar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link DatoContactoDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con tipoContacto
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_datoContacto_existe_no_tipoContacto() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
        Mockito.doReturn(DatoContactoServiceTestData.getDatoContacto()).when(datoContactoDaoAPI).get(ID_PRIMER_DATOCONTACTO);
    	datoContactoServiceAPI.editar(DatoContactoServiceTestData.getDatoContactoDTO());
    }
    
    
}
