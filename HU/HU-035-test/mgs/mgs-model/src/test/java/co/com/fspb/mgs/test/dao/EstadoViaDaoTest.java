package co.com.fspb.mgs.test.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.com.fspb.mgs.dao.api.EstadoViaDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoVia;
import co.com.fspb.mgs.test.dao.data.EstadoViaDaoTestData;

/**
 * Prueba unitaria para el DAO {@link EstadoViaDaoAPI} de la entidad {@link EstadoVia}.
 * Extiende la clase abstracta {@link EstadoViaDaoTestData} para los metodoós genéricos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoViaDaoTest
 * @date nov 11, 2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/test-context.xml" })
public class EstadoViaDaoTest extends EstadoViaDaoTestData {

	private static final int ROWS_TOTAL = 10;
	private static final int INITIAL_ROW = 0;
	private static final int MAX_ROWS = 5;

	@Autowired
	private EstadoViaDaoAPI estadoViaDaoAPI;

	/**
	 * Consulta con {@link PmzPagingCriteria} nulo
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetRecords_pagingCriteria_nulo() {
		estadoViaDaoAPI.getRecords(null);
	}

	/**
	 * Consulta el total con {@link PmzPagingCriteria} nulo
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCountRecords_pagingCriteria_nulo() {
		estadoViaDaoAPI.countRecords(null);
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} con atributos nulos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_paging_vacio() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaEmpty());
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

	/**
	 * Consulta el total con {@link PmzPagingCriteria} con atributos nulos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testCountRecords_pagingCriteria_paging_vacio() {
		int total = estadoViaDaoAPI.countRecords(getPagingCriteriaEmpty())
				.intValue();
		Assert.assertEquals(ROWS_TOTAL, total);
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} atributos vacíos
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_atributos_vacios() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaAttr());
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} pagínado
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_paginado() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(createPagingCriteria(null, null, INITIAL_ROW,
						MAX_ROWS, null, null, null));
		Assert.assertEquals(MAX_ROWS, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado ascendente
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_asc_searchs() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaFull("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado y con fechas iniciales
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_initial() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaFullInital("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} filtrado y con fechas finales
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_final() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaFullFinal("id", true, true));
		Assert.assertEquals(1, lista.size());
	}

	/**
	 * Consulta con {@link PmzPagingCriteria} sin filtros
	 * 
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 */
	@Test
	public void testGetRecords_pagingCriteria_full_des_no_searchs() {
		List<EstadoVia> lista = estadoViaDaoAPI
				.getRecords(getPagingCriteriaFull("id", false, false));
		Assert.assertEquals(ROWS_TOTAL, lista.size());
	}

}
