package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.service.api.IniciativaServiceAPI;
import co.com.fspb.mgs.service.impl.IniciativaServiceImpl;
import co.com.fspb.mgs.test.service.data.IniciativaServiceTestData;
  import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;

/**
 * Prueba unitaria para el servicio {@link IniciativaServiceAPI} de la entidad {@link Iniciativa}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IniciativaServiceAPI
 * @date nov 11, 2016
 */
public class IniciativaServiceTest {
	
    private static final Long ID_PRIMER_INICIATIVA = 1L;

    @InjectMocks
    private IniciativaServiceAPI iniciativaServiceAPI = new IniciativaServiceImpl();

    @Mock
    private IniciativaDaoAPI iniciativaDaoAPI;
    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(IniciativaServiceTestData.getIniciativas()).when(iniciativaDaoAPI).getRecords(null);
        Mockito.doReturn(IniciativaServiceTestData.getTotalIniciativas()).when(iniciativaDaoAPI).countRecords(null);
        PmzResultSet<IniciativaDTO> iniciativas = iniciativaServiceAPI.getRecords(null);
        Assert.assertEquals(IniciativaServiceTestData.getTotalIniciativas(), iniciativas.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link IniciativaDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_iniciativa_existe(){
        Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(ID_PRIMER_INICIATIVA);
        IniciativaDTO iniciativa = iniciativaServiceAPI.getIniciativaDTO(ID_PRIMER_INICIATIVA);
        Assert.assertEquals(IniciativaServiceTestData.getIniciativa().getId(), iniciativa.getId());
    }

    /**
     * Prueba unitaria de get con {@link IniciativaDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_iniciativa_no_existe(){
        Mockito.doReturn(null).when(iniciativaDaoAPI).get(ID_PRIMER_INICIATIVA);
        IniciativaDTO iniciativa = iniciativaServiceAPI.getIniciativaDTO(ID_PRIMER_INICIATIVA);
        Assert.assertEquals(null, iniciativa);
    }

    /**
     * Prueba unitaria de registro de una {@link IniciativaDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_iniciativa_no_existe() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
    	iniciativaServiceAPI.guardar(IniciativaServiceTestData.getIniciativaDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link IniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_iniciativa_existe() throws PmzException{
        Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(ID_PRIMER_INICIATIVA);
        iniciativaServiceAPI.guardar(IniciativaServiceTestData.getIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link IniciativaDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_iniciativa_no_existe_no_usuario() throws PmzException{
    	iniciativaServiceAPI.guardar(IniciativaServiceTestData.getIniciativaDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link IniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_iniciativa_existe() throws PmzException{
    	Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(UsuarioServiceTestData.getUsuario().getId());
        Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(ID_PRIMER_INICIATIVA);
        iniciativaServiceAPI.editar(IniciativaServiceTestData.getIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IniciativaDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_iniciativa_no_existe() throws PmzException{
        iniciativaServiceAPI.editar(IniciativaServiceTestData.getIniciativaDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link IniciativaDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con usuario
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_iniciativa_existe_no_usuario() throws PmzException{
        Mockito.doReturn(IniciativaServiceTestData.getIniciativa()).when(iniciativaDaoAPI).get(ID_PRIMER_INICIATIVA);
    	iniciativaServiceAPI.editar(IniciativaServiceTestData.getIniciativaDTO());
    }
    
    
}
