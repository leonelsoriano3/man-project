package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.LineaIntervencion;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link LineaIntervencion}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaIntervencionServiceTestData
 * @date nov 11, 2016
 */
public abstract class LineaIntervencionServiceTestData {
    
    private static List<LineaIntervencion> entities = new ArrayList<LineaIntervencion>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            LineaIntervencion entity = new LineaIntervencion();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link LineaIntervencion} Mock 
     * tranformado a DTO {@link LineaIntervencionDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link LineaIntervencionDTO}
     */
    public static LineaIntervencionDTO getLineaIntervencionDTO(){
        return DTOTransformer.getLineaIntervencionDTOFromLineaIntervencion(getLineaIntervencion());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link LineaIntervencion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link LineaIntervencion} Mock
     */
    public static LineaIntervencion getLineaIntervencion(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link LineaIntervencion} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link LineaIntervencion}
     */
    public static List<LineaIntervencion> getLineaIntervencions(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<LineaIntervencionDTO> getResultSetLineaIntervencionsDTO(){
        List<LineaIntervencionDTO> dtos = new ArrayList<LineaIntervencionDTO>();
        for (LineaIntervencion entity : entities) {
            dtos.add(DTOTransformer.getLineaIntervencionDTOFromLineaIntervencion(entity));
        }
        return new PmzResultSet<LineaIntervencionDTO>(dtos, getTotalLineaIntervencions(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalLineaIntervencions(){
        return Long.valueOf(entities.size());
    }

}
