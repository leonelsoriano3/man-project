package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.ParientePK;
import co.com.fspb.mgs.dao.model.Pariente;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Pariente}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParienteServiceTestData
 * @date nov 11, 2016
 */
public abstract class ParienteServiceTestData {
    
    private static List<Pariente> entities = new ArrayList<Pariente>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Pariente entity = new Pariente();

		    ParientePK pk = new ParientePK();
            pk.setBeneficiario(BeneficiarioServiceTestData.getBeneficiario());
            pk.setPersona(PersonaServiceTestData.getPersona());
            entity.setId(pk);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setParentesco(ParentescoServiceTestData.getParentesco());
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Pariente} Mock 
     * tranformado a DTO {@link ParienteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link ParienteDTO}
     */
    public static ParienteDTO getParienteDTO(){
        return DTOTransformer.getParienteDTOFromPariente(getPariente());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Pariente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Pariente} Mock
     */
    public static Pariente getPariente(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Pariente} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Pariente}
     */
    public static List<Pariente> getParientes(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<ParienteDTO> getResultSetParientesDTO(){
        List<ParienteDTO> dtos = new ArrayList<ParienteDTO>();
        for (Pariente entity : entities) {
            dtos.add(DTOTransformer.getParienteDTOFromPariente(entity));
        }
        return new PmzResultSet<ParienteDTO>(dtos, getTotalParientes(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalParientes(){
        return Long.valueOf(entities.size());
    }

}
