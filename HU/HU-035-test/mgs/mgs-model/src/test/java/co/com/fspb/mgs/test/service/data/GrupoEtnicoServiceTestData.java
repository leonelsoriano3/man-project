package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.GrupoEtnico;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link GrupoEtnico}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class GrupoEtnicoServiceTestData
 * @date nov 11, 2016
 */
public abstract class GrupoEtnicoServiceTestData {
    
    private static List<GrupoEtnico> entities = new ArrayList<GrupoEtnico>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            GrupoEtnico entity = new GrupoEtnico();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link GrupoEtnico} Mock 
     * tranformado a DTO {@link GrupoEtnicoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link GrupoEtnicoDTO}
     */
    public static GrupoEtnicoDTO getGrupoEtnicoDTO(){
        return DTOTransformer.getGrupoEtnicoDTOFromGrupoEtnico(getGrupoEtnico());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link GrupoEtnico} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link GrupoEtnico} Mock
     */
    public static GrupoEtnico getGrupoEtnico(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link GrupoEtnico} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link GrupoEtnico}
     */
    public static List<GrupoEtnico> getGrupoEtnicos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<GrupoEtnicoDTO> getResultSetGrupoEtnicosDTO(){
        List<GrupoEtnicoDTO> dtos = new ArrayList<GrupoEtnicoDTO>();
        for (GrupoEtnico entity : entities) {
            dtos.add(DTOTransformer.getGrupoEtnicoDTOFromGrupoEtnico(entity));
        }
        return new PmzResultSet<GrupoEtnicoDTO>(dtos, getTotalGrupoEtnicos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalGrupoEtnicos(){
        return Long.valueOf(entities.size());
    }

}
