package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AsistenteDTO;
import co.com.fspb.mgs.facade.api.AsistenteFacadeAPI;
import co.com.fspb.mgs.facade.impl.AsistenteFacadeImpl;
import co.com.fspb.mgs.service.api.AsistenteServiceAPI;
import co.com.fspb.mgs.test.service.data.AsistenteServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link AsistenteFacadeAPI} de la entidad {@link Asistente}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AsistenteFacadeTest
 * @date nov 11, 2016
 */
public class AsistenteFacadeTest {

    private static final Long ID_PRIMER_ASISTENTE = 1L;

    @InjectMocks
    private AsistenteFacadeAPI asistenteFacadeAPI = new AsistenteFacadeImpl();

    @Mock
    private AsistenteServiceAPI asistenteServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(AsistenteServiceTestData.getResultSetAsistentesDTO()).when(asistenteServiceAPI).getRecords(null);
        PmzResultSet<AsistenteDTO> asistentes = asistenteFacadeAPI.getRecords(null);
        Assert.assertEquals(AsistenteServiceTestData.getTotalAsistentes(), asistentes.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link AsistenteDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_asistente_existe(){
        Mockito.doReturn(AsistenteServiceTestData.getAsistenteDTO()).when(asistenteServiceAPI).getAsistenteDTO(ID_PRIMER_ASISTENTE);
        AsistenteDTO asistente = asistenteFacadeAPI.getAsistenteDTO(ID_PRIMER_ASISTENTE);
        Assert.assertEquals(AsistenteServiceTestData.getAsistenteDTO().getId(), asistente.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link AsistenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_asistente_no_existe() throws PmzException{
        asistenteFacadeAPI.guardar(AsistenteServiceTestData.getAsistenteDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link AsistenteDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_asistente_existe() throws PmzException{
        asistenteFacadeAPI.editar(AsistenteServiceTestData.getAsistenteDTO());
    }
    
}
