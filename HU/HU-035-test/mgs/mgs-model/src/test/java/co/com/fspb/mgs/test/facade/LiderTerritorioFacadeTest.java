package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.facade.api.LiderTerritorioFacadeAPI;
import co.com.fspb.mgs.facade.impl.LiderTerritorioFacadeImpl;
import co.com.fspb.mgs.service.api.LiderTerritorioServiceAPI;
import co.com.fspb.mgs.test.service.data.LiderTerritorioServiceTestData;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.test.service.data.LiderServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link LiderTerritorioFacadeAPI} de la entidad {@link LiderTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioFacadeTest
 * @date nov 11, 2016
 */
public class LiderTerritorioFacadeTest {

    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final LiderDTO LIDERDTO = LiderServiceTestData.getLiderDTO();

    @InjectMocks
    private LiderTerritorioFacadeAPI liderTerritorioFacadeAPI = new LiderTerritorioFacadeImpl();

    @Mock
    private LiderTerritorioServiceAPI liderTerritorioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LiderTerritorioServiceTestData.getResultSetLiderTerritoriosDTO()).when(liderTerritorioServiceAPI).getRecords(null);
        PmzResultSet<LiderTerritorioDTO> liderTerritorios = liderTerritorioFacadeAPI.getRecords(null);
        Assert.assertEquals(LiderTerritorioServiceTestData.getTotalLiderTerritorios(), liderTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LiderTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_liderTerritorio_existe(){
        Mockito.doReturn(LiderTerritorioServiceTestData.getLiderTerritorioDTO()).when(liderTerritorioServiceAPI).getLiderTerritorioDTO(TERRITORIODTO, LIDERDTO);
        LiderTerritorioDTO liderTerritorio = liderTerritorioFacadeAPI.getLiderTerritorioDTO(TERRITORIODTO, LIDERDTO);
        Assert.assertEquals(LiderTerritorioServiceTestData.getLiderTerritorioDTO().getId(), liderTerritorio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link LiderTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_liderTerritorio_no_existe() throws PmzException{
        liderTerritorioFacadeAPI.guardar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link LiderTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_liderTerritorio_existe() throws PmzException{
        liderTerritorioFacadeAPI.editar(LiderTerritorioServiceTestData.getLiderTerritorioDTO());
    }
    
}
