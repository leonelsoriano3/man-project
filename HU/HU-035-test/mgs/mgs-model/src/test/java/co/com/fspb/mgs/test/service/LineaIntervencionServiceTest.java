package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.dao.api.LineaIntervencionDaoAPI;
import co.com.fspb.mgs.service.api.LineaIntervencionServiceAPI;
import co.com.fspb.mgs.service.impl.LineaIntervencionServiceImpl;
import co.com.fspb.mgs.test.service.data.LineaIntervencionServiceTestData;

/**
 * Prueba unitaria para el servicio {@link LineaIntervencionServiceAPI} de la entidad {@link LineaIntervencion}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaIntervencionServiceAPI
 * @date nov 11, 2016
 */
public class LineaIntervencionServiceTest {
	
    private static final Long ID_PRIMER_LINEAINTERVENCION = 1L;

    @InjectMocks
    private LineaIntervencionServiceAPI lineaIntervencionServiceAPI = new LineaIntervencionServiceImpl();

    @Mock
    private LineaIntervencionDaoAPI lineaIntervencionDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencions()).when(lineaIntervencionDaoAPI).getRecords(null);
        Mockito.doReturn(LineaIntervencionServiceTestData.getTotalLineaIntervencions()).when(lineaIntervencionDaoAPI).countRecords(null);
        PmzResultSet<LineaIntervencionDTO> lineaIntervencions = lineaIntervencionServiceAPI.getRecords(null);
        Assert.assertEquals(LineaIntervencionServiceTestData.getTotalLineaIntervencions(), lineaIntervencions.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link LineaIntervencionDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lineaIntervencion_existe(){
        Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(ID_PRIMER_LINEAINTERVENCION);
        LineaIntervencionDTO lineaIntervencion = lineaIntervencionServiceAPI.getLineaIntervencionDTO(ID_PRIMER_LINEAINTERVENCION);
        Assert.assertEquals(LineaIntervencionServiceTestData.getLineaIntervencion().getId(), lineaIntervencion.getId());
    }

    /**
     * Prueba unitaria de get con {@link LineaIntervencionDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_lineaIntervencion_no_existe(){
        Mockito.doReturn(null).when(lineaIntervencionDaoAPI).get(ID_PRIMER_LINEAINTERVENCION);
        LineaIntervencionDTO lineaIntervencion = lineaIntervencionServiceAPI.getLineaIntervencionDTO(ID_PRIMER_LINEAINTERVENCION);
        Assert.assertEquals(null, lineaIntervencion);
    }

    /**
     * Prueba unitaria de registro de una {@link LineaIntervencionDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_lineaIntervencion_no_existe() throws PmzException{
    	lineaIntervencionServiceAPI.guardar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link LineaIntervencionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_lineaIntervencion_existe() throws PmzException{
        Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(ID_PRIMER_LINEAINTERVENCION);
        lineaIntervencionServiceAPI.guardar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link LineaIntervencionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_lineaIntervencion_existe() throws PmzException{
        Mockito.doReturn(LineaIntervencionServiceTestData.getLineaIntervencion()).when(lineaIntervencionDaoAPI).get(ID_PRIMER_LINEAINTERVENCION);
        lineaIntervencionServiceAPI.editar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link LineaIntervencionDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_lineaIntervencion_no_existe() throws PmzException{
        lineaIntervencionServiceAPI.editar(LineaIntervencionServiceTestData.getLineaIntervencionDTO());
    }
    
    
}
