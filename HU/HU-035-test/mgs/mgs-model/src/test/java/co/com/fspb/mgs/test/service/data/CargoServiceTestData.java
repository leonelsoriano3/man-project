package co.com.fspb.mgs.test.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.fspb.mgs.dao.model.Cargo;
import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Clase Mock que simula la lista de respuesta de entidades {@link Cargo}.
 * Se usa en las pruebas unitarias de Services y Facades. 
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class CargoServiceTestData
 * @date nov 11, 2016
 */
public abstract class CargoServiceTestData {
    
    private static List<Cargo> entities = new ArrayList<Cargo>();
    
    /**
     * Bloque estatico que inicia la lista de entidades.
     */
    static {
        for (int i = 1; i < 6; i++) {
            Cargo entity = new Cargo();

            entity.setId(i);
            
            entity.setFechaModifica(new Date());
            entity.setEstado(EstadoEnum.ESTADOENUM_1);
            entity.setValor("Valor" + i);
            entity.setUsuarioModifica("UsuarioModifica" + i);
            entity.setUsuarioCrea("UsuarioCrea" + i);
            entity.setFechaCrea(new Date());
            entities.add(entity);
        }
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Cargo} Mock 
     * tranformado a DTO {@link CargoDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link CargoDTO}
     */
    public static CargoDTO getCargoDTO(){
        return DTOTransformer.getCargoDTOFromCargo(getCargo());
    }

    /**
     * Retorna el primer elemento de la lista de enidades {@link Cargo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Entidad {@link Cargo} Mock
     */
    public static Cargo getCargo(){
        return entities.get(0);
    }

    /**
     * Retorna lista de entidades {@link Cargo} Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return Lista de {@link Cargo}
     */
    public static List<Cargo> getCargos(){
        return entities;
    }

    /**
     * Retorna el {@link PmzResultSet} de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return {@link PmzResultSet}
     */
    public static PmzResultSet<CargoDTO> getResultSetCargosDTO(){
        List<CargoDTO> dtos = new ArrayList<CargoDTO>();
        for (Cargo entity : entities) {
            dtos.add(DTOTransformer.getCargoDTOFromCargo(entity));
        }
        return new PmzResultSet<CargoDTO>(dtos, getTotalCargos(), dtos.size());
    }

    /**
     * Retorna el total de la lista Mock
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     * @return total de entidades Mock
     */
    public static Long getTotalCargos(){
        return Long.valueOf(entities.size());
    }

}
