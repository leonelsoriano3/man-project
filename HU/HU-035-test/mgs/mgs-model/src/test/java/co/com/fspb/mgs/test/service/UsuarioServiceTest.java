package co.com.fspb.mgs.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.dto.PmzResultSet;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.api.UsuarioDaoAPI;
import co.com.fspb.mgs.service.api.UsuarioServiceAPI;
import co.com.fspb.mgs.service.impl.UsuarioServiceImpl;
import co.com.fspb.mgs.test.service.data.UsuarioServiceTestData;
    import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.test.service.data.PersonaServiceTestData;
import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.dao.api.CargoDaoAPI;
import co.com.fspb.mgs.test.service.data.CargoServiceTestData;

/**
 * Prueba unitaria para el servicio {@link UsuarioServiceAPI} de la entidad {@link Usuario}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class UsuarioServiceAPI
 * @date nov 11, 2016
 */
public class UsuarioServiceTest {
	
    private static final Long ID_PRIMER_USUARIO = 1L;

    @InjectMocks
    private UsuarioServiceAPI usuarioServiceAPI = new UsuarioServiceImpl();

    @Mock
    private UsuarioDaoAPI usuarioDaoAPI;
    @Mock
    private PersonaDaoAPI personaDaoAPI;
    @Mock
    private CargoDaoAPI cargoDaoAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */   
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(UsuarioServiceTestData.getUsuarios()).when(usuarioDaoAPI).getRecords(null);
        Mockito.doReturn(UsuarioServiceTestData.getTotalUsuarios()).when(usuarioDaoAPI).countRecords(null);
        PmzResultSet<UsuarioDTO> usuarios = usuarioServiceAPI.getRecords(null);
        Assert.assertEquals(UsuarioServiceTestData.getTotalUsuarios(), usuarios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link UsuarioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_usuario_existe(){
        Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
        UsuarioDTO usuario = usuarioServiceAPI.getUsuarioDTO(ID_PRIMER_USUARIO);
        Assert.assertEquals(UsuarioServiceTestData.getUsuario().getId(), usuario.getId());
    }

    /**
     * Prueba unitaria de get con {@link UsuarioDTO} no existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_usuario_no_existe(){
        Mockito.doReturn(null).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
        UsuarioDTO usuario = usuarioServiceAPI.getUsuarioDTO(ID_PRIMER_USUARIO);
        Assert.assertEquals(null, usuario);
    }

    /**
     * Prueba unitaria de registro de una {@link UsuarioDTO} que no existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_guardar_usuario_no_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(CargoServiceTestData.getCargo().getId());
    	usuarioServiceAPI.guardar(UsuarioServiceTestData.getUsuarioDTO());
    }

    /**
     * Prueba unitaria de registro de una {@link UsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_usuario_existe() throws PmzException{
        Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
        usuarioServiceAPI.guardar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link UsuarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_usuario_no_existe_no_persona() throws PmzException{
    	Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(CargoServiceTestData.getCargo().getId());
    	usuarioServiceAPI.guardar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de registro de una {@link UsuarioDTO} que no existe en la Base de Datos.
     * La entidad a almacenar no tiene relación con cargo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_guardar_usuario_no_existe_no_cargo() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	usuarioServiceAPI.guardar(UsuarioServiceTestData.getUsuarioDTO());
    }
    

    /**
     * Prueba unitaria de actualización de una {@link UsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test
    public void test_editar_usuario_existe() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
    	Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(CargoServiceTestData.getCargo().getId());
        Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
        usuarioServiceAPI.editar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link UsuarioDTO} que existe en la Base de Datos
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */ 
    @Test(expected=PmzException.class)
    public void test_editar_usuario_no_existe() throws PmzException{
        usuarioServiceAPI.editar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link UsuarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con persona
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_usuario_existe_no_persona() throws PmzException{
    	Mockito.doReturn(CargoServiceTestData.getCargo()).when(cargoDaoAPI).get(CargoServiceTestData.getCargo().getId());
        Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
    	usuarioServiceAPI.editar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    /**
     * Prueba unitaria de actualización de una {@link UsuarioDTO} que existe en la Base de Datos.
     * La entidad a actualizar no tiene relación con cargo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test(expected=PmzException.class)
    public void test_editar_usuario_existe_no_cargo() throws PmzException{
    	Mockito.doReturn(PersonaServiceTestData.getPersona()).when(personaDaoAPI).get(PersonaServiceTestData.getPersona().getId());
        Mockito.doReturn(UsuarioServiceTestData.getUsuario()).when(usuarioDaoAPI).get(ID_PRIMER_USUARIO);
    	usuarioServiceAPI.editar(UsuarioServiceTestData.getUsuarioDTO());
    }
    
    
}
