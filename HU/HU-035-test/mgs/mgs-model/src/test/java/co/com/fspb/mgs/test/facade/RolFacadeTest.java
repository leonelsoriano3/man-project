package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.facade.api.RolFacadeAPI;
import co.com.fspb.mgs.facade.impl.RolFacadeImpl;
import co.com.fspb.mgs.service.api.RolServiceAPI;
import co.com.fspb.mgs.test.service.data.RolServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link RolFacadeAPI} de la entidad {@link Rol}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFacadeTest
 * @date nov 11, 2016
 */
public class RolFacadeTest {

    private static final Long ID_PRIMER_ROL = 1L;

    @InjectMocks
    private RolFacadeAPI rolFacadeAPI = new RolFacadeImpl();

    @Mock
    private RolServiceAPI rolServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(RolServiceTestData.getResultSetRolsDTO()).when(rolServiceAPI).getRecords(null);
        PmzResultSet<RolDTO> rols = rolFacadeAPI.getRecords(null);
        Assert.assertEquals(RolServiceTestData.getTotalRols(), rols.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link RolDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_rol_existe(){
        Mockito.doReturn(RolServiceTestData.getRolDTO()).when(rolServiceAPI).getRolDTO(ID_PRIMER_ROL);
        RolDTO rol = rolFacadeAPI.getRolDTO(ID_PRIMER_ROL);
        Assert.assertEquals(RolServiceTestData.getRolDTO().getId(), rol.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link RolDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_rol_no_existe() throws PmzException{
        rolFacadeAPI.guardar(RolServiceTestData.getRolDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link RolDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_rol_existe() throws PmzException{
        rolFacadeAPI.editar(RolServiceTestData.getRolDTO());
    }
    
}
