package co.com.fspb.mgs.test.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.facade.api.ProyectoTerritorioFacadeAPI;
import co.com.fspb.mgs.facade.impl.ProyectoTerritorioFacadeImpl;
import co.com.fspb.mgs.service.api.ProyectoTerritorioServiceAPI;
import co.com.fspb.mgs.test.service.data.ProyectoTerritorioServiceTestData;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.test.service.data.TerritorioServiceTestData;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.test.service.data.ProyectoServiceTestData;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Prueba unitaria para la fachada {@link ProyectoTerritorioFacadeAPI} de la entidad {@link ProyectoTerritorio}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoTerritorioFacadeTest
 * @date nov 11, 2016
 */
public class ProyectoTerritorioFacadeTest {

    private static final TerritorioDTO TERRITORIODTO = TerritorioServiceTestData.getTerritorioDTO();
    private static final ProyectoDTO PROYECTODTO = ProyectoServiceTestData.getProyectoDTO();

    @InjectMocks
    private ProyectoTerritorioFacadeAPI proyectoTerritorioFacadeAPI = new ProyectoTerritorioFacadeImpl();

    @Mock
    private ProyectoTerritorioServiceAPI proyectoTerritorioServiceAPI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Prueba unitaria de getRecords con {@link PmzPagingCriteria} nulo
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_records_paging_null(){
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getResultSetProyectoTerritoriosDTO()).when(proyectoTerritorioServiceAPI).getRecords(null);
        PmzResultSet<ProyectoTerritorioDTO> proyectoTerritorios = proyectoTerritorioFacadeAPI.getRecords(null);
        Assert.assertEquals(ProyectoTerritorioServiceTestData.getTotalProyectoTerritorios(), proyectoTerritorios.getTotalRecords());
    }

    /**
     * Prueba unitaria de get con {@link ProyectoTerritorioDTO} existente
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_get_proyectoTerritorio_existe(){
        Mockito.doReturn(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO()).when(proyectoTerritorioServiceAPI).getProyectoTerritorioDTO(TERRITORIODTO, PROYECTODTO);
        ProyectoTerritorioDTO proyectoTerritorio = proyectoTerritorioFacadeAPI.getProyectoTerritorioDTO(TERRITORIODTO, PROYECTODTO);
        Assert.assertEquals(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO().getId(), proyectoTerritorio.getId());
    }

    /**
     * Prueba unitaria de registro de una {@link ProyectoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_guardar_proyectoTerritorio_no_existe() throws PmzException{
        proyectoTerritorioFacadeAPI.guardar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }

    /**
     * Prueba unitaria de actualización de una {@link ProyectoTerritorioDTO}
     * 
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @Test
    public void test_editar_proyectoTerritorio_existe() throws PmzException{
        proyectoTerritorioFacadeAPI.editar(ProyectoTerritorioServiceTestData.getProyectoTerritorioDTO());
    }
    
}
