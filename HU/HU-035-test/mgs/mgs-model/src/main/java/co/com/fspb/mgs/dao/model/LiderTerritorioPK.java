package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;


/**
 * Mapeo de llave compuesta.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioPK
 * @date nov 11, 2016
 */
@Embeddable
public class LiderTerritorioPK implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_territorio", nullable = true)
    private Territorio territorio; 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_lider", nullable = true)
    private Lider lider; 
 
 
    public Territorio getTerritorio() {
        return territorio;
    }

    public void setTerritorio(Territorio territorio) {
        this.territorio = territorio;
    }

    public Lider getLider() {
        return lider;
    }

    public void setLider(Lider lider) {
        this.lider = lider;
    }

}
