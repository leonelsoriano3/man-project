package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.facade.api.ComponenteFacadeAPI;
import co.com.fspb.mgs.service.api.ComponenteServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ComponenteFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteFacadeImpl
 * @date nov 11, 2016
 */
@Service("componenteFacade")
public class ComponenteFacadeImpl implements ComponenteFacadeAPI {

    @Autowired
    private ComponenteServiceAPI componenteService;

    /**
     * @see co.com.fspb.mgs.facade.api.ComponenteFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ComponenteDTO> getRecords(PmzPagingCriteria criteria) {
        return componenteService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ComponenteFacadeAPI#guardar()
     */
    @Override
    public void guardar(ComponenteDTO componente) throws PmzException {
        componenteService.guardar(componente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ComponenteFacadeAPI#editar()
     */
    @Override
    public void editar(ComponenteDTO componente) throws PmzException {
        componenteService.editar(componente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ComponenteFacadeAPI#getComponenteDTO()
     */
    @Override
    public ComponenteDTO getComponenteDTO(Long id) {
        return componenteService.getComponenteDTO(id);
    }

}
