package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dto.UsuarioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link RolUsuario}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioServiceAPI
 * @date nov 11, 2016
 */
public interface RolUsuarioServiceAPI {

    /**
	 * Registra una entidad {@link RolUsuario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolUsuario - {@link RolUsuarioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(RolUsuarioDTO rolUsuario) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link RolUsuario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolUsuario - {@link RolUsuarioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(RolUsuarioDTO rolUsuario) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<RolUsuarioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link RolUsuarioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rol - {@link RolDTO}
	 * @param usuario - {@link UsuarioDTO}
     * @return {@link RolUsuarioDTO}
     */
    RolUsuarioDTO getRolUsuarioDTO(RolDTO rol, UsuarioDTO usuario);

}
