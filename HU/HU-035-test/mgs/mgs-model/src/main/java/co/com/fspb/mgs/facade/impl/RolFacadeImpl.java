package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.facade.api.RolFacadeAPI;
import co.com.fspb.mgs.service.api.RolServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link RolFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFacadeImpl
 * @date nov 11, 2016
 */
@Service("rolFacade")
public class RolFacadeImpl implements RolFacadeAPI {

    @Autowired
    private RolServiceAPI rolService;

    /**
     * @see co.com.fspb.mgs.facade.api.RolFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolDTO> getRecords(PmzPagingCriteria criteria) {
        return rolService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.RolFacadeAPI#guardar()
     */
    @Override
    public void guardar(RolDTO rol) throws PmzException {
        rolService.guardar(rol);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolFacadeAPI#editar()
     */
    @Override
    public void editar(RolDTO rol) throws PmzException {
        rolService.editar(rol);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolFacadeAPI#getRolDTO()
     */
    @Override
    public RolDTO getRolDTO(Long id) {
        return rolService.getRolDTO(id);
    }

}
