package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.facade.api.LineaFacadeAPI;
import co.com.fspb.mgs.service.api.LineaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link LineaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaFacadeImpl
 * @date nov 11, 2016
 */
@Service("lineaFacade")
public class LineaFacadeImpl implements LineaFacadeAPI {

    @Autowired
    private LineaServiceAPI lineaService;

    /**
     * @see co.com.fspb.mgs.facade.api.LineaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<LineaDTO> getRecords(PmzPagingCriteria criteria) {
        return lineaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.LineaFacadeAPI#guardar()
     */
    @Override
    public void guardar(LineaDTO linea) throws PmzException {
        lineaService.guardar(linea);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LineaFacadeAPI#editar()
     */
    @Override
    public void editar(LineaDTO linea) throws PmzException {
        lineaService.editar(linea);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.LineaFacadeAPI#getLineaDTO()
     */
    @Override
    public LineaDTO getLineaDTO(Long id) {
        return lineaService.getLineaDTO(id);
    }

}
