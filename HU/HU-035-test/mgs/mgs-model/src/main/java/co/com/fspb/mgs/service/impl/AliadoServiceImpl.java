package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.AliadoDaoAPI;
import co.com.fspb.mgs.dao.api.TipoAliadoDaoAPI;
import co.com.fspb.mgs.dao.model.TipoAliado;
import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.Aliado;
import co.com.fspb.mgs.service.api.AliadoServiceAPI;
import co.com.fspb.mgs.dto.AliadoDTO;

/**
 * Implementación de la Interfaz {@link AliadoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class AliadoServiceImpl implements AliadoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private AliadoDaoAPI aliadoDao;
    @Autowired
    private TipoAliadoDaoAPI tipoAliadoDao;
    @Autowired
    private EntidadDaoAPI entidadDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(AliadoDTO aliadoDTO) throws PmzException {

        Aliado aliadoValidate = aliadoDao.get(aliadoDTO.getId());

        if (aliadoValidate != null) {
            throw new PmzException("El aliado ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Aliado aliado = DTOTransformer.getAliadoFromAliadoDTO(new Aliado(), aliadoDTO);

		if (aliadoDTO.getTipoAliadoDTO() != null) {
       		TipoAliado tipoAliado = tipoAliadoDao.get(aliadoDTO.getTipoAliadoDTO().getId());
       		if (tipoAliado == null) {
                throw new PmzException("El TipoAliado no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setTipoAliado(tipoAliado);
		}
		
		if (aliadoDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(aliadoDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setEntidad(entidad);
		}
		
		if (aliadoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(aliadoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setProyecto(proyecto);
		}
		
        aliadoDao.save(aliado);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<AliadoDTO> getRecords(PmzPagingCriteria criteria) {
        List<Aliado> listaReturn = aliadoDao.getRecords(criteria);
        Long countTotalRegistros = aliadoDao.countRecords(criteria);

        List<AliadoDTO> resultList = new ArrayList<AliadoDTO>();
        for (Aliado aliado : listaReturn) {
            AliadoDTO aliadoDTO = DTOTransformer
                    .getAliadoDTOFromAliado(aliado);
            resultList.add(aliadoDTO);
        }
        
        return new PmzResultSet<AliadoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(AliadoDTO aliadoDTO) throws PmzException{

        Aliado aliado = aliadoDao.get(aliadoDTO.getId());

        if (aliado == null) {
            throw new PmzException("El aliado no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getAliadoFromAliadoDTO(aliado, aliadoDTO);

		if (aliadoDTO.getTipoAliadoDTO() != null) {
       		TipoAliado tipoAliado = tipoAliadoDao.get(aliadoDTO.getTipoAliadoDTO().getId());
       		if (tipoAliado == null) {
                throw new PmzException("El TipoAliado no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setTipoAliado(tipoAliado);
		}
		
		if (aliadoDTO.getEntidadDTO() != null) {
       		Entidad entidad = entidadDao.get(aliadoDTO.getEntidadDTO().getId());
       		if (entidad == null) {
                throw new PmzException("El Entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setEntidad(entidad);
		}
		
		if (aliadoDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(aliadoDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		aliado.setProyecto(proyecto);
		}
		
        aliadoDao.update(aliado);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public AliadoDTO getAliadoDTO(Long id) {
        Aliado aliado = aliadoDao.get(id);
        return DTOTransformer.getAliadoDTOFromAliado(aliado);
    }
}
