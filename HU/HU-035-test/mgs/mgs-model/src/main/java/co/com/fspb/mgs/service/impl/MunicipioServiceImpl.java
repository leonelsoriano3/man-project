package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.MunicipioDaoAPI;
import co.com.fspb.mgs.dao.model.Municipio;
import co.com.fspb.mgs.service.api.MunicipioServiceAPI;
import co.com.fspb.mgs.dto.MunicipioDTO;

/**
 * Implementación de la Interfaz {@link MunicipioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class MunicipioServiceImpl implements MunicipioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private MunicipioDaoAPI municipioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(MunicipioDTO municipioDTO) throws PmzException {

        Municipio municipioValidate = municipioDao.get(municipioDTO.getId());

        if (municipioValidate != null) {
            throw new PmzException("El municipio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Municipio municipio = DTOTransformer.getMunicipioFromMunicipioDTO(new Municipio(), municipioDTO);

        municipioDao.save(municipio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<MunicipioDTO> getRecords(PmzPagingCriteria criteria) {
        List<Municipio> listaReturn = municipioDao.getRecords(criteria);
        Long countTotalRegistros = municipioDao.countRecords(criteria);

        List<MunicipioDTO> resultList = new ArrayList<MunicipioDTO>();
        for (Municipio municipio : listaReturn) {
            MunicipioDTO municipioDTO = DTOTransformer
                    .getMunicipioDTOFromMunicipio(municipio);
            resultList.add(municipioDTO);
        }
        
        return new PmzResultSet<MunicipioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(MunicipioDTO municipioDTO) throws PmzException{

        Municipio municipio = municipioDao.get(municipioDTO.getId());

        if (municipio == null) {
            throw new PmzException("El municipio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getMunicipioFromMunicipioDTO(municipio, municipioDTO);

        municipioDao.update(municipio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public MunicipioDTO getMunicipioDTO(Long id) {
        Municipio municipio = municipioDao.get(id);
        return DTOTransformer.getMunicipioDTOFromMunicipio(municipio);
    }
}
