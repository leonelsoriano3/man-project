package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.LiderTerritorioDaoAPI;
import co.com.fspb.mgs.dao.api.LiderDaoAPI;
import co.com.fspb.mgs.dao.model.Lider;
import co.com.fspb.mgs.dao.api.TerritorioDaoAPI;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dao.model.LiderTerritorio;
import co.com.fspb.mgs.dao.model.LiderTerritorioPK;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dao.model.Lider;
import co.com.fspb.mgs.service.api.LiderTerritorioServiceAPI;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dto.LiderDTO;

/**
 * Implementación de la Interfaz {@link LiderTerritorioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderTerritorioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class LiderTerritorioServiceImpl implements LiderTerritorioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private LiderTerritorioDaoAPI liderTerritorioDao;
    @Autowired
    private LiderDaoAPI liderDao;
    @Autowired
    private TerritorioDaoAPI territorioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(LiderTerritorioDTO liderTerritorioDTO) throws PmzException {


		LiderTerritorioPK pk = new LiderTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), liderTerritorioDTO.getTerritorioDTO()));
        pk.setLider(DTOTransformer.getLiderFromLiderDTO(new Lider(), liderTerritorioDTO.getLiderDTO()));
        LiderTerritorio liderTerritorioValidate = liderTerritorioDao.get(pk);

        if (liderTerritorioValidate != null) {
            throw new PmzException("El liderTerritorio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        LiderTerritorio liderTerritorio = DTOTransformer.getLiderTerritorioFromLiderTerritorioDTO(new LiderTerritorio(), liderTerritorioDTO);

		pk = liderTerritorio.getId();
		if (liderTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(liderTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (liderTerritorioDTO.getLiderDTO() != null) {
       		Lider lider = liderDao.get(liderTerritorioDTO.getLiderDTO().getId());
       		if (lider == null) {
                throw new PmzException("El Lider no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setLider(lider);
		}
		
        liderTerritorio.setId(pk);
        
        liderTerritorioDao.save(liderTerritorio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<LiderTerritorioDTO> getRecords(PmzPagingCriteria criteria) {
        List<LiderTerritorio> listaReturn = liderTerritorioDao.getRecords(criteria);
        Long countTotalRegistros = liderTerritorioDao.countRecords(criteria);

        List<LiderTerritorioDTO> resultList = new ArrayList<LiderTerritorioDTO>();
        for (LiderTerritorio liderTerritorio : listaReturn) {
            LiderTerritorioDTO liderTerritorioDTO = DTOTransformer
                    .getLiderTerritorioDTOFromLiderTerritorio(liderTerritorio);
            resultList.add(liderTerritorioDTO);
        }
        
        return new PmzResultSet<LiderTerritorioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(LiderTerritorioDTO liderTerritorioDTO) throws PmzException{


		LiderTerritorioPK pk = new LiderTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), liderTerritorioDTO.getTerritorioDTO()));
        pk.setLider(DTOTransformer.getLiderFromLiderDTO(new Lider(), liderTerritorioDTO.getLiderDTO()));
        LiderTerritorio liderTerritorio = liderTerritorioDao.get(pk);

        if (liderTerritorio == null) {
            throw new PmzException("El liderTerritorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getLiderTerritorioFromLiderTerritorioDTO(liderTerritorio, liderTerritorioDTO);

		pk = liderTerritorio.getId();
		if (liderTerritorioDTO.getTerritorioDTO() != null) {
       		Territorio territorio = territorioDao.get(liderTerritorioDTO.getTerritorioDTO().getId());
       		if (territorio == null) {
                throw new PmzException("El Territorio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setTerritorio(territorio);
		}
		
		if (liderTerritorioDTO.getLiderDTO() != null) {
       		Lider lider = liderDao.get(liderTerritorioDTO.getLiderDTO().getId());
       		if (lider == null) {
                throw new PmzException("El Lider no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setLider(lider);
		}
		
        liderTerritorio.setId(pk);
        
        liderTerritorioDao.update(liderTerritorio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public LiderTerritorioDTO getLiderTerritorioDTO(TerritorioDTO territorio, LiderDTO lider) {

		LiderTerritorioPK pk = new LiderTerritorioPK();
        pk.setTerritorio(DTOTransformer.getTerritorioFromTerritorioDTO(new Territorio(), territorio));
        pk.setLider(DTOTransformer.getLiderFromLiderDTO(new Lider(), lider));
        LiderTerritorio liderTerritorio = liderTerritorioDao.get(pk);
        return DTOTransformer.getLiderTerritorioDTOFromLiderTerritorio(liderTerritorio);
    }
}
