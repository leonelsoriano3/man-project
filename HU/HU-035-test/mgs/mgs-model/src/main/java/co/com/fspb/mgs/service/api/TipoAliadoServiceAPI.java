package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TipoAliadoDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link TipoAliado}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoServiceAPI
 * @date nov 11, 2016
 */
public interface TipoAliadoServiceAPI {

    /**
	 * Registra una entidad {@link TipoAliado} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoAliado - {@link TipoAliadoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(TipoAliadoDTO tipoAliado) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link TipoAliado} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoAliado - {@link TipoAliadoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(TipoAliadoDTO tipoAliado) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TipoAliadoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link TipoAliadoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TipoAliadoDTO}
     */
    TipoAliadoDTO getTipoAliadoDTO(Long id);

}
