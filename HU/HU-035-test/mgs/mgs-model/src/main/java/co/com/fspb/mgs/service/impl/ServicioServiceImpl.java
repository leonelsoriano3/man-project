package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ServicioDaoAPI;
import co.com.fspb.mgs.dao.model.Servicio;
import co.com.fspb.mgs.service.api.ServicioServiceAPI;
import co.com.fspb.mgs.dto.ServicioDTO;

/**
 * Implementación de la Interfaz {@link ServicioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ServicioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ServicioServiceImpl implements ServicioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ServicioDaoAPI servicioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ServicioDTO servicioDTO) throws PmzException {

        Servicio servicioValidate = servicioDao.get(servicioDTO.getId());

        if (servicioValidate != null) {
            throw new PmzException("El servicio ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Servicio servicio = DTOTransformer.getServicioFromServicioDTO(new Servicio(), servicioDTO);

        servicioDao.save(servicio);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ServicioDTO> getRecords(PmzPagingCriteria criteria) {
        List<Servicio> listaReturn = servicioDao.getRecords(criteria);
        Long countTotalRegistros = servicioDao.countRecords(criteria);

        List<ServicioDTO> resultList = new ArrayList<ServicioDTO>();
        for (Servicio servicio : listaReturn) {
            ServicioDTO servicioDTO = DTOTransformer
                    .getServicioDTOFromServicio(servicio);
            resultList.add(servicioDTO);
        }
        
        return new PmzResultSet<ServicioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ServicioDTO servicioDTO) throws PmzException{

        Servicio servicio = servicioDao.get(servicioDTO.getId());

        if (servicio == null) {
            throw new PmzException("El servicio no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getServicioFromServicioDTO(servicio, servicioDTO);

        servicioDao.update(servicio);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ServicioDTO getServicioDTO(Long id) {
        Servicio servicio = servicioDao.get(id);
        return DTOTransformer.getServicioDTOFromServicio(servicio);
    }
}
