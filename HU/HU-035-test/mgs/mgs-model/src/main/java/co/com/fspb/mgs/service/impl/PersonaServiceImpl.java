package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.service.api.PersonaServiceAPI;
import co.com.fspb.mgs.dto.PersonaDTO;

/**
 * Implementación de la Interfaz {@link PersonaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class PersonaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class PersonaServiceImpl implements PersonaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private PersonaDaoAPI personaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(PersonaDTO personaDTO) throws PmzException {

        Persona personaValidate = personaDao.get(personaDTO.getId());

        if (personaValidate != null) {
            throw new PmzException("El persona ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Persona persona = DTOTransformer.getPersonaFromPersonaDTO(new Persona(), personaDTO);

        personaDao.save(persona);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<PersonaDTO> getRecords(PmzPagingCriteria criteria) {
        List<Persona> listaReturn = personaDao.getRecords(criteria);
        Long countTotalRegistros = personaDao.countRecords(criteria);

        List<PersonaDTO> resultList = new ArrayList<PersonaDTO>();
        for (Persona persona : listaReturn) {
            PersonaDTO personaDTO = DTOTransformer
                    .getPersonaDTOFromPersona(persona);
            resultList.add(personaDTO);
        }
        
        return new PmzResultSet<PersonaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(PersonaDTO personaDTO) throws PmzException{

        Persona persona = personaDao.get(personaDTO.getId());

        if (persona == null) {
            throw new PmzException("El persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getPersonaFromPersonaDTO(persona, personaDTO);

        personaDao.update(persona);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public PersonaDTO getPersonaDTO(Long id) {
        Persona persona = personaDao.get(id);
        return DTOTransformer.getPersonaDTOFromPersona(persona);
    }
}
