package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link TrazaActividad}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaActividadServiceAPI
 * @date nov 11, 2016
 */
public interface TrazaActividadServiceAPI {

    /**
	 * Registra una entidad {@link TrazaActividad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaActividad - {@link TrazaActividadDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(TrazaActividadDTO trazaActividad) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link TrazaActividad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaActividad - {@link TrazaActividadDTO}
	 * @throws {@link PmzException}
	 */
	void editar(TrazaActividadDTO trazaActividad) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<TrazaActividadDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link TrazaActividadDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link TrazaActividadDTO}
     */
    TrazaActividadDTO getTrazaActividadDTO(Long id);

}
