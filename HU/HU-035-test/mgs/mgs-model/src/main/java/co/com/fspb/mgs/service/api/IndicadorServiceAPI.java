package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.IndicadorDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Indicador}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorServiceAPI
 * @date nov 11, 2016
 */
public interface IndicadorServiceAPI {

    /**
	 * Registra una entidad {@link Indicador} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link IndicadorDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(IndicadorDTO indicador) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Indicador} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link IndicadorDTO}
	 * @throws {@link PmzException}
	 */
	void editar(IndicadorDTO indicador) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<IndicadorDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link IndicadorDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link IndicadorDTO}
     */
    IndicadorDTO getIndicadorDTO(Long id);

}
