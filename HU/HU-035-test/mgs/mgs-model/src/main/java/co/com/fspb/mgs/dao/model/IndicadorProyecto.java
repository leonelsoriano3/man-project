package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_indicador_proyecto en la BD a la entidad IndicadorProyecto.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorProyecto
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_indicador_proyecto" )
public class IndicadorProyecto implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private IndicadorProyectoPK id; 


 
    public IndicadorProyectoPK getId() {
        return id;
    }

    public void setId(IndicadorProyectoPK id) {
        this.id = id;
    }

}
