package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI;
import co.com.fspb.mgs.service.api.ProyectoBeneficiarioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ProyectoBeneficiarioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioFacadeImpl
 * @date nov 11, 2016
 */
@Service("proyectoBeneficiarioFacade")
public class ProyectoBeneficiarioFacadeImpl implements ProyectoBeneficiarioFacadeAPI {

    @Autowired
    private ProyectoBeneficiarioServiceAPI proyectoBeneficiarioService;

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoBeneficiarioDTO> getRecords(PmzPagingCriteria criteria) {
        return proyectoBeneficiarioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI#guardar()
     */
    @Override
    public void guardar(ProyectoBeneficiarioDTO proyectoBeneficiario) throws PmzException {
        proyectoBeneficiarioService.guardar(proyectoBeneficiario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI#editar()
     */
    @Override
    public void editar(ProyectoBeneficiarioDTO proyectoBeneficiario) throws PmzException {
        proyectoBeneficiarioService.editar(proyectoBeneficiario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ProyectoBeneficiarioFacadeAPI#getProyectoBeneficiarioDTO()
     */
    @Override
    public ProyectoBeneficiarioDTO getProyectoBeneficiarioDTO(Integer id) {
        return proyectoBeneficiarioService.getProyectoBeneficiarioDTO(id);
    }

}
