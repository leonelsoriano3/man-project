package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_componente_proyecto en la BD a la entidad ComponenteProyecto.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyecto
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_componente_proyecto" )
public class ComponenteProyecto implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private ComponenteProyectoPK id; 


 
    public ComponenteProyectoPK getId() {
        return id;
    }

    public void setId(ComponenteProyectoPK id) {
        this.id = id;
    }

}
