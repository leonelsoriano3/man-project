package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.facade.api.AliadoFacadeAPI;
import co.com.fspb.mgs.service.api.AliadoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link AliadoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AliadoFacadeImpl
 * @date nov 11, 2016
 */
@Service("aliadoFacade")
public class AliadoFacadeImpl implements AliadoFacadeAPI {

    @Autowired
    private AliadoServiceAPI aliadoService;

    /**
     * @see co.com.fspb.mgs.facade.api.AliadoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<AliadoDTO> getRecords(PmzPagingCriteria criteria) {
        return aliadoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.AliadoFacadeAPI#guardar()
     */
    @Override
    public void guardar(AliadoDTO aliado) throws PmzException {
        aliadoService.guardar(aliado);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AliadoFacadeAPI#editar()
     */
    @Override
    public void editar(AliadoDTO aliado) throws PmzException {
        aliadoService.editar(aliado);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AliadoFacadeAPI#getAliadoDTO()
     */
    @Override
    public AliadoDTO getAliadoDTO(Long id) {
        return aliadoService.getAliadoDTO(id);
    }

}
