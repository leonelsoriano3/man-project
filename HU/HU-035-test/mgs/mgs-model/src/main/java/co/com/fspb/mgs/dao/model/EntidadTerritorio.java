package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_entidad_territorio en la BD a la entidad EntidadTerritorio.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadTerritorio
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_entidad_territorio" )
public class EntidadTerritorio implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
    @EmbeddedId
	private EntidadTerritorioPK id; 


 
    public EntidadTerritorioPK getId() {
        return id;
    }

    public void setId(EntidadTerritorioPK id) {
        this.id = id;
    }

}
