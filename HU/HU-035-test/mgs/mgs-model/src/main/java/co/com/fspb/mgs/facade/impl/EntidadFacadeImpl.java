package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.facade.api.EntidadFacadeAPI;
import co.com.fspb.mgs.service.api.EntidadServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EntidadFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadFacadeImpl
 * @date nov 11, 2016
 */
@Service("entidadFacade")
public class EntidadFacadeImpl implements EntidadFacadeAPI {

    @Autowired
    private EntidadServiceAPI entidadService;

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadDTO> getRecords(PmzPagingCriteria criteria) {
        return entidadService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EntidadFacadeAPI#guardar()
     */
    @Override
    public void guardar(EntidadDTO entidad) throws PmzException {
        entidadService.guardar(entidad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadFacadeAPI#editar()
     */
    @Override
    public void editar(EntidadDTO entidad) throws PmzException {
        entidadService.editar(entidad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EntidadFacadeAPI#getEntidadDTO()
     */
    @Override
    public EntidadDTO getEntidadDTO(Long id) {
        return entidadService.getEntidadDTO(id);
    }

}
