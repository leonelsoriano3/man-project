package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.RolFundacionDaoAPI;
import co.com.fspb.mgs.dao.model.RolFundacion;
import co.com.fspb.mgs.service.api.RolFundacionServiceAPI;
import co.com.fspb.mgs.dto.RolFundacionDTO;

/**
 * Implementación de la Interfaz {@link RolFundacionServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFundacionServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class RolFundacionServiceImpl implements RolFundacionServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private RolFundacionDaoAPI rolFundacionDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(RolFundacionDTO rolFundacionDTO) throws PmzException {

        RolFundacion rolFundacionValidate = rolFundacionDao.get(rolFundacionDTO.getId());

        if (rolFundacionValidate != null) {
            throw new PmzException("El rolFundacion ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        RolFundacion rolFundacion = DTOTransformer.getRolFundacionFromRolFundacionDTO(new RolFundacion(), rolFundacionDTO);

        rolFundacionDao.save(rolFundacion);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolFundacionDTO> getRecords(PmzPagingCriteria criteria) {
        List<RolFundacion> listaReturn = rolFundacionDao.getRecords(criteria);
        Long countTotalRegistros = rolFundacionDao.countRecords(criteria);

        List<RolFundacionDTO> resultList = new ArrayList<RolFundacionDTO>();
        for (RolFundacion rolFundacion : listaReturn) {
            RolFundacionDTO rolFundacionDTO = DTOTransformer
                    .getRolFundacionDTOFromRolFundacion(rolFundacion);
            resultList.add(rolFundacionDTO);
        }
        
        return new PmzResultSet<RolFundacionDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(RolFundacionDTO rolFundacionDTO) throws PmzException{

        RolFundacion rolFundacion = rolFundacionDao.get(rolFundacionDTO.getId());

        if (rolFundacion == null) {
            throw new PmzException("El rolFundacion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getRolFundacionFromRolFundacionDTO(rolFundacion, rolFundacionDTO);

        rolFundacionDao.update(rolFundacion);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public RolFundacionDTO getRolFundacionDTO(Long id) {
        RolFundacion rolFundacion = rolFundacionDao.get(id);
        return DTOTransformer.getRolFundacionDTOFromRolFundacion(rolFundacion);
    }
}
