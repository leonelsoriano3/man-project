package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.facade.api.ParienteFacadeAPI;
import co.com.fspb.mgs.service.api.ParienteServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ParienteFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParienteFacadeImpl
 * @date nov 11, 2016
 */
@Service("parienteFacade")
public class ParienteFacadeImpl implements ParienteFacadeAPI {

    @Autowired
    private ParienteServiceAPI parienteService;

    /**
     * @see co.com.fspb.mgs.facade.api.ParienteFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParienteDTO> getRecords(PmzPagingCriteria criteria) {
        return parienteService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ParienteFacadeAPI#guardar()
     */
    @Override
    public void guardar(ParienteDTO pariente) throws PmzException {
        parienteService.guardar(pariente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParienteFacadeAPI#editar()
     */
    @Override
    public void editar(ParienteDTO pariente) throws PmzException {
        parienteService.editar(pariente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ParienteFacadeAPI#getParienteDTO()
     */
    @Override
    public ParienteDTO getParienteDTO(BeneficiarioDTO beneficiario, PersonaDTO persona) {
        return parienteService.getParienteDTO(beneficiario, persona);
    }

}
