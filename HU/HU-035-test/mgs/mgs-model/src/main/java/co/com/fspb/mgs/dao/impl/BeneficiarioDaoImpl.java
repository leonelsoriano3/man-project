package co.com.fspb.mgs.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.premize.pmz.dao.dto.Paginator;

import com.premize.pmz.dao.PmzAbstractDaoImpl;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.Beneficiario;

import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzDateInterval;
import com.premize.pmz.api.dto.PmzSearch;
import com.premize.pmz.api.dto.PmzSortDirection;
import com.premize.pmz.api.dto.PmzSortField;

import co.com.fspb.mgs.enums.EstratoSocioeconomicoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Implementación de la interfaz {@link BeneficiarioDaoAPI}. Extiende el DAO
 * abstracto de PMZ {@link PmzAbstractDaoImpl} para la implementacion de métodos
 * genéricos de consultas y transacciones
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class BeneficiarioDaoImpl
 * @date nov 11, 2016
 */
@Repository
public class BeneficiarioDaoImpl extends PmzAbstractDaoImpl<Beneficiario, Long>
        implements BeneficiarioDaoAPI {

    /**
	 * @see co.com.fspb.mgs.dao.api.AccionCalidadDaoAPI#getRecords()
	 */
    @Override
    public List<Beneficiario> getRecords(PmzPagingCriteria criteria) {

        if (criteria == null) {
            throw new IllegalArgumentException(
                    "La propiedad PmzPagingCriteria no puede ser nula");
        }

        Integer displaySize = criteria.getDisplaySize();
        Integer displayStart = criteria.getDisplayStart();
        String searchLike = criteria.getSearch();
        List<PmzSearch> searchs = criteria.getSearchFields();
        List<PmzSortField> sortFields = criteria.getSortFields();
        List<PmzDateInterval> intervals = criteria.getDateInterval();

        // Consultar Registros
        DetachedCriteria findCriteria = createDetachedCriteriaFilter(
                searchLike, searchs, intervals);

        // Ordenar Campos
        if (sortFields != null && !sortFields.isEmpty()) {
            for (PmzSortField sortField : sortFields) {
                String field = sortField.getField();
                PmzSortDirection direction = sortField.getDirection();
                if (PmzSortDirection.ASC == direction) {
                    findCriteria.addOrder(Order.asc(field));
                } else {
                    findCriteria.addOrder(Order.desc(field));
                }
            }
        }

        return findByCriteria(findCriteria, new Paginator(displayStart,
                displaySize));
    }

    /**
	 * @see com.premize.pmz.dao.PmzAbstractDaoImpl#createDetachedCriteriaFilter()
	 */
    @Override
    public DetachedCriteria createDetachedCriteriaFilter(String searchLike,
            List<PmzSearch> searchs, List<PmzDateInterval> intervals) {
        DetachedCriteria cq = DetachedCriteria.forClass(Beneficiario.class);

        if ((searchLike != null) && (!(searchLike.isEmpty()))) {
            Disjunction disyuctionConsulta = Restrictions.disjunction();
            //TODO PMZ-Generado co.com.fspb.mgs: En caso de ser necesario crear un filtro de "OR" basado en varios campos de la entidad
            cq.add(disyuctionConsulta);
        }
        buildSearchs(searchs, cq);
        buildDateIntervals(intervals, cq);
        return cq;
    }

    /**
	 * @see com.premize.pmz.dao.PmzAbstractDaoImpl#buildSearchs()
	 */
    @Override
    public void buildSearchs(List<PmzSearch> searchs, DetachedCriteria cq) {
        if (searchs == null || searchs.isEmpty()) {
            return;
        }
        Beneficiario entity = new Beneficiario();
        for (PmzSearch search : searchs) {
            String field = search.getField();
            Object value = search.getValue();

            if (field != null && !field.isEmpty() && value != null) {
                    if ("areaEducacion.nombre".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("areaEducacion", "areaEducacion");
					    criteria.add(Restrictions.like("areaEducacion.nombre", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("estructuraFamiliar.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("estructuraFamiliar", "estructuraFamiliar");
					    criteria.add(Restrictions.like("estructuraFamiliar.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("estratoSocioeconomicoDesc".equalsIgnoreCase(field)) {
                        cq.add(Restrictions.eq("estratoSocioeconomico", EstratoSocioeconomicoEnum.valueOf(value.toString())));
                   } else if ("estadoCivil.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("estadoCivil", "estadoCivil");
					    criteria.add(Restrictions.like("estadoCivil.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("descIndependiente.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("descIndependiente", "descIndependiente");
					    criteria.add(Restrictions.like("descIndependiente.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("nivelEducativo.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("nivelEducativo", "nivelEducativo");
					    criteria.add(Restrictions.like("nivelEducativo.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("tipoVivienda.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("tipoVivienda", "tipoVivienda");
					    criteria.add(Restrictions.like("tipoVivienda.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("tipoTenencia.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("tipoTenencia", "tipoTenencia");
					    criteria.add(Restrictions.like("tipoTenencia.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("zona.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("zona", "zona");
					    criteria.add(Restrictions.like("zona.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("materialVivienda.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("materialVivienda", "materialVivienda");
					    criteria.add(Restrictions.like("materialVivienda.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("bdNacional.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("bdNacional", "bdNacional");
					    criteria.add(Restrictions.like("bdNacional.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("afiliadoSalud.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("afiliadoSalud", "afiliadoSalud");
					    criteria.add(Restrictions.like("afiliadoSalud.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("persona.nombres".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("persona", "persona");
					    criteria.add(Restrictions.like("persona.nombres", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("grupoEtnico.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("grupoEtnico", "grupoEtnico");
					    criteria.add(Restrictions.like("grupoEtnico.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("ocupacion.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("ocupacion", "ocupacion");
					    criteria.add(Restrictions.like("ocupacion.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else if ("estadoDesc".equalsIgnoreCase(field)) {
                        cq.add(Restrictions.eq("estado", EstadoEnum.valueOf(value.toString())));
                   } else if ("municipio.valor".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("municipio", "municipio");
					    criteria.add(Restrictions.like("municipio.valor", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else {
                        addSimpleCriteria(cq, entity, field, value);
                   }
            }
        }
    }

}
