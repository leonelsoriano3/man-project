package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_componente en la BD a la entidad Componente.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Componente
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_componente" )
public class Componente implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_componente", nullable = false)
	private Long id; 

	@Column(name = "nombre", nullable = true, length = 50)
    private String nombre; 


	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

	@Column(name = "info_actividades", nullable = true, length = 250)
    private String infoActividades; 

	@Column(name = "resultados_esperados", nullable = true, length = 250)
    private String resultadosEsperados; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public String getInfoActividades() {
        return infoActividades;
    }

    public void setInfoActividades(String infoActividades) {
        this.infoActividades = infoActividades;
    }

    public String getResultadosEsperados() {
        return resultadosEsperados;
    }

    public void setResultadosEsperados(String resultadosEsperados) {
        this.resultadosEsperados = resultadosEsperados;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

}
