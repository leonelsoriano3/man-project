package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AsistenteDTO;
import co.com.fspb.mgs.facade.api.AsistenteFacadeAPI;
import co.com.fspb.mgs.service.api.AsistenteServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link AsistenteFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AsistenteFacadeImpl
 * @date nov 11, 2016
 */
@Service("asistenteFacade")
public class AsistenteFacadeImpl implements AsistenteFacadeAPI {

    @Autowired
    private AsistenteServiceAPI asistenteService;

    /**
     * @see co.com.fspb.mgs.facade.api.AsistenteFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<AsistenteDTO> getRecords(PmzPagingCriteria criteria) {
        return asistenteService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.AsistenteFacadeAPI#guardar()
     */
    @Override
    public void guardar(AsistenteDTO asistente) throws PmzException {
        asistenteService.guardar(asistente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AsistenteFacadeAPI#editar()
     */
    @Override
    public void editar(AsistenteDTO asistente) throws PmzException {
        asistenteService.editar(asistente);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AsistenteFacadeAPI#getAsistenteDTO()
     */
    @Override
    public AsistenteDTO getAsistenteDTO(Long id) {
        return asistenteService.getAsistenteDTO(id);
    }

}
