package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ObjetivoIniciativaDaoAPI;
import co.com.fspb.mgs.dao.api.ObjetivoDaoAPI;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dao.api.IniciativaDaoAPI;
import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativa;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativaPK;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.service.api.ObjetivoIniciativaServiceAPI;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dto.IniciativaDTO;

/**
 * Implementación de la Interfaz {@link ObjetivoIniciativaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ObjetivoIniciativaServiceImpl implements ObjetivoIniciativaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ObjetivoIniciativaDaoAPI objetivoIniciativaDao;
    @Autowired
    private ObjetivoDaoAPI objetivoDao;
    @Autowired
    private IniciativaDaoAPI iniciativaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ObjetivoIniciativaDTO objetivoIniciativaDTO) throws PmzException {


		ObjetivoIniciativaPK pk = new ObjetivoIniciativaPK();
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), objetivoIniciativaDTO.getObjetivoDTO()));
        pk.setIniciativa(DTOTransformer.getIniciativaFromIniciativaDTO(new Iniciativa(), objetivoIniciativaDTO.getIniciativaDTO()));
        pk.setIdUsuarioResp(objetivoIniciativaDTO.getIdUsuarioResp());
        ObjetivoIniciativa objetivoIniciativaValidate = objetivoIniciativaDao.get(pk);

        if (objetivoIniciativaValidate != null) {
            throw new PmzException("El objetivoIniciativa ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ObjetivoIniciativa objetivoIniciativa = DTOTransformer.getObjetivoIniciativaFromObjetivoIniciativaDTO(new ObjetivoIniciativa(), objetivoIniciativaDTO);

		pk = objetivoIniciativa.getId();
		if (objetivoIniciativaDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(objetivoIniciativaDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setObjetivo(objetivo);
		}
		
		if (objetivoIniciativaDTO.getIniciativaDTO() != null) {
       		Iniciativa iniciativa = iniciativaDao.get(objetivoIniciativaDTO.getIniciativaDTO().getId());
       		if (iniciativa == null) {
                throw new PmzException("El Iniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIniciativa(iniciativa);
		}
		
        objetivoIniciativa.setId(pk);
        
        objetivoIniciativaDao.save(objetivoIniciativa);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ObjetivoIniciativaDTO> getRecords(PmzPagingCriteria criteria) {
        List<ObjetivoIniciativa> listaReturn = objetivoIniciativaDao.getRecords(criteria);
        Long countTotalRegistros = objetivoIniciativaDao.countRecords(criteria);

        List<ObjetivoIniciativaDTO> resultList = new ArrayList<ObjetivoIniciativaDTO>();
        for (ObjetivoIniciativa objetivoIniciativa : listaReturn) {
            ObjetivoIniciativaDTO objetivoIniciativaDTO = DTOTransformer
                    .getObjetivoIniciativaDTOFromObjetivoIniciativa(objetivoIniciativa);
            resultList.add(objetivoIniciativaDTO);
        }
        
        return new PmzResultSet<ObjetivoIniciativaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ObjetivoIniciativaDTO objetivoIniciativaDTO) throws PmzException{


		ObjetivoIniciativaPK pk = new ObjetivoIniciativaPK();
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), objetivoIniciativaDTO.getObjetivoDTO()));
        pk.setIniciativa(DTOTransformer.getIniciativaFromIniciativaDTO(new Iniciativa(), objetivoIniciativaDTO.getIniciativaDTO()));
        pk.setIdUsuarioResp(objetivoIniciativaDTO.getIdUsuarioResp());
        ObjetivoIniciativa objetivoIniciativa = objetivoIniciativaDao.get(pk);

        if (objetivoIniciativa == null) {
            throw new PmzException("El objetivoIniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getObjetivoIniciativaFromObjetivoIniciativaDTO(objetivoIniciativa, objetivoIniciativaDTO);

		pk = objetivoIniciativa.getId();
		if (objetivoIniciativaDTO.getObjetivoDTO() != null) {
       		Objetivo objetivo = objetivoDao.get(objetivoIniciativaDTO.getObjetivoDTO().getId());
       		if (objetivo == null) {
                throw new PmzException("El Objetivo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setObjetivo(objetivo);
		}
		
		if (objetivoIniciativaDTO.getIniciativaDTO() != null) {
       		Iniciativa iniciativa = iniciativaDao.get(objetivoIniciativaDTO.getIniciativaDTO().getId());
       		if (iniciativa == null) {
                throw new PmzException("El Iniciativa no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setIniciativa(iniciativa);
		}
		
        objetivoIniciativa.setId(pk);
        
        objetivoIniciativaDao.update(objetivoIniciativa);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ObjetivoIniciativaDTO getObjetivoIniciativaDTO(ObjetivoDTO objetivo, IniciativaDTO iniciativa, Integer idUsuarioResp) {

		ObjetivoIniciativaPK pk = new ObjetivoIniciativaPK();
        pk.setObjetivo(DTOTransformer.getObjetivoFromObjetivoDTO(new Objetivo(), objetivo));
        pk.setIniciativa(DTOTransformer.getIniciativaFromIniciativaDTO(new Iniciativa(), iniciativa));
        pk.setIdUsuarioResp(idUsuarioResp);
        ObjetivoIniciativa objetivoIniciativa = objetivoIniciativaDao.get(pk);
        return DTOTransformer.getObjetivoIniciativaDTOFromObjetivoIniciativa(objetivoIniciativa);
    }
}
