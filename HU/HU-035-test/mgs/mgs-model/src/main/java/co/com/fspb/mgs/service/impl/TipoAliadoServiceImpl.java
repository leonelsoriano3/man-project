package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TipoAliadoDaoAPI;
import co.com.fspb.mgs.dao.model.TipoAliado;
import co.com.fspb.mgs.service.api.TipoAliadoServiceAPI;
import co.com.fspb.mgs.dto.TipoAliadoDTO;

/**
 * Implementación de la Interfaz {@link TipoAliadoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoAliadoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TipoAliadoServiceImpl implements TipoAliadoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TipoAliadoDaoAPI tipoAliadoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TipoAliadoDTO tipoAliadoDTO) throws PmzException {

        TipoAliado tipoAliadoValidate = tipoAliadoDao.get(tipoAliadoDTO.getId());

        if (tipoAliadoValidate != null) {
            throw new PmzException("El tipoAliado ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TipoAliado tipoAliado = DTOTransformer.getTipoAliadoFromTipoAliadoDTO(new TipoAliado(), tipoAliadoDTO);

        tipoAliadoDao.save(tipoAliado);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoAliadoDTO> getRecords(PmzPagingCriteria criteria) {
        List<TipoAliado> listaReturn = tipoAliadoDao.getRecords(criteria);
        Long countTotalRegistros = tipoAliadoDao.countRecords(criteria);

        List<TipoAliadoDTO> resultList = new ArrayList<TipoAliadoDTO>();
        for (TipoAliado tipoAliado : listaReturn) {
            TipoAliadoDTO tipoAliadoDTO = DTOTransformer
                    .getTipoAliadoDTOFromTipoAliado(tipoAliado);
            resultList.add(tipoAliadoDTO);
        }
        
        return new PmzResultSet<TipoAliadoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TipoAliadoDTO tipoAliadoDTO) throws PmzException{

        TipoAliado tipoAliado = tipoAliadoDao.get(tipoAliadoDTO.getId());

        if (tipoAliado == null) {
            throw new PmzException("El tipoAliado no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTipoAliadoFromTipoAliadoDTO(tipoAliado, tipoAliadoDTO);

        tipoAliadoDao.update(tipoAliado);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TipoAliadoDTO getTipoAliadoDTO(Long id) {
        TipoAliado tipoAliado = tipoAliadoDao.get(id);
        return DTOTransformer.getTipoAliadoDTOFromTipoAliado(tipoAliado);
    }
}
