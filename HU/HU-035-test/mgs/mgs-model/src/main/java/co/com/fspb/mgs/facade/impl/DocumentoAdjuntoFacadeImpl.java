package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;
import co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI;
import co.com.fspb.mgs.service.api.DocumentoAdjuntoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link DocumentoAdjuntoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DocumentoAdjuntoFacadeImpl
 * @date nov 11, 2016
 */
@Service("documentoAdjuntoFacade")
public class DocumentoAdjuntoFacadeImpl implements DocumentoAdjuntoFacadeAPI {

    @Autowired
    private DocumentoAdjuntoServiceAPI documentoAdjuntoService;

    /**
     * @see co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<DocumentoAdjuntoDTO> getRecords(PmzPagingCriteria criteria) {
        return documentoAdjuntoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI#guardar()
     */
    @Override
    public void guardar(DocumentoAdjuntoDTO documentoAdjunto) throws PmzException {
        documentoAdjuntoService.guardar(documentoAdjunto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI#editar()
     */
    @Override
    public void editar(DocumentoAdjuntoDTO documentoAdjunto) throws PmzException {
        documentoAdjuntoService.editar(documentoAdjunto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.DocumentoAdjuntoFacadeAPI#getDocumentoAdjuntoDTO()
     */
    @Override
    public DocumentoAdjuntoDTO getDocumentoAdjuntoDTO(Long id) {
        return documentoAdjuntoService.getDocumentoAdjuntoDTO(id);
    }

}
