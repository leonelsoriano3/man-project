package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ProyectoBeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dao.api.ProyectoDaoAPI;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dao.model.ProyectoBeneficiario;
import co.com.fspb.mgs.service.api.ProyectoBeneficiarioServiceAPI;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;

/**
 * Implementación de la Interfaz {@link ProyectoBeneficiarioServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ProyectoBeneficiarioServiceImpl implements ProyectoBeneficiarioServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ProyectoBeneficiarioDaoAPI proyectoBeneficiarioDao;
    @Autowired
    private BeneficiarioDaoAPI beneficiarioDao;
    @Autowired
    private ProyectoDaoAPI proyectoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ProyectoBeneficiarioDTO proyectoBeneficiarioDTO) throws PmzException {

        ProyectoBeneficiario proyectoBeneficiarioValidate = proyectoBeneficiarioDao.get(proyectoBeneficiarioDTO.getId());

        if (proyectoBeneficiarioValidate != null) {
            throw new PmzException("El proyectoBeneficiario ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        ProyectoBeneficiario proyectoBeneficiario = DTOTransformer.getProyectoBeneficiarioFromProyectoBeneficiarioDTO(new ProyectoBeneficiario(), proyectoBeneficiarioDTO);

		if (proyectoBeneficiarioDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(proyectoBeneficiarioDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyectoBeneficiario.setBeneficiario(beneficiario);
		}
		
		if (proyectoBeneficiarioDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(proyectoBeneficiarioDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyectoBeneficiario.setProyecto(proyecto);
		}
		
        proyectoBeneficiarioDao.save(proyectoBeneficiario);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ProyectoBeneficiarioDTO> getRecords(PmzPagingCriteria criteria) {
        List<ProyectoBeneficiario> listaReturn = proyectoBeneficiarioDao.getRecords(criteria);
        Long countTotalRegistros = proyectoBeneficiarioDao.countRecords(criteria);

        List<ProyectoBeneficiarioDTO> resultList = new ArrayList<ProyectoBeneficiarioDTO>();
        for (ProyectoBeneficiario proyectoBeneficiario : listaReturn) {
            ProyectoBeneficiarioDTO proyectoBeneficiarioDTO = DTOTransformer
                    .getProyectoBeneficiarioDTOFromProyectoBeneficiario(proyectoBeneficiario);
            resultList.add(proyectoBeneficiarioDTO);
        }
        
        return new PmzResultSet<ProyectoBeneficiarioDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ProyectoBeneficiarioDTO proyectoBeneficiarioDTO) throws PmzException{

        ProyectoBeneficiario proyectoBeneficiario = proyectoBeneficiarioDao.get(proyectoBeneficiarioDTO.getId());

        if (proyectoBeneficiario == null) {
            throw new PmzException("El proyectoBeneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getProyectoBeneficiarioFromProyectoBeneficiarioDTO(proyectoBeneficiario, proyectoBeneficiarioDTO);

		if (proyectoBeneficiarioDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(proyectoBeneficiarioDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyectoBeneficiario.setBeneficiario(beneficiario);
		}
		
		if (proyectoBeneficiarioDTO.getProyectoDTO() != null) {
       		Proyecto proyecto = proyectoDao.get(proyectoBeneficiarioDTO.getProyectoDTO().getId());
       		if (proyecto == null) {
                throw new PmzException("El Proyecto no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		proyectoBeneficiario.setProyecto(proyecto);
		}
		
        proyectoBeneficiarioDao.update(proyectoBeneficiario);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ProyectoBeneficiarioDTO getProyectoBeneficiarioDTO(Integer id) {
        ProyectoBeneficiario proyectoBeneficiario = proyectoBeneficiarioDao.get(id);
        return DTOTransformer.getProyectoBeneficiarioDTOFromProyectoBeneficiario(proyectoBeneficiario);
    }
}
