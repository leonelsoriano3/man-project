package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.TipoTenenciaDaoAPI;
import co.com.fspb.mgs.dao.model.TipoTenencia;
import co.com.fspb.mgs.service.api.TipoTenenciaServiceAPI;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;

/**
 * Implementación de la Interfaz {@link TipoTenenciaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TipoTenenciaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class TipoTenenciaServiceImpl implements TipoTenenciaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private TipoTenenciaDaoAPI tipoTenenciaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(TipoTenenciaDTO tipoTenenciaDTO) throws PmzException {

        TipoTenencia tipoTenenciaValidate = tipoTenenciaDao.get(tipoTenenciaDTO.getId());

        if (tipoTenenciaValidate != null) {
            throw new PmzException("El tipoTenencia ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        TipoTenencia tipoTenencia = DTOTransformer.getTipoTenenciaFromTipoTenenciaDTO(new TipoTenencia(), tipoTenenciaDTO);

        tipoTenenciaDao.save(tipoTenencia);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<TipoTenenciaDTO> getRecords(PmzPagingCriteria criteria) {
        List<TipoTenencia> listaReturn = tipoTenenciaDao.getRecords(criteria);
        Long countTotalRegistros = tipoTenenciaDao.countRecords(criteria);

        List<TipoTenenciaDTO> resultList = new ArrayList<TipoTenenciaDTO>();
        for (TipoTenencia tipoTenencia : listaReturn) {
            TipoTenenciaDTO tipoTenenciaDTO = DTOTransformer
                    .getTipoTenenciaDTOFromTipoTenencia(tipoTenencia);
            resultList.add(tipoTenenciaDTO);
        }
        
        return new PmzResultSet<TipoTenenciaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(TipoTenenciaDTO tipoTenenciaDTO) throws PmzException{

        TipoTenencia tipoTenencia = tipoTenenciaDao.get(tipoTenenciaDTO.getId());

        if (tipoTenencia == null) {
            throw new PmzException("El tipoTenencia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getTipoTenenciaFromTipoTenenciaDTO(tipoTenencia, tipoTenenciaDTO);

        tipoTenenciaDao.update(tipoTenencia);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public TipoTenenciaDTO getTipoTenenciaDTO(Long id) {
        TipoTenencia tipoTenencia = tipoTenenciaDao.get(id);
        return DTOTransformer.getTipoTenenciaDTOFromTipoTenencia(tipoTenencia);
    }
}
