package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dto.ComponenteDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link ComponenteProyecto}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoServiceAPI
 * @date nov 11, 2016
 */
public interface ComponenteProyectoServiceAPI {

    /**
	 * Registra una entidad {@link ComponenteProyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componenteProyecto - {@link ComponenteProyectoDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ComponenteProyectoDTO componenteProyecto) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link ComponenteProyecto} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componenteProyecto - {@link ComponenteProyectoDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ComponenteProyectoDTO componenteProyecto) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ComponenteProyectoDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ComponenteProyectoDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyecto - {@link ProyectoDTO}
	 * @param componente - {@link ComponenteDTO}
     * @return {@link ComponenteProyectoDTO}
     */
    ComponenteProyectoDTO getComponenteProyectoDTO(ProyectoDTO proyecto, ComponenteDTO componente);

}
