package co.com.fspb.mgs.dao.api;

import java.util.List;

import com.premize.pmz.dao.api.Dao;

import co.com.fspb.mgs.dao.model.ObjetivoIniciativa;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativaPK;
import com.premize.pmz.api.dto.PmzPagingCriteria;

/**
 * Interfaz de Data Access Object (DAO) para la entidad {@link ObjetivoIniciativa}
 * Extiende la interfaz de PMZ {@link Dao}
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaDaoAPI
 * @date nov 11, 2016
 */
public interface ObjetivoIniciativaDaoAPI extends Dao<ObjetivoIniciativa, ObjetivoIniciativaPK> {

    /**
	 * Retorna la lista paginada y filtrada según el {@link PmzPagingCriteria}
	 * de la entidad {@link ObjetivoIniciativa}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public List<ObjetivoIniciativa> getRecords(PmzPagingCriteria criteria);
    
    /**
	 * Retorna el total de regirtos según el {@link PmzPagingCriteria}
	 * de la entidad {@link ObjetivoIniciativa}
	 * 
	 * @author @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param criteria - {@link PmzPagingCriteria}
	 * @return {@link List}
	 */
    public Long countRecords(PmzPagingCriteria criteria);

}
