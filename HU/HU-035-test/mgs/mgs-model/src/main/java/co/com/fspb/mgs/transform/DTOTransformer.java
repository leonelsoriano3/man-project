package co.com.fspb.mgs.transform;

import co.com.fspb.mgs.dao.model.AreaEducacion;
import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.dao.model.TipoVivienda;
import co.com.fspb.mgs.dto.TipoViviendaDTO;
import co.com.fspb.mgs.dao.model.Servicio;
import co.com.fspb.mgs.dto.ServicioDTO;
import co.com.fspb.mgs.dao.model.GrupoEtnico;
import co.com.fspb.mgs.dto.GrupoEtnicoDTO;
import co.com.fspb.mgs.dao.model.EstadoVia;
import co.com.fspb.mgs.dto.EstadoViaDTO;
import co.com.fspb.mgs.dao.model.ParametroGeneral;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.dao.model.EstadoActividad;
import co.com.fspb.mgs.dto.EstadoActividadDTO;
import co.com.fspb.mgs.dao.model.LineaIntervencion;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;
import co.com.fspb.mgs.dao.model.ProblemaComunidad;
import co.com.fspb.mgs.dto.ProblemaComunidadDTO;
import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.dto.IndicadorDTO;
import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.dto.ZonaDTO;
import co.com.fspb.mgs.dao.model.Rol;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dao.model.BdNacional;
import co.com.fspb.mgs.dto.BdNacionalDTO;
import co.com.fspb.mgs.dao.model.TipoTenencia;
import co.com.fspb.mgs.dto.TipoTenenciaDTO;
import co.com.fspb.mgs.dao.model.Municipio;
import co.com.fspb.mgs.dto.MunicipioDTO;
import co.com.fspb.mgs.dao.model.Componente;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.dao.model.NivelEducativo;
import co.com.fspb.mgs.dto.NivelEducativoDTO;
import co.com.fspb.mgs.dao.model.DescIndependiente;
import co.com.fspb.mgs.dto.DescIndependienteDTO;
import co.com.fspb.mgs.dao.model.Parentesco;
import co.com.fspb.mgs.dto.ParentescoDTO;
import co.com.fspb.mgs.dao.model.EstadoCivil;
import co.com.fspb.mgs.dto.EstadoCivilDTO;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dto.PersonaDTO;
import co.com.fspb.mgs.dao.model.Ocupacion;
import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.dao.model.MaterialVivienda;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.dao.model.RolFundacion;
import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.dao.model.Cargo;
import co.com.fspb.mgs.dto.CargoDTO;
import co.com.fspb.mgs.dao.model.AfiliadoSalud;
import co.com.fspb.mgs.dto.AfiliadoSaludDTO;
import co.com.fspb.mgs.dao.model.EstructuraFamiliar;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.dao.model.TipoContacto;
import co.com.fspb.mgs.dto.TipoContactoDTO;
import co.com.fspb.mgs.dao.model.TipoAliado;
import co.com.fspb.mgs.dto.TipoAliadoDTO;
import co.com.fspb.mgs.dao.model.Usuario;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.dao.model.Lider;
import co.com.fspb.mgs.dto.LiderDTO;
import co.com.fspb.mgs.dao.model.TipoTerritorio;
import co.com.fspb.mgs.dto.TipoTerritorioDTO;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.dto.EntidadDTO;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dao.model.DatoContacto;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.dao.model.Pariente;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.dao.model.ParientePK;
import co.com.fspb.mgs.dao.model.BeneficiarioServicio;
import co.com.fspb.mgs.dto.BeneficiarioServicioDTO;
import co.com.fspb.mgs.dao.model.BeneficiarioServicioPK;
import co.com.fspb.mgs.dao.model.Objetivo;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dao.model.RolUsuario;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.dao.model.RolUsuarioPK;
import co.com.fspb.mgs.dao.model.EntidadLinea;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.dao.model.EntidadLineaPK;
import co.com.fspb.mgs.dao.model.Iniciativa;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.dao.model.Territorio;
import co.com.fspb.mgs.dto.TerritorioDTO;
import co.com.fspb.mgs.dao.model.Actividad;
import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.dao.model.EntidadTerritorio;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.dao.model.EntidadTerritorioPK;
import co.com.fspb.mgs.dao.model.Linea;
import co.com.fspb.mgs.dto.LineaDTO;
import co.com.fspb.mgs.dao.model.IndicadorObjetivo;
import co.com.fspb.mgs.dto.IndicadorObjetivoDTO;
import co.com.fspb.mgs.dao.model.IndicadorObjetivoPK;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativa;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.dao.model.ObjetivoIniciativaPK;
import co.com.fspb.mgs.dao.model.LiderTerritorio;
import co.com.fspb.mgs.dto.LiderTerritorioDTO;
import co.com.fspb.mgs.dao.model.LiderTerritorioPK;
import co.com.fspb.mgs.dao.model.TrazaActividad;
import co.com.fspb.mgs.dto.TrazaActividadDTO;
import co.com.fspb.mgs.dao.model.Proyecto;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dao.model.ProyectoBeneficiario;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import co.com.fspb.mgs.dao.model.ProyectoTerritorio;
import co.com.fspb.mgs.dto.ProyectoTerritorioDTO;
import co.com.fspb.mgs.dao.model.ProyectoTerritorioPK;
import co.com.fspb.mgs.dao.model.TrazaPresupuesto;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import co.com.fspb.mgs.dao.model.ComponenteProyecto;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.dao.model.ComponenteProyectoPK;
import co.com.fspb.mgs.dao.model.DocumentoAdjunto;
import co.com.fspb.mgs.dto.DocumentoAdjuntoDTO;
import co.com.fspb.mgs.dao.model.IndicadorProyecto;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.dao.model.IndicadorProyectoPK;
import co.com.fspb.mgs.dao.model.Aliado;
import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.dao.model.DatoAdicional;
import co.com.fspb.mgs.dto.DatoAdicionalDTO;
import co.com.fspb.mgs.dao.model.Asistente;
import co.com.fspb.mgs.dto.AsistenteDTO;
import co.com.fspb.mgs.dao.model.ValorDatoAdicional;
import co.com.fspb.mgs.dto.ValorDatoAdicionalDTO;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.TipoParametroEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.TipoIdEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstratoSocioeconomicoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import co.com.fspb.mgs.enums.EstadoEnum;
import com.premize.pmz.api.dto.PmzEnumUtils;

/**
 * Clase de transformación de entidades de base de datos a DTOs y viceversa
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DTOTransformer
 * @date nov 11, 2016
 */
public class DTOTransformer {
    
    private DTOTransformer(){
    }

    /**
	 * Transforma la entidad {@link AreaEducacion} al DTO {@link AreaEducacionDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param areaEducacion - {@link AreaEducacion}
	 * @return {@link AreaEducacionDTO}
	 */
    public static AreaEducacionDTO getAreaEducacionDTOFromAreaEducacion(AreaEducacion areaEducacion){
    
    	if (areaEducacion == null) {
            return null;
        }
        
        AreaEducacionDTO dto = new AreaEducacionDTO();

        dto.setId(areaEducacion.getId());
        
        dto.setNombre(areaEducacion.getNombre());
        dto.setFechaModifica(areaEducacion.getFechaModifica());
        dto.setEstado(areaEducacion.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(areaEducacion.getEstado()));
        dto.setUsuarioModifica(areaEducacion.getUsuarioModifica());
        dto.setUsuarioCrea(areaEducacion.getUsuarioCrea());
        dto.setFechaCrea(areaEducacion.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link AreaEducacionDTO} a la entidad {@link AreaEducacion}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param areaEducacion - {@link AreaEducacion}
	 * @param areaEducacionDTO - {@link AreaEducacionDTO}
	 * @return {@link AreaEducacion}
	 */
    public static AreaEducacion getAreaEducacionFromAreaEducacionDTO(AreaEducacion areaEducacion, AreaEducacionDTO areaEducacionDTO){
    
    	if (areaEducacionDTO == null) {
            return null;
        }
        
        if(areaEducacion == null){
        	areaEducacion = new AreaEducacion();
        }
        
        areaEducacion.setId(areaEducacionDTO.getId());
        
        areaEducacion.setNombre(areaEducacionDTO.getNombre());
        areaEducacion.setFechaModifica(areaEducacionDTO.getFechaModifica());
        areaEducacion.setEstado(EstadoEnum.valueOf(areaEducacionDTO.getEstado()));
        areaEducacion.setUsuarioModifica(areaEducacionDTO.getUsuarioModifica());
        areaEducacion.setUsuarioCrea(areaEducacionDTO.getUsuarioCrea());
        areaEducacion.setFechaCrea(areaEducacionDTO.getFechaCrea());
    
    	return areaEducacion;
    
    }

    /**
	 * Transforma la entidad {@link TipoVivienda} al DTO {@link TipoViviendaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoVivienda - {@link TipoVivienda}
	 * @return {@link TipoViviendaDTO}
	 */
    public static TipoViviendaDTO getTipoViviendaDTOFromTipoVivienda(TipoVivienda tipoVivienda){
    
    	if (tipoVivienda == null) {
            return null;
        }
        
        TipoViviendaDTO dto = new TipoViviendaDTO();

        dto.setId(tipoVivienda.getId());
        
        dto.setFechaModifica(tipoVivienda.getFechaModifica());
        dto.setEstado(tipoVivienda.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(tipoVivienda.getEstado()));
        dto.setValor(tipoVivienda.getValor());
        dto.setUsuarioModifica(tipoVivienda.getUsuarioModifica());
        dto.setUsuarioCrea(tipoVivienda.getUsuarioCrea());
        dto.setFechaCrea(tipoVivienda.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TipoViviendaDTO} a la entidad {@link TipoVivienda}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoVivienda - {@link TipoVivienda}
	 * @param tipoViviendaDTO - {@link TipoViviendaDTO}
	 * @return {@link TipoVivienda}
	 */
    public static TipoVivienda getTipoViviendaFromTipoViviendaDTO(TipoVivienda tipoVivienda, TipoViviendaDTO tipoViviendaDTO){
    
    	if (tipoViviendaDTO == null) {
            return null;
        }
        
        if(tipoVivienda == null){
        	tipoVivienda = new TipoVivienda();
        }
        
        tipoVivienda.setId(tipoViviendaDTO.getId());
        
        tipoVivienda.setFechaModifica(tipoViviendaDTO.getFechaModifica());
        tipoVivienda.setEstado(EstadoEnum.valueOf(tipoViviendaDTO.getEstado()));
        tipoVivienda.setValor(tipoViviendaDTO.getValor());
        tipoVivienda.setUsuarioModifica(tipoViviendaDTO.getUsuarioModifica());
        tipoVivienda.setUsuarioCrea(tipoViviendaDTO.getUsuarioCrea());
        tipoVivienda.setFechaCrea(tipoViviendaDTO.getFechaCrea());
    
    	return tipoVivienda;
    
    }

    /**
	 * Transforma la entidad {@link Servicio} al DTO {@link ServicioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param servicio - {@link Servicio}
	 * @return {@link ServicioDTO}
	 */
    public static ServicioDTO getServicioDTOFromServicio(Servicio servicio){
    
    	if (servicio == null) {
            return null;
        }
        
        ServicioDTO dto = new ServicioDTO();

        dto.setId(servicio.getId());
        
        dto.setNombre(servicio.getNombre());
        dto.setFechaModifica(servicio.getFechaModifica());
        dto.setEstado(servicio.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(servicio.getEstado()));
        dto.setUsuarioModifica(servicio.getUsuarioModifica());
        dto.setTipoServicio(servicio.getTipoServicio());
        dto.setUsuarioCrea(servicio.getUsuarioCrea());
        dto.setFechaCrea(servicio.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ServicioDTO} a la entidad {@link Servicio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param servicio - {@link Servicio}
	 * @param servicioDTO - {@link ServicioDTO}
	 * @return {@link Servicio}
	 */
    public static Servicio getServicioFromServicioDTO(Servicio servicio, ServicioDTO servicioDTO){
    
    	if (servicioDTO == null) {
            return null;
        }
        
        if(servicio == null){
        	servicio = new Servicio();
        }
        
        servicio.setId(servicioDTO.getId());
        
        servicio.setNombre(servicioDTO.getNombre());
        servicio.setFechaModifica(servicioDTO.getFechaModifica());
        servicio.setEstado(EstadoEnum.valueOf(servicioDTO.getEstado()));
        servicio.setUsuarioModifica(servicioDTO.getUsuarioModifica());
        servicio.setTipoServicio(servicioDTO.getTipoServicio());
        servicio.setUsuarioCrea(servicioDTO.getUsuarioCrea());
        servicio.setFechaCrea(servicioDTO.getFechaCrea());
    
    	return servicio;
    
    }

    /**
	 * Transforma la entidad {@link GrupoEtnico} al DTO {@link GrupoEtnicoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param grupoEtnico - {@link GrupoEtnico}
	 * @return {@link GrupoEtnicoDTO}
	 */
    public static GrupoEtnicoDTO getGrupoEtnicoDTOFromGrupoEtnico(GrupoEtnico grupoEtnico){
    
    	if (grupoEtnico == null) {
            return null;
        }
        
        GrupoEtnicoDTO dto = new GrupoEtnicoDTO();

        dto.setId(grupoEtnico.getId());
        
        dto.setFechaModifica(grupoEtnico.getFechaModifica());
        dto.setEstado(grupoEtnico.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(grupoEtnico.getEstado()));
        dto.setValor(grupoEtnico.getValor());
        dto.setUsuarioModifica(grupoEtnico.getUsuarioModifica());
        dto.setUsuarioCrea(grupoEtnico.getUsuarioCrea());
        dto.setFechaCrea(grupoEtnico.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link GrupoEtnicoDTO} a la entidad {@link GrupoEtnico}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param grupoEtnico - {@link GrupoEtnico}
	 * @param grupoEtnicoDTO - {@link GrupoEtnicoDTO}
	 * @return {@link GrupoEtnico}
	 */
    public static GrupoEtnico getGrupoEtnicoFromGrupoEtnicoDTO(GrupoEtnico grupoEtnico, GrupoEtnicoDTO grupoEtnicoDTO){
    
    	if (grupoEtnicoDTO == null) {
            return null;
        }
        
        if(grupoEtnico == null){
        	grupoEtnico = new GrupoEtnico();
        }
        
        grupoEtnico.setId(grupoEtnicoDTO.getId());
        
        grupoEtnico.setFechaModifica(grupoEtnicoDTO.getFechaModifica());
        grupoEtnico.setEstado(EstadoEnum.valueOf(grupoEtnicoDTO.getEstado()));
        grupoEtnico.setValor(grupoEtnicoDTO.getValor());
        grupoEtnico.setUsuarioModifica(grupoEtnicoDTO.getUsuarioModifica());
        grupoEtnico.setUsuarioCrea(grupoEtnicoDTO.getUsuarioCrea());
        grupoEtnico.setFechaCrea(grupoEtnicoDTO.getFechaCrea());
    
    	return grupoEtnico;
    
    }

    /**
	 * Transforma la entidad {@link EstadoVia} al DTO {@link EstadoViaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoVia - {@link EstadoVia}
	 * @return {@link EstadoViaDTO}
	 */
    public static EstadoViaDTO getEstadoViaDTOFromEstadoVia(EstadoVia estadoVia){
    
    	if (estadoVia == null) {
            return null;
        }
        
        EstadoViaDTO dto = new EstadoViaDTO();

        dto.setId(estadoVia.getId());
        
        dto.setFechaModifica(estadoVia.getFechaModifica());
        dto.setEstado(estadoVia.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(estadoVia.getEstado()));
        dto.setValor(estadoVia.getValor());
        dto.setUsuarioModifica(estadoVia.getUsuarioModifica());
        dto.setUsuarioCrea(estadoVia.getUsuarioCrea());
        dto.setFechaCrea(estadoVia.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EstadoViaDTO} a la entidad {@link EstadoVia}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoVia - {@link EstadoVia}
	 * @param estadoViaDTO - {@link EstadoViaDTO}
	 * @return {@link EstadoVia}
	 */
    public static EstadoVia getEstadoViaFromEstadoViaDTO(EstadoVia estadoVia, EstadoViaDTO estadoViaDTO){
    
    	if (estadoViaDTO == null) {
            return null;
        }
        
        if(estadoVia == null){
        	estadoVia = new EstadoVia();
        }
        
        estadoVia.setId(estadoViaDTO.getId());
        
        estadoVia.setFechaModifica(estadoViaDTO.getFechaModifica());
        estadoVia.setEstado(EstadoEnum.valueOf(estadoViaDTO.getEstado()));
        estadoVia.setValor(estadoViaDTO.getValor());
        estadoVia.setUsuarioModifica(estadoViaDTO.getUsuarioModifica());
        estadoVia.setUsuarioCrea(estadoViaDTO.getUsuarioCrea());
        estadoVia.setFechaCrea(estadoViaDTO.getFechaCrea());
    
    	return estadoVia;
    
    }

    /**
	 * Transforma la entidad {@link ParametroGeneral} al DTO {@link ParametroGeneralDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parametroGeneral - {@link ParametroGeneral}
	 * @return {@link ParametroGeneralDTO}
	 */
    public static ParametroGeneralDTO getParametroGeneralDTOFromParametroGeneral(ParametroGeneral parametroGeneral){
    
    	if (parametroGeneral == null) {
            return null;
        }
        
        ParametroGeneralDTO dto = new ParametroGeneralDTO();

        dto.setId(parametroGeneral.getId());
        
        dto.setTipoParametro(parametroGeneral.getTipoParametro().name());
        dto.setTipoParametroDesc(PmzEnumUtils.obtenerEnumLabel(parametroGeneral.getTipoParametro()));
        dto.setFechaModifica(parametroGeneral.getFechaModifica());
        dto.setEstado(parametroGeneral.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(parametroGeneral.getEstado()));
        dto.setValor(parametroGeneral.getValor());
        dto.setUsuarioModifica(parametroGeneral.getUsuarioModifica());
        dto.setDescripcion(parametroGeneral.getDescripcion());
        dto.setUsuarioCrea(parametroGeneral.getUsuarioCrea());
        dto.setFechaCrea(parametroGeneral.getFechaCrea());
        dto.setClave(parametroGeneral.getClave());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ParametroGeneralDTO} a la entidad {@link ParametroGeneral}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parametroGeneral - {@link ParametroGeneral}
	 * @param parametroGeneralDTO - {@link ParametroGeneralDTO}
	 * @return {@link ParametroGeneral}
	 */
    public static ParametroGeneral getParametroGeneralFromParametroGeneralDTO(ParametroGeneral parametroGeneral, ParametroGeneralDTO parametroGeneralDTO){
    
    	if (parametroGeneralDTO == null) {
            return null;
        }
        
        if(parametroGeneral == null){
        	parametroGeneral = new ParametroGeneral();
        }
        
        parametroGeneral.setId(parametroGeneralDTO.getId());
        
        parametroGeneral.setTipoParametro(TipoParametroEnum.valueOf(parametroGeneralDTO.getTipoParametro()));
        parametroGeneral.setFechaModifica(parametroGeneralDTO.getFechaModifica());
        parametroGeneral.setEstado(EstadoEnum.valueOf(parametroGeneralDTO.getEstado()));
        parametroGeneral.setValor(parametroGeneralDTO.getValor());
        parametroGeneral.setUsuarioModifica(parametroGeneralDTO.getUsuarioModifica());
        parametroGeneral.setDescripcion(parametroGeneralDTO.getDescripcion());
        parametroGeneral.setUsuarioCrea(parametroGeneralDTO.getUsuarioCrea());
        parametroGeneral.setFechaCrea(parametroGeneralDTO.getFechaCrea());
        parametroGeneral.setClave(parametroGeneralDTO.getClave());
    
    	return parametroGeneral;
    
    }

    /**
	 * Transforma la entidad {@link EstadoActividad} al DTO {@link EstadoActividadDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoActividad - {@link EstadoActividad}
	 * @return {@link EstadoActividadDTO}
	 */
    public static EstadoActividadDTO getEstadoActividadDTOFromEstadoActividad(EstadoActividad estadoActividad){
    
    	if (estadoActividad == null) {
            return null;
        }
        
        EstadoActividadDTO dto = new EstadoActividadDTO();

        dto.setId(estadoActividad.getId());
        
        dto.setFechaModifica(estadoActividad.getFechaModifica());
        dto.setEstado(estadoActividad.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(estadoActividad.getEstado()));
        dto.setValor(estadoActividad.getValor());
        dto.setUsuarioModifica(estadoActividad.getUsuarioModifica());
        dto.setUsuarioCrea(estadoActividad.getUsuarioCrea());
        dto.setFechaCrea(estadoActividad.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EstadoActividadDTO} a la entidad {@link EstadoActividad}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoActividad - {@link EstadoActividad}
	 * @param estadoActividadDTO - {@link EstadoActividadDTO}
	 * @return {@link EstadoActividad}
	 */
    public static EstadoActividad getEstadoActividadFromEstadoActividadDTO(EstadoActividad estadoActividad, EstadoActividadDTO estadoActividadDTO){
    
    	if (estadoActividadDTO == null) {
            return null;
        }
        
        if(estadoActividad == null){
        	estadoActividad = new EstadoActividad();
        }
        
        estadoActividad.setId(estadoActividadDTO.getId());
        
        estadoActividad.setFechaModifica(estadoActividadDTO.getFechaModifica());
        estadoActividad.setEstado(EstadoEnum.valueOf(estadoActividadDTO.getEstado()));
        estadoActividad.setValor(estadoActividadDTO.getValor());
        estadoActividad.setUsuarioModifica(estadoActividadDTO.getUsuarioModifica());
        estadoActividad.setUsuarioCrea(estadoActividadDTO.getUsuarioCrea());
        estadoActividad.setFechaCrea(estadoActividadDTO.getFechaCrea());
    
    	return estadoActividad;
    
    }

    /**
	 * Transforma la entidad {@link LineaIntervencion} al DTO {@link LineaIntervencionDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param lineaIntervencion - {@link LineaIntervencion}
	 * @return {@link LineaIntervencionDTO}
	 */
    public static LineaIntervencionDTO getLineaIntervencionDTOFromLineaIntervencion(LineaIntervencion lineaIntervencion){
    
    	if (lineaIntervencion == null) {
            return null;
        }
        
        LineaIntervencionDTO dto = new LineaIntervencionDTO();

        dto.setId(lineaIntervencion.getId());
        
        dto.setFechaModifica(lineaIntervencion.getFechaModifica());
        dto.setEstado(lineaIntervencion.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(lineaIntervencion.getEstado()));
        dto.setValor(lineaIntervencion.getValor());
        dto.setUsuarioModifica(lineaIntervencion.getUsuarioModifica());
        dto.setUsuarioCrea(lineaIntervencion.getUsuarioCrea());
        dto.setFechaCrea(lineaIntervencion.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link LineaIntervencionDTO} a la entidad {@link LineaIntervencion}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param lineaIntervencion - {@link LineaIntervencion}
	 * @param lineaIntervencionDTO - {@link LineaIntervencionDTO}
	 * @return {@link LineaIntervencion}
	 */
    public static LineaIntervencion getLineaIntervencionFromLineaIntervencionDTO(LineaIntervencion lineaIntervencion, LineaIntervencionDTO lineaIntervencionDTO){
    
    	if (lineaIntervencionDTO == null) {
            return null;
        }
        
        if(lineaIntervencion == null){
        	lineaIntervencion = new LineaIntervencion();
        }
        
        lineaIntervencion.setId(lineaIntervencionDTO.getId());
        
        lineaIntervencion.setFechaModifica(lineaIntervencionDTO.getFechaModifica());
        lineaIntervencion.setEstado(EstadoEnum.valueOf(lineaIntervencionDTO.getEstado()));
        lineaIntervencion.setValor(lineaIntervencionDTO.getValor());
        lineaIntervencion.setUsuarioModifica(lineaIntervencionDTO.getUsuarioModifica());
        lineaIntervencion.setUsuarioCrea(lineaIntervencionDTO.getUsuarioCrea());
        lineaIntervencion.setFechaCrea(lineaIntervencionDTO.getFechaCrea());
    
    	return lineaIntervencion;
    
    }

    /**
	 * Transforma la entidad {@link ProblemaComunidad} al DTO {@link ProblemaComunidadDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param problemaComunidad - {@link ProblemaComunidad}
	 * @return {@link ProblemaComunidadDTO}
	 */
    public static ProblemaComunidadDTO getProblemaComunidadDTOFromProblemaComunidad(ProblemaComunidad problemaComunidad){
    
    	if (problemaComunidad == null) {
            return null;
        }
        
        ProblemaComunidadDTO dto = new ProblemaComunidadDTO();

        dto.setId(problemaComunidad.getId());
        
        dto.setFechaModifica(problemaComunidad.getFechaModifica());
        dto.setEstado(problemaComunidad.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(problemaComunidad.getEstado()));
        dto.setValor(problemaComunidad.getValor());
        dto.setUsuarioModifica(problemaComunidad.getUsuarioModifica());
        dto.setUsuarioCrea(problemaComunidad.getUsuarioCrea());
        dto.setFechaCrea(problemaComunidad.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ProblemaComunidadDTO} a la entidad {@link ProblemaComunidad}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param problemaComunidad - {@link ProblemaComunidad}
	 * @param problemaComunidadDTO - {@link ProblemaComunidadDTO}
	 * @return {@link ProblemaComunidad}
	 */
    public static ProblemaComunidad getProblemaComunidadFromProblemaComunidadDTO(ProblemaComunidad problemaComunidad, ProblemaComunidadDTO problemaComunidadDTO){
    
    	if (problemaComunidadDTO == null) {
            return null;
        }
        
        if(problemaComunidad == null){
        	problemaComunidad = new ProblemaComunidad();
        }
        
        problemaComunidad.setId(problemaComunidadDTO.getId());
        
        problemaComunidad.setFechaModifica(problemaComunidadDTO.getFechaModifica());
        problemaComunidad.setEstado(EstadoEnum.valueOf(problemaComunidadDTO.getEstado()));
        problemaComunidad.setValor(problemaComunidadDTO.getValor());
        problemaComunidad.setUsuarioModifica(problemaComunidadDTO.getUsuarioModifica());
        problemaComunidad.setUsuarioCrea(problemaComunidadDTO.getUsuarioCrea());
        problemaComunidad.setFechaCrea(problemaComunidadDTO.getFechaCrea());
    
    	return problemaComunidad;
    
    }

    /**
	 * Transforma la entidad {@link Indicador} al DTO {@link IndicadorDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link Indicador}
	 * @return {@link IndicadorDTO}
	 */
    public static IndicadorDTO getIndicadorDTOFromIndicador(Indicador indicador){
    
    	if (indicador == null) {
            return null;
        }
        
        IndicadorDTO dto = new IndicadorDTO();

        dto.setId(indicador.getId());
        
        dto.setNombre(indicador.getNombre());
        dto.setFechaModifica(indicador.getFechaModifica());
        dto.setEstado(indicador.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(indicador.getEstado()));
        dto.setUsuarioModifica(indicador.getUsuarioModifica());
        dto.setUsuarioCrea(indicador.getUsuarioCrea());
        dto.setFechaCrea(indicador.getFechaCrea());
        dto.setMeta(indicador.getMeta());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link IndicadorDTO} a la entidad {@link Indicador}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicador - {@link Indicador}
	 * @param indicadorDTO - {@link IndicadorDTO}
	 * @return {@link Indicador}
	 */
    public static Indicador getIndicadorFromIndicadorDTO(Indicador indicador, IndicadorDTO indicadorDTO){
    
    	if (indicadorDTO == null) {
            return null;
        }
        
        if(indicador == null){
        	indicador = new Indicador();
        }
        
        indicador.setId(indicadorDTO.getId());
        
        indicador.setNombre(indicadorDTO.getNombre());
        indicador.setFechaModifica(indicadorDTO.getFechaModifica());
        indicador.setEstado(EstadoEnum.valueOf(indicadorDTO.getEstado()));
        indicador.setUsuarioModifica(indicadorDTO.getUsuarioModifica());
        indicador.setUsuarioCrea(indicadorDTO.getUsuarioCrea());
        indicador.setFechaCrea(indicadorDTO.getFechaCrea());
        indicador.setMeta(indicadorDTO.getMeta());
    
    	return indicador;
    
    }

    /**
	 * Transforma la entidad {@link Zona} al DTO {@link ZonaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param zona - {@link Zona}
	 * @return {@link ZonaDTO}
	 */
    public static ZonaDTO getZonaDTOFromZona(Zona zona){
    
    	if (zona == null) {
            return null;
        }
        
        ZonaDTO dto = new ZonaDTO();

        dto.setId(zona.getId());
        
        dto.setFechaModifica(zona.getFechaModifica());
        dto.setEstado(zona.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(zona.getEstado()));
        dto.setValor(zona.getValor());
        dto.setUsuarioModifica(zona.getUsuarioModifica());
        dto.setUsuarioCrea(zona.getUsuarioCrea());
        dto.setFechaCrea(zona.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ZonaDTO} a la entidad {@link Zona}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param zona - {@link Zona}
	 * @param zonaDTO - {@link ZonaDTO}
	 * @return {@link Zona}
	 */
    public static Zona getZonaFromZonaDTO(Zona zona, ZonaDTO zonaDTO){
    
    	if (zonaDTO == null) {
            return null;
        }
        
        if(zona == null){
        	zona = new Zona();
        }
        
        zona.setId(zonaDTO.getId());
        
        zona.setFechaModifica(zonaDTO.getFechaModifica());
        zona.setEstado(EstadoEnum.valueOf(zonaDTO.getEstado()));
        zona.setValor(zonaDTO.getValor());
        zona.setUsuarioModifica(zonaDTO.getUsuarioModifica());
        zona.setUsuarioCrea(zonaDTO.getUsuarioCrea());
        zona.setFechaCrea(zonaDTO.getFechaCrea());
    
    	return zona;
    
    }

    /**
	 * Transforma la entidad {@link Rol} al DTO {@link RolDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rol - {@link Rol}
	 * @return {@link RolDTO}
	 */
    public static RolDTO getRolDTOFromRol(Rol rol){
    
    	if (rol == null) {
            return null;
        }
        
        RolDTO dto = new RolDTO();

        dto.setId(rol.getId());
        
        dto.setNombre(rol.getNombre());
        dto.setFechaModifica(rol.getFechaModifica());
        dto.setEstado(rol.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(rol.getEstado()));
        dto.setUsuarioModifica(rol.getUsuarioModifica());
        dto.setDescripcion(rol.getDescripcion());
        dto.setUsuarioCrea(rol.getUsuarioCrea());
        dto.setFechaCrea(rol.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link RolDTO} a la entidad {@link Rol}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rol - {@link Rol}
	 * @param rolDTO - {@link RolDTO}
	 * @return {@link Rol}
	 */
    public static Rol getRolFromRolDTO(Rol rol, RolDTO rolDTO){
    
    	if (rolDTO == null) {
            return null;
        }
        
        if(rol == null){
        	rol = new Rol();
        }
        
        rol.setId(rolDTO.getId());
        
        rol.setNombre(rolDTO.getNombre());
        rol.setFechaModifica(rolDTO.getFechaModifica());
        rol.setEstado(EstadoEnum.valueOf(rolDTO.getEstado()));
        rol.setUsuarioModifica(rolDTO.getUsuarioModifica());
        rol.setDescripcion(rolDTO.getDescripcion());
        rol.setUsuarioCrea(rolDTO.getUsuarioCrea());
        rol.setFechaCrea(rolDTO.getFechaCrea());
    
    	return rol;
    
    }

    /**
	 * Transforma la entidad {@link BdNacional} al DTO {@link BdNacionalDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param bdNacional - {@link BdNacional}
	 * @return {@link BdNacionalDTO}
	 */
    public static BdNacionalDTO getBdNacionalDTOFromBdNacional(BdNacional bdNacional){
    
    	if (bdNacional == null) {
            return null;
        }
        
        BdNacionalDTO dto = new BdNacionalDTO();

        dto.setId(bdNacional.getId());
        
        dto.setFechaModifica(bdNacional.getFechaModifica());
        dto.setEstado(bdNacional.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(bdNacional.getEstado()));
        dto.setValor(bdNacional.getValor());
        dto.setUsuarioModifica(bdNacional.getUsuarioModifica());
        dto.setUsuarioCrea(bdNacional.getUsuarioCrea());
        dto.setFechaCrea(bdNacional.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link BdNacionalDTO} a la entidad {@link BdNacional}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param bdNacional - {@link BdNacional}
	 * @param bdNacionalDTO - {@link BdNacionalDTO}
	 * @return {@link BdNacional}
	 */
    public static BdNacional getBdNacionalFromBdNacionalDTO(BdNacional bdNacional, BdNacionalDTO bdNacionalDTO){
    
    	if (bdNacionalDTO == null) {
            return null;
        }
        
        if(bdNacional == null){
        	bdNacional = new BdNacional();
        }
        
        bdNacional.setId(bdNacionalDTO.getId());
        
        bdNacional.setFechaModifica(bdNacionalDTO.getFechaModifica());
        bdNacional.setEstado(EstadoEnum.valueOf(bdNacionalDTO.getEstado()));
        bdNacional.setValor(bdNacionalDTO.getValor());
        bdNacional.setUsuarioModifica(bdNacionalDTO.getUsuarioModifica());
        bdNacional.setUsuarioCrea(bdNacionalDTO.getUsuarioCrea());
        bdNacional.setFechaCrea(bdNacionalDTO.getFechaCrea());
    
    	return bdNacional;
    
    }

    /**
	 * Transforma la entidad {@link TipoTenencia} al DTO {@link TipoTenenciaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTenencia - {@link TipoTenencia}
	 * @return {@link TipoTenenciaDTO}
	 */
    public static TipoTenenciaDTO getTipoTenenciaDTOFromTipoTenencia(TipoTenencia tipoTenencia){
    
    	if (tipoTenencia == null) {
            return null;
        }
        
        TipoTenenciaDTO dto = new TipoTenenciaDTO();

        dto.setId(tipoTenencia.getId());
        
        dto.setFechaModifica(tipoTenencia.getFechaModifica());
        dto.setEstado(tipoTenencia.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(tipoTenencia.getEstado()));
        dto.setValor(tipoTenencia.getValor());
        dto.setUsuarioModifica(tipoTenencia.getUsuarioModifica());
        dto.setUsuarioCrea(tipoTenencia.getUsuarioCrea());
        dto.setFechaCrea(tipoTenencia.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TipoTenenciaDTO} a la entidad {@link TipoTenencia}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTenencia - {@link TipoTenencia}
	 * @param tipoTenenciaDTO - {@link TipoTenenciaDTO}
	 * @return {@link TipoTenencia}
	 */
    public static TipoTenencia getTipoTenenciaFromTipoTenenciaDTO(TipoTenencia tipoTenencia, TipoTenenciaDTO tipoTenenciaDTO){
    
    	if (tipoTenenciaDTO == null) {
            return null;
        }
        
        if(tipoTenencia == null){
        	tipoTenencia = new TipoTenencia();
        }
        
        tipoTenencia.setId(tipoTenenciaDTO.getId());
        
        tipoTenencia.setFechaModifica(tipoTenenciaDTO.getFechaModifica());
        tipoTenencia.setEstado(EstadoEnum.valueOf(tipoTenenciaDTO.getEstado()));
        tipoTenencia.setValor(tipoTenenciaDTO.getValor());
        tipoTenencia.setUsuarioModifica(tipoTenenciaDTO.getUsuarioModifica());
        tipoTenencia.setUsuarioCrea(tipoTenenciaDTO.getUsuarioCrea());
        tipoTenencia.setFechaCrea(tipoTenenciaDTO.getFechaCrea());
    
    	return tipoTenencia;
    
    }

    /**
	 * Transforma la entidad {@link Municipio} al DTO {@link MunicipioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param municipio - {@link Municipio}
	 * @return {@link MunicipioDTO}
	 */
    public static MunicipioDTO getMunicipioDTOFromMunicipio(Municipio municipio){
    
    	if (municipio == null) {
            return null;
        }
        
        MunicipioDTO dto = new MunicipioDTO();

        dto.setId(municipio.getId());
        
        dto.setFechaModifica(municipio.getFechaModifica());
        dto.setEstado(municipio.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(municipio.getEstado()));
        dto.setValor(municipio.getValor());
        dto.setUsuarioModifica(municipio.getUsuarioModifica());
        dto.setUsuarioCrea(municipio.getUsuarioCrea());
        dto.setFechaCrea(municipio.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link MunicipioDTO} a la entidad {@link Municipio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param municipio - {@link Municipio}
	 * @param municipioDTO - {@link MunicipioDTO}
	 * @return {@link Municipio}
	 */
    public static Municipio getMunicipioFromMunicipioDTO(Municipio municipio, MunicipioDTO municipioDTO){
    
    	if (municipioDTO == null) {
            return null;
        }
        
        if(municipio == null){
        	municipio = new Municipio();
        }
        
        municipio.setId(municipioDTO.getId());
        
        municipio.setFechaModifica(municipioDTO.getFechaModifica());
        municipio.setEstado(EstadoEnum.valueOf(municipioDTO.getEstado()));
        municipio.setValor(municipioDTO.getValor());
        municipio.setUsuarioModifica(municipioDTO.getUsuarioModifica());
        municipio.setUsuarioCrea(municipioDTO.getUsuarioCrea());
        municipio.setFechaCrea(municipioDTO.getFechaCrea());
    
    	return municipio;
    
    }

    /**
	 * Transforma la entidad {@link Componente} al DTO {@link ComponenteDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componente - {@link Componente}
	 * @return {@link ComponenteDTO}
	 */
    public static ComponenteDTO getComponenteDTOFromComponente(Componente componente){
    
    	if (componente == null) {
            return null;
        }
        
        ComponenteDTO dto = new ComponenteDTO();

        dto.setId(componente.getId());
        
        dto.setNombre(componente.getNombre());
        dto.setFechaModifica(componente.getFechaModifica());
        dto.setEstado(componente.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(componente.getEstado()));
        dto.setUsuarioModifica(componente.getUsuarioModifica());
        dto.setInfoActividades(componente.getInfoActividades());
        dto.setResultadosEsperados(componente.getResultadosEsperados());
        dto.setUsuarioCrea(componente.getUsuarioCrea());
        dto.setFechaCrea(componente.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ComponenteDTO} a la entidad {@link Componente}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componente - {@link Componente}
	 * @param componenteDTO - {@link ComponenteDTO}
	 * @return {@link Componente}
	 */
    public static Componente getComponenteFromComponenteDTO(Componente componente, ComponenteDTO componenteDTO){
    
    	if (componenteDTO == null) {
            return null;
        }
        
        if(componente == null){
        	componente = new Componente();
        }
        
        componente.setId(componenteDTO.getId());
        
        componente.setNombre(componenteDTO.getNombre());
        componente.setFechaModifica(componenteDTO.getFechaModifica());
        componente.setEstado(EstadoEnum.valueOf(componenteDTO.getEstado()));
        componente.setUsuarioModifica(componenteDTO.getUsuarioModifica());
        componente.setInfoActividades(componenteDTO.getInfoActividades());
        componente.setResultadosEsperados(componenteDTO.getResultadosEsperados());
        componente.setUsuarioCrea(componenteDTO.getUsuarioCrea());
        componente.setFechaCrea(componenteDTO.getFechaCrea());
    
    	return componente;
    
    }

    /**
	 * Transforma la entidad {@link NivelEducativo} al DTO {@link NivelEducativoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param nivelEducativo - {@link NivelEducativo}
	 * @return {@link NivelEducativoDTO}
	 */
    public static NivelEducativoDTO getNivelEducativoDTOFromNivelEducativo(NivelEducativo nivelEducativo){
    
    	if (nivelEducativo == null) {
            return null;
        }
        
        NivelEducativoDTO dto = new NivelEducativoDTO();

        dto.setId(nivelEducativo.getId());
        
        dto.setFechaModifica(nivelEducativo.getFechaModifica());
        dto.setEstado(nivelEducativo.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(nivelEducativo.getEstado()));
        dto.setValor(nivelEducativo.getValor());
        dto.setUsuarioModifica(nivelEducativo.getUsuarioModifica());
        dto.setUsuarioCrea(nivelEducativo.getUsuarioCrea());
        dto.setFechaCrea(nivelEducativo.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link NivelEducativoDTO} a la entidad {@link NivelEducativo}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param nivelEducativo - {@link NivelEducativo}
	 * @param nivelEducativoDTO - {@link NivelEducativoDTO}
	 * @return {@link NivelEducativo}
	 */
    public static NivelEducativo getNivelEducativoFromNivelEducativoDTO(NivelEducativo nivelEducativo, NivelEducativoDTO nivelEducativoDTO){
    
    	if (nivelEducativoDTO == null) {
            return null;
        }
        
        if(nivelEducativo == null){
        	nivelEducativo = new NivelEducativo();
        }
        
        nivelEducativo.setId(nivelEducativoDTO.getId());
        
        nivelEducativo.setFechaModifica(nivelEducativoDTO.getFechaModifica());
        nivelEducativo.setEstado(EstadoEnum.valueOf(nivelEducativoDTO.getEstado()));
        nivelEducativo.setValor(nivelEducativoDTO.getValor());
        nivelEducativo.setUsuarioModifica(nivelEducativoDTO.getUsuarioModifica());
        nivelEducativo.setUsuarioCrea(nivelEducativoDTO.getUsuarioCrea());
        nivelEducativo.setFechaCrea(nivelEducativoDTO.getFechaCrea());
    
    	return nivelEducativo;
    
    }

    /**
	 * Transforma la entidad {@link DescIndependiente} al DTO {@link DescIndependienteDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param descIndependiente - {@link DescIndependiente}
	 * @return {@link DescIndependienteDTO}
	 */
    public static DescIndependienteDTO getDescIndependienteDTOFromDescIndependiente(DescIndependiente descIndependiente){
    
    	if (descIndependiente == null) {
            return null;
        }
        
        DescIndependienteDTO dto = new DescIndependienteDTO();

        dto.setId(descIndependiente.getId());
        
        dto.setFechaModifica(descIndependiente.getFechaModifica());
        dto.setEstado(descIndependiente.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(descIndependiente.getEstado()));
        dto.setValor(descIndependiente.getValor());
        dto.setUsuarioModifica(descIndependiente.getUsuarioModifica());
        dto.setUsuarioCrea(descIndependiente.getUsuarioCrea());
        dto.setFechaCrea(descIndependiente.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link DescIndependienteDTO} a la entidad {@link DescIndependiente}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param descIndependiente - {@link DescIndependiente}
	 * @param descIndependienteDTO - {@link DescIndependienteDTO}
	 * @return {@link DescIndependiente}
	 */
    public static DescIndependiente getDescIndependienteFromDescIndependienteDTO(DescIndependiente descIndependiente, DescIndependienteDTO descIndependienteDTO){
    
    	if (descIndependienteDTO == null) {
            return null;
        }
        
        if(descIndependiente == null){
        	descIndependiente = new DescIndependiente();
        }
        
        descIndependiente.setId(descIndependienteDTO.getId());
        
        descIndependiente.setFechaModifica(descIndependienteDTO.getFechaModifica());
        descIndependiente.setEstado(EstadoEnum.valueOf(descIndependienteDTO.getEstado()));
        descIndependiente.setValor(descIndependienteDTO.getValor());
        descIndependiente.setUsuarioModifica(descIndependienteDTO.getUsuarioModifica());
        descIndependiente.setUsuarioCrea(descIndependienteDTO.getUsuarioCrea());
        descIndependiente.setFechaCrea(descIndependienteDTO.getFechaCrea());
    
    	return descIndependiente;
    
    }

    /**
	 * Transforma la entidad {@link Parentesco} al DTO {@link ParentescoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parentesco - {@link Parentesco}
	 * @return {@link ParentescoDTO}
	 */
    public static ParentescoDTO getParentescoDTOFromParentesco(Parentesco parentesco){
    
    	if (parentesco == null) {
            return null;
        }
        
        ParentescoDTO dto = new ParentescoDTO();

        dto.setId(parentesco.getId());
        
        dto.setFechaModifica(parentesco.getFechaModifica());
        dto.setEstado(parentesco.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(parentesco.getEstado()));
        dto.setValor(parentesco.getValor());
        dto.setUsuarioModifica(parentesco.getUsuarioModifica());
        dto.setUsuarioCrea(parentesco.getUsuarioCrea());
        dto.setFechaCrea(parentesco.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ParentescoDTO} a la entidad {@link Parentesco}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param parentesco - {@link Parentesco}
	 * @param parentescoDTO - {@link ParentescoDTO}
	 * @return {@link Parentesco}
	 */
    public static Parentesco getParentescoFromParentescoDTO(Parentesco parentesco, ParentescoDTO parentescoDTO){
    
    	if (parentescoDTO == null) {
            return null;
        }
        
        if(parentesco == null){
        	parentesco = new Parentesco();
        }
        
        parentesco.setId(parentescoDTO.getId());
        
        parentesco.setFechaModifica(parentescoDTO.getFechaModifica());
        parentesco.setEstado(EstadoEnum.valueOf(parentescoDTO.getEstado()));
        parentesco.setValor(parentescoDTO.getValor());
        parentesco.setUsuarioModifica(parentescoDTO.getUsuarioModifica());
        parentesco.setUsuarioCrea(parentescoDTO.getUsuarioCrea());
        parentesco.setFechaCrea(parentescoDTO.getFechaCrea());
    
    	return parentesco;
    
    }

    /**
	 * Transforma la entidad {@link EstadoCivil} al DTO {@link EstadoCivilDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoCivil - {@link EstadoCivil}
	 * @return {@link EstadoCivilDTO}
	 */
    public static EstadoCivilDTO getEstadoCivilDTOFromEstadoCivil(EstadoCivil estadoCivil){
    
    	if (estadoCivil == null) {
            return null;
        }
        
        EstadoCivilDTO dto = new EstadoCivilDTO();

        dto.setId(estadoCivil.getId());
        
        dto.setFechaModifica(estadoCivil.getFechaModifica());
        dto.setEstado(estadoCivil.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(estadoCivil.getEstado()));
        dto.setValor(estadoCivil.getValor());
        dto.setUsuarioModifica(estadoCivil.getUsuarioModifica());
        dto.setUsuarioCrea(estadoCivil.getUsuarioCrea());
        dto.setFechaCrea(estadoCivil.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EstadoCivilDTO} a la entidad {@link EstadoCivil}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estadoCivil - {@link EstadoCivil}
	 * @param estadoCivilDTO - {@link EstadoCivilDTO}
	 * @return {@link EstadoCivil}
	 */
    public static EstadoCivil getEstadoCivilFromEstadoCivilDTO(EstadoCivil estadoCivil, EstadoCivilDTO estadoCivilDTO){
    
    	if (estadoCivilDTO == null) {
            return null;
        }
        
        if(estadoCivil == null){
        	estadoCivil = new EstadoCivil();
        }
        
        estadoCivil.setId(estadoCivilDTO.getId());
        
        estadoCivil.setFechaModifica(estadoCivilDTO.getFechaModifica());
        estadoCivil.setEstado(EstadoEnum.valueOf(estadoCivilDTO.getEstado()));
        estadoCivil.setValor(estadoCivilDTO.getValor());
        estadoCivil.setUsuarioModifica(estadoCivilDTO.getUsuarioModifica());
        estadoCivil.setUsuarioCrea(estadoCivilDTO.getUsuarioCrea());
        estadoCivil.setFechaCrea(estadoCivilDTO.getFechaCrea());
    
    	return estadoCivil;
    
    }

    /**
	 * Transforma la entidad {@link Persona} al DTO {@link PersonaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param persona - {@link Persona}
	 * @return {@link PersonaDTO}
	 */
    public static PersonaDTO getPersonaDTOFromPersona(Persona persona){
    
    	if (persona == null) {
            return null;
        }
        
        PersonaDTO dto = new PersonaDTO();

        dto.setId(persona.getId());
        
        dto.setTipoId(persona.getTipoId().name());
        dto.setTipoIdDesc(PmzEnumUtils.obtenerEnumLabel(persona.getTipoId()));
        dto.setFechaModifica(persona.getFechaModifica());
        dto.setNumId(persona.getNumId());
        dto.setEstado(persona.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(persona.getEstado()));
        dto.setNombres(persona.getNombres());
        dto.setApellidos(persona.getApellidos());
        dto.setUsuarioModifica(persona.getUsuarioModifica());
        dto.setUsuarioCrea(persona.getUsuarioCrea());
        dto.setFechaCrea(persona.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link PersonaDTO} a la entidad {@link Persona}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param persona - {@link Persona}
	 * @param personaDTO - {@link PersonaDTO}
	 * @return {@link Persona}
	 */
    public static Persona getPersonaFromPersonaDTO(Persona persona, PersonaDTO personaDTO){
    
    	if (personaDTO == null) {
            return null;
        }
        
        if(persona == null){
        	persona = new Persona();
        }
        
        persona.setId(personaDTO.getId());
        
        persona.setTipoId(TipoIdEnum.valueOf(personaDTO.getTipoId()));
        persona.setFechaModifica(personaDTO.getFechaModifica());
        persona.setNumId(personaDTO.getNumId());
        persona.setEstado(EstadoEnum.valueOf(personaDTO.getEstado()));
        persona.setNombres(personaDTO.getNombres());
        persona.setApellidos(personaDTO.getApellidos());
        persona.setUsuarioModifica(personaDTO.getUsuarioModifica());
        persona.setUsuarioCrea(personaDTO.getUsuarioCrea());
        persona.setFechaCrea(personaDTO.getFechaCrea());
    
    	return persona;
    
    }

    /**
	 * Transforma la entidad {@link Ocupacion} al DTO {@link OcupacionDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param ocupacion - {@link Ocupacion}
	 * @return {@link OcupacionDTO}
	 */
    public static OcupacionDTO getOcupacionDTOFromOcupacion(Ocupacion ocupacion){
    
    	if (ocupacion == null) {
            return null;
        }
        
        OcupacionDTO dto = new OcupacionDTO();

        dto.setId(ocupacion.getId());
        
        dto.setFechaModifica(ocupacion.getFechaModifica());
        dto.setEstado(ocupacion.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(ocupacion.getEstado()));
        dto.setValor(ocupacion.getValor());
        dto.setUsuarioModifica(ocupacion.getUsuarioModifica());
        dto.setUsuarioCrea(ocupacion.getUsuarioCrea());
        dto.setFechaCrea(ocupacion.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link OcupacionDTO} a la entidad {@link Ocupacion}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param ocupacion - {@link Ocupacion}
	 * @param ocupacionDTO - {@link OcupacionDTO}
	 * @return {@link Ocupacion}
	 */
    public static Ocupacion getOcupacionFromOcupacionDTO(Ocupacion ocupacion, OcupacionDTO ocupacionDTO){
    
    	if (ocupacionDTO == null) {
            return null;
        }
        
        if(ocupacion == null){
        	ocupacion = new Ocupacion();
        }
        
        ocupacion.setId(ocupacionDTO.getId());
        
        ocupacion.setFechaModifica(ocupacionDTO.getFechaModifica());
        ocupacion.setEstado(EstadoEnum.valueOf(ocupacionDTO.getEstado()));
        ocupacion.setValor(ocupacionDTO.getValor());
        ocupacion.setUsuarioModifica(ocupacionDTO.getUsuarioModifica());
        ocupacion.setUsuarioCrea(ocupacionDTO.getUsuarioCrea());
        ocupacion.setFechaCrea(ocupacionDTO.getFechaCrea());
    
    	return ocupacion;
    
    }

    /**
	 * Transforma la entidad {@link MaterialVivienda} al DTO {@link MaterialViviendaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param materialVivienda - {@link MaterialVivienda}
	 * @return {@link MaterialViviendaDTO}
	 */
    public static MaterialViviendaDTO getMaterialViviendaDTOFromMaterialVivienda(MaterialVivienda materialVivienda){
    
    	if (materialVivienda == null) {
            return null;
        }
        
        MaterialViviendaDTO dto = new MaterialViviendaDTO();

        dto.setId(materialVivienda.getId());
        
        dto.setFechaModifica(materialVivienda.getFechaModifica());
        dto.setEstado(materialVivienda.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(materialVivienda.getEstado()));
        dto.setValor(materialVivienda.getValor());
        dto.setUsuarioModifica(materialVivienda.getUsuarioModifica());
        dto.setUsuarioCrea(materialVivienda.getUsuarioCrea());
        dto.setFechaCrea(materialVivienda.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link MaterialViviendaDTO} a la entidad {@link MaterialVivienda}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param materialVivienda - {@link MaterialVivienda}
	 * @param materialViviendaDTO - {@link MaterialViviendaDTO}
	 * @return {@link MaterialVivienda}
	 */
    public static MaterialVivienda getMaterialViviendaFromMaterialViviendaDTO(MaterialVivienda materialVivienda, MaterialViviendaDTO materialViviendaDTO){
    
    	if (materialViviendaDTO == null) {
            return null;
        }
        
        if(materialVivienda == null){
        	materialVivienda = new MaterialVivienda();
        }
        
        materialVivienda.setId(materialViviendaDTO.getId());
        
        materialVivienda.setFechaModifica(materialViviendaDTO.getFechaModifica());
        materialVivienda.setEstado(EstadoEnum.valueOf(materialViviendaDTO.getEstado()));
        materialVivienda.setValor(materialViviendaDTO.getValor());
        materialVivienda.setUsuarioModifica(materialViviendaDTO.getUsuarioModifica());
        materialVivienda.setUsuarioCrea(materialViviendaDTO.getUsuarioCrea());
        materialVivienda.setFechaCrea(materialViviendaDTO.getFechaCrea());
    
    	return materialVivienda;
    
    }

    /**
	 * Transforma la entidad {@link RolFundacion} al DTO {@link RolFundacionDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolFundacion - {@link RolFundacion}
	 * @return {@link RolFundacionDTO}
	 */
    public static RolFundacionDTO getRolFundacionDTOFromRolFundacion(RolFundacion rolFundacion){
    
    	if (rolFundacion == null) {
            return null;
        }
        
        RolFundacionDTO dto = new RolFundacionDTO();

        dto.setId(rolFundacion.getId());
        
        dto.setFechaModifica(rolFundacion.getFechaModifica());
        dto.setEstado(rolFundacion.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(rolFundacion.getEstado()));
        dto.setValor(rolFundacion.getValor());
        dto.setUsuarioModifica(rolFundacion.getUsuarioModifica());
        dto.setUsuarioCrea(rolFundacion.getUsuarioCrea());
        dto.setFechaCrea(rolFundacion.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link RolFundacionDTO} a la entidad {@link RolFundacion}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolFundacion - {@link RolFundacion}
	 * @param rolFundacionDTO - {@link RolFundacionDTO}
	 * @return {@link RolFundacion}
	 */
    public static RolFundacion getRolFundacionFromRolFundacionDTO(RolFundacion rolFundacion, RolFundacionDTO rolFundacionDTO){
    
    	if (rolFundacionDTO == null) {
            return null;
        }
        
        if(rolFundacion == null){
        	rolFundacion = new RolFundacion();
        }
        
        rolFundacion.setId(rolFundacionDTO.getId());
        
        rolFundacion.setFechaModifica(rolFundacionDTO.getFechaModifica());
        rolFundacion.setEstado(EstadoEnum.valueOf(rolFundacionDTO.getEstado()));
        rolFundacion.setValor(rolFundacionDTO.getValor());
        rolFundacion.setUsuarioModifica(rolFundacionDTO.getUsuarioModifica());
        rolFundacion.setUsuarioCrea(rolFundacionDTO.getUsuarioCrea());
        rolFundacion.setFechaCrea(rolFundacionDTO.getFechaCrea());
    
    	return rolFundacion;
    
    }

    /**
	 * Transforma la entidad {@link Cargo} al DTO {@link CargoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param cargo - {@link Cargo}
	 * @return {@link CargoDTO}
	 */
    public static CargoDTO getCargoDTOFromCargo(Cargo cargo){
    
    	if (cargo == null) {
            return null;
        }
        
        CargoDTO dto = new CargoDTO();

        dto.setId(cargo.getId());
        
        dto.setFechaModifica(cargo.getFechaModifica());
        dto.setEstado(cargo.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(cargo.getEstado()));
        dto.setValor(cargo.getValor());
        dto.setUsuarioModifica(cargo.getUsuarioModifica());
        dto.setUsuarioCrea(cargo.getUsuarioCrea());
        dto.setFechaCrea(cargo.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link CargoDTO} a la entidad {@link Cargo}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param cargo - {@link Cargo}
	 * @param cargoDTO - {@link CargoDTO}
	 * @return {@link Cargo}
	 */
    public static Cargo getCargoFromCargoDTO(Cargo cargo, CargoDTO cargoDTO){
    
    	if (cargoDTO == null) {
            return null;
        }
        
        if(cargo == null){
        	cargo = new Cargo();
        }
        
        cargo.setId(cargoDTO.getId());
        
        cargo.setFechaModifica(cargoDTO.getFechaModifica());
        cargo.setEstado(EstadoEnum.valueOf(cargoDTO.getEstado()));
        cargo.setValor(cargoDTO.getValor());
        cargo.setUsuarioModifica(cargoDTO.getUsuarioModifica());
        cargo.setUsuarioCrea(cargoDTO.getUsuarioCrea());
        cargo.setFechaCrea(cargoDTO.getFechaCrea());
    
    	return cargo;
    
    }

    /**
	 * Transforma la entidad {@link AfiliadoSalud} al DTO {@link AfiliadoSaludDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param afiliadoSalud - {@link AfiliadoSalud}
	 * @return {@link AfiliadoSaludDTO}
	 */
    public static AfiliadoSaludDTO getAfiliadoSaludDTOFromAfiliadoSalud(AfiliadoSalud afiliadoSalud){
    
    	if (afiliadoSalud == null) {
            return null;
        }
        
        AfiliadoSaludDTO dto = new AfiliadoSaludDTO();

        dto.setId(afiliadoSalud.getId());
        
        dto.setFechaModifica(afiliadoSalud.getFechaModifica());
        dto.setEstado(afiliadoSalud.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(afiliadoSalud.getEstado()));
        dto.setValor(afiliadoSalud.getValor());
        dto.setUsuarioModifica(afiliadoSalud.getUsuarioModifica());
        dto.setUsuarioCrea(afiliadoSalud.getUsuarioCrea());
        dto.setFechaCrea(afiliadoSalud.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link AfiliadoSaludDTO} a la entidad {@link AfiliadoSalud}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param afiliadoSalud - {@link AfiliadoSalud}
	 * @param afiliadoSaludDTO - {@link AfiliadoSaludDTO}
	 * @return {@link AfiliadoSalud}
	 */
    public static AfiliadoSalud getAfiliadoSaludFromAfiliadoSaludDTO(AfiliadoSalud afiliadoSalud, AfiliadoSaludDTO afiliadoSaludDTO){
    
    	if (afiliadoSaludDTO == null) {
            return null;
        }
        
        if(afiliadoSalud == null){
        	afiliadoSalud = new AfiliadoSalud();
        }
        
        afiliadoSalud.setId(afiliadoSaludDTO.getId());
        
        afiliadoSalud.setFechaModifica(afiliadoSaludDTO.getFechaModifica());
        afiliadoSalud.setEstado(EstadoEnum.valueOf(afiliadoSaludDTO.getEstado()));
        afiliadoSalud.setValor(afiliadoSaludDTO.getValor());
        afiliadoSalud.setUsuarioModifica(afiliadoSaludDTO.getUsuarioModifica());
        afiliadoSalud.setUsuarioCrea(afiliadoSaludDTO.getUsuarioCrea());
        afiliadoSalud.setFechaCrea(afiliadoSaludDTO.getFechaCrea());
    
    	return afiliadoSalud;
    
    }

    /**
	 * Transforma la entidad {@link EstructuraFamiliar} al DTO {@link EstructuraFamiliarDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estructuraFamiliar - {@link EstructuraFamiliar}
	 * @return {@link EstructuraFamiliarDTO}
	 */
    public static EstructuraFamiliarDTO getEstructuraFamiliarDTOFromEstructuraFamiliar(EstructuraFamiliar estructuraFamiliar){
    
    	if (estructuraFamiliar == null) {
            return null;
        }
        
        EstructuraFamiliarDTO dto = new EstructuraFamiliarDTO();

        dto.setId(estructuraFamiliar.getId());
        
        dto.setFechaModifica(estructuraFamiliar.getFechaModifica());
        dto.setEstado(estructuraFamiliar.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(estructuraFamiliar.getEstado()));
        dto.setValor(estructuraFamiliar.getValor());
        dto.setUsuarioModifica(estructuraFamiliar.getUsuarioModifica());
        dto.setUsuarioCrea(estructuraFamiliar.getUsuarioCrea());
        dto.setFechaCrea(estructuraFamiliar.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EstructuraFamiliarDTO} a la entidad {@link EstructuraFamiliar}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param estructuraFamiliar - {@link EstructuraFamiliar}
	 * @param estructuraFamiliarDTO - {@link EstructuraFamiliarDTO}
	 * @return {@link EstructuraFamiliar}
	 */
    public static EstructuraFamiliar getEstructuraFamiliarFromEstructuraFamiliarDTO(EstructuraFamiliar estructuraFamiliar, EstructuraFamiliarDTO estructuraFamiliarDTO){
    
    	if (estructuraFamiliarDTO == null) {
            return null;
        }
        
        if(estructuraFamiliar == null){
        	estructuraFamiliar = new EstructuraFamiliar();
        }
        
        estructuraFamiliar.setId(estructuraFamiliarDTO.getId());
        
        estructuraFamiliar.setFechaModifica(estructuraFamiliarDTO.getFechaModifica());
        estructuraFamiliar.setEstado(EstadoEnum.valueOf(estructuraFamiliarDTO.getEstado()));
        estructuraFamiliar.setValor(estructuraFamiliarDTO.getValor());
        estructuraFamiliar.setUsuarioModifica(estructuraFamiliarDTO.getUsuarioModifica());
        estructuraFamiliar.setUsuarioCrea(estructuraFamiliarDTO.getUsuarioCrea());
        estructuraFamiliar.setFechaCrea(estructuraFamiliarDTO.getFechaCrea());
    
    	return estructuraFamiliar;
    
    }

    /**
	 * Transforma la entidad {@link TipoContacto} al DTO {@link TipoContactoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoContacto - {@link TipoContacto}
	 * @return {@link TipoContactoDTO}
	 */
    public static TipoContactoDTO getTipoContactoDTOFromTipoContacto(TipoContacto tipoContacto){
    
    	if (tipoContacto == null) {
            return null;
        }
        
        TipoContactoDTO dto = new TipoContactoDTO();

        dto.setId(tipoContacto.getId());
        
        dto.setFechaModifica(tipoContacto.getFechaModifica());
        dto.setEstado(tipoContacto.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(tipoContacto.getEstado()));
        dto.setValor(tipoContacto.getValor());
        dto.setUsuarioModifica(tipoContacto.getUsuarioModifica());
        dto.setUsuarioCrea(tipoContacto.getUsuarioCrea());
        dto.setFechaCrea(tipoContacto.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TipoContactoDTO} a la entidad {@link TipoContacto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoContacto - {@link TipoContacto}
	 * @param tipoContactoDTO - {@link TipoContactoDTO}
	 * @return {@link TipoContacto}
	 */
    public static TipoContacto getTipoContactoFromTipoContactoDTO(TipoContacto tipoContacto, TipoContactoDTO tipoContactoDTO){
    
    	if (tipoContactoDTO == null) {
            return null;
        }
        
        if(tipoContacto == null){
        	tipoContacto = new TipoContacto();
        }
        
        tipoContacto.setId(tipoContactoDTO.getId());
        
        tipoContacto.setFechaModifica(tipoContactoDTO.getFechaModifica());
        tipoContacto.setEstado(EstadoEnum.valueOf(tipoContactoDTO.getEstado()));
        tipoContacto.setValor(tipoContactoDTO.getValor());
        tipoContacto.setUsuarioModifica(tipoContactoDTO.getUsuarioModifica());
        tipoContacto.setUsuarioCrea(tipoContactoDTO.getUsuarioCrea());
        tipoContacto.setFechaCrea(tipoContactoDTO.getFechaCrea());
    
    	return tipoContacto;
    
    }

    /**
	 * Transforma la entidad {@link TipoAliado} al DTO {@link TipoAliadoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoAliado - {@link TipoAliado}
	 * @return {@link TipoAliadoDTO}
	 */
    public static TipoAliadoDTO getTipoAliadoDTOFromTipoAliado(TipoAliado tipoAliado){
    
    	if (tipoAliado == null) {
            return null;
        }
        
        TipoAliadoDTO dto = new TipoAliadoDTO();

        dto.setId(tipoAliado.getId());
        
        dto.setFechaModifica(tipoAliado.getFechaModifica());
        dto.setEstado(tipoAliado.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(tipoAliado.getEstado()));
        dto.setValor(tipoAliado.getValor());
        dto.setUsuarioModifica(tipoAliado.getUsuarioModifica());
        dto.setUsuarioCrea(tipoAliado.getUsuarioCrea());
        dto.setFechaCrea(tipoAliado.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TipoAliadoDTO} a la entidad {@link TipoAliado}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoAliado - {@link TipoAliado}
	 * @param tipoAliadoDTO - {@link TipoAliadoDTO}
	 * @return {@link TipoAliado}
	 */
    public static TipoAliado getTipoAliadoFromTipoAliadoDTO(TipoAliado tipoAliado, TipoAliadoDTO tipoAliadoDTO){
    
    	if (tipoAliadoDTO == null) {
            return null;
        }
        
        if(tipoAliado == null){
        	tipoAliado = new TipoAliado();
        }
        
        tipoAliado.setId(tipoAliadoDTO.getId());
        
        tipoAliado.setFechaModifica(tipoAliadoDTO.getFechaModifica());
        tipoAliado.setEstado(EstadoEnum.valueOf(tipoAliadoDTO.getEstado()));
        tipoAliado.setValor(tipoAliadoDTO.getValor());
        tipoAliado.setUsuarioModifica(tipoAliadoDTO.getUsuarioModifica());
        tipoAliado.setUsuarioCrea(tipoAliadoDTO.getUsuarioCrea());
        tipoAliado.setFechaCrea(tipoAliadoDTO.getFechaCrea());
    
    	return tipoAliado;
    
    }

    /**
	 * Transforma la entidad {@link Usuario} al DTO {@link UsuarioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param usuario - {@link Usuario}
	 * @return {@link UsuarioDTO}
	 */
    public static UsuarioDTO getUsuarioDTOFromUsuario(Usuario usuario){
    
    	if (usuario == null) {
            return null;
        }
        
        UsuarioDTO dto = new UsuarioDTO();

        dto.setId(usuario.getId());
        
        dto.setFechaModifica(usuario.getFechaModifica());
        dto.setEstado(usuario.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(usuario.getEstado()));
        dto.setNombreUsuario(usuario.getNombreUsuario());
        dto.setUsuarioModifica(usuario.getUsuarioModifica());
        dto.setPersonaDTO(getPersonaDTOFromPersona(usuario.getPersona()));
        dto.setUsuarioCrea(usuario.getUsuarioCrea());
        dto.setFechaCrea(usuario.getFechaCrea());
        dto.setCargoDTO(getCargoDTOFromCargo(usuario.getCargo()));
        dto.setContrasena(usuario.getContrasena());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link UsuarioDTO} a la entidad {@link Usuario}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param usuario - {@link Usuario}
	 * @param usuarioDTO - {@link UsuarioDTO}
	 * @return {@link Usuario}
	 */
    public static Usuario getUsuarioFromUsuarioDTO(Usuario usuario, UsuarioDTO usuarioDTO){
    
    	if (usuarioDTO == null) {
            return null;
        }
        
        if(usuario == null){
        	usuario = new Usuario();
        }
        
        usuario.setId(usuarioDTO.getId());
        
        usuario.setFechaModifica(usuarioDTO.getFechaModifica());
        usuario.setEstado(EstadoEnum.valueOf(usuarioDTO.getEstado()));
        usuario.setNombreUsuario(usuarioDTO.getNombreUsuario());
        usuario.setUsuarioModifica(usuarioDTO.getUsuarioModifica());
        usuario.setUsuarioCrea(usuarioDTO.getUsuarioCrea());
        usuario.setFechaCrea(usuarioDTO.getFechaCrea());
        usuario.setContrasena(usuarioDTO.getContrasena());
    
    	return usuario;
    
    }

    /**
	 * Transforma la entidad {@link Lider} al DTO {@link LiderDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param lider - {@link Lider}
	 * @return {@link LiderDTO}
	 */
    public static LiderDTO getLiderDTOFromLider(Lider lider){
    
    	if (lider == null) {
            return null;
        }
        
        LiderDTO dto = new LiderDTO();

        dto.setId(lider.getId());
        
        dto.setFechaModifica(lider.getFechaModifica());
        dto.setEstado(lider.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(lider.getEstado()));
        dto.setUsuarioModifica(lider.getUsuarioModifica());
        dto.setPersonaDTO(getPersonaDTOFromPersona(lider.getPersona()));
        dto.setUsuarioCrea(lider.getUsuarioCrea());
        dto.setFechaCrea(lider.getFechaCrea());
        dto.setOrganizacionComun(lider.getOrganizacionComun());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link LiderDTO} a la entidad {@link Lider}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param lider - {@link Lider}
	 * @param liderDTO - {@link LiderDTO}
	 * @return {@link Lider}
	 */
    public static Lider getLiderFromLiderDTO(Lider lider, LiderDTO liderDTO){
    
    	if (liderDTO == null) {
            return null;
        }
        
        if(lider == null){
        	lider = new Lider();
        }
        
        lider.setId(liderDTO.getId());
        
        lider.setFechaModifica(liderDTO.getFechaModifica());
        lider.setEstado(EstadoEnum.valueOf(liderDTO.getEstado()));
        lider.setUsuarioModifica(liderDTO.getUsuarioModifica());
        lider.setUsuarioCrea(liderDTO.getUsuarioCrea());
        lider.setFechaCrea(liderDTO.getFechaCrea());
        lider.setOrganizacionComun(liderDTO.getOrganizacionComun());
    
    	return lider;
    
    }

    /**
	 * Transforma la entidad {@link TipoTerritorio} al DTO {@link TipoTerritorioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTerritorio - {@link TipoTerritorio}
	 * @return {@link TipoTerritorioDTO}
	 */
    public static TipoTerritorioDTO getTipoTerritorioDTOFromTipoTerritorio(TipoTerritorio tipoTerritorio){
    
    	if (tipoTerritorio == null) {
            return null;
        }
        
        TipoTerritorioDTO dto = new TipoTerritorioDTO();

        dto.setId(tipoTerritorio.getId());
        
        dto.setFechaModifica(tipoTerritorio.getFechaModifica());
        dto.setEstado(tipoTerritorio.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(tipoTerritorio.getEstado()));
        dto.setValor(tipoTerritorio.getValor());
        dto.setUsuarioModifica(tipoTerritorio.getUsuarioModifica());
        dto.setUsuarioCrea(tipoTerritorio.getUsuarioCrea());
        dto.setZonaDTO(getZonaDTOFromZona(tipoTerritorio.getZona()));
        dto.setFechaCrea(tipoTerritorio.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TipoTerritorioDTO} a la entidad {@link TipoTerritorio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param tipoTerritorio - {@link TipoTerritorio}
	 * @param tipoTerritorioDTO - {@link TipoTerritorioDTO}
	 * @return {@link TipoTerritorio}
	 */
    public static TipoTerritorio getTipoTerritorioFromTipoTerritorioDTO(TipoTerritorio tipoTerritorio, TipoTerritorioDTO tipoTerritorioDTO){
    
    	if (tipoTerritorioDTO == null) {
            return null;
        }
        
        if(tipoTerritorio == null){
        	tipoTerritorio = new TipoTerritorio();
        }
        
        tipoTerritorio.setId(tipoTerritorioDTO.getId());
        
        tipoTerritorio.setFechaModifica(tipoTerritorioDTO.getFechaModifica());
        tipoTerritorio.setEstado(EstadoEnum.valueOf(tipoTerritorioDTO.getEstado()));
        tipoTerritorio.setValor(tipoTerritorioDTO.getValor());
        tipoTerritorio.setUsuarioModifica(tipoTerritorioDTO.getUsuarioModifica());
        tipoTerritorio.setUsuarioCrea(tipoTerritorioDTO.getUsuarioCrea());
        tipoTerritorio.setFechaCrea(tipoTerritorioDTO.getFechaCrea());
    
    	return tipoTerritorio;
    
    }

    /**
	 * Transforma la entidad {@link Entidad} al DTO {@link EntidadDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidad - {@link Entidad}
	 * @return {@link EntidadDTO}
	 */
    public static EntidadDTO getEntidadDTOFromEntidad(Entidad entidad){
    
    	if (entidad == null) {
            return null;
        }
        
        EntidadDTO dto = new EntidadDTO();

        dto.setId(entidad.getId());
        
        dto.setTipoId(entidad.getTipoId().name());
        dto.setTipoIdDesc(PmzEnumUtils.obtenerEnumLabel(entidad.getTipoId()));
        dto.setFechaModifica(entidad.getFechaModifica());
        dto.setContactoDTO(getPersonaDTOFromPersona(entidad.getContacto()));
        dto.setContactoDTO(getPersonaDTOFromPersona(entidad.getContacto()));
        dto.setFechaConstitucion(entidad.getFechaConstitucion());
        dto.setInfoConvocatorias(entidad.getInfoConvocatorias());
        dto.setNombre(entidad.getNombre());
        dto.setNumId(entidad.getNumId());
        dto.setEstado(entidad.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(entidad.getEstado()));
        dto.setCertificaciones(entidad.getCertificaciones());
        dto.setUsuarioModifica(entidad.getUsuarioModifica());
        dto.setUsuarioCrea(entidad.getUsuarioCrea());
        dto.setFechaCrea(entidad.getFechaCrea());
        dto.setTipoEntidad(entidad.getTipoEntidad());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EntidadDTO} a la entidad {@link Entidad}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidad - {@link Entidad}
	 * @param entidadDTO - {@link EntidadDTO}
	 * @return {@link Entidad}
	 */
    public static Entidad getEntidadFromEntidadDTO(Entidad entidad, EntidadDTO entidadDTO){
    
    	if (entidadDTO == null) {
            return null;
        }
        
        if(entidad == null){
        	entidad = new Entidad();
        }
        
        entidad.setId(entidadDTO.getId());
        
        entidad.setTipoId(TipoIdEnum.valueOf(entidadDTO.getTipoId()));
        entidad.setFechaModifica(entidadDTO.getFechaModifica());
        entidad.setFechaConstitucion(entidadDTO.getFechaConstitucion());
        entidad.setInfoConvocatorias(entidadDTO.getInfoConvocatorias());
        entidad.setNombre(entidadDTO.getNombre());
        entidad.setNumId(entidadDTO.getNumId());
        entidad.setEstado(EstadoEnum.valueOf(entidadDTO.getEstado()));
        entidad.setCertificaciones(entidadDTO.getCertificaciones());
        entidad.setUsuarioModifica(entidadDTO.getUsuarioModifica());
        entidad.setUsuarioCrea(entidadDTO.getUsuarioCrea());
        entidad.setFechaCrea(entidadDTO.getFechaCrea());
        entidad.setTipoEntidad(entidadDTO.getTipoEntidad());
    
    	return entidad;
    
    }

    /**
	 * Transforma la entidad {@link Beneficiario} al DTO {@link BeneficiarioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link Beneficiario}
	 * @return {@link BeneficiarioDTO}
	 */
    public static BeneficiarioDTO getBeneficiarioDTOFromBeneficiario(Beneficiario beneficiario){
    
    	if (beneficiario == null) {
            return null;
        }
        
        BeneficiarioDTO dto = new BeneficiarioDTO();

        dto.setId(beneficiario.getId());
        
        dto.setFechaModifica(beneficiario.getFechaModifica());
        dto.setAreaEducacionDTO(getAreaEducacionDTOFromAreaEducacion(beneficiario.getAreaEducacion()));
        dto.setEstructuraFamiliarDTO(getEstructuraFamiliarDTOFromEstructuraFamiliar(beneficiario.getEstructuraFamiliar()));
        dto.setEstratoSocioeconomico(beneficiario.getEstratoSocioeconomico().name());
        dto.setEstratoSocioeconomicoDesc(PmzEnumUtils.obtenerEnumLabel(beneficiario.getEstratoSocioeconomico()));
        dto.setEstadoCivilDTO(getEstadoCivilDTOFromEstadoCivil(beneficiario.getEstadoCivil()));
        dto.setDescIndependienteDTO(getDescIndependienteDTOFromDescIndependiente(beneficiario.getDescIndependiente()));
        dto.setNivelEducativoDTO(getNivelEducativoDTOFromNivelEducativo(beneficiario.getNivelEducativo()));
        dto.setOtraAreaEducacion(beneficiario.getOtraAreaEducacion());
        dto.setTipoViviendaDTO(getTipoViviendaDTOFromTipoVivienda(beneficiario.getTipoVivienda()));
        dto.setCantPersonasHogar(beneficiario.getCantPersonasHogar());
        dto.setOrganizacionComunitaria(beneficiario.getOrganizacionComunitaria());
        dto.setNivelIngresos(beneficiario.getNivelIngresos());
        dto.setCantPersonasCargo(beneficiario.getCantPersonasCargo());
        dto.setUsuarioCrea(beneficiario.getUsuarioCrea());
        dto.setTipoTenenciaDTO(getTipoTenenciaDTOFromTipoTenencia(beneficiario.getTipoTenencia()));
        dto.setZonaDTO(getZonaDTOFromZona(beneficiario.getZona()));
        dto.setMaterialViviendaDTO(getMaterialViviendaDTOFromMaterialVivienda(beneficiario.getMaterialVivienda()));
        dto.setBdNacionalDTO(getBdNacionalDTOFromBdNacional(beneficiario.getBdNacional()));
        dto.setAfiliadoSaludDTO(getAfiliadoSaludDTOFromAfiliadoSalud(beneficiario.getAfiliadoSalud()));
        dto.setPersonaDTO(getPersonaDTOFromPersona(beneficiario.getPersona()));
        dto.setGrupoEtnicoDTO(getGrupoEtnicoDTOFromGrupoEtnico(beneficiario.getGrupoEtnico()));
        dto.setOcupacionDTO(getOcupacionDTOFromOcupacion(beneficiario.getOcupacion()));
        dto.setGenero(beneficiario.getGenero());
        dto.setEstado(beneficiario.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(beneficiario.getEstado()));
        dto.setUsuarioModifica(beneficiario.getUsuarioModifica());
        dto.setFechaCrea(beneficiario.getFechaCrea());
        dto.setMunicipioDTO(getMunicipioDTOFromMunicipio(beneficiario.getMunicipio()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link BeneficiarioDTO} a la entidad {@link Beneficiario}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiario - {@link Beneficiario}
	 * @param beneficiarioDTO - {@link BeneficiarioDTO}
	 * @return {@link Beneficiario}
	 */
    public static Beneficiario getBeneficiarioFromBeneficiarioDTO(Beneficiario beneficiario, BeneficiarioDTO beneficiarioDTO){
    
    	if (beneficiarioDTO == null) {
            return null;
        }
        
        if(beneficiario == null){
        	beneficiario = new Beneficiario();
        }
        
        beneficiario.setId(beneficiarioDTO.getId());
        
        beneficiario.setFechaModifica(beneficiarioDTO.getFechaModifica());
        beneficiario.setEstratoSocioeconomico(EstratoSocioeconomicoEnum.valueOf(beneficiarioDTO.getEstratoSocioeconomico()));
        beneficiario.setOtraAreaEducacion(beneficiarioDTO.getOtraAreaEducacion());
        beneficiario.setCantPersonasHogar(beneficiarioDTO.getCantPersonasHogar());
        beneficiario.setOrganizacionComunitaria(beneficiarioDTO.getOrganizacionComunitaria());
        beneficiario.setNivelIngresos(beneficiarioDTO.getNivelIngresos());
        beneficiario.setCantPersonasCargo(beneficiarioDTO.getCantPersonasCargo());
        beneficiario.setUsuarioCrea(beneficiarioDTO.getUsuarioCrea());
        beneficiario.setGenero(beneficiarioDTO.getGenero());
        beneficiario.setEstado(EstadoEnum.valueOf(beneficiarioDTO.getEstado()));
        beneficiario.setUsuarioModifica(beneficiarioDTO.getUsuarioModifica());
        beneficiario.setFechaCrea(beneficiarioDTO.getFechaCrea());
    
    	return beneficiario;
    
    }

    /**
	 * Transforma la entidad {@link DatoContacto} al DTO {@link DatoContactoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoContacto - {@link DatoContacto}
	 * @return {@link DatoContactoDTO}
	 */
    public static DatoContactoDTO getDatoContactoDTOFromDatoContacto(DatoContacto datoContacto){
    
    	if (datoContacto == null) {
            return null;
        }
        
        DatoContactoDTO dto = new DatoContactoDTO();

        dto.setId(datoContacto.getId());
        
        dto.setFechaModifica(datoContacto.getFechaModifica());
        dto.setEstado(datoContacto.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(datoContacto.getEstado()));
        dto.setValor(datoContacto.getValor());
        dto.setUsuarioModifica(datoContacto.getUsuarioModifica());
        dto.setPersonaDTO(getPersonaDTOFromPersona(datoContacto.getPersona()));
        dto.setUsuarioCrea(datoContacto.getUsuarioCrea());
        dto.setFechaCrea(datoContacto.getFechaCrea());
        dto.setTipoContactoDTO(getTipoContactoDTOFromTipoContacto(datoContacto.getTipoContacto()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link DatoContactoDTO} a la entidad {@link DatoContacto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoContacto - {@link DatoContacto}
	 * @param datoContactoDTO - {@link DatoContactoDTO}
	 * @return {@link DatoContacto}
	 */
    public static DatoContacto getDatoContactoFromDatoContactoDTO(DatoContacto datoContacto, DatoContactoDTO datoContactoDTO){
    
    	if (datoContactoDTO == null) {
            return null;
        }
        
        if(datoContacto == null){
        	datoContacto = new DatoContacto();
        }
        
        datoContacto.setId(datoContactoDTO.getId());
        
        datoContacto.setFechaModifica(datoContactoDTO.getFechaModifica());
        datoContacto.setEstado(EstadoEnum.valueOf(datoContactoDTO.getEstado()));
        datoContacto.setValor(datoContactoDTO.getValor());
        datoContacto.setUsuarioModifica(datoContactoDTO.getUsuarioModifica());
        datoContacto.setUsuarioCrea(datoContactoDTO.getUsuarioCrea());
        datoContacto.setFechaCrea(datoContactoDTO.getFechaCrea());
    
    	return datoContacto;
    
    }

    /**
	 * Transforma la entidad {@link Pariente} al DTO {@link ParienteDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param pariente - {@link Pariente}
	 * @return {@link ParienteDTO}
	 */
    public static ParienteDTO getParienteDTOFromPariente(Pariente pariente){
    
    	if (pariente == null) {
            return null;
        }
        
        ParienteDTO dto = new ParienteDTO();

		ParientePK pk = pariente.getId();
        dto.setBeneficiarioDTO(getBeneficiarioDTOFromBeneficiario(pk.getBeneficiario()));
        dto.setPersonaDTO(getPersonaDTOFromPersona(pk.getPersona()));
        
        dto.setFechaModifica(pariente.getFechaModifica());
        dto.setEstado(pariente.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(pariente.getEstado()));
        dto.setUsuarioModifica(pariente.getUsuarioModifica());
        dto.setParentescoDTO(getParentescoDTOFromParentesco(pariente.getParentesco()));
        dto.setUsuarioCrea(pariente.getUsuarioCrea());
        dto.setFechaCrea(pariente.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ParienteDTO} a la entidad {@link Pariente}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param pariente - {@link Pariente}
	 * @param parienteDTO - {@link ParienteDTO}
	 * @return {@link Pariente}
	 */
    public static Pariente getParienteFromParienteDTO(Pariente pariente, ParienteDTO parienteDTO){
    
    	if (parienteDTO == null) {
            return null;
        }
        
        if(pariente == null){
        	pariente = new Pariente();
        }
        
		ParientePK pk = new ParientePK();
        pk.setBeneficiario(getBeneficiarioFromBeneficiarioDTO(pk.getBeneficiario(), parienteDTO.getBeneficiarioDTO()));
        pk.setPersona(getPersonaFromPersonaDTO(pk.getPersona(), parienteDTO.getPersonaDTO()));
        pariente.setId(pk);
        
        pariente.setFechaModifica(parienteDTO.getFechaModifica());
        pariente.setEstado(EstadoEnum.valueOf(parienteDTO.getEstado()));
        pariente.setUsuarioModifica(parienteDTO.getUsuarioModifica());
        pariente.setUsuarioCrea(parienteDTO.getUsuarioCrea());
        pariente.setFechaCrea(parienteDTO.getFechaCrea());
    
    	return pariente;
    
    }

    /**
	 * Transforma la entidad {@link BeneficiarioServicio} al DTO {@link BeneficiarioServicioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiarioServicio - {@link BeneficiarioServicio}
	 * @return {@link BeneficiarioServicioDTO}
	 */
    public static BeneficiarioServicioDTO getBeneficiarioServicioDTOFromBeneficiarioServicio(BeneficiarioServicio beneficiarioServicio){
    
    	if (beneficiarioServicio == null) {
            return null;
        }
        
        BeneficiarioServicioDTO dto = new BeneficiarioServicioDTO();

		BeneficiarioServicioPK pk = beneficiarioServicio.getId();
        dto.setBeneficiarioDTO(getBeneficiarioDTOFromBeneficiario(pk.getBeneficiario()));
        dto.setServicioDTO(getServicioDTOFromServicio(pk.getServicio()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link BeneficiarioServicioDTO} a la entidad {@link BeneficiarioServicio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param beneficiarioServicio - {@link BeneficiarioServicio}
	 * @param beneficiarioServicioDTO - {@link BeneficiarioServicioDTO}
	 * @return {@link BeneficiarioServicio}
	 */
    public static BeneficiarioServicio getBeneficiarioServicioFromBeneficiarioServicioDTO(BeneficiarioServicio beneficiarioServicio, BeneficiarioServicioDTO beneficiarioServicioDTO){
    
    	if (beneficiarioServicioDTO == null) {
            return null;
        }
        
        if(beneficiarioServicio == null){
        	beneficiarioServicio = new BeneficiarioServicio();
        }
        
		BeneficiarioServicioPK pk = new BeneficiarioServicioPK();
        pk.setBeneficiario(getBeneficiarioFromBeneficiarioDTO(pk.getBeneficiario(), beneficiarioServicioDTO.getBeneficiarioDTO()));
        pk.setServicio(getServicioFromServicioDTO(pk.getServicio(), beneficiarioServicioDTO.getServicioDTO()));
        beneficiarioServicio.setId(pk);
        
    
    	return beneficiarioServicio;
    
    }

    /**
	 * Transforma la entidad {@link Objetivo} al DTO {@link ObjetivoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivo - {@link Objetivo}
	 * @return {@link ObjetivoDTO}
	 */
    public static ObjetivoDTO getObjetivoDTOFromObjetivo(Objetivo objetivo){
    
    	if (objetivo == null) {
            return null;
        }
        
        ObjetivoDTO dto = new ObjetivoDTO();

        dto.setId(objetivo.getId());
        
        dto.setFechaModifica(objetivo.getFechaModifica());
        dto.setEstado(objetivo.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(objetivo.getEstado()));
        dto.setUsuarioModifica(objetivo.getUsuarioModifica());
        dto.setDescripcion(objetivo.getDescripcion());
        dto.setUsuarioCrea(objetivo.getUsuarioCrea());
        dto.setUsuarioDTO(getUsuarioDTOFromUsuario(objetivo.getUsuario()));
        dto.setFechaCrea(objetivo.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ObjetivoDTO} a la entidad {@link Objetivo}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivo - {@link Objetivo}
	 * @param objetivoDTO - {@link ObjetivoDTO}
	 * @return {@link Objetivo}
	 */
    public static Objetivo getObjetivoFromObjetivoDTO(Objetivo objetivo, ObjetivoDTO objetivoDTO){
    
    	if (objetivoDTO == null) {
            return null;
        }
        
        if(objetivo == null){
        	objetivo = new Objetivo();
        }
        
        objetivo.setId(objetivoDTO.getId());
        
        objetivo.setFechaModifica(objetivoDTO.getFechaModifica());
        objetivo.setEstado(EstadoEnum.valueOf(objetivoDTO.getEstado()));
        objetivo.setUsuarioModifica(objetivoDTO.getUsuarioModifica());
        objetivo.setDescripcion(objetivoDTO.getDescripcion());
        objetivo.setUsuarioCrea(objetivoDTO.getUsuarioCrea());
        objetivo.setFechaCrea(objetivoDTO.getFechaCrea());
    
    	return objetivo;
    
    }

    /**
	 * Transforma la entidad {@link RolUsuario} al DTO {@link RolUsuarioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolUsuario - {@link RolUsuario}
	 * @return {@link RolUsuarioDTO}
	 */
    public static RolUsuarioDTO getRolUsuarioDTOFromRolUsuario(RolUsuario rolUsuario){
    
    	if (rolUsuario == null) {
            return null;
        }
        
        RolUsuarioDTO dto = new RolUsuarioDTO();

		RolUsuarioPK pk = rolUsuario.getId();
        dto.setRolDTO(getRolDTOFromRol(pk.getRol()));
        dto.setUsuarioDTO(getUsuarioDTOFromUsuario(pk.getUsuario()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link RolUsuarioDTO} a la entidad {@link RolUsuario}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param rolUsuario - {@link RolUsuario}
	 * @param rolUsuarioDTO - {@link RolUsuarioDTO}
	 * @return {@link RolUsuario}
	 */
    public static RolUsuario getRolUsuarioFromRolUsuarioDTO(RolUsuario rolUsuario, RolUsuarioDTO rolUsuarioDTO){
    
    	if (rolUsuarioDTO == null) {
            return null;
        }
        
        if(rolUsuario == null){
        	rolUsuario = new RolUsuario();
        }
        
		RolUsuarioPK pk = new RolUsuarioPK();
        pk.setRol(getRolFromRolDTO(pk.getRol(), rolUsuarioDTO.getRolDTO()));
        pk.setUsuario(getUsuarioFromUsuarioDTO(pk.getUsuario(), rolUsuarioDTO.getUsuarioDTO()));
        rolUsuario.setId(pk);
        
    
    	return rolUsuario;
    
    }

    /**
	 * Transforma la entidad {@link EntidadLinea} al DTO {@link EntidadLineaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadLinea - {@link EntidadLinea}
	 * @return {@link EntidadLineaDTO}
	 */
    public static EntidadLineaDTO getEntidadLineaDTOFromEntidadLinea(EntidadLinea entidadLinea){
    
    	if (entidadLinea == null) {
            return null;
        }
        
        EntidadLineaDTO dto = new EntidadLineaDTO();

		EntidadLineaPK pk = entidadLinea.getId();
        dto.setEntidadDTO(getEntidadDTOFromEntidad(pk.getEntidad()));
        dto.setLineaIntervencionDTO(getLineaIntervencionDTOFromLineaIntervencion(pk.getLineaIntervencion()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EntidadLineaDTO} a la entidad {@link EntidadLinea}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadLinea - {@link EntidadLinea}
	 * @param entidadLineaDTO - {@link EntidadLineaDTO}
	 * @return {@link EntidadLinea}
	 */
    public static EntidadLinea getEntidadLineaFromEntidadLineaDTO(EntidadLinea entidadLinea, EntidadLineaDTO entidadLineaDTO){
    
    	if (entidadLineaDTO == null) {
            return null;
        }
        
        if(entidadLinea == null){
        	entidadLinea = new EntidadLinea();
        }
        
		EntidadLineaPK pk = new EntidadLineaPK();
        pk.setEntidad(getEntidadFromEntidadDTO(pk.getEntidad(), entidadLineaDTO.getEntidadDTO()));
        pk.setLineaIntervencion(getLineaIntervencionFromLineaIntervencionDTO(pk.getLineaIntervencion(), entidadLineaDTO.getLineaIntervencionDTO()));
        entidadLinea.setId(pk);
        
    
    	return entidadLinea;
    
    }

    /**
	 * Transforma la entidad {@link Iniciativa} al DTO {@link IniciativaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param iniciativa - {@link Iniciativa}
	 * @return {@link IniciativaDTO}
	 */
    public static IniciativaDTO getIniciativaDTOFromIniciativa(Iniciativa iniciativa){
    
    	if (iniciativa == null) {
            return null;
        }
        
        IniciativaDTO dto = new IniciativaDTO();

        dto.setId(iniciativa.getId());
        
        dto.setNombre(iniciativa.getNombre());
        dto.setFechaModifica(iniciativa.getFechaModifica());
        dto.setEstado(iniciativa.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(iniciativa.getEstado()));
        dto.setUsuarioModifica(iniciativa.getUsuarioModifica());
        dto.setDescripcion(iniciativa.getDescripcion());
        dto.setUsuarioCrea(iniciativa.getUsuarioCrea());
        dto.setUsuarioDTO(getUsuarioDTOFromUsuario(iniciativa.getUsuario()));
        dto.setFechaCrea(iniciativa.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link IniciativaDTO} a la entidad {@link Iniciativa}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param iniciativa - {@link Iniciativa}
	 * @param iniciativaDTO - {@link IniciativaDTO}
	 * @return {@link Iniciativa}
	 */
    public static Iniciativa getIniciativaFromIniciativaDTO(Iniciativa iniciativa, IniciativaDTO iniciativaDTO){
    
    	if (iniciativaDTO == null) {
            return null;
        }
        
        if(iniciativa == null){
        	iniciativa = new Iniciativa();
        }
        
        iniciativa.setId(iniciativaDTO.getId());
        
        iniciativa.setNombre(iniciativaDTO.getNombre());
        iniciativa.setFechaModifica(iniciativaDTO.getFechaModifica());
        iniciativa.setEstado(EstadoEnum.valueOf(iniciativaDTO.getEstado()));
        iniciativa.setUsuarioModifica(iniciativaDTO.getUsuarioModifica());
        iniciativa.setDescripcion(iniciativaDTO.getDescripcion());
        iniciativa.setUsuarioCrea(iniciativaDTO.getUsuarioCrea());
        iniciativa.setFechaCrea(iniciativaDTO.getFechaCrea());
    
    	return iniciativa;
    
    }

    /**
	 * Transforma la entidad {@link Territorio} al DTO {@link TerritorioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link Territorio}
	 * @return {@link TerritorioDTO}
	 */
    public static TerritorioDTO getTerritorioDTOFromTerritorio(Territorio territorio){
    
    	if (territorio == null) {
            return null;
        }
        
        TerritorioDTO dto = new TerritorioDTO();

        dto.setId(territorio.getId());
        
        dto.setFechaModifica(territorio.getFechaModifica());
        dto.setJovenesHombres(territorio.getJovenesHombres());
        dto.setJovenesMujeres(territorio.getJovenesMujeres());
        dto.setMenoresMujeres(territorio.getMenoresMujeres());
        dto.setEstadoViaDTO(getEstadoViaDTOFromEstadoVia(territorio.getEstadoVia()));
        dto.setNombre(territorio.getNombre());
        dto.setAdultosMayoresHombres(territorio.getAdultosMayoresHombres());
        dto.setTipoTerritorioDTO(getTipoTerritorioDTOFromTipoTerritorio(territorio.getTipoTerritorio()));
        dto.setEstado(territorio.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(territorio.getEstado()));
        dto.setProblemaComunidadDTO(getProblemaComunidadDTOFromProblemaComunidad(territorio.getProblemaComunidad()));
        dto.setUsuarioModifica(territorio.getUsuarioModifica());
        dto.setAdultosMayoresMujeres(territorio.getAdultosMayoresMujeres());
        dto.setUsuarioCrea(territorio.getUsuarioCrea());
        dto.setMenoresHombres(territorio.getMenoresHombres());
        dto.setZonaDTO(getZonaDTOFromZona(territorio.getZona()));
        dto.setFechaCrea(territorio.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TerritorioDTO} a la entidad {@link Territorio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param territorio - {@link Territorio}
	 * @param territorioDTO - {@link TerritorioDTO}
	 * @return {@link Territorio}
	 */
    public static Territorio getTerritorioFromTerritorioDTO(Territorio territorio, TerritorioDTO territorioDTO){
    
    	if (territorioDTO == null) {
            return null;
        }
        
        if(territorio == null){
        	territorio = new Territorio();
        }
        
        territorio.setId(territorioDTO.getId());
        
        territorio.setFechaModifica(territorioDTO.getFechaModifica());
        territorio.setJovenesHombres(territorioDTO.getJovenesHombres());
        territorio.setJovenesMujeres(territorioDTO.getJovenesMujeres());
        territorio.setMenoresMujeres(territorioDTO.getMenoresMujeres());
        territorio.setNombre(territorioDTO.getNombre());
        territorio.setAdultosMayoresHombres(territorioDTO.getAdultosMayoresHombres());
        territorio.setEstado(EstadoEnum.valueOf(territorioDTO.getEstado()));
        territorio.setUsuarioModifica(territorioDTO.getUsuarioModifica());
        territorio.setAdultosMayoresMujeres(territorioDTO.getAdultosMayoresMujeres());
        territorio.setUsuarioCrea(territorioDTO.getUsuarioCrea());
        territorio.setMenoresHombres(territorioDTO.getMenoresHombres());
        territorio.setFechaCrea(territorioDTO.getFechaCrea());
    
    	return territorio;
    
    }

    /**
	 * Transforma la entidad {@link Actividad} al DTO {@link ActividadDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param actividad - {@link Actividad}
	 * @return {@link ActividadDTO}
	 */
    public static ActividadDTO getActividadDTOFromActividad(Actividad actividad){
    
    	if (actividad == null) {
            return null;
        }
        
        ActividadDTO dto = new ActividadDTO();

        dto.setId(actividad.getId());
        
        dto.setComponenteDTO(getComponenteDTOFromComponente(actividad.getComponente()));
        dto.setFechaModifica(actividad.getFechaModifica());
        dto.setResultadoObtenido(actividad.getResultadoObtenido());
        dto.setEstadoActividadDTO(getEstadoActividadDTOFromEstadoActividad(actividad.getEstadoActividad()));
        dto.setObservacionSeg(actividad.getObservacionSeg());
        dto.setResultadoEsperado(actividad.getResultadoEsperado());
        dto.setFechaSeguimiento(actividad.getFechaSeguimiento());
        dto.setNombre(actividad.getNombre());
        dto.setRecursos(actividad.getRecursos());
        dto.setEstado(actividad.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(actividad.getEstado()));
        dto.setUsuarioModifica(actividad.getUsuarioModifica());
        dto.setCantAsistentes(actividad.getCantAsistentes());
        dto.setUsuarioCrea(actividad.getUsuarioCrea());
        dto.setUsuarioDTO(getUsuarioDTOFromUsuario(actividad.getUsuario()));
        dto.setFechaEjecucion(actividad.getFechaEjecucion());
        dto.setFechaCrea(actividad.getFechaCrea());
        dto.setLeccionesAprendidas(actividad.getLeccionesAprendidas());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ActividadDTO} a la entidad {@link Actividad}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param actividad - {@link Actividad}
	 * @param actividadDTO - {@link ActividadDTO}
	 * @return {@link Actividad}
	 */
    public static Actividad getActividadFromActividadDTO(Actividad actividad, ActividadDTO actividadDTO){
    
    	if (actividadDTO == null) {
            return null;
        }
        
        if(actividad == null){
        	actividad = new Actividad();
        }
        
        actividad.setId(actividadDTO.getId());
        
        actividad.setFechaModifica(actividadDTO.getFechaModifica());
        actividad.setResultadoObtenido(actividadDTO.getResultadoObtenido());
        actividad.setObservacionSeg(actividadDTO.getObservacionSeg());
        actividad.setResultadoEsperado(actividadDTO.getResultadoEsperado());
        actividad.setFechaSeguimiento(actividadDTO.getFechaSeguimiento());
        actividad.setNombre(actividadDTO.getNombre());
        actividad.setRecursos(actividadDTO.getRecursos());
        actividad.setEstado(EstadoEnum.valueOf(actividadDTO.getEstado()));
        actividad.setUsuarioModifica(actividadDTO.getUsuarioModifica());
        actividad.setCantAsistentes(actividadDTO.getCantAsistentes());
        actividad.setUsuarioCrea(actividadDTO.getUsuarioCrea());
        actividad.setFechaEjecucion(actividadDTO.getFechaEjecucion());
        actividad.setFechaCrea(actividadDTO.getFechaCrea());
        actividad.setLeccionesAprendidas(actividadDTO.getLeccionesAprendidas());
    
    	return actividad;
    
    }

    /**
	 * Transforma la entidad {@link EntidadTerritorio} al DTO {@link EntidadTerritorioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadTerritorio - {@link EntidadTerritorio}
	 * @return {@link EntidadTerritorioDTO}
	 */
    public static EntidadTerritorioDTO getEntidadTerritorioDTOFromEntidadTerritorio(EntidadTerritorio entidadTerritorio){
    
    	if (entidadTerritorio == null) {
            return null;
        }
        
        EntidadTerritorioDTO dto = new EntidadTerritorioDTO();

		EntidadTerritorioPK pk = entidadTerritorio.getId();
        dto.setTerritorioDTO(getTerritorioDTOFromTerritorio(pk.getTerritorio()));
        dto.setEntidadDTO(getEntidadDTOFromEntidad(pk.getEntidad()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link EntidadTerritorioDTO} a la entidad {@link EntidadTerritorio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidadTerritorio - {@link EntidadTerritorio}
	 * @param entidadTerritorioDTO - {@link EntidadTerritorioDTO}
	 * @return {@link EntidadTerritorio}
	 */
    public static EntidadTerritorio getEntidadTerritorioFromEntidadTerritorioDTO(EntidadTerritorio entidadTerritorio, EntidadTerritorioDTO entidadTerritorioDTO){
    
    	if (entidadTerritorioDTO == null) {
            return null;
        }
        
        if(entidadTerritorio == null){
        	entidadTerritorio = new EntidadTerritorio();
        }
        
		EntidadTerritorioPK pk = new EntidadTerritorioPK();
        pk.setTerritorio(getTerritorioFromTerritorioDTO(pk.getTerritorio(), entidadTerritorioDTO.getTerritorioDTO()));
        pk.setEntidad(getEntidadFromEntidadDTO(pk.getEntidad(), entidadTerritorioDTO.getEntidadDTO()));
        entidadTerritorio.setId(pk);
        
    
    	return entidadTerritorio;
    
    }

    /**
	 * Transforma la entidad {@link Linea} al DTO {@link LineaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param linea - {@link Linea}
	 * @return {@link LineaDTO}
	 */
    public static LineaDTO getLineaDTOFromLinea(Linea linea){
    
    	if (linea == null) {
            return null;
        }
        
        LineaDTO dto = new LineaDTO();

        dto.setId(linea.getId());
        
        dto.setNombre(linea.getNombre());
        dto.setFechaModifica(linea.getFechaModifica());
        dto.setEstado(linea.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(linea.getEstado()));
        dto.setObjetivoDTO(getObjetivoDTOFromObjetivo(linea.getObjetivo()));
        dto.setUsuarioModifica(linea.getUsuarioModifica());
        dto.setUsuarioCrea(linea.getUsuarioCrea());
        dto.setFechaCrea(linea.getFechaCrea());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link LineaDTO} a la entidad {@link Linea}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param linea - {@link Linea}
	 * @param lineaDTO - {@link LineaDTO}
	 * @return {@link Linea}
	 */
    public static Linea getLineaFromLineaDTO(Linea linea, LineaDTO lineaDTO){
    
    	if (lineaDTO == null) {
            return null;
        }
        
        if(linea == null){
        	linea = new Linea();
        }
        
        linea.setId(lineaDTO.getId());
        
        linea.setNombre(lineaDTO.getNombre());
        linea.setFechaModifica(lineaDTO.getFechaModifica());
        linea.setEstado(EstadoEnum.valueOf(lineaDTO.getEstado()));
        linea.setUsuarioModifica(lineaDTO.getUsuarioModifica());
        linea.setUsuarioCrea(lineaDTO.getUsuarioCrea());
        linea.setFechaCrea(lineaDTO.getFechaCrea());
    
    	return linea;
    
    }

    /**
	 * Transforma la entidad {@link IndicadorObjetivo} al DTO {@link IndicadorObjetivoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorObjetivo - {@link IndicadorObjetivo}
	 * @return {@link IndicadorObjetivoDTO}
	 */
    public static IndicadorObjetivoDTO getIndicadorObjetivoDTOFromIndicadorObjetivo(IndicadorObjetivo indicadorObjetivo){
    
    	if (indicadorObjetivo == null) {
            return null;
        }
        
        IndicadorObjetivoDTO dto = new IndicadorObjetivoDTO();

		IndicadorObjetivoPK pk = indicadorObjetivo.getId();
        dto.setIndicadorDTO(getIndicadorDTOFromIndicador(pk.getIndicador()));
        dto.setObjetivoDTO(getObjetivoDTOFromObjetivo(pk.getObjetivo()));
        
        dto.setResultado(indicadorObjetivo.getResultado());
        dto.setMeta(indicadorObjetivo.getMeta());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link IndicadorObjetivoDTO} a la entidad {@link IndicadorObjetivo}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorObjetivo - {@link IndicadorObjetivo}
	 * @param indicadorObjetivoDTO - {@link IndicadorObjetivoDTO}
	 * @return {@link IndicadorObjetivo}
	 */
    public static IndicadorObjetivo getIndicadorObjetivoFromIndicadorObjetivoDTO(IndicadorObjetivo indicadorObjetivo, IndicadorObjetivoDTO indicadorObjetivoDTO){
    
    	if (indicadorObjetivoDTO == null) {
            return null;
        }
        
        if(indicadorObjetivo == null){
        	indicadorObjetivo = new IndicadorObjetivo();
        }
        
		IndicadorObjetivoPK pk = new IndicadorObjetivoPK();
        pk.setIndicador(getIndicadorFromIndicadorDTO(pk.getIndicador(), indicadorObjetivoDTO.getIndicadorDTO()));
        pk.setObjetivo(getObjetivoFromObjetivoDTO(pk.getObjetivo(), indicadorObjetivoDTO.getObjetivoDTO()));
        indicadorObjetivo.setId(pk);
        
        indicadorObjetivo.setResultado(indicadorObjetivoDTO.getResultado());
        indicadorObjetivo.setMeta(indicadorObjetivoDTO.getMeta());
    
    	return indicadorObjetivo;
    
    }

    /**
	 * Transforma la entidad {@link ObjetivoIniciativa} al DTO {@link ObjetivoIniciativaDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivoIniciativa - {@link ObjetivoIniciativa}
	 * @return {@link ObjetivoIniciativaDTO}
	 */
    public static ObjetivoIniciativaDTO getObjetivoIniciativaDTOFromObjetivoIniciativa(ObjetivoIniciativa objetivoIniciativa){
    
    	if (objetivoIniciativa == null) {
            return null;
        }
        
        ObjetivoIniciativaDTO dto = new ObjetivoIniciativaDTO();

		ObjetivoIniciativaPK pk = objetivoIniciativa.getId();
        dto.setObjetivoDTO(getObjetivoDTOFromObjetivo(pk.getObjetivo()));
        dto.setIniciativaDTO(getIniciativaDTOFromIniciativa(pk.getIniciativa()));
        dto.setIdUsuarioResp(pk.getIdUsuarioResp());
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ObjetivoIniciativaDTO} a la entidad {@link ObjetivoIniciativa}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param objetivoIniciativa - {@link ObjetivoIniciativa}
	 * @param objetivoIniciativaDTO - {@link ObjetivoIniciativaDTO}
	 * @return {@link ObjetivoIniciativa}
	 */
    public static ObjetivoIniciativa getObjetivoIniciativaFromObjetivoIniciativaDTO(ObjetivoIniciativa objetivoIniciativa, ObjetivoIniciativaDTO objetivoIniciativaDTO){
    
    	if (objetivoIniciativaDTO == null) {
            return null;
        }
        
        if(objetivoIniciativa == null){
        	objetivoIniciativa = new ObjetivoIniciativa();
        }
        
		ObjetivoIniciativaPK pk = new ObjetivoIniciativaPK();
        pk.setObjetivo(getObjetivoFromObjetivoDTO(pk.getObjetivo(), objetivoIniciativaDTO.getObjetivoDTO()));
        pk.setIniciativa(getIniciativaFromIniciativaDTO(pk.getIniciativa(), objetivoIniciativaDTO.getIniciativaDTO()));
        pk.setIdUsuarioResp(objetivoIniciativaDTO.getIdUsuarioResp());
        objetivoIniciativa.setId(pk);
        
    
    	return objetivoIniciativa;
    
    }

    /**
	 * Transforma la entidad {@link LiderTerritorio} al DTO {@link LiderTerritorioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param liderTerritorio - {@link LiderTerritorio}
	 * @return {@link LiderTerritorioDTO}
	 */
    public static LiderTerritorioDTO getLiderTerritorioDTOFromLiderTerritorio(LiderTerritorio liderTerritorio){
    
    	if (liderTerritorio == null) {
            return null;
        }
        
        LiderTerritorioDTO dto = new LiderTerritorioDTO();

		LiderTerritorioPK pk = liderTerritorio.getId();
        dto.setTerritorioDTO(getTerritorioDTOFromTerritorio(pk.getTerritorio()));
        dto.setLiderDTO(getLiderDTOFromLider(pk.getLider()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link LiderTerritorioDTO} a la entidad {@link LiderTerritorio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param liderTerritorio - {@link LiderTerritorio}
	 * @param liderTerritorioDTO - {@link LiderTerritorioDTO}
	 * @return {@link LiderTerritorio}
	 */
    public static LiderTerritorio getLiderTerritorioFromLiderTerritorioDTO(LiderTerritorio liderTerritorio, LiderTerritorioDTO liderTerritorioDTO){
    
    	if (liderTerritorioDTO == null) {
            return null;
        }
        
        if(liderTerritorio == null){
        	liderTerritorio = new LiderTerritorio();
        }
        
		LiderTerritorioPK pk = new LiderTerritorioPK();
        pk.setTerritorio(getTerritorioFromTerritorioDTO(pk.getTerritorio(), liderTerritorioDTO.getTerritorioDTO()));
        pk.setLider(getLiderFromLiderDTO(pk.getLider(), liderTerritorioDTO.getLiderDTO()));
        liderTerritorio.setId(pk);
        
    
    	return liderTerritorio;
    
    }

    /**
	 * Transforma la entidad {@link TrazaActividad} al DTO {@link TrazaActividadDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaActividad - {@link TrazaActividad}
	 * @return {@link TrazaActividadDTO}
	 */
    public static TrazaActividadDTO getTrazaActividadDTOFromTrazaActividad(TrazaActividad trazaActividad){
    
    	if (trazaActividad == null) {
            return null;
        }
        
        TrazaActividadDTO dto = new TrazaActividadDTO();

        dto.setId(trazaActividad.getId());
        
        dto.setFecha(trazaActividad.getFecha());
        dto.setUsuario(trazaActividad.getUsuario());
        dto.setEstadoActividad(trazaActividad.getEstadoActividad());
        dto.setActividadDTO(getActividadDTOFromActividad(trazaActividad.getActividad()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TrazaActividadDTO} a la entidad {@link TrazaActividad}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaActividad - {@link TrazaActividad}
	 * @param trazaActividadDTO - {@link TrazaActividadDTO}
	 * @return {@link TrazaActividad}
	 */
    public static TrazaActividad getTrazaActividadFromTrazaActividadDTO(TrazaActividad trazaActividad, TrazaActividadDTO trazaActividadDTO){
    
    	if (trazaActividadDTO == null) {
            return null;
        }
        
        if(trazaActividad == null){
        	trazaActividad = new TrazaActividad();
        }
        
        trazaActividad.setId(trazaActividadDTO.getId());
        
        trazaActividad.setFecha(trazaActividadDTO.getFecha());
        trazaActividad.setUsuario(trazaActividadDTO.getUsuario());
        trazaActividad.setEstadoActividad(trazaActividadDTO.getEstadoActividad());
    
    	return trazaActividad;
    
    }

    /**
	 * Transforma la entidad {@link Proyecto} al DTO {@link ProyectoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyecto - {@link Proyecto}
	 * @return {@link ProyectoDTO}
	 */
    public static ProyectoDTO getProyectoDTOFromProyecto(Proyecto proyecto){
    
    	if (proyecto == null) {
            return null;
        }
        
        ProyectoDTO dto = new ProyectoDTO();

        dto.setId(proyecto.getId());
        
        dto.setNombre(proyecto.getNombre());
        dto.setFechaModifica(proyecto.getFechaModifica());
        dto.setEstado(proyecto.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(proyecto.getEstado()));
        dto.setRolFundacionDTO(getRolFundacionDTOFromRolFundacion(proyecto.getRolFundacion()));
        dto.setUsuarioModifica(proyecto.getUsuarioModifica());
        dto.setUsuarioCrea(proyecto.getUsuarioCrea());
        dto.setUsuarioDTO(getUsuarioDTOFromUsuario(proyecto.getUsuario()));
        dto.setIniciativaDTO(getIniciativaDTOFromIniciativa(proyecto.getIniciativa()));
        dto.setFechaCrea(proyecto.getFechaCrea());
        dto.setPpto(proyecto.getPpto());
        dto.setPoblacionBeneficiaria(proyecto.getPoblacionBeneficiaria());
        dto.setDuracion(proyecto.getDuracion());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ProyectoDTO} a la entidad {@link Proyecto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyecto - {@link Proyecto}
	 * @param proyectoDTO - {@link ProyectoDTO}
	 * @return {@link Proyecto}
	 */
    public static Proyecto getProyectoFromProyectoDTO(Proyecto proyecto, ProyectoDTO proyectoDTO){
    
    	if (proyectoDTO == null) {
            return null;
        }
        
        if(proyecto == null){
        	proyecto = new Proyecto();
        }
        
        proyecto.setId(proyectoDTO.getId());
        
        proyecto.setNombre(proyectoDTO.getNombre());
        proyecto.setFechaModifica(proyectoDTO.getFechaModifica());
        proyecto.setEstado(EstadoEnum.valueOf(proyectoDTO.getEstado()));
        proyecto.setUsuarioModifica(proyectoDTO.getUsuarioModifica());
        proyecto.setUsuarioCrea(proyectoDTO.getUsuarioCrea());
        proyecto.setFechaCrea(proyectoDTO.getFechaCrea());
        proyecto.setPpto(proyectoDTO.getPpto());
        proyecto.setPoblacionBeneficiaria(proyectoDTO.getPoblacionBeneficiaria());
        proyecto.setDuracion(proyectoDTO.getDuracion());
    
    	return proyecto;
    
    }

    /**
	 * Transforma la entidad {@link ProyectoBeneficiario} al DTO {@link ProyectoBeneficiarioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoBeneficiario - {@link ProyectoBeneficiario}
	 * @return {@link ProyectoBeneficiarioDTO}
	 */
    public static ProyectoBeneficiarioDTO getProyectoBeneficiarioDTOFromProyectoBeneficiario(ProyectoBeneficiario proyectoBeneficiario){
    
    	if (proyectoBeneficiario == null) {
            return null;
        }
        
        ProyectoBeneficiarioDTO dto = new ProyectoBeneficiarioDTO();

        dto.setId(proyectoBeneficiario.getId());
        
        dto.setBeneficiarioDTO(getBeneficiarioDTOFromBeneficiario(proyectoBeneficiario.getBeneficiario()));
        dto.setProyectoDTO(getProyectoDTOFromProyecto(proyectoBeneficiario.getProyecto()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ProyectoBeneficiarioDTO} a la entidad {@link ProyectoBeneficiario}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoBeneficiario - {@link ProyectoBeneficiario}
	 * @param proyectoBeneficiarioDTO - {@link ProyectoBeneficiarioDTO}
	 * @return {@link ProyectoBeneficiario}
	 */
    public static ProyectoBeneficiario getProyectoBeneficiarioFromProyectoBeneficiarioDTO(ProyectoBeneficiario proyectoBeneficiario, ProyectoBeneficiarioDTO proyectoBeneficiarioDTO){
    
    	if (proyectoBeneficiarioDTO == null) {
            return null;
        }
        
        if(proyectoBeneficiario == null){
        	proyectoBeneficiario = new ProyectoBeneficiario();
        }
        
        proyectoBeneficiario.setId(proyectoBeneficiarioDTO.getId());
        
    
    	return proyectoBeneficiario;
    
    }

    /**
	 * Transforma la entidad {@link ProyectoTerritorio} al DTO {@link ProyectoTerritorioDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoTerritorio - {@link ProyectoTerritorio}
	 * @return {@link ProyectoTerritorioDTO}
	 */
    public static ProyectoTerritorioDTO getProyectoTerritorioDTOFromProyectoTerritorio(ProyectoTerritorio proyectoTerritorio){
    
    	if (proyectoTerritorio == null) {
            return null;
        }
        
        ProyectoTerritorioDTO dto = new ProyectoTerritorioDTO();

		ProyectoTerritorioPK pk = proyectoTerritorio.getId();
        dto.setTerritorioDTO(getTerritorioDTOFromTerritorio(pk.getTerritorio()));
        dto.setProyectoDTO(getProyectoDTOFromProyecto(pk.getProyecto()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ProyectoTerritorioDTO} a la entidad {@link ProyectoTerritorio}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoTerritorio - {@link ProyectoTerritorio}
	 * @param proyectoTerritorioDTO - {@link ProyectoTerritorioDTO}
	 * @return {@link ProyectoTerritorio}
	 */
    public static ProyectoTerritorio getProyectoTerritorioFromProyectoTerritorioDTO(ProyectoTerritorio proyectoTerritorio, ProyectoTerritorioDTO proyectoTerritorioDTO){
    
    	if (proyectoTerritorioDTO == null) {
            return null;
        }
        
        if(proyectoTerritorio == null){
        	proyectoTerritorio = new ProyectoTerritorio();
        }
        
		ProyectoTerritorioPK pk = new ProyectoTerritorioPK();
        pk.setTerritorio(getTerritorioFromTerritorioDTO(pk.getTerritorio(), proyectoTerritorioDTO.getTerritorioDTO()));
        pk.setProyecto(getProyectoFromProyectoDTO(pk.getProyecto(), proyectoTerritorioDTO.getProyectoDTO()));
        proyectoTerritorio.setId(pk);
        
    
    	return proyectoTerritorio;
    
    }

    /**
	 * Transforma la entidad {@link TrazaPresupuesto} al DTO {@link TrazaPresupuestoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaPresupuesto - {@link TrazaPresupuesto}
	 * @return {@link TrazaPresupuestoDTO}
	 */
    public static TrazaPresupuestoDTO getTrazaPresupuestoDTOFromTrazaPresupuesto(TrazaPresupuesto trazaPresupuesto){
    
    	if (trazaPresupuesto == null) {
            return null;
        }
        
        TrazaPresupuestoDTO dto = new TrazaPresupuestoDTO();

        dto.setId(trazaPresupuesto.getId());
        
        dto.setAnio(trazaPresupuesto.getAnio());
        dto.setFechaModifica(trazaPresupuesto.getFechaModifica());
        dto.setEstado(trazaPresupuesto.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(trazaPresupuesto.getEstado()));
        dto.setUsuarioModifica(trazaPresupuesto.getUsuarioModifica());
        dto.setLineaDTO(getLineaDTOFromLinea(trazaPresupuesto.getLinea()));
        dto.setUsuarioCrea(trazaPresupuesto.getUsuarioCrea());
        dto.setPresupuestoEjecutado(trazaPresupuesto.getPresupuestoEjecutado());
        dto.setFechaCrea(trazaPresupuesto.getFechaCrea());
        dto.setPresupuestoPlaneado(trazaPresupuesto.getPresupuestoPlaneado());
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link TrazaPresupuestoDTO} a la entidad {@link TrazaPresupuesto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param trazaPresupuesto - {@link TrazaPresupuesto}
	 * @param trazaPresupuestoDTO - {@link TrazaPresupuestoDTO}
	 * @return {@link TrazaPresupuesto}
	 */
    public static TrazaPresupuesto getTrazaPresupuestoFromTrazaPresupuestoDTO(TrazaPresupuesto trazaPresupuesto, TrazaPresupuestoDTO trazaPresupuestoDTO){
    
    	if (trazaPresupuestoDTO == null) {
            return null;
        }
        
        if(trazaPresupuesto == null){
        	trazaPresupuesto = new TrazaPresupuesto();
        }
        
        trazaPresupuesto.setId(trazaPresupuestoDTO.getId());
        
        trazaPresupuesto.setAnio(trazaPresupuestoDTO.getAnio());
        trazaPresupuesto.setFechaModifica(trazaPresupuestoDTO.getFechaModifica());
        trazaPresupuesto.setEstado(EstadoEnum.valueOf(trazaPresupuestoDTO.getEstado()));
        trazaPresupuesto.setUsuarioModifica(trazaPresupuestoDTO.getUsuarioModifica());
        trazaPresupuesto.setUsuarioCrea(trazaPresupuestoDTO.getUsuarioCrea());
        trazaPresupuesto.setPresupuestoEjecutado(trazaPresupuestoDTO.getPresupuestoEjecutado());
        trazaPresupuesto.setFechaCrea(trazaPresupuestoDTO.getFechaCrea());
        trazaPresupuesto.setPresupuestoPlaneado(trazaPresupuestoDTO.getPresupuestoPlaneado());
    
    	return trazaPresupuesto;
    
    }

    /**
	 * Transforma la entidad {@link ComponenteProyecto} al DTO {@link ComponenteProyectoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componenteProyecto - {@link ComponenteProyecto}
	 * @return {@link ComponenteProyectoDTO}
	 */
    public static ComponenteProyectoDTO getComponenteProyectoDTOFromComponenteProyecto(ComponenteProyecto componenteProyecto){
    
    	if (componenteProyecto == null) {
            return null;
        }
        
        ComponenteProyectoDTO dto = new ComponenteProyectoDTO();

		ComponenteProyectoPK pk = componenteProyecto.getId();
        dto.setProyectoDTO(getProyectoDTOFromProyecto(pk.getProyecto()));
        dto.setComponenteDTO(getComponenteDTOFromComponente(pk.getComponente()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ComponenteProyectoDTO} a la entidad {@link ComponenteProyecto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param componenteProyecto - {@link ComponenteProyecto}
	 * @param componenteProyectoDTO - {@link ComponenteProyectoDTO}
	 * @return {@link ComponenteProyecto}
	 */
    public static ComponenteProyecto getComponenteProyectoFromComponenteProyectoDTO(ComponenteProyecto componenteProyecto, ComponenteProyectoDTO componenteProyectoDTO){
    
    	if (componenteProyectoDTO == null) {
            return null;
        }
        
        if(componenteProyecto == null){
        	componenteProyecto = new ComponenteProyecto();
        }
        
		ComponenteProyectoPK pk = new ComponenteProyectoPK();
        pk.setProyecto(getProyectoFromProyectoDTO(pk.getProyecto(), componenteProyectoDTO.getProyectoDTO()));
        pk.setComponente(getComponenteFromComponenteDTO(pk.getComponente(), componenteProyectoDTO.getComponenteDTO()));
        componenteProyecto.setId(pk);
        
    
    	return componenteProyecto;
    
    }

    /**
	 * Transforma la entidad {@link DocumentoAdjunto} al DTO {@link DocumentoAdjuntoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param documentoAdjunto - {@link DocumentoAdjunto}
	 * @return {@link DocumentoAdjuntoDTO}
	 */
    public static DocumentoAdjuntoDTO getDocumentoAdjuntoDTOFromDocumentoAdjunto(DocumentoAdjunto documentoAdjunto){
    
    	if (documentoAdjunto == null) {
            return null;
        }
        
        DocumentoAdjuntoDTO dto = new DocumentoAdjuntoDTO();

        dto.setId(documentoAdjunto.getId());
        
        dto.setNombre(documentoAdjunto.getNombre());
        dto.setFechaModifica(documentoAdjunto.getFechaModifica());
        dto.setEstado(documentoAdjunto.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(documentoAdjunto.getEstado()));
        dto.setUsuarioModifica(documentoAdjunto.getUsuarioModifica());
        dto.setDescripcion(documentoAdjunto.getDescripcion());
        dto.setUsuarioCrea(documentoAdjunto.getUsuarioCrea());
        dto.setFechaCrea(documentoAdjunto.getFechaCrea());
        dto.setProyectoDTO(getProyectoDTOFromProyecto(documentoAdjunto.getProyecto()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link DocumentoAdjuntoDTO} a la entidad {@link DocumentoAdjunto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param documentoAdjunto - {@link DocumentoAdjunto}
	 * @param documentoAdjuntoDTO - {@link DocumentoAdjuntoDTO}
	 * @return {@link DocumentoAdjunto}
	 */
    public static DocumentoAdjunto getDocumentoAdjuntoFromDocumentoAdjuntoDTO(DocumentoAdjunto documentoAdjunto, DocumentoAdjuntoDTO documentoAdjuntoDTO){
    
    	if (documentoAdjuntoDTO == null) {
            return null;
        }
        
        if(documentoAdjunto == null){
        	documentoAdjunto = new DocumentoAdjunto();
        }
        
        documentoAdjunto.setId(documentoAdjuntoDTO.getId());
        
        documentoAdjunto.setNombre(documentoAdjuntoDTO.getNombre());
        documentoAdjunto.setFechaModifica(documentoAdjuntoDTO.getFechaModifica());
        documentoAdjunto.setEstado(EstadoEnum.valueOf(documentoAdjuntoDTO.getEstado()));
        documentoAdjunto.setUsuarioModifica(documentoAdjuntoDTO.getUsuarioModifica());
        documentoAdjunto.setDescripcion(documentoAdjuntoDTO.getDescripcion());
        documentoAdjunto.setUsuarioCrea(documentoAdjuntoDTO.getUsuarioCrea());
        documentoAdjunto.setFechaCrea(documentoAdjuntoDTO.getFechaCrea());
    
    	return documentoAdjunto;
    
    }

    /**
	 * Transforma la entidad {@link IndicadorProyecto} al DTO {@link IndicadorProyectoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorProyecto - {@link IndicadorProyecto}
	 * @return {@link IndicadorProyectoDTO}
	 */
    public static IndicadorProyectoDTO getIndicadorProyectoDTOFromIndicadorProyecto(IndicadorProyecto indicadorProyecto){
    
    	if (indicadorProyecto == null) {
            return null;
        }
        
        IndicadorProyectoDTO dto = new IndicadorProyectoDTO();

		IndicadorProyectoPK pk = indicadorProyecto.getId();
        dto.setIndicadorDTO(getIndicadorDTOFromIndicador(pk.getIndicador()));
        dto.setProyectoDTO(getProyectoDTOFromProyecto(pk.getProyecto()));
        
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link IndicadorProyectoDTO} a la entidad {@link IndicadorProyecto}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param indicadorProyecto - {@link IndicadorProyecto}
	 * @param indicadorProyectoDTO - {@link IndicadorProyectoDTO}
	 * @return {@link IndicadorProyecto}
	 */
    public static IndicadorProyecto getIndicadorProyectoFromIndicadorProyectoDTO(IndicadorProyecto indicadorProyecto, IndicadorProyectoDTO indicadorProyectoDTO){
    
    	if (indicadorProyectoDTO == null) {
            return null;
        }
        
        if(indicadorProyecto == null){
        	indicadorProyecto = new IndicadorProyecto();
        }
        
		IndicadorProyectoPK pk = new IndicadorProyectoPK();
        pk.setIndicador(getIndicadorFromIndicadorDTO(pk.getIndicador(), indicadorProyectoDTO.getIndicadorDTO()));
        pk.setProyecto(getProyectoFromProyectoDTO(pk.getProyecto(), indicadorProyectoDTO.getProyectoDTO()));
        indicadorProyecto.setId(pk);
        
    
    	return indicadorProyecto;
    
    }

    /**
	 * Transforma la entidad {@link Aliado} al DTO {@link AliadoDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param aliado - {@link Aliado}
	 * @return {@link AliadoDTO}
	 */
    public static AliadoDTO getAliadoDTOFromAliado(Aliado aliado){
    
    	if (aliado == null) {
            return null;
        }
        
        AliadoDTO dto = new AliadoDTO();

        dto.setId(aliado.getId());
        
        dto.setTipoAliadoDTO(getTipoAliadoDTOFromTipoAliado(aliado.getTipoAliado()));
        dto.setFechaModifica(aliado.getFechaModifica());
        dto.setEntidadDTO(getEntidadDTOFromEntidad(aliado.getEntidad()));
        dto.setEstado(aliado.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(aliado.getEstado()));
        dto.setUsuarioModifica(aliado.getUsuarioModifica());
        dto.setUsuarioCrea(aliado.getUsuarioCrea());
        dto.setFechaCrea(aliado.getFechaCrea());
        dto.setProyectoDTO(getProyectoDTOFromProyecto(aliado.getProyecto()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link AliadoDTO} a la entidad {@link Aliado}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param aliado - {@link Aliado}
	 * @param aliadoDTO - {@link AliadoDTO}
	 * @return {@link Aliado}
	 */
    public static Aliado getAliadoFromAliadoDTO(Aliado aliado, AliadoDTO aliadoDTO){
    
    	if (aliadoDTO == null) {
            return null;
        }
        
        if(aliado == null){
        	aliado = new Aliado();
        }
        
        aliado.setId(aliadoDTO.getId());
        
        aliado.setFechaModifica(aliadoDTO.getFechaModifica());
        aliado.setEstado(EstadoEnum.valueOf(aliadoDTO.getEstado()));
        aliado.setUsuarioModifica(aliadoDTO.getUsuarioModifica());
        aliado.setUsuarioCrea(aliadoDTO.getUsuarioCrea());
        aliado.setFechaCrea(aliadoDTO.getFechaCrea());
    
    	return aliado;
    
    }

    /**
	 * Transforma la entidad {@link DatoAdicional} al DTO {@link DatoAdicionalDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoAdicional - {@link DatoAdicional}
	 * @return {@link DatoAdicionalDTO}
	 */
    public static DatoAdicionalDTO getDatoAdicionalDTOFromDatoAdicional(DatoAdicional datoAdicional){
    
    	if (datoAdicional == null) {
            return null;
        }
        
        DatoAdicionalDTO dto = new DatoAdicionalDTO();

        dto.setId(datoAdicional.getId());
        
        dto.setNombreDato(datoAdicional.getNombreDato());
        dto.setProyectoDTO(getProyectoDTOFromProyecto(datoAdicional.getProyecto()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link DatoAdicionalDTO} a la entidad {@link DatoAdicional}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param datoAdicional - {@link DatoAdicional}
	 * @param datoAdicionalDTO - {@link DatoAdicionalDTO}
	 * @return {@link DatoAdicional}
	 */
    public static DatoAdicional getDatoAdicionalFromDatoAdicionalDTO(DatoAdicional datoAdicional, DatoAdicionalDTO datoAdicionalDTO){
    
    	if (datoAdicionalDTO == null) {
            return null;
        }
        
        if(datoAdicional == null){
        	datoAdicional = new DatoAdicional();
        }
        
        datoAdicional.setId(datoAdicionalDTO.getId());
        
        datoAdicional.setNombreDato(datoAdicionalDTO.getNombreDato());
    
    	return datoAdicional;
    
    }

    /**
	 * Transforma la entidad {@link Asistente} al DTO {@link AsistenteDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param asistente - {@link Asistente}
	 * @return {@link AsistenteDTO}
	 */
    public static AsistenteDTO getAsistenteDTOFromAsistente(Asistente asistente){
    
    	if (asistente == null) {
            return null;
        }
        
        AsistenteDTO dto = new AsistenteDTO();

        dto.setId(asistente.getId());
        
        dto.setFechaModifica(asistente.getFechaModifica());
        dto.setEstado(asistente.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(asistente.getEstado()));
        dto.setUsuarioModifica(asistente.getUsuarioModifica());
        dto.setProyectoBeneficiarioDTO(getProyectoBeneficiarioDTOFromProyectoBeneficiario(asistente.getProyectoBeneficiario()));
        dto.setUsuarioCrea(asistente.getUsuarioCrea());
        dto.setFechaCrea(asistente.getFechaCrea());
        dto.setActividadDTO(getActividadDTOFromActividad(asistente.getActividad()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link AsistenteDTO} a la entidad {@link Asistente}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param asistente - {@link Asistente}
	 * @param asistenteDTO - {@link AsistenteDTO}
	 * @return {@link Asistente}
	 */
    public static Asistente getAsistenteFromAsistenteDTO(Asistente asistente, AsistenteDTO asistenteDTO){
    
    	if (asistenteDTO == null) {
            return null;
        }
        
        if(asistente == null){
        	asistente = new Asistente();
        }
        
        asistente.setId(asistenteDTO.getId());
        
        asistente.setFechaModifica(asistenteDTO.getFechaModifica());
        asistente.setEstado(EstadoEnum.valueOf(asistenteDTO.getEstado()));
        asistente.setUsuarioModifica(asistenteDTO.getUsuarioModifica());
        asistente.setUsuarioCrea(asistenteDTO.getUsuarioCrea());
        asistente.setFechaCrea(asistenteDTO.getFechaCrea());
    
    	return asistente;
    
    }

    /**
	 * Transforma la entidad {@link ValorDatoAdicional} al DTO {@link ValorDatoAdicionalDTO}.
	 * Si la entidad es nula retorna null.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param valorDatoAdicional - {@link ValorDatoAdicional}
	 * @return {@link ValorDatoAdicionalDTO}
	 */
    public static ValorDatoAdicionalDTO getValorDatoAdicionalDTOFromValorDatoAdicional(ValorDatoAdicional valorDatoAdicional){
    
    	if (valorDatoAdicional == null) {
            return null;
        }
        
        ValorDatoAdicionalDTO dto = new ValorDatoAdicionalDTO();

        dto.setId(valorDatoAdicional.getId());
        
        dto.setFechaModifica(valorDatoAdicional.getFechaModifica());
        dto.setEstado(valorDatoAdicional.getEstado().name());
        dto.setEstadoDesc(PmzEnumUtils.obtenerEnumLabel(valorDatoAdicional.getEstado()));
        dto.setValor(valorDatoAdicional.getValor());
        dto.setUsuarioModifica(valorDatoAdicional.getUsuarioModifica());
        dto.setUsuarioCrea(valorDatoAdicional.getUsuarioCrea());
        dto.setDatoAdicionalDTO(getDatoAdicionalDTOFromDatoAdicional(valorDatoAdicional.getDatoAdicional()));
        dto.setFechaCrea(valorDatoAdicional.getFechaCrea());
        dto.setBeneficiarioDTO(getBeneficiarioDTOFromBeneficiario(valorDatoAdicional.getBeneficiario()));
    
    	return dto;
    
    }

    /**
	 * Transforma el DTO {@link ValorDatoAdicionalDTO} a la entidad {@link ValorDatoAdicional}.
	 * Si el DTO es nulo retorna null. Si la entidad es nula se crea una nueva instancia.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param valorDatoAdicional - {@link ValorDatoAdicional}
	 * @param valorDatoAdicionalDTO - {@link ValorDatoAdicionalDTO}
	 * @return {@link ValorDatoAdicional}
	 */
    public static ValorDatoAdicional getValorDatoAdicionalFromValorDatoAdicionalDTO(ValorDatoAdicional valorDatoAdicional, ValorDatoAdicionalDTO valorDatoAdicionalDTO){
    
    	if (valorDatoAdicionalDTO == null) {
            return null;
        }
        
        if(valorDatoAdicional == null){
        	valorDatoAdicional = new ValorDatoAdicional();
        }
        
        valorDatoAdicional.setId(valorDatoAdicionalDTO.getId());
        
        valorDatoAdicional.setFechaModifica(valorDatoAdicionalDTO.getFechaModifica());
        valorDatoAdicional.setEstado(EstadoEnum.valueOf(valorDatoAdicionalDTO.getEstado()));
        valorDatoAdicional.setValor(valorDatoAdicionalDTO.getValor());
        valorDatoAdicional.setUsuarioModifica(valorDatoAdicionalDTO.getUsuarioModifica());
        valorDatoAdicional.setUsuarioCrea(valorDatoAdicionalDTO.getUsuarioCrea());
        valorDatoAdicional.setFechaCrea(valorDatoAdicionalDTO.getFechaCrea());
    
    	return valorDatoAdicional;
    
    }

}
