package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EstadoViaDaoAPI;
import co.com.fspb.mgs.dao.model.EstadoVia;
import co.com.fspb.mgs.service.api.EstadoViaServiceAPI;
import co.com.fspb.mgs.dto.EstadoViaDTO;

/**
 * Implementación de la Interfaz {@link EstadoViaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstadoViaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EstadoViaServiceImpl implements EstadoViaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EstadoViaDaoAPI estadoViaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EstadoViaDTO estadoViaDTO) throws PmzException {

        EstadoVia estadoViaValidate = estadoViaDao.get(estadoViaDTO.getId());

        if (estadoViaValidate != null) {
            throw new PmzException("El estadoVia ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        EstadoVia estadoVia = DTOTransformer.getEstadoViaFromEstadoViaDTO(new EstadoVia(), estadoViaDTO);

        estadoViaDao.save(estadoVia);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstadoViaDTO> getRecords(PmzPagingCriteria criteria) {
        List<EstadoVia> listaReturn = estadoViaDao.getRecords(criteria);
        Long countTotalRegistros = estadoViaDao.countRecords(criteria);

        List<EstadoViaDTO> resultList = new ArrayList<EstadoViaDTO>();
        for (EstadoVia estadoVia : listaReturn) {
            EstadoViaDTO estadoViaDTO = DTOTransformer
                    .getEstadoViaDTOFromEstadoVia(estadoVia);
            resultList.add(estadoViaDTO);
        }
        
        return new PmzResultSet<EstadoViaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EstadoViaDTO estadoViaDTO) throws PmzException{

        EstadoVia estadoVia = estadoViaDao.get(estadoViaDTO.getId());

        if (estadoVia == null) {
            throw new PmzException("El estadoVia no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEstadoViaFromEstadoViaDTO(estadoVia, estadoViaDTO);

        estadoViaDao.update(estadoVia);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EstadoViaDTO getEstadoViaDTO(Long id) {
        EstadoVia estadoVia = estadoViaDao.get(id);
        return DTOTransformer.getEstadoViaDTOFromEstadoVia(estadoVia);
    }
}
