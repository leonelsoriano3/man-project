package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ProyectoBeneficiarioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link ProyectoBeneficiario}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ProyectoBeneficiarioServiceAPI
 * @date nov 11, 2016
 */
public interface ProyectoBeneficiarioServiceAPI {

    /**
	 * Registra una entidad {@link ProyectoBeneficiario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoBeneficiario - {@link ProyectoBeneficiarioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(ProyectoBeneficiarioDTO proyectoBeneficiario) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link ProyectoBeneficiario} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param proyectoBeneficiario - {@link ProyectoBeneficiarioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(ProyectoBeneficiarioDTO proyectoBeneficiario) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<ProyectoBeneficiarioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link ProyectoBeneficiarioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Integer
     * @return {@link ProyectoBeneficiarioDTO}
     */
    ProyectoBeneficiarioDTO getProyectoBeneficiarioDTO(Integer id);

}
