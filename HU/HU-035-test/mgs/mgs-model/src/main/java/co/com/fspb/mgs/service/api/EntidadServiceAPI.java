package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EntidadDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Entidad}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadServiceAPI
 * @date nov 11, 2016
 */
public interface EntidadServiceAPI {

    /**
	 * Registra una entidad {@link Entidad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidad - {@link EntidadDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(EntidadDTO entidad) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Entidad} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param entidad - {@link EntidadDTO}
	 * @throws {@link PmzException}
	 */
	void editar(EntidadDTO entidad) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<EntidadDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link EntidadDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link EntidadDTO}
     */
    EntidadDTO getEntidadDTO(Long id);

}
