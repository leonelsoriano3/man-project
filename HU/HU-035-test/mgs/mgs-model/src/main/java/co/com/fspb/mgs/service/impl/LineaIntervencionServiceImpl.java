package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.LineaIntervencionDaoAPI;
import co.com.fspb.mgs.dao.model.LineaIntervencion;
import co.com.fspb.mgs.service.api.LineaIntervencionServiceAPI;
import co.com.fspb.mgs.dto.LineaIntervencionDTO;

/**
 * Implementación de la Interfaz {@link LineaIntervencionServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LineaIntervencionServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class LineaIntervencionServiceImpl implements LineaIntervencionServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private LineaIntervencionDaoAPI lineaIntervencionDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(LineaIntervencionDTO lineaIntervencionDTO) throws PmzException {

        LineaIntervencion lineaIntervencionValidate = lineaIntervencionDao.get(lineaIntervencionDTO.getId());

        if (lineaIntervencionValidate != null) {
            throw new PmzException("El lineaIntervencion ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        LineaIntervencion lineaIntervencion = DTOTransformer.getLineaIntervencionFromLineaIntervencionDTO(new LineaIntervencion(), lineaIntervencionDTO);

        lineaIntervencionDao.save(lineaIntervencion);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<LineaIntervencionDTO> getRecords(PmzPagingCriteria criteria) {
        List<LineaIntervencion> listaReturn = lineaIntervencionDao.getRecords(criteria);
        Long countTotalRegistros = lineaIntervencionDao.countRecords(criteria);

        List<LineaIntervencionDTO> resultList = new ArrayList<LineaIntervencionDTO>();
        for (LineaIntervencion lineaIntervencion : listaReturn) {
            LineaIntervencionDTO lineaIntervencionDTO = DTOTransformer
                    .getLineaIntervencionDTOFromLineaIntervencion(lineaIntervencion);
            resultList.add(lineaIntervencionDTO);
        }
        
        return new PmzResultSet<LineaIntervencionDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(LineaIntervencionDTO lineaIntervencionDTO) throws PmzException{

        LineaIntervencion lineaIntervencion = lineaIntervencionDao.get(lineaIntervencionDTO.getId());

        if (lineaIntervencion == null) {
            throw new PmzException("El lineaIntervencion no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getLineaIntervencionFromLineaIntervencionDTO(lineaIntervencion, lineaIntervencionDTO);

        lineaIntervencionDao.update(lineaIntervencion);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public LineaIntervencionDTO getLineaIntervencionDTO(Long id) {
        LineaIntervencion lineaIntervencion = lineaIntervencionDao.get(id);
        return DTOTransformer.getLineaIntervencionDTOFromLineaIntervencion(lineaIntervencion);
    }
}
