package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;
import co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI;
import co.com.fspb.mgs.service.api.MaterialViviendaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link MaterialViviendaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MaterialViviendaFacadeImpl
 * @date nov 11, 2016
 */
@Service("materialViviendaFacade")
public class MaterialViviendaFacadeImpl implements MaterialViviendaFacadeAPI {

    @Autowired
    private MaterialViviendaServiceAPI materialViviendaService;

    /**
     * @see co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<MaterialViviendaDTO> getRecords(PmzPagingCriteria criteria) {
        return materialViviendaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI#guardar()
     */
    @Override
    public void guardar(MaterialViviendaDTO materialVivienda) throws PmzException {
        materialViviendaService.guardar(materialVivienda);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI#editar()
     */
    @Override
    public void editar(MaterialViviendaDTO materialVivienda) throws PmzException {
        materialViviendaService.editar(materialVivienda);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.MaterialViviendaFacadeAPI#getMaterialViviendaDTO()
     */
    @Override
    public MaterialViviendaDTO getMaterialViviendaDTO(Long id) {
        return materialViviendaService.getMaterialViviendaDTO(id);
    }

}
