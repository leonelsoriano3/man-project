package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.DescIndependienteDaoAPI;
import co.com.fspb.mgs.dao.model.DescIndependiente;
import co.com.fspb.mgs.service.api.DescIndependienteServiceAPI;
import co.com.fspb.mgs.dto.DescIndependienteDTO;

/**
 * Implementación de la Interfaz {@link DescIndependienteServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DescIndependienteServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class DescIndependienteServiceImpl implements DescIndependienteServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private DescIndependienteDaoAPI descIndependienteDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(DescIndependienteDTO descIndependienteDTO) throws PmzException {

        DescIndependiente descIndependienteValidate = descIndependienteDao.get(descIndependienteDTO.getId());

        if (descIndependienteValidate != null) {
            throw new PmzException("El descIndependiente ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        DescIndependiente descIndependiente = DTOTransformer.getDescIndependienteFromDescIndependienteDTO(new DescIndependiente(), descIndependienteDTO);

        descIndependienteDao.save(descIndependiente);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<DescIndependienteDTO> getRecords(PmzPagingCriteria criteria) {
        List<DescIndependiente> listaReturn = descIndependienteDao.getRecords(criteria);
        Long countTotalRegistros = descIndependienteDao.countRecords(criteria);

        List<DescIndependienteDTO> resultList = new ArrayList<DescIndependienteDTO>();
        for (DescIndependiente descIndependiente : listaReturn) {
            DescIndependienteDTO descIndependienteDTO = DTOTransformer
                    .getDescIndependienteDTOFromDescIndependiente(descIndependiente);
            resultList.add(descIndependienteDTO);
        }
        
        return new PmzResultSet<DescIndependienteDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(DescIndependienteDTO descIndependienteDTO) throws PmzException{

        DescIndependiente descIndependiente = descIndependienteDao.get(descIndependienteDTO.getId());

        if (descIndependiente == null) {
            throw new PmzException("El descIndependiente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getDescIndependienteFromDescIndependienteDTO(descIndependiente, descIndependienteDTO);

        descIndependienteDao.update(descIndependiente);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public DescIndependienteDTO getDescIndependienteDTO(Long id) {
        DescIndependiente descIndependiente = descIndependienteDao.get(id);
        return DTOTransformer.getDescIndependienteDTOFromDescIndependiente(descIndependiente);
    }
}
