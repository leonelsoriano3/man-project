package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolFundacionDTO;
import co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI;
import co.com.fspb.mgs.service.api.RolFundacionServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link RolFundacionFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolFundacionFacadeImpl
 * @date nov 11, 2016
 */
@Service("rolFundacionFacade")
public class RolFundacionFacadeImpl implements RolFundacionFacadeAPI {

    @Autowired
    private RolFundacionServiceAPI rolFundacionService;

    /**
     * @see co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolFundacionDTO> getRecords(PmzPagingCriteria criteria) {
        return rolFundacionService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI#guardar()
     */
    @Override
    public void guardar(RolFundacionDTO rolFundacion) throws PmzException {
        rolFundacionService.guardar(rolFundacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI#editar()
     */
    @Override
    public void editar(RolFundacionDTO rolFundacion) throws PmzException {
        rolFundacionService.editar(rolFundacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolFundacionFacadeAPI#getRolFundacionDTO()
     */
    @Override
    public RolFundacionDTO getRolFundacionDTO(Long id) {
        return rolFundacionService.getRolFundacionDTO(id);
    }

}
