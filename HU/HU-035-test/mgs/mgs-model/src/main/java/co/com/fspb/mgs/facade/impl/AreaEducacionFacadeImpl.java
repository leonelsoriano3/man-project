package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.AreaEducacionDTO;
import co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI;
import co.com.fspb.mgs.service.api.AreaEducacionServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link AreaEducacionFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class AreaEducacionFacadeImpl
 * @date nov 11, 2016
 */
@Service("areaEducacionFacade")
public class AreaEducacionFacadeImpl implements AreaEducacionFacadeAPI {

    @Autowired
    private AreaEducacionServiceAPI areaEducacionService;

    /**
     * @see co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<AreaEducacionDTO> getRecords(PmzPagingCriteria criteria) {
        return areaEducacionService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI#guardar()
     */
    @Override
    public void guardar(AreaEducacionDTO areaEducacion) throws PmzException {
        areaEducacionService.guardar(areaEducacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI#editar()
     */
    @Override
    public void editar(AreaEducacionDTO areaEducacion) throws PmzException {
        areaEducacionService.editar(areaEducacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.AreaEducacionFacadeAPI#getAreaEducacionDTO()
     */
    @Override
    public AreaEducacionDTO getAreaEducacionDTO(Long id) {
        return areaEducacionService.getAreaEducacionDTO(id);
    }

}
