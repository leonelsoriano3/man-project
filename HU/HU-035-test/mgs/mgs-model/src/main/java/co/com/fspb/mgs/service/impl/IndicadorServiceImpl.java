package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.IndicadorDaoAPI;
import co.com.fspb.mgs.dao.model.Indicador;
import co.com.fspb.mgs.service.api.IndicadorServiceAPI;
import co.com.fspb.mgs.dto.IndicadorDTO;

/**
 * Implementación de la Interfaz {@link IndicadorServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class IndicadorServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class IndicadorServiceImpl implements IndicadorServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private IndicadorDaoAPI indicadorDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(IndicadorDTO indicadorDTO) throws PmzException {

        Indicador indicadorValidate = indicadorDao.get(indicadorDTO.getId());

        if (indicadorValidate != null) {
            throw new PmzException("El indicador ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Indicador indicador = DTOTransformer.getIndicadorFromIndicadorDTO(new Indicador(), indicadorDTO);

        indicadorDao.save(indicador);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<IndicadorDTO> getRecords(PmzPagingCriteria criteria) {
        List<Indicador> listaReturn = indicadorDao.getRecords(criteria);
        Long countTotalRegistros = indicadorDao.countRecords(criteria);

        List<IndicadorDTO> resultList = new ArrayList<IndicadorDTO>();
        for (Indicador indicador : listaReturn) {
            IndicadorDTO indicadorDTO = DTOTransformer
                    .getIndicadorDTOFromIndicador(indicador);
            resultList.add(indicadorDTO);
        }
        
        return new PmzResultSet<IndicadorDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(IndicadorDTO indicadorDTO) throws PmzException{

        Indicador indicador = indicadorDao.get(indicadorDTO.getId());

        if (indicador == null) {
            throw new PmzException("El indicador no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getIndicadorFromIndicadorDTO(indicador, indicadorDTO);

        indicadorDao.update(indicador);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public IndicadorDTO getIndicadorDTO(Long id) {
        Indicador indicador = indicadorDao.get(id);
        return DTOTransformer.getIndicadorDTOFromIndicador(indicador);
    }
}
