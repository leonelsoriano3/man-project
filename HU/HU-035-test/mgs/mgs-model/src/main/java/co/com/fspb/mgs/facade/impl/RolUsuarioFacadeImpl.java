package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.RolUsuarioDTO;
import co.com.fspb.mgs.dto.RolDTO;
import co.com.fspb.mgs.dto.UsuarioDTO;
import co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI;
import co.com.fspb.mgs.service.api.RolUsuarioServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link RolUsuarioFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class RolUsuarioFacadeImpl
 * @date nov 11, 2016
 */
@Service("rolUsuarioFacade")
public class RolUsuarioFacadeImpl implements RolUsuarioFacadeAPI {

    @Autowired
    private RolUsuarioServiceAPI rolUsuarioService;

    /**
     * @see co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<RolUsuarioDTO> getRecords(PmzPagingCriteria criteria) {
        return rolUsuarioService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI#guardar()
     */
    @Override
    public void guardar(RolUsuarioDTO rolUsuario) throws PmzException {
        rolUsuarioService.guardar(rolUsuario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI#editar()
     */
    @Override
    public void editar(RolUsuarioDTO rolUsuario) throws PmzException {
        rolUsuarioService.editar(rolUsuario);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.RolUsuarioFacadeAPI#getRolUsuarioDTO()
     */
    @Override
    public RolUsuarioDTO getRolUsuarioDTO(RolDTO rol, UsuarioDTO usuario) {
        return rolUsuarioService.getRolUsuarioDTO(rol, usuario);
    }

}
