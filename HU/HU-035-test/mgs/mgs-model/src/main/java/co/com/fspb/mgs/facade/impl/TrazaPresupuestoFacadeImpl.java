package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.TrazaPresupuestoDTO;
import co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI;
import co.com.fspb.mgs.service.api.TrazaPresupuestoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link TrazaPresupuestoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class TrazaPresupuestoFacadeImpl
 * @date nov 11, 2016
 */
@Service("trazaPresupuestoFacade")
public class TrazaPresupuestoFacadeImpl implements TrazaPresupuestoFacadeAPI {

    @Autowired
    private TrazaPresupuestoServiceAPI trazaPresupuestoService;

    /**
     * @see co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<TrazaPresupuestoDTO> getRecords(PmzPagingCriteria criteria) {
        return trazaPresupuestoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI#guardar()
     */
    @Override
    public void guardar(TrazaPresupuestoDTO trazaPresupuesto) throws PmzException {
        trazaPresupuestoService.guardar(trazaPresupuesto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI#editar()
     */
    @Override
    public void editar(TrazaPresupuestoDTO trazaPresupuesto) throws PmzException {
        trazaPresupuestoService.editar(trazaPresupuesto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.TrazaPresupuestoFacadeAPI#getTrazaPresupuestoDTO()
     */
    @Override
    public TrazaPresupuestoDTO getTrazaPresupuestoDTO(Long id) {
        return trazaPresupuestoService.getTrazaPresupuestoDTO(id);
    }

}
