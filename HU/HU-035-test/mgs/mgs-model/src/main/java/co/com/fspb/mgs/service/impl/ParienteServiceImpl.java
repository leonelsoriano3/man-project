package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ParienteDaoAPI;
import co.com.fspb.mgs.dao.api.ParentescoDaoAPI;
import co.com.fspb.mgs.dao.model.Parentesco;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.api.BeneficiarioDaoAPI;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dao.model.Pariente;
import co.com.fspb.mgs.dao.model.ParientePK;
import co.com.fspb.mgs.dao.model.Beneficiario;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.service.api.ParienteServiceAPI;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.dto.BeneficiarioDTO;
import co.com.fspb.mgs.dto.PersonaDTO;

/**
 * Implementación de la Interfaz {@link ParienteServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ParienteServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ParienteServiceImpl implements ParienteServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ParienteDaoAPI parienteDao;
    @Autowired
    private ParentescoDaoAPI parentescoDao;
    @Autowired
    private PersonaDaoAPI personaDao;
    @Autowired
    private BeneficiarioDaoAPI beneficiarioDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ParienteDTO parienteDTO) throws PmzException {


		ParientePK pk = new ParientePK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), parienteDTO.getBeneficiarioDTO()));
        pk.setPersona(DTOTransformer.getPersonaFromPersonaDTO(new Persona(), parienteDTO.getPersonaDTO()));
        Pariente parienteValidate = parienteDao.get(pk);

        if (parienteValidate != null) {
            throw new PmzException("El pariente ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Pariente pariente = DTOTransformer.getParienteFromParienteDTO(new Pariente(), parienteDTO);

		pk = pariente.getId();
		if (parienteDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(parienteDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setBeneficiario(beneficiario);
		}
		
		if (parienteDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(parienteDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setPersona(persona);
		}
		
        pariente.setId(pk);
        
		if (parienteDTO.getParentescoDTO() != null) {
       		Parentesco parentesco = parentescoDao.get(parienteDTO.getParentescoDTO().getId());
       		if (parentesco == null) {
                throw new PmzException("El Parentesco no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pariente.setParentesco(parentesco);
		}
		
        parienteDao.save(pariente);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ParienteDTO> getRecords(PmzPagingCriteria criteria) {
        List<Pariente> listaReturn = parienteDao.getRecords(criteria);
        Long countTotalRegistros = parienteDao.countRecords(criteria);

        List<ParienteDTO> resultList = new ArrayList<ParienteDTO>();
        for (Pariente pariente : listaReturn) {
            ParienteDTO parienteDTO = DTOTransformer
                    .getParienteDTOFromPariente(pariente);
            resultList.add(parienteDTO);
        }
        
        return new PmzResultSet<ParienteDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ParienteDTO parienteDTO) throws PmzException{


		ParientePK pk = new ParientePK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), parienteDTO.getBeneficiarioDTO()));
        pk.setPersona(DTOTransformer.getPersonaFromPersonaDTO(new Persona(), parienteDTO.getPersonaDTO()));
        Pariente pariente = parienteDao.get(pk);

        if (pariente == null) {
            throw new PmzException("El pariente no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getParienteFromParienteDTO(pariente, parienteDTO);

		pk = pariente.getId();
		if (parienteDTO.getBeneficiarioDTO() != null) {
       		Beneficiario beneficiario = beneficiarioDao.get(parienteDTO.getBeneficiarioDTO().getId());
       		if (beneficiario == null) {
                throw new PmzException("El Beneficiario no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setBeneficiario(beneficiario);
		}
		
		if (parienteDTO.getPersonaDTO() != null) {
       		Persona persona = personaDao.get(parienteDTO.getPersonaDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pk.setPersona(persona);
		}
		
        pariente.setId(pk);
        
		if (parienteDTO.getParentescoDTO() != null) {
       		Parentesco parentesco = parentescoDao.get(parienteDTO.getParentescoDTO().getId());
       		if (parentesco == null) {
                throw new PmzException("El Parentesco no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		pariente.setParentesco(parentesco);
		}
		
        parienteDao.update(pariente);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ParienteDTO getParienteDTO(BeneficiarioDTO beneficiario, PersonaDTO persona) {

		ParientePK pk = new ParientePK();
        pk.setBeneficiario(DTOTransformer.getBeneficiarioFromBeneficiarioDTO(new Beneficiario(), beneficiario));
        pk.setPersona(DTOTransformer.getPersonaFromPersonaDTO(new Persona(), persona));
        Pariente pariente = parienteDao.get(pk);
        return DTOTransformer.getParienteDTOFromPariente(pariente);
    }
}
