package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.EntidadDaoAPI;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.api.PersonaDaoAPI;
import co.com.fspb.mgs.dao.model.Persona;
import co.com.fspb.mgs.dao.model.Entidad;
import co.com.fspb.mgs.service.api.EntidadServiceAPI;
import co.com.fspb.mgs.dto.EntidadDTO;

/**
 * Implementación de la Interfaz {@link EntidadServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EntidadServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class EntidadServiceImpl implements EntidadServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private EntidadDaoAPI entidadDao;
    @Autowired
    private PersonaDaoAPI personaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(EntidadDTO entidadDTO) throws PmzException {

        Entidad entidadValidate = entidadDao.get(entidadDTO.getId());

        if (entidadValidate != null) {
            throw new PmzException("El entidad ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Entidad entidad = DTOTransformer.getEntidadFromEntidadDTO(new Entidad(), entidadDTO);

		if (entidadDTO.getContactoDTO() != null) {
       		Persona persona = personaDao.get(entidadDTO.getContactoDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		entidad.setContacto(persona);
		}
		
		if (entidadDTO.getContactoDTO() != null) {
       		Persona persona = personaDao.get(entidadDTO.getContactoDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		entidad.setContacto(persona);
		}
		
        entidadDao.save(entidad);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<EntidadDTO> getRecords(PmzPagingCriteria criteria) {
        List<Entidad> listaReturn = entidadDao.getRecords(criteria);
        Long countTotalRegistros = entidadDao.countRecords(criteria);

        List<EntidadDTO> resultList = new ArrayList<EntidadDTO>();
        for (Entidad entidad : listaReturn) {
            EntidadDTO entidadDTO = DTOTransformer
                    .getEntidadDTOFromEntidad(entidad);
            resultList.add(entidadDTO);
        }
        
        return new PmzResultSet<EntidadDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(EntidadDTO entidadDTO) throws PmzException{

        Entidad entidad = entidadDao.get(entidadDTO.getId());

        if (entidad == null) {
            throw new PmzException("El entidad no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getEntidadFromEntidadDTO(entidad, entidadDTO);

		if (entidadDTO.getContactoDTO() != null) {
       		Persona persona = personaDao.get(entidadDTO.getContactoDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		entidad.setContacto(persona);
		}
		
		if (entidadDTO.getContactoDTO() != null) {
       		Persona persona = personaDao.get(entidadDTO.getContactoDTO().getId());
       		if (persona == null) {
                throw new PmzException("El Persona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
            }
       		entidad.setContacto(persona);
		}
		
        entidadDao.update(entidad);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public EntidadDTO getEntidadDTO(Long id) {
        Entidad entidad = entidadDao.get(id);
        return DTOTransformer.getEntidadDTOFromEntidad(entidad);
    }
}
