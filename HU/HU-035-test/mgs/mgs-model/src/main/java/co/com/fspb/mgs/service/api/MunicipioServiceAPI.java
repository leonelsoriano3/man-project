package co.com.fspb.mgs.service.api;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.MunicipioDTO;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Interfaz de la lógica de negocio (Services) para la entidad {@link Municipio}.
 * La implementación de esta interfaz debe utilizar la capa de DAOs para acceso a datos.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MunicipioServiceAPI
 * @date nov 11, 2016
 */
public interface MunicipioServiceAPI {

    /**
	 * Registra una entidad {@link Municipio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param municipio - {@link MunicipioDTO}
	 * @throws {@link PmzException}
	 */
	void guardar(MunicipioDTO municipio) throws PmzException;
	
    /**
	 * Actualiza una entidad {@link Municipio} en la Base de Datos.
	 *
	 * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
	 * @param municipio - {@link MunicipioDTO}
	 * @throws {@link PmzException}
	 */
	void editar(MunicipioDTO municipio) throws PmzException;

    /**
     * Metodo de consulta paginada que retorna un objeto {@link PmzResultSet}.
     * Este contiene la lista y el total de registros en la tabla.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param criteria - {@link PmzPagingCriteria}
     * @return {@link PmzResultSet}
     */
    PmzResultSet<MunicipioDTO> getRecords(PmzPagingCriteria criteria);

	/**
     * Metodo que obtiene un {@link MunicipioDTO} a partir de su llave primaria.
     *
     * @author PMZ - Premize S.A.S
	 * @date nov 11, 2016
     * @param id - Long
     * @return {@link MunicipioDTO}
     */
    MunicipioDTO getMunicipioDTO(Long id);

}
