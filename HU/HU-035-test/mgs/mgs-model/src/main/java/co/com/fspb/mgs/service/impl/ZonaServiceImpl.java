package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.ZonaDaoAPI;
import co.com.fspb.mgs.dao.model.Zona;
import co.com.fspb.mgs.service.api.ZonaServiceAPI;
import co.com.fspb.mgs.dto.ZonaDTO;

/**
 * Implementación de la Interfaz {@link ZonaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ZonaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class ZonaServiceImpl implements ZonaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private ZonaDaoAPI zonaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(ZonaDTO zonaDTO) throws PmzException {

        Zona zonaValidate = zonaDao.get(zonaDTO.getId());

        if (zonaValidate != null) {
            throw new PmzException("El zona ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        Zona zona = DTOTransformer.getZonaFromZonaDTO(new Zona(), zonaDTO);

        zonaDao.save(zona);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<ZonaDTO> getRecords(PmzPagingCriteria criteria) {
        List<Zona> listaReturn = zonaDao.getRecords(criteria);
        Long countTotalRegistros = zonaDao.countRecords(criteria);

        List<ZonaDTO> resultList = new ArrayList<ZonaDTO>();
        for (Zona zona : listaReturn) {
            ZonaDTO zonaDTO = DTOTransformer
                    .getZonaDTOFromZona(zona);
            resultList.add(zonaDTO);
        }
        
        return new PmzResultSet<ZonaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(ZonaDTO zonaDTO) throws PmzException{

        Zona zona = zonaDao.get(zonaDTO.getId());

        if (zona == null) {
            throw new PmzException("El zona no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getZonaFromZonaDTO(zona, zonaDTO);

        zonaDao.update(zona);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public ZonaDTO getZonaDTO(Long id) {
        Zona zona = zonaDao.get(id);
        return DTOTransformer.getZonaDTOFromZona(zona);
    }
}
