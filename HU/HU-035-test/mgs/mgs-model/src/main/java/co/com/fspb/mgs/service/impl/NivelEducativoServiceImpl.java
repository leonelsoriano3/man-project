package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.NivelEducativoDaoAPI;
import co.com.fspb.mgs.dao.model.NivelEducativo;
import co.com.fspb.mgs.service.api.NivelEducativoServiceAPI;
import co.com.fspb.mgs.dto.NivelEducativoDTO;

/**
 * Implementación de la Interfaz {@link NivelEducativoServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class NivelEducativoServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class NivelEducativoServiceImpl implements NivelEducativoServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private NivelEducativoDaoAPI nivelEducativoDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(NivelEducativoDTO nivelEducativoDTO) throws PmzException {

        NivelEducativo nivelEducativoValidate = nivelEducativoDao.get(nivelEducativoDTO.getId());

        if (nivelEducativoValidate != null) {
            throw new PmzException("El nivelEducativo ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        NivelEducativo nivelEducativo = DTOTransformer.getNivelEducativoFromNivelEducativoDTO(new NivelEducativo(), nivelEducativoDTO);

        nivelEducativoDao.save(nivelEducativo);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<NivelEducativoDTO> getRecords(PmzPagingCriteria criteria) {
        List<NivelEducativo> listaReturn = nivelEducativoDao.getRecords(criteria);
        Long countTotalRegistros = nivelEducativoDao.countRecords(criteria);

        List<NivelEducativoDTO> resultList = new ArrayList<NivelEducativoDTO>();
        for (NivelEducativo nivelEducativo : listaReturn) {
            NivelEducativoDTO nivelEducativoDTO = DTOTransformer
                    .getNivelEducativoDTOFromNivelEducativo(nivelEducativo);
            resultList.add(nivelEducativoDTO);
        }
        
        return new PmzResultSet<NivelEducativoDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(NivelEducativoDTO nivelEducativoDTO) throws PmzException{

        NivelEducativo nivelEducativo = nivelEducativoDao.get(nivelEducativoDTO.getId());

        if (nivelEducativo == null) {
            throw new PmzException("El nivelEducativo no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getNivelEducativoFromNivelEducativoDTO(nivelEducativo, nivelEducativoDTO);

        nivelEducativoDao.update(nivelEducativo);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public NivelEducativoDTO getNivelEducativoDTO(Long id) {
        NivelEducativo nivelEducativo = nivelEducativoDao.get(id);
        return DTOTransformer.getNivelEducativoDTOFromNivelEducativo(nivelEducativo);
    }
}
