package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ActividadDTO;
import co.com.fspb.mgs.facade.api.ActividadFacadeAPI;
import co.com.fspb.mgs.service.api.ActividadServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ActividadFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ActividadFacadeImpl
 * @date nov 11, 2016
 */
@Service("actividadFacade")
public class ActividadFacadeImpl implements ActividadFacadeAPI {

    @Autowired
    private ActividadServiceAPI actividadService;

    /**
     * @see co.com.fspb.mgs.facade.api.ActividadFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ActividadDTO> getRecords(PmzPagingCriteria criteria) {
        return actividadService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ActividadFacadeAPI#guardar()
     */
    @Override
    public void guardar(ActividadDTO actividad) throws PmzException {
        actividadService.guardar(actividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ActividadFacadeAPI#editar()
     */
    @Override
    public void editar(ActividadDTO actividad) throws PmzException {
        actividadService.editar(actividad);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ActividadFacadeAPI#getActividadDTO()
     */
    @Override
    public ActividadDTO getActividadDTO(Long id) {
        return actividadService.getActividadDTO(id);
    }

}
