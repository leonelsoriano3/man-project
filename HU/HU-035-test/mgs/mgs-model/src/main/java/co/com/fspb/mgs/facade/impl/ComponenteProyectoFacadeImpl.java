package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ComponenteProyectoDTO;
import co.com.fspb.mgs.dto.ProyectoDTO;
import co.com.fspb.mgs.dto.ComponenteDTO;
import co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI;
import co.com.fspb.mgs.service.api.ComponenteProyectoServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ComponenteProyectoFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ComponenteProyectoFacadeImpl
 * @date nov 11, 2016
 */
@Service("componenteProyectoFacade")
public class ComponenteProyectoFacadeImpl implements ComponenteProyectoFacadeAPI {

    @Autowired
    private ComponenteProyectoServiceAPI componenteProyectoService;

    /**
     * @see co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ComponenteProyectoDTO> getRecords(PmzPagingCriteria criteria) {
        return componenteProyectoService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI#guardar()
     */
    @Override
    public void guardar(ComponenteProyectoDTO componenteProyecto) throws PmzException {
        componenteProyectoService.guardar(componenteProyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI#editar()
     */
    @Override
    public void editar(ComponenteProyectoDTO componenteProyecto) throws PmzException {
        componenteProyectoService.editar(componenteProyecto);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ComponenteProyectoFacadeAPI#getComponenteProyectoDTO()
     */
    @Override
    public ComponenteProyectoDTO getComponenteProyectoDTO(ProyectoDTO proyecto, ComponenteDTO componente) {
        return componenteProyectoService.getComponenteProyectoDTO(proyecto, componente);
    }

}
