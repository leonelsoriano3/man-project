package co.com.fspb.mgs.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Mapeo de la tabla mgs_dato_adicional en la BD a la entidad DatoAdicional.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class DatoAdicional
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_dato_adicional" )
public class DatoAdicional implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_dato_adicional", nullable = false)
	private Long id; 

	@Column(name = "nombre_dato", nullable = false, length = 50)
    private String nombreDato; 


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proyecto", nullable = false)
    private Proyecto proyecto; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreDato() {
        return nombreDato;
    }

    public void setNombreDato(String nombreDato) {
        this.nombreDato = nombreDato;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

}
