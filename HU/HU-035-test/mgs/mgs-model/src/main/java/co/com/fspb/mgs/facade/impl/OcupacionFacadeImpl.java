package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.OcupacionDTO;
import co.com.fspb.mgs.facade.api.OcupacionFacadeAPI;
import co.com.fspb.mgs.service.api.OcupacionServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link OcupacionFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class OcupacionFacadeImpl
 * @date nov 11, 2016
 */
@Service("ocupacionFacade")
public class OcupacionFacadeImpl implements OcupacionFacadeAPI {

    @Autowired
    private OcupacionServiceAPI ocupacionService;

    /**
     * @see co.com.fspb.mgs.facade.api.OcupacionFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<OcupacionDTO> getRecords(PmzPagingCriteria criteria) {
        return ocupacionService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.OcupacionFacadeAPI#guardar()
     */
    @Override
    public void guardar(OcupacionDTO ocupacion) throws PmzException {
        ocupacionService.guardar(ocupacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.OcupacionFacadeAPI#editar()
     */
    @Override
    public void editar(OcupacionDTO ocupacion) throws PmzException {
        ocupacionService.editar(ocupacion);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.OcupacionFacadeAPI#getOcupacionDTO()
     */
    @Override
    public OcupacionDTO getOcupacionDTO(Long id) {
        return ocupacionService.getOcupacionDTO(id);
    }

}
