package co.com.fspb.mgs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.premize.pmz.api.exception.PmzErrorCode;
import com.premize.pmz.api.exception.PmzException;

import co.com.fspb.mgs.transform.DTOTransformer;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

import co.com.fspb.mgs.dao.api.MaterialViviendaDaoAPI;
import co.com.fspb.mgs.dao.model.MaterialVivienda;
import co.com.fspb.mgs.service.api.MaterialViviendaServiceAPI;
import co.com.fspb.mgs.dto.MaterialViviendaDTO;

/**
 * Implementación de la Interfaz {@link MaterialViviendaServiceAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class MaterialViviendaServiceImpl
 * @date nov 11, 2016
 */
@Service
@Transactional
public class MaterialViviendaServiceImpl implements MaterialViviendaServiceAPI {

    private static final String ENTIDAD_EXISTE = "MSG_ENTIDAD_EXISTE";
    private static final String ENTIDAD_NO_EXISTE = "MSG_ENTIDAD_NO_EXISTE";
    
    @Autowired
    private MaterialViviendaDaoAPI materialViviendaDao;

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#guardar()
     */
    @Override
    public void guardar(MaterialViviendaDTO materialViviendaDTO) throws PmzException {

        MaterialVivienda materialViviendaValidate = materialViviendaDao.get(materialViviendaDTO.getId());

        if (materialViviendaValidate != null) {
            throw new PmzException("El materialVivienda ya existe", new PmzErrorCode(ENTIDAD_EXISTE));
        }

        MaterialVivienda materialVivienda = DTOTransformer.getMaterialViviendaFromMaterialViviendaDTO(new MaterialVivienda(), materialViviendaDTO);

        materialViviendaDao.save(materialVivienda);

    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getRecords()
     */
    @Override
    public PmzResultSet<MaterialViviendaDTO> getRecords(PmzPagingCriteria criteria) {
        List<MaterialVivienda> listaReturn = materialViviendaDao.getRecords(criteria);
        Long countTotalRegistros = materialViviendaDao.countRecords(criteria);

        List<MaterialViviendaDTO> resultList = new ArrayList<MaterialViviendaDTO>();
        for (MaterialVivienda materialVivienda : listaReturn) {
            MaterialViviendaDTO materialViviendaDTO = DTOTransformer
                    .getMaterialViviendaDTOFromMaterialVivienda(materialVivienda);
            resultList.add(materialViviendaDTO);
        }
        
        return new PmzResultSet<MaterialViviendaDTO>(resultList,
                countTotalRegistros, listaReturn.size());
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#editar()
     */
    @Override
    public void editar(MaterialViviendaDTO materialViviendaDTO) throws PmzException{

        MaterialVivienda materialVivienda = materialViviendaDao.get(materialViviendaDTO.getId());

        if (materialVivienda == null) {
            throw new PmzException("El materialVivienda no existe", new PmzErrorCode(ENTIDAD_NO_EXISTE));
        }
       	
       	DTOTransformer.getMaterialViviendaFromMaterialViviendaDTO(materialVivienda, materialViviendaDTO);

        materialViviendaDao.update(materialVivienda);
        
    }

    /**
     * @see co.com.fspb.mgs.service.api.UsuarioProcesoServiceAPI#getUsuarioProcesoDTO()
     */
    @Override
    public MaterialViviendaDTO getMaterialViviendaDTO(Long id) {
        MaterialVivienda materialVivienda = materialViviendaDao.get(id);
        return DTOTransformer.getMaterialViviendaDTOFromMaterialVivienda(materialVivienda);
    }
}
