package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.dto.ObjetivoDTO;
import co.com.fspb.mgs.dto.IniciativaDTO;
import co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI;
import co.com.fspb.mgs.service.api.ObjetivoIniciativaServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link ObjetivoIniciativaFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class ObjetivoIniciativaFacadeImpl
 * @date nov 11, 2016
 */
@Service("objetivoIniciativaFacade")
public class ObjetivoIniciativaFacadeImpl implements ObjetivoIniciativaFacadeAPI {

    @Autowired
    private ObjetivoIniciativaServiceAPI objetivoIniciativaService;

    /**
     * @see co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<ObjetivoIniciativaDTO> getRecords(PmzPagingCriteria criteria) {
        return objetivoIniciativaService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI#guardar()
     */
    @Override
    public void guardar(ObjetivoIniciativaDTO objetivoIniciativa) throws PmzException {
        objetivoIniciativaService.guardar(objetivoIniciativa);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI#editar()
     */
    @Override
    public void editar(ObjetivoIniciativaDTO objetivoIniciativa) throws PmzException {
        objetivoIniciativaService.editar(objetivoIniciativa);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI#getObjetivoIniciativaDTO()
     */
    @Override
    public ObjetivoIniciativaDTO getObjetivoIniciativaDTO(ObjetivoDTO objetivo, IniciativaDTO iniciativa, Integer idUsuarioResp) {
        return objetivoIniciativaService.getObjetivoIniciativaDTO(objetivo, iniciativa, idUsuarioResp);
    }

}
