package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_actividad en la BD a la entidad Actividad.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Actividad
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_actividad" )
public class Actividad implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_actividad", nullable = false)
	private Long id; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_componente", nullable = true)
    private Componente componente; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

	@Column(name = "resultado_obtenido", nullable = false, length = 500)
    private String resultadoObtenido; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_estado", nullable = false)
    private EstadoActividad estadoActividad; 

	@Column(name = "observacion_seg", nullable = false, length = 500)
    private String observacionSeg; 

	@Column(name = "resultado_esperado", nullable = true, length = 500)
    private String resultadoEsperado; 

	@Column(name = "fecha_seguimiento", nullable = true, length = 13)
    private Date fechaSeguimiento; 

	@Column(name = "nombre", nullable = true, length = 50)
    private String nombre; 

	@Column(name = "recursos", nullable = true, length = 250)
    private String recursos; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

    @Column(name = "cant_asistentes", nullable = false, precision = 10, scale = 0)
    private Integer cantAsistentes; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_resp", nullable = true)
    private Usuario usuario; 

	@Column(name = "fecha_ejecucion", nullable = true, length = 13)
    private Date fechaEjecucion; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 


	@Column(name = "lecciones_aprendidas", nullable = false, length = 500)
    private String leccionesAprendidas; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Componente getComponente() {
        return componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public String getResultadoObtenido() {
        return resultadoObtenido;
    }

    public void setResultadoObtenido(String resultadoObtenido) {
        this.resultadoObtenido = resultadoObtenido;
    }

    public EstadoActividad getEstadoActividad() {
        return estadoActividad;
    }

    public void setEstadoActividad(EstadoActividad estadoActividad) {
        this.estadoActividad = estadoActividad;
    }

    public String getObservacionSeg() {
        return observacionSeg;
    }

    public void setObservacionSeg(String observacionSeg) {
        this.observacionSeg = observacionSeg;
    }

    public String getResultadoEsperado() {
        return resultadoEsperado;
    }

    public void setResultadoEsperado(String resultadoEsperado) {
        this.resultadoEsperado = resultadoEsperado;
    }

    public Date getFechaSeguimiento() {
        return fechaSeguimiento != null ? new Date(fechaSeguimiento.getTime()) : null;
    }

    public void setFechaSeguimiento(Date fechaSeguimiento) {
        this.fechaSeguimiento = fechaSeguimiento != null ? new Date(fechaSeguimiento.getTime()) : null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Integer getCantAsistentes() {
        return cantAsistentes;
    }

    public void setCantAsistentes(Integer cantAsistentes) {
        this.cantAsistentes = cantAsistentes;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getFechaEjecucion() {
        return fechaEjecucion != null ? new Date(fechaEjecucion.getTime()) : null;
    }

    public void setFechaEjecucion(Date fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion != null ? new Date(fechaEjecucion.getTime()) : null;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public String getLeccionesAprendidas() {
        return leccionesAprendidas;
    }

    public void setLeccionesAprendidas(String leccionesAprendidas) {
        this.leccionesAprendidas = leccionesAprendidas;
    }

}
