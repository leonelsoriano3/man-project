package co.com.fspb.mgs.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.dto.EstructuraFamiliarDTO;
import co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI;
import co.com.fspb.mgs.service.api.EstructuraFamiliarServiceAPI;
import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzResultSet;

/**
 * Implementación de la Interfaz {@link EstructuraFamiliarFacadeAPI}.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class EstructuraFamiliarFacadeImpl
 * @date nov 11, 2016
 */
@Service("estructuraFamiliarFacade")
public class EstructuraFamiliarFacadeImpl implements EstructuraFamiliarFacadeAPI {

    @Autowired
    private EstructuraFamiliarServiceAPI estructuraFamiliarService;

    /**
     * @see co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI#getRecords()
     */
    @Override
    public PmzResultSet<EstructuraFamiliarDTO> getRecords(PmzPagingCriteria criteria) {
        return estructuraFamiliarService.getRecords(criteria);
    }

    /**
     * @see co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI#guardar()
     */
    @Override
    public void guardar(EstructuraFamiliarDTO estructuraFamiliar) throws PmzException {
        estructuraFamiliarService.guardar(estructuraFamiliar);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI#editar()
     */
    @Override
    public void editar(EstructuraFamiliarDTO estructuraFamiliar) throws PmzException {
        estructuraFamiliarService.editar(estructuraFamiliar);
    }

	/**
     * @see co.com.fspb.mgs.facade.api.EstructuraFamiliarFacadeAPI#getEstructuraFamiliarDTO()
     */
    @Override
    public EstructuraFamiliarDTO getEstructuraFamiliarDTO(Long id) {
        return estructuraFamiliarService.getEstructuraFamiliarDTO(id);
    }

}
