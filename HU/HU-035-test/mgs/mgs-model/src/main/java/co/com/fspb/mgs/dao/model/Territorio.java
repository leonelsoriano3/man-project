package co.com.fspb.mgs.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Mapeo de la tabla mgs_territorio en la BD a la entidad Territorio.
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class Territorio
 * @date nov 11, 2016
 */
@Entity
@Table( name = "mgs_territorio" )
public class Territorio implements java.io.Serializable {

	//TODO PMZ-Generado co.com.fspb.mgs: Generar Serializable
    private static final long serialVersionUID = -1L;
    
	@Id
	@Column(name = "id_territorio", nullable = false)
	private Long id; 

	@Column(name = "fecha_modifica", nullable = false, length = 13)
    private Date fechaModifica; 

    @Column(name = "jovenes_hombres", nullable = false, precision = 10, scale = 0)
    private Integer jovenesHombres; 

    @Column(name = "jovenes_mujeres", nullable = false, precision = 10, scale = 0)
    private Integer jovenesMujeres; 

    @Column(name = "menores_mujeres", nullable = false, precision = 10, scale = 0)
    private Integer menoresMujeres; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_estado_via", nullable = false)
    private EstadoVia estadoVia; 


	@Column(name = "nombre", nullable = true, length = 50)
    private String nombre; 

    @Column(name = "adultos_mayores_hombres", nullable = false, precision = 10, scale = 0)
    private Integer adultosMayoresHombres; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_tipo", nullable = false)
    private TipoTerritorio tipoTerritorio; 

	@Column(name = "estado", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private EstadoEnum estado; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_problema_comunidad", nullable = false)
    private ProblemaComunidad problemaComunidad; 

	@Column(name = "usuario_modifica", nullable = false, length = 20)
    private String usuarioModifica; 

    @Column(name = "adultos_mayores_mujeres", nullable = false, precision = 10, scale = 0)
    private Integer adultosMayoresMujeres; 

	@Column(name = "usuario_crea", nullable = false, length = 20)
    private String usuarioCrea; 

    @Column(name = "menores_hombres", nullable = false, precision = 10, scale = 0)
    private Integer menoresHombres; 

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p_zona", nullable = false)
    private Zona zona; 

	@Column(name = "fecha_crea", nullable = false, length = 13)
    private Date fechaCrea; 
 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaModifica() {
        return fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica != null ? new Date(fechaModifica.getTime()) : null;
    }

    public Integer getJovenesHombres() {
        return jovenesHombres;
    }

    public void setJovenesHombres(Integer jovenesHombres) {
        this.jovenesHombres = jovenesHombres;
    }

    public Integer getJovenesMujeres() {
        return jovenesMujeres;
    }

    public void setJovenesMujeres(Integer jovenesMujeres) {
        this.jovenesMujeres = jovenesMujeres;
    }

    public Integer getMenoresMujeres() {
        return menoresMujeres;
    }

    public void setMenoresMujeres(Integer menoresMujeres) {
        this.menoresMujeres = menoresMujeres;
    }

    public EstadoVia getEstadoVia() {
        return estadoVia;
    }

    public void setEstadoVia(EstadoVia estadoVia) {
        this.estadoVia = estadoVia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getAdultosMayoresHombres() {
        return adultosMayoresHombres;
    }

    public void setAdultosMayoresHombres(Integer adultosMayoresHombres) {
        this.adultosMayoresHombres = adultosMayoresHombres;
    }

    public TipoTerritorio getTipoTerritorio() {
        return tipoTerritorio;
    }

    public void setTipoTerritorio(TipoTerritorio tipoTerritorio) {
        this.tipoTerritorio = tipoTerritorio;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public ProblemaComunidad getProblemaComunidad() {
        return problemaComunidad;
    }

    public void setProblemaComunidad(ProblemaComunidad problemaComunidad) {
        this.problemaComunidad = problemaComunidad;
    }

    public String getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Integer getAdultosMayoresMujeres() {
        return adultosMayoresMujeres;
    }

    public void setAdultosMayoresMujeres(Integer adultosMayoresMujeres) {
        this.adultosMayoresMujeres = adultosMayoresMujeres;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Integer getMenoresHombres() {
        return menoresHombres;
    }

    public void setMenoresHombres(Integer menoresHombres) {
        this.menoresHombres = menoresHombres;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public Date getFechaCrea() {
        return fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea != null ? new Date(fechaCrea.getTime()) : null;
    }

}
