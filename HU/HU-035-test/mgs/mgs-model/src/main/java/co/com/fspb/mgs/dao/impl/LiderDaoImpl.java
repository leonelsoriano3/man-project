package co.com.fspb.mgs.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.premize.pmz.dao.dto.Paginator;

import com.premize.pmz.dao.PmzAbstractDaoImpl;
import co.com.fspb.mgs.dao.api.LiderDaoAPI;
import co.com.fspb.mgs.dao.model.Lider;

import com.premize.pmz.api.dto.PmzPagingCriteria;
import com.premize.pmz.api.dto.PmzDateInterval;
import com.premize.pmz.api.dto.PmzSearch;
import com.premize.pmz.api.dto.PmzSortDirection;
import com.premize.pmz.api.dto.PmzSortField;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Implementación de la interfaz {@link LiderDaoAPI}. Extiende el DAO
 * abstracto de PMZ {@link PmzAbstractDaoImpl} para la implementacion de métodos
 * genéricos de consultas y transacciones
 * 
 * @author PMZ - Premize S.A.S
 * @project Modelo
 * @class LiderDaoImpl
 * @date nov 11, 2016
 */
@Repository
public class LiderDaoImpl extends PmzAbstractDaoImpl<Lider, Long>
        implements LiderDaoAPI {

    /**
	 * @see co.com.fspb.mgs.dao.api.AccionCalidadDaoAPI#getRecords()
	 */
    @Override
    public List<Lider> getRecords(PmzPagingCriteria criteria) {

        if (criteria == null) {
            throw new IllegalArgumentException(
                    "La propiedad PmzPagingCriteria no puede ser nula");
        }

        Integer displaySize = criteria.getDisplaySize();
        Integer displayStart = criteria.getDisplayStart();
        String searchLike = criteria.getSearch();
        List<PmzSearch> searchs = criteria.getSearchFields();
        List<PmzSortField> sortFields = criteria.getSortFields();
        List<PmzDateInterval> intervals = criteria.getDateInterval();

        // Consultar Registros
        DetachedCriteria findCriteria = createDetachedCriteriaFilter(
                searchLike, searchs, intervals);

        // Ordenar Campos
        if (sortFields != null && !sortFields.isEmpty()) {
            for (PmzSortField sortField : sortFields) {
                String field = sortField.getField();
                PmzSortDirection direction = sortField.getDirection();
                if (PmzSortDirection.ASC == direction) {
                    findCriteria.addOrder(Order.asc(field));
                } else {
                    findCriteria.addOrder(Order.desc(field));
                }
            }
        }

        return findByCriteria(findCriteria, new Paginator(displayStart,
                displaySize));
    }

    /**
	 * @see com.premize.pmz.dao.PmzAbstractDaoImpl#createDetachedCriteriaFilter()
	 */
    @Override
    public DetachedCriteria createDetachedCriteriaFilter(String searchLike,
            List<PmzSearch> searchs, List<PmzDateInterval> intervals) {
        DetachedCriteria cq = DetachedCriteria.forClass(Lider.class);

        if ((searchLike != null) && (!(searchLike.isEmpty()))) {
            Disjunction disyuctionConsulta = Restrictions.disjunction();
            //TODO PMZ-Generado co.com.fspb.mgs: En caso de ser necesario crear un filtro de "OR" basado en varios campos de la entidad
            cq.add(disyuctionConsulta);
        }
        buildSearchs(searchs, cq);
        buildDateIntervals(intervals, cq);
        return cq;
    }

    /**
	 * @see com.premize.pmz.dao.PmzAbstractDaoImpl#buildSearchs()
	 */
    @Override
    public void buildSearchs(List<PmzSearch> searchs, DetachedCriteria cq) {
        if (searchs == null || searchs.isEmpty()) {
            return;
        }
        Lider entity = new Lider();
        for (PmzSearch search : searchs) {
            String field = search.getField();
            Object value = search.getValue();

            if (field != null && !field.isEmpty() && value != null) {
                    if ("estadoDesc".equalsIgnoreCase(field)) {
                        cq.add(Restrictions.eq("estado", EstadoEnum.valueOf(value.toString())));
                   } else if ("persona.nombres".equalsIgnoreCase(field)) {
                        DetachedCriteria criteria = cq.createAlias("persona", "persona");
					    criteria.add(Restrictions.like("persona.nombres", value.toString().trim(), MatchMode.ANYWHERE).ignoreCase());			
                   } else {
                        addSimpleCriteria(cq, entity, field, value);
                   }
            }
        }
    }

}
