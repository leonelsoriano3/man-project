package co.com.fspb.mgs.view.util;

/**
 * Enumeración de utilidades para el proyecto de presentación
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ConstantesWeb
 * @date nov 11, 2016
 */
public enum ConstantesWeb {
	
    ERROR_PROCESO("FAIL"),
    PROCESO_EXITOSO("SUCCESS"),
    MSG_GUARDAR("MSG_GUARDAR"),
    MSG_GUARDAR_CODIGO("MSG_GUARDAR_CODIGO"),
    MSG_ACTUALIZAR_CODIGO("MSG_ACTUALIZAR_CODIGO"),
    MSG_EDITAR("MSG_ACTUALIZAR");
	
	private String value;
    
    private ConstantesWeb(String value) {
       this.value = value;
    }

    public String getValue() {
        return value;
    }
}
