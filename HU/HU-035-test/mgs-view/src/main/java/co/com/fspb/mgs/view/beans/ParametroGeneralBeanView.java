package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ParametroGeneralLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ParametroGeneralDTO;
import co.com.fspb.mgs.facade.api.ParametroGeneralFacadeAPI;

import co.com.fspb.mgs.enums.TipoParametroEnum;
import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link ParametroGeneral}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ParametroGeneralBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ParametroGeneralBeanView extends AbstractManagedBeanBase<ParametroGeneralDTO> {

    private static final String FORM_PG_FORM = "formParametroGeneral:pgForm";
    private static final String I18N_MSG = "msgParametroGeneral";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-parametroGeneral";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ParametroGeneralBeanView.class);

    private List<SelectItem> tipoParametroList; 
    private List<SelectItem> tipoParametroFilter; 
    
    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ParametroGeneralLazyList lazyDataModel;


    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{parametroGeneralFacade}")
    private ParametroGeneralFacadeAPI parametroGeneralFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        tipoParametroList = ManagedBeanUtils
                .convertirSelectItem(TipoParametroEnum.class);
        tipoParametroFilter = ManagedBeanUtils.convertirSelectItem(
                TipoParametroEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblTipoParametro"));
    
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new ParametroGeneralDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ParametroGeneralLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            parametroGeneralFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            parametroGeneralFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ParametroGeneralDTO parametroGeneralDTO) {
        setBaseDTO(parametroGeneralDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ParametroGeneralDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ParametroGeneralFacadeAPI getParametroGeneralFacade() {
        return parametroGeneralFacade;
    }

    public void setParametroGeneralFacade(ParametroGeneralFacadeAPI parametroGeneralFacade) {
        this.parametroGeneralFacade = parametroGeneralFacade;
    }

    public ParametroGeneralDTO getSelectedParametroGeneral() {
        return getBaseDTO();
    }
    
    public ParametroGeneralLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ParametroGeneralLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getTipoParametroList() {
        return tipoParametroList;
    }

    public void setTipoParametroList(List<SelectItem> tipoParametroList) {
        this.tipoParametroList = tipoParametroList;
    }

	public List<SelectItem> getTipoParametroFilter() {
        return tipoParametroFilter;
    }

    public void setTipoParametroFilter(List<SelectItem> tipoParametroFilter) {
        this.tipoParametroFilter = tipoParametroFilter;
    }

	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
