package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ObjetivoIniciativaLazyList;
import co.com.fspb.mgs.view.lazylist.ObjetivoLazyList;
import co.com.fspb.mgs.view.lazylist.IniciativaLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ObjetivoIniciativaDTO;
import co.com.fspb.mgs.facade.api.ObjetivoIniciativaFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link ObjetivoIniciativa}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ObjetivoIniciativaBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ObjetivoIniciativaBeanView extends AbstractManagedBeanBase<ObjetivoIniciativaDTO> {

    private static final String FORM_PG_FORM = "formObjetivoIniciativa:pgForm";
    private static final String I18N_MSG = "msgObjetivoIniciativa";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-objetivoIniciativa";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ObjetivoIniciativaBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ObjetivoIniciativaLazyList lazyDataModel;

    private ObjetivoLazyList objetivoLazyDataModel; 
    private IniciativaLazyList iniciativaLazyDataModel; 


    @ManagedProperty("#{objetivoIniciativaFacade}")
    private ObjetivoIniciativaFacadeAPI objetivoIniciativaFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new ObjetivoIniciativaDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ObjetivoIniciativaLazyList();
        objetivoLazyDataModel = new ObjetivoLazyList();
        iniciativaLazyDataModel = new IniciativaLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            objetivoIniciativaFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            objetivoIniciativaFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ObjetivoIniciativaDTO objetivoIniciativaDTO) {
        setBaseDTO(objetivoIniciativaDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ObjetivoIniciativaDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ObjetivoIniciativaFacadeAPI getObjetivoIniciativaFacade() {
        return objetivoIniciativaFacade;
    }

    public void setObjetivoIniciativaFacade(ObjetivoIniciativaFacadeAPI objetivoIniciativaFacade) {
        this.objetivoIniciativaFacade = objetivoIniciativaFacade;
    }

    public ObjetivoIniciativaDTO getSelectedObjetivoIniciativa() {
        return getBaseDTO();
    }
    
    public ObjetivoIniciativaLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ObjetivoIniciativaLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public ObjetivoLazyList getObjetivoLazyDataModel() {
        return objetivoLazyDataModel;
    }

    public void setObjetivoLazyDataModel(ObjetivoLazyList objetivoLazyDataModel) {
        this.objetivoLazyDataModel = objetivoLazyDataModel;
    }

	public IniciativaLazyList getIniciativaLazyDataModel() {
        return iniciativaLazyDataModel;
    }

    public void setIniciativaLazyDataModel(IniciativaLazyList iniciativaLazyDataModel) {
        this.iniciativaLazyDataModel = iniciativaLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
