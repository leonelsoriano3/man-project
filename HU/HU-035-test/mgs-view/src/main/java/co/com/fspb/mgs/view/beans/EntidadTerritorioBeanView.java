package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.EntidadTerritorioLazyList;
import co.com.fspb.mgs.view.lazylist.EntidadLazyList;
import co.com.fspb.mgs.view.lazylist.TerritorioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.EntidadTerritorioDTO;
import co.com.fspb.mgs.facade.api.EntidadTerritorioFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link EntidadTerritorio}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class EntidadTerritorioBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class EntidadTerritorioBeanView extends AbstractManagedBeanBase<EntidadTerritorioDTO> {

    private static final String FORM_PG_FORM = "formEntidadTerritorio:pgForm";
    private static final String I18N_MSG = "msgEntidadTerritorio";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-entidadTerritorio";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EntidadTerritorioBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private EntidadTerritorioLazyList lazyDataModel;

    private EntidadLazyList entidadLazyDataModel; 
    private TerritorioLazyList territorioLazyDataModel; 


    @ManagedProperty("#{entidadTerritorioFacade}")
    private EntidadTerritorioFacadeAPI entidadTerritorioFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new EntidadTerritorioDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new EntidadTerritorioLazyList();
        entidadLazyDataModel = new EntidadLazyList();
        territorioLazyDataModel = new TerritorioLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            entidadTerritorioFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            entidadTerritorioFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(EntidadTerritorioDTO entidadTerritorioDTO) {
        setBaseDTO(entidadTerritorioDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new EntidadTerritorioDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public EntidadTerritorioFacadeAPI getEntidadTerritorioFacade() {
        return entidadTerritorioFacade;
    }

    public void setEntidadTerritorioFacade(EntidadTerritorioFacadeAPI entidadTerritorioFacade) {
        this.entidadTerritorioFacade = entidadTerritorioFacade;
    }

    public EntidadTerritorioDTO getSelectedEntidadTerritorio() {
        return getBaseDTO();
    }
    
    public EntidadTerritorioLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(EntidadTerritorioLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public EntidadLazyList getEntidadLazyDataModel() {
        return entidadLazyDataModel;
    }

    public void setEntidadLazyDataModel(EntidadLazyList entidadLazyDataModel) {
        this.entidadLazyDataModel = entidadLazyDataModel;
    }

	public TerritorioLazyList getTerritorioLazyDataModel() {
        return territorioLazyDataModel;
    }

    public void setTerritorioLazyDataModel(TerritorioLazyList territorioLazyDataModel) {
        this.territorioLazyDataModel = territorioLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
