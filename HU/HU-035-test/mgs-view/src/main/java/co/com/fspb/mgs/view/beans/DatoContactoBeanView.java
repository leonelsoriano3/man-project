package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.DatoContactoLazyList;
import co.com.fspb.mgs.view.lazylist.PersonaLazyList;
import co.com.fspb.mgs.view.lazylist.TipoContactoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.DatoContactoDTO;
import co.com.fspb.mgs.facade.api.DatoContactoFacadeAPI;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link DatoContacto}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class DatoContactoBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class DatoContactoBeanView extends AbstractManagedBeanBase<DatoContactoDTO> {

    private static final String FORM_PG_FORM = "formDatoContacto:pgForm";
    private static final String I18N_MSG = "msgDatoContacto";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-datoContacto";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DatoContactoBeanView.class);

    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private DatoContactoLazyList lazyDataModel;

    private PersonaLazyList personaLazyDataModel; 
    private TipoContactoLazyList tipoContactoLazyDataModel; 

    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{datoContactoFacade}")
    private DatoContactoFacadeAPI datoContactoFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new DatoContactoDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new DatoContactoLazyList();
        personaLazyDataModel = new PersonaLazyList();
        tipoContactoLazyDataModel = new TipoContactoLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            datoContactoFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            datoContactoFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(DatoContactoDTO datoContactoDTO) {
        setBaseDTO(datoContactoDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new DatoContactoDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public DatoContactoFacadeAPI getDatoContactoFacade() {
        return datoContactoFacade;
    }

    public void setDatoContactoFacade(DatoContactoFacadeAPI datoContactoFacade) {
        this.datoContactoFacade = datoContactoFacade;
    }

    public DatoContactoDTO getSelectedDatoContacto() {
        return getBaseDTO();
    }
    
    public DatoContactoLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(DatoContactoLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public PersonaLazyList getPersonaLazyDataModel() {
        return personaLazyDataModel;
    }

    public void setPersonaLazyDataModel(PersonaLazyList personaLazyDataModel) {
        this.personaLazyDataModel = personaLazyDataModel;
    }

	public TipoContactoLazyList getTipoContactoLazyDataModel() {
        return tipoContactoLazyDataModel;
    }

    public void setTipoContactoLazyDataModel(TipoContactoLazyList tipoContactoLazyDataModel) {
        this.tipoContactoLazyDataModel = tipoContactoLazyDataModel;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
