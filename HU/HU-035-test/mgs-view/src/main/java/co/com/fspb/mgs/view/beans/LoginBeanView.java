package co.com.fspb.mgs.view.beans;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@ViewScoped
public class LoginBeanView {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginBeanView.class);
	
	public String login() {
	    try {
	    	FacesContext facesContext = FacesContext.getCurrentInstance();
		    ExternalContext extenalContext = facesContext.getExternalContext();
		    RequestDispatcher dispatcher = ((ServletRequest)extenalContext.getRequest()).getRequestDispatcher("/j_spring_security_check");
			dispatcher.forward((ServletRequest)extenalContext.getRequest(), (ServletResponse)extenalContext.getResponse());
			facesContext.responseComplete();
		} catch (ServletException e) {
		    LOGGER.error(e.getMessage(), e);
		} catch (IOException e) {
		    LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

}
