package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.EntidadLineaLazyList;
import co.com.fspb.mgs.view.lazylist.LineaIntervencionLazyList;
import co.com.fspb.mgs.view.lazylist.EntidadLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.EntidadLineaDTO;
import co.com.fspb.mgs.facade.api.EntidadLineaFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link EntidadLinea}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class EntidadLineaBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class EntidadLineaBeanView extends AbstractManagedBeanBase<EntidadLineaDTO> {

    private static final String FORM_PG_FORM = "formEntidadLinea:pgForm";
    private static final String I18N_MSG = "msgEntidadLinea";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-entidadLinea";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EntidadLineaBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private EntidadLineaLazyList lazyDataModel;

    private LineaIntervencionLazyList lineaIntervencionLazyDataModel; 
    private EntidadLazyList entidadLazyDataModel; 


    @ManagedProperty("#{entidadLineaFacade}")
    private EntidadLineaFacadeAPI entidadLineaFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new EntidadLineaDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new EntidadLineaLazyList();
        lineaIntervencionLazyDataModel = new LineaIntervencionLazyList();
        entidadLazyDataModel = new EntidadLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            entidadLineaFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            entidadLineaFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(EntidadLineaDTO entidadLineaDTO) {
        setBaseDTO(entidadLineaDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new EntidadLineaDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public EntidadLineaFacadeAPI getEntidadLineaFacade() {
        return entidadLineaFacade;
    }

    public void setEntidadLineaFacade(EntidadLineaFacadeAPI entidadLineaFacade) {
        this.entidadLineaFacade = entidadLineaFacade;
    }

    public EntidadLineaDTO getSelectedEntidadLinea() {
        return getBaseDTO();
    }
    
    public EntidadLineaLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(EntidadLineaLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public LineaIntervencionLazyList getLineaIntervencionLazyDataModel() {
        return lineaIntervencionLazyDataModel;
    }

    public void setLineaIntervencionLazyDataModel(LineaIntervencionLazyList lineaIntervencionLazyDataModel) {
        this.lineaIntervencionLazyDataModel = lineaIntervencionLazyDataModel;
    }

	public EntidadLazyList getEntidadLazyDataModel() {
        return entidadLazyDataModel;
    }

    public void setEntidadLazyDataModel(EntidadLazyList entidadLazyDataModel) {
        this.entidadLazyDataModel = entidadLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
