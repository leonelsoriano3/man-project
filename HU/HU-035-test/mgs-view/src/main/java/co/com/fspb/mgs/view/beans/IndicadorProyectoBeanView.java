package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.IndicadorProyectoLazyList;
import co.com.fspb.mgs.view.lazylist.IndicadorLazyList;
import co.com.fspb.mgs.view.lazylist.ProyectoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.IndicadorProyectoDTO;
import co.com.fspb.mgs.facade.api.IndicadorProyectoFacadeAPI;


/**
 * Managed Bean de Faces para la entidad {@link IndicadorProyecto}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class IndicadorProyectoBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class IndicadorProyectoBeanView extends AbstractManagedBeanBase<IndicadorProyectoDTO> {

    private static final String FORM_PG_FORM = "formIndicadorProyecto:pgForm";
    private static final String I18N_MSG = "msgIndicadorProyecto";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-indicadorProyecto";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(IndicadorProyectoBeanView.class);

    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private IndicadorProyectoLazyList lazyDataModel;

    private IndicadorLazyList indicadorLazyDataModel; 
    private ProyectoLazyList proyectoLazyDataModel; 


    @ManagedProperty("#{indicadorProyectoFacade}")
    private IndicadorProyectoFacadeAPI indicadorProyectoFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        setBaseDTO(new IndicadorProyectoDTO());
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new IndicadorProyectoLazyList();
        indicadorLazyDataModel = new IndicadorLazyList();
        proyectoLazyDataModel = new ProyectoLazyList();

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            indicadorProyectoFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            indicadorProyectoFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(IndicadorProyectoDTO indicadorProyectoDTO) {
        setBaseDTO(indicadorProyectoDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new IndicadorProyectoDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public IndicadorProyectoFacadeAPI getIndicadorProyectoFacade() {
        return indicadorProyectoFacade;
    }

    public void setIndicadorProyectoFacade(IndicadorProyectoFacadeAPI indicadorProyectoFacade) {
        this.indicadorProyectoFacade = indicadorProyectoFacade;
    }

    public IndicadorProyectoDTO getSelectedIndicadorProyecto() {
        return getBaseDTO();
    }
    
    public IndicadorProyectoLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(IndicadorProyectoLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public IndicadorLazyList getIndicadorLazyDataModel() {
        return indicadorLazyDataModel;
    }

    public void setIndicadorLazyDataModel(IndicadorLazyList indicadorLazyDataModel) {
        this.indicadorLazyDataModel = indicadorLazyDataModel;
    }

	public ProyectoLazyList getProyectoLazyDataModel() {
        return proyectoLazyDataModel;
    }

    public void setProyectoLazyDataModel(ProyectoLazyList proyectoLazyDataModel) {
        this.proyectoLazyDataModel = proyectoLazyDataModel;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
