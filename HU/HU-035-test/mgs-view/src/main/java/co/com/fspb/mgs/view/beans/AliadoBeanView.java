package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.AliadoLazyList;
import co.com.fspb.mgs.view.lazylist.TipoAliadoLazyList;
import co.com.fspb.mgs.view.lazylist.EntidadLazyList;
import co.com.fspb.mgs.view.lazylist.ProyectoLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.AliadoDTO;
import co.com.fspb.mgs.facade.api.AliadoFacadeAPI;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link Aliado}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class AliadoBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class AliadoBeanView extends AbstractManagedBeanBase<AliadoDTO> {

    private static final String FORM_PG_FORM = "formAliado:pgForm";
    private static final String I18N_MSG = "msgAliado";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-aliado";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AliadoBeanView.class);

    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private AliadoLazyList lazyDataModel;

    private TipoAliadoLazyList tipoAliadoLazyDataModel; 
    private EntidadLazyList entidadLazyDataModel; 
    private ProyectoLazyList proyectoLazyDataModel; 

    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{aliadoFacade}")
    private AliadoFacadeAPI aliadoFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new AliadoDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new AliadoLazyList();
        tipoAliadoLazyDataModel = new TipoAliadoLazyList();
        entidadLazyDataModel = new EntidadLazyList();
        proyectoLazyDataModel = new ProyectoLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            aliadoFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            aliadoFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(AliadoDTO aliadoDTO) {
        setBaseDTO(aliadoDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new AliadoDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public AliadoFacadeAPI getAliadoFacade() {
        return aliadoFacade;
    }

    public void setAliadoFacade(AliadoFacadeAPI aliadoFacade) {
        this.aliadoFacade = aliadoFacade;
    }

    public AliadoDTO getSelectedAliado() {
        return getBaseDTO();
    }
    
    public AliadoLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(AliadoLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public TipoAliadoLazyList getTipoAliadoLazyDataModel() {
        return tipoAliadoLazyDataModel;
    }

    public void setTipoAliadoLazyDataModel(TipoAliadoLazyList tipoAliadoLazyDataModel) {
        this.tipoAliadoLazyDataModel = tipoAliadoLazyDataModel;
    }

	public EntidadLazyList getEntidadLazyDataModel() {
        return entidadLazyDataModel;
    }

    public void setEntidadLazyDataModel(EntidadLazyList entidadLazyDataModel) {
        this.entidadLazyDataModel = entidadLazyDataModel;
    }

	public ProyectoLazyList getProyectoLazyDataModel() {
        return proyectoLazyDataModel;
    }

    public void setProyectoLazyDataModel(ProyectoLazyList proyectoLazyDataModel) {
        this.proyectoLazyDataModel = proyectoLazyDataModel;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
