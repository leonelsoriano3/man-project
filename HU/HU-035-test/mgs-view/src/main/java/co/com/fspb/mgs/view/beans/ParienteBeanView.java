package co.com.fspb.mgs.view.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.premize.pmz.api.exception.PmzException;
import co.com.fspb.mgs.view.lazylist.ParienteLazyList;
import co.com.fspb.mgs.view.lazylist.ParentescoLazyList;
import co.com.fspb.mgs.view.lazylist.PersonaLazyList;
import co.com.fspb.mgs.view.lazylist.BeneficiarioLazyList;
import com.premize.pmz.prime5.AbstractManagedBeanBase;
import com.premize.pmz.prime5.util.ManagedBeanUtils;
import com.premize.pmz.prime5.util.PrimeDateInterval;
import co.com.fspb.mgs.view.util.ConstantesWeb;
import co.com.fspb.mgs.dto.ParienteDTO;
import co.com.fspb.mgs.facade.api.ParienteFacadeAPI;

import co.com.fspb.mgs.enums.EstadoEnum;

/**
 * Managed Bean de Faces para la entidad {@link Pariente}
 * Extiende la interfaz de PMZ {@link AbstractManagedBeanBase}
 * 
 * @author PMZ - Premize S.A.S
 * @project Presentación PrimeFaces 5
 * @class ParienteBeanView
 * @date nov 11, 2016
 */
@ManagedBean
@ViewScoped
public class ParienteBeanView extends AbstractManagedBeanBase<ParienteDTO> {

    private static final String FORM_PG_FORM = "formPariente:pgForm";
    private static final String I18N_MSG = "msgPariente";
    private static final String I18N_BUNDLE_ENTIDAD = "i18n.msg-pariente";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ParienteBeanView.class);

    private List<SelectItem> estadoList; 
    private List<SelectItem> estadoFilter; 
    
    private Boolean idInputDisabled;
    private Boolean renderSaveEditButton;

    private ParienteLazyList lazyDataModel;

    private ParentescoLazyList parentescoLazyDataModel; 
    private PersonaLazyList personaLazyDataModel; 
    private BeneficiarioLazyList beneficiarioLazyDataModel; 

    private PrimeDateInterval fechaModificaInterval; 
    private PrimeDateInterval fechaCreaInterval; 

    @ManagedProperty("#{parienteFacade}")
    private ParienteFacadeAPI parienteFacade;

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    @PostConstruct
    public void init() {
        estadoList = ManagedBeanUtils
                .convertirSelectItem(EstadoEnum.class);
        estadoFilter = ManagedBeanUtils.convertirSelectItem(
                EstadoEnum.class,
                getMessage(I18N_BUNDLE_ENTIDAD, "lblEstado"));
    
        setBaseDTO(new ParienteDTO());
        fechaModificaInterval = new PrimeDateInterval("fechaModifica");
        fechaCreaInterval = new PrimeDateInterval("fechaCrea");
        idInputDisabled = false;
        renderSaveEditButton = true;

        lazyDataModel = new ParienteLazyList();
        parentescoLazyDataModel = new ParentescoLazyList();
        personaLazyDataModel = new PersonaLazyList();
        beneficiarioLazyDataModel = new BeneficiarioLazyList();

        List<PrimeDateInterval> dateIntervalList = new ArrayList<PrimeDateInterval>();
        dateIntervalList.add(fechaModificaInterval);
        dateIntervalList.add(fechaCreaInterval);
        lazyDataModel.setDateIntervalList(dateIntervalList);

    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionGuardar() {
        try {
            parienteFacade.guardar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_GUARDAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (PmzException ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionModificar() {
        try {
            parienteFacade.editar(getBaseDTO());
            String msg = getMessage(I18N_BUNDLE_ENTIDAD, ConstantesWeb.MSG_ACTUALIZAR_CODIGO.getValue())
                    + getBaseDTO().getId();
            addMessage(I18N_MSG, msg, FacesMessage.SEVERITY_INFO);
            RequestContext.getCurrentInstance().reset(FORM_PG_FORM);
            btnActionLimpiar();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            error(ex);
        }
        return "";
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public void actionModificarRegistro(ParienteDTO parienteDTO) {
        setBaseDTO(parienteDTO);
        renderSaveEditButton = false;
        idInputDisabled = true;
    }

    /**
     * @author PMZ - Premize S.A.S
     * @date nov 11, 2016
     */
    public String btnActionLimpiar() {
        setBaseDTO(new ParienteDTO());
        renderSaveEditButton = true;
        idInputDisabled = false;
        return "";
    }

    public ParienteFacadeAPI getParienteFacade() {
        return parienteFacade;
    }

    public void setParienteFacade(ParienteFacadeAPI parienteFacade) {
        this.parienteFacade = parienteFacade;
    }

    public ParienteDTO getSelectedPariente() {
        return getBaseDTO();
    }
    
    public ParienteLazyList getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(ParienteLazyList lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }
    
	public List<SelectItem> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<SelectItem> estadoList) {
        this.estadoList = estadoList;
    }

	public List<SelectItem> getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(List<SelectItem> estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

	public ParentescoLazyList getParentescoLazyDataModel() {
        return parentescoLazyDataModel;
    }

    public void setParentescoLazyDataModel(ParentescoLazyList parentescoLazyDataModel) {
        this.parentescoLazyDataModel = parentescoLazyDataModel;
    }

	public PersonaLazyList getPersonaLazyDataModel() {
        return personaLazyDataModel;
    }

    public void setPersonaLazyDataModel(PersonaLazyList personaLazyDataModel) {
        this.personaLazyDataModel = personaLazyDataModel;
    }

	public BeneficiarioLazyList getBeneficiarioLazyDataModel() {
        return beneficiarioLazyDataModel;
    }

    public void setBeneficiarioLazyDataModel(BeneficiarioLazyList beneficiarioLazyDataModel) {
        this.beneficiarioLazyDataModel = beneficiarioLazyDataModel;
    }

	public PrimeDateInterval getFechaModificaInterval() {
        return fechaModificaInterval;
    }

    public void setFechaModificaInterval(PrimeDateInterval fechaModificaInterval) {
        this.fechaModificaInterval = fechaModificaInterval;
    }

	public PrimeDateInterval getFechaCreaInterval() {
        return fechaCreaInterval;
    }

    public void setFechaCreaInterval(PrimeDateInterval fechaCreaInterval) {
        this.fechaCreaInterval = fechaCreaInterval;
    }

    public Boolean getIdInputDisabled() {
        return idInputDisabled;
    }

    public void setIdInputDisabled(Boolean idInputDisabled) {
        this.idInputDisabled = idInputDisabled;
    }

    public Boolean getRenderSaveEditButton() {
        return renderSaveEditButton;
    }

    public void setRenderSaveEditButton(Boolean renderSaveEditButton) {
        this.renderSaveEditButton = renderSaveEditButton;
    }
}
